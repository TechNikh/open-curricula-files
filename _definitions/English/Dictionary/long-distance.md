---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/long-distance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484427181
title: long-distance
categories:
    - Dictionary
---
long-distance
     adj : covering long distances; "a long-distance runner";
           "long-distance telephone"
     n : a telephone call made outside the local calling area [syn: {long-distance
         call}, {trunk call}]
