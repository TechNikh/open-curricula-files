---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thoroughly
offline_file: ""
offline_thumbnail: ""
uuid: 6482270c-beca-426f-b199-2f6656bf734b
updated: 1484310316
title: thoroughly
categories:
    - Dictionary
---
thoroughly
     adv 1: in a complete and thorough manner (`good' is sometimes used
            informally for `thoroughly'); "he was soundly
            defeated"; "we beat him good" [syn: {soundly}, {good}]
     2: in an exhaustive manner; "we searched the files thoroughly"
        [syn: {exhaustively}]
