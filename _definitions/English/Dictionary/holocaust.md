---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holocaust
offline_file: ""
offline_thumbnail: ""
uuid: 6f3a9e0b-6c57-4347-9a69-c6d7c74aaf37
updated: 1484310567
title: holocaust
categories:
    - Dictionary
---
holocaust
     n 1: an act of great destruction and loss of life
     2: the Nazi program of exterminating Jews under Hitler [syn: {final
        solution}]
