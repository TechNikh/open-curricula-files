---
version: 1
type: video
provider: YouTube
id: Kk9IDameJXk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Slope%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4eac3504-d505-4df9-90d3-39c1702ddd14
updated: 1486069573
title: 'Algebra- Slope 2'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Kk9IDameJXk/default.jpg
    - https://i3.ytimg.com/vi/Kk9IDameJXk/1.jpg
    - https://i3.ytimg.com/vi/Kk9IDameJXk/2.jpg
    - https://i3.ytimg.com/vi/Kk9IDameJXk/3.jpg
---
Second part of determining the slope of a line
