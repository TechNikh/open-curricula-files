---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dryland
offline_file: ""
offline_thumbnail: ""
uuid: 607764fd-a05c-437f-a30d-3c2ee325517d
updated: 1484308471
title: Drylands
categories:
    - Uncategorized
---
Drylands are defined by their scarcity of water. They are zones where precipitation is counterbalanced by evaporation from surfaces and transpiration by plants (evapotranspiration).[1] UNEP defines drylands as tropical and temperate areas with an aridity index of less than 0.65.[2] The drylands can be further classified into four sub-types: dry sub-humid lands, semi-arid lands, arid lands, and hyper-arid lands. Some authorities consider Hyper-arid lands as deserts (UNCCD) although a number of the world’s deserts include both hyper arid and arid climate zones. The UNCCD excludes hyper-arid zones from its definition of drylands.
