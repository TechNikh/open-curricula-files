---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inversely
offline_file: ""
offline_thumbnail: ""
uuid: 4860952e-24b4-48d8-befa-82cb0a8f11a1
updated: 1484310200
title: inversely
categories:
    - Dictionary
---
inversely
     adv : in an inverse or contrary manner; "inversely related";
           "wavelength and frequency are, of course, related
           reciprocally"- F.A.Geldard [syn: {reciprocally}]
