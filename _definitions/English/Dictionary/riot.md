---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/riot
offline_file: ""
offline_thumbnail: ""
uuid: 2b667011-34e7-44e2-8b25-c5415381a460
updated: 1484310589
title: riot
categories:
    - Dictionary
---
riot
     n 1: a public act of violence by an unruly mob [syn: {public
          violence}]
     2: a state of disorder involving group violence [syn: {rioting}]
     3: a joke that seems extremely funny [syn: {belly laugh}, {sidesplitter},
         {howler}, {thigh-slapper}, {scream}, {wow}]
     4: a wild gathering involving excessive drinking and
        promiscuity [syn: {orgy}, {debauch}, {debauchery}, {saturnalia},
         {bacchanal}, {bacchanalia}, {drunken revelry}]
     v 1: take part in a riot; disturb the public peace by engaging in
          a riot; "Students were rioting everywhere in 1968"
     2: engage in boisterous, drunken merry-making; "They were out
        carousing ...
