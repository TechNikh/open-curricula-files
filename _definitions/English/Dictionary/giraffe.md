---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/giraffe
offline_file: ""
offline_thumbnail: ""
uuid: f513350a-911a-409a-8b33-b93949bdc074
updated: 1484310293
title: giraffe
categories:
    - Dictionary
---
giraffe
     n : tallest living quadruped; having a spotted coat and small
         horns and very long neck and legs; of savannahs of
         tropical Africa [syn: {camelopard}, {Giraffa
         camelopardalis}]
