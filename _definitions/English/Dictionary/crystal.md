---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crystal
offline_file: ""
offline_thumbnail: ""
uuid: 89bddf18-8fa3-4a98-abab-c071d500f0d2
updated: 1484310405
title: crystal
categories:
    - Dictionary
---
crystal
     n 1: a solid formed by the solidification of a chemical and
          having a highly regular atomic structure
     2: a crystalline element used as a component in various
        electronic devices
     3: a rock formed by the solidification of a substance; has
        regularly repeating internal structure; external plane
        faces [syn: {crystallization}]
     4: colorless glass made of almost pure silica [syn: {quartz
        glass}, {quartz}, {vitreous silica}, {lechatelierite}]
     5: glassware made of quartz
     6: a protective cover that protects the face of a watch [syn: {watch
        crystal}, {watch glass}]
