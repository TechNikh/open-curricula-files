---
version: 1
type: video
provider: YouTube
id: YP_P6bYvEjE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Neuron%20Resting%20Potential.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e3bd2042-c194-4312-a920-a9f1a397b63b
updated: 1486069668
title: Neuron Resting Potential
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YP_P6bYvEjE/default.jpg
    - https://i3.ytimg.com/vi/YP_P6bYvEjE/1.jpg
    - https://i3.ytimg.com/vi/YP_P6bYvEjE/2.jpg
    - https://i3.ytimg.com/vi/YP_P6bYvEjE/3.jpg
---
