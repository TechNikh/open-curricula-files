---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extremal_graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: 2a3c1ef5-d561-4397-b9d4-0c94d7021d7b
updated: 1484309408
title: Extremal graph theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Turan_13-4.svg.png
tags:
    - Examples
    - History
    - Density results
    - Minimum degree conditions
    - Notes
categories:
    - Extremal graph theory
---
Extremal graph theory is a branch of the mathematical field of graph theory. Extremal graph theory studies extremal (maximal or minimal) graphs which satisfy a certain property. Extremality can be taken with respect to different graph invariants, such as order, size or girth. More abstractly, it studies how global properties of a graph influence local substructures of the graph.[1]
