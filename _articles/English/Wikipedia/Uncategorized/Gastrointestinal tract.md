---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enteric
offline_file: ""
offline_thumbnail: ""
uuid: e23fa2a9-1d71-43ac-8b27-606b84b07845
updated: 1484308378
title: Gastrointestinal tract
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Stomach_colon_rectum_diagram-en.svg.png
tags:
    - Human gastrointestinal tract
    - Structure
    - Upper gastrointestinal tract
    - Lower gastrointestinal tract
    - Small intestine
    - Large intestine
    - Development
    - Histology
    - Mucosa
    - Submucosa
    - Muscular layer
    - Adventitia and serosa
    - Function
    - Immune function
    - Immune barrier
    - Immune system homeostasis
    - Intestinal microbiota
    - Detoxification and drug metabolism
    - Clinical significance
    - Diseases
    - Symptoms
    - Treatment
    - Imaging
    - Other related diseases
    - Uses of animal guts
    - Other animals
categories:
    - Uncategorized
---
Gastrointestinal is an adjective meaning of or pertaining to the stomach and intestines. A tract is a collection of related anatomic structures or a series of connected body organs.
