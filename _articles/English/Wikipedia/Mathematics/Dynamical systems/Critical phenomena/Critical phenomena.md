---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_phenomena
offline_file: ""
offline_thumbnail: ""
uuid: 3446c87f-edeb-4a3d-bf60-43e7e895249d
updated: 1484308743
title: Critical phenomena
tags:
    - The critical point of the 2D Ising model
    - Divergences at the critical point
    - Critical exponents and universality
    - Critical dynamics
    - Ergodicity breaking
    - Mathematical tools
    - Applications
    - Brain and intelligence
    - Bibliography
categories:
    - Critical phenomena
---
In physics, critical phenomena is the collective name associated with the physics of critical points. Most of them stem from the divergence of the correlation length, but also the dynamics slows down. Critical phenomena include scaling relations among different quantities, power-law divergences of some quantities (such as the magnetic susceptibility in the ferromagnetic phase transition) described by critical exponents, universality, fractal behaviour, ergodicity breaking. Critical phenomena take place in second order phase transition, although not exclusively.
