---
version: 1
type: article
id: https://en.wikipedia.org/wiki/No-reserve_auction
offline_file: ""
offline_thumbnail: ""
uuid: 0efd48ae-e290-453a-9b64-bde768a7064d
updated: 1484309011
title: No-reserve auction
categories:
    - Auctions
---
A No-reserve auction (NR), also known as an absolute auction, is an auction in which the item for sale will be sold regardless of price.[1][2] From the seller's perspective, advertising an auction as having no reserve price can be desirable because it potentially attracts a greater number of bidders due to the possibility of a bargain.[1] If more bidders attend the auction, a higher price might ultimately be achieved because of heightened competition from bidders.[2] This contrasts with a reserve auction, where the item for sale may not be sold if the final bid is not high enough to satisfy the seller. In practice, an auction advertised as "absolute" or "no-reserve" may nonetheless still ...
