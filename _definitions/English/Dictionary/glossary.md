---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glossary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412001
title: glossary
categories:
    - Dictionary
---
glossary
     n : an alphabetical list of technical terms in some specialized
         field of knowledge; usually published as an appendix to a
         text on that field [syn: {gloss}]
