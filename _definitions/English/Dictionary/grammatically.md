---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grammatically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464861
title: grammatically
categories:
    - Dictionary
---
grammatically
     adv : in a grammatical manner; "this child already speaks
           grammatically" [ant: {ungrammatically}]
