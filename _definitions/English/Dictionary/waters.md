---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waters
offline_file: ""
offline_thumbnail: ""
uuid: dcb072fa-015e-4348-bf02-4bad775d0a66
updated: 1484310268
title: waters
categories:
    - Dictionary
---
Waters
     n 1: United States actress and singer (1896-1977) [syn: {Ethel
          Waters}]
     2: the serous fluid in which the embryo is suspended inside the
        amnion; "before a woman gives birth her waters break"
        [syn: {amniotic fluid}, {amnionic fluid}]
