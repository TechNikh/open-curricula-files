---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mall
offline_file: ""
offline_thumbnail: ""
uuid: 98959635-5b16-485b-8488-beef07368a40
updated: 1484310249
title: mall
categories:
    - Dictionary
---
mall
     n 1: a public area set aside as a pedestrian walk [syn: {promenade}]
     2: mercantile establishment consisting of a carefully
        landscaped complex of shops representing leading
        merchandisers; usually includes restaurants and a
        convenient parking area; a modern version of the
        traditional marketplace; "a good plaza should have a movie
        house"; "they spent their weekends at the local malls"
        [syn: {plaza}, {center}, {shopping mall}, {shopping center},
         {shopping centre}]
