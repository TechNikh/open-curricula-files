---
version: 1
type: video
provider: YouTube
id: VIVIegSt81k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 40e873fb-ca8d-4a5d-a6e3-3b6dfe4771fe
updated: 1486069660
title: Hexaflexagons
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/VIVIegSt81k/default.jpg
    - https://i3.ytimg.com/vi/VIVIegSt81k/1.jpg
    - https://i3.ytimg.com/vi/VIVIegSt81k/2.jpg
    - https://i3.ytimg.com/vi/VIVIegSt81k/3.jpg
---
