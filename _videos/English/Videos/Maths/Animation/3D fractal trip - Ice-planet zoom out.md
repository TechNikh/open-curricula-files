---
version: 1
type: video
provider: YouTube
id: waXtbgR23kg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 327d006b-64d5-435c-a5c4-19b6d06912f8
updated: 1486069644
title: '3D fractal trip - Ice-planet zoom out'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/waXtbgR23kg/default.jpg
    - https://i3.ytimg.com/vi/waXtbgR23kg/1.jpg
    - https://i3.ytimg.com/vi/waXtbgR23kg/2.jpg
    - https://i3.ytimg.com/vi/waXtbgR23kg/3.jpg
---
