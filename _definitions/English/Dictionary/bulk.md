---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bulk
offline_file: ""
offline_thumbnail: ""
uuid: 7ac0ffd0-fdc3-42f1-8e93-e26ee87ab37b
updated: 1484310529
title: bulk
categories:
    - Dictionary
---
bulk
     n 1: the property resulting from being or relating to the greater
          in number of two parts; the main part; "the majority of
          his customers prefer it"; "the bulk of the work is
          finished" [syn: {majority}] [ant: {minority}]
     2: the property of something that is great in magnitude; "it is
        cheaper to buy it in bulk"; "he received a mass of
        correspondence"; "the volume of exports" [syn: {mass}, {volume}]
     3: the property possessed by a large mass
     v 1: stick out or up; "The parcel bulked in the sack"
     2: cause to bulge or swell outwards [syn: {bulge}]
