---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hackenbush
offline_file: ""
offline_thumbnail: ""
uuid: a53d4066-6bd2-4589-b417-c0a248f16a96
updated: 1484308535
title: Hackenbush
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hackenbush_girl.svg.png
tags:
    - Gameplay
    - Variants
    - Analysis
categories:
    - Combinatorial game theory
---
Hackenbush is a two-player game invented by mathematician John Horton Conway.[1] It may be played on any configuration of colored line segments connected to one another by their endpoints and to a "ground" line.
