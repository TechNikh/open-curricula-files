---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gauss
offline_file: ""
offline_thumbnail: ""
uuid: e8a7a30b-a004-4d28-86d0-d9251f7bdf40
updated: 1484310140
title: gauss
categories:
    - Dictionary
---
gauss
     n 1: a unit of magnetic flux density equal to 1 maxwell per
          square centimeter
     2: German mathematician who developed the theory of numbers and
        who applied mathematics to electricity and magnetism and
        astronomy and geodesy (1777-1855) [syn: {Karl Gauss}, {Karl
        Friedrich Gauss}]
