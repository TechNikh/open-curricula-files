---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pee
offline_file: ""
offline_thumbnail: ""
uuid: 01f40e5c-7073-41f4-a984-a706343585ec
updated: 1484310413
title: pee
categories:
    - Dictionary
---
pee
     n 1: liquid excretory product; "there was blood in his urine";
          "the child had to make water" [syn: {urine}, {piss}, {piddle},
           {weewee}, {water}]
     2: informal terms for urination; "he took a pee" [syn: {peeing},
         {pissing}, {piss}]
     v : eliminate urine; "Again, the cat had made on the expensive
         rug" [syn: {make}, {urinate}, {piddle}, {puddle}, {micturate},
          {piss}, {pee-pee}, {make water}, {relieve oneself}, {take
         a leak}, {spend a penny}, {wee}, {wee-wee}, {pass water}]
