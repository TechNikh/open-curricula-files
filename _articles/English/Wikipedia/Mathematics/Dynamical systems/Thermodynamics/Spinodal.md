---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spinodal
offline_file: ""
offline_thumbnail: ""
uuid: 619a26fa-1977-4095-b9d2-d79b6d980e63
updated: 1484308868
title: Spinodal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-LCST-UCST_plot.svg_2.png
categories:
    - Thermodynamics
---
In thermodynamics, the limit of local stability with respect to small fluctuations is clearly defined by the condition that the second derivative of Gibbs free energy is zero. The locus of these points (the inflection point within a G-x or G-c curve, Gibbs free energy as a function of composition) is known as the spinodal curve.[1][2] For compositions within this curve, infinitesimally small fluctuations in composition and density will lead to phase separation via spinodal decomposition. Outside of the curve, the solution will be at least metastable with respect to fluctuations.[2] In other words, outside the spinodal curve some careful process may obtain a single phase system.[2] Inside ...
