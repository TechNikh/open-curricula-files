---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haematite
offline_file: ""
offline_thumbnail: ""
uuid: 48402287-edfa-4804-8cf8-f052533876b8
updated: 1484310409
title: haematite
categories:
    - Dictionary
---
haematite
     n : the principal form of iron ore; consists of ferric oxide in
         crystalline form; occurs in a red earthy form [syn: {hematite}]
