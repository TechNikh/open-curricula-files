---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Abhyankar%E2%80%93Moh_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f86ab153-24ce-4ac7-9cc6-9aede8d7759d
updated: 1484309352
title: Abhyankar–Moh theorem
categories:
    - Geometry stubs
---
In mathematics, the Abhyankar–Moh theorem states that if 
  
    
      
        L
      
    
    {\displaystyle L}
  
 is a complex line in the complex affine plane 
  
    
      
        
          
            C
          
          
            2
          
        
      
    
    {\displaystyle \mathbb {C} ^{2}}
  
, then every embedding of 
  
    
      
        L
      
    
    {\displaystyle L}
  
 into 
  
    
      
        
          
            C
          
          
            2
          
        
      
    
    {\displaystyle \mathbb {C} ^{2}}
  
 extends to an automorphism of the plane. It is named after Shreeram Shankar Abhyankar and T.-T. Moh, who published it ...
