---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/European_Association_for_Theoretical_Computer_Science
offline_file: ""
offline_thumbnail: ""
uuid: be2b8648-779e-4189-9516-56156a731d08
updated: 1484308671
title: European Association for Theoretical Computer Science
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Europe_green_light_0.png
tags:
    - EATCS Award
    - Presburger Award
    - EATCS Fellows
    - Texts in Theoretical Computer Science
    - EATCS Bulletin
    - ΕATCS Young Researchers Schools
categories:
    - Theoretical computer science
---
The European Association for Theoretical Computer Science (EATCS[1]) is an international organization with a European focus, founded in 1972. Its aim is to facilitate the exchange of ideas and results among theoretical computer scientists as well as to stimulate cooperation between the theoretical and the practical community in computer science.
