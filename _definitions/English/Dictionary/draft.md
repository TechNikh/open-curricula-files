---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/draft
offline_file: ""
offline_thumbnail: ""
uuid: d570cc1a-ee51-4ec4-8a98-34a1c0104051
updated: 1484310521
title: draft
categories:
    - Dictionary
---
draft
     n 1: a document ordering the payment of money; drawn by one
          person or bank on another [syn: {bill of exchange}, {order
          of payment}]
     2: a current of air (usually coming into a room or vehicle)
        [syn: {draught}]
     3: a preliminary sketch of a design or picture [syn: {rough
        drawing}]
     4: a serving of drink (usually alcoholic) drawn from a keg;
        "they served beer on draft" [syn: {draught}, {potation}, {tipple}]
     5: preliminary version of a written work [syn: {draft copy}]
     6: the depth of a vessel's keel below the surface (especially
        when loaded) [syn: {draught}]
     7: a regulator for controlling the flow of air ...
