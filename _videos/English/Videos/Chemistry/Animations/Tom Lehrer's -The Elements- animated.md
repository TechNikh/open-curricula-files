---
version: 1
type: video
provider: YouTube
id: zGM-wSKFBpo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2ec5d265-4074-480c-a449-dc9fbd0397a4
updated: 1486069662
title: |
    Tom Lehrer's "The Elements" animated
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zGM-wSKFBpo/default.jpg
    - https://i3.ytimg.com/vi/zGM-wSKFBpo/1.jpg
    - https://i3.ytimg.com/vi/zGM-wSKFBpo/2.jpg
    - https://i3.ytimg.com/vi/zGM-wSKFBpo/3.jpg
---
