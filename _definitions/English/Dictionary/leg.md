---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leg
offline_file: ""
offline_thumbnail: ""
uuid: f594e300-2e3e-4343-a693-0994645b6688
updated: 1484310289
title: leg
categories:
    - Dictionary
---
leg
     n 1: a human limb; commonly used to refer to a whole limb but
          technically only the part between the knee and ankle
     2: a structure in animals that is similar to a human leg and
        used for locomotion
     3: one of the supports for a piece of furniture
     4: a part of a forked or branching shape; "he broke off one of
        the branches"; "they took the south fork" [syn: {branch},
        {fork}, {ramification}]
     5: the limb of an animal used for food
     6: a prosthesis that replaces a missing leg [syn: {peg}, {wooden
        leg}, {pegleg}]
     7: cloth covering consisting of the part of a garment that
        covers the leg
     8: (nautical) the ...
