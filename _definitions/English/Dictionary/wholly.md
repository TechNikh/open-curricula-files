---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wholly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473921
title: wholly
categories:
    - Dictionary
---
wholly
     adv : to a complete degree or to the full or entire extent
           (`whole' is often used informally for `wholly'); "he
           was wholly convinced"; "entirely satisfied with the
           meal"; "it was completely different from what we
           expected"; "was completely at fault"; "a totally new
           situation"; "the directions were all wrong"; "it was
           not altogether her fault"; "an altogether new
           approach"; "a whole new idea" [syn: {entirely}, {completely},
            {totally}, {all}, {altogether}, {whole}] [ant: {partially}]
