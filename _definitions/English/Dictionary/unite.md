---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unite
offline_file: ""
offline_thumbnail: ""
uuid: a4437e10-1f38-48ca-bbfe-72e4ce650c41
updated: 1484310387
title: unite
categories:
    - Dictionary
---
unite
     v 1: act in concert or unite in a common purpose or belief [syn:
          {unify}] [ant: {divide}]
     2: become one; "Germany unified officially in 1990"; "Will the
        two Koreas unify?" [syn: {unify}, {merge}] [ant: {disunify}]
     3: have or possess in combination; "she unites charm with a
        good business sense" [syn: {combine}]
     4: be or become joined or united or linked; "The two streets
        connect to become a highway"; "Our paths joined"; "The
        travelers linked up again at the airport" [syn: {connect},
         {link}, {link up}, {join}]
     5: join or combine; "We merged our resources" [syn: {unify}, {merge}]
