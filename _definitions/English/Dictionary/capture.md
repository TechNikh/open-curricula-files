---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capture
offline_file: ""
offline_thumbnail: ""
uuid: e58b695c-c60d-4428-b770-d6c740261d11
updated: 1484310206
title: capture
categories:
    - Dictionary
---
capture
     n 1: the act of forcibly dispossessing an owner of property [syn:
           {gaining control}, {seizure}]
     2: a process whereby a star or planet holds an object in its
        gravitational field
     3: any process in which an atomic or nuclear system acquires an
        additional particle
     4: the act of taking of a person by force [syn: {seizure}]
     5: the removal of an opponent's piece from the chess board
     v 1: succeed in representing or expressing something intangible;
          "capture the essence of Spring"; "capture an idea"
     2: attract; cause to be enamored; "She captured all the men's
        hearts" [syn: {enamour}, {trance}, {catch}, {becharm}, ...
