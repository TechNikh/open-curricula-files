---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Endosulfan
offline_file: ""
offline_thumbnail: ""
uuid: 4efb3a10-c7e5-474b-9702-09a692474548
updated: 1484308466
title: Endosulfan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Yellow_toxicity_label.svg.png
tags:
    - Uses
    - Production
    - History of commercialization and regulation
    - Health effects
    - Toxicity
    - Endocrine disruption
    - Reproductive and developmental effects
    - Endosulfan and cancer
    - Environmental fate
    - Status by region
categories:
    - Uncategorized
---
Endosulfan is an off-patent organochlorine insecticide and acaricide that is being phased out globally. The two isomers, endo and exo, are known popularly as I and II. Endosulfan sulfate is a product of oxidation containing one extra O atom attached to the S atom. Endosulfan became a highly controversial agrichemical[2] due to its acute toxicity, potential for bioaccumulation, and role as an endocrine disruptor. Because of its threats to human health and the environment, a global ban on the manufacture and use of endosulfan was negotiated under the Stockholm Convention in April 2011. The ban has taken effect in mid-2012, with certain uses exempted for five additional years.[3] More than 80 ...
