---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hyperk%C3%A4hler_quotient'
offline_file: ""
offline_thumbnail: ""
uuid: bf618194-4b28-4f9d-af70-6a4919c6dc29
updated: 1484309223
title: Hyperkähler quotient
categories:
    - Differential geometry
---
In mathematics, the hyperkähler quotient of a hyperkähler manifold acted on by a group G is the quotient of a fiber of the hyperkähler moment map M→R3⊗g* over a G-fixed point (or more generally a G-orbit) by the action of G. It was introduced by N. J. Hitchin, A. Karlhede, and U. Lindström et al. (1987). It is a hyperkähler analogue of the Kähler quotient.
