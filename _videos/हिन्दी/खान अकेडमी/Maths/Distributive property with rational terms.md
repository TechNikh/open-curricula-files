---
version: 1
type: video
provider: YouTube
id: NSQv304Qw5Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 94226cf5-be94-4e4e-ac55-9cd65b51670a
updated: 1486069609
title: Distributive property with rational terms
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/NSQv304Qw5Y/default.jpg
    - https://i3.ytimg.com/vi/NSQv304Qw5Y/1.jpg
    - https://i3.ytimg.com/vi/NSQv304Qw5Y/2.jpg
    - https://i3.ytimg.com/vi/NSQv304Qw5Y/3.jpg
---
