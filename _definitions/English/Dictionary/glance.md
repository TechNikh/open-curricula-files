---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484403601
title: glance
categories:
    - Dictionary
---
glance
     n : a quick look [syn: {glimpse}, {coup d'oeil}]
     v 1: throw a glance at; take a brief look at; "She only glanced
          at the paper"; "I only peeked--I didn't see anything
          interesting" [syn: {peek}, {glint}]
     2: rebound after hitting; "The car caromed off several
        lampposts" [syn: {carom}]
