---
version: 1
type: video
provider: YouTube
id: gzLM6ltw3l0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Dark%20Energy%2C%20Cosmology%20part%202-%20Crash%20Course%20Astronomy%20%2343.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0fd34aeb-d205-49dd-9199-e5358b89fb07
updated: 1486069684
title: 'Dark Energy, Cosmology part 2- Crash Course Astronomy #43'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/gzLM6ltw3l0/default.jpg
    - https://i3.ytimg.com/vi/gzLM6ltw3l0/1.jpg
    - https://i3.ytimg.com/vi/gzLM6ltw3l0/2.jpg
    - https://i3.ytimg.com/vi/gzLM6ltw3l0/3.jpg
---
