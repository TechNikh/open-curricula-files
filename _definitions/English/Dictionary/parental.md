---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parental
offline_file: ""
offline_thumbnail: ""
uuid: bb31ddfe-f43a-4351-93ba-ac49c9987a45
updated: 1484310299
title: parental
categories:
    - Dictionary
---
parental
     adj 1: designating the generation of organisms from which hybrid
            offspring are produced [ant: {filial}]
     2: relating to or characteristic of or befitting a parent;
        "parental guidance" [syn: {maternal}, {paternal}] [ant: {filial}]
