---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_class_field_theory
offline_file: ""
offline_thumbnail: ""
uuid: 172e580e-4211-4838-a810-7eb742e1bf34
updated: 1484309149
title: Geometric class field theory
categories:
    - Algebraic geometry
---
In mathematics, geometric class field theory is an extension of class field theory to higher-dimensional geometrical objects: much the same way as class field theory describes the abelianization of the Galois group of a local or global field, geometric class field theory describes the abelianized fundamental group of higher dimensional schemes in terms of data related to algebraic cycles.
