---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Eguchi%E2%80%93Hanson_space'
offline_file: ""
offline_thumbnail: ""
uuid: 58dfbb3f-1020-49cc-abb9-d4b198b8661a
updated: 1484309216
title: Eguchi–Hanson space
categories:
    - Differential geometry
---
In mathematics and theoretical physics, the Eguchi–Hanson space is a non-compact, self-dual, asymptotically locally Euclidean (ALE) metric on the cotangent bundle of the 2-sphere T*S2. The holonomy group of this 4-real-dimensional manifold is SU(2), as it is for a Calabi-Yau K3 surface. While the metric is generally attributed to the physicists Eguchi and Hanson,[1] it was actually discovered independently by the mathematician Eugenio Calabi[2] around the same time.
