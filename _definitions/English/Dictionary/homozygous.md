---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homozygous
offline_file: ""
offline_thumbnail: ""
uuid: 00f67d60-c1e6-4feb-b3f6-bcc33c3d78ca
updated: 1484310303
title: homozygous
categories:
    - Dictionary
---
homozygous
     adj : having identical alleles at corresponding chromosomal loci;
           "these two fruit flies are homozygous for red eye
           color" [ant: {heterozygous}]
