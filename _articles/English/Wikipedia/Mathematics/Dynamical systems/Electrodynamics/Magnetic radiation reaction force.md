---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Magnetic_radiation_reaction_force
offline_file: ""
offline_thumbnail: ""
uuid: 9edafc4c-2ff4-4b37-889c-dbc006ab42c5
updated: 1484308760
title: Magnetic radiation reaction force
tags:
    - Definition and description
    - Background
    - Derivation
    - Signals from the future
categories:
    - Electrodynamics
---
it is when the electromagnet, one can derive an electric radiation reaction force for an accelerating charged particle caused by the particle emitting electromagnetic radiation. Likewise, a magnetic radiation reaction force can be derived for an accelerating magnetic moment emitting electromagnetic radiation.
