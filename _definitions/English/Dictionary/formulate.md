---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formulate
offline_file: ""
offline_thumbnail: ""
uuid: 53e371d6-76f1-4a5c-90e7-7b999c609e50
updated: 1484310138
title: formulate
categories:
    - Dictionary
---
formulate
     v 1: elaborate, as of theories and hypotheses; "Could you develop
          the ideas in your thesis" [syn: {explicate}, {develop}]
     2: come up with (an idea, plan, explanation, theory, or
        priciple) after a mental effort; "excogitate a way to
        measure the speed of light" [syn: {invent}, {contrive}, {devise},
         {excogitate}, {forge}]
     3: put into words or an expression; "He formulated his concerns
        to the board of trustees" [syn: {give voice}, {word}, {phrase},
         {articulate}]
     4: prepare according to a formula
