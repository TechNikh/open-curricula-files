---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Viviani%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1bc97cf1-8cbc-419b-a0c7-2689b391baf7
updated: 1484309130
title: "Viviani's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Viviani_Theorem.svg.png
tags:
    - Proof
    - Converse
    - Applications
    - Extensions
    - Parallelogram
    - Regular polygon
    - Equiangular polygon
    - Convex polygon
    - Regular polyhedron
    - Notes
categories:
    - Theorems in geometry
---
Viviani's theorem, named after Vincenzo Viviani, states that the sum of the distances from any interior point to the sides of an equilateral triangle equals the length of the triangle's altitude.[1]
