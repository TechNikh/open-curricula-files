---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contemplation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640421
title: contemplation
categories:
    - Dictionary
---
contemplation
     n 1: a long and thoughtful observation
     2: a calm lengthy intent consideration [syn: {reflection}, {reflexion},
         {rumination}, {musing}, {thoughtfulness}]
