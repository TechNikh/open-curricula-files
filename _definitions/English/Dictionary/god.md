---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/god
offline_file: ""
offline_thumbnail: ""
uuid: fe4f453f-8f5d-41e9-b110-1f1fed8ea92b
updated: 1484310563
title: god
categories:
    - Dictionary
---
God
     n 1: the supernatural being conceived as the perfect and
          omnipotent and omniscient originator and ruler of the
          universe; the object of worship in monotheistic
          religions [syn: {Supreme Being}]
     2: any supernatural being worshipped as controlling some part
        of the world or some aspect of life or who is the
        personification of a force [syn: {deity}, {divinity}, {immortal}]
     3: a man of such superior qualities that he seems like a deity
        to other people; "he was a god among men"
     4: a material effigy that is worshipped as a god; "thou shalt
        not make unto thee any graven image"; "money was his god"
        [syn: ...
