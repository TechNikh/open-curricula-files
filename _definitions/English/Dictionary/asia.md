---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asia
offline_file: ""
offline_thumbnail: ""
uuid: bdb16aa2-a3ad-43d8-8c64-2575e33549e2
updated: 1484310281
title: asia
categories:
    - Dictionary
---
Asia
     n 1: the largest continent with 60% of the earth's population; it
          is joined to Europe on the west to form Eurasia; it is
          the site of some of the world's earliest civilizations
     2: the nations of the Asian continent collectively
