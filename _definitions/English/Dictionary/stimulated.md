---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimulated
offline_file: ""
offline_thumbnail: ""
uuid: 78b270d5-128a-4b44-b731-0f0fe9ce7688
updated: 1484310328
title: stimulated
categories:
    - Dictionary
---
stimulated
     adj : emotionally aroused [syn: {stirred}, {stirred up}, {aroused}]
