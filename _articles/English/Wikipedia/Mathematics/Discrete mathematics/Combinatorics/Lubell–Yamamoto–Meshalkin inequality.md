---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lubell%E2%80%93Yamamoto%E2%80%93Meshalkin_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 4333752a-54fe-4fca-9184-25e5568e96a1
updated: 1484308585
title: Lubell–Yamamoto–Meshalkin inequality
categories:
    - Combinatorics
---
In combinatorial mathematics, the Lubell–Yamamoto–Meshalkin inequality, more commonly known as the LYM inequality, is an inequality on the sizes of sets in a Sperner family, proved by Bollobás (1965), Lubell (1966), Meshalkin (1963), and Yamamoto (1954). It is named for the initials of three of its discoverers.
