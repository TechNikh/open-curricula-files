---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/representation
offline_file: ""
offline_thumbnail: ""
uuid: dc72bf88-302a-41a8-b6b3-df2c8b8ba260
updated: 1484310330
title: representation
categories:
    - Dictionary
---
representation
     n 1: a presentation to the mind in the form of an idea or image
          [syn: {mental representation}, {internal representation}]
     2: a creation that is a visual or tangible rendering of someone
        or something
     3: the act of representing; standing in for someone or some
        group and speaking with authority in their behalf
     4: the state of serving as an official and authorized delegate
        or agent [syn: {delegacy}, {agency}]
     5: a body of legislators that serve in behalf of some
        constituency; "a Congressional vacancy occurred in the
        representation from California"
     6: a factual statement made by one party in order to ...
