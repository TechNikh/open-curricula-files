---
version: 1
type: video
provider: YouTube
id: pziWqz8j_a8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a436201b-d7f9-4826-816b-671ad357f4b5
updated: 1486069637
title: '"Reflecting Light" - a science experiment by Liam & Fionn'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/pziWqz8j_a8/default.jpg
    - https://i3.ytimg.com/vi/pziWqz8j_a8/1.jpg
    - https://i3.ytimg.com/vi/pziWqz8j_a8/2.jpg
    - https://i3.ytimg.com/vi/pziWqz8j_a8/3.jpg
---
