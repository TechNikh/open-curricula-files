---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rebel
offline_file: ""
offline_thumbnail: ""
uuid: 823bb526-e5d7-4267-b870-a2f5cb1fa542
updated: 1484310189
title: rebel
categories:
    - Dictionary
---
rebel
     adj 1: used by northerners of Confederate soldiers; "the rebel
            yell"
     2: participating in organized resistance to a constituted
        government; "the rebelling confederacy" [syn: {rebel(a)},
        {rebelling(a)}, {rebellious}]
     n 1: `johnny' was applied as a nickname for Confederate soldiers
          by the Federal soldiers in the American Civil War;
          `grayback' derived from their gray Confederate uniforms
          [syn: {Reb}, {Johnny Reb}, {Johnny}, {grayback}]
     2: a person who takes part in an armed rebellion against the
        constituted authority (especially in the hope of improving
        conditions) [syn: {insurgent}, ...
