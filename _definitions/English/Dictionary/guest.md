---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442721
title: guest
categories:
    - Dictionary
---
guest
     adj : staying temporarily; "a visiting foreigner"; "guest
           conductor" [syn: {visiting}, {guest(a)}]
     n 1: a visitor to whom hospitality is extended [syn: {invitee}]
     2: United States journalist (born in England) noted for his
        syndicated homey verse (1881-1959) [syn: {Edgar Guest}, {Edgar
        Albert Guest}]
     3: a customer of a hotel or restaurant etc.
     4: (computer science) any computer that is hooked up to a
        computer network [syn: {node}, {client}]
