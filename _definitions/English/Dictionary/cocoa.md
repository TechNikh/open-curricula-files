---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cocoa
offline_file: ""
offline_thumbnail: ""
uuid: dddbc9ae-02be-41d3-89a6-f68d099287ed
updated: 1484310577
title: cocoa
categories:
    - Dictionary
---
cocoa
     n 1: a beverage made from cocoa powder and milk and sugar;
          usually drunk hot [syn: {chocolate}, {hot chocolate}, {drinking
          chocolate}]
     2: powder of ground roasted cocao beans with most of the fat
        removed
