---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Low-density_parity-check_code
offline_file: ""
offline_thumbnail: ""
uuid: fcdfec5c-f404-42f5-9c12-16566b10e420
updated: 1484308567
title: Low-density parity-check code
tags:
    - History
    - Applications
    - Operational use
    - Example Encoder
    - Decoding
    - Updating node information
    - Lookup table decoding
    - Code construction
    - people
    - Theory
    - Applications
    - Other capacity-approaching codes
categories:
    - Coding theory
---
In information theory, a low-density parity-check (LDPC) code is a linear error correcting code, a method of transmitting a message over a noisy transmission channel.[1][2] An LDPC is constructed using a sparse bipartite graph.[3] LDPC codes are capacity-approaching codes, which means that practical constructions exist that allow the noise threshold to be set very close (or even arbitrarily close on the BEC) to the theoretical maximum (the Shannon limit) for a symmetric memoryless channel. The noise threshold defines an upper bound for the channel noise, up to which the probability of lost information can be made as small as desired. Using iterative belief propagation techniques, LDPC codes ...
