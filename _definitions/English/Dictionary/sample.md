---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sample
offline_file: ""
offline_thumbnail: ""
uuid: d327f036-eb5b-4ca8-943d-8cfa4267b66f
updated: 1484310262
title: sample
categories:
    - Dictionary
---
sample
     n 1: a small part of something intended as representative of the
          whole
     2: items selected at random from a population and used to test
        hypotheses about the population [syn: {sample distribution},
         {sampling}]
     3: all or part of a natural object that is collected and
        preserved as an example of its class
     v : take a sample of; "Try these new crackers"; "Sample the
         regional dishes" [syn: {try}, {try out}, {taste}]
