---
title: What percentage of U.S. freshwater is used for fracking?
youtube_id: QUklBISjpus
tags:
    - Hydraulic Fracturing (Film Subject)
    - fracking
    - shale
    - energyfromshale
categories:
    - Water management
---
