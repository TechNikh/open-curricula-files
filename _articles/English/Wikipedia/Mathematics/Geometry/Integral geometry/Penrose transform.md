---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Penrose_transform
offline_file: ""
offline_thumbnail: ""
uuid: 45dacceb-ca4d-4192-8940-25a5b2495b4c
updated: 1484309255
title: Penrose transform
tags:
    - Overview
    - Example
    - Penrose–Ward transform
categories:
    - Integral geometry
---
In mathematical physics, the Penrose transform, introduced by Roger Penrose (1967, 1968, 1969), is a complex analogue of the Radon transform that relates massless fields on spacetime to cohomology of sheaves on complex projective space. The projective space in question is the twistor space, a geometrical space naturally associated to the original spacetime, and the twistor transform is also geometrically natural in the sense of integral geometry. The Penrose transform is a major component of classical twistor theory.
