---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tsarist
offline_file: ""
offline_thumbnail: ""
uuid: b384930d-240c-4d73-9a81-c128e91a5069
updated: 1484310557
title: tsarist
categories:
    - Dictionary
---
tsarist
     adj : of or relating to or characteristic of a czar [syn: {czarist},
            {czaristic}, {tsaristic}, {tzarist}]
