---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Von_Neumann_paradox
offline_file: ""
offline_thumbnail: ""
uuid: 7c640b16-f64b-414d-b57f-4015abdf010d
updated: 1484308999
title: Von Neumann paradox
tags:
    - Sketch of the method
    - Consequences
    - Recent progress
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the von Neumann paradox, named after John von Neumann, is the idea that one can break a planar figure such as the unit square into sets of points and subject each set to an area-preserving affine transformation such that the result is two planar figures of the same size as the original. This was proved in 1929 by John von Neumann, assuming the axiom of choice. It is based on the earlier Banach–Tarski paradox, which is in turn based on the Hausdorff paradox.
