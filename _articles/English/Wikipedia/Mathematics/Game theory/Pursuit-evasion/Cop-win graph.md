---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cop-win_graph
offline_file: ""
offline_thumbnail: ""
uuid: 36326fc6-3e25-4c04-a876-facb58bcff31
updated: 1484309062
title: Cop-win graph
tags:
    - Definitions
    - Pursuit-evasion
    - Dismantling
    - Closure properties
    - Equivalence of cop-win and dismantlability
    - >
        Recognition algorithms and the family of all dismantling
        orders
    - Related families of graphs
categories:
    - Pursuit-evasion
---
In graph theory, a cop-win graph is an undirected graph on which the pursuer (cop) can always win a pursuit-evasion game in which he chases a robber, the players alternately moving along an edge of a graph or staying put, until the cop lands on the robber's vertex.[1] Finite cop-win graphs are also called dismantlable graphs or constructible graphs, because they can be dismantled by repeatedly removing a dominated vertex (one whose closed neighborhood is a subset of another vertex's neighborhood) or constructed by repeatedly adding such a vertex. The cop-win graphs can be recognized in polynomial time by a greedy algorithm that constructs a dismantling order. They include the chordal ...
