---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/khan
offline_file: ""
offline_thumbnail: ""
uuid: 65b7aba4-a834-4dca-9fe4-bc27aa6cf2db
updated: 1484310589
title: khan
categories:
    - Dictionary
---
khan
     n 1: a title given to rulers or other important people in Asian
          countries
     2: an inn in some Eastern countries with a large courtyard that
        provides accommodation for caravans [syn: {caravansary}, {caravanserai},
         {caravan inn}]
