---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Axiom_A
offline_file: ""
offline_thumbnail: ""
uuid: 40077277-e5f0-41e0-bbc5-b6b9d301f1a1
updated: 1484308773
title: Axiom A
tags:
    - Definition
    - Properties
    - Omega stability
categories:
    - Ergodic theory
---
In mathematics, Smale's axiom A defines a class of dynamical systems which have been extensively studied and whose dynamics is relatively well understood. A prominent example is the Smale horseshoe map. The term "axiom A" originates with Stephen Smale.[1][2] The importance of such systems is demonstrated by the chaotic hypothesis, which states that, 'for all practical purposes', a many-body thermostatted system is approximated by an Anosov system.[3]
