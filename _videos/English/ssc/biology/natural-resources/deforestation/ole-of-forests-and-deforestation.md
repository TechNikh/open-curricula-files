---
title: Understanding the role of forests and deforestation on local, regional and global precipitations
youtube_id: 2_R4Sp_-Kfg
tags:
    - Local
    - Global
    - Documentary
categories:
    - Deforestation
---
