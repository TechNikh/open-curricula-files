---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vomit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408281
title: vomit
categories:
    - Dictionary
---
vomit
     n 1: the matter ejected in vomiting [syn: {vomitus}, {puke}]
     2: a medicine that induces nausea and vomiting [syn: {emetic},
        {vomitive}, {nauseant}]
     3: the reflex act of ejecting the contents of the stomach
        through the mouth [syn: {vomiting}, {emesis}, {regurgitation},
         {disgorgement}, {puking}]
     v : eject the contents of the stomach through the mouth; "After
         drinking too much, the students vomited"; "He purged
         continuously"; "The patient regurgitated the food we gave
         him last night" [syn: {vomit up}, {purge}, {cast}, {sick},
          {cat}, {be sick}, {disgorge}, {regorge}, {retch}, {puke},
          {barf}, ...
