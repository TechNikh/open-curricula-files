---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chick
offline_file: ""
offline_thumbnail: ""
uuid: 040894e0-a675-4b26-af31-0c02ba193ec7
updated: 1484310286
title: chick
categories:
    - Dictionary
---
chick
     n 1: young bird especially of domestic fowl [syn: {biddy}]
     2: informal terms for a (young) woman [syn: {dame}, {doll}, {wench},
         {skirt}, {bird}]
