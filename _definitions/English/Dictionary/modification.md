---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modification
offline_file: ""
offline_thumbnail: ""
uuid: 1b61ec0e-fd3c-4dfd-9e5c-0edb51988efb
updated: 1484310214
title: modification
categories:
    - Dictionary
---
modification
     n 1: the act of making something different (as e.g. the size of a
          garment) [syn: {alteration}, {adjustment}]
     2: slightly modified copy; not an exact copy; "a modification
        of last year's model"
     3: the grammatical relation that exists when a word qualifies
        the meaning of the phrase [syn: {qualifying}, {limiting}]
     4: an event that occurs when something passes from one state or
        phase to another; "the change was intended to increase
        sales"; "this storm is certainly a change for the worse";
        "the neighborhood had undergone few modifications since
        his last visit years ago" [syn: {change}, {alteration}]
