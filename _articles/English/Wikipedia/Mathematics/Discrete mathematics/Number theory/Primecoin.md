---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primecoin
offline_file: ""
offline_thumbnail: ""
uuid: 40ea059a-4ce1-44a6-b678-d5c7bd7915e9
updated: 1484308652
title: Primecoin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Primecoin_Logo.png
tags:
    - features
    - Proof-of-work system
categories:
    - Number theory
---
Primecoin (sign: Ψ; code: XPM) is a peer-to-peer open source cryptocurrency that implements a unique scientific computing proof-of-work system.[2] Primecoin's proof-of-work system searches for chains of prime numbers.[2] Primecoin was created by a person or group of people who use the pseudonym Sunny King. This entity is also related with the cryptocurrency Peercoin.[3][4] The Primecoin source code is copyrighted by a person or group called “Primecoin Developers”, and distributed under a conditional MIT/X11 software license.[5]
