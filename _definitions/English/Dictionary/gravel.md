---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gravel
offline_file: ""
offline_thumbnail: ""
uuid: fad3c075-e597-4168-97a1-45cebe2c0719
updated: 1484310264
title: gravel
categories:
    - Dictionary
---
gravel
     adj : unpleasantly harsh or grating in sound; "a gravelly voice"
           [syn: {grating}, {gravelly}, {rasping}, {raspy}, {rough}]
     n : rock fragments and pebbles [syn: {crushed rock}]
     v 1: cause annoyance in; disturb, especially by minor
          irritations; "Mosquitoes buzzing in my ear really
          bothers me"; "It irritates me that she never closes the
          door after she leaves" [syn: {annoy}, {rag}, {get to}, {bother},
           {get at}, {irritate}, {rile}, {nark}, {nettle}, {vex},
          {chafe}, {devil}]
     2: cover with gravel; "We gravelled the driveway"
     3: be a mystery or bewildering to; "This beats me!"; "Got me--I
        don't ...
