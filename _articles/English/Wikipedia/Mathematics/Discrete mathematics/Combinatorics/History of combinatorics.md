---
version: 1
type: article
id: https://en.wikipedia.org/wiki/History_of_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 5a208df3-ec48-42fe-ac08-4fd131c14793
updated: 1484308565
title: History of combinatorics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg.png
tags:
    - Earliest records
    - Combinatorics in the West
    - Notes
categories:
    - Combinatorics
---
