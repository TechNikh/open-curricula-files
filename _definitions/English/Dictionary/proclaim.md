---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proclaim
offline_file: ""
offline_thumbnail: ""
uuid: 8fe5df21-29ed-474c-96c2-3284af22dfed
updated: 1484310593
title: proclaim
categories:
    - Dictionary
---
proclaim
     v 1: declare formally; declare someone to be something; of
          titles; "He was proclaimed King"
     2: state or announce; "`I am not a Communist,' " he exclaimed;
        "The King will proclaim an amnesty" [syn: {exclaim}, {promulgate}]
     3: affirm or declare as an attribute or quality of; "The speech
        predicated the fitness of the candidate to be President"
        [syn: {predicate}]
     4: praise, glorify, or honor; "extol the virtues of one's
        children"; "glorify one's spouse's cooking" [syn: {laud},
        {extol}, {exalt}, {glorify}]
