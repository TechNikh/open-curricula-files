---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atomic
offline_file: ""
offline_thumbnail: ""
uuid: 90b173ed-7151-4d19-be21-d949e6e8ab76
updated: 1484310373
title: atomic
categories:
    - Dictionary
---
atomic
     adj 1: of or relating to or comprising atoms; "atomic structure";
            "atomic hydrogen"
     2: (weapons) deriving destructive energy from  the release of
        atomic energy; "nuclear war"; "nuclear weapons"; "atomic
        bombs" [syn: {nuclear}] [ant: {conventional}]
     3: immeasurably small [syn: {atomlike}, {minute}]
