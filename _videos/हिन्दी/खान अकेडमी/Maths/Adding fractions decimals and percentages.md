---
version: 1
type: video
provider: YouTube
id: RTnR0P37rPc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8013dbb7-ffe6-47bb-9448-18824d2e3d52
updated: 1486069605
title: Adding fractions decimals and percentages
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/RTnR0P37rPc/default.jpg
    - https://i3.ytimg.com/vi/RTnR0P37rPc/1.jpg
    - https://i3.ytimg.com/vi/RTnR0P37rPc/2.jpg
    - https://i3.ytimg.com/vi/RTnR0P37rPc/3.jpg
---
