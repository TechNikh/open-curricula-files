---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ash
offline_file: ""
offline_thumbnail: ""
uuid: 16553e0d-47c6-47b9-801c-86fd4279f9ed
updated: 1484310399
title: ash
categories:
    - Dictionary
---
ash
     n 1: the residue that remains when something is burned
     2: any of various deciduous pinnate-leaved ornamental or timber
        trees of the genus Fraxinus [syn: {ash tree}]
     3: strong elastic wood of any of various ash trees; used for
        furniture and tool handles and sporting goods such as
        baseball bats
     v : convert into ashes
