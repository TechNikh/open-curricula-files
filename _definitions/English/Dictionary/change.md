---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/change
offline_file: ""
offline_thumbnail: ""
uuid: fb6715b8-8b9f-4b81-b90d-d161f68b97fc
updated: 1484310337
title: change
categories:
    - Dictionary
---
change
     n 1: an event that occurs when something passes from one state or
          phase to another; "the change was intended to increase
          sales"; "this storm is certainly a change for the
          worse"; "the neighborhood had undergone few
          modifications since his last visit years ago" [syn: {alteration},
           {modification}]
     2: a relational difference between states; especially between
        states before and after some event; "he attributed the
        change to their marriage"
     3: the action of changing something; "the change of government
        had no impact on the economy"; "his change on abortion
        cost him the election"
     4: the ...
