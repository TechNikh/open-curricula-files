---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shed
offline_file: ""
offline_thumbnail: ""
uuid: 0748efc6-631f-47a5-83de-4c786b6cd178
updated: 1484310522
title: shed
categories:
    - Dictionary
---
shed
     adj : shed at an early stage of development; "most amphibians have
           caducous gills"; "the caducous calyx of a poppy" [syn:
           {caducous}] [ant: {persistent}]
     n : an outbuilding with a single story; used for shelter or
         storage
     v 1: get rid of; "he shed his image as a pushy boss"; "shed your
          clothes" [syn: {cast}, {cast off}, {shake off}, {throw},
           {throw off}, {throw away}, {drop}]
     2: pour out in drops or small quantities or as if in drops or
        small quantities; "shed tears"; "spill blood"; "God shed
        His grace on Thee" [syn: {spill}, {pour forth}]
     3: cause or allow (a solid substance) to flow or run ...
