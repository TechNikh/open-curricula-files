---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calculating
offline_file: ""
offline_thumbnail: ""
uuid: cc9dcdf6-a362-44f6-970a-0a1cfc8073a6
updated: 1484310447
title: calculating
categories:
    - Dictionary
---
calculating
     adj : used of persons; "the most calculating and selfish men in
           the community" [syn: {calculative}, {conniving}, {scheming},
            {shrewd}]
