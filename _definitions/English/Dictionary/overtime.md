---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overtime
offline_file: ""
offline_thumbnail: ""
uuid: 094aaf29-1129-48d4-9b1e-86da893b3318
updated: 1484310457
title: overtime
categories:
    - Dictionary
---
overtime
     n 1: work done in addition to regular working hours
     2: playing time beyond regulation, to break a tie [syn: {extra
        time}] [ant: {regulation time}]
     adv : beyond the regular time; "she often has to work overtime"
