---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Torrence_Parsons
offline_file: ""
offline_thumbnail: ""
uuid: 870051b7-7811-4be2-9ae3-fccec00efaf6
updated: 1484309472
title: Torrence Parsons
categories:
    - Graph theorists
---
He worked mainly in graph theory, and is known for introducing a graph-theoretic view of pursuit-evasion problems (Parsons 1976, 1978). He obtained his Ph.D. from Princeton University in 1966 under the supervision of Albert W. Tucker.[1]
