---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seasoned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484574901
title: seasoned
categories:
    - Dictionary
---
seasoned
     adj 1: aged or processed; "seasoned wood" [ant: {unseasoned}]
     2: having been given flavor (as by seasoning) [syn: {flavored},
         {flavoured}]
     3: rendered competent through trial and experience; "troops
        seasoned in combat"; "a seasoned traveler"; "veteran
        steadiness"; "a veteran officer" [syn: {veteran(a)}]
