---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/covalency
offline_file: ""
offline_thumbnail: ""
uuid: a7ba6635-06f9-4354-9bb4-c18109e15a92
updated: 1484310405
title: covalency
categories:
    - Dictionary
---
covalency
     n : valence characterized by the sharing of electrons in a
         chemical compound; the number of pairs of electrons an
         atom can share [syn: {covalence}]
