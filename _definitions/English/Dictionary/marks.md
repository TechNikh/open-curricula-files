---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marks
offline_file: ""
offline_thumbnail: ""
uuid: 23d79513-60ef-4fcd-bda1-ce2e2e0006a8
updated: 1484310284
title: marks
categories:
    - Dictionary
---
Marks
     n : English businessman who created a retail chain (1888-1964)
         [syn: {Simon Marks}, {First Baron Marks of Broughton}]
