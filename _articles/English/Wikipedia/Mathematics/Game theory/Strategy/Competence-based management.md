---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Competence-based_management
offline_file: ""
offline_thumbnail: ""
uuid: 621d8888-daa5-47c5-8c82-52a0f8c33542
updated: 1484309063
title: Competence-based management
tags:
    - Aspects of the competent organization
    - Five modes of competence
    - Notes
categories:
    - Strategy
---
Competence-based Strategic Management is a way of thinking about how organizations gain high performance for a significant period of time. Established as a theory in the early 1990s, competence-based strategic management theory explains how organizations can develop sustainable competitive advantage in a systematic and structural way. The theory of competence-based strategic management is an integrative strategy theory that incorporates economic, organizational and behavioural concerns in a framework that is dynamic, systemic, cognitive and holistic (Sanchez and Heene, 2004). This theory defines competence as: the ability to sustain the coordinated deployment of resources in ways that helps ...
