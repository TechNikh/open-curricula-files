---
version: 1
type: video
provider: YouTube
id: 405boztgZig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Linear%20equations%20and%20functions/x-%20and%20y-intercepts%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3217b504-0d6e-4872-9655-7545e79f7fc1
updated: 1486069563
title: 'x- and y-intercepts 2'
categories:
    - Linear equations and functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/405boztgZig/default.jpg
    - https://i3.ytimg.com/vi/405boztgZig/1.jpg
    - https://i3.ytimg.com/vi/405boztgZig/2.jpg
    - https://i3.ytimg.com/vi/405boztgZig/3.jpg
---
