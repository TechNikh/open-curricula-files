---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%B4di_Husimi'
offline_file: ""
offline_thumbnail: ""
uuid: 5b85485d-446f-47f1-a4d0-edc1162f5678
updated: 1484309463
title: Kôdi Husimi
tags:
    - Education and career
    - Contributions
    - Physics
    - Graph theory
    - Pacifism and world affairs
    - Recreational mathematics
    - Additional reading
categories:
    - Graph theorists
---
Kôdi Husimi (June 29, 1909 – May 8, 2008, Japanese: 伏見康治) was a Japanese theoretical physicist who served as the president of the Science Council of Japan.[1] Husimi trees in graph theory, and the Husimi Q representation in quantum mechanics, are named after him.
