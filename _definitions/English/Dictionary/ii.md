---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ii
offline_file: ""
offline_thumbnail: ""
uuid: 158ef8d3-efb6-4e9a-8e03-70279add950a
updated: 1484310319
title: ii
categories:
    - Dictionary
---
ii
     adj : being one more than one; "he received two messages" [syn: {two},
            {2}]
     n : the cardinal number that is the sum of one and one or a
         numeral representing this number [syn: {two}, {2}, {deuce}]
