---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lambda2_method
offline_file: ""
offline_thumbnail: ""
uuid: 3ba871e6-aded-45fc-bc61-99eb7737c27d
updated: 1484308874
title: Lambda2 method
categories:
    - Vortices
---
The Lambda2 method, or Lambda2 vortex criterion, is a detection algorithm that can adequately identify vortices from a three-dimensional velocity field.[1] The Lambda2 method is Galilean invariant, which means it produces the same results when a uniform velocity field is added to the existing velocity field or when the field is translated.
