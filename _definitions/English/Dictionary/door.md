---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/door
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484369941
title: door
categories:
    - Dictionary
---
door
     n 1: a swinging or sliding barrier that will close the entrance
          to a room or building or vehicle; "he knocked on the
          door"; "he slammed the door as he left"
     2: the entrance (the space in a wall) through which you enter
        or leave a room or building; the space that a door can
        close; "he stuck his head in the doorway" [syn: {doorway},
         {room access}, {threshold}]
     3: anything providing a means of access (or escape); "we closed
        the door to Haitian immigrants"; "education is the door to
        success"
     4: a structure where people live or work (usually ordered along
        a street or road); "the office next door"; "they ...
