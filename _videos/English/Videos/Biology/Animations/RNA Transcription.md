---
version: 1
type: video
provider: YouTube
id: IsMHhnGILwg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 404f2f6b-fc02-4172-80a7-81c12454cbcd
updated: 1486069673
title: RNA Transcription
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/IsMHhnGILwg/default.jpg
    - https://i3.ytimg.com/vi/IsMHhnGILwg/1.jpg
    - https://i3.ytimg.com/vi/IsMHhnGILwg/2.jpg
    - https://i3.ytimg.com/vi/IsMHhnGILwg/3.jpg
---
