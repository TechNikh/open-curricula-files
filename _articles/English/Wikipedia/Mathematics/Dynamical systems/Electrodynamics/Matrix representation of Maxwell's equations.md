---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Matrix_representation_of_Maxwell%27s_equations'
offline_file: ""
offline_thumbnail: ""
uuid: d929812e-d174-4701-af2c-a16a891265b4
updated: 1484308759
title: "Matrix representation of Maxwell's equations"
tags:
    - Introduction
    - Homogeneous medium
    - Inhomogeneous medium
    - Applications
    - Notes
    - Others
categories:
    - Electrodynamics
---
In electromagnetism, a branch of fundamental physics, the matrix representations of the Maxwell's equations are a formulation of Maxwell's equations using matrices, complex numbers, and vector calculus. These representations are for a homogeneous medium, an approximation in an inhomogeneous medium. A matrix representation for an inhomogeneous medium was presented using a pair of matrix equations.[1] A single equation using 4 × 4 matrices is necessary and sufficient for any homogeneous medium. For an inhomogeneous medium it necessarily requires 8 × 8 matrices.[2]
