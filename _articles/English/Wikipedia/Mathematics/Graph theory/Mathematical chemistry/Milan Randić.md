---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Milan_Randi%C4%87'
offline_file: ""
offline_thumbnail: ""
uuid: 05947ef0-1bb4-4145-976d-ba65ac4a58e7
updated: 1484309434
title: Milan Randić
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Galleria_vittorio_emanuele_ii_02.jpg
tags:
    - Birth and education
    - Academic career
    - Research and achievements
    - Selected publications
categories:
    - Mathematical chemistry
---
