---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strong_orientation
offline_file: ""
offline_thumbnail: ""
uuid: 6f755689-6393-4d70-8751-f783e39d3442
updated: 1484309405
title: Strong orientation
tags:
    - Application to traffic control
    - Related types of orientation
    - Flip graphs
    - Algorithms and Complexity
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a strong orientation of an undirected graph is an assignment of a direction to each edge (an orientation) that makes it into a strongly connected graph.
