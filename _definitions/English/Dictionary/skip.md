---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skip
offline_file: ""
offline_thumbnail: ""
uuid: 7d8f8185-32d8-4a0a-a253-84b084f5d8c4
updated: 1484310413
title: skip
categories:
    - Dictionary
---
skip
     n 1: a gait in which steps and hops alternate
     2: a mistake resulting from neglect [syn: {omission}]
     v 1: bypass; "He skipped a row in the text and so the sentence
          was incomprehensible" [syn: {jump}, {pass over}, {skip
          over}]
     2: intentionally fail to attend; "cut class" [syn: {cut}]
     3: jump lightly [syn: {hop}, {hop-skip}]
     4: leave suddenly; "She persuaded him to decamp"; "skip town"
        [syn: {decamp}, {vamoose}]
     5: bound off one point after another [syn: {bound off}]
     6: cause to skip over a surface; "Skip a stone across the pond"
        [syn: {skim}, {skitter}]
     [also: {skipping}, {skipped}]
