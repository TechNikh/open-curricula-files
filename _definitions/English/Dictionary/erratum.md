---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erratum
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464382
title: erratum
categories:
    - Dictionary
---
erratum
     n : a mistake in printed matter resulting from mechanical
         failures of some kind [syn: {misprint}, {typographical
         error}, {typo}, {literal error}, {literal}]
     [also: {errata} (pl)]
