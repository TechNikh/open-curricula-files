---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mighty
offline_file: ""
offline_thumbnail: ""
uuid: bd83cba6-06ab-497e-ad49-8d0dda451927
updated: 1484310439
title: mighty
categories:
    - Dictionary
---
mighty
     adj : having or showing great strength or force or intensity;
           "struck a mighty blow"; "the mighty logger Paul
           Bunyan"; "the pen is mightier than the sword"-
           Bulwer-Lytton
     adv : (Southern regional intensive) very; "the baby is mighty
           cute"; "he's mighty tired"; "it is powerful humid";
           "that boy is powerful big now"; "they have a right nice
           place" [syn: {powerful}, {right}]
     [also: {mightiest}, {mightier}]
