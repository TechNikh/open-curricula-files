---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-confident
offline_file: ""
offline_thumbnail: ""
uuid: a356a83b-8d48-42da-b1c9-873c397f7c5f
updated: 1484310448
title: self-confident
categories:
    - Dictionary
---
self-confident
     adj : showing poise and confidence in your own worth; "hardly more
           than a boy but firm-knit and self-confident" [syn: {self-assured}]
