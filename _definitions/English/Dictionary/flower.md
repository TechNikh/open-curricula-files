---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flower
offline_file: ""
offline_thumbnail: ""
uuid: 962b0656-9375-443a-bc98-4c47e2646d19
updated: 1484310303
title: flower
categories:
    - Dictionary
---
flower
     n 1: a plant cultivated for its blooms or blossoms
     2: reproductive organ of angiosperm plants especially one
        having showy or colorful parts [syn: {bloom}, {blossom}]
     3: the period of greatest prosperity or productivity [syn: {prime},
         {peak}, {heyday}, {bloom}, {blossom}, {efflorescence}, {flush}]
     v : produce or yield flowers; "The cherry tree bloomed" [syn: {bloom},
          {blossom}]
