---
version: 1
type: video
provider: YouTube
id: VMEV__2wW3E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Deductive%20reasoning%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7c317ee1-ab18-469a-bb0a-b603253355f0
updated: 1486069559
title: Deductive reasoning 2
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/VMEV__2wW3E/default.jpg
    - https://i3.ytimg.com/vi/VMEV__2wW3E/1.jpg
    - https://i3.ytimg.com/vi/VMEV__2wW3E/2.jpg
    - https://i3.ytimg.com/vi/VMEV__2wW3E/3.jpg
---
