---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enrol
offline_file: ""
offline_thumbnail: ""
uuid: 37d81460-9e50-424d-a53b-19d3851d429e
updated: 1484310172
title: enrol
categories:
    - Dictionary
---
enrol
     v : register formally as a participant or member; "The party
         recruited many new members" [syn: {enroll}, {inscribe}, {enter},
          {recruit}]
     [also: {enrolling}, {enrolled}]
