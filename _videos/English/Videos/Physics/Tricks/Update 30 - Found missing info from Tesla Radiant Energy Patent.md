---
version: 1
type: video
provider: YouTube
id: geYfUHh6nD8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: df88c580-5a34-4967-9095-714fa8d5528e
updated: 1486069657
title: 'Update 30 - Found missing info from Tesla Radiant Energy Patent'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/geYfUHh6nD8/default.jpg
    - https://i3.ytimg.com/vi/geYfUHh6nD8/1.jpg
    - https://i3.ytimg.com/vi/geYfUHh6nD8/2.jpg
    - https://i3.ytimg.com/vi/geYfUHh6nD8/3.jpg
---
