---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interpreting
offline_file: ""
offline_thumbnail: ""
uuid: 7e1e566d-161b-4334-b23c-6ee80e0b3f49
updated: 1484310373
title: interpreting
categories:
    - Dictionary
---
interpreting
     n : an explanation of something that is not immediately obvious;
         "the edict was subject to many interpretations"; "he
         annoyed us with his interpreting of parables"; "often
         imitations are extended to provide a more accurate
         rendition of the child's intended meaning" [syn: {interpretation},
          {rendition}, {rendering}]
