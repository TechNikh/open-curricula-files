---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verification
offline_file: ""
offline_thumbnail: ""
uuid: a57da401-9d24-4ede-a94a-146987d7d32c
updated: 1484310221
title: verification
categories:
    - Dictionary
---
verification
     n 1: additional proof that something that was believed (some fact
          or hypothesis or theory) is correct; "fossils provided
          further confirmation of the evolutionary theory" [syn: {confirmation},
           {check}, {substantiation}]
     2: (law) an affidavit attached to a statement confirming the
        truth of that statement
