---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_topology
offline_file: ""
offline_thumbnail: ""
uuid: c0c54e9f-88a5-435a-bfe6-6be096f28d88
updated: 1484308544
title: Arithmetic topology
tags:
    - Analogies
    - History
    - Notes
categories:
    - Number theory
---
Arithmetic topology is an area of mathematics that is a combination of algebraic number theory and topology. It establishes an analogy between number fields and closed, orientable 3-manifolds.
