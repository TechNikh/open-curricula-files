---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bullock
offline_file: ""
offline_thumbnail: ""
uuid: 18b16a95-4dfa-4a53-b95a-15159fc43a7d
updated: 1484310517
title: bullock
categories:
    - Dictionary
---
bullock
     n 1: young bull
     2: castrated bull [syn: {steer}]
