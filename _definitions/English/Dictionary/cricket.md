---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cricket
offline_file: ""
offline_thumbnail: ""
uuid: e3304428-7177-4673-ad97-dd895e5157fa
updated: 1484310144
title: cricket
categories:
    - Dictionary
---
cricket
     n 1: leaping insect; male makes chirping noises by rubbing the
          forewings together
     2: a game played with a ball and bat by two teams of 11
        players; teams take turns trying to score runs
     v : play cricket
