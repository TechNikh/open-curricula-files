---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hunt
offline_file: ""
offline_thumbnail: ""
uuid: 13c1b3d3-3dfa-4d4a-afc5-eab288b22854
updated: 1484310297
title: hunt
categories:
    - Dictionary
---
Hunt
     n 1: Englishman and Pre-Raphaelite painter (1827-1910) [syn: {Holman
          Hunt}, {William Holman Hunt}]
     2: United States architect (1827-1895) [syn: {Richard Morris
        Hunt}]
     3: British writer who defended the romanticism of Keats and
        Shelley (1784-1859) [syn: {Leigh Hunt}, {James Henry Leigh
        Hunt}]
     4: an association of huntsmen who hunt for sport [syn: {hunt
        club}]
     5: an instance of searching for something; "the hunt for
        submarines"
     6: the activity of looking thoroughly in order to find
        something or someone [syn: {search}, {hunting}]
     7: the work of finding and killing or capturing animals for
        ...
