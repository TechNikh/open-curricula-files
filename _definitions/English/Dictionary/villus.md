---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/villus
offline_file: ""
offline_thumbnail: ""
uuid: 1ec272c5-9028-4d79-ac16-26493f8598d8
updated: 1484310325
title: villus
categories:
    - Dictionary
---
villus
     n : a minute hairlike projection on mucous membrane
     [also: {villi} (pl)]
