---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schanuel%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 3fe10675-68c5-4a38-8f24-77f0e1ea2268
updated: 1484308659
title: "Schanuel's conjecture"
tags:
    - Statement
    - Consequences
    - Related conjectures and results
    - "Zilber's pseudo-exponentiation"
categories:
    - Number theory
---
In mathematics, specifically transcendental number theory, Schanuel's conjecture is a conjecture made by Stephen Schanuel in the 1960s concerning the transcendence degree of certain field extensions of the rational numbers.
