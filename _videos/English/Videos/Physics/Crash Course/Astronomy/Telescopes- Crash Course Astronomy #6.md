---
version: 1
type: video
provider: YouTube
id: mYhy7eaazIk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Telescopes-%20Crash%20Course%20Astronomy%20%236.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1a0cff79-e3cc-4521-ac1c-7237e7087745
updated: 1486069684
title: 'Telescopes- Crash Course Astronomy #6'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/mYhy7eaazIk/default.jpg
    - https://i3.ytimg.com/vi/mYhy7eaazIk/1.jpg
    - https://i3.ytimg.com/vi/mYhy7eaazIk/2.jpg
    - https://i3.ytimg.com/vi/mYhy7eaazIk/3.jpg
---
