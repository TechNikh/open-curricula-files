---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bangle
offline_file: ""
offline_thumbnail: ""
uuid: c9d10134-a85c-4a79-b606-dfc7c22dcb73
updated: 1484310146
title: bangle
categories:
    - Dictionary
---
bangle
     n 1: jewelry worn around the wrist for decoration [syn: {bracelet}]
     2: cheap showy jewelry or ornament on clothing [syn: {bauble},
        {gaud}, {gewgaw}, {novelty}, {fallal}, {trinket}]
