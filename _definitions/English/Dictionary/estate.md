---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/estate
offline_file: ""
offline_thumbnail: ""
uuid: 0f4c5555-70f9-4878-9e4a-4aebd498c5ad
updated: 1484310453
title: estate
categories:
    - Dictionary
---
estate
     n 1: everything you own; all of your assets (whether real
          property or personal property) and liabilities
     2: extensive landed property (especially in the country)
        retained by the owner for his own use; "the family owned a
        large estate on Long Island" [syn: {land}, {landed estate},
         {acres}, {demesne}]
     3: a major social class or order of persons regarded
        collectively as part of the body politic of the country
        and formerly possessing distinct political rights [syn: {estate
        of the realm}]
