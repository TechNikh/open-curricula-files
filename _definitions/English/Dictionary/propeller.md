---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propeller
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412241
title: propeller
categories:
    - Dictionary
---
propeller
     n : a mechanical device that rotates to push against air or
         water [syn: {propellor}]
