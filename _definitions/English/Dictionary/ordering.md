---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ordering
offline_file: ""
offline_thumbnail: ""
uuid: 6379d0fe-640a-4ddd-8818-f2019290a959
updated: 1484310186
title: ordering
categories:
    - Dictionary
---
ordering
     n 1: logical or comprehensible arrangement of separate elements;
          "we shall consider these questions in the inverse order
          of their presentation" [syn: {order}, {ordination}]
     2: putting in order; "there were mistakes in the ordering of
        items on the list" [syn: {order}]
