---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/structured
offline_file: ""
offline_thumbnail: ""
uuid: 02c216da-fe15-4d74-8d6c-c57102bb52a6
updated: 1484310337
title: structured
categories:
    - Dictionary
---
structured
     adj 1: having definite and highly organized structure; "a
            structured environment" [ant: {unstructured}]
     2: resembling a living organism in organization or development;
        "society as an integrated whole" [syn: {integrated}]
