---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tetrahedron
offline_file: ""
offline_thumbnail: ""
uuid: b72138d4-c455-4d71-86e5-5c8e3b7629b3
updated: 1484310407
title: tetrahedron
categories:
    - Dictionary
---
tetrahedron
     n : any polyhedron having four plane faces
     [also: {tetrahedra} (pl)]
