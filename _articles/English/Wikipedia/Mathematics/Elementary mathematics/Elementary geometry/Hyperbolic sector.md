---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_sector
offline_file: ""
offline_thumbnail: ""
uuid: 862ec7fb-54df-4efd-bf36-07a36fe080d5
updated: 1484308932
title: Hyperbolic sector
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Cartesian_hyperbolic_triangle.svg.png
tags:
    - area
    - Hyperbolic triangle
    - Hyperbolic logarithm
    - Hyperbolic geometry
categories:
    - Elementary geometry
---
A hyperbolic sector is a region of the Cartesian plane {(x,y)} bounded by rays from the origin to two points (a, 1/a) and (b, 1/b) and by the rectangular hyperbola xy = 1 (or the corresponding region when this hyperbola is rescaled and its orientation is altered by a rotation leaving the center at the origin, as with the unit hyperbola).
