---
version: 1
type: video
provider: YouTube
id: ItW5CxLsLSo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20ex2%20part%202-%20Evaluating%20integral.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 737d744c-a97c-40cc-ba11-a35e56a6f8f3
updated: 1486069692
title: 'Surface integral ex2 part 2- Evaluating integral'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/ItW5CxLsLSo/default.jpg
    - https://i3.ytimg.com/vi/ItW5CxLsLSo/1.jpg
    - https://i3.ytimg.com/vi/ItW5CxLsLSo/2.jpg
    - https://i3.ytimg.com/vi/ItW5CxLsLSo/3.jpg
---
