---
version: 1
type: video
provider: YouTube
id: txidhOqgySU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 47149be7-a7db-49a3-ac7e-1dbabc7ceef9
updated: 1486069641
title: 'Pitch Black - Freefall - Psychedelic Kaleidoscope Fractal Animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/txidhOqgySU/default.jpg
    - https://i3.ytimg.com/vi/txidhOqgySU/1.jpg
    - https://i3.ytimg.com/vi/txidhOqgySU/2.jpg
    - https://i3.ytimg.com/vi/txidhOqgySU/3.jpg
---
