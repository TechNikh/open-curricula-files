---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyano
offline_file: ""
offline_thumbnail: ""
uuid: 7c1d6350-8657-45cd-8b47-a285f63111ea
updated: 1484308403
title: Cyanide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Cyanide-montage.png
tags:
    - Nomenclature and etymology
    - Occurrence and reactions
    - In nature
    - Interstellar medium
    - Pyrolysis and combustion product
    - Coordination chemistry
    - Organic derivatives
    - Manufacture
    - Toxicity
    - Antidote
    - Sensitivity
    - Applications
    - mining
    - Industrial organic chemistry
    - Medical uses
    - Illegal fishing and poaching
    - Pest control
    - Niche uses
    - Human poisoning
    - Food additive
    - Chemical tests for cyanide
    - Prussian blue
    - para-Benzoquinone in DMSO
    - Copper and an aromatic amine
    - Pyridine-barbituric acid colorimetry
    - Gas diffusion flow injection analysis — amperometry
categories:
    - Uncategorized
---
A cyanide is any chemical compound that contains monovalent combining group CN. This group, known as the cyano group, consists of a carbon atom triple-bonded to a nitrogen atom.[1]
