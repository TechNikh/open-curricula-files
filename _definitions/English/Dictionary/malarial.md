---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malarial
offline_file: ""
offline_thumbnail: ""
uuid: cafe67f6-b187-473b-80c5-b9b62236c740
updated: 1484310160
title: malarial
categories:
    - Dictionary
---
malarial
     adj : of or infected by or resembling malaria; "malarial fever"
