---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Domain_theory
offline_file: ""
offline_thumbnail: ""
uuid: cb32ff18-0e4a-46b3-aaca-09c1d05d631c
updated: 1484308777
title: Domain theory
tags:
    - Motivation and intuition
    - A guide to the formal definitions
    - Directed sets as converging specifications
    - Computations and domains
    - Approximation and finiteness
    - Way-below relation
    - Bases of domains
    - Special types of domains
    - Important results
    - Generalizations
categories:
    - Fixed points (mathematics)
---
Domain theory is a branch of mathematics that studies special kinds of partially ordered sets (posets) commonly called domains. Consequently, domain theory can be considered as a branch of order theory. The field has major applications in computer science, where it is used to specify denotational semantics, especially for functional programming languages. Domain theory formalizes the intuitive ideas of approximation and convergence in a very general way and has close relations to topology. An alternative important approach to denotational semantics in computer science is that of metric spaces.
