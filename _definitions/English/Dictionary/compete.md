---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compete
offline_file: ""
offline_thumbnail: ""
uuid: 02e502a4-2636-4f99-ace0-71472cb7eae2
updated: 1484310286
title: compete
categories:
    - Dictionary
---
compete
     v : compete for something; engage in a contest; measure oneself
         against others [syn: {vie}, {contend}]
