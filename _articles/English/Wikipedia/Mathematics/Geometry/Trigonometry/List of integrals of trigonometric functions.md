---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_integrals_of_trigonometric_functions
offline_file: ""
offline_thumbnail: ""
uuid: b3828b15-b8d4-4718-aa18-4866aea45d00
updated: 1484309347
title: List of integrals of trigonometric functions
tags:
    - Integrands involving only sine
    - Integrands involving only cosine
    - Integrands involving only tangent
    - Integrands involving only secant
    - Integrands involving only cosecant
    - Integrands involving only cotangent
    - Integrands involving both sine and cosine
    - Integrands involving both sine and tangent
    - Integrand involving both cosine and tangent
    - Integrand involving both sine and cotangent
    - Integrand involving both cosine and cotangent
    - Integrand involving both secant and tangent
    - Integrals in a quarter period
    - Integrals with symmetric limits
    - Integral over a full circle
categories:
    - Trigonometry
---
The following is a list of integrals (antiderivative functions) of trigonometric functions. For antiderivatives involving both exponential and trigonometric functions, see List of integrals of exponential functions. For a complete list of antiderivative functions, see Lists of integrals. For the special antiderivatives involving trigonometric functions, see Trigonometric integral.
