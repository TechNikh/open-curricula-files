---
version: 1
type: video
provider: YouTube
id: Nx4Xk5S4HQA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Two%20Passing%20Bicycles%20Word%20Problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9094a550-a442-490d-979a-6edec8067d46
updated: 1486069571
title: Two Passing Bicycles Word Problem
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Nx4Xk5S4HQA/default.jpg
    - https://i3.ytimg.com/vi/Nx4Xk5S4HQA/1.jpg
    - https://i3.ytimg.com/vi/Nx4Xk5S4HQA/2.jpg
    - https://i3.ytimg.com/vi/Nx4Xk5S4HQA/3.jpg
---
Algebra word problem involving two bicycles passing each other.
More free lessons at: http://www.khanacademy.org/video?v=Nx4Xk5S4HQA
