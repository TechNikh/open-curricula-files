---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regulated
offline_file: ""
offline_thumbnail: ""
uuid: 8b0d8722-75fc-4309-87f7-be6925dcd499
updated: 1484310365
title: regulated
categories:
    - Dictionary
---
regulated
     adj 1: controlled or governed according to rule or principle or
            law; "well regulated industries"; "houses with
            regulated temperature" [ant: {unregulated}]
     2: marked by system or regularity or discipline; "a quiet
        ordered house"; "an orderly universe"; "a well regulated
        life" [syn: {ordered}, {orderly}]
