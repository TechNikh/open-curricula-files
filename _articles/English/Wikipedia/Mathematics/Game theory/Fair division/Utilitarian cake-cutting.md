---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Utilitarian_cake-cutting
offline_file: ""
offline_thumbnail: ""
uuid: 4b3ab4c4-2fcb-4a25-8656-f6a938a6a5e7
updated: 1484308520
title: Utilitarian cake-cutting
tags:
    - Example
    - Notation
    - Utilitarianism and Pareto-efficiency
    - Characterization of the utilitarian rule
    - Finding utilitarian-maximal divisions
    - Disconnected pieces
    - Connected pieces
    - Utilitiarianism vs. fairness
categories:
    - Fair division
---
Utilitarian cake-cutting is a rule for dividing a heterogeneous resource, such as a cake or a land-estate, among several partners with different cardinal utility functions. The rule says that the cake should be divided in a way which maximizes the sum of the utilities of the partners. It is called "utilitarian" because it is inspired by utilitarianism. An alternative term is maxsum cake-cutting.
