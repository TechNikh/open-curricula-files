---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phrase
offline_file: ""
offline_thumbnail: ""
uuid: bdd4648c-a17b-4331-9e8c-4a39243ca1b6
updated: 1484310148
title: phrase
categories:
    - Dictionary
---
phrase
     n 1: an expression forming a grammatical constituent of a
          sentence but not containing a finite verb
     2: a short musical passage [syn: {musical phrase}]
     3: an expression whose meanings cannot be inferred from the
        meanings of the words that make it up [syn: {idiom}, {idiomatic
        expression}, {phrasal idiom}, {set phrase}]
     v : put into words or an expression; "He formulated his concerns
         to the board of trustees" [syn: {give voice}, {formulate},
          {word}, {articulate}]
