---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optimistic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413081
title: optimistic
categories:
    - Dictionary
---
optimistic
     adj 1: expecting the best in this best of all possible worlds; "in
            an optimistic mood"; "optimistic plans"; "took an
            optimistic view" [ant: {pessimistic}]
     2: expecting the best; "an affirmative outlook" [syn: {affirmative}]
