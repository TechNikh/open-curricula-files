---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acyclic
offline_file: ""
offline_thumbnail: ""
uuid: fe73a36c-7a84-4e93-b52e-5384d5a9aa89
updated: 1484310419
title: acyclic
categories:
    - Dictionary
---
acyclic
     adj 1: botany; not cyclic; especially having parts arranged in
            spirals rather than whorls [ant: {cyclic}]
     2: chemistry; not cyclic; having an open chain structure [syn:
        {open-chain}] [ant: {cyclic}]
