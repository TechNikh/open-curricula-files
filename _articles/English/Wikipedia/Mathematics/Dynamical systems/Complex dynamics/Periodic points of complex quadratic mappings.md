---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
offline_file: ""
offline_thumbnail: ""
uuid: 750ee687-4da5-4584-9633-10db005c23db
updated: 1484308727
title: Periodic points of complex quadratic mappings
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Multiplier4_f.png
tags:
    - Definitions
    - 'Stability of periodic points (orbit) - multiplier'
    - Period-1 points (fixed points)
    - Finite fixed points
    - Complex dynamics
    - Special cases
    - Only one fixed point
    - Infinite fixed point
    - Period-2 cycles
    - First method of factorization
    - Second method of factorization
    - Special cases
    - Cycles for period greater than 2
categories:
    - Complex dynamics
---
This article describes periodic points of some complex quadratic maps. A map is a formula for computing a value of a variable based on its own previous value or values; a quadratic map is one that involves the previous value raised to the powers one and two; and a complex map is one in which the variable and the parameters are complex numbers. A periodic point of a map is a value of the variable that occurs repeatedly after intervals of a fixed length.
