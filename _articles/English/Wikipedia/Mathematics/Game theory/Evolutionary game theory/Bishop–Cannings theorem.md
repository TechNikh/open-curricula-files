---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bishop%E2%80%93Cannings_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ba6fef19-c55b-4b6d-a96e-724a823fc950
updated: 1484308531
title: Bishop–Cannings theorem
categories:
    - Evolutionary game theory
---
The Bishop–Cannings theorem is a theorem in evolutionary game theory. It states that (i) all members of a mixed evolutionarily stable strategy (ESS) have the same payoff (Theorem 2), and (ii) that none of these can also be a pure ESS[1] (from their Theorem 3). The usefulness of the results comes from the fact that they can be used to directly find ESSes algebraically, rather than simulating the game and solving it by iteration.
