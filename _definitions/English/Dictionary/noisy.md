---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noisy
offline_file: ""
offline_thumbnail: ""
uuid: 34ae39cc-91aa-4b92-87f9-6919874ddb2e
updated: 1484310144
title: noisy
categories:
    - Dictionary
---
noisy
     adj : full of or characterized by loud and nonmusical sounds; "a
           noisy cafeteria"; "a small noisy dog" [ant: {quiet}]
     [also: {noisiest}, {noisier}]
