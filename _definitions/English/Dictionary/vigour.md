---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vigour
offline_file: ""
offline_thumbnail: ""
uuid: c6ec4d8b-c50f-4f9d-894d-404757bcbe76
updated: 1484310409
title: vigour
categories:
    - Dictionary
---
vigour
     n 1: an exertion of force; "he plays tennis with great energy"
          [syn: {energy}, {vigor}]
     2: active strength of body or mind [syn: {vigor}]
     3: an imaginative lively style (especially style of writing);
        "his writing conveys great energy" [syn: {energy}, {vigor},
         {vim}]
