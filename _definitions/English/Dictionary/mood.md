---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mood
offline_file: ""
offline_thumbnail: ""
uuid: dd53bb35-080e-4fea-829e-a74e54941a62
updated: 1484310589
title: mood
categories:
    - Dictionary
---
mood
     n 1: a characteristic (habitual or relatively temporary) state of
          feeling; "whether he praised or cursed me depended on
          his temper at the time"; "he was in a bad humor" [syn: {temper},
           {humor}, {humour}]
     2: the prevailing psychological state; "the climate of
        opinion"; "the national mood had changed radically since
        the last election" [syn: {climate}]
     3: verb inflections that express how the action or state is
        conceived by the speaker [syn: {mode}, {modality}]
