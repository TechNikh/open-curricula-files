---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apology
offline_file: ""
offline_thumbnail: ""
uuid: 064af52e-f869-4279-a48e-a4a7d9f87610
updated: 1484310186
title: apology
categories:
    - Dictionary
---
apology
     n 1: an expression of regret at having caused trouble for
          someone; "he wrote a letter of apology to the hostess"
     2: a formal written defense of something you believe in
        strongly [syn: {apologia}]
     3: a poor example; "it was an apology for a meal"; "a poor
        excuse for an automobile" [syn: {excuse}]
