---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arimaa
offline_file: ""
offline_thumbnail: ""
uuid: 15d64700-e5a5-4499-8d2f-32d5df2f3429
updated: 1484308999
title: Arimaa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Arimaa_egb74.png
tags:
    - Rules
    - Goal
    - Setup
    - Movement
    - Pushing and pulling
    - Freezing
    - Capturing
    - Ways to lose
    - Strategy and tactics
    - Annual tournaments
    - World Championship
    - World Computer Championship
    - Arimaa Challenge
    - Patent and trademark
    - Notes
categories:
    - Game artificial intelligence
---
Arimaa i/əˈriːmə/ (ə-REE-mə) is a two-player strategy board game that was designed to be playable with a standard chess set and difficult for computers while still being easy to learn and fun to play for humans. Every year since 2004, the Arimaa community has held three tournaments: a World Championship (humans only), a Computer Championship (computers only), and the Arimaa Challenge (human vs. computer). In 2015, the challenge was won decisively by the computer (Sharp by David Wu), with top players agreeing that computers had become better at the game than humans.[1] As it was a prerequisite for the prize to be awarded, most of ICGA Journal Issue 38/1 was dedicated to this topic.[2]
