---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stopping
offline_file: ""
offline_thumbnail: ""
uuid: dd431140-5e6f-4a13-bf87-aef3bfe45ff5
updated: 1484310192
title: stopping
categories:
    - Dictionary
---
stop
     n 1: the event of something ending; "it came to a stop at the
          bottom of the hill" [syn: {halt}]
     2: the act of stopping something; "the third baseman made some
        remarkable stops"; "his stoppage of the flow resulted in a
        flood" [syn: {stoppage}]
     3: a brief stay in the course of a journey; "they made a
        stopover to visit their friends" [syn: {stopover}, {layover}]
     4: the state of inactivity following an interruption; "the
        negotiations were in arrest"; "held them in check";
        "during the halt he got some lunch"; "the momentary stay
        enabled him to escape the blow"; "he spent the entire stop
        in his seat" [syn: ...
