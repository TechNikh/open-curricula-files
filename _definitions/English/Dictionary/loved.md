---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loved
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406181
title: loved
categories:
    - Dictionary
---
loved
     adj : held dear; "his loved companion of many years" [ant: {unloved}]
