---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/downstream
offline_file: ""
offline_thumbnail: ""
uuid: d04f58af-ad25-4822-9097-3983a06acc13
updated: 1484310462
title: downstream
categories:
    - Dictionary
---
downstream
     adj : in the direction of a stream's current [ant: {upstream}]
     adv : away from the source or with the current [syn: {downriver}]
           [ant: {upriver}, {upriver}]
