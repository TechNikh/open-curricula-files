---
version: 1
type: video
provider: YouTube
id: 2cHb0Pp9zzY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f5dfb463-5bd2-4b5f-882b-434d9504bee7
updated: 1486069660
title: 'Dry Ice Mini Smoke Ring Launcher - Cool Science Experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/2cHb0Pp9zzY/default.jpg
    - https://i3.ytimg.com/vi/2cHb0Pp9zzY/1.jpg
    - https://i3.ytimg.com/vi/2cHb0Pp9zzY/2.jpg
    - https://i3.ytimg.com/vi/2cHb0Pp9zzY/3.jpg
---
