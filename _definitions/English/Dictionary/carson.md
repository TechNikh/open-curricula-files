---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carson
offline_file: ""
offline_thumbnail: ""
uuid: 4c0ce924-e7f4-4ebd-9a2b-cf1676875646
updated: 1484310541
title: carson
categories:
    - Dictionary
---
Carson
     n 1: United States biologist remembered for her opposition to the
          use of pesticides that were hazardous to wildlife
          (1907-1964) [syn: {Rachel Carson}, {Rachel Louise Carson}]
     2: United States frontiersman who guided Fremont's expeditions
        in the 1840s and served as a Union general in the Civil
        War (1809-1868) [syn: {Kit Carson}, {Christopher Carson}]
