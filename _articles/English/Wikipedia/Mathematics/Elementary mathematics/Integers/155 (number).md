---
version: 1
type: article
id: https://en.wikipedia.org/wiki/155_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 20e5e35f-3d5e-45ec-b6a2-5ea256621fd9
updated: 1484308983
title: 155 (number)
tags:
    - In the military
    - In sports
    - In transportation
    - In other fields
categories:
    - Integers
---
If you add up all the primes from the least through the greatest prime factors of 155, that is, 5 and 31, the result is 155. (sequence A055233 in the OEIS) Only three other small semiprimes (10, 39, and 371) share this attribute.
