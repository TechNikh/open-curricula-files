---
version: 1
type: video
provider: YouTube
id: ZGl2ExHwdak
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20Basics/Adding%20and%20subtracting%20polynomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6532bbbd-03c0-4fd6-86a1-ef8b225e78af
updated: 1486069563
title: Adding and subtracting polynomials
categories:
    - Algebra Basics
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZGl2ExHwdak/default.jpg
    - https://i3.ytimg.com/vi/ZGl2ExHwdak/1.jpg
    - https://i3.ytimg.com/vi/ZGl2ExHwdak/2.jpg
    - https://i3.ytimg.com/vi/ZGl2ExHwdak/3.jpg
---
