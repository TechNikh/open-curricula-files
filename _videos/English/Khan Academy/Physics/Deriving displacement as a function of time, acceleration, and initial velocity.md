---
version: 1
type: video
provider: YouTube
id: wlB0x9W-qBU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Deriving%20displacement%20as%20a%20function%20of%20time%2C%20acceleration%2C%20and%20initial%20velocity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 16a78601-9f6a-497d-8b0d-672c7fb5b33b
updated: 1486069597
title: >
    Deriving displacement as a function of time, acceleration,
    and initial velocity
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/wlB0x9W-qBU/default.jpg
    - https://i3.ytimg.com/vi/wlB0x9W-qBU/1.jpg
    - https://i3.ytimg.com/vi/wlB0x9W-qBU/2.jpg
    - https://i3.ytimg.com/vi/wlB0x9W-qBU/3.jpg
---
Deriving displacement as a function of time, constant acceleration and initial velocity. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/v/plotting-projectile-displacement-acceleration-and-velocity?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/v/acceleration-of-aircraft-carrier-takeoff?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
