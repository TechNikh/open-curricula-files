---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ed_Scheinerman
offline_file: ""
offline_thumbnail: ""
uuid: 1f64585b-9cb9-497a-afa1-b419972d7bd9
updated: 1484309476
title: Ed Scheinerman
categories:
    - Graph theorists
---
Edward R. Scheinerman is an American mathematician, specializing in graph theory and order theory. He is a professor of applied mathematics, statistics, and computer science at Johns Hopkins University.[1] His contributions to mathematics include Scheinerman's conjecture, now proven, stating that every planar graph may be represented as an intersection graph of line segments.[2]
