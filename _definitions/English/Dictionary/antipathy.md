---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antipathy
offline_file: ""
offline_thumbnail: ""
uuid: 9fae2f2d-1d36-4cc3-89a6-0f3c27a7bc3a
updated: 1484310591
title: antipathy
categories:
    - Dictionary
---
antipathy
     n 1: a feeling of intense dislike [syn: {aversion}, {distaste}]
     2: the object of a feeling of intense aversion; something to be
        avoided; "cats were his greatest antipathy"
