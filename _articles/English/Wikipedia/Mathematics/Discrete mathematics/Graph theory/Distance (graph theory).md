---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 073f605f-ef75-40f5-9d8d-5408a9c46123
updated: 1484308608
title: Distance (graph theory)
tags:
    - Related concepts
    - Algorithm for finding pseudo-peripheral vertices
    - Notes
categories:
    - Graph theory
---
In the mathematical field of graph theory, the distance between two vertices in a graph is the number of edges in a shortest path (also called a graph geodesic) connecting them. This is also known as the geodesic distance.[1] Notice that there may be more than one shortest path between two vertices.[2] If there is no path connecting the two vertices, i.e., if they belong to different connected components, then conventionally the distance is defined as infinite.
