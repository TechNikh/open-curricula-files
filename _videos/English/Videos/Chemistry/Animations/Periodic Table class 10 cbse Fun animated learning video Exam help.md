---
version: 1
type: video
provider: YouTube
id: up8OXkK1HOU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Periodic%20Table%20class%2010%20cbse%20Fun%20animated%20learning%20video%20Exam%20help.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ebc378df-5023-4750-a439-0a470a1e65e6
updated: 1486069664
title: >
    Periodic Table class 10 cbse Fun animated learning video
    Exam help
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/up8OXkK1HOU/default.jpg
    - https://i3.ytimg.com/vi/up8OXkK1HOU/1.jpg
    - https://i3.ytimg.com/vi/up8OXkK1HOU/2.jpg
    - https://i3.ytimg.com/vi/up8OXkK1HOU/3.jpg
---
