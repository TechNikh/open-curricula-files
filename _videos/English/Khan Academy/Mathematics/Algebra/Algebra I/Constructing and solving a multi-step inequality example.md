---
version: 1
type: video
provider: YouTube
id: cCMpin3Te4s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Constructing%20and%20solving%20a%20multi-step%20inequality%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 561d2f02-4e7e-42e9-8293-d308ea8018a0
updated: 1486069559
title: Constructing and solving a multi-step inequality example
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/cCMpin3Te4s/default.jpg
    - https://i3.ytimg.com/vi/cCMpin3Te4s/1.jpg
    - https://i3.ytimg.com/vi/cCMpin3Te4s/2.jpg
    - https://i3.ytimg.com/vi/cCMpin3Te4s/3.jpg
---
