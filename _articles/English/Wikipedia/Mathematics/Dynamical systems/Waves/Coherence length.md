---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coherence_length
offline_file: ""
offline_thumbnail: ""
uuid: fb8b51c6-036e-4e17-82a9-3c90fe0e9c23
updated: 1484308889
title: Coherence length
tags:
    - Formulas
    - Lasers
categories:
    - Waves
---
In physics, coherence length is the propagation distance over which a coherent wave (e.g. an electromagnetic wave) maintains a specified degree of coherence. Wave interference is strong when the paths taken by all of the interfering waves differ by less than the coherence length. A wave with a longer coherence length is closer to a perfect sinusoidal wave. Coherence length is important in holography and telecommunications engineering.
