---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Alfr%C3%A9d_R%C3%A9nyi'
offline_file: ""
offline_thumbnail: ""
uuid: 1b94b048-2f4e-4209-a0fb-2615cbc76b7c
updated: 1484309473
title: Alfréd Rényi
tags:
    - life
    - Work
    - Quotations
    - Remembrance
    - Books
categories:
    - Graph theorists
---
Alfréd Rényi (20 March 1921 – 1 February 1970) was a Hungarian mathematician who made contributions in combinatorics, graph theory, number theory but mostly in probability theory.[2][3]
