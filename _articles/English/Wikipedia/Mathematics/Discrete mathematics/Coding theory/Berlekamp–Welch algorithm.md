---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Berlekamp%E2%80%93Welch_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: cdc6b9be-99fe-41f5-bcee-d6f067a1df78
updated: 1484308538
title: Berlekamp–Welch algorithm
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Recovering_P%2528x%2529_with_an_Error_Locator_Polynomial.png'
tags:
    - History on decoding Reed–Solomon codes
    - Error locator polynomial of Reed–Solomon codes
    - The Berlekamp–Welch decoder and algorithm
    - Example
categories:
    - Coding theory
---
The Berlekamp–Welch algorithm, also known as the Welch–Berlekamp algorithm, is named for Elwyn R. Berlekamp and Lloyd R. Welch. The algorithm efficiently corrects errors in BCH codes and Reed–Solomon codes (which are a subset of BCH codes). Unlike many other decoding algorithms, and in correspondence with the code-domain Berlekamp–Massey algorithm that uses syndrome decoding and the dual of the codes, the Berlekamp–Welch decoding algorithm provides a method for decoding Reed–Solomon codes using just the generator matrix and not syndromes.
