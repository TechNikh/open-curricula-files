---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immensely
offline_file: ""
offline_thumbnail: ""
uuid: eb5f4659-d188-40cb-b2bd-447455bcd94c
updated: 1484310549
title: immensely
categories:
    - Dictionary
---
immensely
     adv : to an exceedingly great extent or degree; "He had vastly
           overestimated his resources"; "was immensely more
           important to the project as a scientist than as an
           administrator" [syn: {vastly}]
