---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monitoring
offline_file: ""
offline_thumbnail: ""
uuid: 1cc535fc-8f34-4ffa-8bbc-1b64304b18b7
updated: 1484310537
title: monitoring
categories:
    - Dictionary
---
monitoring
     n : the act of observing something (and sometimes keeping a
         record of it); "the monitoring of enemy communications
         plays an important role in war times"
