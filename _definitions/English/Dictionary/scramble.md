---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scramble
offline_file: ""
offline_thumbnail: ""
uuid: 34da1d03-ff24-4b87-a299-3590b158d313
updated: 1484310551
title: scramble
categories:
    - Dictionary
---
scramble
     n 1: an unceremonious and disorganized struggle [syn: {scuffle}]
     2: rushing about hastily in an undignified way [syn: {scamper},
         {scurry}]
     v 1: to move hurriedly; "The friend scrambled after them"
     2: climb awkwardly, as if by scrambling [syn: {clamber}, {shin},
         {shinny}, {skin}, {struggle}, {sputter}]
     3: bring into random order [syn: {jumble}, {throw together}]
     4: stir vigorously; "beat the egg whites"; "beat the cream"
        [syn: {beat}]
     5: make unintelligible; "scramble the message so that nobody
        can understand it" [ant: {unscramble}]
