---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/employment
offline_file: ""
offline_thumbnail: ""
uuid: 69c95c71-5e69-49b7-8bcc-e935a5caf2a3
updated: 1484310441
title: employment
categories:
    - Dictionary
---
employment
     n 1: the state of being employed or having a job; "they are
          looking for employment"; "he was in the employ of the
          city" [syn: {employ}] [ant: {unemployment}]
     2: the occupation for which you are paid; "he is looking for
        employment"; "a lot of people are out of work" [syn: {work}]
     3: the act of giving someone a job [syn: {engagement}]
     4: the act of using; "he warned against the use of narcotic
        drugs"; "skilled in the utilization of computers" [syn: {use},
         {usage}, {utilization}, {utilisation}, {exercise}]
