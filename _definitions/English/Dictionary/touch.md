---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/touch
offline_file: ""
offline_thumbnail: ""
uuid: b28d5700-cb61-4213-9d41-8ca57f537651
updated: 1484310349
title: touch
categories:
    - Dictionary
---
touch
     n 1: the event of something coming in contact with the body; "he
          longed for the touch of her hand"; "the cooling touch of
          the night air" [syn: {touching}]
     2: the faculty of touch; "only sight and touch enable us to
        locate objects in the space around us" [syn: {sense of
        touch}, {skin senses}, {touch modality}, {cutaneous senses}]
     3: a suggestion of some quality; "there was a touch of sarcasm
        in his tone"; "he detected a ghost of a smile on her face"
        [syn: {trace}, {ghost}]
     4: a distinguishing style; "this room needs a woman's touch"
        [syn: {signature}]
     5: the act of putting two things together with no ...
