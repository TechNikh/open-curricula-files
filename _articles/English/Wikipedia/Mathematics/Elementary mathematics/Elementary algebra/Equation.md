---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equation
offline_file: ""
offline_thumbnail: ""
uuid: 30d0ceae-60ce-46cb-9e21-e053b7854fcf
updated: 1484308912
title: Equation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/First_Equation_Ever.png
tags:
    - Introduction
    - Analogous illustration
    - Parameters and unknowns
    - Identities
    - Properties
    - Algebra
    - Polynomial equations
    - Systems of linear equations
    - Geometry
    - Analytic geometry
    - Cartesian equations
    - Parametric equations
    - Number theory
    - Diophantine equations
    - Algebraic and transcendental numbers
    - Algebraic geometry
    - Differential equations
    - Ordinary differential equations
    - Partial differential equations
    - Types of equations
categories:
    - Elementary algebra
---
In mathematics, an equation is a statement of an equality containing one or more variables. Solving the equation consists of determining which values of the variables make the equality true. Variables are also called unknowns and the values of the unknowns which satisfy the equality are called solutions of the equation. There are two kinds of equations: identity equations and conditional equations. An identity equation is true for all values of the variable. A conditional equation is true for only particular values of the variables.[1][2]
