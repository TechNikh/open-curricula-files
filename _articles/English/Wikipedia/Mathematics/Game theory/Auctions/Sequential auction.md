---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequential_auction
offline_file: ""
offline_thumbnail: ""
uuid: 664ce4f7-95c2-4a50-97e8-ac58e2c7cbfd
updated: 1484309020
title: Sequential auction
tags:
    - Nash equilibrium
    - Social welfare
    - Revenue maximization
    - Composeable mechanisms
categories:
    - Auctions
---
A sequential auction is an auction– in which several items are sold, one after the other, to the same group of potential buyers. In a sequential first-price auction (SAFP), the auction in each step is a first price auction, while in a sequential second-price auction (SASP), the auction in each step is a second price auction.
