---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/volatility
offline_file: ""
offline_thumbnail: ""
uuid: deda206d-ca90-4af1-a65b-4344ece4c050
updated: 1484310148
title: volatility
categories:
    - Dictionary
---
volatility
     n 1: the property of changing readily from a solid or liquid to a
          vapor
     2: the trait of being unpredictably irresolute; "the volatility
        of the market drove many investors away" [syn: {unpredictability}]
     3: being easily excited [syn: {excitability}, {excitableness}]
