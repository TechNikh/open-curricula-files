---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disquisitiones_Arithmeticae
offline_file: ""
offline_thumbnail: ""
uuid: 9b095eb4-245d-4d46-b9fd-16d4db861861
updated: 1484308635
title: Disquisitiones Arithmeticae
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Disqvisitiones-800.jpg
tags:
    - Scope
    - Contents
    - Importance
    - Notes
categories:
    - Number theory
---
The Disquisitiones Arithmeticae (Latin for "Arithmetical Investigations") is a textbook of number theory written in Latin[1] by Carl Friedrich Gauss in 1798 when Gauss was 21 and first published in 1801 when he was 24. In this book Gauss brings together results in number theory obtained by mathematicians such as Fermat, Euler, Lagrange and Legendre and adds important new results of his own.
