---
version: 1
type: video
provider: YouTube
id: Z9IQA5v66HU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8196f0df-e177-4ace-a106-04d0ab4b744c
updated: 1486069609
title: Fractions and unit conversion word problem
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z9IQA5v66HU/default.jpg
    - https://i3.ytimg.com/vi/Z9IQA5v66HU/1.jpg
    - https://i3.ytimg.com/vi/Z9IQA5v66HU/2.jpg
    - https://i3.ytimg.com/vi/Z9IQA5v66HU/3.jpg
---
