---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Worm%27s-eye_view'
offline_file: ""
offline_thumbnail: ""
uuid: 3e624cdb-d005-4aeb-9424-3c7828290397
updated: 1484309336
title: "Worm's-eye view"
categories:
    - Technical drawing
---
A worm's-eye view is a view of an object from below, as though the observer were a worm; the opposite of a bird's-eye view. A worm's eye view is used commonly for third perspective, with one vanishing point on top, one on the left, and one on the right.
