---
version: 1
type: video
provider: YouTube
id: vGq3x31ex1Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5195ff84-b42e-49c3-98a2-f4cccba4e466
updated: 1486069649
title: 'Fractal Universe - Vortex - Spiral Galaxy - 2013'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vGq3x31ex1Y/default.jpg
    - https://i3.ytimg.com/vi/vGq3x31ex1Y/1.jpg
    - https://i3.ytimg.com/vi/vGq3x31ex1Y/2.jpg
    - https://i3.ytimg.com/vi/vGq3x31ex1Y/3.jpg
---
