---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_topologies_on_the_category_of_schemes
offline_file: ""
offline_thumbnail: ""
uuid: f4db9317-0761-4fcd-8159-f01bae87e803
updated: 1484309511
title: List of topologies on the category of schemes
categories:
    - Algebraic geometry
---
The most fundamental item of study in modern algebraic geometry is the category of schemes. This category admits many different Grothendieck topologies, each of which is well-suited for a different purpose. This is a list of some of the topologies on the category of schemes.
