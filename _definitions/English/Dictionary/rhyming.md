---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rhyming
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494321
title: rhyming
categories:
    - Dictionary
---
rhyming
     adj : having corrnesponding sounds especially terminal sounds;
           "rhymed verse"; "rhyming words" [syn: {rhymed}, {riming}]
           [ant: {unrhymed}]
