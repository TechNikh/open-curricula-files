---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blister
offline_file: ""
offline_thumbnail: ""
uuid: 54ce966f-38ad-414b-a0ad-97d88cf3121b
updated: 1484310411
title: blister
categories:
    - Dictionary
---
blister
     n : (pathology) an elevation of the skin filled with serous
         fluid [syn: {bulla}, {bleb}]
     v 1: get blistered; "Her feet blistered during the long hike"
          [syn: {vesicate}]
     2: subject to harsh criticism; "The Senator blistered the
        administration in his speech on Friday"; "the professor
        scaled the students"; "your invectives scorched the
        community" [syn: {scald}, {whip}]
     3: cause blisters to from on; "the tight shoes and perspiration
        blistered her feet"
