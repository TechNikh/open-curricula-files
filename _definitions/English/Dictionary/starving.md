---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starving
offline_file: ""
offline_thumbnail: ""
uuid: 715ff1d6-a79e-4060-8665-d44bbc7dad7b
updated: 1484310290
title: starving
categories:
    - Dictionary
---
starving
     adj : suffering from lack of food [syn: {starved}]
     n : the act of depriving of food or subjecting to famine; "the
         beseigers used starvation to induce surrender"; "they
         were charged with the starvation of children in their
         care" [syn: {starvation}]
