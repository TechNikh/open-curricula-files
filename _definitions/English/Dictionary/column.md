---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/column
offline_file: ""
offline_thumbnail: ""
uuid: 83726b52-4f9f-4ba1-a362-e81bb59d0f3e
updated: 1484310232
title: column
categories:
    - Dictionary
---
column
     n 1: a line of (usually military) units following one after
          another
     2: a vertical glass tube used in column chromatography; a
        mixture is poured in the top and washed through a
        stationary substance where components of the mixture are
        adsorbed selectively to form colored bands [syn: {chromatography
        column}]
     3: a linear array of numbers one above another
     4: anything tall and thin approximating the shape of a column
        or tower; "the test tube held a column of white powder";
        "a tower of dust rose above the horizon"; "a thin pillar
        of smoke betrayed their campsite" [syn: {tower}, {pillar}]
     5: an ...
