---
version: 1
type: article
id: https://en.wikipedia.org/wiki/22_(number)
offline_file: ""
offline_thumbnail: ""
uuid: cf38a3d9-5b04-454b-a400-a2ad68da5dca
updated: 1484308946
title: 22 (number)
tags:
    - In mathematics
    - In physics and chemistry
    - In aircraft
    - In art, entertainment, and media
    - In music
    - In other fields
    - In computing and technology
    - In religion
    - In sports
    - In American football
    - In Australian rules football
    - In cricket
    - In jerseys
    - In motorsports
    - In rugby union
    - In soccer
    - In weights and measures
    - In other uses
categories:
    - Integers
---
