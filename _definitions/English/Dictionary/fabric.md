---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fabric
offline_file: ""
offline_thumbnail: ""
uuid: 675f045f-e640-4df2-a554-c09b584f80f6
updated: 1484310387
title: fabric
categories:
    - Dictionary
---
fabric
     n 1: artifact made by weaving or felting or knitting or
          crocheting natural or synthetic fibers; "the fabric in
          the curtains was light and semitraqnsparent"; "woven
          cloth originated in Mesopotamia around 5000 BC"; "she
          measured off enough material for a dress" [syn: {cloth},
           {material}, {textile}]
     2: the underlying structure; "restoring the framework of the
        bombed building"; "it is part of the fabric of society"
        [syn: {framework}]
