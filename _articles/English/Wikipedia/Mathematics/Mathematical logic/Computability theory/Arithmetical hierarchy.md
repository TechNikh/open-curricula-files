---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetical_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 777201d6-686c-488c-86c1-68eb0132b20e
updated: 1484309483
title: Arithmetical hierarchy
tags:
    - The arithmetical hierarchy of formulas
    - The arithmetical hierarchy of sets of natural numbers
    - Relativized arithmetical hierarchies
    - Arithmetic reducibility and degrees
    - >
        The arithmetical hierarchy of subsets of Cantor and Baire
        space
    - Extensions and variations
    - Meaning of the notation
    - Examples
    - Properties
    - Relation to Turing machines
    - Computable sets
    - Summary of main results
    - Relation to other hierarchies
categories:
    - Computability theory
---
In mathematical logic, the arithmetical hierarchy, arithmetic hierarchy or Kleene–Mostowski hierarchy classifies certain sets based on the complexity of formulas that define them. Any set that receives a classification is called arithmetical.
