---
version: 1
type: video
provider: YouTube
id: _aIr3ps2qEw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 753e228f-1249-49ef-ba0b-a0dec43eb7ff
updated: 1486069619
title: Examples algebraically analyzing solutions to systems
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/_aIr3ps2qEw/default.jpg
    - https://i3.ytimg.com/vi/_aIr3ps2qEw/1.jpg
    - https://i3.ytimg.com/vi/_aIr3ps2qEw/2.jpg
    - https://i3.ytimg.com/vi/_aIr3ps2qEw/3.jpg
---
