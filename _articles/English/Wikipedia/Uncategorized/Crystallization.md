---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crystalisation
offline_file: ""
offline_thumbnail: ""
uuid: cf3a89eb-2816-4e2f-a4c8-3e56de3f6e44
updated: 1484308309
title: Crystallization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Snow_crystallization_in_Akureyri_2005-02-26_19-03-37.jpeg
tags:
    - Process
    - In nature
    - Methods
    - Typical equipment
    - Thermodynamic view
    - Dynamics
    - Nucleation
    - Primary nucleation
    - Secondary nucleation
    - growth
    - Size distribution
    - Main crystallization processes
    - Cooling crystallization
    - Application
    - Cooling crystallizers
    - Evaporative crystallization
    - Evaporative crystallizers
    - DTB crystallizer
categories:
    - Uncategorized
---
Crystallization is the (natural or artificial) process where a solid forms where the atoms or molecules are highly organized in a structure known as a crystal. Some of the ways which crystals form are through precipitating from a solution, melting or more rarely deposition directly from a gas. Crystallization is also a chemical solid–liquid separation technique, in which mass transfer of a solute from the liquid solution to a pure solid crystalline phase occurs. In chemical engineering crystallization occurs in a crystallizer. Crystallization is therefore related to precipitation, although the result is not amorphous or disordered, but a crystal.
