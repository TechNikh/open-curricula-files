---
version: 1
type: video
provider: YouTube
id: Xrqj88zQZJg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Einstein%27s%20Relativistic%20Train%20in%20a%20Tunnel%20Paradox-%20Special%20Relativity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 663b42ec-18a3-4d5b-9991-2e89c59ef248
updated: 1486069657
title: "Einstein's Relativistic Train in a Tunnel Paradox- Special Relativity"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xrqj88zQZJg/default.jpg
    - https://i3.ytimg.com/vi/Xrqj88zQZJg/1.jpg
    - https://i3.ytimg.com/vi/Xrqj88zQZJg/2.jpg
    - https://i3.ytimg.com/vi/Xrqj88zQZJg/3.jpg
---
