---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selenium
offline_file: ""
offline_thumbnail: ""
uuid: c7fc622c-20ef-4dc6-9e52-9c781f262c57
updated: 1484310397
title: selenium
categories:
    - Dictionary
---
selenium
     n : a toxic nonmetallic element related to sulfur and tellurium;
         occurs in several allotropic forms; a stable gray
         metallike allotrope conducts electricity better in the
         light than in the dark and is used in photocells; occurs
         in sulfide ores (as pyrite) [syn: {Se}, {atomic number 34}]
