---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Constructible_sheaf
offline_file: ""
offline_thumbnail: ""
uuid: 9af194a9-82a3-4189-9b1b-cb8139ba9183
updated: 1484309138
title: Constructible sheaf
categories:
    - Algebraic geometry
---
In mathematics, a constructible sheaf is a sheaf of abelian groups over some topological space X, such that X is the union of a finite number of locally closed subsets on each of which the sheaf is a locally constant sheaf. It is a generalization of constructible topology in classical algebraic geometry.
