---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Logic_of_graphs
offline_file: ""
offline_thumbnail: ""
uuid: 93c0095f-69c3-4e34-9d41-c205010d141f
updated: 1484308636
title: Logic of graphs
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/287px-Logic_portal.svg.png
tags:
    - First order
    - Examples
    - Axioms
    - Zero-one law
    - Parameterized complexity
    - Data compression and graph isomorphism
    - Satisfiability
    - Second order
    - Examples
    - "Courcelle's theorem"
    - "Seese's theorem"
    - Notes
categories:
    - Graph theory
---
In the mathematical fields of graph theory and finite model theory, the logic of graphs deals with formal specifications of graph properties using logical formulas. There are several variations in the types of logical operation that can be used in these formulas. The first order logic of graphs concerns formulas in which the variables and predicates concern individual vertices and edges of a graph, while monadic second order graph logic allows quantification over sets of vertices or edges.
