---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/black
offline_file: ""
offline_thumbnail: ""
uuid: 6e2245fb-f721-43ee-b233-7d2b19717973
updated: 1484310337
title: black
categories:
    - Dictionary
---
black
     adj 1: being of the achromatic color of maximum darkness; having
            little or no hue owing to absorption of almost all
            incident light; "black leather jackets"; "as black as
            coal"; "rich black soil" [syn: {achromatic}] [ant: {white}]
     2: of or belonging to a racial group having dark skin
        especially of sub-Saharan African origin; "a great
        people--a black people--...injected new meaning and
        dignity into the veins of civilization"- Martin Luther
        King Jr. [ant: {white}]
     3: marked by anger or resentment or hostility; "black looks";
        "black words"
     4: stemming from evil characteristics or forces; wicked ...
