---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/typesetting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317501
title: typesetting
categories:
    - Dictionary
---
typeset
     v : set in type; "My book will be typeset nicely"
     [also: {typesetting}]
