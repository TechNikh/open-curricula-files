---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jure_Zupan
offline_file: ""
offline_thumbnail: ""
uuid: 81d57ed8-5300-46e1-bd6e-b4e91f9cc5f9
updated: 1484309434
title: Jure Zupan
tags:
    - life
    - Work
    - Selected publications
    - Political career
categories:
    - Mathematical chemistry
---
Jure Zupan is a Slovenian physicist and founder of chemomectrics research in Slovenia, known for his work in applications and development of artificial neural networks in chemistry.
