---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenolphthalein
offline_file: ""
offline_thumbnail: ""
uuid: 82a9a73f-9b28-4b79-83c3-218f2eec37ac
updated: 1484310379
title: phenolphthalein
categories:
    - Dictionary
---
phenolphthalein
     n : a laxative used in many preparations under various trade
         names; also used as an acid-base indicator in titrations
         involving weak acids and strong bases because it is
         brilliant red at high alkalinity and colorless below pH 8
