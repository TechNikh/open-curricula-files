---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appealing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608621
title: appealing
categories:
    - Dictionary
---
appealing
     adj 1: able to attract interest or draw favorable attention; "He
            added an appealing and memorable figure to popular
            American mythology"- Vincent Starrett; "an appealing
            sense of humor"; "the idea of having enough money to
            retire at fifty is very appealing" [ant: {unappealing}]
     2: (of characters in literature or drama) evoking empathic or
        sympathetic feelings; "the sympathetic characters in the
        play" [syn: {sympathetic}, {likeable}, {likable}] [ant: {unsympathetic}]
     3: expressing earnest entreaty; "the appealing and frightened
        look worn by an injured dog"; "she holds out her hand for
        ...
