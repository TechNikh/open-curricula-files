---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deflect
offline_file: ""
offline_thumbnail: ""
uuid: 73b018b3-0a73-4a91-8ecb-a90d4273110d
updated: 1484310194
title: deflect
categories:
    - Dictionary
---
deflect
     v 1: prevent the occurrence of; prevent from happening; "Let's
          avoid a confrontation"; "head off a confrontation";
          "avert a strike" [syn: {debar}, {obviate}, {avert}, {head
          off}, {stave off}, {fend off}, {avoid}, {ward off}]
     2: turn from a straight course , fixed direction, or line of
        interest [syn: {bend}, {turn away}]
     3: turn aside [syn: {deviate}]
     4: draw someone's attention away from something; "The thief
        distracted the bystanders"; "He deflected his competitors"
        [syn: {distract}]
     5: impede the movement of (an opponent or a ball); "block an
        attack" [syn: {parry}, {block}]
