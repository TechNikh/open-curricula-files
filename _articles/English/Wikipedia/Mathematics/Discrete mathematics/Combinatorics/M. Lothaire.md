---
version: 1
type: article
id: https://en.wikipedia.org/wiki/M._Lothaire
offline_file: ""
offline_thumbnail: ""
uuid: d92e426a-736c-4c38-b478-243de767c16c
updated: 1484308583
title: M. Lothaire
tags:
    - Publications
categories:
    - Combinatorics
---
M. Lothaire is the pseudonym of a group of mathematicians, many of whom were students of Marcel-Paul Schützenberger. The name is used as the author of several of their joint books about combinatorics on words. He is named for Lothair I.[1]
