---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Well-separated_pair_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: ce8e2ede-4ba2-49ef-80f1-46f689b0f438
updated: 1484309176
title: Well-separated pair decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Visual_representation_of_well-separated_pair.svg.png
tags:
    - Definition
    - Construction
    - Split tree
    - WSPD computation
    - Properties
    - Applications
categories:
    - Computational geometry
---
In computational geometry, a well-separated pair decomposition (WSPD) of a set of points 
  
    
      
        S
        ⊂
        
          
            R
          
          
            d
          
        
      
    
    {\displaystyle S\subset \mathbb {R} ^{d}}
  
, is a sequence of pairs of sets 
  
    
      
        (
        
          A
          
            i
          
        
        ,
        
          B
          
            i
          
        
        )
      
    
    {\displaystyle (A_{i},B_{i})}
  
, such that each pair is well-separated, and for each two distinct points 
  
    
      
        p
        ,
        q
        ∈
        S
      
    
    ...
