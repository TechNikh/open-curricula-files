---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minamata
offline_file: ""
offline_thumbnail: ""
uuid: b14ec2d1-e84e-4247-9a9f-5016e7a67218
updated: 1484308337
title: Minamata, Kumamoto
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Population_of_Minamata.jpg
tags:
    - Minamata environmental disaster
    - History
    - Legacy
    - Minamata Eco-Town
    - Activities for environmentally friendly living
    - Garbage collection
    - "Women's Liaison Meeting for Reducing Waste"
    - Promoting ISO
    - Eco-Shop Certification system
    - Development of environmentally friendly industries
    - Development of a nature-oriented ecological town
    - Development of The City for Environmental Learning
    - Sister cities
    - Notable people
categories:
    - Uncategorized
---
Minamata (水俣市, Minamata-shi?) is a city located in Kumamoto Prefecture, Japan. It is on the west coast of Kyūshū and faces Amakusa islands. Minamata was established as a village in 1889, re-designated as a town in 1912 and grew into a city in 1949.[1]
