---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scope
offline_file: ""
offline_thumbnail: ""
uuid: 575fc7d5-ba68-4919-835a-b4e6473b9d43
updated: 1484310517
title: scope
categories:
    - Dictionary
---
scope
     n 1: an area in which something acts or operates or has power or
          control: "the range of a supersonic jet"; "the ambit of
          municipal legislation"; "within the compass of this
          article"; "within the scope of an investigation";
          "outside the reach of the law"; "in the political orbit
          of a world power" [syn: {range}, {reach}, {orbit}, {compass},
           {ambit}]
     2: the state of the environment in which a situation exists;
        "you can't do that in a university setting" [syn: {setting},
         {background}]
     3: a magnifier of images of distant objects [syn: {telescope}]
     4: electronic equipment that provides visual ...
