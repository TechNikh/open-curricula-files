---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bauxite
offline_file: ""
offline_thumbnail: ""
uuid: 1b476160-14cf-455e-907d-ad692208dbd5
updated: 1484310407
title: bauxite
categories:
    - Dictionary
---
bauxite
     n : a clay-like mineral; the chief ore of aluminum; composed of
         aluminum oxides and aluminum hydroxides; used as an
         abrasive and catalyst
