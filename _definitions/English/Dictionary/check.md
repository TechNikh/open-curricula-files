---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/check
offline_file: ""
offline_thumbnail: ""
uuid: 739fb2de-f261-4e42-ac68-3b7bbf768c64
updated: 1484310224
title: check
categories:
    - Dictionary
---
check
     n 1: a written order directing a bank to pay money; "he paid all
          his bills by check" [syn: {bank check}, {cheque}]
     2: an appraisal of the state of affairs; "they made an assay of
        the contents"; "a check on its dependability under stress"
        [syn: {assay}]
     3: the bill in a restaurant; "he asked the waiter for the
        check" [syn: {chit}, {tab}]
     4: the state of inactivity following an interruption; "the
        negotiations were in arrest"; "held them in check";
        "during the halt he got some lunch"; "the momentary stay
        enabled him to escape the blow"; "he spent the entire stop
        in his seat" [syn: {arrest}, {halt}, ...
