---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ane
offline_file: ""
offline_thumbnail: ""
uuid: f33491e2-22b2-43b6-a6f8-c4923b91ec58
updated: 1484310421
title: ane
categories:
    - Dictionary
---
ane
     adj : used of a single unit or thing; not two or more; "`ane' is
           Scottish" [syn: {one}, {1}, {i}]
