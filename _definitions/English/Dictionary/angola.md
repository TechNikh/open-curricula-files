---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/angola
offline_file: ""
offline_thumbnail: ""
uuid: 5b75ad71-0a50-4eb3-b808-788041f0867f
updated: 1484310180
title: angola
categories:
    - Dictionary
---
Angola
     n : a republic in southwestern Africa on the Atlantic Ocean;
         achieved independence from Portugal in 1975 and was the
         scene of civil war until 1990 [syn: {Republic of Angola}]
