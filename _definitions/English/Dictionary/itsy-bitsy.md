---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/itsy-bitsy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495461
title: itsy-bitsy
categories:
    - Dictionary
---
itsy-bitsy
     adj : (used informally) very small; "a wee tot" [syn: {bitty}, {bittie},
            {teensy}, {teentsy}, {teeny}, {wee}, {weeny}, {weensy},
            {teensy-weensy}, {teeny-weeny}, {itty-bitty}]
