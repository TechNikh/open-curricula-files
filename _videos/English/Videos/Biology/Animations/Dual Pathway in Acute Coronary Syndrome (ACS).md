---
version: 1
type: video
provider: YouTube
id: 6WdBW6cUjaQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 85528f20-5c8a-4a15-a8a2-24d59c8a9a8e
updated: 1486069670
title: Dual Pathway in Acute Coronary Syndrome (ACS)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6WdBW6cUjaQ/default.jpg
    - https://i3.ytimg.com/vi/6WdBW6cUjaQ/1.jpg
    - https://i3.ytimg.com/vi/6WdBW6cUjaQ/2.jpg
    - https://i3.ytimg.com/vi/6WdBW6cUjaQ/3.jpg
---
