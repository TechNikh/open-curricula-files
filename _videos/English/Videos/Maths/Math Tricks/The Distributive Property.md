---
version: 1
type: video
provider: YouTube
id: c0x4Kh8czVA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ed5f0163-070a-4686-918c-0aa883c2fa06
updated: 1486069625
title: The Distributive Property
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/c0x4Kh8czVA/default.jpg
    - https://i3.ytimg.com/vi/c0x4Kh8czVA/1.jpg
    - https://i3.ytimg.com/vi/c0x4Kh8czVA/2.jpg
    - https://i3.ytimg.com/vi/c0x4Kh8czVA/3.jpg
---
