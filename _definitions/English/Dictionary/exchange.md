---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exchange
offline_file: ""
offline_thumbnail: ""
uuid: 9f6d35a2-29c2-4150-a400-9aca3c591f0e
updated: 1484310375
title: exchange
categories:
    - Dictionary
---
exchange
     n 1: chemical process in which one atom or ion or group changes
          places with another
     2: a mutual expression of views (especially an unpleasant one);
        "they had a bitter exchange"
     3: the act of changing one thing for another thing; "Adam was
        promised immortality in exchange for his disobedience";
        "there was an exchange of prisoners"
     4: the act of giving something in return for something
        received; "deductible losses on sales or exchanges of
        property are allowable"
     5: a workplace that serves as a telecommunications facility
        where lines from telephones can be connected together to
        permit ...
