---
version: 1
type: video
provider: YouTube
id: dmcevC55K3s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83170250-90df-47f6-ad1c-4f97e0e0a013
updated: 1486069665
title: 'All of the energy in the universe is... - George Zaidan and Charles Morton'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dmcevC55K3s/default.jpg
    - https://i3.ytimg.com/vi/dmcevC55K3s/1.jpg
    - https://i3.ytimg.com/vi/dmcevC55K3s/2.jpg
    - https://i3.ytimg.com/vi/dmcevC55K3s/3.jpg
---
