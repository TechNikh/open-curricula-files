---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: 89960d0e-36d7-40fe-9f91-51999e98f8e5
updated: 1484308605
title: Graph theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-6n-graf.svg.png
tags:
    - Definitions
    - Graph
    - Applications
    - History
    - Graph drawing
    - Graph-theoretic data structures
    - Problems in graph theory
    - Enumeration
    - Subgraphs, induced subgraphs, and minors
    - Graph coloring
    - Subsumption and unification
    - Route problems
    - Network flow
    - Visibility problems
    - Covering problems
    - Decomposition problems
    - Graph classes
    - Related topics
    - Algorithms
    - Subareas
    - Related areas of mathematics
    - Generalizations
    - Prominent graph theorists
    - Notes
    - Online textbooks
categories:
    - Graph theory
---
In mathematics graph theory is the study of graphs, which are mathematical structures used to model pairwise relations between objects. A graph in this context is made up of vertices, nodes, or points which are connected by edges, arcs, or lines. A graph may be undirected, meaning that there is no distinction between the two vertices associated with each edge, or its edges may be directed from one vertex to another; see Graph (discrete mathematics) for more detailed definitions and for other variations in the types of graph that are commonly considered. Graphs are one of the prime objects of study in discrete mathematics.
