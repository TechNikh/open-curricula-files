---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/migrant
offline_file: ""
offline_thumbnail: ""
uuid: 3c1ae016-f302-4d31-b2b1-de3e0f20a018
updated: 1484310477
title: migrant
categories:
    - Dictionary
---
migrant
     adj : habitually moving from place to place especially in search
           of seasonal work; "appalled by the social conditions of
           migrant life"; "migratory workers" [syn: {migratory}]
     n : traveler who moves from one region or country to another
         [syn: {migrator}]
