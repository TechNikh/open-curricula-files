---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engrave
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484502241
title: engrave
categories:
    - Dictionary
---
engrave
     v 1: carve, cut, or etch into a material or surface; "engrave a
          pen"; "engraved the winner's name onto the trophy cup"
          [syn: {grave}, {inscribe}]
     2: impress or affect deeply; "The event engraved itself into
        her memory"
     3: carve, cut, or etch into a block used for printing or print
        from such a block; "engrave a letter"
     4: carve, cut, or etch a design or letters into; "engrave the
        pen with the owner's name"
