---
version: 1
type: video
provider: YouTube
id: UNZ171fDja4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Refraction%20of%20seismic%20waves.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d14913a1-3a44-4d63-b753-8c6f66843a82
updated: 1486069686
title: Refraction of seismic waves
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/UNZ171fDja4/default.jpg
    - https://i3.ytimg.com/vi/UNZ171fDja4/1.jpg
    - https://i3.ytimg.com/vi/UNZ171fDja4/2.jpg
    - https://i3.ytimg.com/vi/UNZ171fDja4/3.jpg
---
