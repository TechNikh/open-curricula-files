---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pressing
offline_file: ""
offline_thumbnail: ""
uuid: 1da62c61-b6f1-476f-b447-09413b8330a8
updated: 1484310346
title: pressing
categories:
    - Dictionary
---
pressing
     adj : compelling immediate action; "too pressing to permit of
           longer delay"; "the urgent words `Hurry! Hurry!'";
           "bridges in urgent need of repair" [syn: {urgent}]
     n 1: the act of pressing; the exertion of pressure; "he gave the
          button a press"; "he used pressure to stop the
          bleeding"; "at the pressing of a button" [syn: {press},
          {pressure}]
     2: a metal or plastic part that is made by a mechanical press
