---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gangotri
offline_file: ""
offline_thumbnail: ""
uuid: 5123992d-a412-405e-9948-52877290b9a4
updated: 1484308432
title: Gangotri
tags:
    - geography
    - Gangotri Temple
    - Historical relation
    - Submerged Shivlingam
    - Demographics
categories:
    - Uncategorized
---
Gangotri (Hindi: गंगोत्री) is a town and a Nagar Panchayat (municipality) in Uttarkashi district in the state of Uttarakhand, India. It is a Hindu pilgrim town on the banks of the river Bhagirathi and origin of River Ganges. It is on the Greater Himalayan Range, at a height of 3,100 metres (10,200 ft). According to popular Hindu legend, it was here that Goddess Ganga descended when Lord Shiva released the mighty river from the locks of his hair.
