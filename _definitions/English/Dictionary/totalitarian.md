---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/totalitarian
offline_file: ""
offline_thumbnail: ""
uuid: c9978d7e-f029-4042-9d07-f901c453127f
updated: 1484310599
title: totalitarian
categories:
    - Dictionary
---
totalitarian
     adj 1: characterized by a government in which the political
            authority exercises absolute and centralized control;
            "a totalitarian regime crushes all autonomous
            institutions in its drive to seize the human soul"-
            Arthur M.Schlesinger, Jr.
     2: of or relating to the principles of totalitarianism
        according to which the state regulates every realm of
        life; "totalitarian theory and practice"; "operating in a
        totalistic fashion" [syn: {totalistic}]
