---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/keeping
offline_file: ""
offline_thumbnail: ""
uuid: 4969a415-3658-461b-b8e3-469c2b6c2a72
updated: 1484310316
title: keeping
categories:
    - Dictionary
---
keeping
     n 1: conformity or harmony; "his behavior was not in keeping with
          the occasion"
     2: the responsibility of a guardian or keeper; "he left his car
        in my keeping" [syn: {guardianship}, {safekeeping}]
     3: the act of keeping in your possession [syn: {retention}, {holding}]
