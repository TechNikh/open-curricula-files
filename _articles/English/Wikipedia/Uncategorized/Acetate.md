---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ethanoate
offline_file: ""
offline_thumbnail: ""
uuid: 862ea9bc-f3f9-4e14-a4df-f4d32215ec00
updated: 1484308405
title: Acetate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Acetate-anion-canonical-form-2D-skeletal.png
tags:
    - Nomenclature and common formula
    - Salts
    - Esters
    - Acetate in biology
    - Fermentation of acetate
    - Structures
categories:
    - Uncategorized
---
An acetate /ˈæsᵻteɪt/ is a salt formed by the combination of acetic acid with an alkaline, earthy, or metallic base. "Acetate" also describes the conjugate base or ion (specifically, the negatively charged ion called an anion) typically found in aqueous solution and written with the chemical formula C2H3O2−. The neutral molecules formed by the combination of the acetate ion and a positive ion (called a cation) are also commonly called "acetates" (hence, acetate of lead, acetate of aluminum, etc.). The simplest of these is hydrogen acetate (called acetic acid) with corresponding salts, esters, and the polyatomic anion CH3CO2−, or CH3COO−.
