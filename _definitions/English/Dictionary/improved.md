---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/improved
offline_file: ""
offline_thumbnail: ""
uuid: 0b43ace1-cdfa-4cd3-b3fc-a56a590d4df7
updated: 1484310397
title: improved
categories:
    - Dictionary
---
improved
     adj 1: made more desirable or valuable or profitable; especially
            made ready for use or marketing; "new houses are
            springing up on an improved tract of land near the
            river"; "an improved breed" [ant: {unimproved}]
     2: become or made better in quality; "was proud of his improved
        grades"; "an improved viewfinder"
     3: (of land) made ready for development or agriculture by
        clearing of trees and brush; "improved farmlands"
