---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maze_generation_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: ecaa336b-7c34-4fe1-a59e-a3e1d4793366
updated: 1484309450
title: Maze generation algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Prim_Maze.svg.png
tags:
    - Graph theory based methods
    - Depth-first search
    - Recursive backtracker
    - "Randomized Kruskal's algorithm"
    - "Randomized Prim's algorithm"
    - Modified version
    - Recursive division method
    - Simple algorithms
    - Cellular automaton algorithms
    - Python code example
categories:
    - Random graphs
---
