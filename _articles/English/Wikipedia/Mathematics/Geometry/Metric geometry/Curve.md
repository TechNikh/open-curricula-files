---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curve
offline_file: ""
offline_thumbnail: ""
uuid: 0d8f53e9-887d-47a4-a9b1-76b41f24636b
updated: 1484309262
title: Curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parabola.svg.png
tags:
    - History
    - Definition
    - Differentiable curve
    - Length of a curve
    - Differential geometry
    - Algebraic curve
    - Notes
categories:
    - Metric geometry
---
In mathematics, a curve (also called a curved line in older texts) is, generally speaking, an object similar to a line but that need not be straight. Thus, a curve is a generalization of a line, in that curvature is not necessarily zero.[a]
