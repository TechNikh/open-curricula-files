---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parabola
offline_file: ""
offline_thumbnail: ""
uuid: a8a02005-56d1-4eef-9fcd-e6aafad1d7d0
updated: 1484310138
title: parabola
categories:
    - Dictionary
---
parabola
     n : a plane curve formed by the intersection of a right circular
         cone and a plane parallel to an element of the curve
