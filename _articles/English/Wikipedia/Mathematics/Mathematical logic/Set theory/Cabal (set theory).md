---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cabal_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 4119d7ab-82d8-4551-ad54-cb69010d10c6
updated: 1484309501
title: Cabal (set theory)
categories:
    - Set theory
---
The Cabal was, or perhaps is, a grouping of set theorists in Southern California, particularly at UCLA and Caltech, but also at UC Irvine. Organization and procedures range from informal to nonexistent, so it is difficult to say whether it still exists or exactly who has been a member, but it has included such notable figures as Donald A. Martin, Yiannis N. Moschovakis, John R. Steel, and Alexander S. Kechris. Others who have published in the proceedings of the Cabal seminar include Robert M. Solovay, W. Hugh Woodin, Matthew Foreman, and Steve Jackson.
