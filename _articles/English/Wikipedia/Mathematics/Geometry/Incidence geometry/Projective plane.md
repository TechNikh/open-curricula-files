---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Projective_plane
offline_file: ""
offline_thumbnail: ""
uuid: e75ee51e-0a7e-495d-a6c2-cddba4614d90
updated: 1484309255
title: Projective plane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Railroad-Tracks-Perspective.jpg
tags:
    - Definition
    - Some examples
    - The extended Euclidean plane
    - Projective Moulton Plane
    - A finite example
    - Vector space construction
    - Classical examples
    - Finite field planes
    - "Desargues' theorem and Desarguesian planes"
    - Subplanes
    - Fano subplanes
    - Affine planes
    - Definition
    - Construction of projective planes from affine planes
    - Generalized coordinates
    - Degenerate planes
    - Collineations
    - Homography
    - Plane duality
    - Correlations
    - Finite projective planes
    - Projective planes in higher-dimensional projective spaces
    - Notes
categories:
    - Incidence geometry
---
In mathematics, a projective plane is a geometric structure that extends the concept of a plane. In the ordinary Euclidean plane, two lines typically intersect in a single point, but there are some pairs of lines (namely, parallel lines) that do not intersect. A projective plane can be thought of as an ordinary plane equipped with additional "points at infinity" where parallel lines intersect. Thus any two lines in a projective plane intersect in one and only one point.
