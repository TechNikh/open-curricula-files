---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optic
offline_file: ""
offline_thumbnail: ""
uuid: f0bfccfd-695c-46fd-bcae-fb32ea404e00
updated: 1484310315
title: optic
categories:
    - Dictionary
---
optic
     adj 1: of or relating to or resembling the eye; "ocular muscles";
            "an ocular organ"; "ocular diseases"; "the optic (or
            optical) axis of the eye"; "an ocular spot is a
            pigmented organ or part believed to be sensitive to
            light" [syn: {ocular}, {optical}, {opthalmic}]
     2: relating to or using sight; "ocular inspection"; "an optical
        illusion"; "visual powers"; "visual navigation" [syn: {ocular},
         {optical}, {visual}]
     n : the organ of sight [syn: {eye}, {oculus}]
