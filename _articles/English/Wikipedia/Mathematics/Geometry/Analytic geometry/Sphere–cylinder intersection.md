---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sphere%E2%80%93cylinder_intersection'
offline_file: ""
offline_thumbnail: ""
uuid: cbf87dcf-391d-4647-9cd9-4eb3b6ffa799
updated: 1484309167
title: Sphere–cylinder intersection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Viviani_curve.png
tags:
    - Trivial cases
    - Sphere lies entirely within cylinder
    - Sphere touches cylinder in one point
    - Sphere centered on cylinder axis
    - Non-trivial cases
    - Intersection consists of two closed curves
    - Intersection is a single closed curve
    - Limiting case
categories:
    - Analytic geometry
---
In the theory of analytic geometry for real three-dimensional space, the intersection between a sphere and a cylinder can be a circle, a point, the empty set, or a special type of curve.
