---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/halfway
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484644501
title: halfway
categories:
    - Dictionary
---
halfway
     adj 1: equally distant from the extremes [syn: {center(a)}, {middle(a)},
             {midway}]
     2: at a point midway between two extremes; "at the halfway
        mark"
     3: including only half or a portion; "halfway measures"
     adv : at half the distance; at the middle; "he was halfway down
           the ladder when he fell" [syn: {midway}]
     [also: {halfways}]
