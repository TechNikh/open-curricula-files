---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steering
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484510341
title: steering
categories:
    - Dictionary
---
steering
     n 1: the act of guiding or showing the way [syn: {guidance}]
     2: the act of setting and holding a course; "a new council was
        installed under the direction of the king" [syn: {guidance},
         {direction}]
     3: the act of steering a ship [syn: {steerage}]
