---
version: 1
type: video
provider: YouTube
id: 7kWXMqA66oY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 950e92ac-9aa7-441d-a615-cf23aafd435b
updated: 1486069609
title: Halves and fourths
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/7kWXMqA66oY/default.jpg
    - https://i3.ytimg.com/vi/7kWXMqA66oY/1.jpg
    - https://i3.ytimg.com/vi/7kWXMqA66oY/2.jpg
    - https://i3.ytimg.com/vi/7kWXMqA66oY/3.jpg
---
