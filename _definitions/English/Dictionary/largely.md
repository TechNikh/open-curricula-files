---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/largely
offline_file: ""
offline_thumbnail: ""
uuid: 5d6f8e58-2317-4e54-b9e0-6abb65796dd0
updated: 1484310273
title: largely
categories:
    - Dictionary
---
largely
     adv 1: in large part; mainly or chiefly; "These accounts are
            largely inactive" [syn: {mostly}, {for the most part}]
     2: on a large scale; "the sketch was so largely drawn that you
        could see it from the back row"
