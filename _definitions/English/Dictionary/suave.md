---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suave
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416081
title: suave
categories:
    - Dictionary
---
suave
     adj 1: having a sophisticated charm; "a debonair gentleman" [syn: {debonair},
             {debonaire}, {debonnaire}]
     2: smoothly agreeable and courteous with a degree of
        sophistication; "he was too politic to quarrel with so
        important a personage"; "the hostess averted a
        confrontation between two guests with a diplomatic change
        of subject"; "the manager pacified the customer with a
        smooth apology for the error"; "affable, suave, moderate
        men...smugly convinced of their respectability" - Ezra
        Pound [syn: {politic}, {smooth}]
