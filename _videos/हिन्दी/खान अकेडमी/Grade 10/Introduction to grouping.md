---
version: 1
type: video
provider: YouTube
id: Ag3DCW4WH3A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b11261c0-7884-47a2-b8e4-14f73bcfdb73
updated: 1486069619
title: Introduction to grouping
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ag3DCW4WH3A/default.jpg
    - https://i3.ytimg.com/vi/Ag3DCW4WH3A/1.jpg
    - https://i3.ytimg.com/vi/Ag3DCW4WH3A/2.jpg
    - https://i3.ytimg.com/vi/Ag3DCW4WH3A/3.jpg
---
