---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Eigenstate_thermalization_hypothesis
offline_file: ""
offline_thumbnail: ""
uuid: 1448974a-0344-4374-9a41-5edfc49f21a4
updated: 1484308843
title: Eigenstate thermalization hypothesis
tags:
    - Statement of the ETH
    - Motivation
    - Equivalence of the Diagonal and Microcanonical Ensembles
    - Tests of the Eigenstate Thermalization Hypothesis
    - Alternatives to Eigenstate Thermalization
    - Temporal Fluctuations of Expectation Values
    - Quantum Fluctuations and Thermal Fluctuations
    - General Validity of the ETH
categories:
    - Thermodynamics
---
The Eigenstate Thermalization Hypothesis (or ETH) is a set of ideas which purports to explain when and why an isolated quantum mechanical system can be accurately described using equilibrium statistical mechanics. In particular, it is devoted to understanding how systems which are initially prepared in far-from-equilibrium states can evolve in time to a state which appears to be in thermal equilibrium. The phrase "eigenstate thermalization" was first coined by Mark Srednicki in 1994,[1] after similar ideas had been introduced by Josh Deutsch in 1991.[2] The principal philosophy underlying the eigenstate thermalization hypothesis is that instead of explaining the ergodicity of a ...
