---
version: 1
type: video
provider: YouTube
id: 9PmWIrgkjGg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 820a1f60-6396-4a55-ba0e-28e36a8bee95
updated: 1486069636
title: Optics Experiment Number 1 (By Amy)
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/9PmWIrgkjGg/default.jpg
    - https://i3.ytimg.com/vi/9PmWIrgkjGg/1.jpg
    - https://i3.ytimg.com/vi/9PmWIrgkjGg/2.jpg
    - https://i3.ytimg.com/vi/9PmWIrgkjGg/3.jpg
---
