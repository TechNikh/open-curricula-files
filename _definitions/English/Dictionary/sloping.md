---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sloping
offline_file: ""
offline_thumbnail: ""
uuid: 6fb0131e-24e3-4f53-9d92-a31235e88f0f
updated: 1484310479
title: sloping
categories:
    - Dictionary
---
sloping
     adj 1: having an oblique or slanted direction [syn: {aslant}, {aslope},
             {diagonal}, {slanted}, {slanting}, {sloped}]
     2: having a slanting form or direction; "an area of gently
        sloping hills"; "a room with a sloping ceiling"
