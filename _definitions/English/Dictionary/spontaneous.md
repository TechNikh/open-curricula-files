---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spontaneous
offline_file: ""
offline_thumbnail: ""
uuid: 984c7130-a63d-4559-b073-811b81b6a1c3
updated: 1484310559
title: spontaneous
categories:
    - Dictionary
---
spontaneous
     adj 1: happening or arising without apparent external cause;
            "spontaneous laughter"; "spontaneous combustion"; "a
            spontaneous abortion" [syn: {self-generated}] [ant: {induced}]
     2: said or done without having been planned or written in
        advance; "he made a few ad-lib remarks" [syn: {ad-lib}, {unwritten}]
     3: produced without being planted or without human labor; "wild
        strawberries" [syn: {wild}]
