---
version: 1
type: video
provider: YouTube
id: wejE9j-TeZE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8fa59c40-9153-4c3a-b10b-770a0477610a
updated: 1486069670
title: 'Cas9- As a Transcriptional Activator'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/wejE9j-TeZE/default.jpg
    - https://i3.ytimg.com/vi/wejE9j-TeZE/1.jpg
    - https://i3.ytimg.com/vi/wejE9j-TeZE/2.jpg
    - https://i3.ytimg.com/vi/wejE9j-TeZE/3.jpg
---
