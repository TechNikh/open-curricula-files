---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engagement
offline_file: ""
offline_thumbnail: ""
uuid: 0b658cfe-c07b-4e5b-a3e0-80aba573918d
updated: 1484310601
title: engagement
categories:
    - Dictionary
---
engagement
     n 1: a hostile meeting of opposing military forces in the course
          of a war; "Grant won a decisive victory in the battle of
          Chickamauga"; "he lost his romantic ideas about war when
          he got into a real engagement" [syn: {battle}, {conflict},
           {fight}]
     2: a meeting arranged in advance; "she asked how to avoid
        kissing at the end of a date" [syn: {date}, {appointment}]
     3: a mutual promise to marry [syn: {betrothal}, {troth}]
     4: the act of giving someone a job [syn: {employment}]
     5: employment for performers or performing groups that lasts
        for a limited period of time; "the play had bookings
        ...
