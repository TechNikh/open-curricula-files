---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/essentially
offline_file: ""
offline_thumbnail: ""
uuid: 021f8425-603f-4092-9408-e85c2a10d791
updated: 1484310221
title: essentially
categories:
    - Dictionary
---
essentially
     adv : at bottom or by one's (or its) very nature; "He is basically
           dishonest"; "the argument was essentially a technical
           one"; "for all his bluster he is in essence a shy
           person" [syn: {basically}, {fundamentally}, {in essence},
            {au fond}]
