---
version: 1
type: video
provider: YouTube
id: X0BdP2Tk6ok
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9cd1054-0495-45e8-b247-edb92f27960b
updated: 1486069630
title: 'Learn Hindi Grammar - Kaal (काल) -Tenses'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/X0BdP2Tk6ok/default.jpg
    - https://i3.ytimg.com/vi/X0BdP2Tk6ok/1.jpg
    - https://i3.ytimg.com/vi/X0BdP2Tk6ok/2.jpg
    - https://i3.ytimg.com/vi/X0BdP2Tk6ok/3.jpg
---
