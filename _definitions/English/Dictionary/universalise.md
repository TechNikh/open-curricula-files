---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/universalise
offline_file: ""
offline_thumbnail: ""
uuid: 808d92d3-b509-4515-857b-22085d686a4b
updated: 1484310559
title: universalise
categories:
    - Dictionary
---
universalise
     v : make universal; "This author's stories unversalize old
         themes" [syn: {universalize}]
