---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ante_Graovac
offline_file: ""
offline_thumbnail: ""
uuid: 4e0c246b-126b-47f3-b42d-10f7351c3cdf
updated: 1484309431
title: Ante Graovac
categories:
    - Mathematical chemistry
---
Ante Graovac[1] is a Croatian scientist (born July 15, 1945 in Split, died November 13, 2012 in Zagreb) known for his contribution to chemical graph theory. He was director of 26 successful annual meetings MATH/CHEM/COMP held in Dubrovnik.[2] He was Secretary of the International Academy of Mathematical Chemistry.
