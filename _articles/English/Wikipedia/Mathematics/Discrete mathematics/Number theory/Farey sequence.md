---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Farey_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 39f20f9e-d2f8-488d-bad8-fa75167f1eb6
updated: 1484308641
title: Farey sequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Farey_diagram_square_9.png
tags:
    - Examples
    - History
    - Properties
    - Sequence length and index of a fraction
    - Farey neighbours
    - Applications
    - Ford circles
    - Riemann hypothesis
    - Next term
categories:
    - Number theory
---
In mathematics, the Farey sequence of order n is the sequence of completely reduced fractions between 0 and 1 which when in lowest terms have denominators less than or equal to n, arranged in order of increasing size.
