---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saponification
offline_file: ""
offline_thumbnail: ""
uuid: 25ad0c3d-5324-4eff-9894-d98cdb58d227
updated: 1484308405
title: Saponification
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Detail_of_Madame_X_%2528Madame_Pierre_Gautreau%2529%252C_John_Singer_Sargent%252C_1884.jpg'
tags:
    - Triglycerides
    - Mechanism of base hydrolysis
    - Steam hydrolysis
    - Applications
    - Soft versus hard soap
    - Lithium soaps
    - Fire extinguishers
    - Oil paints
categories:
    - Uncategorized
---
