---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Categorical_logic
offline_file: ""
offline_thumbnail: ""
uuid: 41095c5b-d039-48f8-a452-4344afadd389
updated: 1484308669
title: Categorical logic
categories:
    - Theoretical computer science
---
Categorical logic is a branch of category theory within mathematics, adjacent to mathematical logic but more notable for its connections to theoretical computer science. In broad terms, categorical logic represents both syntax and semantics by a category, and an interpretation by a functor. The categorical framework provides a rich conceptual background for logical and type-theoretic constructions. The subject has been recognisable in these terms since around 1970.
