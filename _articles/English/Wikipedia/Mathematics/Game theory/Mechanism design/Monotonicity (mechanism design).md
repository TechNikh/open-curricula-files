---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Monotonicity_(mechanism_design)
offline_file: ""
offline_thumbnail: ""
uuid: 280e74a2-66a5-4ac9-ba9f-0d11486efa09
updated: 1484309046
title: Monotonicity (mechanism design)
tags:
    - Notation
    - In mechanisms without money
    - Necessity
    - In mechanisms with money
    - Necessity
    - Sufficiency
    - Examples
categories:
    - Mechanism design
---
In mechanism design, monotonicity is a property of a social choice function. It is a necessary condition for being able to implement the function using a strategyproof mechanism. Its verbal description is:[1]
