---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vindictive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484457061
title: vindictive
categories:
    - Dictionary
---
vindictive
     adj 1: disposed to seek revenge or intended for revenge; "more
            vindictive than jealous love"- Shakespeare;
            "punishments...essentially vindictive in their
            nature"- M.R.Cohen [syn: {revengeful}, {vengeful}]
     2: showing malicious ill will and a desire to hurt; motivated
        by spite; "a despiteful fiend"; "a truly spiteful child";
        "a vindictive man will look for occasions for resentment"
        [syn: {despiteful}, {spiteful}]
