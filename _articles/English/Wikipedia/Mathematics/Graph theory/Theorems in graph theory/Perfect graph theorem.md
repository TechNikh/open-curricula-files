---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfect_graph_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 5455e9ad-9ae9-4a91-94ac-2b7c73d0df43
updated: 1484309457
title: Perfect graph theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-7-hole_and_antihole.svg.png
tags:
    - Theorem statement
    - Example
    - Applications
    - "Lovász's proof"
    - Relation to the strong perfect graph theorem
    - Generalizations
    - Notes
categories:
    - Theorems in graph theory
---
In graph theory, the perfect graph theorem of László Lovász (1972a, 1972b) states that an undirected graph is perfect if and only if its complement graph is also perfect. This result had been conjectured by Berge (1961, 1963), and it is sometimes called the weak perfect graph theorem to distinguish it from the strong perfect graph theorem[1] characterizing perfect graphs by their forbidden induced subgraphs.
