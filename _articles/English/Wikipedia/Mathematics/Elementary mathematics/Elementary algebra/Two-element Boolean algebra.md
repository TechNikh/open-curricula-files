---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-element_Boolean_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 676669e6-d55f-49af-b0e4-dcbec48a34df
updated: 1484308913
title: Two-element Boolean algebra
tags:
    - Definition
    - Some basic identities
    - Metatheory
    - Footnotes
categories:
    - Elementary algebra
---
In mathematics and abstract algebra, the two-element Boolean algebra is the Boolean algebra whose underlying set (or universe or carrier) B is the Boolean domain. The elements of the Boolean domain are 1 and 0 by convention, so that B = {0, 1}. Paul Halmos's name for this algebra "2" has some following in the literature, and will be employed here.
