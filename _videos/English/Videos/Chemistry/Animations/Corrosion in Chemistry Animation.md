---
version: 1
type: video
provider: YouTube
id: 7oqW2VgSayM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0667b9da-7cf6-4924-a979-48520e87c8d9
updated: 1486069662
title: Corrosion in Chemistry Animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/7oqW2VgSayM/default.jpg
    - https://i3.ytimg.com/vi/7oqW2VgSayM/1.jpg
    - https://i3.ytimg.com/vi/7oqW2VgSayM/2.jpg
    - https://i3.ytimg.com/vi/7oqW2VgSayM/3.jpg
---
