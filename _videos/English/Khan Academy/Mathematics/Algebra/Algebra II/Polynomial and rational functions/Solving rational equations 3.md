---
version: 1
type: video
provider: YouTube
id: 5wUJLMWZ5Fw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Solving%20rational%20equations%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dc085899-e633-4e39-b0e9-021b1b90f9cc
updated: 1486069565
title: Solving rational equations 3
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/5wUJLMWZ5Fw/default.jpg
    - https://i3.ytimg.com/vi/5wUJLMWZ5Fw/1.jpg
    - https://i3.ytimg.com/vi/5wUJLMWZ5Fw/2.jpg
    - https://i3.ytimg.com/vi/5wUJLMWZ5Fw/3.jpg
---
