---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexander_Brudno
offline_file: ""
offline_thumbnail: ""
uuid: bbc33e13-dfc4-46e5-9d07-1381e63df765
updated: 1484308997
title: Alexander Brudno
tags:
    - Biography
    - Brudno – Kronrod seminar
    - Early alpha-beta pruning
    - Notes
categories:
    - Game artificial intelligence
---
Alexander L'vovich Brudno (Russian: Александр Львович Брудно) (January 10, 1918 – December 1, 2009)[1] was a Russian Jewish computer scientist, best known for fully describing the alpha-beta pruning algorithm.[2] From 1991 until his death he lived in Israel.
