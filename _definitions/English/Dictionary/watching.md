---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watching
offline_file: ""
offline_thumbnail: ""
uuid: 3eb639ad-6878-43d2-ad28-464299c2b3b3
updated: 1484310146
title: watching
categories:
    - Dictionary
---
watching
     n : the act of observing; taking a patient look [syn: {observation},
          {observance}]
