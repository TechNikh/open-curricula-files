---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bruce_Kleiner
offline_file: ""
offline_thumbnail: ""
uuid: 1d854a27-bf91-4aab-a8b1-deb69e11f4e4
updated: 1484309088
title: Bruce Kleiner
categories:
    - Geometers
---
He received his Ph.D. in 1990 from the University of California, Berkeley. His advisor was Wu-Yi Hsiang. He is now Professor of Mathematics at New York University.
