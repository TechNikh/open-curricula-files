---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shilling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484510041
title: shilling
categories:
    - Dictionary
---
shilling
     n 1: the basic unit of money in Uganda; equal to 100 cents [syn:
          {Ugandan shilling}]
     2: the basic unit of money in Tanzania; equal to 100 cents
        [syn: {Tanzanian shilling}]
     3: the basic unit of money in Somalia; equal to 100 cents [syn:
         {Somalian shilling}]
     4: the basic unit of money in Kenya; equal to 100 cents [syn: {Kenyan
        shilling}]
     5: a former monetary unit in Great Britain [syn: {British
        shilling}, {bob}]
     6: an English coin worth one twentieth of a pound
