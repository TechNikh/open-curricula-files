---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hilbert_metric
offline_file: ""
offline_thumbnail: ""
uuid: 0c024f18-7204-4d67-84f0-e63ae86767d4
updated: 1484309268
title: Hilbert metric
tags:
    - Definition
    - Examples
    - Motivation and applications
categories:
    - Metric geometry
---
In mathematics, the Hilbert metric, also known as the Hilbert projective metric, is an explicitly defined distance function on a bounded convex subset of the n-dimensional Euclidean space Rn. It was introduced by David Hilbert (1895) as a generalization of the Cayley's formula for the distance in the Cayley–Klein model of hyperbolic geometry, where the convex set is the n-dimensional open unit ball. Hilbert's metric has been applied to Perron–Frobenius theory and to constructing Gromov hyperbolic spaces.
