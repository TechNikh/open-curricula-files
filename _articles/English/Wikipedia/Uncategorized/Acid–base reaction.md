---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acid-base
offline_file: ""
offline_thumbnail: ""
uuid: 884ba5b8-4f89-480c-9bf9-34b6d858a66c
updated: 1484308312
title: Acid–base reaction
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Johannes_Br%25C3%25B8nsted.jpg'
tags:
    - Acid–base definitions
    - Historic development
    - "Lavoisier's oxygen theory of acids"
    - "Liebig's hydrogen theory of acids"
    - Arrhenius definition
    - Brønsted–Lowry definition
    - Lewis definition
    - Solvent system definition
    - Lux–Flood definition
    - Usanovich definition
    - Pearson definition
    - Acid–base equilibrium
    - Acid–alkali reaction
    - Notes
categories:
    - Uncategorized
---
An acid–base reaction is a chemical reaction that occurs between an acid and a base. Several theoretical frameworks provide alternative conceptions of the reaction mechanisms and their application in solving related problems; these are called acid–base theories, for example, Brønsted–Lowry acid–base theory. Their importance becomes apparent in analyzing acid–base reactions for gaseous or liquid species, or when acid or base character may be somewhat less apparent. The first of these concepts was provided by the French chemist Antoine Lavoisier, around 1776.[1]
