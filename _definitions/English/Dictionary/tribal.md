---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tribal
offline_file: ""
offline_thumbnail: ""
uuid: a27ea4c5-d761-42ab-a08b-0d06f3fcc694
updated: 1484310515
title: tribal
categories:
    - Dictionary
---
tribal
     adj : relating to or characteristic of a tribe; "tribal customs"
