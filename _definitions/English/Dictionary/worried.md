---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worried
offline_file: ""
offline_thumbnail: ""
uuid: 061ca0cd-271e-4624-b417-eeda51dbf2a7
updated: 1484310473
title: worried
categories:
    - Dictionary
---
worry
     n 1: something or someone that causes anxiety; a source of
          unhappiness; "New York traffic is a constant concern";
          "it's a major worry" [syn: {concern}, {headache}, {vexation}]
     2: a strong feeling of anxiety; "his worry over the prospect of
        being fired"; "it is not work but worry that kills"; "he
        wanted to die and end his troubles" [syn: {trouble}]
     v 1: be worried, concerned, anxious, troubled, or uneasy; "I
          worry about my job"
     2: be concerned with; "I worry about my grades" [syn: {care}]
     3: disturb the peace of mind of; afflict with mental agitation
        or distress; "I cannot sleep--my daughter's health is
     ...
