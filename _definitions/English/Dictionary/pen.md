---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pen
offline_file: ""
offline_thumbnail: ""
uuid: 6705be6e-92c5-4e5b-bbd1-a4869ef3abe9
updated: 1484310301
title: pen
categories:
    - Dictionary
---
pen
     n 1: a writing implement with a point from which ink flows
     2: an enclosure for confining livestock
     3: a portable enclosure in which babies may be left to play
        [syn: {playpen}]
     4: a correctional institution for those convicted of major
        crimes [syn: {penitentiary}]
     5: female swan
     v : produce a literary work; "She composed a poem"; "He wrote
         four novels" [syn: {write}, {compose}, {indite}]
     [also: {pent}, {penning}, {penned}]
