---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proud
offline_file: ""
offline_thumbnail: ""
uuid: 80c9effa-073a-4c8b-9d74-ec1524fa0263
updated: 1484310152
title: proud
categories:
    - Dictionary
---
proud
     adj 1: feeling self-respect or pleasure in something by which you
            measure your self-worth; or being a reason for pride;
            "proud parents"; "proud of his accomplishments"; "a
            proud moment"; "proud to serve his country"; "a proud
            name"; "proud princes" [ant: {humble}]
     2: having or displaying great dignity or nobility; "a gallant
        pageant"; "lofty ships"; "majestic cities"; "proud alpine
        peaks" [syn: {gallant}, {lofty}, {majestic}]
