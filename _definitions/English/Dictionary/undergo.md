---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undergo
offline_file: ""
offline_thumbnail: ""
uuid: a293cfb6-e835-4461-9544-3c8876288917
updated: 1484310212
title: undergo
categories:
    - Dictionary
---
undergo
     v 1: of mental or physical states or experiences; "get an idea";
          "experience vertigo"; "get nauseous"; "undergo a strange
          sensation"; "The chemical undergoes a sudden change";
          "The fluid undergoes shear"; "receive injuries"; "have a
          feeling" [syn: {experience}, {receive}, {have}, {get}]
     2: go or live through; "We had many trials to go through"; "he
        saw action in Viet Nam" [syn: {experience}, {see}, {go
        through}]
     3: accept or undergo, often unwillingly; "We took a pay cut"
        [syn: {take}, {submit}]
     [also: {underwent}, {undergone}]
