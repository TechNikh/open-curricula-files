---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/synthetically
offline_file: ""
offline_thumbnail: ""
uuid: 82c37cff-dc2a-476e-9512-dfd090c29060
updated: 1484310395
title: synthetically
categories:
    - Dictionary
---
synthetically
     adv : by synthesis; in a synthetic manner; "some of these drugs
           have been derived from opium and others have been
           produced synthetically"
