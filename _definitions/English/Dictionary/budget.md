---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/budget
offline_file: ""
offline_thumbnail: ""
uuid: 9d41e13a-de63-4e63-a38a-94735c506e4f
updated: 1484310448
title: budget
categories:
    - Dictionary
---
budget
     n 1: a sum of money allocated for a particular purpose; "the
          laboratory runs on a budget of a million a year"
     2: a summary of intended expenditures along with proposals for
        how to meet them; "the president submitted the annual
        budget to Congress"
     v : make a budget
