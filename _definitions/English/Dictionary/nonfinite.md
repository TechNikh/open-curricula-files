---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonfinite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484607181
title: nonfinite
categories:
    - Dictionary
---
non-finite
     adj : of verbs; having neither person nor number nor mood (as a
           participle or gerund or infinitive); "infinite verb
           form" [syn: {infinite}] [ant: {finite}]
