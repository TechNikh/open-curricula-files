---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hodge%E2%80%93Arakelov_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 1dca4d5b-6f02-4bc3-b34d-e1607e8cb291
updated: 1484308641
title: Hodge–Arakelov theory
categories:
    - Number theory
---
In mathematics, Hodge–Arakelov theory of elliptic curves is an analogue of classical and p-adic Hodge theory for elliptic curves carried out in the framework of Arakelov theory. It was introduced by Mochizuki (1999).
