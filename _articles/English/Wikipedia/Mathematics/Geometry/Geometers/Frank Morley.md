---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frank_Morley
offline_file: ""
offline_thumbnail: ""
uuid: 0383e4c1-9d15-4ba0-91c1-9d1dbb5b9f83
updated: 1484309096
title: Frank Morley
tags:
    - life
    - Works
categories:
    - Geometers
---
Frank Morley (September 9, 1860 – October 17, 1937) was a leading mathematician, known mostly for his teaching and research in the fields of algebra and geometry. Among his mathematical accomplishments was the discovery and proof of the celebrated Morley's trisector theorem in elementary plane geometry. He led 50 Ph.D.'s to their degrees, and was said to be:
