---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thomas_Kirkman
offline_file: ""
offline_thumbnail: ""
uuid: c6f5136e-42b6-4a62-9e22-6024ae899206
updated: 1484309088
title: Thomas Kirkman
tags:
    - Early life and education
    - Ordination and ministry
    - Mathematics
    - "Kirkman's Schoolgirl Problem"
    - Pluquaternions
    - Polyhedral combinatorics
    - Late work
    - Awards and honours
    - Notes
categories:
    - Geometers
---
Thomas Penyngton Kirkman FRS (31 March 1806 – 3 February 1895) was a British mathematician and ordained minister of the Church of England. Despite being primarily a churchman, he maintained an active interest in research-level mathematics, and was listed by Alexander Macfarlane as one of ten leading 19th-century British mathematicians.[1][2][3] In the 1840s, he obtained an existence theorem for Steiner triple systems that founded the field of combinatorial design theory, while the related Kirkman's schoolgirl problem is named after him.[4][5]
