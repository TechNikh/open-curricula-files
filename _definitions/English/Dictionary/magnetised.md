---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnetised
offline_file: ""
offline_thumbnail: ""
uuid: 9d7dcbd4-5166-4ed0-a04a-1b9b501b5510
updated: 1484310369
title: magnetised
categories:
    - Dictionary
---
magnetised
     adj : having the properties of a magnet; i.e. of attracting iron
           or steel; "the hard disk is covered with a thin coat of
           magnetic material" [syn: {magnetic}, {magnetized}]
           [ant: {antimagnetic}]
