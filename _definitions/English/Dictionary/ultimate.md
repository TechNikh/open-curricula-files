---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ultimate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484435041
title: ultimate
categories:
    - Dictionary
---
ultimate
     adj 1: furthest or highest in degree or order; utmost or extreme;
            "the ultimate achievement"; "the ultimate question";
            "man's ultimate destiny"; "the ultimate insult";
            "one's ultimate goal in life" [ant: {proximate}]
     2: being the last or concluding element of a series; "the
        ultimate sonata of that opus"; "a distinction between the
        verb and noun senses of `conflict' is that in the verb the
        stress is on the ultimate (or last) syllable"
     3: being the ultimate or elemental constituents of anything;
        "the elemental stuff of...out of which the many forms of
        life have been molded"- Jack London; "the ...
