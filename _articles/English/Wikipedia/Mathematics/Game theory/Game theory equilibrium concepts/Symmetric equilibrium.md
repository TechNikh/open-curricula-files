---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: ccc23834-fb9a-4f20-b283-23ae8b547b04
updated: 1484309035
title: Symmetric equilibrium
categories:
    - Game theory equilibrium concepts
---
In game theory, a symmetric equilibrium is an equilibrium where all players use the same strategy (possibly mixed) in the equilibrium. In the Prisoner's Dilemma game pictured to the right, the only Nash equilibrium is (D, D). Since both players use the same strategy, the equilibrium is symmetric.
