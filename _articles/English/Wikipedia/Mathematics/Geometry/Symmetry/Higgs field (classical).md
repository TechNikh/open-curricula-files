---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Higgs_field_(classical)
offline_file: ""
offline_thumbnail: ""
uuid: 9e92e6a9-8083-4c7b-8e16-91554d918626
updated: 1484309312
title: Higgs field (classical)
categories:
    - Symmetry
---
Spontaneous symmetry breaking, a vacuum Higgs field, and its associated fundamental particle the Higgs boson are quantum phenomena. A vacuum Higgs field is responsible for spontaneous symmetry breaking the gauge symmetries of fundamental interactions and provides the Higgs mechanism of generating mass of elementary particles.
