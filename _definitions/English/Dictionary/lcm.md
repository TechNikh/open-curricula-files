---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lcm
offline_file: ""
offline_thumbnail: ""
uuid: 63740acb-8868-439f-b195-3acaa871dfac
updated: 1484310156
title: lcm
categories:
    - Dictionary
---
lcm
     n : the smallest multiple that is exactly divisible by every
         member of a set of numbers; "the least common multiple of
         12 and 18 is 36" [syn: {lowest common multiple}, {least
         common multiple}]
