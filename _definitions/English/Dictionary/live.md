---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/live
offline_file: ""
offline_thumbnail: ""
uuid: b632d87b-8ceb-4999-b918-24ed1221e70b
updated: 1484310290
title: live
categories:
    - Dictionary
---
live
     adj 1: actually being performed at the time of hearing or viewing;
            "a live television program"; "brought to you live from
            Lincoln Center"; "live entertainment involves
            performers actually in the physical presence of a live
            audience" [syn: {unrecorded}] [ant: {recorded}]
     2: showing characteristics of life; exerting force or
        containing energy; "live coals"; "tossed a live cigarette
        out the window"; "got a shock from a live wire"; "live ore
        is unmined ore"; "a live bomb"; "a live ball is one in
        play" [ant: {dead}]
     3: highly reverberant; "a live concert hall" [syn: {live(a)}]
     4: charged with ...
