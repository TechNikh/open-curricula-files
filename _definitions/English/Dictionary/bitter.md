---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bitter
offline_file: ""
offline_thumbnail: ""
uuid: 6d890671-afaf-4f36-9fd4-685665791b03
updated: 1484310353
title: bitter
categories:
    - Dictionary
---
bitter
     adj 1: marked by strong resentment or cynicism; "an acrimonious
            dispute"; "bitter about the divorce" [syn: {acrimonious}]
     2: very difficult to accept or bear; "the bitter truth"; "a
        bitter sorrow"
     3: harsh or corrosive in tone; "an acerbic tone piercing
        otherwise flowery prose"; "a barrage of acid comments";
        "her acrid remarks make her many enemies"; "bitter words";
        "blistering criticism"; "caustic jokes about political
        assassination, talk-show hosts and medical ethics"; "a
        sulfurous denunciation" [syn: {acerb}, {acerbic}, {acid},
        {acrid}, {blistering}, {caustic}, {sulfurous}, {sulphurous},
         ...
