---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organize
offline_file: ""
offline_thumbnail: ""
uuid: 39809a1c-be61-4f94-aad3-364e8a199d06
updated: 1484310484
title: organize
categories:
    - Dictionary
---
organize
     v 1: create (as an entity); "social groups form everywhere";
          "They formed a company" [syn: {form}, {organise}]
     2: cause to be structured or ordered or operating according to
        some principle or idea [syn: {organise}] [ant: {disorganize},
         {disorganize}]
     3: plan and direct (a complex undertaking); "he masterminded
        the robbery" [syn: {mastermind}, {engineer}, {direct}, {organise},
         {orchestrate}]
     4: bring order and organization to; "Can you help me organize
        my files?" [syn: {organise}, {coordinate}]
     5: arrange by systematic planning and united effort; "machinate
        a plot"; "organize a strike"; "devise a ...
