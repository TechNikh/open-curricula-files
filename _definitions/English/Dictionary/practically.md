---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/practically
offline_file: ""
offline_thumbnail: ""
uuid: a81f1d0e-975c-4986-8a97-440926b91cc8
updated: 1484310399
title: practically
categories:
    - Dictionary
---
practically
     adv 1: almost; "he was practically the only guest at the party"
     2: (degree adverb used before a noun phrase) for all practical
        purposes but not completely; "much the same thing happened
        every time" [syn: {much}]
