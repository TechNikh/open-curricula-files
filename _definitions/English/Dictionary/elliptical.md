---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elliptical
offline_file: ""
offline_thumbnail: ""
uuid: fd378d32-692c-43c0-ba9b-c7c7eeb39a64
updated: 1484310391
title: elliptical
categories:
    - Dictionary
---
elliptical
     adj 1: containing or characterized by ellipsis; "the clause of
            comparison is often elliptical"- G.O.Curme [syn: {elliptic}]
     2: rounded like an egg [syn: {egg-shaped}, {elliptic}, {oval},
        {ovate}, {oviform}, {ovoid}, {prolate}]
     3: characterized by extreme economy of expression or omission
        of superfluous elements; "the dialogue is elliptic and
        full of dark hints"; "the explanation was concise, even
        elliptical to the verge of obscurity"- H.O.Taylor [syn: {elliptic}]
