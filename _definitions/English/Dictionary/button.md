---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/button
offline_file: ""
offline_thumbnail: ""
uuid: ef76ce5f-2039-4652-a0f3-972bdd78f6e3
updated: 1484310140
title: button
categories:
    - Dictionary
---
butt on
     v : lie adjacent to another or share a boundary; "Canada adjoins
         the U.S."; "England marches with Scotland" [syn: {border},
          {adjoin}, {edge}, {abut}, {march}, {butt}, {butt against}]
