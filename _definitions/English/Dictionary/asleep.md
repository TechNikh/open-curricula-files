---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asleep
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450341
title: asleep
categories:
    - Dictionary
---
asleep
     adj 1: in a state of sleep; "were all asleep when the phone rang";
            "fell asleep at the wheel" [syn: {asleep(p)}] [ant: {awake(p)}]
     2: lacking sensation; "my foot is asleep"; "numb with cold"
        [syn: {asleep(p)}, {benumbed}, {numb}]
     3: dead; "he is deceased"; "our dear departed friend" [syn: {asleep(p)},
         {at peace(p)}, {at rest(p)}, {deceased}, {departed}, {gone}]
     adv 1: into a sleeping state; "he fell asleep"
     2: in the sleep of death
