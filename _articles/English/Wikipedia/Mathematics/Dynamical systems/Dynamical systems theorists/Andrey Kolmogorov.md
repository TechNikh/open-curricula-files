---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Andrey_Kolmogorov
offline_file: ""
offline_thumbnail: ""
uuid: 1340ef6f-d2f9-48f1-9668-eb2a632d817a
updated: 1484308688
title: Andrey Kolmogorov
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kolm_complexity_lect.jpg
tags:
    - Biography
    - Early life
    - Adulthood
    - Awards and honours
    - Bibliography
categories:
    - Dynamical systems theorists
---
Andrey Nikolaevich Kolmogorov (Russian: Андрей Николаевич Колмогоров; IPA: [ɐnˈdrʲej nʲɪkɐˈlajɪvʲɪtɕ kəlmɐˈɡorəf] ( listen), 25 April 1903 – 20 October 1987)[4][5] was a 20th-century Russian mathematician who made significant contributions to the mathematics of probability theory, topology, intuitionistic logic, turbulence, classical mechanics, algorithmic information theory and computational complexity.[1][3][6]
