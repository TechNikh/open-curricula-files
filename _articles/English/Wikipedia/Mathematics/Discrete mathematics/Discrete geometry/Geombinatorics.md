---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geombinatorics
offline_file: ""
offline_thumbnail: ""
uuid: df358978-d677-41fa-9713-0023ef5f75c3
updated: 1484308598
title: Geombinatorics
categories:
    - Discrete geometry
---
Geombinatorics is a mathematical research journal (ISSN 1065-7371) founded by Alexander Soifer and published by the University of Colorado, United States, since 1991 under an international board of editors (Ronald L. Graham, University of California, San Diego; Branko Grünbaum, University of Washington; Peter Johnson,Jr., Auburn University; Jaroslav Nešetřil, Charles University - Czech Republic; János Pach, Courant Institute of Mathematical Sciences and Heiko Harborth, Braunschweig University of Technology). It is devoted to problems in discrete, convex, and combinatorial geometry, as well as related areas.
