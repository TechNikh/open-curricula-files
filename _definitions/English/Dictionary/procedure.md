---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/procedure
offline_file: ""
offline_thumbnail: ""
uuid: 30b42619-428d-4298-85a4-45c87bbaebe0
updated: 1484310316
title: procedure
categories:
    - Dictionary
---
procedure
     n 1: a particular course of action intended to achieve a result;
          "the procedure of obtaining a driver's license"; "it was
          a process of trial and error" [syn: {process}]
     2: a process or series of acts especially of a practical or
        mechanical nature involved in a particular form of work;
        "the operations in building a house"; "certain machine
        tool operations" [syn: {operation}]
     3: a set sequence of steps, part of larger computer program
        [syn: {routine}, {subroutine}, {subprogram}, {function}]
     4: a mode of conducting legal and parliamentary proceedings
