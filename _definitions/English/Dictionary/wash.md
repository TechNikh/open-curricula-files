---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wash
offline_file: ""
offline_thumbnail: ""
uuid: 3e3eac31-d556-4263-87f7-eb65499906e3
updated: 1484310351
title: wash
categories:
    - Dictionary
---
wash
     n 1: a thin coat of water-base paint
     2: the work of cleansing (usually with soap and water) [syn: {washing},
         {lavation}]
     3: the dry bed of an intermittent stream (as at the bottom of a
        canyon) [syn: {dry wash}]
     4: the erosive process of washing away soil or gravel by water
        (as from a roadway); "from the house they watched the
        washout of their newly seeded lawn by the water" [syn: {washout}]
     5: the flow of air that is driven backwards by an aircraft
        propeller [syn: {slipstream}, {airstream}, {race}, {backwash}]
     6: a watercolor made by applying a series of monochrome washes
        one over the other [syn: {wash ...
