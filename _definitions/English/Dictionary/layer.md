---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/layer
offline_file: ""
offline_thumbnail: ""
uuid: f59f9274-1458-49f8-8a05-c822dceaf3b2
updated: 1484310335
title: layer
categories:
    - Dictionary
---
layer
     n 1: single thickness of usually some homogeneous substance;
          "slices of hard-boiled egg on a bed of spinach" [syn: {bed}]
     2: a relatively thin sheetlike expanse or region lying over or
        under another
     3: an abstract place usually conceived as having depth; "a good
        actor communicates on several levels"; "a simile has at
        least two layers of meaning"; "the mind functions on many
        strata simultaneously" [syn: {level}, {stratum}]
     4: a hen that lays eggs
     5: thin structure composed of a single thickness of cells
     v : make or form a layer; "layer the different colored sands"
