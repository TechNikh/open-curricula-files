---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_Tate_duality
offline_file: ""
offline_thumbnail: ""
uuid: bd699068-4ce6-4b7d-84eb-3c6d0a61a36e
updated: 1484309234
title: Local Tate duality
tags:
    - Statement
    - Case of finite modules
    - Case of p-adic representations
    - Notes
categories:
    - Duality theories
---
In Galois cohomology, local Tate duality (or simply local duality) is a duality for Galois modules for the absolute Galois group of a non-archimedean local field. It is named after John Tate who first proved it. It shows that the dual of such a Galois module is the Tate twist of usual linear dual. This new dual is called the (local) Tate dual.
