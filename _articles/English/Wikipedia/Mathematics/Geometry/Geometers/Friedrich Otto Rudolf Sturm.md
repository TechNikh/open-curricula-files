---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Friedrich_Otto_Rudolf_Sturm
offline_file: ""
offline_thumbnail: ""
uuid: bf946b72-84af-4039-8c1f-1509c1014d10
updated: 1484309103
title: Friedrich Otto Rudolf Sturm
categories:
    - Geometers
---
Friedrich Otto Rudolf Sturm (6 January 1841 – 12 April 1919) was a German mathematician. His Ph.D. advisor was Heinrich Eduard Schroeter, and Otto Toeplitz was one of his Ph.D. students. His best ever proposal type claim is commonly known as "Sturm's Theorem" based on finding the complex imaginary roots of an infinite arbitrary-integer series.
