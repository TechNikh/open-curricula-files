---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divisor
offline_file: ""
offline_thumbnail: ""
uuid: cef4d810-d183-4ba9-8ff0-7ad1a983ad97
updated: 1484310138
title: divisor
categories:
    - Dictionary
---
divisor
     n 1: one of two or more integers that can be exactly divided into
          another integer; "what are the 4 factors of 6?" [syn: {factor}]
     2: the number by which a dividend is divided
