---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/various
offline_file: ""
offline_thumbnail: ""
uuid: e46d76a9-f762-4490-a9e6-12ee77a371cb
updated: 1484310264
title: various
categories:
    - Dictionary
---
various
     adj 1: of many different kinds purposefully arranged but lacking
            any uniformity; "assorted sizes"; "his disguises are
            many and various"; "various experiments have failed to
            disprove the theory"; "cited various reasons for his
            behavior" [syn: {assorted}]
     2: considered individually; "the respective club members";
        "specialists in their several fields"; "the various
        reports all agreed" [syn: {respective(a)}, {several(a)}, {various(a)}]
     3: distinctly dissimilar or unlike; "diverse parts of the
        country"; "celebrities as diverse as Bob Hope and Bob
        Dylan"; "animals as various as the jaguar and ...
