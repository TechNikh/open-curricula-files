---
version: 1
type: video
provider: YouTube
id: zJMrlwuQiN0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4052922d-6a0e-4aba-a198-64ef8ff14442
updated: 1486069617
title: Solving problems with pictographs 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/zJMrlwuQiN0/default.jpg
    - https://i3.ytimg.com/vi/zJMrlwuQiN0/1.jpg
    - https://i3.ytimg.com/vi/zJMrlwuQiN0/2.jpg
    - https://i3.ytimg.com/vi/zJMrlwuQiN0/3.jpg
---
