---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/punished
offline_file: ""
offline_thumbnail: ""
uuid: 7fd3cf56-b557-4783-8b77-b596720abc54
updated: 1484310565
title: punished
categories:
    - Dictionary
---
punished
     adj : subjected to a penalty (as pain or shame or restraint or
           loss) for an offense or fault or in order to coerce
           some behavior (as a confession or obedience) [ant: {unpunished}]
