---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/densely
offline_file: ""
offline_thumbnail: ""
uuid: 68996ca2-60b2-43e7-bebb-473c65aa806c
updated: 1484310473
title: densely
categories:
    - Dictionary
---
densely
     adv 1: in a stupid manner; "he had so rapaciously desired and so
            obtusely expected to find her alone" [syn: {dumbly}, {obtusely}]
     2: in a concentrated manner; "old houses are often so densely
        packed that perhaps three or four have to be demolished
        for every new one built"; "a thickly populated area" [syn:
         {thickly}] [ant: {thinly}]
