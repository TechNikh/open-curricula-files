---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meanwhile
offline_file: ""
offline_thumbnail: ""
uuid: 7ad65b09-2f5f-4c95-be07-5b77b886104b
updated: 1484310486
title: meanwhile
categories:
    - Dictionary
---
meanwhile
     adv 1: at the same time but in another place; "meanwhile, back at
            the ranch..."
     2: during the intervening time; "meanwhile I will not think
        about the problem"; "meantime he was attentive to his
        other interests"; "in the meantime the police were
        notified" [syn: {meantime}, {in the meantime}]
