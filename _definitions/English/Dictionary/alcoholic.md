---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alcoholic
offline_file: ""
offline_thumbnail: ""
uuid: 385e5aad-3697-42d7-9edf-8894ed45ebad
updated: 1484310424
title: alcoholic
categories:
    - Dictionary
---
alcoholic
     adj 1: used of beverages containing alcohol; "alcoholic drinks"
            [ant: {nonalcoholic}]
     2: addicted to alcohol; "alcoholic expatriates in Paris"- Carl
        Van Doren [syn: {alcohol-dependent}]
     n : a person who drinks alcohol to excess habitually [syn: {alky},
          {dipsomaniac}, {boozer}, {lush}, {soaker}, {souse}]
