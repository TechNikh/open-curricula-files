---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/racism
offline_file: ""
offline_thumbnail: ""
uuid: c6fbe1b7-ca57-4b43-bfc0-86c4393c4cfe
updated: 1484310579
title: racism
categories:
    - Dictionary
---
racism
     n 1: the prejudice that members of one race are intrinsically
          superior to members of other races
     2: discriminatory or abusive behavior towards members of
        another race [syn: {racialism}, {racial discrimination}]
