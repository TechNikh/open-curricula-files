---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/splash
offline_file: ""
offline_thumbnail: ""
uuid: 1d4069dd-74cd-49aa-9594-b6fec3d7e00e
updated: 1484310383
title: splash
categories:
    - Dictionary
---
splash
     n 1: the sound like water splashing [syn: {plash}]
     2: a small quantity of something moist or soft; "a dab of
        paint"; "a splatter of mud" [syn: {dab}, {splatter}]
     3: the act of splashing a (liquid) substance on a surface [syn:
         {spatter}, {spattering}, {splashing}, {splattering}]
     4: the act of scattering water about haphazardly [syn: {splashing}]
     v 1: cause (a liquid) to spatter about, especially with force;
          "She splashed the water around her" [syn: {sprinkle}, {splosh}]
     2: walk through mud or mire; "We had to splosh across the wet
        meadow" [syn: {squelch}, {squish}, {splosh}, {slosh}, {slop}]
     3: dash a liquid upon or ...
