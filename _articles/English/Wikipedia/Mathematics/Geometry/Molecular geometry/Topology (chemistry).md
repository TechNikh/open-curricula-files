---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topology_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 0c2bc5f8-5269-45ac-879d-da39eed21882
updated: 1484309280
title: Topology (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Klein_bottle_translucent.png
categories:
    - Molecular geometry
---
In chemistry, topology provides a convenient way of describing and predicting the molecular structure within the constraints of three-dimensional (3-D) space. Given the determinants of chemical bonding and the chemical properties of the atoms, topology provides a model for explaining how the atoms ethereal wave functions must fit together. Molecular topology is a part of mathematical chemistry dealing with the algebraic description of chemical compounds so allowing a unique and easy characterization of them.
