---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/younger
offline_file: ""
offline_thumbnail: ""
uuid: 5cdc66ba-8e60-41f1-9744-cfafdb9b5a32
updated: 1484310585
title: younger
categories:
    - Dictionary
---
young
     adj 1: (used of living things especially persons) in an early
            period of life or development or growth; "young
            people" [syn: {immature}] [ant: {old}]
     2: (of crops) harvested at an early stage of development;
        before complete maturity; "new potatoes"; "young corn"
        [syn: {new}]
     n 1: any immature animal [syn: {offspring}]
     2: United States film and television actress (1913-2000) [syn:
        {Loretta Young}]
     3: United States civil rights leader (1921-1971) [syn: {Whitney
        Young}, {Whitney Moore Young Jr.}]
     4: British physicist and Egyptologist; he revived the wave
        theory of light and proposed a ...
