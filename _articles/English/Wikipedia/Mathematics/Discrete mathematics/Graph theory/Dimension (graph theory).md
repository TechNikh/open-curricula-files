---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dimension_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 0bae52cf-8421-4324-bfba-5c716a284582
updated: 1484308620
title: Dimension (graph theory)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Petersen_graph%252C_unit_distance.svg.png'
tags:
    - Examples
    - Complete graph
    - Complete bipartite graphs
    - Dimension and chromatic number
    - Euclidean dimension
    - Euclidean dimension and maximal degree
    - Computational complexity
categories:
    - Graph theory
---
In mathematics, and particularly in graph theory, the dimension of a graph is the least integer n such that there exists a "classical representation" of the graph in the Euclidean space of dimension n with all the edges having unit length.
