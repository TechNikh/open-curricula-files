---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pour
offline_file: ""
offline_thumbnail: ""
uuid: 1e286ed8-9bee-413f-9e37-31df3bb8dafa
updated: 1484310230
title: pour
categories:
    - Dictionary
---
pour
     v 1: cause to run; "pour water over the floor"
     2: move in large numbers; "people were pouring out of the
        theater"; "beggars pullulated in the plaza" [syn: {swarm},
         {stream}, {teem}, {pullulate}]
     3: pour out; "the sommelier decanted the wines" [syn: {decant},
         {pour out}]
     4: flow in a spurt; "Water poured all over the floor"
     5: supply in large amounts or quantities; "We poured money into
        the education of our children"
     6: rain heavily; "Put on your rain coat-- it's pouring
        outside!" [syn: {pelt}, {stream}, {rain cats and dogs}, {rain
        buckets}]
