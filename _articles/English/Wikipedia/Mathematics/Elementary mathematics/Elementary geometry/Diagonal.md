---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diagonal
offline_file: ""
offline_thumbnail: ""
uuid: a8d6a590-0c04-4f1f-bcf9-1eb5d24c7cc8
updated: 1484308930
title: Diagonal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cube_diagonals.svg.png
tags:
    - Non-mathematical uses
    - Polygons
    - Regions formed by diagonals
    - Matrices
    - Geometry
    - Notes
categories:
    - Elementary geometry
---
In geometry, a diagonal is a line segment joining two vertices of a polygon or polyhedron, when those vertices are not on the same edge. Informally, any sloping line is called diagonal. The word "diagonal" derives from the ancient Greek διαγώνιος diagonios,[1] "from angle to angle" (from διά- dia-, "through", "across" and γωνία gonia, "angle", related to gony "knee"); it was used by both Strabo[2] and Euclid[3] to refer to a line connecting two vertices of a rhombus or cuboid,[4] and later adopted into Latin as diagonus ("slanting line").
