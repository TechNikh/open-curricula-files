---
version: 1
type: video
provider: YouTube
id: VUe3uFDfJvU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: afa69734-584c-4baa-98f7-e70d373e6d8a
updated: 1486069617
title: Understanding Proportions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/VUe3uFDfJvU/default.jpg
    - https://i3.ytimg.com/vi/VUe3uFDfJvU/1.jpg
    - https://i3.ytimg.com/vi/VUe3uFDfJvU/2.jpg
    - https://i3.ytimg.com/vi/VUe3uFDfJvU/3.jpg
---
