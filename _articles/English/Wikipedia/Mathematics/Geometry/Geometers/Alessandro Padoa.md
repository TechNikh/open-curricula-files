---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alessandro_Padoa
offline_file: ""
offline_thumbnail: ""
uuid: 128f4f53-9fab-4070-930f-9ff12c999675
updated: 1484309093
title: Alessandro Padoa
tags:
    - Congressional addresses
    - "Philosophers' congress"
    - "Mathematicians' congress"
    - Bibliography
categories:
    - Geometers
---
Alessandro Padoa (14 October 1868 – 25 November 1937) was an Italian mathematician and logician, a contributor to the school of Giuseppe Peano.[1] He is remembered for a method for deciding whether, given some formal theory, a new primitive notion is truly independent of the other primitive notions. There is an analogous problem in axiomatic theories, namely deciding whether a given axiom is independent of the other axioms.
