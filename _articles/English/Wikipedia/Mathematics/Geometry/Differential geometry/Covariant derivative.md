---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covariant_derivative
offline_file: ""
offline_thumbnail: ""
uuid: 91d5dba6-190c-42ed-970c-23ac6fa4af76
updated: 1484309212
title: Covariant derivative
tags:
    - Introduction and history
    - Motivation
    - Remarks
    - Informal definition using an embedding into Euclidean space
    - Formal definition
    - Functions
    - Vector fields
    - Covector fields
    - Tensor fields
    - Coordinate description
    - Examples
    - Notation
    - Derivative along curve
    - Relation to Lie derivative
    - Notes
categories:
    - Differential geometry
---
In mathematics, the covariant derivative is a way of specifying a derivative along tangent vectors of a manifold. Alternatively, the covariant derivative is a way of introducing and working with a connection on a manifold by means of a differential operator, to be contrasted with the approach given by a principal connection on the frame bundle – see affine connection. In the special case of a manifold isometrically embedded into a higher-dimensional Euclidean space, the covariant derivative can be viewed as the orthogonal projection of the Euclidean derivative along a tangent vector onto the manifold's tangent space. In this case the Euclidean derivative is broken into two parts, the ...
