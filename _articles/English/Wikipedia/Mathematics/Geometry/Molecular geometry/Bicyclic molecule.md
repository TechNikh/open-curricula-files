---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bicyclic_molecule
offline_file: ""
offline_thumbnail: ""
uuid: c8abc1ab-1ed6-4b00-875d-ce3cdfc65ffd
updated: 1484309277
title: Bicyclic molecule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-NorbornaneNumbering.png
tags:
    - Nomenclature
categories:
    - Molecular geometry
---
A bicyclic molecule (bi = two, cycle = ring) is a molecule that features two joined rings.[1] Bicyclic structures occur widely, for example in many biologically important molecules like α-thujene and camphor. A bicyclic compound can be carbocyclic (all of the ring atoms are carbons), or heterocyclic (the rings atoms consist of at least two different elements), like DABCO.[2] Moreover, the two rings can both be aliphatic (e.g. decalin and norbornane), or can be aromatic (e.g. naphthalene), or a combination of aliphatic and aromatic (e.g. tetralin).
