---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/people
offline_file: ""
offline_thumbnail: ""
uuid: ea6184ba-4e55-4e72-a9f3-13b504f79221
updated: 1484310335
title: people
categories:
    - Dictionary
---
people
     n 1: (plural) any group of human beings (men or women or
          children) collectively; "old people"; "there were at
          least 200 people in the audience"
     2: the body of citizens of a state or country; "the Spanish
        people" [syn: {citizenry}]
     3: the common people generally; "separate the warriors from the
        mass"; "power to the people" [syn: {multitude}, {masses},
        {mass}, {hoi polloi}]
     4: members of a family line; "his people have been farmers for
        generations"; "are your people still alive?"
     v 1: fill with people or supply with inhabitants; "people a
          room"; "The government wanted to populate the remote
          ...
