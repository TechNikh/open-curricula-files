---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secularism
offline_file: ""
offline_thumbnail: ""
uuid: 6dc7eefd-fd59-437a-b64d-26d9b48532d8
updated: 1484310599
title: secularism
categories:
    - Dictionary
---
secularism
     n : a doctrine that rejects religion and religious
         considerations
