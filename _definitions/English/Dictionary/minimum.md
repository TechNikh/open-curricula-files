---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minimum
offline_file: ""
offline_thumbnail: ""
uuid: fba1fa00-5b40-494e-b50b-9e1c25d61e4f
updated: 1484310226
title: minimum
categories:
    - Dictionary
---
minimum
     adj : the least possible; "needed to enforce minimal standards";
           "her grades were minimal"; "minimum wage"; "a minimal
           charge for the service" [syn: {minimal}] [ant: {maximal},
            {maximal}]
     n 1: the smallest possible quantity [syn: {lower limit}] [ant: {maximum}]
     2: the point on a curve where the tangent changes from negative
        on the left to positive on the right [ant: {maximum}]
     [also: {minima} (pl)]
