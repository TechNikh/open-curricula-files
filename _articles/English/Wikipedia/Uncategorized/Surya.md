---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surya
offline_file: ""
offline_thumbnail: ""
uuid: d0e041fa-6d70-4388-88da-b07482957ad5
updated: 1484308327
title: Surya
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-WLANL_-_23dingenvoormusea_-_Suryabeeldje.jpg
tags:
    - Texts and history
    - Vedic
    - Epics
    - Buddhist
    - Greek and Persian influences
    - Iconography
    - Arka, Mitra and other synonyms
    - Astronomy
    - Zodiac and astrology
    - In Buddhism
    - Sun Temples
    - Dedicated temples
    - Surya temples outside India
    - Surya in Indian culture
    - Festivals
    - Dances
    - Surya Namaskar
    - Gallery
    - Sources
categories:
    - Uncategorized
---
Surya (/ˈsʊərjə/[citation needed], Sanskrit: सूर्य, IAST: ‘'Sūrya’') means the Sun in Nepal and India.[1] Synonyms of Surya in ancient Indian literature include Aditya, Arka, Bhanu, Savitr, Pushan, Ravi, Martanda, Mitra and Vivasvan.[3][4][5]
