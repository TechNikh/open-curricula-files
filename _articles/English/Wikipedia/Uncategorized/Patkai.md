---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Patkai
offline_file: ""
offline_thumbnail: ""
uuid: 6a9da1a2-32ab-4f54-97f1-6a3bdf08266a
updated: 1484308420
title: Patkai
tags:
    - geography
    - Indian states along the Patkai
    - Notes
categories:
    - Uncategorized
---
The Pat-kai (Pron: ˈpʌtˌkaɪ) or Patkai Bum meaning "to cut (pat) chicken (Kai)" in Tai-Ahom language are the hills on India's north-eastern border with Burma. They were created by the same tectonic processes that resulted in the formation of the Himalayas in the Mesozoic.[citation needed]
