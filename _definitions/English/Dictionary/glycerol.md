---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glycerol
offline_file: ""
offline_thumbnail: ""
uuid: f01145ed-ddc7-405b-a6be-039887823e20
updated: 1484310426
title: glycerol
categories:
    - Dictionary
---
glycerol
     n : a sweet syrupy trihydroxy alcohol obtained by saponification
         of fats and oils [syn: {glycerin}, {glycerine}]
