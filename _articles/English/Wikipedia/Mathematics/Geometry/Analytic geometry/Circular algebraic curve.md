---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circular_algebraic_curve
offline_file: ""
offline_thumbnail: ""
uuid: de98780b-e165-428c-87b4-b06f195eb805
updated: 1484309159
title: Circular algebraic curve
tags:
    - Multicircular algebraic curves
    - Examples
    - Footnotes
categories:
    - Analytic geometry
---
In geometry, a circular algebraic curve is a type of plane algebraic curve determined by an equation F(x, y) = 0, where F is a polynomial with real coefficients and the highest-order terms of F form a polynomial divisible by x2 + y2. More precisely, if F = Fn + Fn−1 + ... + F1 + F0, where each Fi is homogeneous of degree i, then the curve F(x, y) = 0 is circular if and only if Fn is divisible by x2 + y2.
