---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deal
offline_file: ""
offline_thumbnail: ""
uuid: 647f4699-1d14-4b7b-acd8-5deaeccfdcf3
updated: 1484310275
title: deal
categories:
    - Dictionary
---
deal
     adj : made of fir or pine; "a plain deal table"
     n 1: a particular instance of buying or selling; "it was a
          package deal"; "I had no further trade with him"; "he's
          a master of the business deal" [syn: {trade}, {business
          deal}]
     2: an agreement between parties (usually arrived at after
        discussion) fixing obligations of each; "he made a bargain
        with the devil"; "he rose to prominence through a series
        of shady deals" [syn: {bargain}]
     3: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it ...
