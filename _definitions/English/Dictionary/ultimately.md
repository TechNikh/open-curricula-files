---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ultimately
offline_file: ""
offline_thumbnail: ""
uuid: 25c2ea57-931f-4b34-925e-b1d8fa52ee6e
updated: 1484310266
title: ultimately
categories:
    - Dictionary
---
ultimately
     adv 1: as the end result of a succession or process; "ultimately he
            had to give in"; "at long last the winter was over"
            [syn: {finally}, {in the end}, {at last}, {at long
            last}]
     2: after everything has been considered; "in the final
        analysis, we are quite well off" [syn: {in the last
        analysis}, {in the final analysis}]
