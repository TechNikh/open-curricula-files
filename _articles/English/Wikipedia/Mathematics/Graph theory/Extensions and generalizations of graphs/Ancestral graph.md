---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ancestral_graph
offline_file: ""
offline_thumbnail: ""
uuid: c3b76a5b-e53e-4d1f-88b2-b3fb6287aac4
updated: 1484309406
title: Ancestral graph
tags:
    - Definition
    - Applications
categories:
    - Extensions and generalizations of graphs
---
In statistics and Markov modeling, an ancestral graph is a type of mixed graph to provide a graphical representation for the result of marginalizing one or more vertices in a graphical model that takes the form of a directed acyclic graph.
