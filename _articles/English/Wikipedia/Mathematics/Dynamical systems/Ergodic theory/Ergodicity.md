---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodicity
offline_file: ""
offline_thumbnail: ""
uuid: 0307fe71-89de-4f5c-a89c-876a1f7222d8
updated: 1484308777
title: Ergodicity
tags:
    - Etymology
    - Formal definition
    - Measurable flows
    - Unique ergodicity
    - Markov chains
    - Examples in electronics
    - Ergodic decomposition
    - Notes
categories:
    - Ergodic theory
---
In mathematics, the term ergodic is used to describe a dynamical system which, broadly speaking, has the same behavior averaged over time as averaged over the space of all the system's states (phase space). In physics the term is used to imply that a system satisfies the ergodic hypothesis of thermodynamics.
