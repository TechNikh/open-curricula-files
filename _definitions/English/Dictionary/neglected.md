---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neglected
offline_file: ""
offline_thumbnail: ""
uuid: 2139a7e1-abdd-40b1-b378-276b38554de3
updated: 1484310196
title: neglected
categories:
    - Dictionary
---
neglected
     adj 1: disregarded; "his cries were unheeded"; "Shaw's neglected
            one-act comedy, `A Village Wooing'"; "her ignored
            advice" [syn: {ignored}, {unheeded}]
     2: lacking a caretaker; "a neglected child"; "many casualties
        were lying unattended" [syn: {unattended}]
