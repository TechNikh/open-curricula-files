---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exact
offline_file: ""
offline_thumbnail: ""
uuid: 9ba41844-ea23-4bff-aa5a-b8a1276d5a26
updated: 1484310391
title: exact
categories:
    - Dictionary
---
exact
     adj 1: marked by strict and particular and complete accordance with
            fact; "an exact mind"; "an exact copy"; "hit the exact
            center of the target" [ant: {inexact}]
     2: (of ideas, images, representations, expressions)
        characterized by perfect conformity to fact or truth ;
        strictly correct; "a precise image"; "a precise
        measurement" [syn: {accurate}, {precise}]
     v 1: claim as due or just; "The bank demanded payment of the
          loan" [syn: {demand}]
     2: take as an undesirable consequence of some event or state of
        affairs; "the accident claimed three lives"; "The hard
        work took its toll on her" [syn: ...
