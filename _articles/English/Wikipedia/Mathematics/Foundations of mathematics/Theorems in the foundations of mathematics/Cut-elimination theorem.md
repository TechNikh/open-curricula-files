---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cut-elimination_theorem
offline_file: ""
offline_thumbnail: ""
uuid: d2fc8ce3-5317-4494-94db-4a071e264522
updated: 1484308997
title: Cut-elimination theorem
tags:
    - The cut rule
    - Cut elimination
    - Consequences of the theorem
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
The cut-elimination theorem (or Gentzen's Hauptsatz) is the central result establishing the significance of the sequent calculus. It was originally proved by Gerhard Gentzen 1934 in his landmark paper "Investigations in Logical Deduction" for the systems LJ and LK formalising intuitionistic and classical logic respectively. The cut-elimination theorem states that any judgement that possesses a proof in the sequent calculus that makes use of the cut rule also possesses a cut-free proof, that is, a proof that does not make use of the cut rule.[1][2]
