---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finer
offline_file: ""
offline_thumbnail: ""
uuid: 498ed1ea-38ee-4813-b313-e8e53ad17346
updated: 1484310391
title: finer
categories:
    - Dictionary
---
finer
     adj : (comparative of `fine') greater in quality or excellence; "a
           finer wine"; "a finer musician"
