---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Greninger_chart
offline_file: ""
offline_thumbnail: ""
uuid: 96998aa8-a46b-4d73-b217-e1437ad5f5f2
updated: 1484309343
title: Greninger chart
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GreningerChart.png
tags:
    - Sources
categories:
    - Trigonometry
---
In crystallography, a Greninger chart[1] /ˈɡrɛnɪŋər/ is a chart that allows angular relations between zones and planes in a crystal to be directly read from an x-ray diffraction photograph.
