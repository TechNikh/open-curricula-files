---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galactic_coordinate_system
offline_file: ""
offline_thumbnail: ""
uuid: d28e0683-3965-41e9-8dd2-dc8154506e8f
updated: 1484309295
title: Galactic coordinate system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-236084main_MilkyWay-full-annotated.jpg
tags:
    - Galactic longitude
    - Galactic latitude
    - Definition
    - Rectangular coordinates
    - In the constellations
categories:
    - Orientation (geometry)
---
The galactic coordinate system is a celestial coordinate system in spherical coordinates, with the Sun as its center, the primary direction aligned with the approximate center of the Milky Way galaxy, and the fundamental plane approximately in the galactic plane. It uses the right-handed convention, meaning that coordinates are positive toward the north and toward the east in the fundamental plane.[1]
