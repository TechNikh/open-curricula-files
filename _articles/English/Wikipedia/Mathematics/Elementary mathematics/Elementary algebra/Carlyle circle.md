---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carlyle_circle
offline_file: ""
offline_thumbnail: ""
uuid: 1f025214-c149-45ae-9512-397cab888526
updated: 1484308907
title: Carlyle circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-CarlyleCircle.svg.png
tags:
    - Definition
    - Defining property
    - Construction of regular polygons
    - Regular pentagon
    - Regular heptadecagon
    - Regular 257-gon
    - Regular 65537-gon
    - History
categories:
    - Elementary algebra
---
In mathematics, a Carlyle circle (after Thomas Carlyle (1795–1881)) is a certain circle in a coordinate plane associated with a quadratic equation. The circle has the property that the solutions of the quadratic equation are the horizontal coordinates of the intersections of the circle with the horizontal axis. Carlyle circles have been used to develop ruler-and-compass constructions of regular polygons.
