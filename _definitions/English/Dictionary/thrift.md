---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thrift
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484511361
title: thrift
categories:
    - Dictionary
---
thrift
     n 1: any of numerous sun-loving low-growing evergreens of the
          genus Armeria having round heads of pink or white
          flowers
     2: extreme care in spending money; reluctance to spend money
        unnecessarily [syn: {parsimony}, {parsimoniousness}, {penny-pinching}]
