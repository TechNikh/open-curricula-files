---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triode
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484427001
title: triode
categories:
    - Dictionary
---
triode
     n : a thermionic vacuum tube having three electrodes;
         fluctuations of the charge on the grid control the flow
         from cathode to anode which making amplification possible
