---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Robertson%E2%80%93Seymour_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5aeac995-ff21-4b08-9217-3e71500f4ff3
updated: 1484309411
title: Robertson–Seymour theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Petersen_family.svg.png
tags:
    - Statement
    - Forbidden minor characterizations
    - Examples of minor-closed families
    - Obstruction sets
    - Polynomial time recognition
    - Fixed-parameter tractability
    - Finite form of the graph minor theorem
    - Notes
categories:
    - Graph minor theory
---
In graph theory, the Robertson–Seymour theorem (also called the graph minor theorem[1]) states that the undirected graphs, partially ordered by the graph minor relationship, form a well-quasi-ordering.[2] Equivalently, every family of graphs that is closed under minors can be defined by a finite set of forbidden minors, in the same way that Wagner's theorem characterizes the planar graphs as being the graphs that do not have the complete graph K5 and the complete bipartite graph K3,3 as minors.
