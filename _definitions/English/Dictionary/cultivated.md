---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivated
offline_file: ""
offline_thumbnail: ""
uuid: 6421a226-84d0-40da-a779-8bfab9e86997
updated: 1484310473
title: cultivated
categories:
    - Dictionary
---
cultivated
     adj 1: (of land or fields) prepared for raising crops by plowing or
            fertilizing; "cultivated land" [ant: {uncultivated}]
     2: no longer in the natural state; developed by human care and
        for human use; "cultivated roses"; "cultivated
        blackberries"
     3: marked by refinement in taste and manners; "cultivated
        speech"; "cultured Bostonians"; "cultured tastes"; "a
        genteel old lady"; "polite society" [syn: {civilized}, {civilised},
         {cultured}, {genteel}, {polite}]
