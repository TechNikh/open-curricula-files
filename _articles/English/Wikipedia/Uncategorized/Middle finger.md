---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Middle_fingers
offline_file: ""
offline_thumbnail: ""
uuid: f69e0c54-ff24-4f96-b077-1660b0e0dd35
updated: 1484308372
title: Middle finger
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Middle_finger_BNC.jpg
categories:
    - Uncategorized
---
The middle finger, long finger, or tall finger is the third digit of the human hand, located between the index finger and the ring finger. It is usually the longest finger. In anatomy, it is also called the third finger, digitus medius, digitus tertius, or digitus III.
