---
version: 1
type: video
provider: YouTube
id: 7BkcNLOf56w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Dependent%20probability%20example%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9783b26d-6fc3-47e2-ac01-1edeaeb81813
updated: 1486069690
title: Dependent probability example 2
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/7BkcNLOf56w/default.jpg
    - https://i3.ytimg.com/vi/7BkcNLOf56w/1.jpg
    - https://i3.ytimg.com/vi/7BkcNLOf56w/2.jpg
    - https://i3.ytimg.com/vi/7BkcNLOf56w/3.jpg
---
