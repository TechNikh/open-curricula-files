---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/popularly
offline_file: ""
offline_thumbnail: ""
uuid: e7e08b73-25ad-4add-96c0-9711b94241db
updated: 1484310303
title: popularly
categories:
    - Dictionary
---
popularly
     adv : among the people; "this topic was popularly discussed"
