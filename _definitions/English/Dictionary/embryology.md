---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embryology
offline_file: ""
offline_thumbnail: ""
uuid: 403dbacb-7e9e-4593-9408-84ccb015415e
updated: 1484310286
title: embryology
categories:
    - Dictionary
---
embryology
     n : the branch of biology that studies the formation and early
         development of living organisms
