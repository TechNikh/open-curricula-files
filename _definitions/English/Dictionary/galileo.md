---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galileo
offline_file: ""
offline_thumbnail: ""
uuid: 2fa8a092-c3fe-486e-b941-0682518dd85c
updated: 1484310204
title: galileo
categories:
    - Dictionary
---
Galileo
     n : Italian astronomer and mathematician who was the first to
         use a telescope to study the stars; demonstrated that
         different weights descend at the same rate; perfected the
         refracting telescope that enabled him to make many
         discoveries (1564-1642) [syn: {Galileo Galilei}]
