---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CC_system
offline_file: ""
offline_thumbnail: ""
uuid: cbc37d48-dda8-45d3-ac2f-cc7f6b47c47f
updated: 1484309170
title: CC system
tags:
    - Axioms
    - Construction from planar point sets
    - Equivalent notions
    - Algorithmic applications
    - Combinatorial enumeration
    - Notes
categories:
    - Computational geometry
---
In computational geometry, a CC system or counterclockwise system is a ternary relation pqr introduced by Donald Knuth to model the clockwise ordering of triples of points in general position in the Euclidean plane.[1]
