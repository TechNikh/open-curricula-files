---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffer
offline_file: ""
offline_thumbnail: ""
uuid: 1cc997e9-4d15-4fc1-a226-87385708aa6d
updated: 1484310220
title: suffer
categories:
    - Dictionary
---
suffer
     v 1: undergo or be subjected to; "He suffered the penalty"; "Many
          saints suffered martyrdom" [syn: {endure}] [ant: {enjoy}]
     2: undergo (as of injuries and illnesses); "She suffered a
        fracture in the accident"; "He had an insulin shock after
        eating three candy bars"; "She got a bruise on her leg";
        "He got his arm broken in the scuffle" [syn: {sustain}, {have},
         {get}]
     3: endure (emotional pain); "Every time her husband gets drunk,
        she suffers"
     4: put up with something or somebody unpleasant; "I cannot bear
        his constant criticism"; "The new secretary had to endure
        a lot of unprofessional remarks"; "he ...
