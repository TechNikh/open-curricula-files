---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nuclear
offline_file: ""
offline_thumbnail: ""
uuid: bb48e261-e50f-45c0-8fd1-bf96b613769c
updated: 1484310242
title: nuclear
categories:
    - Dictionary
---
nuclear
     adj 1: (weapons) deriving destructive energy from  the release of
            atomic energy; "nuclear war"; "nuclear weapons";
            "atomic bombs" [syn: {atomic}] [ant: {conventional}]
     2: of or relating to or constituting the nucleus of an atom;
        "nuclear physics"; "nuclear fission"; "nuclear forces"
     3: of or relating to or constituting the nucleus of a cell;
        "nuclear membrane"; "nuclear division"
     4: constituting or like a nucleus; "annexation of the suburban
        fringe by the nuclear metropolis"; "the nuclear core of
        the congregation"
