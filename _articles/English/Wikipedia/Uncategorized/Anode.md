---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anodic
offline_file: ""
offline_thumbnail: ""
uuid: d1b891e1-2ada-4cf7-b670-18c73b7ed47d
updated: 1484308391
title: Anode
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zinc_anode_2.svg.png
tags:
    - Charge flow
    - Examples
    - Etymology
    - Electrolytic anode
    - Battery or galvanic cell anode
    - Vacuum tube anode
    - Diode anode
    - Sacrificial anode
    - Related antonym
categories:
    - Uncategorized
---
An anode is an electrode through which conventional current flows into a polarized electrical device. A common mnemonic is ACID for "anode current into device".[1] The direction of (positive) electric current is opposite to the direction of electron flow: (negatively charged) electrons flow out the anode to the outside circuit.
