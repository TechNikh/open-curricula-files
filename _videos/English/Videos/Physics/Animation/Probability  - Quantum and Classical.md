---
version: 1
type: video
provider: YouTube
id: X2eomv6XfWo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 76756a82-1d15-4ce5-80f0-52232fe21f6d
updated: 1486069656
title: 'Probability  - Quantum and Classical'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/X2eomv6XfWo/default.jpg
    - https://i3.ytimg.com/vi/X2eomv6XfWo/1.jpg
    - https://i3.ytimg.com/vi/X2eomv6XfWo/2.jpg
    - https://i3.ytimg.com/vi/X2eomv6XfWo/3.jpg
---
