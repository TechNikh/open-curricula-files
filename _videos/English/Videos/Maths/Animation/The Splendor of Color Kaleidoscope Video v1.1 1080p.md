---
version: 1
type: video
provider: YouTube
id: q2fIWB8o-bs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d740339-e729-4573-aec9-b55bc8372adf
updated: 1486069649
title: The Splendor of Color Kaleidoscope Video v1.1 1080p
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/q2fIWB8o-bs/default.jpg
    - https://i3.ytimg.com/vi/q2fIWB8o-bs/1.jpg
    - https://i3.ytimg.com/vi/q2fIWB8o-bs/2.jpg
    - https://i3.ytimg.com/vi/q2fIWB8o-bs/3.jpg
---
