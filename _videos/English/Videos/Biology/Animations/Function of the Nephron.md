---
version: 1
type: video
provider: YouTube
id: glu0dzK4dbU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 99918286-d6fd-4a62-9542-9fb3bbdb3b15
updated: 1486069669
title: Function of the Nephron
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/glu0dzK4dbU/default.jpg
    - https://i3.ytimg.com/vi/glu0dzK4dbU/1.jpg
    - https://i3.ytimg.com/vi/glu0dzK4dbU/2.jpg
    - https://i3.ytimg.com/vi/glu0dzK4dbU/3.jpg
---
