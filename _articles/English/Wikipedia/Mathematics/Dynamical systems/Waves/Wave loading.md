---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_loading
offline_file: ""
offline_thumbnail: ""
uuid: 0698c292-1329-418f-a543-ea9e1cd0d1f4
updated: 1484308900
title: Wave loading
categories:
    - Waves
---
Wave loading is most commonly the application of a pulsed or wavelike load to a material or object. This is most commonly used in the analysis of piping, ships, or building structures which experience wind, water, or seismic disturbances.
