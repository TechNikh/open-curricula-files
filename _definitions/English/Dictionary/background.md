---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/background
offline_file: ""
offline_thumbnail: ""
uuid: 7efbf0ff-63a2-4c40-800a-9996332cb358
updated: 1484310433
title: background
categories:
    - Dictionary
---
background
     n 1: a person's social heritage: previous experience or training;
          "he is a lawyer with a sports background"
     2: the part of a scene (or picture) that lies behind objects in
        the foreground; "he posed her against a background of
        rolling hills" [syn: {ground}]
     3: information that is essential to understanding a situation
        or problem; "the embassy filled him in on the background
        of the incident" [syn: {background knowledge}]
     4: extraneous signals that can be confused with the phenomenon
        to be observed or measured; "they got a bad connection and
        could hardly hear one another over the background signals"
       ...
