---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/datum
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463901
title: datum
categories:
    - Dictionary
---
datum
     n : an item of factual information derived from measurement or
         research [syn: {data point}]
     [also: {data} (pl)]
