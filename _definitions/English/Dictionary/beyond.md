---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beyond
offline_file: ""
offline_thumbnail: ""
uuid: 9db50bff-4ea5-4749-8e6e-6de990f27566
updated: 1484310226
title: beyond
categories:
    - Dictionary
---
beyond
     adv 1: farther along in space or time or degree; "through the
            valley and beyond"; "to the eighth grade but not
            beyond"; "will be influential in the 1990s and beyond"
     2: on the farther side from the observer; "a pond with a
        hayfield beyond" [syn: {on the far side}]
     3: in addition; "agreed to provide essentials but nothing
        beyond"
