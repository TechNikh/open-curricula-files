---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electro-Mechanical_Modeling
offline_file: ""
offline_thumbnail: ""
uuid: 5c64fcfd-1701-48f0-96a5-979fe87cfdb2
updated: 1484308753
title: Electro-Mechanical Modeling
categories:
    - Electrodynamics
---
The purpose of Electro-Mechanical Modeling is to model and simulate an electro-mechanical system, such that it's physical parameters can be examined before the actual system is built. Parameter estimation and physical realization of the overall system is the major design objective of Electro-Mechanical modeling. Theory driven mathematical model can be used or applied to other system to judge the performance of the joint system as a whole. This is a well known & proven technique for designing large control system for industrial as well as academic multi-disciplinary complex system.
