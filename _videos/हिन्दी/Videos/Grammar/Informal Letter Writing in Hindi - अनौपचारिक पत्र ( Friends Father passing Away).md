---
version: 1
type: video
provider: YouTube
id: 75TBdSCwxJY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b19f051e-c281-4dda-a753-7220b29b5641
updated: 1486069629
title: 'Informal Letter Writing in Hindi - अनौपचारिक पत्र ( Friends Father passing Away)'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/75TBdSCwxJY/default.jpg
    - https://i3.ytimg.com/vi/75TBdSCwxJY/1.jpg
    - https://i3.ytimg.com/vi/75TBdSCwxJY/2.jpg
    - https://i3.ytimg.com/vi/75TBdSCwxJY/3.jpg
---
