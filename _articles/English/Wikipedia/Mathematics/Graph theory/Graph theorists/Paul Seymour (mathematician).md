---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_Seymour_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 2955bc8d-5d9d-4faa-989d-541088387943
updated: 1484309476
title: Paul Seymour (mathematician)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Paul_Seymour.jpeg
tags:
    - Early life
    - Career
    - Personal life
    - Major contributions
categories:
    - Graph theorists
---
Paul Seymour (born July 26, 1950) is currently a professor at Princeton University; half in the department of mathematics and half in the program in applied and computational math. His research interest is in discrete mathematics, especially graph theory. He (with others) was responsible for important progress on regular matroids and totally unimodular matrices, the four colour theorem, linkless embeddings, graph minors and structure, the perfect graph conjecture, the Hadwiger conjecture, and claw-free graphs. Many of his recent papers are available from his website.[1]
