---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invite
offline_file: ""
offline_thumbnail: ""
uuid: eb3fb686-9aa3-4f65-b919-c30ab827aa42
updated: 1484310515
title: invite
categories:
    - Dictionary
---
invite
     n : a colloquial expression for invitation; "he didn't get no
         invite to the party"
     v 1: increase the likelihood of; "ask for trouble"; "invite
          criticism" [syn: {ask for}]
     2: invite someone to one's house; "Can I invite you for dinner
        on Sunday night?" [syn: {ask over}, {ask round}]
     3: give rise to a desire by being attractive or inviting; "the
        window displays tempted the shoppers" [syn: {tempt}]
     4: ask someone in a friendly way to do something [syn: {bid}]
     5: have as a guest; "I invited them to a restaurant" [syn: {pay
        for}]
     6: ask to enter; "We invited the neighbors in for a cup of
        coffee" [syn: ...
