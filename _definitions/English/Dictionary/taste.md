---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taste
offline_file: ""
offline_thumbnail: ""
uuid: 4dac58c2-348d-49a8-9247-4b14b7344d0c
updated: 1484310357
title: taste
categories:
    - Dictionary
---
taste
     n 1: the sensation that results when taste buds in the tongue and
          throat convey information about the chemical composition
          of a soluble stimulus; "the candy left him with a bad
          taste"; "the melon had a delicious taste" [syn: {taste
          sensation}, {gustatory sensation}, {taste perception}, {gustatory
          perception}]
     2: a strong liking; "my own preference is for good literature";
        "the Irish have a penchant for blarney" [syn: {preference},
         {penchant}, {predilection}]
     3: delicate discrimination (especially of aesthetic values);
        "arrogance and lack of taste contributed to his rapid
        success"; "to ask ...
