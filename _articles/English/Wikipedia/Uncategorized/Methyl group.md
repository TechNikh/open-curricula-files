---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Methyl
offline_file: ""
offline_thumbnail: ""
uuid: 2d449680-798c-4d99-9dde-41a07643ef63
updated: 1484308398
title: Methyl group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/110px-Methyl_Group_General_Formulae_V.1.png
tags:
    - Methyl cation, anion, and radical
    - Methyl cation
    - Methyl anion
    - Methyl radical
    - Reactivity
    - Oxidation
    - Methylation
    - Deprotonation
    - Free radical reactions
    - Chiral methyl
    - Etymology
categories:
    - Uncategorized
---
A methyl group is an alkyl derived from methane, containing one carbon atom bonded to three hydrogen atoms — CH3. In formulas, the group is often abbreviated Me. Such hydrocarbon groups occur in many organic compounds. It is a very stable group in most molecules. While the methyl group is usually part of a larger molecule, it can be found on its own in any of three forms: anion, cation or radical. The anion has eight valence electrons, the radical seven and the cation six. All three forms are highly reactive and rarely observed.[1]
