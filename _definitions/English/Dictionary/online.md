---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/online
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484537821
title: online
categories:
    - Dictionary
---
on-line
     adj 1: on a regular route of a railroad or bus or airline system;
            "on-line industries" [ant: {off-line}]
     2: connected to a computer network or accessible by computer;
        "an on-line database" [ant: {off-line}]
     3: being in progress now; "on-line editorial projects" [syn: {on-line(a)}]
