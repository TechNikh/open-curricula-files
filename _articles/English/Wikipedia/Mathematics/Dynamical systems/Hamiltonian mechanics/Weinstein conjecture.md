---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weinstein_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: bd811019-26ac-4d9d-8b98-a1954bdc78ba
updated: 1484308802
title: Weinstein conjecture
categories:
    - Hamiltonian mechanics
---
In mathematics, the Weinstein conjecture refers to a general existence problem for periodic orbits of Hamiltonian or Reeb vector flows. More specifically, the conjecture claims that on a compact contact manifold, its Reeb vector field should carry at least one periodic orbit.
