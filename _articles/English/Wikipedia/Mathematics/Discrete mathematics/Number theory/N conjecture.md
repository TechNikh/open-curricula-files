---
version: 1
type: article
id: https://en.wikipedia.org/wiki/N_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 65d1ae6e-61d3-4e5d-9c05-cda8bcf38979
updated: 1484308654
title: N conjecture
categories:
    - Number theory
---
In number theory the n conjecture is a conjecture stated by Browkin & Brzeziński (1994) as a generalization of the abc conjecture to more than three integers.
