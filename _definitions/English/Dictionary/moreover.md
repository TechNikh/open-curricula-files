---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moreover
offline_file: ""
offline_thumbnail: ""
uuid: b9635699-eaf8-4158-a692-d0193ab21d69
updated: 1484310242
title: moreover
categories:
    - Dictionary
---
moreover
     adv : in addition; "computer chess games are getting cheaper all
           the time; furthermore, their quality is improving";
           "the cellar was dark; moreover, mice nested there";
           "what is more, there's no sign of a change" [syn: {furthermore},
            {what is more}]
