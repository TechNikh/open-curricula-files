---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/installation
offline_file: ""
offline_thumbnail: ""
uuid: 5acf18ae-869f-4e72-930e-f09405619187
updated: 1484310258
title: installation
categories:
    - Dictionary
---
installation
     n 1: the act of installing something (as equipment); "the
          telephone installation took only a few minutes" [syn: {installing},
           {installment}, {instalment}]
     2: a building or place that provides a particular service or is
        used for a particular industry; "the assembly plant is an
        enormous facility" [syn: {facility}]
     3: a formal entry into an organization or position or office;
        "his initiation into the club"; "he was ordered to report
        for induction into the army"; "he gave a speech as part of
        his installation into the hall of fame" [syn: {initiation},
         {induction}]
