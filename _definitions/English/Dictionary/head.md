---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/head
offline_file: ""
offline_thumbnail: ""
uuid: b464190f-09a9-42d1-9479-fc6e68995ca3
updated: 1484310307
title: head
categories:
    - Dictionary
---
head
     n 1: the upper part of the human body or the front part of the
          body in animals; contains the face and brains; "he stuck
          his head out the window" [syn: {caput}]
     2: a single domestic animal; "200 head of cattle"
     3: that which is responsible for one's thoughts and feelings;
        the seat of the faculty of reason; "his mind wandered"; "I
        couldn't get his words out of my head" [syn: {mind}, {brain},
         {psyche}, {nous}]
     4: a person who is in charge; "the head of the whole operation"
        [syn: {chief}, {top dog}]
     5: the front of a military formation or procession; "the head
        of the column advanced boldly"; "they were at ...
