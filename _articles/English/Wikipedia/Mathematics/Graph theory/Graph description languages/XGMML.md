---
version: 1
type: article
id: https://en.wikipedia.org/wiki/XGMML
offline_file: ""
offline_thumbnail: ""
uuid: 5b97e75a-a31e-4e2b-996a-f8fd23402fe7
updated: 1484309413
title: XGMML
categories:
    - Graph description languages
---
XGMML (the eXtensible Graph Markup and Modeling Language) is an XML application based on GML which is used for graph description. Technically, while GML is not related to XML nor SGML, XGMML is an XML application that is so designed that there's a 1:1 relation towards GML for trivial conversion between the two formats.
