---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_law_of_cosines
offline_file: ""
offline_thumbnail: ""
uuid: eb9f638a-98f3-4a6b-a6f0-26d75651a64e
updated: 1484309365
title: Hyperbolic law of cosines
tags:
    - Hyperbolic law of Haversines
categories:
    - Geometry stubs
---
In hyperbolic geometry, the law of cosines is a pair of theorems relating the sides and angles of triangles on a hyperbolic plane, analogous to the planar law of cosines from plane trigonometry, or the spherical law of cosines in spherical trigonometry.
