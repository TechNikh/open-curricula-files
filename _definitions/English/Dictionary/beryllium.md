---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beryllium
offline_file: ""
offline_thumbnail: ""
uuid: b5e162e5-ca8c-4607-8eee-b8cef5db9784
updated: 1484310397
title: beryllium
categories:
    - Dictionary
---
beryllium
     n : a light strong brittle gray toxic bivalent metallic element
         [syn: {Be}, {glucinium}, {atomic number 4}]
