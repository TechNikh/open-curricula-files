---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arthur_Hobbs_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 6ba27c53-a870-4e60-ab5d-3e75892a059d
updated: 1484309463
title: Arthur Hobbs (mathematician)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_15.jpg
tags:
    - Early and personal life
    - Education and early career
    - Academic career
    - Research
    - Publications
categories:
    - Graph theorists
---
