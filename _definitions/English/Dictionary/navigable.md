---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/navigable
offline_file: ""
offline_thumbnail: ""
uuid: e150cada-59ee-4d49-84b5-bf0ca0bab993
updated: 1484310461
title: navigable
categories:
    - Dictionary
---
navigable
     adj : able to be sailed on or through safely; "navigable waters";
           "a navigable channel"
