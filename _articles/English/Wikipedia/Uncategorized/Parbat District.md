---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parbat
offline_file: ""
offline_thumbnail: ""
uuid: 0ac14bd2-54a1-4c1e-b4a1-dffbda9513ed
updated: 1484308426
title: Parbat District
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-NepalParbatDistrictmap.png
tags:
    - Geography and Climate
    - Village development committees (VDCs) and municipalities
categories:
    - Uncategorized
---
Parbat District (Nepali: पर्वत जिल्ला Listen (help·info), is a hilly area of Nepal. It is a part of Province No. 4 and one of the seventy-five districts of Nepal, a landlocked country of South Asia. The district, with Kusma as its district headquarters, covers an area of 494 km² and has a population (2001) of 157,826. It is the fourth smallest district of Nepal with 47 VDCs currently (before Kushma Municipality was formed, total VDCs remained 55.). It is mainly famous for the Gupteshwar Cave, which is visited by thousands of pilgrims during Shivaratri. Patheshwari Temple is the most famous temple of Kushma located at Katuwa Chaupari of Kushma-13. Patheshwori ...
