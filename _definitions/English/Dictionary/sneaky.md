---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sneaky
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414281
title: sneaky
categories:
    - Dictionary
---
sneaky
     adj 1: marked by deception; "achieved success in business only by
            underhand methods" [syn: {underhand}, {underhanded}]
     2: marked by quiet and caution and secrecy; taking pains to
        avoid being observed; "a furtive manner"; "a lurking
        prowler"; "a sneak attack"; "stealthy footsteps"; "a
        surreptitious glance at his watch"; "someone skulking in
        the shadows" [syn: {furtive}, {lurking}, {skulking}, {sneak(a)},
         {stealthy}, {surreptitious}]
