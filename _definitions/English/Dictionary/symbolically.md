---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symbolically
offline_file: ""
offline_thumbnail: ""
uuid: 86ec82a7-a4b8-416e-a821-f3344d476cb4
updated: 1484310299
title: symbolically
categories:
    - Dictionary
---
symbolically
     adv 1: in a symbolic manner; "symbolically accepted goals"
     2: by means of symbols; "symbolically expressed"
