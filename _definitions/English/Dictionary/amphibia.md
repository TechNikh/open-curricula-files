---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amphibia
offline_file: ""
offline_thumbnail: ""
uuid: 50b8a82f-31d1-46cb-b3ab-915c97a368db
updated: 1484310163
title: amphibia
categories:
    - Dictionary
---
Amphibia
     n : frogs; toads; newts; salamanders; caecilians [syn: {class
         Amphibia}]
