---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lethal
offline_file: ""
offline_thumbnail: ""
uuid: 2120caf8-e823-4a2f-817c-326737712465
updated: 1484310541
title: lethal
categories:
    - Dictionary
---
lethal
     adj : of an instrument of certain death; "deadly poisons"; "lethal
           weapon"; "a lethal injection" [syn: {deadly}]
