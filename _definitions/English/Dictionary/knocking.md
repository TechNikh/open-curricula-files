---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knocking
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448722
title: knocking
categories:
    - Dictionary
---
knocking
     n : the sound of knocking (as on a door or in an engine or
         bearing); "the knocking grew louder" [syn: {knock}]
