---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bile
offline_file: ""
offline_thumbnail: ""
uuid: f28041bc-6d0d-4d40-8ae9-bac2cb4fb5e4
updated: 1484310325
title: bile
categories:
    - Dictionary
---
bile
     n : a digestive juice secreted by the liver and stored in the
         gallbladder; aids in the digestion of fats [syn: {gall}]
