---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fire_point
offline_file: ""
offline_thumbnail: ""
uuid: 7a242a60-c535-4e59-809b-2246f426e413
updated: 1484308846
title: Fire point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Large_bonfire_0.jpg
categories:
    - Thermodynamics
---
The fire point of a fuel is the lowest temperature at which the vapour of that fuel will continue to burn for at least 5 seconds after ignition by an open flame. At the flash point, a lower temperature, a substance will ignite briefly, but vapor might not be produced at a rate to sustain the fire. Most tables of material properties will only list material flash points. Although in general the fire points can be assumed to be about 10 °C higher than the flash points[1] this is no substitute for testing if the fire point is safety critical.[1]
