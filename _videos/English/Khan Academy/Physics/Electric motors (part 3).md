---
version: 1
type: video
provider: YouTube
id: XMkUDyl1ZRo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Electric%20motors%20%28part%203%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b1e3fcb9-f4c6-4aae-8b45-ed0e90adcc78
updated: 1486069591
title: Electric motors (part 3)
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/XMkUDyl1ZRo/default.jpg
    - https://i3.ytimg.com/vi/XMkUDyl1ZRo/1.jpg
    - https://i3.ytimg.com/vi/XMkUDyl1ZRo/2.jpg
    - https://i3.ytimg.com/vi/XMkUDyl1ZRo/3.jpg
---
Sal finishes the explanation of how a commutator will allow a loop of wire to continue spinning in a magnetic field, thereby allowing it to work as an electric motor. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/electric-motors/v/the-dot-product?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/electric-motors/v/magnetism-10-electric-motors?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
