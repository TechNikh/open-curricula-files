---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coo
offline_file: ""
offline_thumbnail: ""
uuid: a7e0e7bc-bbfa-4c26-9413-acd49078cc82
updated: 1484310424
title: coo
categories:
    - Dictionary
---
coo
     n : the sound made by a pigeon
     v 1: speak softly or lovingly; "The mother who held her baby was
          cooing softly"
     2: cry softly, as of pigeons
