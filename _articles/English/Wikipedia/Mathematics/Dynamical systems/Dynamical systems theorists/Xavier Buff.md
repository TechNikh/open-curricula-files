---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Xavier_Buff
offline_file: ""
offline_thumbnail: ""
uuid: f0ae98bc-5f64-4f22-a5cf-fbc1956b4a3c
updated: 1484308690
title: Xavier Buff
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Xavier_buff.jpg
categories:
    - Dynamical systems theorists
---
Buff received in 1996 his Ph.D. (promotion) from the University of Paris-Sud under Adrien Douady with thesis Points fixe de renormalisation.[1] As a postdoc he was in the academic year 1997–1998 the H. C. Wang Assistant Professor at Cornell University. At the Paul Sabatier University (Université Toulouse III) he became in 1998 a maître de conférences, achieved in 2006 his habilitation with habilitation thesis Disques de Siegel et ensembles de Julia d'aire strictement positive, and became in 2008 a full professor.[2]
