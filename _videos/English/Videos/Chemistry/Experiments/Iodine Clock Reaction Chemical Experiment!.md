---
version: 1
type: video
provider: YouTube
id: OPTdAmSx4m4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c5b497af-aa85-4a56-9eaa-cd3df744a8ed
updated: 1486069661
title: Iodine Clock Reaction Chemical Experiment!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/OPTdAmSx4m4/default.jpg
    - https://i3.ytimg.com/vi/OPTdAmSx4m4/1.jpg
    - https://i3.ytimg.com/vi/OPTdAmSx4m4/2.jpg
    - https://i3.ytimg.com/vi/OPTdAmSx4m4/3.jpg
---
