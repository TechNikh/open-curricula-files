---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precondition
offline_file: ""
offline_thumbnail: ""
uuid: 583481c0-ef87-46cc-86f2-c9d112137c23
updated: 1484310461
title: precondition
categories:
    - Dictionary
---
precondition
     n 1: an assumption on which rests the validity or effect of
          something else [syn: {condition}, {stipulation}]
     2: an assumption that is taken for granted [syn: {given}, {presumption}]
     3: a condition that is a prerequisite
     v : put into the required condition beforehand
