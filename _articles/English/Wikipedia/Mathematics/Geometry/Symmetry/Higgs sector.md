---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Higgs_sector
offline_file: ""
offline_thumbnail: ""
uuid: e11a7e75-4b6a-4db0-8d77-3b0af6f55451
updated: 1484309312
title: Higgs sector
categories:
    - Symmetry
---
In particle physics, the Higgs sector is the collection of quantum fields and/or particles that are responsible for the Higgs mechanism, i.e. for the spontaneous symmetry breaking of the Higgs field. The word "sector" refers to a subgroup of the total set of fields and particles. [1]
