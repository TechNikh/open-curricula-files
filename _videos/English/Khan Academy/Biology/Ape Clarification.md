---
version: 1
type: video
provider: YouTube
id: oFGkYA_diDA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Ape%20Clarification.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d653633a-4af2-43a0-ae6e-5039496eab67
updated: 1486069581
title: Ape Clarification
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/oFGkYA_diDA/default.jpg
    - https://i3.ytimg.com/vi/oFGkYA_diDA/1.jpg
    - https://i3.ytimg.com/vi/oFGkYA_diDA/2.jpg
    - https://i3.ytimg.com/vi/oFGkYA_diDA/3.jpg
---
Ape Clarification - that they have no tails
More free lessons at: http://www.khanacademy.org/video?v=oFGkYA_diDA
