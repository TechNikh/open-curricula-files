---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hostage
offline_file: ""
offline_thumbnail: ""
uuid: 8a1b8045-fad8-4b5c-8ebf-2756c941b35d
updated: 1484310181
title: hostage
categories:
    - Dictionary
---
hostage
     n : a prisoner who is held by one party to insure that another
         party will meet specified terms [syn: {surety}]
