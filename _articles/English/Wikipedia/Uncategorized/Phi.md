---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phi
offline_file: ""
offline_thumbnail: ""
uuid: f3031580-8c92-45bb-a374-0dadad9bd0e2
updated: 1484308319
title: Phi
tags:
    - Use as a symbol
    - Computing
categories:
    - Uncategorized
---
Phi (uppercase Φ, lowercase  or ; Ancient Greek: ϕεῖ, pheî, [pʰé͜e]; modern Greek: φι, fi, [fi]; English: /faɪ/[1]) is the 21st letter of the Greek alphabet. In Ancient Greek, it represented an aspirated voiceless bilabial plosive ([pʰ]), which was the origin of its usual romanization as "ph". In modern Greek, it represents a voiceless labiodental fricative ([f]) and is correspondingly romanized as "f". Its origin is uncertain but it may be that phi originated as the letter qoppa and initially represented the sound /kʷʰ/ before shifting to Classical Greek [pʰ].[2] In traditional Greek numerals, phi has a value of 500 (φʹ) or 500 000 (͵φ). The Cyrillic letter Ef (Ф, ...
