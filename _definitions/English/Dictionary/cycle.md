---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cycle
offline_file: ""
offline_thumbnail: ""
uuid: 16db4d13-954a-400e-88c6-40b9746ecb9d
updated: 1484310337
title: cycle
categories:
    - Dictionary
---
cycle
     n 1: an interval during which a recurring sequence of events
          occurs; "the neverending cycle of the seasons" [syn: {rhythm},
           {round}]
     2: a series of poems or songs on the same theme; "schubert's
        song cycles"
     3: a periodically repeated sequence of events; "a cycle of
        reprisal and retaliation"
     4: the unit of frequency; one Hertz has a periodic interval of
        one second [syn: {Hertz}, {Hz}, {cycle per second}, {cycles/second},
         {cps}]
     5: a single complete execution of a periodically repeated
        phenomenon; "a year constitutes a cycle of the seasons"
        [syn: {oscillation}]
     6: a wheeled vehicle that ...
