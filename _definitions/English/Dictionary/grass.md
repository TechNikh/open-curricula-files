---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grass
offline_file: ""
offline_thumbnail: ""
uuid: e7633789-126b-4896-9b73-ebcff59ebff2
updated: 1484310309
title: grass
categories:
    - Dictionary
---
grass
     n 1: narrow-leaved green herbage: grown as lawns; used as pasture
          for grazing animals; cut and dried as hay
     2: German writer of novels and poetry and plays (born 1927)
        [syn: {Gunter Grass}, {Gunter Wilhelm Grass}]
     3: animal food for browsing or grazing [syn: {eatage}, {forage},
         {pasture}, {pasturage}]
     4: street names for marijuana [syn: {pot}, {green goddess}, {dope},
         {weed}, {gage}, {sess}, {sens}, {smoke}, {skunk}, {locoweed},
         {Mary Jane}]
     v 1: shoot down, of birds
     2: cover with grass; "The owners decided to grass their
        property"
     3: spread out clothes on the grass to let it dry and bleach
     4: ...
