---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Saccheri%E2%80%93Legendre_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5dddd646-196a-41d5-b5ad-c09eaea0bc63
updated: 1484309127
title: Saccheri–Legendre theorem
categories:
    - Theorems in geometry
---
In absolute geometry, the Saccheri–Legendre theorem states that the sum of the angles in a triangle is at most 180°.[1] Absolute geometry is the geometry obtained from assuming all the axioms that lead to Euclidean geometry with the exception of the axiom that is equivalent to the parallel postulate of Euclid.[2]
