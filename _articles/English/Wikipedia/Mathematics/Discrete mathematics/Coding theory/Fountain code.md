---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fountain_code
offline_file: ""
offline_thumbnail: ""
uuid: 7bcbd896-dbea-4318-8429-48417f470a92
updated: 1484308547
title: Fountain code
tags:
    - Applications
    - Fountain codes in standards
    - Fountain codes for data storage
    - Notes
categories:
    - Coding theory
---
In coding theory, fountain codes (also known as rateless erasure codes) are a class of erasure codes with the property that a potentially limitless sequence of encoding symbols can be generated from a given set of source symbols such that the original source symbols can ideally be recovered from any subset of the encoding symbols of size equal to or only slightly larger than the number of source symbols. The term fountain or rateless refers to the fact that these codes do not exhibit a fixed code rate.
