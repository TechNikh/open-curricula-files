---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maria_Chudnovsky
offline_file: ""
offline_thumbnail: ""
uuid: 69844a05-1e9f-4ba3-9b3b-ba8c55d8449e
updated: 1484309459
title: Maria Chudnovsky
tags:
    - Biography
    - Research
    - Selected publications
    - Awards and honors
categories:
    - Graph theorists
---
Maria Chudnovsky (born January 6, 1977) is an Israeli-American mathematician working on graph theory and combinatorial optimization.[1] She is a 2012 MacArthur Fellow.[2]
