---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/millions
offline_file: ""
offline_thumbnail: ""
uuid: a1ce1b9e-7d08-4935-a079-fa1a9bc50f5e
updated: 1484310416
title: millions
categories:
    - Dictionary
---
millions
     n : a very large indefinite number (usually hyperbole) [syn: {billions},
          {trillions}, {zillions}, {jillions}]
