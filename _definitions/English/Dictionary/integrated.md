---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integrated
offline_file: ""
offline_thumbnail: ""
uuid: 6020e37d-44c0-466a-b4b1-24289efa6fc5
updated: 1484310200
title: integrated
categories:
    - Dictionary
---
integrated
     adj 1: formed or united into a whole [syn: {incorporate}, {incorporated},
             {merged}, {unified}]
     2: formed into a whole or introduced into another entity; "a
        more closely integrated economic and political system"-
        Dwight D.Eisenhower; "an integrated Europe" [ant: {nonintegrated}]
     3: not segregated; designated as available to all races or
        groups; "integrated schools" [ant: {segregated}]
     4: resembling a living organism in organization or development;
        "society as an integrated whole" [syn: {structured}]
     5: caused to combine or unite [syn: {amalgamated}, {intermingled},
         {mixed}]
