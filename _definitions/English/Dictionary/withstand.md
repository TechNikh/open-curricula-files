---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/withstand
offline_file: ""
offline_thumbnail: ""
uuid: 37618219-33a9-4938-ade3-ec1fea872da5
updated: 1484310181
title: withstand
categories:
    - Dictionary
---
withstand
     v 1: resist or confront with resistance; "The politician defied
          public opinion"; "The new material withstands even the
          greatest wear and tear"; "The bridge held" [syn: {defy},
           {hold}, {hold up}]
     2: stand up or offer resistance to somebody or something [syn:
        {resist}, {hold out}, {stand firm}] [ant: {surrender}]
     [also: {withstood}]
