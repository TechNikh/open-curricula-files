---
version: 1
type: video
provider: YouTube
id: vKGrvgPGXe4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3a22ffea-97d1-440e-bd98-c3f7159991b7
updated: 1486069616
title: Comparing with multiplication basic exercises
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/vKGrvgPGXe4/default.jpg
    - https://i3.ytimg.com/vi/vKGrvgPGXe4/1.jpg
    - https://i3.ytimg.com/vi/vKGrvgPGXe4/2.jpg
    - https://i3.ytimg.com/vi/vKGrvgPGXe4/3.jpg
---
