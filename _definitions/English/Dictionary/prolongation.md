---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prolongation
offline_file: ""
offline_thumbnail: ""
uuid: f456efe6-5077-4a65-a489-2fa8f35e3191
updated: 1484310579
title: prolongation
categories:
    - Dictionary
---
prolongation
     n 1: the act of prolonging something; "there was an indefinite
          prolongation of the peace talks" [syn: {protraction}, {perpetuation},
           {lengthening}]
     2: amount or degree or range to which something extends; "the
        wire has an extension of 50 feet" [syn: {extension}, {lengthiness}]
     3: the consequence of being lengthened in duration [syn: {lengthiness},
         {continuation}, {protraction}]
