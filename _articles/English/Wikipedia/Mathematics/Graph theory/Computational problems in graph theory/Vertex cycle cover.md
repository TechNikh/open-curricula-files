---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex_cycle_cover
offline_file: ""
offline_thumbnail: ""
uuid: f60932e4-7f07-4116-98d8-b83bcd457f87
updated: 1484309396
title: Vertex cycle cover
tags:
    - Properties and applications
    - Permanent
    - Minimal disjoint cycle covers
categories:
    - Computational problems in graph theory
---
In mathematics, a vertex cycle cover (commonly called simply cycle cover) of a graph G is a set of cycles which are subgraphs of G and contain all vertices of G.
