---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moment
offline_file: ""
offline_thumbnail: ""
uuid: 7b365fa6-c4fd-497f-8b6b-a54f573e92b2
updated: 1484310341
title: moment
categories:
    - Dictionary
---
moment
     n 1: a particular point in time; "the moment he arrived the party
          began" [syn: {minute}, {second}, {instant}]
     2: an indefinitely short time; "wait just a moment"; "it only
        takes a minute"; "in just a bit" [syn: {minute}, {second},
         {bit}]
     3: at this time; "the disappointments of the here and now";
        "she is studying at the moment" [syn: {here and now}, {present
        moment}]
     4: having important effects or influence; "decisions of great
        consequence are made by the president himself"; "virtue is
        of more moment that security" [syn: {consequence}, {import}]
        [ant: {inconsequence}]
     5: the moment of a couple ...
