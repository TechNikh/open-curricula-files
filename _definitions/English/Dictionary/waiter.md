---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waiter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484488141
title: waiter
categories:
    - Dictionary
---
waiter
     n 1: a person whose occupation is to serve at table (as in a
          restaurant) [syn: {server}]
     2: a person who waits or awaits
