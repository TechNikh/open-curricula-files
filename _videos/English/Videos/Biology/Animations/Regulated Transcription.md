---
version: 1
type: video
provider: YouTube
id: vi-zWoobt_Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Regulated%20Transcription.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b0901eb5-f9b6-4c64-8456-7d62cfd05a36
updated: 1486069669
title: Regulated Transcription
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vi-zWoobt_Q/default.jpg
    - https://i3.ytimg.com/vi/vi-zWoobt_Q/1.jpg
    - https://i3.ytimg.com/vi/vi-zWoobt_Q/2.jpg
    - https://i3.ytimg.com/vi/vi-zWoobt_Q/3.jpg
---
