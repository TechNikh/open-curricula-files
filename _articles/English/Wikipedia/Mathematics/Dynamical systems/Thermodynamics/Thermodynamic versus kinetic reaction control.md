---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Thermodynamic_versus_kinetic_reaction_control
offline_file: ""
offline_thumbnail: ""
uuid: 56108b62-7ad8-4b1a-b423-221e00d4dfec
updated: 1484308870
title: Thermodynamic versus kinetic reaction control
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Thermodyamic_versus_kinetic_control.png
tags:
    - Scope
    - In Diels-Alder reactions
    - In enolate chemistry
    - In electrophilic additions
    - characteristics
    - History
categories:
    - Thermodynamics
---
Thermodynamic reaction control or kinetic reaction control in a chemical reaction can decide the composition in a reaction product mixture when competing pathways lead to different products and the reaction conditions influence the selectivity or stereoselectivity. The distinction is relevant when product A forms faster than product B because the activation energy for product A is lower than that for product B, yet product B is more stable. In such a case A is the kinetic product and is favoured under kinetic control and B is the thermodynamic product and is favoured under thermodynamic control.[1][2][3]
