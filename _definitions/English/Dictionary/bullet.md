---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bullet
offline_file: ""
offline_thumbnail: ""
uuid: b87a4319-695f-4339-89d6-0b19d49f6d76
updated: 1484310486
title: bullet
categories:
    - Dictionary
---
bullet
     n 1: a projectile that is fired from a gun [syn: {slug}]
     2: a high-speed passenger train [syn: {bullet train}]
     3: (baseball) a pitch thrown with maximum velocity; "he swung
        late on the fastball"; "he showed batters nothing but
        smoke" [syn: {fastball}, {heater}, {smoke}, {hummer}]
