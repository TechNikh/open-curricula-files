---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/powder
offline_file: ""
offline_thumbnail: ""
uuid: a26023eb-983a-404d-b8cb-623a4cd7e3fb
updated: 1484310346
title: powder
categories:
    - Dictionary
---
powder
     n 1: a solid substance in the form of tiny loose particles; a
          solid that has been pulverized [syn: {pulverization}, {pulverisation}]
     2: a mixture of potassium nitrate, charcoal, and sulfur in a
        75:15:10 ratio which is used in gunnery, time fuses, and
        fireworks [syn: {gunpowder}]
     3: any of various cosmetic or medical preparations dispensed in
        the form of a powder
     v 1: apply powder to; "She powdered her nose"; "The King wears a
          powdered wig"
     2: make into a powder by breaking up or cause to become dust;
        "pulverize the grains" [syn: {powderize}, {powderise}, {pulverize},
         {pulverise}]
