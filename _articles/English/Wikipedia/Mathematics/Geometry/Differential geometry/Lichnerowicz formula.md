---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lichnerowicz_formula
offline_file: ""
offline_thumbnail: ""
uuid: b3efda5f-eed1-4e1c-8653-5afd46e25215
updated: 1484309226
title: Lichnerowicz formula
categories:
    - Differential geometry
---
The Lichnerowicz formula (also known as the Lichnerowicz–Weitzenböck formula) is a fundamental equation in the analysis of spinors on pseudo-Riemannian manifolds. In dimension 4, it forms a piece of Seiberg–Witten theory and other aspects of gauge theory. It is named after noted mathematicians André Lichnerowicz who proved it in 1963, and Roland Weitzenböck. The formula gives a relationship between the Dirac operator and the Laplace–Beltrami operator acting on spinors, in which the scalar curvature appears in a natural way. The result is significant because it provides an interface between results from the study of elliptic partial differential equations, results concerning the ...
