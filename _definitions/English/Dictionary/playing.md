---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/playing
offline_file: ""
offline_thumbnail: ""
uuid: b2a32e56-7945-48a0-b0fb-d2fae6c0adfe
updated: 1484310527
title: playing
categories:
    - Dictionary
---
playing
     n 1: the act of playing a musical instrument
     2: the action of taking part in a game or sport or other
        recreation
     3: the performance of a part or role in a drama [syn: {acting},
         {playacting}, {performing}]
