---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consistently
offline_file: ""
offline_thumbnail: ""
uuid: 0e906431-d9d5-47db-8763-f0ca8acc3d0a
updated: 1484310467
title: consistently
categories:
    - Dictionary
---
consistently
     adv : in a systematic or consistent manner; "they systematically
           excluded women" [syn: {systematically}] [ant: {unsystematically},
            {inconsistently}]
