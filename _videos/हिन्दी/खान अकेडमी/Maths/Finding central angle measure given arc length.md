---
version: 1
type: video
provider: YouTube
id: Xae2vDjghTk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 12e1deed-c1ae-454c-90c6-746e8703d8d8
updated: 1486069616
title: Finding central angle measure given arc length
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xae2vDjghTk/default.jpg
    - https://i3.ytimg.com/vi/Xae2vDjghTk/1.jpg
    - https://i3.ytimg.com/vi/Xae2vDjghTk/2.jpg
    - https://i3.ytimg.com/vi/Xae2vDjghTk/3.jpg
---
