---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extinct
offline_file: ""
offline_thumbnail: ""
uuid: b5b9d2bb-3d81-4448-adab-5bf4355a668a
updated: 1484310283
title: extinct
categories:
    - Dictionary
---
extinct
     adj 1: no longer in existence; lost or especially having died out
            leaving no living representatives; "an extinct species
            of fish"; "an extinct royal family"; "extinct laws and
            customs" [syn: {nonextant}] [ant: {extant}]
     2: of e.g. volcanos; permanently inactive; "an extinct volcano"
        [syn: {inactive}] [ant: {active}, {dormant}]
     3: of a fire; being out or having grown cold; "threw his
        extinct cigarette into the stream";  "the fire is out"
        [syn: {out(p)}]
