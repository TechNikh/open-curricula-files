---
version: 1
type: video
provider: YouTube
id: udG9KhNMKJw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Exactly%20three%20heads%20in%20five%20flips.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 61280791-bc09-47ca-8d05-fdc7332dfb17
updated: 1486069688
title: Exactly three heads in five flips
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/udG9KhNMKJw/default.jpg
    - https://i3.ytimg.com/vi/udG9KhNMKJw/1.jpg
    - https://i3.ytimg.com/vi/udG9KhNMKJw/2.jpg
    - https://i3.ytimg.com/vi/udG9KhNMKJw/3.jpg
---
