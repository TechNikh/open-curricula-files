---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Col_(game)
offline_file: ""
offline_thumbnail: ""
uuid: ef8b9f71-6f6b-404a-8d89-6813dfb599a6
updated: 1484309020
title: Col (game)
tags:
    - Example game
    - Snort
    - Analysis
categories:
    - Combinatorial game theory
---
Col is a pencil and paper game, specifically a map-coloring game, involving the shading of areas in a line drawing according to the rules of Graph coloring. With each move, the graph must remain proper (no two areas of the same colour may touch), and a player who cannot make a legal move loses. The game was described and analysed by John Conway, who attributed it to Colin Vout, in On Numbers and Games.[1]
