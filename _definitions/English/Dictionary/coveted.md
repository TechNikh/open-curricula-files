---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coveted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484625902
title: coveted
categories:
    - Dictionary
---
coveted
     adj : greatly desired [syn: {desired}, {in demand(p)}, {sought
           after}]
