---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dynamo
offline_file: ""
offline_thumbnail: ""
uuid: 262a3350-11c7-4004-8e4e-19d1be06ec9d
updated: 1484310194
title: dynamo
categories:
    - Dictionary
---
dynamo
     n : generator consisting of a coil (the armature) that rotates
         between the poles of an electromagnet (the field magnet)
         causing a current to flow in the armature
