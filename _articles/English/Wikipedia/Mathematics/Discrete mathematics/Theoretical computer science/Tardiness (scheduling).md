---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tardiness_(scheduling)
offline_file: ""
offline_thumbnail: ""
uuid: 6db66120-8692-4875-b8da-c40404ebb795
updated: 1484308681
title: Tardiness (scheduling)
categories:
    - Theoretical computer science
---
In scheduling, tardiness is a measure of a delay in executing certain operations and earliness is a measure of finishing operations before due time. The operations may depend on each other and on the availability of equipment to perform them.
