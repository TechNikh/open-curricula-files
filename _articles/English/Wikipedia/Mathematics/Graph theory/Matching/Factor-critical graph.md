---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Factor-critical_graph
offline_file: ""
offline_thumbnail: ""
uuid: ccc3fc51-20b6-4fef-86a7-b38db9608edc
updated: 1484309426
title: Factor-critical graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Factor_critical.svg.png
tags:
    - Examples
    - Characterizations
    - Properties
    - Applications
    - Generalizations and related concepts
categories:
    - Matching
---
In graph theory, a mathematical discipline, a factor-critical graph (or hypomatchable graph[1][2]) is a graph with n vertices in which every subgraph of n − 1 vertices has a perfect matching. (A perfect matching in a graph is a subset of its edges with the property that each of its vertices is the endpoint of exactly one of the edges in the subset.)
