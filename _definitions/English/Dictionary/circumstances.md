---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circumstances
offline_file: ""
offline_thumbnail: ""
uuid: ef386cb1-1b29-41eb-8236-649667344140
updated: 1484310393
title: circumstances
categories:
    - Dictionary
---
circumstances
     n 1: your overall circumstances or condition in life (including
          everything that happens to you); "whatever my fortune
          may be"; "deserved a better fate"; "has a happy lot";
          "the luck of the Irish"; "a victim of circumstances";
          "success that was her portion" [syn: {fortune}, {destiny},
           {fate}, {luck}, {lot}, {portion}]
     2: the state (usually personal) with regard to wealth; "each
        person was helped according to his circumstances"
     3: a person's financial situation (good or bad); "he found
        himself in straitened circumstances"
