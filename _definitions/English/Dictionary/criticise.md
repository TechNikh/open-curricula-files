---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/criticise
offline_file: ""
offline_thumbnail: ""
uuid: dca06a25-c3f4-4b8a-a60f-dfc759fa742b
updated: 1484310595
title: criticise
categories:
    - Dictionary
---
criticise
     v 1: find fault with; express criticism of; point out real or
          perceived flaws; "The paper criticized the new movie";
          "Don't knock the food--it's free" [syn: {knock}, {criticize},
           {pick apart}] [ant: {praise}]
     2: act as a critic; "Those who criticize others often are not
        perfect, either" [syn: {criticize}]
