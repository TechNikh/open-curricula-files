---
version: 1
type: video
provider: YouTube
id: XGGd3NuAntc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/16.9%20Lymphocyte%20Homing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e0683983-aab0-43b8-9776-a1373c0c1a98
updated: 1486069674
title: 16.9 Lymphocyte Homing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/XGGd3NuAntc/default.jpg
    - https://i3.ytimg.com/vi/XGGd3NuAntc/1.jpg
    - https://i3.ytimg.com/vi/XGGd3NuAntc/2.jpg
    - https://i3.ytimg.com/vi/XGGd3NuAntc/3.jpg
---
