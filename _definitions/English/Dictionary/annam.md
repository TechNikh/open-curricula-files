---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annam
offline_file: ""
offline_thumbnail: ""
uuid: 20b805eb-5d66-4e27-ac3e-d38359199a75
updated: 1484310575
title: annam
categories:
    - Dictionary
---
Annam
     n : a communist state in Indochina on the South China Sea;
         achieved independence from France in 1945 [syn: {Vietnam},
          {Socialist Republic of Vietnam}, {Viet Nam}]
