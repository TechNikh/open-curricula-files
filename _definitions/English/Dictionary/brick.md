---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brick
offline_file: ""
offline_thumbnail: ""
uuid: 8eedc0be-e895-493f-89c0-eed25cd7909a
updated: 1484310486
title: brick
categories:
    - Dictionary
---
brick
     n 1: rectangular block of clay baked by the sun or in a kiln;
          used as a building or paving material
     2: a good fellow; helpful and trustworthy
