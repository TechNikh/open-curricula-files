---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nozzle_and_flapper
offline_file: ""
offline_thumbnail: ""
uuid: 9b320aaa-819a-47cc-a06e-83ae9b1cfed0
updated: 1484308739
title: Nozzle and flapper
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nozzle_and_flapper_open_and_closed_loop.png
categories:
    - Control engineering
---
The nozzle and flapper mechanism is a displacement type detector which converts mechanical movement into a pressure signal, by covering the orifice of a nozzle with a flat plate called the flapper.[1] This restricts the nozzle fluid flow and generates a pressure signal.
