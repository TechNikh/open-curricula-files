---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harappa
offline_file: ""
offline_thumbnail: ""
uuid: 45742b22-0d8c-47a9-bb1c-eed3790e4e81
updated: 1484308347
title: Harappa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-IVC-major-sites-2.jpg
tags:
    - History
    - Culture and economy
    - Archaeology
    - Early symbols similar to Indus script
    - Notes
categories:
    - Uncategorized
---
Harappa (Punjabi pronunciation: [ɦəɽəppaː]; Urdu/Punjabi: ہڑپّہا) is an archaeological site in Punjab, Pakistan, about 24 km (15 mi) west of Sahiwal. The site takes its name from a modern village located near the former course of the Ravi River. The current village of Harappa is 6 km (3.7 mi) from the ancient site. Although modern Harappa has a legacy railway station from the period of the British Raj, it is today just a small crossroads town of population 15,000.
