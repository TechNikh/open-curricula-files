---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A9vy_hierarchy'
offline_file: ""
offline_thumbnail: ""
uuid: 441eddf4-02ec-4018-96b7-e4d1ee6bec8e
updated: 1484309504
title: Lévy hierarchy
tags:
    - Definitions
    - Examples
    - Σ0=Π0=Δ0 formulas and concepts
    - Δ1-formulas and concepts
    - Σ1-formulas and concepts
    - Π1-formulas and concepts
    - Δ2-formulas and concepts
    - Σ2-formulas and concepts
    - Π2-formulas and concepts
    - Δ3-formulas and concepts
    - Σ3-formulas and concepts
    - Π3-formulas and concepts
    - Σ4-formulas and concepts
    - Properties
categories:
    - Set theory
---
In set theory and mathematical logic, the Lévy hierarchy, introduced by Azriel Lévy in 1965, is a hierarchy of formulas in the formal language of the Zermelo–Fraenkel set theory, which is typically called just the language of set theory. This is analogous to the arithmetical hierarchy which provides the classifications but for sentences of the language of arithmetic.
