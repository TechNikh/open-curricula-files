---
version: 1
type: video
provider: YouTube
id: 7G_5JXe9LzI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5d630869-4940-4f38-a499-c58f1c798efd
updated: 1486069614
title: Counting 1 exercise
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/7G_5JXe9LzI/default.jpg
    - https://i3.ytimg.com/vi/7G_5JXe9LzI/1.jpg
    - https://i3.ytimg.com/vi/7G_5JXe9LzI/2.jpg
    - https://i3.ytimg.com/vi/7G_5JXe9LzI/3.jpg
---
