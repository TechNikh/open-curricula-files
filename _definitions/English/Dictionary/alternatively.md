---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alternatively
offline_file: ""
offline_thumbnail: ""
uuid: ba81e4d2-cb4f-4d23-8c52-168041775b60
updated: 1484310369
title: alternatively
categories:
    - Dictionary
---
alternatively
     adv : in place of, or as an alternative to; "Felix became a
           herpetologist instead"; "alternatively we could buy a
           used car" [syn: {as an alternative}, {instead}, {or
           else}]
