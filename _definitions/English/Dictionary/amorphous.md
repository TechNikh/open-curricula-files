---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amorphous
offline_file: ""
offline_thumbnail: ""
uuid: 4eb4a8f5-0f19-43f7-803f-cc51e6033a35
updated: 1484310414
title: amorphous
categories:
    - Dictionary
---
amorphous
     adj 1: having no definite form or distinct shape; "amorphous clouds
            of insects"; "an aggregate of formless particles"; "a
            shapeless mass of protoplasm" [syn: {formless}, {shapeless}]
     2: lacking the system or structure characteristic of living
        bodies [syn: {unstructured}]
     3: without real or apparent crystalline form; "an amorphous
        mineral"; "amorphous structure" [syn: {uncrystallized}, {uncrystallised}]
