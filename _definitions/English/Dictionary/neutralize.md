---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neutralize
offline_file: ""
offline_thumbnail: ""
uuid: 7e3af33b-87aa-4987-b857-7cfb063dbb43
updated: 1484310379
title: neutralize
categories:
    - Dictionary
---
neutralize
     v 1: make politically neutral and thus inoffensive; "The treaty
          neutralized the small republic"
     2: make ineffective by counterbalancing the effect of; "Her
        optimism neutralizes his gloom"; "This action will negate
        the effect of my efforts" [syn: {neutralise}, {nullify}, {negate}]
     3: oppose and mitigate the effects of by contrary actions;
        "This will counteract the foolish actions of my
        colleagues" [syn: {counteract}, {countervail}, {counterbalance}]
     4: get rid of (someone who may be a threat) by killing; "The
        mafia liquidated the informer"; "the double agent was
        neutralized" [syn: {neutralise}, ...
