---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ketone
offline_file: ""
offline_thumbnail: ""
uuid: e2a06174-19b6-4bf4-9b50-5791c3503751
updated: 1484310419
title: ketone
categories:
    - Dictionary
---
ketone
     n : any of a class of organic compounds having a carbonyl group
         linked to a carbon atom in each of two hydrocarbon
         radicals
