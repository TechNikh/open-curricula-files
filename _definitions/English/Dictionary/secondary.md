---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secondary
offline_file: ""
offline_thumbnail: ""
uuid: dd35ac4d-95c3-4b61-9aae-91e63578b642
updated: 1484310275
title: secondary
categories:
    - Dictionary
---
secondary
     adj 1: of second rank or importance or value; not direct or
            immediate; "the stone will be hauled to a secondary
            crusher"; "a secondary source"; "a secondary issue";
            "secondary streams" [ant: {primary}]
     2: inferior in rank or status; "the junior faculty"; "a lowly
        corporal"; "petty officialdom"; "a subordinate
        functionary" [syn: {junior-grade}, {inferior}, {lower}, {lower-ranking},
         {lowly}, {petty(a)}, {subaltern}, {subordinate}]
     3: depending on or incidental to what is original or primary;
        "a secondary infection"
     4: not of major importance; "played a secondary role in world
        events"
    ...
