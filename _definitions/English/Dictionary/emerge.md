---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emerge
offline_file: ""
offline_thumbnail: ""
uuid: 9a4dbb43-e906-4d78-8599-c7db3cd6ce27
updated: 1484310531
title: emerge
categories:
    - Dictionary
---
emerge
     v 1: come out into view, as from concealment; "Suddenly, the
          proprietor emerged from his office"
     2: come out of; "Water issued from the hole in the wall"; "The
        words seemed to come out by themselves" [syn: {issue}, {come
        out}, {come forth}, {go forth}, {egress}]
     3: become known or apparent; "Some nice results emerged from
        the study"
     4: come up to the surface of or rise; "He felt new emotions
        emerge"
     5: happen or occur as a result of something [syn: {come forth}]
