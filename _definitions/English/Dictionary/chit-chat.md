---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chit-chat
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494261
title: chit-chat
categories:
    - Dictionary
---
chitchat
     n : light informal conversation for social occasions [syn: {small
         talk}, {gab}, {gabfest}, {gossip}, {tittle-tattle}, {chin-wag},
          {chin-wagging}, {causerie}]
     v : talk socially without exchanging too much information; "the
         men were sitting in the cafe and shooting the breeze"
         [syn: {chew the fat}, {shoot the breeze}, {chat}, {confabulate},
          {confab}, {chatter}, {chaffer}, {natter}, {gossip}, {jaw},
          {claver}, {visit}]
     [also: {chitchatting}, {chitchatted}]
