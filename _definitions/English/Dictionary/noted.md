---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noted
offline_file: ""
offline_thumbnail: ""
uuid: 00d20630-5058-4c9b-8634-4c41633a4a12
updated: 1484310307
title: noted
categories:
    - Dictionary
---
noted
     adj 1: widely known and esteemed; "a famous actor"; "a celebrated
            musician"; "a famed scientist"; "an illustrious
            judge"; "a notable historian"; "a renowned painter"
            [syn: {celebrated}, {famed}, {far-famed}, {famous}, {illustrious},
             {notable}, {renowned}]
     2: worthy of notice or attention; "a noted increase in the
        crime rate"
