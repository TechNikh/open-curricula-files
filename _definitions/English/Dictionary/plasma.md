---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plasma
offline_file: ""
offline_thumbnail: ""
uuid: 61fb149f-a9e2-41c1-b2b0-8d99f5a63df5
updated: 1484310158
title: plasma
categories:
    - Dictionary
---
plasma
     n 1: colorless watery fluid of blood and lymph containing no
          cells and in which erythrocytes and leukocytes and
          platelets are suspended [syn: {plasm}]
     2: a green slightly translucent variety of chalcedony used as a
        gemstone
     3: (physical chemistry) a fourth state of matter distinct from
        solid or liquid or gas and present in stars and fusion
        reactors; a gas becomes a plasma when it is heated until
        the atoms lose all their electrons, leaving a highly
        electrified collection of nuclei and free electrons;
        "particles in space exist in the form of a plasma"
