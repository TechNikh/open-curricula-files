---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/onset
offline_file: ""
offline_thumbnail: ""
uuid: ee4e8ff2-ff83-4063-a87c-fd8c6996e98a
updated: 1484310561
title: onset
categories:
    - Dictionary
---
onset
     n 1: the beginning or early stages; "the onset of pneumonia"
          [syn: {oncoming}]
     2: (military) an offensive against an enemy (using weapons);
        "the attack began at dawn" [syn: {attack}, {onslaught}, {onrush}]
