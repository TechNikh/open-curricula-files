---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complete
offline_file: ""
offline_thumbnail: ""
uuid: 78b228bc-b5fc-4267-85e8-4176894f097f
updated: 1484310266
title: complete
categories:
    - Dictionary
---
complete
     adj 1: having every necessary or normal part or component or step;
            "a complete meal"; "a complete wardrobe"; "a complete
            set pf the Britannica"; "a complete set of china"; "a
            complete defeat"; "a complete accounting" [ant: {incomplete},
             {incomplete}]
     2: perfect and complete in every respect; having all necessary
        qualities; "a complete gentleman"; "consummate happiness";
        "a consummate performance" [syn: {consummate}]
     3: having all four whorls or principal parts--sepals and petals
        and stamens and carpels (or pistils); "complete flowers"
        [ant: {incomplete}]
     4: highly skilled; "an ...
