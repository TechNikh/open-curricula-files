---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cosec
offline_file: ""
offline_thumbnail: ""
uuid: 9821eeb5-c21c-40c7-8e50-ff27ff094572
updated: 1484310154
title: cosec
categories:
    - Dictionary
---
cosec
     n : ratio of the hypotenuse to the opposite side of a
         right-angled triangle [syn: {cosecant}]
