---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/competent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484601421
title: competent
categories:
    - Dictionary
---
competent
     adj 1: properly or sufficiently qualified or capable or efficient;
            "a competent typist" [ant: {incompetent}]
     2: adequate for the purpose; "a competent performance"
