---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sommerfeld_radiation_condition
offline_file: ""
offline_thumbnail: ""
uuid: 1ebae56e-14ea-4f64-86f0-12dc2db4018b
updated: 1484308898
title: Sommerfeld radiation condition
categories:
    - Waves
---
Mathematically, consider the inhomogeneous Helmholtz equation
