---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lass
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556841
title: lass
categories:
    - Dictionary
---
lass
     n : a girl or young woman who is unmarried [syn: {lassie}, {young
         girl}, {jeune fille}]
