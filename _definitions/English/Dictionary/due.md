---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/due
offline_file: ""
offline_thumbnail: ""
uuid: a74504b8-85e6-45cf-bc08-25bdb7a8af92
updated: 1484310359
title: due
categories:
    - Dictionary
---
due
     adj 1: owed and payable immediately or on demand; "payment is due"
            [syn: {owed}] [ant: {undue}]
     2: proper and appropriate; fitting; "richly deserved
        punishment"; "due esteem" [syn: {deserved}]
     3: scheduled to arrive; "the train is due in 15 minutes" [syn:
        {due(p)}]
     4: suitable to or expected in the circumstances; "all due
        respect"; "due cause to honor them"; "a long due
        promotion"; "in due course" [ant: {undue}]
     5: reasonable in the circumstances; "gave my comments due
        consideration"; "exercising due care"
     n 1: that which is deserved or owed; "give the devil his due"
     2: a payment that is due (e.g., as ...
