---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_responsibility
offline_file: ""
offline_thumbnail: ""
uuid: a26e8824-80c3-4ab8-a221-ef4d29607911
updated: 1484308365
title: Social responsibility
tags:
    - Student social responsibility
    - Corporate social responsibility
    - Social Responsibility of Scientists and Engineers
    - Notes
categories:
    - Uncategorized
---
Social responsibility is an ethical framework and suggests that an entity, be it an organization or individual, has an obligation to act for the benefit of society at large. Social responsibility is a duty every individual has to perform so as to maintain a balance between the economy and the ecosystems. A trade-off may exist between economic development, in the material sense, and the welfare of the society and environment,[1] though this has been challenged by many reports over the past decade.[2][3] Social responsibility means sustaining the equilibrium between the two. It pertains not only to business organizations but also to everyone whose any action impacts the environment.[4] This ...
