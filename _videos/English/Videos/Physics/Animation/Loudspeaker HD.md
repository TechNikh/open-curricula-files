---
version: 1
type: video
provider: YouTube
id: L0b3_15DIos
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c595d73c-c819-47c5-be0a-4675d0e3918e
updated: 1486069652
title: Loudspeaker HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/L0b3_15DIos/default.jpg
    - https://i3.ytimg.com/vi/L0b3_15DIos/1.jpg
    - https://i3.ytimg.com/vi/L0b3_15DIos/2.jpg
    - https://i3.ytimg.com/vi/L0b3_15DIos/3.jpg
---
