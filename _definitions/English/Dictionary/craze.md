---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/craze
offline_file: ""
offline_thumbnail: ""
uuid: 2a8127a4-5eab-435c-af57-563c34afdb89
updated: 1484310533
title: craze
categories:
    - Dictionary
---
craze
     n 1: an interest followed with exaggerated zeal; "he always
          follows the latest fads"; "it was all the rage that
          season" [syn: {fad}, {furor}, {furore}, {cult}, {rage}]
     2: state of violent mental agitation [syn: {delirium}, {frenzy},
         {fury}, {hysteria}]
     3: a fine crack in a glaze or other surface
     v 1: cause to go crazy; cause to lose one's mind [syn: {madden}]
     2: develop a fine network of cracks; "Crazed ceramics"
