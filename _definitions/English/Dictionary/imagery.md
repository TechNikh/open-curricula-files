---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imagery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484544961
title: imagery
categories:
    - Dictionary
---
imagery
     n : the ability to form mental images of things or events; "he
         could still hear her in his imagination" [syn: {imagination},
          {imaging}, {mental imagery}]
