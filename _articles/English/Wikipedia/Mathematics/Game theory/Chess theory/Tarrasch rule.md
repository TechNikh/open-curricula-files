---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tarrasch_rule
offline_file: ""
offline_thumbnail: ""
uuid: f60d5841-bbb4-4db1-a0c2-980fdebd5ad0
updated: 1484309020
title: Tarrasch rule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tarrasch_72.jpg
tags:
    - Reasons
    - Illustrations
    - 'Rook behind own passed pawn: win'
    - 'Rook behind enemy passed pawn: usually a draw'
    - New analysis
    - Exceptions
    - Short vs. Yusupov
    - Kharlov vs. Morozevich
    - Kramnik vs. Beliavsky
    - Yusupov vs. Timman
    - Notes
categories:
    - Chess theory
---
The Tarrasch rule is a general principle that applies in the majority of chess middlegames and endgames. Siegbert Tarrasch (1862–1934) stated the "rule" that rooks should be placed behind passed pawns – either yours or your opponent's. The idea behind the guideline is that (1) if a player's rook is behind his passed pawn, the rook protects it as it advances, and (2) if it is behind an opponent's passed pawn, the pawn cannot advance unless it is protected along its way.
