---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quarters
offline_file: ""
offline_thumbnail: ""
uuid: 21309ce6-d985-4e42-8d13-c3eb02132961
updated: 1484310559
title: quarters
categories:
    - Dictionary
---
quarters
     n : housing available for people to live in; "he found quarters
         for his family"; "I visited his bachelor quarters" [syn:
         {living quarters}]
