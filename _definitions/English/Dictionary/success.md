---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/success
offline_file: ""
offline_thumbnail: ""
uuid: f258ec1c-dcb0-4c20-8bcd-02d462353da3
updated: 1484310569
title: success
categories:
    - Dictionary
---
success
     n 1: an event that accomplishes its intended purpose; "let's call
          heads a success and tails a failure"; "the election was
          a remarkable success for Republicans" [ant: {failure}]
     2: an attainment that is successful; "his success in the
        marathon was unexpected"; "his new play was a great
        success"
     3: a state of prosperity or fame; "he is enjoying great
        success"; "he does not consider wealth synonymous with
        success" [ant: {failure}]
     4: a person with a record of successes; "his son would never be
        the achiever that his father was"; "only winners need
        apply"; "if you want to be a success you have to ...
