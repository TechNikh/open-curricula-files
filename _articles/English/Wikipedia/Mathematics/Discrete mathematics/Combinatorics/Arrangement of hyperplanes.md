---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arrangement_of_hyperplanes
offline_file: ""
offline_thumbnail: ""
uuid: 8b9d8d05-98ad-4e33-8cbc-f1fc5a976ef0
updated: 1484308562
title: Arrangement of hyperplanes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Arrangement_hyperplans.png
tags:
    - General theory
    - The intersection semilattice and the matroid
    - Polynomials
    - The Orlik–Solomon algebra
    - Real arrangements
    - Complex arrangements
    - Technicalities
categories:
    - Combinatorics
---
In geometry and combinatorics, an arrangement of hyperplanes is an arrangement of a finite set A of hyperplanes in a linear, affine, or projective space S. Questions about a hyperplane arrangement A generally concern geometrical, topological, or other properties of the complement, M(A), which is the set that remains when the hyperplanes are removed from the whole space. One may ask how these properties are related to the arrangement and its intersection semilattice. The intersection semilattice of A, written L(A), is the set of all subspaces that are obtained by intersecting some of the hyperplanes; among these subspaces are S itself, all the individual hyperplanes, all intersections of ...
