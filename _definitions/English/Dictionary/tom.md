---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tom
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420281
title: tom
categories:
    - Dictionary
---
Tom
     n 1: contemptuous name for a Black man who is abjectly servile
          and deferential to Whites [syn: {Uncle Tom}]
     2: male cat [syn: {tomcat}]
     3: male turkey [syn: {turkey cock}, {gobbler}, {tom turkey}]
