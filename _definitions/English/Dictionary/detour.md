---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detour
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421121
title: detour
categories:
    - Dictionary
---
detour
     n : a roundabout road (especially one that is used temporarily
         while a main route is blocked) [syn: {roundabout way}]
     v : travel via a detour
