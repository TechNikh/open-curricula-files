---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pierced
offline_file: ""
offline_thumbnail: ""
uuid: 3144756d-3289-4eb2-a2e0-097dfef09e21
updated: 1484310220
title: pierced
categories:
    - Dictionary
---
pierced
     adj : having a hole cut through; "pierced ears"; "a perforated
           eardrum"; "a punctured balloon" [syn: {perforated}, {perforate},
            {punctured}]
