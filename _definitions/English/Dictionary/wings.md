---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wings
offline_file: ""
offline_thumbnail: ""
uuid: ffbc8200-fbc8-4780-8b1c-71349fc25184
updated: 1484310286
title: wings
categories:
    - Dictionary
---
wings
     n 1: a means of flight or ascent; "necessity lends wings to
          inspiration"
     2: stylized bird wings worn as an insignia by qualified pilots
        or air crew members
