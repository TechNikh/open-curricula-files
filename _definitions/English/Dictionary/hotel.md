---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hotel
offline_file: ""
offline_thumbnail: ""
uuid: ecbf576b-44c4-40b7-840f-4a64fad9223e
updated: 1484310453
title: hotel
categories:
    - Dictionary
---
hotel
     n : a building where travelers can pay for lodging and meals and
         other services
