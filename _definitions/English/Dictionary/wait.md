---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wait
offline_file: ""
offline_thumbnail: ""
uuid: a6cb1aa2-958c-4ea3-90de-10f16c6eea2b
updated: 1484310486
title: wait
categories:
    - Dictionary
---
wait
     n 1: time during which some action is awaited; "instant replay
          caused too long a delay"; "he ordered a hold in the
          action" [syn: {delay}, {hold}, {time lag}, {postponement}]
     2: the act of waiting (remaining inactive in one place while
        expecting something); "the wait was an ordeal for him"
        [syn: {waiting}]
     v 1: stay in one place and anticipate or expect something; "I had
          to wait on line for an hour to get the tickets"
     2: wait before acting [syn: {hold off}, {hold back}]
     3: look forward to the probable occurrence of; "We were
        expecting a visit from our relatives"; "She is looking to
        a promotion"; "he ...
