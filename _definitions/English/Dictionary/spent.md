---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spent
offline_file: ""
offline_thumbnail: ""
uuid: c320a328-ec47-466b-906e-067ee2436741
updated: 1484310202
title: spent
categories:
    - Dictionary
---
spent
     adj 1: drained of energy or effectiveness; extremely tired;
            completely exhausted; "the day's shopping left her
            exhausted"; "he went to bed dog-tired"; "was fagged
            and sweaty"; "the trembling of his played out limbs";
            "felt completely washed-out"; "only worn-out horses
            and cattle"; "you look worn out" [syn: {exhausted}, {dog-tired},
             {fagged}, {fatigued}, {played out}, {washed-out}, {worn-out(a)},
             {worn out(p)}]
     2: having all been spent; "the money is all gone" [syn: {gone},
         {expended}]
