---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/succession
offline_file: ""
offline_thumbnail: ""
uuid: 6b9eb035-8439-43ce-acc4-ccb5cf6aebfa
updated: 1484310461
title: succession
categories:
    - Dictionary
---
succession
     n 1: a following of one thing after another in time; "the doctor
          saw a sequence of patients" [syn: {sequence}, {chronological
          sequence}, {successiveness}, {chronological succession}]
     2: a group of people or things arranged or following in order;
        "a succession of stalls offering soft drinks"; "a
        succession of failures"
     3: the action of following in order; "he played the trumps in
        sequence" [syn: {sequence}]
     4: (ecology) the gradual and orderly process of change in an
        ecosystem brought about by the progressive replacement of
        one community by another until a stable climax is
        established [syn: ...
