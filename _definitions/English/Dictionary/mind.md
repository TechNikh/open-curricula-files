---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mind
offline_file: ""
offline_thumbnail: ""
uuid: 44e963e8-0fa8-4ddd-9bae-f5ec0c3ad780
updated: 1484310305
title: mind
categories:
    - Dictionary
---
mind
     n 1: that which is responsible for one's thoughts and feelings;
          the seat of the faculty of reason; "his mind wandered";
          "I couldn't get his words out of my head" [syn: {head},
          {brain}, {psyche}, {nous}]
     2: recall or remembrance; "it came to mind"
     3: an opinion formed by judging something; "he was reluctant to
        make his judgment known"; "she changed her mind" [syn: {judgment},
         {judgement}]
     4: an important intellectual; "the great minds of the 17th
        century" [syn: {thinker}, {creative thinker}]
     5: attention; "don't pay him any mind"
     6: your intention; what you intend to do; "he had in mind to
        see ...
