---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ECU-TEST
offline_file: ""
offline_thumbnail: ""
uuid: 9ae409aa-b4f5-441b-9786-a3627aca3ec8
updated: 1484308727
title: ECU-TEST
tags:
    - Functionality
    - Methodology
    - Structure
    - Interfaces
    - Supported hardware and software
    - Supported standards
categories:
    - Control engineering
---
ECU-TEST is a software tool developed by TraceTronic GmbH, based in Dresden, Germany, for test and validation of embedded systems. Since the first release of ECU-TEST in 2003,[1] the software is used as standard tool in the development of automotive ECUs[2][3][4] and increasingly in the development of heavy machinery[5][6][7] as well as in factory automation.[8] The development of the software started within a research project on systematic testing of control units and laid the foundation for the spin-off of TraceTronic GmbH from TU Dresden. ECU-TEST aims at the specification, implementation, documentation, execution and assessment of test cases. Owing to various test automation methods, ...
