---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drama
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484490241
title: drama
categories:
    - Dictionary
---
drama
     n 1: a dramatic work intended for performance by actors on a
          stage; "he wrote several plays but only one was produced
          on Broadway" [syn: {play}, {dramatic play}]
     2: an episode that is turbulent or highly emotional [syn: {dramatic
        event}]
     3: the literary genre of works intended for the theater
     4: the quality of being arresting or highly emotional
