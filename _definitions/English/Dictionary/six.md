---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/six
offline_file: ""
offline_thumbnail: ""
uuid: b0741e8f-f444-4a0d-9fef-3a19600d7776
updated: 1484310399
title: six
categories:
    - Dictionary
---
six
     adj : denoting a quantity consisting of six items or units [syn: {6},
            {vi}, {half dozen}, {half a dozen}]
     n : the cardinal number that is the sum of five and one [syn: {6},
          {VI}, {sixer}, {sise}, {Captain Hicks}, {half a dozen},
         {sextet}, {sestet}, {sextuplet}, {hexad}]
