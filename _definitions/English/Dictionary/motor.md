---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motor
offline_file: ""
offline_thumbnail: ""
uuid: 3552afd0-ac27-48a8-b3d2-580b098e9296
updated: 1484310196
title: motor
categories:
    - Dictionary
---
motor
     adj 1: conveying information to the muscles from the CNS; "motor
            nerves" [syn: {centrifugal}, {motor(a)}]
     2: causing or able to cause motion; "a motive force"; "motive
        power"; "motor energy" [syn: {motive(a)}]
     n 1: machine that converts other forms of energy into mechanical
          energy and so imparts motion
     2: a nonspecific agent that imparts motion; "happiness is the
        aim of all men and the motor of all action"
     v : travel or be transported in a vehicle; "We drove to the
         university every morning"; "They motored to London for
         the theater" [syn: {drive}]
