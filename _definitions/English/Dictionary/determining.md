---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/determining
offline_file: ""
offline_thumbnail: ""
uuid: 4d9c9275-12f0-4590-87d5-7a1f4b8388ce
updated: 1484310299
title: determining
categories:
    - Dictionary
---
determining
     adj : having the power or quality of deciding; "the crucial
           experiment"; "cast the deciding vote"; "the
           determinative (or determinant) battle" [syn: {crucial},
            {deciding(a)}, {determinant}, {determinative}, {determining(a)}]
