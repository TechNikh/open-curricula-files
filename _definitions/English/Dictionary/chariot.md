---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chariot
offline_file: ""
offline_thumbnail: ""
uuid: 733ed25e-ec71-4bf2-aabc-36c865cc8120
updated: 1484310603
title: chariot
categories:
    - Dictionary
---
chariot
     n 1: a light four-wheel horse-drawn ceremonial carriage
     2: a two-wheeled horse-drawn battle vehicle; used in war and
        races in ancient Egypt and Greece and Rome
     v 1: transport in a chariot
     2: ride in a chariot
