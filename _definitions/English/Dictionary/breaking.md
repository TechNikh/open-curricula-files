---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breaking
offline_file: ""
offline_thumbnail: ""
uuid: 00a976b9-0822-468f-9377-eda638796756
updated: 1484310323
title: breaking
categories:
    - Dictionary
---
breaking
     adj : (of waves) curling over and crashing into surf or spray;
           "the breaking waves"
     n : the act of breaking something; "the breakage was
         unavoidable" [syn: {breakage}, {break}]
