---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phase
offline_file: ""
offline_thumbnail: ""
uuid: 96939c2b-5907-4675-bf52-958581b94729
updated: 1484310226
title: phase
categories:
    - Dictionary
---
phase
     n 1: (physical chemistry) a distinct state of matter in a system;
          matter that is identical in chemical composition and
          physical state and separated from other material by the
          phase boundary; "the reaction occurs in the liquid phase
          of the system" [syn: {form}]
     2: any distinct time period in a sequence of events; "we are in
        a transitional stage in which many former ideas must be
        revised or rejected" [syn: {stage}]
     3: a particular point in the time of a cycle; measured from
        some arbitrary zero and expressed as an angle [syn: {phase
        angle}]
     4: (astronomy) the particular appearance of a body's ...
