---
version: 1
type: video
provider: YouTube
id: upO6Mh862PI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/AP%20Calculus%20BC%20Exams-%202008%201%20a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a3556131-f9e5-4c83-850c-e976f25f67b0
updated: 1486069690
title: 'AP Calculus BC Exams- 2008 1 a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/upO6Mh862PI/default.jpg
    - https://i3.ytimg.com/vi/upO6Mh862PI/1.jpg
    - https://i3.ytimg.com/vi/upO6Mh862PI/2.jpg
    - https://i3.ytimg.com/vi/upO6Mh862PI/3.jpg
---
