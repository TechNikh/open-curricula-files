---
version: 1
type: video
provider: YouTube
id: 5mNTvtjDnP8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Chronometric%20revolution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92a49386-dad3-4f80-b2ff-fbaf3a1694a3
updated: 1486069688
title: Chronometric revolution
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/5mNTvtjDnP8/default.jpg
    - https://i3.ytimg.com/vi/5mNTvtjDnP8/1.jpg
    - https://i3.ytimg.com/vi/5mNTvtjDnP8/2.jpg
    - https://i3.ytimg.com/vi/5mNTvtjDnP8/3.jpg
---
