---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raoul_Bricard
offline_file: ""
offline_thumbnail: ""
uuid: f3b2559b-a91a-4b97-b239-f621f93fa73e
updated: 1484309075
title: Raoul Bricard
tags:
    - Biography
    - Work
    - Books
    - Notes
categories:
    - Geometers
---
Raoul Bricard (23 March 1870 – 1944) is a French engineer and a mathematician. He is best known for his work in geometry, especially descriptive geometry and scissors congruence, and kinematics, especially mechanical linkages.
