---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mental
offline_file: ""
offline_thumbnail: ""
uuid: 1cd9816f-0f55-4d1c-831c-8ba8a849ac0f
updated: 1484310323
title: mental
categories:
    - Dictionary
---
mental
     adj 1: involving the mind or an intellectual process; "mental
            images of happy times"; "mental calculations"; "in a
            terrible mental state"; "mental suffering"; "free from
            mental defects" [ant: {physical}]
     2: of or relating to the mind; "mental powers"; "mental
        development"; "mental hygiene"
     3: of or relating to the chin- or lip-like structure in insects
        and certain mollusks
     4: of or relating to the chin or median part of the lower jaw
        [syn: {genial}]
     5: affected by a disorder of the mind; "a mental patient";
        "mental illness"
