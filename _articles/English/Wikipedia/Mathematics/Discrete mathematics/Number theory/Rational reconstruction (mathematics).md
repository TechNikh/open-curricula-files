---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Rational_reconstruction_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 7249fe40-cb50-4875-bd74-d3142d16e7ea
updated: 1484308654
title: Rational reconstruction (mathematics)
categories:
    - Number theory
---
In mathematics, rational reconstruction is a method that allows one to recover a rational number from its value modulo an integer. If a problem with a rational solution 
  
    
      
        
          
            r
            s
          
        
      
    
    {\displaystyle {\frac {r}{s}}}
  
 is considered modulo a number m, one will obtain the number 
  
    
      
        n
        =
        r
        ×
        
          s
          
            −
            1
          
        
        
          
          (
          mod
          
          m
          )
        
      
    
    {\displaystyle n=r\times s^{-1}{\pmod {m}}}
  
. If |r| < N and 0 < s < D then r and s can ...
