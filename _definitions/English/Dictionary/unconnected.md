---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unconnected
offline_file: ""
offline_thumbnail: ""
uuid: a7a6bc75-c80d-4ba5-817f-3cb2cf4af376
updated: 1484310194
title: unconnected
categories:
    - Dictionary
---
unconnected
     adj 1: having had the relation broken [syn: {dissociated}]
     2: not joined or linked together [ant: {connected}]
     3: not connected by birth or family [syn: {not kin(p)}]
     4: lacking orderly continuity; "a confused set of
        instructions"; "a confused dream about the end of the
        world"; "disconnected fragments of a story"; "scattered
        thoughts" [syn: {confused}, {disconnected}, {disjointed},
        {disordered}, {garbled}, {illogical}, {scattered}]
