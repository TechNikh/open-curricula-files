---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formally
offline_file: ""
offline_thumbnail: ""
uuid: cf1a54ac-79c2-49cc-9b0a-9a3a86720b0f
updated: 1484310142
title: formally
categories:
    - Dictionary
---
formally
     adv 1: with official authorization; "the club will be formally
            recognized" [syn: {officially}]
     2: in a formal manner; "he was dressed rather formally" [syn: {with
        formality}] [ant: {informally}]
