---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ralegaon
offline_file: ""
offline_thumbnail: ""
uuid: 0b9c9bbf-9832-4c0b-87e4-f32f7d956784
updated: 1484308440
title: Ralegaon
tags:
    - Demographics
    - Education
    - Places to Visit
    - Transport
    - Languages
    - Climate
categories:
    - Uncategorized
---
Ralegaon (राळेगांव) is Tahsil place town in Yavatmal District of Maharashtra (India). It is located in Vidarbha region of Maharashra. Geographically it is located northwest from Yavatmal. It is located 42 km away from Yavatmal and 21 km away from the Kalamb which is on Nagpur-Yavatmal road. And just 41 km from city Wardha.
