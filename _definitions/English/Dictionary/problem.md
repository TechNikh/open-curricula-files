---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/problem
offline_file: ""
offline_thumbnail: ""
uuid: 57509749-3366-4120-b9d0-4b90c1ef3aa0
updated: 1484310307
title: problem
categories:
    - Dictionary
---
problem
     n 1: a state of difficulty that needs to be resolved; "she and
          her husband are having problems"; "it is always a job to
          contact him"; "urban problems such as traffic congestion
          and smog" [syn: {job}]
     2: a source of difficulty; "one trouble after another delayed
        the job"; "what's the problem?" [syn: {trouble}]
     3: a question raised for consideration or solution; "our
        homework consisted of ten problems to solve"
