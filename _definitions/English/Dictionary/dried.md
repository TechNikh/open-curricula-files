---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dried
offline_file: ""
offline_thumbnail: ""
uuid: 7172935d-9a04-4779-bf2f-f56be1a4d8ad
updated: 1484310260
title: dried
categories:
    - Dictionary
---
dried
     adj 1: not still wet; "the ink has dried"; "a face marked with
            dried tears"
     2: preserved by removing natural moisture; "dried beef"; "dried
        fruit"; "dehydrated eggs"; "shredded and desiccated
        coconut meat" [syn: {dehydrated}, {desiccated}]
