---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recorded
offline_file: ""
offline_thumbnail: ""
uuid: 735b06de-44e8-4103-8d85-a09b93eea549
updated: 1484310208
title: recorded
categories:
    - Dictionary
---
recorded
     adj 1: set down or registered in a permanent form especially on
            film or tape for reproduction; "recorded music" [ant:
            {live}]
     2: made a matter of official record; "a properly recorded deed
        to the property" [ant: {unrecorded}]
     3: (of securities) having the owner's name entered in a
        register; "recorded holders of a stock"
     4: recorded or listed in a directory; "a recorded number"
