---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honeycomb
offline_file: ""
offline_thumbnail: ""
uuid: 21e780e0-9b09-4df2-b051-0511c0372a2a
updated: 1484310416
title: honeycomb
categories:
    - Dictionary
---
honeycomb
     n : a framework of hexagonal cells resembling the honeycomb
         built by bees
     v 1: carve a honeycomb pattern into; "The cliffs were
          honeycombed"
     2: penetrate thoroughly and into every part; "the
        revolutionaries honeycombed the organization"
     3: make full of cavities, like a honeycomb
