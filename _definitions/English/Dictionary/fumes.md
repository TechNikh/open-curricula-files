---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fumes
offline_file: ""
offline_thumbnail: ""
uuid: 698fb993-5ca1-4d4b-85bf-cd257db78138
updated: 1484310373
title: fumes
categories:
    - Dictionary
---
fumes
     n : gases ejected from an engine as waste products [syn: {exhaust},
          {exhaust fumes}]
