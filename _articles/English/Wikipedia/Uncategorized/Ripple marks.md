---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ripple_marks
offline_file: ""
offline_thumbnail: ""
uuid: e1fb1ccf-0354-4dac-bdbf-230ff1594cdd
updated: 1484308377
title: Ripple marks
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ripple_marks_in_Moenkopi_Formation_rock_off_of_Capitol_Reef_Scenic_Drive.jpeg
tags:
    - Defining ripple cross-laminae and asymmetric ripples
    - Ripple marks in different environments
    - Wave-formed ripples
    - Ripple marks formed by aeolian processes
    - Definitions
categories:
    - Uncategorized
---
In geology, ripple marks are sedimentary structures (i.e. bedforms of the lower flow regime) and indicate agitation by water (current or waves) or wind.
