---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computability_theory
offline_file: ""
offline_thumbnail: ""
uuid: facf1aaa-6939-4797-894c-90204f402149
updated: 1484309481
title: Computability theory
tags:
    - Computable and uncomputable sets
    - Turing computability
    - Areas of research
    - Relative computability and the Turing degrees
    - Other reducibilities
    - "Rice's theorem and the arithmetical hierarchy"
    - Reverse mathematics
    - Numberings
    - The priority method
    - The lattice of recursively enumerable sets
    - Automorphism problems
    - Kolmogorov complexity
    - Frequency computation
    - Inductive inference
    - Generalizations of Turing computability
    - Continuous computability theory
    - Relationships between definability, proof and computability
    - Name of the subject
    - Professional organizations
    - Notes
categories:
    - Computability theory
---
Computability theory, also called recursion theory, is a branch of mathematical logic, of computer science, and of the theory of computation that originated in the 1930s with the study of computable functions and Turing degrees. The field has since grown to include the study of generalized computability and definability. In these areas, recursion theory overlaps with proof theory and effective descriptive set theory.
