---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unambiguous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484497861
title: unambiguous
categories:
    - Dictionary
---
unambiguous
     adj 1: having or exhibiting a single clearly defined meaning; "As a
            horror, apartheid...is absolutely unambiguous"- Mario
            Vargas Llosa [ant: {ambiguous}]
     2: admitting of no doubt or misunderstanding; having only one
        meaning or interpretation and leading to only one
        conclusion; "unequivocal evidence"; "took an unequivocal
        position"; "an unequivocal success"; "an unequivocal
        promise"; "an unequivocal (or univocal) statement" [syn: {unequivocal},
         {univocal}] [ant: {equivocal}]
