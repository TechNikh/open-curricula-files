---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_contact
offline_file: ""
offline_thumbnail: ""
uuid: 73d06fd6-00d9-416a-bb27-c9753f32fb1a
updated: 1484308868
title: Thermal contact
categories:
    - Thermodynamics
---
In heat transfer and thermodynamics, a thermodynamic system is said to be in thermal contact with another system if it can exchange energy through the process of heat. Perfect thermal isolation is an idealization as real systems are always in thermal contact with their environment to some extent. The area available for heat flow through interstitial gaps often is 2 to 4 orders of magnitude greater than the contact area; heat flow through gaps can't be neglected especially if the solids are relatively poor conductors or interface medium is a good conductor.[1]
