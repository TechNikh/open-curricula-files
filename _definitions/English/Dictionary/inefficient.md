---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inefficient
offline_file: ""
offline_thumbnail: ""
uuid: 0c11391e-1b37-440a-86ed-df56401c2fad
updated: 1484310268
title: inefficient
categories:
    - Dictionary
---
inefficient
     adj 1: not producing desired results; wasteful; "an inefficient
            campaign against drugs"; "outdated and inefficient
            design and methods" [ant: {efficient}]
     2: lacking the ability or skill to perform effectively;
        inadequate; "an ineffective administration"; "inefficient
        workers" [syn: {ineffective}]
