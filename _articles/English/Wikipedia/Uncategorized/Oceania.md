---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oceania
offline_file: ""
offline_thumbnail: ""
uuid: b021c1b3-f380-4ea2-bf6a-8bfb3d487461
updated: 1484308447
title: Oceania
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Oceania_%2528orthographic_projection%2529.svg.png'
tags:
    - Etymology and semantics
    - Definitions
    - Physiographical
    - Biogeographical
    - Ecogeographical
    - Geopolitical
    - Other definitions
    - History
    - Demographics
    - Archaeogenetics
    - religion
    - Sport
    - Multi-sport games
    - Association football (soccer)
    - Australian rules football
    - cricket
    - Rugby league football
    - Rugby union football
categories:
    - Uncategorized
---
Oceania (UK /ˌoʊʃᵻˈɑːniə, ˌoʊsᵻ-/[1] or US /ˌoʊʃiːˈæniə/),[2] also known as Oceanica,[3] is a region centred on the islands of the tropical Pacific Ocean.[4] Opinions of what constitutes Oceania range from its three subregions of Melanesia, Micronesia, and Polynesia[5] to, more broadly, the entire insular region between Southeast Asia and the Americas, including Australasia and the Malay Archipelago.[6]
