---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Madhava%27s_sine_table'
offline_file: ""
offline_thumbnail: ""
uuid: 2ff28611-c49c-42e0-a134-661db13d778f
updated: 1484309345
title: "Madhava's sine table"
tags:
    - The table
    - "Values in Madhava's table"
    - "Derivation of trigonometric sines from Madhava's table"
    - "Madhava's value of pi"
    - Example
    - "Comparison of Madhava's and modern sine values"
    - "Madhava's method of computation"
    - Further references
categories:
    - Trigonometry
---
Madhava's sine table is the table of trigonometric sines of various angles constructed by the 14th century Kerala mathematician-astronomer Madhava of Sangamagrama. The table lists the trigonometric sines of the twenty-four angles 3.75°, 7.50°, 11.25°, ..., and 90.00° (angles that are integral multiples of 3.75°, i.e. 1/24 of a right angle, beginning with 3.75 and ending with 90.00). The table is encoded in the letters of Devanagari using the Katapayadi system. This gives the entries in the table an appearance of the verses of a poem in Sanskrit.
