---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evil
offline_file: ""
offline_thumbnail: ""
uuid: 77dc6f99-3ff7-48c5-8843-db77d839525f
updated: 1484310152
title: evil
categories:
    - Dictionary
---
evil
     adj 1: morally bad or wrong; "evil purposes"; "an evil influence";
            "evil deeds" [syn: {wicked}] [ant: {good}]
     2: having the nature of vice [syn: {depraved}, {vicious}]
     3: tending to cause great harm [syn: {harmful}, {injurious}]
     4: having or exerting a malignant influence; "malevolent
        stars"; "a malefic force" [syn: {malefic}, {malevolent}, {malign}]
     n 1: morally objectionable behavior [syn: {immorality}, {wickedness},
           {iniquity}]
     2: that which causes harm or destruction or misfortune; "the
        evil that men do lives after them; the good is oft
        interred with their bones"- Shakespeare
     3: the quality of being ...
