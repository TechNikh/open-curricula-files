---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Twelvefold_way
offline_file: ""
offline_thumbnail: ""
uuid: 210d71ad-6975-4a3f-9646-b7c13422b9ac
updated: 1484308605
title: Twelvefold way
tags:
    - Overview
    - Viewpoints
    - Balls and boxes
    - Sampling
    - Selection, labelling, grouping
    - Labelling and selection with or without repetition
    - Partitions of sets and numbers
    - Formulas
    - Intuitive meaning of the rows and columns
    - Details of the different cases
    - Functions from N to X
    - Injective functions from N to X
    - Injective functions from N to X, up to a permutation of N
    - Functions from N to X, up to a permutation of N
    - Surjective functions from N to X, up to a permutation of N
    - Injective functions from N to X, up to a permutation of X
    - >
        Injective functions from N to X, up to permutations of N and
        X
    - Surjective functions from N to X, up to a permutation of X
    - Surjective functions from N to X
    - Functions from N to X, up to a permutation of X
    - >
        Surjective functions from N to X, up to permutations of N
        and X
    - Functions from N to X, up to permutations of N and X
    - Extremal cases
    - Generalizations
categories:
    - Combinatorics
---
In combinatorics, the twelvefold way is a name given to a systematic classification of 12 related enumerative problems concerning two finite sets, which include the classical problems of counting permutations, combinations, multisets, and partitions either of a set or of a number. The idea of the classification is credited to Gian-Carlo Rota, and the name was suggested by Joel Spencer.[1]
