---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ventral_root
offline_file: ""
offline_thumbnail: ""
uuid: e4865942-ce6b-4f12-abaa-a881602c9854
updated: 1484308368
title: Ventral root of spinal nerve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Spinal_nerve.svg.png
categories:
    - Uncategorized
---
At its distal end, the ventral root joins with the dorsal root to form a mixed spinal nerve.
