---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/olive
offline_file: ""
offline_thumbnail: ""
uuid: 7378801c-0d70-4dfb-a6c2-a127a338998e
updated: 1484310413
title: olive
categories:
    - Dictionary
---
olive
     adj : of a yellow-green color similar to that of an unripe olive
     n 1: small ovoid fruit of the European olive tree; important food
          and source of oil
     2: evergreen tree cultivated in the Mediterranean region since
        antiquity and now elsewhere; has edible shiny black fruits
        [syn: {European olive tree}, {Olea europaea}]
     3: hard yellow often variegated wood of an olive tree; used in
        cabinetwork
     4: one-seeded fruit of the European olive tree usually pickled
        and used as a relish
     5: a yellow-green color of low brightness and saturation
