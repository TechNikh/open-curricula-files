---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/print
offline_file: ""
offline_thumbnail: ""
uuid: 43a378f6-d9f5-4697-8ea3-98dcc24c7c7b
updated: 1484310281
title: print
categories:
    - Dictionary
---
print
     n 1: the result of the printing process; "I want to see it in
          black and white" [syn: {black and white}]
     2: a picture or design printed from an engraving
     3: a visible indication made on a surface; "some previous
        reader had covered the pages with dozens of marks"; "paw
        prints were everywhere" [syn: {mark}]
     4: a copy of a movie on film (especially a particular version
        of it)
     5: a fabric with a dyed pattern pressed onto it (usually by
        engraved rollers)
     6: a printed picture produced from a photographic negative
        [syn: {photographic print}]
     v 1: put into print; "The newspaper published the news of the
       ...
