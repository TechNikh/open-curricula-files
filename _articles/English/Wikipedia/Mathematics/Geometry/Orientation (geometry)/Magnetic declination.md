---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_declination
offline_file: ""
offline_thumbnail: ""
uuid: 210df8ec-25d2-4d0d-967f-a0ff0ecdc82d
updated: 1484309293
title: Magnetic declination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Magnetic_declination.svg.png
tags:
    - Declination change over time and location
    - Determining declination
    - Direct measurement
    - Determination from maps and models
    - Software
    - Using the declination
    - Adjustable compasses
    - Non-adjustable compasses
    - Navigation
    - Deviation
    - Air navigation
categories:
    - Orientation (geometry)
---
Magnetic declination or variation is the angle on the horizontal plane between magnetic north (the direction the north end of a compass needle points, corresponding to the direction of the Earth's magnetic field lines) and true north (the direction along a meridian towards the geographic North Pole). This angle varies depending on position on the Earth's surface, and changes over time.
