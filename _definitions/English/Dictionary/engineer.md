---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engineer
offline_file: ""
offline_thumbnail: ""
uuid: 9f62f5a5-5f2a-41b5-b0a3-c1f759c897de
updated: 1484310521
title: engineer
categories:
    - Dictionary
---
engineer
     n 1: a person who uses scientific knowledge to solve practical
          problems [syn: {applied scientist}, {technologist}]
     2: the operator of a railway locomotive [syn: {locomotive
        engineer}, {railroad engineer}, {engine driver}]
     v 1: design as an engineer; "He engineered the water supply
          project"
     2: plan and direct (a complex undertaking); "he masterminded
        the robbery" [syn: {mastermind}, {direct}, {organize}, {organise},
         {orchestrate}]
