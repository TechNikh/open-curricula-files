---
version: 1
type: video
provider: YouTube
id: NhioAAdYDJM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Earth%20geological%20and%20climatic%20history/Seismic%20waves.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7de46351-52ae-4305-8b3f-a1e251406ca4
updated: 1486069685
title: Seismic waves
categories:
    - Earth geological and climatic history
thumbnail_urls:
    - https://i3.ytimg.com/vi/NhioAAdYDJM/default.jpg
    - https://i3.ytimg.com/vi/NhioAAdYDJM/1.jpg
    - https://i3.ytimg.com/vi/NhioAAdYDJM/2.jpg
    - https://i3.ytimg.com/vi/NhioAAdYDJM/3.jpg
---
