---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laplacian_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 6c2f93d3-85f0-460f-adde-e86c7c4e6e7f
updated: 1484309374
title: Laplacian matrix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph_Laplacian_Diffusion_Example.gif
tags:
    - Definition
    - Example
    - Properties
    - Incidence matrix
    - Deformed Laplacian
    - Symmetric normalized Laplacian
    - Random walk normalized Laplacian
    - Graphs
    - Interpretation as the discrete Laplace operator
    - Equilibrium Behavior
    - Example of the Operator on a Grid
    - Approximation to the negative continuous Laplacian
    - Directed multigraphs
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, the Laplacian matrix, sometimes called admittance matrix, Kirchhoff matrix or discrete Laplacian, is a matrix representation of a graph. The Laplacian matrix can be used to find many useful properties of graph. Together with Kirchhoff's theorem, it can be used to calculate the number of spanning trees for a given graph. The sparsest cut of a graph can be approximated through the second eigenvalue of its Laplacian by Cheeger's inequality .
