---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realize
offline_file: ""
offline_thumbnail: ""
uuid: 5167c24d-8cce-4cd6-8765-73544904bec3
updated: 1484310353
title: realize
categories:
    - Dictionary
---
realize
     v 1: be fully aware or cognizant of [syn: {recognize}, {recognise},
           {realise}, {agnize}, {agnise}]
     2: perceive (an idea or situation) mentally; "Now I see!"; "I
        just can't see your point"; "Does she realize how
        important this decision is?"; "I don't understand the
        idea" [syn: {understand}, {realise}, {see}]
     3: make real or concrete; give reality or substance to; "our
        ideas must be substantiated into actions" [syn: {realise},
         {actualize}, {actualise}, {substantiate}]
     4: earn on some commercial or business transaction; earn as
        salary or wages; "How much do you make a month in your new
        job?"; "She ...
