---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reserve
offline_file: ""
offline_thumbnail: ""
uuid: f738807c-1823-4728-bd71-db46d8ca654b
updated: 1484310599
title: reserve
categories:
    - Dictionary
---
reserve
     adj 1: not engaged in military action [syn: {inactive}, {reserve(a)}]
     2: kept in reserve especially for emergency use; "a reserve
        supply of food"; "a spare tire"; "spare parts" [syn: {reserve(a)},
         {spare}]
     n 1: formality and propriety of manner [syn: {modesty}]
     2: something kept back or saved for future use or a special
        purpose [syn: {backlog}, {stockpile}]
     3: an athlete who plays only when another member of the team
        drops out [syn: {substitute}]
     4: (medicine) potential capacity to respond in order to
        maintain vital functions
     5: a district that is reserved for particular purpose [syn: {reservation}]
     6: ...
