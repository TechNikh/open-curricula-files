---
version: 1
type: video
provider: YouTube
id: I77-e4Ibj1g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d836eeaa-9a83-4081-bb39-8c046a863b36
updated: 1486069640
title: 'Physics Lab - Measuring Instruments'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/I77-e4Ibj1g/default.jpg
    - https://i3.ytimg.com/vi/I77-e4Ibj1g/1.jpg
    - https://i3.ytimg.com/vi/I77-e4Ibj1g/2.jpg
    - https://i3.ytimg.com/vi/I77-e4Ibj1g/3.jpg
---
