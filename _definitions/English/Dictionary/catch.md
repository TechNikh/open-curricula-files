---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/catch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456221
title: catch
categories:
    - Dictionary
---
catch
     n 1: a hidden drawback; "it sounds good but what's the catch?"
     2: the quantity that was caught; "the catch was only 10 fish"
        [syn: {haul}]
     3: a person regarded as a good matrimonial prospect [syn: {match}]
     4: anything that is caught (especially if it is worth
        catching); "he shared his catch with the others"
     5: a break or check in the voice (usually a sign of strong
        emotion)
     6: a restraint that checks the motion of something; "he used a
        book as a stop to hold the door open" [syn: {stop}]
     7: a fastener that fastens or locks a door or window
     8: a cooperative game in which a ball is passed back and forth;
        "he ...
