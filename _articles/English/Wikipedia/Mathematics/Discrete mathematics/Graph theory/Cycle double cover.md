---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_double_cover
offline_file: ""
offline_thumbnail: ""
uuid: b6330f1b-cd95-4afd-9711-df47a8b5ccd0
updated: 1484308619
title: Cycle double cover
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Petersen_double_cover.svg.png
tags:
    - Formulation
    - Reduction to snarks
    - Reducible configurations
    - Circular embedding conjecture
    - Stronger conjectures and related problems
    - Notes
categories:
    - Graph theory
---
In graph-theoretic mathematics, a cycle double cover is a collection of cycles in an undirected graph that together include each edge of the graph exactly twice. For instance, for any polyhedral graph, the faces of a convex polyhedron that represents the graph provide a double cover of the graph: each edge belongs to exactly two faces.
