---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digging
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484329801
title: digging
categories:
    - Dictionary
---
dig
     n 1: the site of an archeological exploration; "they set up camp
          next to the dig" [syn: {excavation}, {archeological site}]
     2: an aggressive remark directed at a person like a missile and
        intended to have a telling effect; "his parting shot was
        `drop dead'"; "she threw shafts of sarcasm"; "she takes a
        dig at me every chance she gets" [syn: {shot}, {shaft}, {slam},
         {barb}, {jibe}, {gibe}]
     3: a small gouge (as in the cover of a book); "the book was in
        good condition except for a dig in the back cover"
     4: the act of digging; "there's an interesting excavation going
        on near Princeton" [syn: {excavation}, ...
