---
version: 1
type: video
provider: YouTube
id: wob01rn-Bg0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 245e94bf-6756-46de-acfc-223899be4f00
updated: 1486069636
title: 'Polarization Experiment (Location - UMS SST)'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/wob01rn-Bg0/default.jpg
    - https://i3.ytimg.com/vi/wob01rn-Bg0/1.jpg
    - https://i3.ytimg.com/vi/wob01rn-Bg0/2.jpg
    - https://i3.ytimg.com/vi/wob01rn-Bg0/3.jpg
---
