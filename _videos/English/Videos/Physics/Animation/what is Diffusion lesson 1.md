---
version: 1
type: video
provider: YouTube
id: Vcmcmh4gE6E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/what%20is%20Diffusion%20lesson%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8324195e-9b33-4dfc-9bb7-f88cb8e88aaa
updated: 1486069653
title: what is Diffusion lesson 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Vcmcmh4gE6E/default.jpg
    - https://i3.ytimg.com/vi/Vcmcmh4gE6E/1.jpg
    - https://i3.ytimg.com/vi/Vcmcmh4gE6E/2.jpg
    - https://i3.ytimg.com/vi/Vcmcmh4gE6E/3.jpg
---
