---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apache_Wave
offline_file: ""
offline_thumbnail: ""
uuid: 22a33c79-a371-44a5-ab5b-26e3013feec7
updated: 1484308819
title: Apache Wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Googlewave.svg.png
tags:
    - History
    - Origin of name
    - Open source
    - Reception
    - >
        End of development of original Google Wave under Google in
        2010
    - Apache Wave
    - Wave in a Box
    - features
    - Extension programming interface
    - Extensions
    - Robots
    - Gadgets
    - Federation protocol
    - Adoption of Wave Protocol and Wave Federation Protocol
    - Compatible third-party servers
categories:
    - Self-organization
---
Apache Wave is a software framework for real-time collaborative editing online. Google originally developed it as Google Wave.[2] It was announced at the Google I/O conference on May 27, 2009.[3][4]
