---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overload
offline_file: ""
offline_thumbnail: ""
uuid: 1ed94cbc-81de-4c9d-ab54-428bae85dce6
updated: 1484310198
title: overload
categories:
    - Dictionary
---
overload
     n 1: an electrical load that exceeds the available electrical
          power
     2: an excessive burden [syn: {overburden}]
     v 1: become overloaded; "The aerator overloaded"
     2: fill to excess so that function is impaired; "Fear clogged
        her mind"; "The story was clogged with too many details"
        [syn: {clog}]
     3: place too much a load on; "don't overload the car" [syn: {surcharge},
         {overcharge}]
