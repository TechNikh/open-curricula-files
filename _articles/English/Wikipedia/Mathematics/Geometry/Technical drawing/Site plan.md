---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Site_plan
offline_file: ""
offline_thumbnail: ""
uuid: 0dbdfcd4-1d70-41f0-ac19-e35065c6131f
updated: 1484309332
title: Site plan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Site_Plan_24hPlans.jpg
tags:
    - Site plan topics
    - Site analysis
    - Site plan building blocks
    - Site planning
    - Transportation planning
    - Urban planning
    - Examples
categories:
    - Technical drawing
---
A site plan is an architectural plan, landscape architecture document, and a detailed engineering drawing of proposed improvements to a given lot. A site plan usually shows a building footprint, travelways, parking, drainage facilities, sanitary sewer lines, water lines, trails, lighting, and landscaping and garden elements.[1]
