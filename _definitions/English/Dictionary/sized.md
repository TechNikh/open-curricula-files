---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sized
offline_file: ""
offline_thumbnail: ""
uuid: 29af0711-2e17-4ee2-84ed-08a4e36392aa
updated: 1484310208
title: sized
categories:
    - Dictionary
---
sized
     adj 1: having a specified size [ant: {unsized}]
     2: having the surface treated or coated with sizing [ant: {unsized}]
