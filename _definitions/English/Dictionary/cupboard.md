---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cupboard
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484381641
title: cupboard
categories:
    - Dictionary
---
cupboard
     n : a small room (or recess) or cabinet used for storage space
         [syn: {closet}]
