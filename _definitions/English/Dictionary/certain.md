---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/certain
offline_file: ""
offline_thumbnail: ""
uuid: 9bf2d35f-a96f-464e-a64f-f8991989e6a2
updated: 1484310357
title: certain
categories:
    - Dictionary
---
certain
     adj 1: definite but not specified or identified; "set aside a
            certain sum each week"; "to a certain degree";
            "certain breeds do not make good pets"; "certain
            members have not paid their dues"; "a certain popular
            teacher"; "a certain Mrs. Jones" [syn: {certain(a)}]
     2: having or feeling no doubt or uncertainty; confident and
        assured; "felt certain of success"; "was sure (or certain)
        she had seen it"; "was very sure in his beliefs"; "sure of
        her friends" [syn: {certain(p)}, {sure}] [ant: {uncertain},
         {uncertain}]
     3: established beyond doubt or question; definitely known;
        "what is ...
