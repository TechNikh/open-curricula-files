---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Periodic_table_of_shapes
offline_file: ""
offline_thumbnail: ""
uuid: 1f16c692-98e4-4254-be2d-5c5a675850da
updated: 1484309369
title: Periodic table of shapes
categories:
    - Geometry stubs
---
The periodic table of mathematical shapes is popular name given to a project to classify Fano varieties.[1] The project was thought up by Professor Alessio Corti, from the Department of Mathematics at Imperial College London. It aims to categorise all three-, four- and five-dimensional shapes into a single table, analogous to the periodic table of chemical elements. It is meant to hold the equations that describe each shape and, through this, mathematicians and other scientists expect to develop a better understanding of the shapes’ geometric properties and relations.[2]
