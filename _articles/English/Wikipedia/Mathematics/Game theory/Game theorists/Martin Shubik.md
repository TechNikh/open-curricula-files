---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Martin_Shubik
offline_file: ""
offline_thumbnail: ""
uuid: 4d16bed8-6d62-415c-a5d7-42e3426dcd04
updated: 1484309029
title: Martin Shubik
tags:
    - Work in economics
    - Personal life
    - Notes
    - Selected publications
categories:
    - Game theorists
---
