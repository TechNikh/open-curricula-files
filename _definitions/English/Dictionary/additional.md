---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/additional
offline_file: ""
offline_thumbnail: ""
uuid: 10871be4-0ae0-403f-93d7-1e39c1590d04
updated: 1484310393
title: additional
categories:
    - Dictionary
---
additional
     adj 1: existing or coming by way of addition; "an additional
            problem"; "further information"; "there will be
            further delays"; "took more time" [syn: {further(a)},
            {more(a)}]
     2: added to complete or make up a deficiency; "produced
        supplementary volumes"; "additional reading" [syn: {supplementary},
         {supplemental}]
     3: further or added; "called for additional troops"; "need
        extra help"; "an extra pair of shoes"; "I have no other
        shoes"; "there are other possibilities" [syn: {extra}, {other(a)}]
     4: more; "would you like anything else?"; "I have nothing else
        to say" [syn: {else(ip)}]
     ...
