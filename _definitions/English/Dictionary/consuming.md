---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consuming
offline_file: ""
offline_thumbnail: ""
uuid: f7355858-f7d3-4bd0-a6f8-2f72022d9a0b
updated: 1484310316
title: consuming
categories:
    - Dictionary
---
consuming
     adj : very intense; "politics is his consuming passion";
           "overwhelming joy" [syn: {overwhelming}]
