---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/particle
offline_file: ""
offline_thumbnail: ""
uuid: 713ffda6-6b83-4064-8e03-7bbf1591b784
updated: 1484310196
title: particle
categories:
    - Dictionary
---
particle
     n 1: (nontechnical usage) a tiny piece of anything [syn: {atom},
          {molecule}, {corpuscle}, {mote}, {speck}]
     2: a body having finite mass and internal structure but
        negligible dimensions
     3: a function word that can be used in English to form phrasal
        verbs
