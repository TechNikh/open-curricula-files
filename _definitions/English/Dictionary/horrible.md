---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horrible
offline_file: ""
offline_thumbnail: ""
uuid: b2fd51dc-4b9a-4905-80f5-71285ccd8aa0
updated: 1484310565
title: horrible
categories:
    - Dictionary
---
horrible
     adj : provoking horror; "an atrocious automobile accident"; "a
           frightful crime of decapitation"; "an alarming, even
           horrifying, picture"; "war is beyond all words
           horrible"- Winston Churchill; "an ugly wound" [syn: {atrocious},
            {frightful}, {horrifying}, {ugly}]
