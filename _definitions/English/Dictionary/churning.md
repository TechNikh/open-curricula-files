---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/churning
offline_file: ""
offline_thumbnail: ""
uuid: 8b49f2ea-f6bf-40e9-a926-664579785b50
updated: 1484310316
title: churning
categories:
    - Dictionary
---
churning
     adj 1: moving with or producing or produced by vigorous agitation;
            "winds whipped the piled leaves into churning masses";
            "a car stuck in the churned-up mud" [syn: {churned-up}]
     2: (of a liquid) agitated vigorously; in a state of turbulence;
        "the river's roiling current"; "turbulent rapids" [syn: {roiling},
         {roiled}, {roily}, {turbulent}]
