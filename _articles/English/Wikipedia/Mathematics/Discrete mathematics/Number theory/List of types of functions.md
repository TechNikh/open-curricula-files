---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_types_of_functions
offline_file: ""
offline_thumbnail: ""
uuid: 9a849e0e-8a98-4ef3-bb33-955bdeb04b57
updated: 1484308645
title: List of types of functions
tags:
    - Relative to set theory
    - Relative to an operator (c.q. a group or other structure)
    - Relative to a topology
    - Relative to an ordering
    - Relative to the real/complex numbers
    - Ways of defining functions/Relation to Type Theory
    - Relation to Category Theory
categories:
    - Number theory
---
Functions can be identified according to the properties they have. These properties describe the functions behaviour under certain conditions. A parabola is a specific type of function.
