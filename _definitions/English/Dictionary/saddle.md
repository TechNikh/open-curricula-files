---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saddle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470681
title: saddle
categories:
    - Dictionary
---
saddle
     n 1: a seat for the rider of a horse
     2: a pass or ridge that slopes gently between two peaks (is
        shaped like a saddle) [syn: {saddleback}]
     3: cut of meat (especially mutton or lamb) consisting of part
        of the backbone and both loins
     4: a piece of leather across the instep of a shoe
     5: a seat for the rider of a bicycle [syn: {bicycle seat}]
     6: posterior part of the back of a domestic fowl
     v 1: put a saddle on; "saddle the horses" [ant: {unsaddle}]
     2: load or burden; encumber; "he saddled me with that heavy
        responsibility"
     3: impose a task upon, assign a responsibility to; "He charged
        her with cleaning up all ...
