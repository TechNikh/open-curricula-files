---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coordinate_system
offline_file: ""
offline_thumbnail: ""
uuid: 7e231a4c-f718-4c31-9c11-df2197264896
updated: 1484309160
title: Coordinate system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-3D_Spherical.svg.png
tags:
    - Common coordinate systems
    - Number line
    - Cartesian coordinate system
    - Polar coordinate system
    - Cylindrical and spherical coordinate systems
    - Homogeneous coordinate system
    - Other commonly used systems
    - Coordinates of geometric objects
    - Transformations
    - Coordinate lines/curves and planes/surfaces
    - Coordinate maps
    - Orientation-based coordinates
    - Relativistic Coordinate Systems
    - Notes
categories:
    - Analytic geometry
---
In geometry, a coordinate system is a system which uses one or more numbers, or coordinates, to uniquely determine the position of a point or other geometric element on a manifold such as Euclidean space.[1][2] The order of the coordinates is significant, and they are sometimes identified by their position in an ordered tuple and sometimes by a letter, as in "the x-coordinate". The coordinates are taken to be real numbers in elementary mathematics, but may be complex numbers or elements of a more abstract system such as a commutative ring. The use of a coordinate system allows problems in geometry to be translated into problems about numbers and vice versa; this is the basis of analytic ...
