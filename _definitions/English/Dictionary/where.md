---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/where
offline_file: ""
offline_thumbnail: ""
uuid: 02f19b29-5f01-474b-a0f2-86ccec931414
updated: 1484310357
title: where
categories:
    - Dictionary
---
where
     adv : in or at or to what place; "I know where he is"; "use it
           wherever necessary" [syn: {wherever}]
