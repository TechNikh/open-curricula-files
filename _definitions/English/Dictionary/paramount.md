---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paramount
offline_file: ""
offline_thumbnail: ""
uuid: 1f1a79ad-4f91-4544-bdbf-b5af14f3027a
updated: 1484310591
title: paramount
categories:
    - Dictionary
---
paramount
     adj : having superior power and influence; "the predominant mood
           among policy-makers is optimism" [syn: {overriding}, {predominant},
            {predominate}, {preponderant}, {preponderating}]
