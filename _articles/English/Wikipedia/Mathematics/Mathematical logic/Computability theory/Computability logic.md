---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computability_logic
offline_file: ""
offline_thumbnail: ""
uuid: 9fd38832-816e-4dcb-bb1a-a62ade2962ba
updated: 1484309485
title: Computability logic
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Operators_of_computability_logic.png
tags:
    - Literature
categories:
    - Computability theory
---
Computability logic (CoL) is a research program and mathematical framework for redeveloping logic as a systematic formal theory of computability, as opposed to classical logic which is a formal theory of truth. It was introduced and so named by Giorgi Japaridze in 2003.[1]
