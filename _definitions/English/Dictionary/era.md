---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/era
offline_file: ""
offline_thumbnail: ""
uuid: 1af348a8-f29c-4e5e-a08c-ae0b2fbacb5b
updated: 1484310577
title: era
categories:
    - Dictionary
---
era
     n 1: a period marked by distinctive character or reckoned from a
          fixed point or event [syn: {epoch}]
     2: a major division of geological time; an era is usually
        divided into two or more periods [syn: {geological era}]
