---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generosity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484617441
title: generosity
categories:
    - Dictionary
---
generosity
     n 1: the trait of being willing to give your money or time [syn:
          {generousness}] [ant: {stinginess}]
     2: acting generously [syn: {unselfishness}]
