---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tape
offline_file: ""
offline_thumbnail: ""
uuid: aae82079-928c-4579-9a69-1a3b0163049b
updated: 1484310218
title: tape
categories:
    - Dictionary
---
tape
     n 1: a long thin piece of cloth or paper as used for binding or
          fastening; "he used a piece of tape for a belt"; "he
          wrapped a tape around the package"
     2: a recording made on magnetic tape; "the several recordings
        were combined on a master tape" [syn: {tape recording}, {taping}]
     3: the finishing line for a foot race; "he broke the tape in
        record time"
     4: measuring instrument consisting of a narrow strip (cloth or
        metal) marked in inches or centimeters and used for
        measuring lengths; "the carpenter should have used his
        tape measure" [syn: {tapeline}, {tape measure}]
     5: memory device consisting of a long ...
