---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bodied
offline_file: ""
offline_thumbnail: ""
uuid: cf9eb32f-baf8-4e7b-b366-4a960870f3dc
updated: 1484310561
title: bodied
categories:
    - Dictionary
---
bodied
     adj 1: having a body or a body of a specified kind; often used in
            combination; "strong-bodied"; "big-bodied" [ant: {unbodied}]
     2: possessing or existing in bodily form; "what seemed corporal
        melted as breath into the wind"- Shakespeare; "an
        incarnate spirit"; "`corporate' is an archaic term" [syn:
        {corporal}, {corporate}, {embodied}, {incarnate}]
