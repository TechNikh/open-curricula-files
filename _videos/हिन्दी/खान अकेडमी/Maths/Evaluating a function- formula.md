---
version: 1
type: video
provider: YouTube
id: t8h9w8W4rWA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bbe4a0ef-1d36-4298-b52b-d9ca9ecbbd78
updated: 1486069605
title: 'Evaluating a function- formula'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/t8h9w8W4rWA/default.jpg
    - https://i3.ytimg.com/vi/t8h9w8W4rWA/1.jpg
    - https://i3.ytimg.com/vi/t8h9w8W4rWA/2.jpg
    - https://i3.ytimg.com/vi/t8h9w8W4rWA/3.jpg
---
