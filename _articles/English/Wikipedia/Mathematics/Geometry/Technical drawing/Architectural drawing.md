---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Architectural_drawing
offline_file: ""
offline_thumbnail: ""
uuid: e9985291-d98e-40a1-80cb-6f826f739c7d
updated: 1484309321
title: Architectural drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Plan_Port-Royal-des-Champs.jpg
tags:
    - Size and scale
    - Standard views used in architectural drawing
    - Floor plan
    - Site plan
    - Elevation
    - Cross section
    - Isometric and axonometric projections
    - Detail drawings
    - Architectural perspective
    - Sketches and diagrams
    - Types of architectural drawing
    - Presentation drawings
    - Survey drawings
    - Record drawings
    - Working drawings
    - Drafting
    - Computer-aided design
    - Architectural reprographics
categories:
    - Technical drawing
---
An architectural drawing or architect's drawing is a technical drawing of a building (or building project) that falls within the definition of architecture. Architectural drawings are used by architects and others for a number of purposes: to develop a design idea into a coherent proposal, to communicate ideas and concepts, to convince clients of the merits of a design, to enable a building contractor to construct it, as a record of the completed work, and to make a record of a building that already exists.
