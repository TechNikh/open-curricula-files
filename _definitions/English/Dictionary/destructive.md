---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destructive
offline_file: ""
offline_thumbnail: ""
uuid: cf922091-c621-450b-a6fb-d35438d93387
updated: 1484310441
title: destructive
categories:
    - Dictionary
---
destructive
     adj : causing destruction or much damage; "a policy that is
           destructive to the economy"; "destructive criticism"
           [ant: {constructive}]
