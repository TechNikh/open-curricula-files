---
version: 1
type: video
provider: YouTube
id: qBRFIMcxZNM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/What%20is%20a%20Protein-%20Learn%20about%20the%203D%20shape%20and%20function%20of%20macromolecules.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cc70b9f2-cfd7-42e5-a382-8396d8c7f56b
updated: 1486069662
title: >
    What is a Protein? Learn about the 3D shape and function of
    macromolecules
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/qBRFIMcxZNM/default.jpg
    - https://i3.ytimg.com/vi/qBRFIMcxZNM/1.jpg
    - https://i3.ytimg.com/vi/qBRFIMcxZNM/2.jpg
    - https://i3.ytimg.com/vi/qBRFIMcxZNM/3.jpg
---
