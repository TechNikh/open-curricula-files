---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Musselman%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c86e01e0-0705-41e7-bb65-ab1561c56901
updated: 1484309119
title: "Musselman's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Musselman_theorem.svg.png
categories:
    - Theorems in geometry
---
Specifically, let 
  
    
      
        T
      
    
    {\displaystyle T}
  
 be a triangle, and 
  
    
      
        A
      
    
    {\displaystyle A}
  
, 
  
    
      
        B
      
    
    {\displaystyle B}
  
, and 
  
    
      
        C
      
    
    {\displaystyle C}
  
 its vertices. Let 
  
    
      
        
          A
          
            ∗
          
        
      
    
    {\displaystyle A^{*}}
  
, 
  
    
      
        
          B
          
            ∗
          
        
      
    
    {\displaystyle B^{*}}
  
, and 
  
    
      
        
          C
          
            ∗
          
        
      
    
    {\displaystyle C^{*}}
  ...
