---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Floor_plan
offline_file: ""
offline_thumbnail: ""
uuid: 0fb2d93d-bf8b-4ecf-9266-7b92b4962a79
updated: 1484309324
title: Floor plan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Sample_Floorplan.jpg
tags:
    - Overview
    - Floor plan topics
    - Building blocks
    - Plan view
    - 3D Floor plans
    - Examples
categories:
    - Technical drawing
---
In architecture and building engineering, a floor plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure.
