---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperarithmetical_theory
offline_file: ""
offline_thumbnail: ""
uuid: addc1f05-85ab-4e8c-9c67-5d2a6b6e99be
updated: 1484309487
title: Hyperarithmetical theory
tags:
    - Hyperarithmetical sets
    - Hyperarithmetical sets and definability
    - 'Hyperarithmetical sets and iterated Turing jumps: the hyperarithmetical hierarchy'
    - Hyperarithmetical sets and recursion in higher types
    - 'Example: the truth set of arithmetic'
    - Fundamental results
    - Relativized hyperarithmeticity and hyperdegrees
    - Generalizations
    - Relation to other hierarchies
categories:
    - Computability theory
---
In recursion theory, hyperarithmetic theory is a generalization of Turing computability. It has close connections with definability in second-order arithmetic and with weak systems of set theory such as Kripke–Platek set theory. It is an important tool in effective descriptive set theory.
