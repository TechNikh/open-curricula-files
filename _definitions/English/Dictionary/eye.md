---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eye
offline_file: ""
offline_thumbnail: ""
uuid: 153195a9-6f4c-4c09-93f5-f8a0170ae069
updated: 1484310309
title: eye
categories:
    - Dictionary
---
eye
     n 1: the organ of sight [syn: {oculus}, {optic}]
     2: good discernment (either with the eyes or as if with the
        eyes); "she has an eye for fresh talent"; "he has an
        artist's eye"
     3: attention to what is seen; "he tried to catch her eye"
     4: an area that is approximately central within some larger
        region; "it is in the center of town"; "they ran forward
        into the heart of the struggle"; "they were in the eye of
        the storm" [syn: {center}, {centre}, {middle}, {heart}]
     5: a small hole or loop (as in a needle); "the thread wouldn't
        go through the eye"
     v : look at [syn: {eyeball}]
