---
version: 1
type: video
provider: YouTube
id: SauhB2fYQkM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Role%20of%20the%20Sarcoplasmic%20Reticulum%20in%20Muscle%20Cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0621abfa-691f-456d-bb8c-8e35cd4e438e
updated: 1486069585
title: Role of the Sarcoplasmic Reticulum in Muscle Cells
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/SauhB2fYQkM/default.jpg
    - https://i3.ytimg.com/vi/SauhB2fYQkM/1.jpg
    - https://i3.ytimg.com/vi/SauhB2fYQkM/2.jpg
    - https://i3.ytimg.com/vi/SauhB2fYQkM/3.jpg
---
The role of the sarcoplasmic reticulum in controlling calcium ion concentrations within the muscle cell
More free lessons at: http://www.khanacademy.org/video?v=SauhB2fYQkM
