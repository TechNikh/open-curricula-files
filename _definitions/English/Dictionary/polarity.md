---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polarity
offline_file: ""
offline_thumbnail: ""
uuid: bbbdceca-941d-4fe6-bf63-515fc166741b
updated: 1484310407
title: polarity
categories:
    - Dictionary
---
polarity
     n 1: a relation between two opposite attributes or tendencies;
          "he viewed it as a balanced polarity between good and
          evil" [syn: {mutual opposition}]
     2: having an indicated pole (as the distinction between
        positive and negative electric charges); "he got the
        polarity of the battery reversed"; "charges of opposite
        sign" [syn: {sign}]
