---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resistor
offline_file: ""
offline_thumbnail: ""
uuid: ac3053de-60c0-4856-9c4b-d31c65182c5d
updated: 1484310200
title: resistor
categories:
    - Dictionary
---
resistor
     n : an electrical device that resists the flow of electrical
         current [syn: {resistance}]
