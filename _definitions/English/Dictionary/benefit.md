---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/benefit
offline_file: ""
offline_thumbnail: ""
uuid: 6e3d3534-3169-4edd-98de-0ac802520eaf
updated: 1484310254
title: benefit
categories:
    - Dictionary
---
benefit
     n 1: financial assistance in time of need
     2: something that aids or promotes well-being; "for the common
        good" [syn: {welfare}]
     3: a performance to raise money for a charitable cause
     v 1: derive a benefit from; "She profited from his vast
          experience" [syn: {profit}, {gain}]
     2: be beneficial for; "This will do you good" [syn: {do good}]
     [also: {benefitting}, {benefitted}]
