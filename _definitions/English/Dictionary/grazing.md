---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grazing
offline_file: ""
offline_thumbnail: ""
uuid: 23d583c9-5278-4547-86f8-09ec969d6691
updated: 1484310462
title: grazing
categories:
    - Dictionary
---
grazing
     n 1: the act of grazing [syn: {graze}]
     2: the act of brushing against while passing [syn: {shaving}, {skimming}]
