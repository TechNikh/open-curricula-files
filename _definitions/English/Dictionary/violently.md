---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violently
offline_file: ""
offline_thumbnail: ""
uuid: d4ceea0b-8b95-4403-80cf-f87169a9406e
updated: 1484310409
title: violently
categories:
    - Dictionary
---
violently
     adv : in a violent manner; "they attacked violently" [ant: {nonviolently}]
