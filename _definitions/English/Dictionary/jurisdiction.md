---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jurisdiction
offline_file: ""
offline_thumbnail: ""
uuid: bae4a697-773d-46cc-98c2-98639769fa63
updated: 1484310597
title: jurisdiction
categories:
    - Dictionary
---
jurisdiction
     n 1: (law) the right and power to interpret and apply the law;
          "courts having jurisdiction in this district" [syn: {legal
          power}]
     2: in law; the territory within which power can be exercised
