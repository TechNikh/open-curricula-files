---
version: 1
type: video
provider: YouTube
id: -h3Oqhl8fPg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Multiplication%20and%20division/Multiplying%20multiple%20digit%20numbers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d36c4f3b-7c6a-40b6-8ef5-01611f464e31
updated: 1486069565
title: Multiplying multiple digit numbers
categories:
    - Multiplication and division
thumbnail_urls:
    - https://i3.ytimg.com/vi/-h3Oqhl8fPg/default.jpg
    - https://i3.ytimg.com/vi/-h3Oqhl8fPg/1.jpg
    - https://i3.ytimg.com/vi/-h3Oqhl8fPg/2.jpg
    - https://i3.ytimg.com/vi/-h3Oqhl8fPg/3.jpg
---
Working our way up to multiplying multiple-digit numbers with each other. We'll go slow so you can follow along.

Practice this lesson yourself on KhanAcademy.org right now: https://www.khanacademy.org/math/arithmetic/multiplication-division/multi_digit_multiplication/e/multiplication_4?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Watch the next lesson: https://www.khanacademy.org/math/arithmetic/multiplication-division/multi_digit_multiplication/v/multiplying-whole-numbers-and-applications-4?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Missed the previous lesson? 
https://www.khanacademy.org/math/arithmetic/multiplication-division/multi_digit_multiplication/v/multiplying-whole-numbers-and-applications-2?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Arithmetic on Khan Academy: So you're ready to have some arithmetic fun? You've come to the right spot! It's the first "official" math topic and chalked full of fun exercises and great videos which help you start your journey towards math mastery. We'll cover the big ones: addition, subtraction, multiplication, and division, of course. But we don't stop there. We'll get into negative numbers, absolute value, decimals, and fractions, too. Learning math should be fun, and we plan on having some with you. Ready to get started?

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Arithmetic channel: https://www.youtube.com/channel/UCFlJkgTNeC-FYYIQnyE5yzQ?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
