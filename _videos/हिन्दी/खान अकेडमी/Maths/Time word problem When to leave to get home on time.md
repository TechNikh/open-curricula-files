---
version: 1
type: video
provider: YouTube
id: 9zz5JZLbZPc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5763c61-eb39-4eef-b28f-dd85c2bfa0f5
updated: 1486069605
title: Time word problem When to leave to get home on time
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/9zz5JZLbZPc/default.jpg
    - https://i3.ytimg.com/vi/9zz5JZLbZPc/1.jpg
    - https://i3.ytimg.com/vi/9zz5JZLbZPc/2.jpg
    - https://i3.ytimg.com/vi/9zz5JZLbZPc/3.jpg
---
