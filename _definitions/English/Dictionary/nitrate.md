---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nitrate
offline_file: ""
offline_thumbnail: ""
uuid: 28930877-53c4-4b1c-b50c-e5cfea88db84
updated: 1484310373
title: nitrate
categories:
    - Dictionary
---
nitrate
     n : any compound containing the nitrate group (such as a salt or
         ester of nitric acid)
     v : treat with nitric acid, so as to change an organic compound
         into a nitrate; "nitroglycerin is obtained by nitrating
         glycerol"
