---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digital_goods_auction
offline_file: ""
offline_thumbnail: ""
uuid: 8ddde65b-8742-48be-bcc7-4ed039ca11f1
updated: 1484309046
title: Digital goods auction
categories:
    - Mechanism design
---
A typical example is when a company sells a digital good, such as a movie. The company can create an unlimited number of copies of that movie in a negligible cost. The company's goal is to maximize its profit; to do this, it has to find the optimal price: if the price is too high, only few people will buy the item; if the price is too low, many people will buy but the total revenue will be low. The optimal price of the movie depends on the valuations of the potential consumers - how much each consumer is willing to pay to buy a movie.
