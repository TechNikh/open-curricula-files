---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_map
offline_file: ""
offline_thumbnail: ""
uuid: 3803044b-1fee-4bf5-aff3-be58fb978b9a
updated: 1484309481
title: Combinatorial map
tags:
    - Motivation
    - Planar graph representation
    - General definition
categories:
    - Topological graph theory
---
A combinatorial map is a combinatorial object modelling topological structures with subdivided objects. Historically, the concept was introduced informally by J. Edmonds for polyhedral surfaces [1] which are planar graphs. It was given its first definite formal expression under the name "Constellations" by A. Jacques [2] but the concept was already extensively used under the name "rotation" by Gerhard Ringel[3] and J.W.T. Youngs in their famous solution of the Heawood map-coloring problem. The term "constellation" was not retained and instead "combinatorial map" was favored. The concept was later extended to represent higher-dimensional orientable subdivided objects. Combinatorial maps are ...
