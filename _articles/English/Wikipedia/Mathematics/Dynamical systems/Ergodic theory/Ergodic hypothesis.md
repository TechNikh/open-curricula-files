---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodic_hypothesis
offline_file: ""
offline_thumbnail: ""
uuid: 01710400-e631-4d0d-9174-95f68156e5cc
updated: 1484308777
title: Ergodic hypothesis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ergodic_hypothesis_w_reflecting_rays.jpg
tags:
    - Phenomenology
    - Mathematics
categories:
    - Ergodic theory
---
In physics and thermodynamics, the ergodic hypothesis[1] says that, over long periods of time, the time spent by a system in some region of the phase space of microstates with the same energy is proportional to the volume of this region, i.e., that all accessible microstates are equiprobable over a long period of time.
