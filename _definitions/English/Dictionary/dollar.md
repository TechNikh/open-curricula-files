---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dollar
offline_file: ""
offline_thumbnail: ""
uuid: 7b609070-98e4-45fd-8085-33bab3be33d3
updated: 1484310447
title: dollar
categories:
    - Dictionary
---
dollar
     n 1: the basic monetary unit in many countries; equal to 100
          cents
     2: a piece of paper money worth one dollar [syn: {dollar bill},
         {one dollar bill}, {buck}, {clam}]
     3: a United States coin worth one dollar; "the dollar coin has
        never been popular in the United States"
     4: a symbol of commercialism or greed; "he worships the
        almighty dollar"; "the dollar sign means little to him"
        [syn: {dollar mark}, {dollar sign}]
