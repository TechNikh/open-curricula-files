---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/one-way
offline_file: ""
offline_thumbnail: ""
uuid: c8ddc8d2-7e09-4b4d-abad-1f3cf1e40758
updated: 1484310443
title: one-way
categories:
    - Dictionary
---
one-way
     adj : moving or permitting movement in one direction only;
           "one-way streets"; "a one-way ticket"
