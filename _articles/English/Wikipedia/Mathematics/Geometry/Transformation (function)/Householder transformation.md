---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Householder_transformation
offline_file: ""
offline_thumbnail: ""
uuid: b78e6132-b9e2-44d5-be71-470bcaeaea47
updated: 1484309334
title: Householder transformation
tags:
    - Definition and properties
    - Applications
    - Tridiagonalization
    - Examples
    - >
        Computational and theoretical relationship to other unitary
        transformations
categories:
    - Transformation (function)
---
In linear algebra, a Householder transformation (also known as Householder reflection or elementary reflector) is a linear transformation that describes a reflection about a plane or hyperplane containing the origin. Householder transformations are widely used in numerical linear algebra, to perform QR decompositions and is the first step of the QR algorithm. The Householder transformation was introduced in 1958 by Alston Scott Householder.[1]
