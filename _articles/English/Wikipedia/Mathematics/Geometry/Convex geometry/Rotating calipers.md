---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotating_calipers
offline_file: ""
offline_thumbnail: ""
uuid: 59dd8931-cb15-49f8-ac8a-d4d8176cf658
updated: 1484309197
title: Rotating calipers
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rotating_calipers%252C_finding_a_bridge_between_two_convex_polygons.svg.png'
tags:
    - History
    - "Shamos's algorithm"
    - Using monotone chain algorithm
    - Applications
    - Distances
    - Bounding boxes
    - Triangulations
    - Multi-Polygon operations
    - Traversals
    - Others
    - Minimum width of a convex polygon
categories:
    - Convex geometry
---
In computational geometry, the method of rotating calipers is an algorithm design technique that can be used to solve optimization problems including finding the width or diameter of a set of points.
