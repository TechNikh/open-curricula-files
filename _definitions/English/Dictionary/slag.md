---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slag
offline_file: ""
offline_thumbnail: ""
uuid: 879ada54-9cd9-409a-ae02-2858cfda0c4b
updated: 1484310411
title: slag
categories:
    - Dictionary
---
slag
     n : the scum formed by oxidation at the surface of molten metals
         [syn: {scoria}, {dross}]
     [also: {slagging}, {slagged}]
