---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isostructural
offline_file: ""
offline_thumbnail: ""
uuid: b330e7ea-39c8-4d8b-9458-1191950475cb
updated: 1484309277
title: Isostructural
categories:
    - Molecular geometry
---
Isostructural chemical compounds have similar chemical structures. Isomorphous when used in the relation to crystal structures is essentially synonymous.[1] The IUCR definition[2] used by crystallographers is:
