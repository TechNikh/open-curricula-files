---
version: 1
type: video
provider: YouTube
id: _b2cw6NGoOw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c4c27437-566a-445e-92dc-b7e0220ac2cc
updated: 1486069618
title: Similar triangle basics
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/_b2cw6NGoOw/default.jpg
    - https://i3.ytimg.com/vi/_b2cw6NGoOw/1.jpg
    - https://i3.ytimg.com/vi/_b2cw6NGoOw/2.jpg
    - https://i3.ytimg.com/vi/_b2cw6NGoOw/3.jpg
---
