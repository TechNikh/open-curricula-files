---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/categorized
offline_file: ""
offline_thumbnail: ""
uuid: 85a2efb6-5f3f-45b3-afe2-baffc8d47dba
updated: 1484310369
title: categorized
categories:
    - Dictionary
---
categorized
     adj : arranged into categories [syn: {categorised}]
