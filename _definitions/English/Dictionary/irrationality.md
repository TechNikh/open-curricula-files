---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irrationality
offline_file: ""
offline_thumbnail: ""
uuid: 2e7dcf78-6db6-4264-bfda-1b5f6c19be04
updated: 1484310142
title: irrationality
categories:
    - Dictionary
---
irrationality
     n : the state of being irrational; lacking powers of
         understanding [syn: {unreason}]
