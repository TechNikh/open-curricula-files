---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tramcar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484644201
title: tramcar
categories:
    - Dictionary
---
tramcar
     n 1: a four-wheeled wagon that runs on tracks in a mine; "a
          tramcar carries coal out of a coal mine" [syn: {tram}]
     2: a wheeled vehicle that runs on rails and is propelled by
        electricity; "`tram' and `tramcar' are British terms"
        [syn: {streetcar}, {tram}, {trolley}, {trolley car}]
