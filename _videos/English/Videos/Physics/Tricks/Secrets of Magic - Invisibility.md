---
version: 1
type: video
provider: YouTube
id: qh8XXQqwQIc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5358ecf5-cc83-4d00-97c5-9dae6c6e3054
updated: 1486069658
title: 'Secrets of Magic - Invisibility'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/qh8XXQqwQIc/default.jpg
    - https://i3.ytimg.com/vi/qh8XXQqwQIc/1.jpg
    - https://i3.ytimg.com/vi/qh8XXQqwQIc/2.jpg
    - https://i3.ytimg.com/vi/qh8XXQqwQIc/3.jpg
---
