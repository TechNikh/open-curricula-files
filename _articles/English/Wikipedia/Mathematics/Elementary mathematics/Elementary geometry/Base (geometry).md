---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Base_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 75b5ee4b-dfec-4d0e-a525-07a3e9f0f1ac
updated: 1484308921
title: Base (geometry)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Pyramid_coloured_base_%2528geometry%2529.png'
categories:
    - Elementary geometry
---
In geometry, a base is a side of a polygon or a face of a polyhedron, particularly one oriented perpendicular to the direction in which height is measured, or on what is considered to be the "bottom" of the figure.[1] This term is commonly applied to triangles, parallelograms, trapezoids, cylinders, cones, pyramids, parallelepipeds and frustums.
