---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Noncommutative_harmonic_analysis
offline_file: ""
offline_thumbnail: ""
uuid: e63e8aa0-3f4c-4b50-851d-a148fe2559a4
updated: 1484309234
title: Noncommutative harmonic analysis
categories:
    - Duality theories
---
In mathematics, noncommutative harmonic analysis is the field in which results from Fourier analysis are extended to topological groups that are not commutative.[1] Since locally compact abelian groups have a well-understood theory, Pontryagin duality, which includes the basic structures of Fourier series and Fourier transforms, the major business of non-commutative harmonic analysis is usually taken to be the extension of the theory to all groups G that are locally compact. The case of compact groups is understood, qualitatively and after the Peter–Weyl theorem from the 1920s, as being generally analogous to that of finite groups and their character theory.
