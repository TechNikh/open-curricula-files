---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hanging
offline_file: ""
offline_thumbnail: ""
uuid: 869726a2-7bc4-4a87-b05b-0611590c2972
updated: 1484310587
title: hanging
categories:
    - Dictionary
---
hanging
     n 1: decoration that is hung (as a tapestry) on a wall or over a
          window; "the cold castle walls were covered with
          hangings" [syn: {wall hanging}]
     2: a form of capital punishment; victim is suspended by the
        neck from a gallows or gibbet until dead; "in those days
        the hanging of criminals was a public entertainment"
     3: the act of suspending something (hanging it from above so it
        moves freely); "there was a small ceremony for the hanging
        of the portrait" [syn: {suspension}, {dangling}]
