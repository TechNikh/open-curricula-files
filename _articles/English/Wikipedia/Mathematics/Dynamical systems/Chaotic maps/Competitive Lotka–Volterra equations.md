---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Competitive_Lotka%E2%80%93Volterra_equations'
offline_file: ""
offline_thumbnail: ""
uuid: 1727a22c-60e1-4d75-bde9-48ca01273e61
updated: 1484308719
title: Competitive Lotka–Volterra equations
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-4D_competitive_LV_color.png
tags:
    - Overview
    - Two species
    - N species
    - Possible dynamics
    - 4-dimensional example
    - Spatial arrangements
    - Background
    - Matrix organization
    - Lyapunov functions
    - Line systems and eigenvalues
    - Notes
categories:
    - Chaotic maps
---
The competitive Lotka–Volterra equations are a simple model of the population dynamics of species competing for some common resource. They can be further generalised to include trophic interactions.
