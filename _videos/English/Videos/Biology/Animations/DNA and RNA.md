---
version: 1
type: video
provider: YouTube
id: z2oxK0wfyXY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20and%20RNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c7fae1ac-cb36-4182-89e4-6fec8aff9469
updated: 1486069670
title: DNA and RNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/z2oxK0wfyXY/default.jpg
    - https://i3.ytimg.com/vi/z2oxK0wfyXY/1.jpg
    - https://i3.ytimg.com/vi/z2oxK0wfyXY/2.jpg
    - https://i3.ytimg.com/vi/z2oxK0wfyXY/3.jpg
---
