---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calculate
offline_file: ""
offline_thumbnail: ""
uuid: 62c13aa6-d778-4e4a-8183-e311026c3237
updated: 1484310230
title: calculate
categories:
    - Dictionary
---
calculate
     v 1: make a mathematical calculation or computation [syn: {cipher},
           {cypher}, {compute}, {work out}, {reckon}, {figure}]
     2: judge to be probable [syn: {estimate}, {reckon}, {count on},
         {figure}, {forecast}]
     3: keep an account of [syn: {account}]
     4: predict in advance [syn: {forecast}]
     5: specifically design a product, event, or activity for a
        certain public [syn: {aim}, {direct}]
     6: have faith or confidence in; "you can count on me to help
        you any time"; "Look to your friends for support"; "You
        can bet on that!"; "Depend on your family in times of
        crisis" [syn: {count}, {bet}, {depend}, {look}, ...
