---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propagation
offline_file: ""
offline_thumbnail: ""
uuid: 01ab6a38-1991-43da-8f2f-5ea63285f36a
updated: 1484310224
title: propagation
categories:
    - Dictionary
---
propagation
     n 1: the spreading of something (a belief or practice) into new
          regions [syn: {extension}]
     2: the act of producing offspring or multiplying by such
        production [syn: {generation}, {multiplication}]
     3: the movement of a wave through a medium
