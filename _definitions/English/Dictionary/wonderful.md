---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wonderful
offline_file: ""
offline_thumbnail: ""
uuid: 2a6bbb3a-a064-4d11-8c5f-2d32e7c0d087
updated: 1484310359
title: wonderful
categories:
    - Dictionary
---
wonderful
     adj : extraordinarily good; used especially as intensifiers; "a
           fantastic trip to the Orient"; "the film was
           fantastic!"; "a howling success"; "a marvelous
           collection of rare books"; "had a rattling conversation
           about politics"; "a tremendous achievement" [syn: {fantastic},
            {howling(a)}, {marvelous}, {marvellous}, {rattling(a)},
            {terrific}, {tremendous}, {wondrous}]
