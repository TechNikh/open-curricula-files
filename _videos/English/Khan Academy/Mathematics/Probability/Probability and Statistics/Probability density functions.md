---
version: 1
type: video
provider: YouTube
id: Fvi9A_tEmXQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Probability%20density%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 41546b50-01fe-40d9-9d70-225124256722
updated: 1486069689
title: Probability density functions
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/Fvi9A_tEmXQ/default.jpg
    - https://i3.ytimg.com/vi/Fvi9A_tEmXQ/1.jpg
    - https://i3.ytimg.com/vi/Fvi9A_tEmXQ/2.jpg
    - https://i3.ytimg.com/vi/Fvi9A_tEmXQ/3.jpg
---
