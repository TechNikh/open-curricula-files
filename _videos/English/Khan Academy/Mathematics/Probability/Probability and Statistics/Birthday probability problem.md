---
version: 1
type: video
provider: YouTube
id: 9G0w61pZPig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Birthday%20probability%20problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 99d44419-7822-449d-ad59-2160fd500230
updated: 1486069689
title: Birthday probability problem
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/9G0w61pZPig/default.jpg
    - https://i3.ytimg.com/vi/9G0w61pZPig/1.jpg
    - https://i3.ytimg.com/vi/9G0w61pZPig/2.jpg
    - https://i3.ytimg.com/vi/9G0w61pZPig/3.jpg
---
