---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fine
offline_file: ""
offline_thumbnail: ""
uuid: 372754ad-820b-4a01-8489-7fee300e378f
updated: 1484310315
title: fine
categories:
    - Dictionary
---
fine
     adj 1: superior to the average; "in fine spirits"; "a fine
            student"; "made good grades"; "morale was good"; "had
            good weather for the parade" [syn: {good}]
     2: being satisfactory or in satisfactory condition; "an
        all-right movie"; "the passengers were shaken up but are
        all right"; "is everything all right?"; "everything's
        fine"; "things are okay"; "dinner and the movies had been
        fine"; "another minute I'd have been fine" [syn: {all
        right}, {ok}, {o.k.}, {okay}, {hunky-dory}]
     3: minutely precise especially in differences in meaning; "a
        fine distinction"
     4: of texture; being small-grained or smooth ...
