---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lime-water
offline_file: ""
offline_thumbnail: ""
uuid: 0bfe09b9-734b-45dc-9372-dc996f1789f1
updated: 1484308312
title: Limewater
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/140px-HCl_%252B_Ca%2528CO3%25292_expt.png'
tags:
    - chemistry
    - Applications
    - Industry
    - Water treatment
    - Arts
    - Other uses
categories:
    - Uncategorized
---
Limewater is the common name for a diluted solution of calcium hydroxide. Calcium hydroxide, Ca(OH)2, is sparsely soluble in water (1.5 g/L at 25 °C[1]). Pure limewater is clear and colorless, with a slight earthy smell and an alkaline bitter taste of calcium hydroxide. The term lime refers to the alkaline mineral, and is unrelated to the acidic fruit.
