---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximising_measure
offline_file: ""
offline_thumbnail: ""
uuid: fcf7fce1-3154-4227-ad38-2fe6cca50cad
updated: 1484308784
title: Maximising measure
categories:
    - Ergodic theory
---
In mathematics — specifically, in ergodic theory — a maximising measure is a particular kind of probability measure. Informally, a probability measure μ is a maximising measure for some function f if the integral of f with respect to μ is “as big as it can be”. The theory of maximising measures is relatively young and quite little is known about their general structure and properties.
