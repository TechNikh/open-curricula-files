---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonplussed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565421
title: nonplussed
categories:
    - Dictionary
---
nonplus
     v : be a mystery or bewildering to; "This beats me!"; "Got me--I
         don't know the answer!"; "a vexing problem"; "This
         question really stuck me" [syn: {perplex}, {vex}, {stick},
          {get}, {puzzle}, {mystify}, {baffle}, {beat}, {pose}, {bewilder},
          {flummox}, {stupefy}, {gravel}, {amaze}, {dumbfound}]
     [also: {nonplussing}, {nonplusses}, {nonplussed}]
