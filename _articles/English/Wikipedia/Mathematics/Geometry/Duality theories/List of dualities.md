---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_dualities
offline_file: ""
offline_thumbnail: ""
uuid: 1b1d70f8-cc6e-41a9-ae84-0ed31485ad9f
updated: 1484309232
title: List of dualities
tags:
    - Mathematics
    - Philosophy
    - 'Science: Engineering'
    - 'Science: Physics'
categories:
    - Duality theories
---
