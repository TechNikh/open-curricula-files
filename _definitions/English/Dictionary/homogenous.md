---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homogenous
offline_file: ""
offline_thumbnail: ""
uuid: f31f2e5f-71b4-472c-9fbb-cf3b297db4d1
updated: 1484310429
title: homogenous
categories:
    - Dictionary
---
homogenous
     adj : all of the same or similar kind or nature; "a close-knit
           homogeneous group" [syn: {homogeneous}] [ant: {heterogeneous}]
