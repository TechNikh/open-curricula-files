---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Markov_perfect_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 318bf2ec-6c98-4f6c-b7a2-d0f1e2b33a8c
updated: 1484309032
title: Markov perfect equilibrium
tags:
    - Definition
    - Focus on symmetric equilibria
    - Lack of robustness
    - Industrial organization extended example
    - Airfare game
    - Equilibrium
    - Discussion
    - Notes
    - Bibliography
categories:
    - Game theory equilibrium concepts
---
A Markov perfect equilibrium is an equilibrium concept in game theory. It is the refinement of the concept of subgame perfect equilibrium to extensive form games for which a pay-off relevant state space can be readily identified. The term appeared in publications starting about 1988 in the work of economists Jean Tirole and Eric Maskin.[1] It has since been used, among else, in the analysis of industrial organization, macroeconomics and political economy.
