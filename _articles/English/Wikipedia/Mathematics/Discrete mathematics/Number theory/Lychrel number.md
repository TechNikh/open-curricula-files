---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lychrel_number
offline_file: ""
offline_thumbnail: ""
uuid: a05eea69-6eaf-4f42-bfb4-9f73d39011e0
updated: 1484308649
title: Lychrel number
tags:
    - Reverse-and-add process
    - Proof not found
    - Threads, seed and kin numbers
    - 196 palindrome quest
    - Other bases
categories:
    - Number theory
---
A Lychrel number is a natural number that cannot form a palindrome through the iterative process of repeatedly reversing its digits and adding the resulting numbers. This process is sometimes called the 196-algorithm, after the most famous number associated with the process. In base ten, no Lychrel numbers have been yet proved to exist, but many, including 196, are suspected on heuristic[1] and statistical grounds. The name "Lychrel" was coined by Wade Van Landingham as a rough anagram of Cheryl, his girlfriend's first name.
