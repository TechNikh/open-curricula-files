---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bull
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561761
title: bull
categories:
    - Dictionary
---
bull
     n 1: uncastrated adult male of domestic cattle
     2: a large and strong and heavyset man; "he was a bull of a
        man"; "a thick-skinned bruiser ready to give as good as he
        got" [syn: {bruiser}, {strapper}, {Samson}]
     3: obscene words for unacceptable behavior; "I put up with a
        lot of bullshit from that jerk"; "what he said was mostly
        bull" [syn: {bullshit}, {Irish bull}, {horseshit}, {shit},
         {crap}, {dogshit}]
     4: a serious and ludicrous blunder; "he made a bad bull of the
        assignment"
     5: uncomplimentary terms for a policeman [syn: {cop}, {copper},
         {fuzz}, {pig}]
     6: an investor with an optimistic market ...
