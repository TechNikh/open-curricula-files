---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hook_length_formula
offline_file: ""
offline_thumbnail: ""
uuid: 0913c355-122d-4187-9c82-bdb931311d1c
updated: 1484308582
title: Hook length formula
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hook-length_tableau_December_8_2013_3.jpg
tags:
    - Definitions and statement
    - Example
    - History
    - Proofs
    - "Knuth's heuristic argument"
    - Probabilistic proof using the hook walk
    - Connection to representation theory
    - Detailed discussion
    - >
        Outline of the proof of the hook formula using Frobenius
        formula
    - Connection to longest increasing subsequences
    - Related formulas
    - Yet another proof of the hook length formula
    - Specialization of Schur functions
    - Skew shape formula
    - A formula for the Catalan numbers
categories:
    - Combinatorics
---
In combinatorial mathematics, the hook-length formula is a formula for the number of standard Young tableaux whose shape is a given Young diagram. It has applications in diverse areas such as representation theory, probability, and algorithm analysis; for example, the problem of longest increasing subsequences.
