---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shannon_number
offline_file: ""
offline_thumbnail: ""
uuid: 3ed115a4-b715-4a62-aef9-828c04748674
updated: 1484308533
title: Shannon number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Claude_Shannon_color_screengrab_2012-11.jpg
categories:
    - Combinatorial game theory
---
The Shannon number, named after Claude Shannon, is a conservative lower bound (not an estimate) of the game-tree complexity of chess of 10120, based on an average of about 103 possibilities for a pair of moves consisting of a move for White followed by one for Black, and a typical game lasting about 40 such pairs of moves. Shannon calculated it to demonstrate the impracticality of solving chess by brute force, in his 1950 paper "Programming a Computer for Playing Chess".[1] (This influential paper introduced the field of computer chess.)
