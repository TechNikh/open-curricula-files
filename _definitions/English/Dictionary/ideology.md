---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ideology
offline_file: ""
offline_thumbnail: ""
uuid: 0c063c14-999b-45b2-ac6c-6fdcfed40e2f
updated: 1484310553
title: ideology
categories:
    - Dictionary
---
ideology
     n 1: an orientation that characterizes the thinking of a group or
          nation [syn: {political orientation}, {political theory}]
     2: imaginary or visionary theorization
