---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Explicit_symmetry_breaking
offline_file: ""
offline_thumbnail: ""
uuid: 7d09afda-5a2c-4edd-aa77-aa2a9ec73835
updated: 1484309307
title: Explicit symmetry breaking
categories:
    - Symmetry
---
In theoretical physics, explicit symmetry breaking is the breaking of a symmetry of a theory by terms in its defining equations of motion (most typically, to the Lagrangian or the Hamiltonian) that do not respect the symmetry. Usually this term is used in situations where these symmetry-breaking terms are small, so that the symmetry is approximately respected by the theory. An example is the spectral line splitting in the Zeeman effect, due to a magnetic interaction perturbation in the Hamiltonian of the atoms involved.
