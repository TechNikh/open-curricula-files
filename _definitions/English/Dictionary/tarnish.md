---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tarnish
offline_file: ""
offline_thumbnail: ""
uuid: 3bffd2e2-74f6-46bd-8deb-37e2b8a27bb7
updated: 1484310375
title: tarnish
categories:
    - Dictionary
---
tarnish
     n : discoloration of metal surface caused by oxidation
     v : make dirty or spotty, as by exposure to air; also used
         metaphorically; "The silver was tarnished by the long
         exposure to the air"; "Her reputation was sullied after
         the affair with a married man" [syn: {stain}, {maculate},
          {sully}, {defile}]
