---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joint
offline_file: ""
offline_thumbnail: ""
uuid: 8b23334e-e747-4546-849c-ac5093486ec9
updated: 1484310595
title: joint
categories:
    - Dictionary
---
joint
     adj 1: united or combined; "a joint session of Congress"; "joint
            owners" [ant: {separate}]
     2: affecting or involving two or more; "joint income-tax
        return"; "joint ownership"
     3: involving both houses of a legislature; "a joint session of
        Congress"
     n 1: (anatomy) the point of connection between two bones or
          elements of a skeleton (especially if the articulation
          allows motion) [syn: {articulation}, {articulatio}]
     2: a disreputable place of entertainment
     3: the shape or manner in which things come together and a
        connection is made [syn: {articulation}, {join}, {juncture},
         {junction}]
     4: a ...
