---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organization
offline_file: ""
offline_thumbnail: ""
uuid: 2e810618-0b83-4b17-ad3c-b8c19c8d798f
updated: 1484310163
title: organization
categories:
    - Dictionary
---
organization
     n 1: a group of people who work together [syn: {organisation}]
     2: an organized structure for arranging or classifying; "he
        changed the arrangement of the topics"; "the facts were
        familiar but it was in the organization of them that he
        was original"; "he tried to understand their system of
        classification" [syn: {arrangement}, {organisation}, {system}]
     3: the persons (or committees or departments etc.) who make up
        a body for the purpose of administering something; "he
        claims that the present administration is corrupt"; "the
        governance of an association is responsible to its
        members"; "he quickly became ...
