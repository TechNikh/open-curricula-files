---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brittle
offline_file: ""
offline_thumbnail: ""
uuid: 5a7fb450-06b7-42f6-be62-b94092ea780f
updated: 1484310401
title: brittle
categories:
    - Dictionary
---
brittle
     adj 1: having little elasticity; hence easily cracked or fractured
            or snapped; "brittle bones"; "glass is brittle";
            "`brickle' and `brickly' are dialectal" [syn: {brickle},
             {brickly}]
     2: lacking warmth and generosity of spirit; "a brittle and
        calculating woman"
     3: (of metal or glass) not annealed and consequently easily
        cracked or fractured [syn: {unannealed}]
     n : caramelized sugar cooled in thin sheets [syn: {toffee}, {toffy}]
