---
version: 1
type: video
provider: YouTube
id: WU4qXBBPXWw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9ab7c200-3b84-4a25-8fae-f5fec675c2b7
updated: 1486069634
title: working model in Mathematics
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/WU4qXBBPXWw/default.jpg
    - https://i3.ytimg.com/vi/WU4qXBBPXWw/1.jpg
    - https://i3.ytimg.com/vi/WU4qXBBPXWw/2.jpg
    - https://i3.ytimg.com/vi/WU4qXBBPXWw/3.jpg
---
