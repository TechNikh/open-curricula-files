---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Newick_format
offline_file: ""
offline_thumbnail: ""
uuid: 1263ed53-a602-464e-8b51-609e980cc6ed
updated: 1484309409
title: Newick format
tags:
    - Examples
    - Rooted, unrooted, and binary trees
    - grammar
    - The grammar nodes
    - The grammar rules
categories:
    - Graph description languages
---
In mathematics, Newick tree format (or Newick notation or New Hampshire tree format) is a way of representing graph-theoretical trees with edge lengths using parentheses and commas. It was adopted by James Archie, William H. E. Day, Joseph Felsenstein, Wayne Maddison, Christopher Meacham, F. James Rohlf, and David Swofford, at two meetings in 1986, the second of which was at Newick's restaurant in Dover, New Hampshire, US. The adopted format is a generalization of the format developed by Meacham in 1984 for the first tree-drawing programs in Felsenstein's PHYLIP package.[1]
