---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mostly
offline_file: ""
offline_thumbnail: ""
uuid: a3d3d87a-531c-4617-8b86-f06cc5e0838d
updated: 1484310295
title: mostly
categories:
    - Dictionary
---
mostly
     adv 1: in large part; mainly or chiefly; "These accounts are
            largely inactive" [syn: {largely}, {for the most part}]
     2: usually; as a rule; "by and large it doesn't rain much here"
        [syn: {by and large}, {generally}, {more often than not}]
