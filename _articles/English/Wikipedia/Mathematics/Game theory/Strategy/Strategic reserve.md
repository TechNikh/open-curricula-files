---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_reserve
offline_file: ""
offline_thumbnail: ""
uuid: cc8c3726-7b36-435c-bf7a-8a3c432d7e64
updated: 1484309065
title: Strategic reserve
categories:
    - Strategy
---
A strategic reserve is the reserve of a commodity or items that is held back from normal use by governments, organisations, or businesses in pursuance of a particular strategy or to cope with unexpected events.
