---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fondements_de_la_G%C3%A9ometrie_Alg%C3%A9brique'
offline_file: ""
offline_thumbnail: ""
uuid: e4a95bba-5a6c-4ab2-be01-3a3d9c18a6ae
updated: 1484309147
title: Fondements de la Géometrie Algébrique
categories:
    - Algebraic geometry
---
Fondements de la Géometrie Algébrique, or FGA, is a book that collected together seminar notes of Alexander Grothendieck. It is an important source for his pioneering work on scheme theory, which laid foundations for algebraic geometry in its modern technical developments. The title is a translation of the title of Weil's book Foundations of Algebraic Geometry. It contained material on descent theory, and existence theorems including that for the Hilbert scheme. The Technique de descente et théorèmes d'existence en géometrie algébrique is one series of seminars within FGA.
