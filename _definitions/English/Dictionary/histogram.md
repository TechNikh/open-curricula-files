---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/histogram
offline_file: ""
offline_thumbnail: ""
uuid: 7728e718-522c-4d88-b681-a577796c2218
updated: 1484310154
title: histogram
categories:
    - Dictionary
---
histogram
     n : a bar chart representing a frequency distribution; heights
         of the bars represent observed frequencies
