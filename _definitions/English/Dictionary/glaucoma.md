---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glaucoma
offline_file: ""
offline_thumbnail: ""
uuid: 49d6df43-e664-4b2d-aced-a33e5c5ba0c9
updated: 1484310158
title: glaucoma
categories:
    - Dictionary
---
glaucoma
     n : increased pressure in the eyeball due to obstruction of the
         outflow of aqueous humor; damages the optic disc and
         impairs vision (sometimes progressing to blindness)
