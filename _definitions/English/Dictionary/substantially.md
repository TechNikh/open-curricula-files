---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substantially
offline_file: ""
offline_thumbnail: ""
uuid: 65fed85a-fe3c-4b2f-9f4a-489f81173984
updated: 1484310264
title: substantially
categories:
    - Dictionary
---
substantially
     adv 1: to a great extent or degree; "I'm afraid the film was well
            over budget"; "painting the room white made it seem
            considerably (or substantially) larger"; "the house
            has fallen considerably in value"; "the price went up
            substantially" [syn: {well}, {considerably}]
     2: in a strong substantial way; "the house was substantially
        built"
