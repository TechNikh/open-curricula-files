---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unpaid
offline_file: ""
offline_thumbnail: ""
uuid: 5103014e-5d0e-43a3-bdf0-e688fd1898ba
updated: 1484310453
title: unpaid
categories:
    - Dictionary
---
unpaid
     adj 1: not paid; "unpaid wages"; "an unpaid bill" [ant: {paid}]
     2: without payment; "the soup kitchen was run primarily by
        unpaid helpers"; "a volunteer fire department" [syn: {volunteer(a)}]
     3: engaged in as a pastime; "an amateur painter"; "gained
        valuable experience in amateur theatricals"; "recreational
        golfers"; "reading matter that is both recreational and
        mentally stimulating"; "unpaid extras in the documentary"
        [syn: {amateur}, {recreational}]
