---
version: 1
type: video
provider: YouTube
id: frBJEYvyd-8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Negative%20numbers%20and%20absolute%20value/Absolute%20value%20and%20number%20lines.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2f4601c5-bb3b-4dd4-9791-f6aad0fb2aee
updated: 1486069563
title: Absolute value and number lines
categories:
    - Negative numbers and absolute value
thumbnail_urls:
    - https://i3.ytimg.com/vi/frBJEYvyd-8/default.jpg
    - https://i3.ytimg.com/vi/frBJEYvyd-8/1.jpg
    - https://i3.ytimg.com/vi/frBJEYvyd-8/2.jpg
    - https://i3.ytimg.com/vi/frBJEYvyd-8/3.jpg
---
