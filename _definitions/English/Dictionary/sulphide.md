---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sulphide
offline_file: ""
offline_thumbnail: ""
uuid: 1a23b98e-8e6a-423a-84aa-b5743b9056d6
updated: 1484310409
title: sulphide
categories:
    - Dictionary
---
sulphide
     n : a compound of sulphur and some other element that is more
         electropositive [syn: {sulfide}]
