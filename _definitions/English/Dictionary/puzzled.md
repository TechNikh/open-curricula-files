---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/puzzled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484344801
title: puzzled
categories:
    - Dictionary
---
puzzled
     adj : filled with bewilderment; "at a loss to understand those
           remarks"; "puzzled that she left without saying
           goodbye" [syn: {at a loss(p)}, {nonplused}, {nonplussed}]
