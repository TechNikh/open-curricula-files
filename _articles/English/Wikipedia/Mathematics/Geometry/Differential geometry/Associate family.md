---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Associate_family
offline_file: ""
offline_thumbnail: ""
uuid: a9d1fbe1-cae1-4dc0-9fbf-02bb656afe60
updated: 1484309206
title: Associate family
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Helicatenoid.gif
categories:
    - Differential geometry
---
In differential geometry, the associate family (or Bonnet family) of a minimal surface is a one-parameter family of minimal surfaces which share the same Weierstrass data. That is, if the surface has the representation
