---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromagnetism
offline_file: ""
offline_thumbnail: ""
uuid: ce09b5fb-5e0a-4b52-8a02-82d570c1717c
updated: 1484308757
title: Electromagnetism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Bar_magnet.jpg
tags:
    - History of the theory
    - Fundamental forces
    - Classical electrodynamics
    - Quantum mechanics
    - Photoelectric effect
    - Quantum electrodynamics
    - Electroweak interaction
    - Quantities and units
    - Web sources
    - Textbooks
    - General references
categories:
    - Electrodynamics
---
Electromagnetism is a branch of physics which involves the study of the electromagnetic force, a type of physical interaction that occurs between electrically charged particles. The electromagnetic force usually exhibits electromagnetic fields, such as electric fields, magnetic fields, and light. The electromagnetic force is one of the four fundamental interactions (commonly called forces) in nature. The other three fundamental interactions are the strong interaction, the weak interaction, and gravitation.[1]
