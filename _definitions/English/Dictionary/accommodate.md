---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accommodate
offline_file: ""
offline_thumbnail: ""
uuid: dd4de2d2-d1d8-4a7b-ab35-98653bdf41f9
updated: 1484310401
title: accommodate
categories:
    - Dictionary
---
accommodate
     v 1: be agreeable or acceptable to; "This suits my needs" [syn: {suit},
           {fit}]
     2: make fit for, or change to suit a new purpose; "Adapt our
        native cuisine to the available food resources of the new
        country" [syn: {adapt}]
     3: provide with something desired or needed; "Can you
        accommodate me with a rental car?"
     4: have room for; hold without crowding; "This hotel can
        accommodate 250 guests"; "The theater admits 300 people";
        "The auditorium can't hold more than 500 people" [syn: {hold},
         {admit}]
     5: provide housing for; "We are lodging three foreign students
        this semester" [syn: {lodge}]
    ...
