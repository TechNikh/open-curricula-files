---
version: 1
type: video
provider: YouTube
id: d1oNF88SAgg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/6th%20grade/Ratios%2C%20rates%2C%20and%20percentages/Percent%20word%20problem%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a92b2b39-d335-464e-baca-558dd7896dac
updated: 1486069571
title: Percent word problem example 1
categories:
    - Ratios, rates, and percentages
thumbnail_urls:
    - https://i3.ytimg.com/vi/d1oNF88SAgg/default.jpg
    - https://i3.ytimg.com/vi/d1oNF88SAgg/1.jpg
    - https://i3.ytimg.com/vi/d1oNF88SAgg/2.jpg
    - https://i3.ytimg.com/vi/d1oNF88SAgg/3.jpg
---
We're putting a little algebra to work to find the full price when you know the discount price in this percent word problem.

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-ratios-prop-topic/cc-6th-percent-word-problems/e/percentage_word_problems_1?utm_source=YT&utm_medium=Desc&utm_campaign=6thgrade

Watch the next lesson: https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-ratios-prop-topic/cc-6th-percent-word-problems/v/percent-word-problems?utm_source=YT&utm_medium=Desc&utm_campaign=6thgrade

Missed the previous lesson? https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-ratios-prop-topic/cc-6th-percent-problems/v/taking-a-percentage-example?utm_source=YT&utm_medium=Desc&utm_campaign=6thgrade

Grade 6th on Khan Academy: By the 6th grade, you're becoming a sophisticated mathemagician. You'll be able to add, subtract, multiply, and divide any non-negative numbers (including decimals and fractions) that any grumpy ogre throws at you. Mind-blowing ideas like exponents (you saw these briefly in the 5th grade), ratios, percents, negative numbers, and variable expressions will start being in your comfort zone. Most importantly, the algebraic side of mathematics is a whole new kind of fun! And if that is not enough, we are going to continue with our understanding of ideas like the coordinate plane (from 5th grade) and area while beginning to derive meaning from data! (Content was selected for this grade level based on a typical curriculum in the United States.)

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan AcademyÂÃÂªs 6th grade channel:
https://www.youtube.com/channel/UCnif494Ay2S-PuYlDVrOwYQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
