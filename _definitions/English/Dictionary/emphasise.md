---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphasise
offline_file: ""
offline_thumbnail: ""
uuid: 8dc8468d-7e50-465b-8be6-82929b59d498
updated: 1484310181
title: emphasise
categories:
    - Dictionary
---
emphasise
     v 1: give extra weight to (a communication); "Her gesture
          emphasized her words" [syn: {underscore}, {underline}, {emphasize}]
     2: to stress, single out as important; "Dr. Jones emphasizes
        exercise in addition to a change in diet" [syn: {stress},
        {emphasize}, {punctuate}, {accent}, {accentuate}]
