---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marvelled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467381
title: marvelled
categories:
    - Dictionary
---
marvel
     n : something that causes feelings of wonder; "the wonders of
         modern science" [syn: {wonder}]
     v 1: be amazed at; "We marvelled at the child's linguistic
          abilities" [syn: {wonder}]
     2: express astonishment or surprise about something
     [also: {marvelling}, {marvelled}]
