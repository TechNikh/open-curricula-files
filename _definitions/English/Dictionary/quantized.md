---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantized
offline_file: ""
offline_thumbnail: ""
uuid: eec0fa43-69c7-4905-bc6b-abc5145ac98f
updated: 1484310389
title: quantized
categories:
    - Dictionary
---
quantized
     adj : of or relating to a quantum or capable of existing in only
           one of two states [syn: {quantal}]
