---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rat
offline_file: ""
offline_thumbnail: ""
uuid: cb8d3e72-0384-42a7-895e-383754e88a87
updated: 1484310162
title: rat
categories:
    - Dictionary
---
rat
     n 1: any of various long-tailed rodents similar to but larger
          than a mouse
     2: someone who works (or provides workers) during a strike
        [syn: {scab}, {strikebreaker}, {blackleg}]
     3: a person who is deemed to be despicable or contemptible;
        "only a rotter would do that"; "kill the rat"; "throw the
        bum out"; "you cowardly little pukes!"; "the British call
        a contemptible person a `git'" [syn: {rotter}, {dirty dog},
         {skunk}, {stinker}, {stinkpot}, {bum}, {puke}, {crumb}, {lowlife},
         {scum bag}, {so-and-so}, {git}]
     4: one who reveals confidential information in return for money
        [syn: {informer}, {betrayer}, ...
