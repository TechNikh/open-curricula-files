---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/age
offline_file: ""
offline_thumbnail: ""
uuid: fae4e234-a372-48a6-bfab-9b50e093bb0b
updated: 1484310281
title: age
categories:
    - Dictionary
---
age
     n 1: how long something has existed; "it was replaced because of
          its age"
     2: an era of history having some distinctive feature; "we live
        in a litigious age" [syn: {historic period}]
     3: a time in life (usually defined in years) at which some
        particular qualification or power arises; "she was now of
        school age"; "tall for his eld" [syn: {eld}]
     4: a late time of life; "old age is not for sissies"; "he's
        showing his years"; "age hasn't slowed him down at all";
        "a beard white with eld"; "on the brink of geezerhood"
        [syn: {old age}, {years}, {eld}, {geezerhood}]
     5: a prolonged period of time; "we've known each ...
