---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Completing_the_square
offline_file: ""
offline_thumbnail: ""
uuid: be810cde-28d8-4547-8162-86b9e5a61973
updated: 1484308905
title: Completing the square
tags:
    - Overview
    - Background
    - Basic example
    - General description
    - Non-monic case
    - Formula
    - Relation to the graph
    - Solving quadratic equations
    - Irrational and complex roots
    - Non-monic case
    - Other applications
    - Integration
    - Complex numbers
    - Idempotent matrix
    - Geometric perspective
    - A variation on the technique
    - 'Example: the sum of a positive number and its reciprocal'
    - 'Example: factoring a simple quartic polynomial'
categories:
    - Elementary algebra
---
to the form
