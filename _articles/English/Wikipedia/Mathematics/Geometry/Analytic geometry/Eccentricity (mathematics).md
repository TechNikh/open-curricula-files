---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eccentricity_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: e9f083e2-b15c-424a-8299-6d34800f8582
updated: 1484309165
title: Eccentricity (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Eccentricity.svg.png
tags:
    - Definitions
    - Alternative names
    - Notation
    - Values
    - Ellipses
    - Other formulae for the eccentricity of an ellipse
    - Hyperbolas
    - Quadrics
    - Celestial mechanics
    - Analogous classifications
    - Eccentricity for data shapes
categories:
    - Analytic geometry
---
In mathematics, the eccentricity, denoted e or 
  
    
      
        ε
      
    
    {\displaystyle \varepsilon }
  
, is a parameter associated with every conic section. It can be thought of as a measure of how much the conic section deviates from being circular.
