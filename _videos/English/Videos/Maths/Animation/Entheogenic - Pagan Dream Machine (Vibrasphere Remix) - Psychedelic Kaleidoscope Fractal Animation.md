---
version: 1
type: video
provider: YouTube
id: weX9i831gSU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e4b56088-6eaa-41d0-9639-78aa81bed244
updated: 1486069647
title: 'Entheogenic - Pagan Dream Machine (Vibrasphere Remix) - Psychedelic Kaleidoscope Fractal Animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/weX9i831gSU/default.jpg
    - https://i3.ytimg.com/vi/weX9i831gSU/1.jpg
    - https://i3.ytimg.com/vi/weX9i831gSU/2.jpg
    - https://i3.ytimg.com/vi/weX9i831gSU/3.jpg
---
