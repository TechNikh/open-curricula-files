---
version: 1
type: video
provider: YouTube
id: ahANuQQmuak
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/SRP%20-%20Signal%20Recognition%20Particle%20binds%20nascent%20peptide%20from%20ribosome.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d8e900b8-0729-4cd5-a948-e0df80140a75
updated: 1486069668
title: 'SRP - Signal Recognition Particle binds nascent peptide from ribosome'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ahANuQQmuak/default.jpg
    - https://i3.ytimg.com/vi/ahANuQQmuak/1.jpg
    - https://i3.ytimg.com/vi/ahANuQQmuak/2.jpg
    - https://i3.ytimg.com/vi/ahANuQQmuak/3.jpg
---
