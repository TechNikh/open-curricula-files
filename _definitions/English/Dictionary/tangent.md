---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tangent
offline_file: ""
offline_thumbnail: ""
uuid: 894fab6c-2080-484e-b27f-d2fc57efd016
updated: 1484310220
title: tangent
categories:
    - Dictionary
---
tangent
     n 1: a straight line or plane that touches a curve or curved
          surface at a point but does not intersect it at that
          point
     2: ratio of the opposite to the adjacent side of a right-angled
        triangle [syn: {tan}]
