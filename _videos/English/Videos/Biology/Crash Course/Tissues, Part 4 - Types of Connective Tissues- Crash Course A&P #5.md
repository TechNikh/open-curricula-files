---
version: 1
type: video
provider: YouTube
id: Jvtb0a2RXaY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Tissues%2C%20Part%204%20-%20Types%20of%20Connective%20Tissues-%20Crash%20Course%20A%26P%20%235.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 10552b35-de08-4976-aedc-6d84151234b1
updated: 1486069680
title: 'Tissues, Part 4 - Types of Connective Tissues- Crash Course A&P #5'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/Jvtb0a2RXaY/default.jpg
    - https://i3.ytimg.com/vi/Jvtb0a2RXaY/1.jpg
    - https://i3.ytimg.com/vi/Jvtb0a2RXaY/2.jpg
    - https://i3.ytimg.com/vi/Jvtb0a2RXaY/3.jpg
---
