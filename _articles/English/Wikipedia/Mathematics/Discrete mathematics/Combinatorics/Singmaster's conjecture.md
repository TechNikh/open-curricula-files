---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Singmaster%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 36de518b-298a-4465-9276-354317825b13
updated: 1484308592
title: "Singmaster's conjecture"
tags:
    - Known results
    - Numerical examples
    - Do any numbers appear exactly five or seven times?
categories:
    - Combinatorics
---
Singmaster's conjecture is a conjecture in combinatorial number theory in mathematics, named after the British mathematician David Singmaster who proposed it in 1971. It says that there is a finite upper bound on the multiplicities of entries in Pascal's triangle (other than the number 1, which appears infinitely many times). It is clear that the only number that appears infinitely many times in Pascal's triangle is 1, because any other number x can appear only within the first x + 1 rows of the triangle. Paul Erdős said that Singmaster's conjecture is probably true but he suspected it would be very hard to prove.
