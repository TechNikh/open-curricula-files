---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chile
offline_file: ""
offline_thumbnail: ""
uuid: 2e250f34-0e4d-4c3b-a89f-64869c2253de
updated: 1484310475
title: chile
categories:
    - Dictionary
---
Chile
     n 1: a republic in southern South America on the western slopes
          of the Andes on the south Pacific coast [syn: {Republic
          of Chile}]
     2: very hot and finely tapering pepper of special pungency
        [syn: {chili}, {chili pepper}, {chilli}, {chilly}]
