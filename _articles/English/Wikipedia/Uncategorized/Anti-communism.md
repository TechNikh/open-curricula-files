---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anti-communist
offline_file: ""
offline_thumbnail: ""
uuid: 6a48ee16-ae07-4ad3-9d5a-88791863e0a0
updated: 1484308479
title: Anti-communism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Thecristisrizenoldrussiancivilwarposter.jpg
tags:
    - Anti-communist movements
    - Left-wing anti-communism
    - Anarchists
    - Classical liberals
    - Objectivism
    - Ex-communists
    - Fascism and far-right politics
    - Other
    - Religions
    - Buddhists
categories:
    - Uncategorized
---
Anti-communism is opposition to communism. Organized anti-communism developed in reaction to the rise of communism, especially after the 1917 October Revolution in Russia. It reached global dimensions during the Cold War, when America and the Soviet Union engaged in an intense rivalry.
