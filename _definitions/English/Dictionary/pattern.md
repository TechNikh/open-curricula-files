---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pattern
offline_file: ""
offline_thumbnail: ""
uuid: 305037cf-760f-4277-a828-8c75dfad5756
updated: 1484310284
title: pattern
categories:
    - Dictionary
---
pattern
     n 1: a perceptual structure; "the composition presents problems
          for students of musical form"; "a visual pattern must
          include not only objects but the spaces between them"
          [syn: {form}, {shape}]
     2: a customary way of operation or behavior; "it is their
        practice to give annual raises"; "they changed their
        dietary pattern" [syn: {practice}]
     3: a decorative or artistic work; "the coach had a design on
        the doors" [syn: {design}, {figure}]
     4: something regarded as a normative example; "the convention
        of not naming the main character"; "violence is the rule
        not the exception"; "his formula for ...
