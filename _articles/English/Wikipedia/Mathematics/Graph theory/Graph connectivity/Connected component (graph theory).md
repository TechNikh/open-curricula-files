---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Connected_component_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 97e4fd15-3cc1-4311-8a7c-620761280420
updated: 1484309402
title: Connected component (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Pseudoforest.svg.png
tags:
    - An equivalence relation
    - The number of connected components
    - Algorithms
categories:
    - Graph connectivity
---
In graph theory, a connected component (or just component) of an undirected graph is a subgraph in which any two vertices are connected to each other by paths, and which is connected to no additional vertices in the supergraph. For example, the graph shown in the illustration on the right has three connected components. A vertex with no incident edges is itself a connected component. A graph that is itself connected has exactly one connected component, consisting of the whole graph.
