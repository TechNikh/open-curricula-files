---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rock
offline_file: ""
offline_thumbnail: ""
uuid: 8c90ff45-93f3-450c-9dd8-0d8a21e47f87
updated: 1484310281
title: rock
categories:
    - Dictionary
---
rock
     n 1: a lump or mass of hard consolidated mineral matter; "he
          threw a rock at me" [syn: {stone}]
     2: material consisting of the aggregate of minerals like those
        making up the Earth's crust; "that mountain is solid
        rock"; "stone is abundant in New England and there are
        many quarries" [syn: {stone}]
     3: United States gynecologist and devout Catholic who conducted
        the first clinical trials of the oral contraceptive pill
        (1890-1984) [syn: {John Rock}]
     4: (figurative) someone who is strong and stable and
        dependable; "he was her rock during the crisis"; "Thou art
        Peter, and upon this rock I will build my ...
