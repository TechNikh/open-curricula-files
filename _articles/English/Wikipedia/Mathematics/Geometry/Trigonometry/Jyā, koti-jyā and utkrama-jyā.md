---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jy%C4%81,_koti-jy%C4%81_and_utkrama-jy%C4%81'
offline_file: ""
offline_thumbnail: ""
uuid: cd1b27fe-626a-4d7c-b4ad-8c9f01fbdde9
updated: 1484309341
title: Jyā, koti-jyā and utkrama-jyā
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Modern_diagram_for_jya_and_kojya.svg.png
tags:
    - Definition
    - Terminology
    - From jyā to sine
categories:
    - Trigonometry
---
Jyā, koti-jyā and utkrama-jyā are three trigonometric functions introduced by Indian mathematicians and astronomers. The earliest known Indian treatise containing references to these functions is Surya Siddhanta.[1] These are functions of arcs of circles and not functions of angles. Jyā and koti-jyā are closely related to the modern trigonometric functions of sine and cosine. In fact, the origins of the modern terms of "sine" and "cosine" have been traced back to the Sanskrit words jyā and koti-jyā.[1]
