---
version: 1
type: video
provider: YouTube
id: PCYv0_qPk-4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Wave%20Interference.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 59bd15a6-a057-4b05-9736-312fbc7ed5cb
updated: 1486069651
title: Wave Interference
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/PCYv0_qPk-4/default.jpg
    - https://i3.ytimg.com/vi/PCYv0_qPk-4/1.jpg
    - https://i3.ytimg.com/vi/PCYv0_qPk-4/2.jpg
    - https://i3.ytimg.com/vi/PCYv0_qPk-4/3.jpg
---
