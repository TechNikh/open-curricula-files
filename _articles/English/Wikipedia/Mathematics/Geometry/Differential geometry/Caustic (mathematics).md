---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caustic_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: ad5ac44e-dc74-433c-bf29-0f8f523269f4
updated: 1484309207
title: Caustic (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Circle_caustic.png
tags:
    - Catacaustic
    - Example
categories:
    - Differential geometry
---
In differential geometry and geometric optics, a caustic is the envelope of rays either reflected or refracted by a manifold. It is related to the concept of caustics in optics. The ray's source may be a point (called the radiant) or parallel rays from a point at infinity, in which case a direction vector of the rays must be specified.
