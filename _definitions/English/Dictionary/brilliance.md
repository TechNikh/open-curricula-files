---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brilliance
offline_file: ""
offline_thumbnail: ""
uuid: bad81188-e1ed-4de5-be64-c7b4a898900b
updated: 1484310210
title: brilliance
categories:
    - Dictionary
---
brilliance
     n 1: great brightness; "a glare of sunlight"; "the flowers were a
          blaze of color" [syn: {glare}, {blaze}]
     2: the quality of being magnificent or splendid or grand [syn:
        {magnificence}, {splendor}, {splendour}, {grandeur}, {grandness}]
     3: unusual mental ability [syn: {genius}]
