---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/initiative
offline_file: ""
offline_thumbnail: ""
uuid: b70313c3-4b5a-412d-ad56-f32543914eea
updated: 1484310469
title: initiative
categories:
    - Dictionary
---
initiative
     adj : serving to set in motion; "the magazine's inaugural issue";
           "the initiative phase in the negotiations"; "an
           initiatory step toward a treaty"; "his first (or
           maiden) speech in Congress"; "the liner's maiden
           voyage" [syn: {inaugural}, {initiatory}, {first}, {maiden}]
     n 1: readiness to embark on bold new ventures [syn: {enterprise},
           {enterprisingness}, {go-ahead}]
     2: the first of a series of actions; "he memorized all the
        important chess openings" [syn: {first step}, {opening
        move}, {opening}]
