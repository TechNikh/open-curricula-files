---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Category_of_metric_spaces
offline_file: ""
offline_thumbnail: ""
uuid: 08de3e86-0335-4cff-80b8-7add0b4540ef
updated: 1484309262
title: Category of metric spaces
tags:
    - Arrows
    - Objects
    - Products and functors
    - Related categories
categories:
    - Metric geometry
---
In category-theoretic mathematics, Met is a category that has metric spaces as its objects and metric maps (continuous functions between metric spaces that do not increase any pairwise distance) as its morphisms. This is a category because the composition of two metric maps is again a metric map. It was first considered by Isbell (1964).
