---
version: 1
type: video
provider: YouTube
id: jTCZfMMcHBo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Interpreting%20linear%20tables%20word%20problems%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0b55530e-c716-4dcf-ae48-653a1cd8bb14
updated: 1486069561
title: Interpreting linear tables word problems example 1
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/jTCZfMMcHBo/default.jpg
    - https://i3.ytimg.com/vi/jTCZfMMcHBo/1.jpg
    - https://i3.ytimg.com/vi/jTCZfMMcHBo/2.jpg
    - https://i3.ytimg.com/vi/jTCZfMMcHBo/3.jpg
---
