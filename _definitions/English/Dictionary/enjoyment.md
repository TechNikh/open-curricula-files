---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enjoyment
offline_file: ""
offline_thumbnail: ""
uuid: 06fddda5-227e-4db7-bc07-1dcba8e29c9d
updated: 1484310547
title: enjoyment
categories:
    - Dictionary
---
enjoyment
     n 1: the pleasure felt when having a good time
     2: act of receiving pleasure from something [syn: {delectation}]
     3: (law) the exercise of the legal right to enjoy the benefits
        of owning property; "we were given the use of his boat"
        [syn: {use}]
