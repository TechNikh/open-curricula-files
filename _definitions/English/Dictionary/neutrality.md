---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neutrality
offline_file: ""
offline_thumbnail: ""
uuid: feb88fef-d26b-4153-93e1-78073faa8866
updated: 1484310609
title: neutrality
categories:
    - Dictionary
---
neutrality
     n 1: nonparticipation in a dispute or war
     2: tolerance attributable to a lack of involvement [syn: {disinterest}]
     3: pH value of 7
