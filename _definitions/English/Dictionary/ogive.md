---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ogive
offline_file: ""
offline_thumbnail: ""
uuid: 1b1fcba3-66b0-455f-a7b3-bd88431df3ae
updated: 1484310154
title: ogive
categories:
    - Dictionary
---
ogive
     n : front consisting of the conical head of a missile or rocket
         that protects the payload from heat during its passage
         through the atmosphere [syn: {nose cone}]
