---
version: 1
type: video
provider: YouTube
id: D4dj34MYvnY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Lithium%20Metal%20Explosions%20and%20Fails%21%20Reaction%20with%20water%20Compilation%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 57c5549f-e170-4bba-9719-a7c9b524bce5
updated: 1486069661
title: >
    Lithium Metal Explosions and Fails! Reaction with water
    Compilation!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/D4dj34MYvnY/default.jpg
    - https://i3.ytimg.com/vi/D4dj34MYvnY/1.jpg
    - https://i3.ytimg.com/vi/D4dj34MYvnY/2.jpg
    - https://i3.ytimg.com/vi/D4dj34MYvnY/3.jpg
---
