---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wits
offline_file: ""
offline_thumbnail: ""
uuid: 213136c2-e4fd-4a9b-bbde-03623177e3e4
updated: 1484310144
title: wits
categories:
    - Dictionary
---
wits
     n : the basic human power of intelligent thought and perception;
         "he used his wits to get ahead"; "I was scared out of my
         wits"; "he still had all his marbles and was in full
         possession of a lively mind" [syn: {marbles}]
