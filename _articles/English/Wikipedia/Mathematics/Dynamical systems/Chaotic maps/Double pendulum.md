---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_pendulum
offline_file: ""
offline_thumbnail: ""
uuid: 3be3b933-6a68-47a2-83a4-26730a643fa3
updated: 1484308713
title: Double pendulum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Double-Pendulum.svg.png
tags:
    - Analysis and interpretation
    - Lagrangian
    - Chaotic motion
    - Notes
categories:
    - Chaotic maps
---
In physics and mathematics, in the area of dynamical systems, a double pendulum is a pendulum with another pendulum attached to its end, and is a simple physical system that exhibits rich dynamic behavior with a strong sensitivity to initial conditions.[1] The motion of a double pendulum is governed by a set of coupled ordinary differential equations and is chaotic.
