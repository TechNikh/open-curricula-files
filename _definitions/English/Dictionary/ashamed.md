---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ashamed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484491801
title: ashamed
categories:
    - Dictionary
---
ashamed
     adj : used of persons; feeling shame or guilt or embarrassment or
           remorse; "are you ashamed for having lied?"; "felt
           ashamed of my torn coat" [syn: {ashamed(p)}] [ant: {unashamed}]
