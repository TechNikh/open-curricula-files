---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fortuitous
offline_file: ""
offline_thumbnail: ""
uuid: eab146c9-2a7b-4708-b5e3-140aee6a750a
updated: 1484310311
title: fortuitous
categories:
    - Dictionary
---
fortuitous
     adj 1: having no cause or apparent cause; "a causeless miracle";
            "fortuitous encounters--strange accidents of fortune";
            "we cannot regard artistic invention as...uncaused and
            unrelated to the times" [syn: {causeless}, {uncaused}]
     2: occurring by happy chance; "profits were enhanced by a
        fortuitous drop in the cost of raw materials"
