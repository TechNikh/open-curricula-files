---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yuri_Burago
offline_file: ""
offline_thumbnail: ""
uuid: 1580b629-6581-4ca3-b3ea-8773fefe9d5f
updated: 1484309077
title: Yuri Burago
tags:
    - Education and career
    - Works
    - Students
    - Footnotes
categories:
    - Geometers
---
Yuri Dmitrievich Burago (Russian: Ю́рий Дми́триевич Бура́го) (born 1936) is a Russian mathematician. He works in differential and convex geometry.
