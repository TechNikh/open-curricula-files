---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geography
offline_file: ""
offline_thumbnail: ""
uuid: 7f0c9fe9-a8cc-49cf-b863-805cebb3678f
updated: 1484310475
title: geography
categories:
    - Dictionary
---
geography
     n : study of the earth's surface; includes people's responses to
         topography and climate and soil and vegetation [syn: {geographics}]
