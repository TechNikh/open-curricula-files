---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insectivorous
offline_file: ""
offline_thumbnail: ""
uuid: b308bdaf-4680-4b63-9a1b-bd97c40859ce
updated: 1484310273
title: insectivorous
categories:
    - Dictionary
---
insectivorous
     adj : (of animals and plants) feeding on insects [ant: {carnivorous},
            {herbivorous}, {omnivorous}]
