---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harm
offline_file: ""
offline_thumbnail: ""
uuid: e38c70a3-6015-4a05-83e7-44e8ce424202
updated: 1484310246
title: harm
categories:
    - Dictionary
---
harm
     n 1: any physical damage to the body caused by violence or
          accident or fracture etc. [syn: {injury}, {hurt}, {trauma}]
     2: the occurrence of a change for the worse [syn: {damage}, {impairment}]
     3: the act of damaging something or someone [syn: {damage}, {hurt},
         {scathe}]
     v : cause or do harm to; "These pills won't harm your system"
