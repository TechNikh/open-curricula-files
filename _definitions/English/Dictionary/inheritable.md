---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inheritable
offline_file: ""
offline_thumbnail: ""
uuid: c453e1b1-7c31-4a7a-a964-2b75c53e7400
updated: 1484310297
title: inheritable
categories:
    - Dictionary
---
inheritable
     adj : that can be inherited; "inheritable traits such as eye
           color"; "an inheritable title" [syn: {heritable}] [ant:
            {noninheritable}]
