---
version: 1
type: video
provider: YouTube
id: Ef22yTJDUZI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Optimization%20with%20Calculus%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 63ffceea-7c45-4ccc-9e45-1086b0841c85
updated: 1486069693
title: Optimization with Calculus 1
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ef22yTJDUZI/default.jpg
    - https://i3.ytimg.com/vi/Ef22yTJDUZI/1.jpg
    - https://i3.ytimg.com/vi/Ef22yTJDUZI/2.jpg
    - https://i3.ytimg.com/vi/Ef22yTJDUZI/3.jpg
---
