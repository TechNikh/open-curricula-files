---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/keenly
offline_file: ""
offline_thumbnail: ""
uuid: 9230d00c-424b-42ae-8be4-9c210ab40802
updated: 1484310290
title: keenly
categories:
    - Dictionary
---
keenly
     adv : in a keen and discriminating manner; "he was keenly aware of
           his own shortcomings"
