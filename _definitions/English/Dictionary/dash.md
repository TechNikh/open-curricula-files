---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dash
offline_file: ""
offline_thumbnail: ""
uuid: 4e9635ca-1f6f-4954-8a83-f564609f0c47
updated: 1484310559
title: dash
categories:
    - Dictionary
---
dash
     n 1: distinctive and stylish elegance; "he wooed her with the
          confident dash of a cavalry officer" [syn: {elan}, {flair},
           {panache}, {style}]
     2: a quick run [syn: {sprint}]
     3: a footrace run at top speed; "he is preparing for the
        100-yard dash"
     4: a punctuation mark (-) used between parts of a compound word
        or between the syllables of a word when the word is
        divided at the end of a line of text [syn: {hyphen}]
     5: the longer of the two telegraphic signals used in Morse code
        [syn: {dah}]
     6: the act of moving with great haste; "he made a dash for the
        door" [syn: {bolt}]
     v 1: run or move very ...
