---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hadamard_code
offline_file: ""
offline_thumbnail: ""
uuid: fabc93e5-ac20-4961-8cd6-235fc38f96ac
updated: 1484308558
title: Hadamard code
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hadamard-Code.svg.png
tags:
    - History
    - Constructions
    - Construction using inner products
    - Construction using a generator matrix
    - Construction using general Hadamard matrices
    - distance
    - Local decodability
    - Proof of lemma 1
    - Proof of theorem 1
    - Algorithm
    - Proof of correctness
    - Optimality
    - Notes
categories:
    - Coding theory
---
The Hadamard code is an error-correcting code that is used for error detection and correction when transmitting messages over very noisy or unreliable channels. In 1971, the code was used to transmit photos of Mars back to Earth from the NASA space probe Mariner 9.[1] Because of its unique mathematical properties, the Hadamard code is not only used by engineers, but also intensely studied in coding theory, mathematics, and theoretical computer science. The Hadamard code is named after the French mathematician Jacques Hadamard. It is also known under the names Walsh code, Walsh family,[2] and Walsh–Hadamard code[3] in recognition of the American mathematician Joseph Leonard Walsh.
