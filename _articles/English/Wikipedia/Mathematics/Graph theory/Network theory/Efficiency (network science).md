---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Efficiency_(network_science)
offline_file: ""
offline_thumbnail: ""
uuid: 27c741d5-42ac-4ee4-81f8-197f72dca327
updated: 1484309435
title: Efficiency (network science)
categories:
    - Network theory
---
In network science, the efficiency of a network is a measure of how efficiently it exchanges information. [1] The concept of efficiency can be applied to both local and global scales in a network. On a global scale, efficiency quantifies the exchange of information across the whole network where information is concurrently exchanged. The local efficiency quantifies a network's resistance to failure on a small scale. That is the local efficiency of a node 
  
    
      
        i
      
    
    {\displaystyle i}
  
 characterizes how well information is exchanged by its neighbors when it is removed.
