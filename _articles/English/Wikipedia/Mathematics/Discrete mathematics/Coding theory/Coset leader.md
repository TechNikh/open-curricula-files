---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coset_leader
offline_file: ""
offline_thumbnail: ""
uuid: 9c6cc0dd-e9e8-4f36-b0a1-48681e5a0838
updated: 1484308553
title: Coset leader
categories:
    - Coding theory
---
In coding theory, a coset leader is a word of minimum weight in any particular coset - that is, a word with the lowest amount of non-zero entries. Sometimes there are several words of equal minimum weight in a coset, and in that case, any one of those words may be chosen to be the coset leader.
