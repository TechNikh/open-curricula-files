---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cornea_transplant
offline_file: ""
offline_thumbnail: ""
uuid: c0f587d9-4148-42bd-85c0-bc4b5524fe9b
updated: 1484308368
title: Corneal transplantation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Cornea_transplant.jpg
tags:
    - Medical uses
    - Risks
    - Procedure
    - Pre-operative examination
    - Penetrating keratoplasty
    - Lamellar keratoplasty
    - Deep anterior lamellar keratoplasty
    - Endothelial keratoplasty
    - Synthetic corneas
    - Boston keratoprosthesis
    - AlphaCor
    - Osteo-Odonto-Keratoprosthesis
    - Prognosis
    - Alternatives
    - Contact lenses
    - Phototherapeutic keratectomy
    - Intrastromal corneal ring segments
    - Corneal collagen cross-linking
    - Epidemiology
    - History
    - Research
    - High speed lasers
    - DSEK/DSAEK/DMEK
    - Stem cells
    - Biosynthetic corneas
    - Society and culture
    - Cost
categories:
    - Uncategorized
---
Corneal transplantation, also known as corneal grafting, is a surgical procedure where a damaged or diseased cornea is replaced by donated corneal tissue (the graft). When the entire cornea is replaced it is known as penetrating keratoplasty and when only part of the cornea is replaced it is known as lamellar keratoplasty. Keratoplasty simply means surgery to the cornea. The graft is taken from a recently dead individual with no known diseases or other factors that may affect the chance of survival of the donated tissue or the health of the recipient.
