---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genre
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484430601
title: genre
categories:
    - Dictionary
---
genre
     n 1: a kind of literary or artistic work
     2: a style of expressing yourself in writing [syn: {writing
        style}, {literary genre}]
     3: an expressive style of music [syn: {music genre}, {musical
        genre}, {musical style}]
     4: a class of art (or artistic endeavor) having a
        characteristic form or technique
