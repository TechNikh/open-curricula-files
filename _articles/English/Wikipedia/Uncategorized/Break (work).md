---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lunch_hour
offline_file: ""
offline_thumbnail: ""
uuid: 5b2c3180-5632-484c-b19c-2365e180731a
updated: 1484308361
title: Break (work)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Two_Men_Taking_a_Break.JPG
tags:
    - Restroom/WC breaks
    - Coffee break
    - Snack breaks
    - Smoking breaks
categories:
    - Uncategorized
---
A break at work is a period of time during a shift in which an employee is allowed to take time off from his/her job. There are different types of breaks, and depending on the length and the employer's policies, the break may or may not be paid.
