---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/benefitted
offline_file: ""
offline_thumbnail: ""
uuid: a07f72a3-c779-48ec-83d3-c01b944085af
updated: 1484310234
title: benefitted
categories:
    - Dictionary
---
benefit
     n 1: financial assistance in time of need
     2: something that aids or promotes well-being; "for the common
        good" [syn: {welfare}]
     3: a performance to raise money for a charitable cause
     v 1: derive a benefit from; "She profited from his vast
          experience" [syn: {profit}, {gain}]
     2: be beneficial for; "This will do you good" [syn: {do good}]
     [also: {benefitting}, {benefitted}]
