---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unusually
offline_file: ""
offline_thumbnail: ""
uuid: c893afdb-3c33-45d1-bbb6-0f6acdb14bc0
updated: 1484310189
title: unusually
categories:
    - Dictionary
---
unusually
     adv : to a remarkably degree or extent; "she was unusually tall"
           [syn: {remarkably}, {outstandingly}, {unco}] [ant: {normally}]
