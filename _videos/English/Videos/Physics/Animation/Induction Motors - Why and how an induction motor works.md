---
version: 1
type: video
provider: YouTube
id: I1j1aoELovk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Induction%20Motors%20-%20Why%20and%20how%20an%20induction%20motor%20works.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 53ddff0e-bd6c-47ce-a6e7-b6627d01d4e6
updated: 1486069656
title: 'Induction Motors - Why and how an induction motor works'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/I1j1aoELovk/default.jpg
    - https://i3.ytimg.com/vi/I1j1aoELovk/1.jpg
    - https://i3.ytimg.com/vi/I1j1aoELovk/2.jpg
    - https://i3.ytimg.com/vi/I1j1aoELovk/3.jpg
---
