---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alveoli
offline_file: ""
offline_thumbnail: ""
uuid: 7fd8c862-869e-49fb-b457-b65ec6c399cc
updated: 1484310321
title: alveoli
categories:
    - Dictionary
---
alveolus
     n 1: a tiny sac for holding air in the lungs; formed by the
          terminal dilation of tiny air passageways [syn: {air sac},
           {air cell}]
     2: a bony socket in the alveolar ridge that holds a tooth [syn:
         {tooth socket}]
     [also: {alveoli} (pl)]
