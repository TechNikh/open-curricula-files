---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evergreen
offline_file: ""
offline_thumbnail: ""
uuid: 5421603f-8c67-4a43-a1fc-127636178e93
updated: 1484310435
title: evergreen
categories:
    - Dictionary
---
evergreen
     adj : (of plants and shrubs) bearing foliage throughout the year
           [ant: {deciduous}]
     n : a plant having foliage that persists and remains green
         throughout the year [syn: {evergreen plant}] [ant: {deciduous
         plant}]
