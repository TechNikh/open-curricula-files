---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytical_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 2980ba0c-5a97-4c9a-a12d-339e91f12b23
updated: 1484309483
title: Analytical hierarchy
tags:
    - The analytical hierarchy of formulas
    - The analytical hierarchy of sets of natural numbers
    - >
        The analytical hierarchy on subsets of Cantor and Baire
        space
    - Extensions
    - Examples
    - Properties
    - Table
categories:
    - Computability theory
---
In mathematical logic and descriptive set theory, the analytical hierarchy is an extension of the arithmetical hierarchy. The analytical hierarchy of formulas includes formulas in the language of second-order arithmetic, which can have quantifiers over both the set of natural numbers, 
  
    
      
        
          N
        
      
    
    {\displaystyle \mathbb {N} }
  
, and over functions from 
  
    
      
        
          N
        
      
    
    {\displaystyle \mathbb {N} }
  
 to 
  
    
      
        
          N
        
      
    
    {\displaystyle \mathbb {N} }
  
. The analytical hierarchy of sets classifies sets by the formulas that can be used to define them; ...
