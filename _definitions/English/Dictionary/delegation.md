---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delegation
offline_file: ""
offline_thumbnail: ""
uuid: 6ddc3a8b-7519-4cef-9059-776bb916eaff
updated: 1484310589
title: delegation
categories:
    - Dictionary
---
delegation
     n 1: a group of representatives or delegates [syn: {deputation},
          {commission}, {delegacy}, {mission}]
     2: authorizing subordinates to make certain decisions [syn: {delegating},
         {relegating}, {relegation}, {deputation}]
