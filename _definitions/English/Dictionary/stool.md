---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stool
offline_file: ""
offline_thumbnail: ""
uuid: 149fdf2c-6c24-41f8-ad30-b9fc8b070eda
updated: 1484310327
title: stool
categories:
    - Dictionary
---
stool
     n 1: a simple seat without a back or arms
     2: solid excretory product evacuated from the bowels [syn: {fecal
        matter}, {faecal matter}, {feces}, {faeces}, {BM}, {ordure},
         {dejection}]
     3: (forestry) the stump of a tree that has been felled or
        headed for the production of saplings
     4: a plumbing fixture for defecation and urination [syn: {toilet},
         {can}, {commode}, {crapper}, {pot}, {potty}, {throne}]
     v 1: lure with a stool, as of wild fowl
     2: react to a decoy, of wildfowl
     3: grow shoots in the form of stools or tillers [syn: {tiller}]
     4: have a bowel movement; "The dog had made in the flower beds"
        [syn: ...
