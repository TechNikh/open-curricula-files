---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plane_curve
offline_file: ""
offline_thumbnail: ""
uuid: 45e0aa0e-dd7f-4e2f-858f-b5c3331479c2
updated: 1484309369
title: Plane curve
tags:
    - Smooth plane curve
    - Algebraic plane curve
    - Examples
categories:
    - Geometry stubs
---
In mathematics, a plane curve is a curve in a plane that may be either a Euclidean plane, an affine plane or a projective plane. The most frequently studied cases are smooth plane curves (including piecewise smooth plane curves), and algebraic plane curves.
