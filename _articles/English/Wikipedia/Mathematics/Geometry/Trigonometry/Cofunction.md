---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cofunction
offline_file: ""
offline_thumbnail: ""
uuid: 66ce79ac-91c9-445c-92c4-c3610a1f0524
updated: 1484309341
title: Cofunction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sine_cosine_one_period.svg.png
categories:
    - Trigonometry
---
In mathematics, a function f is cofunction of a function g if f(A) = g(B) whenever A and B are complementary angles. This definition typically applies to trigonometric functions.[1]
