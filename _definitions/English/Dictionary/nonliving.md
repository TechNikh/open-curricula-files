---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonliving
offline_file: ""
offline_thumbnail: ""
uuid: 7ef34732-dc49-432d-a51c-079bfede1975
updated: 1484310264
title: nonliving
categories:
    - Dictionary
---
nonliving
     adj : not endowed with life; "the inorganic world is inanimate";
           "inanimate objects"; "dead stones" [syn: {inanimate}, {dead}]
           [ant: {animate}]
