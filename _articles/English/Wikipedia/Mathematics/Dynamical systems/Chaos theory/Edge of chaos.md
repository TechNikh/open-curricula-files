---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_of_chaos
offline_file: ""
offline_thumbnail: ""
uuid: a4b9d3dd-baa1-4806-afc1-38433c9bdaa2
updated: 1484308707
title: Edge of chaos
categories:
    - Chaos theory
---
The phrase edge of chaos was coined by mathematician Doyne Farmer to describe the transition phenomenon discovered by computer scientist Christopher Langton. The phrase originally refers to an area in the range of a variable, λ (lambda), which was varied while examining the behavior of a cellular automaton (CA). As λ varied, the behavior of the CA went through a phase transition of behaviors. Langton found a small area conducive to produce CAs capable of universal computation. At around the same time physicist James P. Crutchfield and others used the phrase onset of chaos to describe more or less the same concept.
