---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fermi_point
offline_file: ""
offline_thumbnail: ""
uuid: dd3b2ef6-c10a-4c76-87b9-415ec5378273
updated: 1484308743
title: Fermi point
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Semiconductor_band_structure_%2528lots_of_bands_2%2529.svg.png'
tags:
    - Fermi point (Quantum Field Theory)
    - Fermi point (Nanoscience)
    - Chirality
    - Notes
categories:
    - Critical phenomena
---
For both applications count that the symmetry between particles and anti-particles in weak interactions is violated:
At this point the particle energy E = cp is zero.[2]
In nanotechnology this concept can be applied to electron behavior.[3] An electron when as single particle is a Fermion obeying the Pauli exclusion principle.
