---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/window
offline_file: ""
offline_thumbnail: ""
uuid: 21f7fd36-3dfa-4c09-aa26-3c09edadef2a
updated: 1484310313
title: window
categories:
    - Dictionary
---
window
     n 1: a framework of wood or metal that contains a glass
          windowpane and is built into a wall or roof to admit
          light or air
     2: a transparent opening in a vehicle that allow vision out of
        the sides or back; usually is capable of being opened
     3: a transparent panel (as of an envelope) inserted in an
        otherwise opaque material
     4: an opening that resembles a window in appearance or
        function; "he could see them through a window in the
        trees"
     5: the time period that is considered best for starting or
        finishing something; "the expanded window will give us
        time to catch the thieves"; "they had a window ...
