---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flow_chart
offline_file: ""
offline_thumbnail: ""
uuid: 8e6637cf-245e-45aa-9a55-b7383235df40
updated: 1484308368
title: Flowchart
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LampFlowchart.svg.png
tags:
    - Overview
    - History
    - Types
    - Building blocks
    - Common Shapes
    - Other shapes
    - Data-flow extensions
    - Software
    - Diagramming
    - Related diagrams
    - Related subjects
categories:
    - Uncategorized
---
A flowchart is a type of diagram that represents an algorithm, workflow or process, showing the steps as boxes of various kinds, and their order by connecting them with arrows. This diagrammatic representation illustrates a solution model to a given problem. Flowcharts are used in analyzing, designing, documenting or managing a process or program in various fields.[1]
