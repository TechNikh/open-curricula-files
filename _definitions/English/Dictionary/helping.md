---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helping
offline_file: ""
offline_thumbnail: ""
uuid: c07fadd7-b3f2-4ede-9c30-6a098727c54a
updated: 1484310344
title: helping
categories:
    - Dictionary
---
helping
     n : an individual quantity of food or drink taken as part of a
         meal; "the helpings were all small"; "his portion was
         larger than hers"; "there's enough for two servings each"
         [syn: {portion}, {serving}]
