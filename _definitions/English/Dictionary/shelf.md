---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shelf
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454061
title: shelf
categories:
    - Dictionary
---
shelf
     n 1: a support that consists of a horizontal surface for holding
          objects
     2: a projecting ridge on a mountain or submerged under water
        [syn: {ledge}]
     [also: {shelves} (pl)]
