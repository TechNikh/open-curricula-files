---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cubic_form
offline_file: ""
offline_thumbnail: ""
uuid: 4d3c3ab6-ad30-40a5-938a-0fafa05f8d2a
updated: 1484309138
title: Cubic form
categories:
    - Algebraic geometry
---
In mathematics, a cubic form is a homogeneous polynomial of degree 3, and a cubic hypersurface is the zero set of a cubic form. In the case of a cubic form in three variables, the zero set is a cubic plane curve.
