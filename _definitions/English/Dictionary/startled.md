---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/startled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447161
title: startled
categories:
    - Dictionary
---
startled
     adj : excited by sudden surprise or alarm and making a quick
           involuntary movement; "students startled by the
           teacher's quiet return"; "the sudden fluttering of the
           startled pigeons"; "her startled expression"
