---
version: 1
type: video
provider: YouTube
id: 8sz1IPjBRS8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Slope%20and%20Y-intercept%20Intuition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 389dc21b-1ec3-4f3d-943f-94f5f94ec4c3
updated: 1486069569
title: Slope and Y-intercept Intuition
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/8sz1IPjBRS8/default.jpg
    - https://i3.ytimg.com/vi/8sz1IPjBRS8/1.jpg
    - https://i3.ytimg.com/vi/8sz1IPjBRS8/2.jpg
    - https://i3.ytimg.com/vi/8sz1IPjBRS8/3.jpg
---
Using the "Graph of a line" module to understand how a line's graph changes when its slope or y-intercept is changed.
More free lessons at: http://www.khanacademy.org/video?v=8sz1IPjBRS8
