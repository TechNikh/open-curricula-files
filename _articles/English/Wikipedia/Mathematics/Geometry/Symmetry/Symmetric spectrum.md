---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_spectrum
offline_file: ""
offline_thumbnail: ""
uuid: b886977e-d97a-4a3a-893d-4716809a037d
updated: 1484309319
title: Symmetric spectrum
categories:
    - Symmetry
---
In algebraic topology, a symmetric spectrum X is a spectrum of pointed simplicial sets that comes with an action of the symmetric group 
  
    
      
        
          Σ
          
            n
          
        
      
    
    {\displaystyle \Sigma _{n}}
  
 on 
  
    
      
        
          X
          
            n
          
        
      
    
    {\displaystyle X_{n}}
  
 such that the composition of structure maps
