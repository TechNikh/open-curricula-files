---
version: 1
type: article
id: https://en.wikipedia.org/wiki/SETAR_(model)
offline_file: ""
offline_thumbnail: ""
uuid: 01fb28a3-cbf0-46be-b2d6-c6230a49f9b6
updated: 1484308813
title: SETAR (model)
tags:
    - Definition
    - Autoregressive Models
    - SETAR as an Extension of the Autoregressive Model
    - Basic Structure
categories:
    - Nonlinear systems
---
In statistics, Self-Exciting Threshold AutoRegressive (SETAR) models are typically applied to time series data as an extension of autoregressive models, in order to allow for higher degree of flexibility in model parameters through a regime switching behaviour.
