---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seminiferous_tubules
offline_file: ""
offline_thumbnail: ""
uuid: 62a467b0-7649-4d51-8ede-1830123cbcfc
updated: 1484308368
title: Seminiferous tubule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Seminiferous_tubule_and_sperm_low_mag.jpg
categories:
    - Uncategorized
---
Seminiferous tubules are located within the testes, and are the specific location of meiosis, and the subsequent creation of male gametes, namely spermatozoa.
