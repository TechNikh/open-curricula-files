---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acousto-electric_effect
offline_file: ""
offline_thumbnail: ""
uuid: e407f61e-4c6a-478a-b837-45b54a49b2a0
updated: 1484308883
title: Acousto-electric effect
categories:
    - Waves
---
Acousto-electric effect is a nonlinear phenomenon of generation of electric current in a piezo-electric semiconductor by a propagating acoustic wave. The generated electric current is proportional to the intensity of the acoustic wave and to the value of its electron-induced attenuation. The effect was theoretically predicted in 1953 by Parmenter. Its first experimental observation was reported in 1957 by Weinreich and White.
