---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lorenz_system
offline_file: ""
offline_thumbnail: ""
uuid: 2300087c-8172-4f41-baa7-092390bd585a
updated: 1484308719
title: Lorenz system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/A_Trajectory_Through_Phase_Space_in_a_Lorenz_Attractor.gif
tags:
    - Overview
    - Analysis
    - MATLAB simulation
    - Mathematica simulation
    - Python simulation
    - >
        Derivation of the Lorenz equations as a model of atmospheric
        convection
    - Gallery
    - Notes
categories:
    - Chaotic maps
---
The Lorenz system is a system of ordinary differential equations (the Lorenz equations, note it is not Lorentz) first studied by Edward Lorenz. It is notable for having chaotic solutions for certain parameter values and initial conditions. In particular, the Lorenz attractor is a set of chaotic solutions of the Lorenz system which, when plotted, resemble a butterfly or figure eight.
