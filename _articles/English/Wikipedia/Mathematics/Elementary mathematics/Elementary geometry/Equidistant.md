---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equidistant
offline_file: ""
offline_thumbnail: ""
uuid: df36734a-9cdd-4fa7-b3e6-ae22a0a8836a
updated: 1484308927
title: Equidistant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Perpendicular_bisector.gif
categories:
    - Elementary geometry
---
In two-dimensional Euclidean geometry the locus of points equidistant from two given (different) points is their perpendicular bisector. In three dimensions, the locus of points equidistant from two given points is a plane, and generalising further, in n-dimensional space the locus of points equidistant from two points in n-space is an (n−1)-space.
