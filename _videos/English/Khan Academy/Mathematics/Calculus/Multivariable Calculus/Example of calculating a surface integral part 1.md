---
version: 1
type: video
provider: YouTube
id: 7sQCcGlK2bY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Example%20of%20calculating%20a%20surface%20integral%20part%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5158c661-9e28-4fa0-bdaa-e617ab1e1ecc
updated: 1486069689
title: Example of calculating a surface integral part 1
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/7sQCcGlK2bY/default.jpg
    - https://i3.ytimg.com/vi/7sQCcGlK2bY/1.jpg
    - https://i3.ytimg.com/vi/7sQCcGlK2bY/2.jpg
    - https://i3.ytimg.com/vi/7sQCcGlK2bY/3.jpg
---
