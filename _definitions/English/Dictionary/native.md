---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/native
offline_file: ""
offline_thumbnail: ""
uuid: 9de6c7b0-721e-4967-98f4-833517b1d2c8
updated: 1484310407
title: native
categories:
    - Dictionary
---
native
     adj 1: being such by origin; "the native North American sugar
            maple"; "many native artists studied abroad" [ant: {foreign}]
     2: belonging to one by birth; "my native land"; "one's native
        language" [ant: {adopted}]
     3: being or composed of people inhabiting a region from the
        beginning; "native Americans"; "the aboriginal peoples of
        Australia" [syn: {aboriginal}] [ant: {nonnative}]
     4: as found in nature in the elemental form; "native copper"
     5: normally existing at birth; "mankind's connatural sense of
        the good" [syn: {connatural}, {inborn}, {inbred}]
     n : a person who was born in a particular place; an indigenous
  ...
