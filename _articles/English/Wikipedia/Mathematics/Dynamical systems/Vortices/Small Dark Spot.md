---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Small_Dark_Spot
offline_file: ""
offline_thumbnail: ""
uuid: 4155802c-f0e0-4bf0-9760-528891c9dcb6
updated: 1484308881
title: Small Dark Spot
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cyclones_on_Neptune.jpg
categories:
    - Vortices
---
The Small Dark Spot, sometimes also called Dark Spot 2 or The Wizard's Eye, was a southern cyclonic storm on the planet Neptune.[1][2] It was the second most intensive storm on the planet in 1989, when Voyager 2 flew by the planet. When the Hubble Space Telescope observed Neptune in 1994, the storm had disappeared.[3]
