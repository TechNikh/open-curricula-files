---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: a3e90346-e13d-42d9-a495-9397f20b6d72
updated: 1484308627
title: Vertex (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graf.svg_0.png
tags:
    - Types of vertices
categories:
    - Graph theory
---
In mathematics, and more specifically in graph theory, a vertex (plural vertices) or node is the fundamental unit of which graphs are formed: an undirected graph consists of a set of vertices and a set of edges (unordered pairs of vertices), while a directed graph consists of a set of vertices and a set of arcs (ordered pairs of vertices). In a diagram of a graph, a vertex is usually represented by a circle with a label, and an edge is represented by a line or arrow extending from one vertex to another.
