---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fulfil
offline_file: ""
offline_thumbnail: ""
uuid: c9ee1196-5af7-4a82-a5ac-08d862c15ae4
updated: 1484310441
title: fulfil
categories:
    - Dictionary
---
fulfil
     v 1: put in effect; "carry out a task"; "execute the decision of
          the people"; "He actioned the operation" [syn: {carry
          through}, {accomplish}, {execute}, {carry out}, {action},
           {fulfill}]
     2: fill or meet a want or need [syn: {meet}, {satisfy}, {fill},
         {fulfill}]
     3: fulfil the requirements or expectations of [syn: {satisfy},
        {fulfill}, {live up to}] [ant: {fall short of}]
     [also: {fulfilling}, {fulfilled}]
