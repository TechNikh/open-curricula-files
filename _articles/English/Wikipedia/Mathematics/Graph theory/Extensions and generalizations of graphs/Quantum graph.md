---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_graph
offline_file: ""
offline_thumbnail: ""
uuid: 502a2bae-31f9-4c29-ae8e-7ea3a207c805
updated: 1484309408
title: Quantum graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Quantumgraph.png
tags:
    - Metric graphs
    - Quantum graphs
    - Theorems
    - Applications
categories:
    - Extensions and generalizations of graphs
---
In mathematics and physics, a quantum graph is a linear, network-shaped structure of vertices connected by bonds (or edges) with a differential or pseudo-differential operator acting on functions defined on the bonds. Such systems were first studied by Linus Pauling as models of free electrons in organic molecules in the 1930s. They arise in a variety of mathematical contexts, e.g. as model systems in quantum chaos, in the study of waveguides, in photonic crystals and in Anderson localization, or as limit on shrinking thin wires. Quantum graphs have become prominent models in mesoscopic physics used to obtain a theoretical understanding of nanotechnology. Another, more simple notion of ...
