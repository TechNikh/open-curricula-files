---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pulse
offline_file: ""
offline_thumbnail: ""
uuid: 70f45ac8-8c1e-41dc-98df-bd975c8813ff
updated: 1484310303
title: pulse
categories:
    - Dictionary
---
pulse
     n 1: (electronics) a sharp transient wave in the normal
          electrical state (or a series of such transients); "the
          pulsations seemed to be coming from a star" [syn: {pulsation},
           {pulsing}, {impulse}]
     2: the rhythmic contraction and expansion of the arteries with
        each beat of the heart; "he could feel the beat of her
        heart" [syn: {pulsation}, {heartbeat}, {beat}]
     3: the rate at which the heart beats; usually measured to
        obtain a quick evaluation of a person's health [syn: {pulse
        rate}, {heart rate}]
     4: edible seeds of various pod-bearing plants (peas or beans or
        lentils etc.)
     v 1: expand and ...
