---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brushing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484563621
title: brushing
categories:
    - Dictionary
---
brushing
     n 1: the act of brushing your teeth; "the dentist recommended two
          brushes a day" [syn: {brush}]
     2: the act of brushing your hair; "he gave his hair a quick
        brush" [syn: {brush}]
