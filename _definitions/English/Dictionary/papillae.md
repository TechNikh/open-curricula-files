---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/papillae
offline_file: ""
offline_thumbnail: ""
uuid: d2690ada-0ee1-430b-bb40-d4a55405567c
updated: 1484310351
title: papillae
categories:
    - Dictionary
---
papilla
     n 1: a small nipple-shaped protuberance concerned with taste,
          touch, or smell; "the papillae of the tongue"
     2: a small projection of tissue at the base of a hair or tooth
        or feather
     [also: {papillae} (pl)]
