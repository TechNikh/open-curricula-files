---
version: 1
type: video
provider: YouTube
id: xzmXrC-Yzfc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/RGB%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ae63d23a-7c3f-4ded-bba2-1322de574921
updated: 1486069649
title: RGB HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xzmXrC-Yzfc/default.jpg
    - https://i3.ytimg.com/vi/xzmXrC-Yzfc/1.jpg
    - https://i3.ytimg.com/vi/xzmXrC-Yzfc/2.jpg
    - https://i3.ytimg.com/vi/xzmXrC-Yzfc/3.jpg
---
