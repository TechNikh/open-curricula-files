---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dock
offline_file: ""
offline_thumbnail: ""
uuid: aa77a5dc-f846-4066-adfd-a16c681a2453
updated: 1484310383
title: dock
categories:
    - Dictionary
---
dock
     n 1: an enclosure in a court of law where the defendant sits
          during the trial
     2: any of certain coarse weedy plants with long taproots,
        sometimes used as table greens or in folk medicine [syn: {sorrel},
         {sour grass}]
     3: a platform built out from the shore into the water and
        supported by piles; provides access to ships and boats
        [syn: {pier}, {wharf}, {wharfage}]
     4: a platform where trucks or trains can be loaded or unloaded
        [syn: {loading dock}]
     5: landing in a harbor next to a pier where ships are loaded
        and unloaded or repaired; may have gates to let water in
        or out; "the ship arrived at the ...
