---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/region
offline_file: ""
offline_thumbnail: ""
uuid: 76475dec-c2e4-4d84-ba5a-bb98c16d6ccf
updated: 1484310262
title: region
categories:
    - Dictionary
---
region
     n 1: the extended spatial location of something; "the farming
          regions of France"; "religions in all parts of the
          world"; "regions of outer space" [syn: {part}]
     2: a part of an animal that has a special function or is
        supplied by a given artery or nerve; "in the abdominal
        region" [syn: {area}]
     3: a large indefinite location on the surface of the Earth;
        "penguins inhabit the polar regions"
     4: the approximate amount of something (usually used
        prepositionally as in `in the region of'); "it was going
        to take in the region of two or three months to finish the
        job"; "the price is in the neighborhood of ...
