---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bundle_gerbe
offline_file: ""
offline_thumbnail: ""
uuid: a0caff0b-5c6e-4004-ab91-adb6d8530799
updated: 1484309206
title: Bundle gerbe
tags:
    - Topology
    - History
    - Relationship with twisted K-theory
    - Relationship with field theory
    - Notes
categories:
    - Differential geometry
---
