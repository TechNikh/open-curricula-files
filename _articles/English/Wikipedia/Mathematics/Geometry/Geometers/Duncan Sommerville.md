---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duncan_Sommerville
offline_file: ""
offline_thumbnail: ""
uuid: 7420f0d8-6107-4812-aa15-8170df72e1e2
updated: 1484309100
title: Duncan Sommerville
tags:
    - Early life
    - Work in New Zealand
    - Textbooks
categories:
    - Geometers
---
Duncan MacLaren Young Sommerville FRSE FRAS (1879–1934) was a Scottish mathematician and astronomer. He compiled a bibliography on non-Euclidean geometry and also wrote a leading textbook in that field. He also wrote Introduction to the Geometry of N Dimensions, advancing the study of polytopes. He was a co-founder and the first secretary of the New Zealand Astronomical Society.
