---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 0c1c810c-482b-4872-8b61-934335ab67ec
updated: 1484309261
title: Conformal dimension
tags:
    - Formal definition
    - Properties
    - Examples
categories:
    - Metric geometry
---
In mathematics, the conformal dimension of a metric space X is the infimum of the Hausdorff dimension over the conformal gauge of X, that is, the class of all metric spaces quasisymmetric to X.[1]
