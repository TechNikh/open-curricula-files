---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/airs
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313301
title: airs
categories:
    - Dictionary
---
airs
     n : affected manners intended to impress others; "don't put on
         airs with me" [syn: {pose}]
