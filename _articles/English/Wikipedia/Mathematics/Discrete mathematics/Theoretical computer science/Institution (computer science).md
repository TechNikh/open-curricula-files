---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Institution_(computer_science)
offline_file: ""
offline_thumbnail: ""
uuid: b88dca34-8425-468f-b88e-76461fc13cda
updated: 1484308675
title: Institution (computer science)
tags:
    - Definition
    - Examples of institutions
    - Notes
categories:
    - Theoretical computer science
---
The notion of institution has been created by Joseph Goguen and Rod Burstall in the late 1970s in order to deal with the "population explosion among the logical systems used in computer science". The notion tries to capture the essence of the concept of "logical system".[1]
