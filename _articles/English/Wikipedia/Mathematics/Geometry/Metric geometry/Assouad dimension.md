---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Assouad_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 817d1f76-7dca-4a47-8311-266855374cf2
updated: 1484309262
title: Assouad dimension
categories:
    - Metric geometry
---
In mathematics — specifically, in fractal geometry — the Assouad dimension is a definition of fractal dimension for subsets of a metric space. It was introduced by Patrice Assouad in his 1977 PhD thesis and later published in 1979. As well as being used to study fractals, the Assouad dimension has also been used to study quasiconformal mappings and embeddability problems.
