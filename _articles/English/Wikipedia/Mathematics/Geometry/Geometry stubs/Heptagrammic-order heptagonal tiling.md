---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Heptagrammic-order_heptagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 9c94c946-b939-4bd5-a2fa-b6d744b66159
updated: 1484309362
title: Heptagrammic-order heptagonal tiling
tags:
    - Related tilings
categories:
    - Geometry stubs
---
In geometry, the Heptagrammic-order heptagonal tiling is a regular star-tiling of the hyperbolic plane. It has Schläfli symbol of {7,7/2}. The vertex figure heptagrams are {7/2}, . The heptagonal faces overlap with density 3.
