---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polyglot
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441161
title: polyglot
categories:
    - Dictionary
---
polyglot
     adj : having a command of or composed in many languages; "a
           polyglot traveler"; "a polyglot Bible contains versions
           in different languages"
     n : a person who speaks more than one language [syn: {linguist}]
