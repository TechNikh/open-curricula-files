---
version: 1
type: video
provider: YouTube
id: O2-nK_y5zWk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 58bd9569-f76d-402f-9ab4-59f420af1c7d
updated: 1486069658
title: Smart Board Physics Tricks
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/O2-nK_y5zWk/default.jpg
    - https://i3.ytimg.com/vi/O2-nK_y5zWk/1.jpg
    - https://i3.ytimg.com/vi/O2-nK_y5zWk/2.jpg
    - https://i3.ytimg.com/vi/O2-nK_y5zWk/3.jpg
---
