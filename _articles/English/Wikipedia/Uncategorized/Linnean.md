---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linnaean
offline_file: ""
offline_thumbnail: ""
uuid: 83323c94-effc-4be0-af64-ac919259c20f
updated: 1484308345
title: Linnean
categories:
    - Uncategorized
---
Linnean is closely associated with the alternative spelling Linnaean, and can refer to any of the following, all of which are related to the original system of scientific taxonomy of biological species or its author Carl Linnaeus. The Linnean Society of London suggests [1] that the distinction in spelling is between ‘Linnaean’ referring to Linnaeus himself, his works, his classification scheme and the system of nomenclature, and ‘Linnean’ referring to the various societies and is in accord with the name von Linné, which Linnaeus adopted after being ennobled in 1761.
