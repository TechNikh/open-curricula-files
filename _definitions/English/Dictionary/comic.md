---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484444041
title: comic
categories:
    - Dictionary
---
comic
     adj 1: arousing or provoking laughter; "an amusing film with a
            steady stream of pranks and pratfalls"; "an amusing
            fellow"; "a comic hat"; "a comical look of surprise";
            "funny stories that made everybody laugh"; "a very
            funny writer"; "it would have been laughable if it
            hadn't hurt so much"; "a mirthful experience";
            "risible courtroom antics" [syn: {amusing}, {comical},
             {funny}, {laughable}, {mirthful}, {risible}]
     2: of or relating to or characteristic of comedy; "comic hero"
     n : a professional performer who tells jokes and performs
         comical acts [syn: {comedian}]
