---
title: Fresh water
article_ref: https://en.wikipedia.org/wiki/Fresh_water
tags:
    - Systems
    - Sources
    - Water distribution
    - Numerical definition
    - Aquatic organisms
    - Fresh water as a resource
    - Agriculture
    - Limited resource
    - Fresh water withdrawal
    - Causes of limited fresh water
    - Fresh water in the future
    - Choices in the use of fresh water
    - Accessing fresh water
    - Canada
    - United States
    - Developing countries
categories:
    - Water management
---
