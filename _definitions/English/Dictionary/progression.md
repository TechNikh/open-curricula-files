---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/progression
offline_file: ""
offline_thumbnail: ""
uuid: d2b5a328-5393-4452-b3cb-7945e5a9709d
updated: 1484310290
title: progression
categories:
    - Dictionary
---
progression
     n 1: a series with a definite pattern of advance [syn: {patterned
          advance}]
     2: a movement forward; "he listened for the progress of the
        troops" [syn: {progress}, {advance}]
     3: the act of moving forward toward a goal [syn: {progress}, {procession},
         {advance}, {advancement}, {forward motion}, {onward
        motion}]
