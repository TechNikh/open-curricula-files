---
version: 1
type: video
provider: YouTube
id: xrqlZDJPwhw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 522700b1-86a6-424f-ba16-66eb972eeb02
updated: 1486069630
title: 'Singular and Plural- Definition with examples'
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/xrqlZDJPwhw/default.jpg
    - https://i3.ytimg.com/vi/xrqlZDJPwhw/1.jpg
    - https://i3.ytimg.com/vi/xrqlZDJPwhw/2.jpg
    - https://i3.ytimg.com/vi/xrqlZDJPwhw/3.jpg
---
