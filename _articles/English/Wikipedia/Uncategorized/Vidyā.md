---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vidya
offline_file: ""
offline_thumbnail: ""
uuid: ebf92e2c-74de-4965-a3d0-934a3f2d30e9
updated: 1484308337
title: Vidyā
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Saraswati.jpg
categories:
    - Uncategorized
---
Vidyā or Vidhya means "correct knowledge" or "clarity" in several South Asian languages such as Sanskrit, Pali & Sinhala. Vidyā is also a popular Indian unisex given name. The Indonesian transliteration of the name is Widya.
