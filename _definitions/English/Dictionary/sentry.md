---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sentry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484482501
title: sentry
categories:
    - Dictionary
---
sentry
     n : a person employed to watch for something to happen [syn: {lookout},
          {lookout man}, {sentinel}, {watch}, {spotter}, {scout},
         {picket}]
