---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/art
offline_file: ""
offline_thumbnail: ""
uuid: 45f1cc2f-37ff-4f3b-87c1-1031517e456f
updated: 1484310543
title: art
categories:
    - Dictionary
---
art
     n 1: the products of human creativity; works of art collectively;
          "an art exhibition"; "a fine collection of art" [syn: {fine
          art}]
     2: the creation of beautiful or significant things; "art does
        not need to be innovative to be good"; "I was never any
        good at art"; "he said that architecture is the art of
        wasting space beautifully" [syn: {artistic creation}, {artistic
        production}]
     3: a superior skill that you can learn by study and practice
        and observation; "the art of conversation"; "it's quite an
        art" [syn: {artistry}, {prowess}]
     4: photographs or other visual representations in a printed
        ...
