---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stepwise
offline_file: ""
offline_thumbnail: ""
uuid: 03f918ab-af23-4afb-9423-80a05418bac1
updated: 1484310275
title: stepwise
categories:
    - Dictionary
---
stepwise
     adj : one thing at a time [syn: {bit-by-bit}, {in small stages}, {piecemeal},
            {step-by-step}]
     adv : proceeding in steps; "the voltage was increased stepwise"
           [syn: {step by step}]
