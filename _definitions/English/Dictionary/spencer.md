---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spencer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484511901
title: spencer
categories:
    - Dictionary
---
Spencer
     n : English philosopher and sociologist who applied the theory
         of natural selection to human societies (1820-1903) [syn:
          {Herbert Spencer}]
