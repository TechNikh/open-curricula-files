---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bulky
offline_file: ""
offline_thumbnail: ""
uuid: c42b0c60-1fe2-42f6-8d69-dcb35e3b90a1
updated: 1484310595
title: bulky
categories:
    - Dictionary
---
bulky
     adj : of large size for its weight
     [also: {bulkiest}, {bulkier}]
