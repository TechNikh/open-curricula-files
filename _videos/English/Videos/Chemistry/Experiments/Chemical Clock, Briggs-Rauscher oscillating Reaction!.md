---
version: 1
type: video
provider: YouTube
id: WpBwlSn1XPQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b778eb3c-1980-42ab-ac8e-143f511ece3e
updated: 1486069661
title: Chemical Clock, Briggs-Rauscher oscillating Reaction!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/WpBwlSn1XPQ/default.jpg
    - https://i3.ytimg.com/vi/WpBwlSn1XPQ/1.jpg
    - https://i3.ytimg.com/vi/WpBwlSn1XPQ/2.jpg
    - https://i3.ytimg.com/vi/WpBwlSn1XPQ/3.jpg
---
