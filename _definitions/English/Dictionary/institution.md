---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/institution
offline_file: ""
offline_thumbnail: ""
uuid: 84f93eaa-d257-4eec-8d22-8a8a6550c66f
updated: 1484310403
title: institution
categories:
    - Dictionary
---
institution
     n 1: an organization founded and united for a specific purpose
          [syn: {establishment}]
     2: an establishment consisting of a building or complex of
        buildings where an organization for the promotion of some
        cause is situated
     3: a custom that for a long time has been an important feature
        of some group or society; "the institution of marriage";
        "the institution of slavery"; "he had become an
        institution in the theater"
     4: the act of starting something for the first time;
        introducing something new; "she looked forward to her
        initiation as an adult"; "the foundation of a new
        scientific ...
