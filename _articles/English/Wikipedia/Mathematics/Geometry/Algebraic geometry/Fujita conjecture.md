---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fujita_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 51a89fad-2541-4abc-a6bc-1084be948e88
updated: 1484309147
title: Fujita conjecture
categories:
    - Algebraic geometry
---
In mathematics, Fujita's conjecture is a problem in the theories of algebraic geometry and complex manifolds, unsolved as of 2013[update]. It is named after Takao Fujita, who formulated it in 1985.
