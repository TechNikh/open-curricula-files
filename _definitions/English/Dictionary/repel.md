---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repel
offline_file: ""
offline_thumbnail: ""
uuid: 503ed642-86e1-4e3e-9d81-2624b8b666bb
updated: 1484310403
title: repel
categories:
    - Dictionary
---
repel
     v 1: cause to move back by force or influence; "repel the enemy";
          "push back the urge to smoke"; "beat back the invaders"
          [syn: {drive}, {repulse}, {force back}, {push back}, {beat
          back}] [ant: {attract}]
     2: be repellent to; cause aversion in [syn: {repulse}] [ant: {attract}]
     3: force or drive back; "repel the attacker"; "fight off the
        onslaught"; "rebuff the attack" [syn: {repulse}, {fight
        off}, {rebuff}, {drive back}]
     4: reject outright and bluntly; "She snubbed his proposal"
        [syn: {rebuff}, {snub}]
     5: fill with distaste; "This spoilt food disgusts me" [syn: {disgust},
         {gross out}, {revolt}]
     ...
