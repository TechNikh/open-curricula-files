---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jules_Ho%C3%BCel'
offline_file: ""
offline_thumbnail: ""
uuid: 1edde1a4-90e0-4d7d-89ef-fce0980ac937
updated: 1484309085
title: Jules Hoüel
categories:
    - Geometers
---
Guillaume-Jules Hoüel (born 7 April 1823 in Thaon; died 14 June 1886 in Périers) was a French mathematician. He entered the École Normale Supérieure in 1843 and received his doctoral degree in 1855 from the Sorbonne. He was sought by Urbain Le Verrier at the Paris Observatory, but chose instead to return to Thaon to study there. In 1859 he began to teach at Bordeaux.
