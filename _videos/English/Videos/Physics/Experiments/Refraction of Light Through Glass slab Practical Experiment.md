---
version: 1
type: video
provider: YouTube
id: qrUCxvpaRc8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 88b10cc8-b085-418a-a187-897d04ac3beb
updated: 1486069635
title: Refraction of Light Through Glass slab Practical Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/qrUCxvpaRc8/default.jpg
    - https://i3.ytimg.com/vi/qrUCxvpaRc8/1.jpg
    - https://i3.ytimg.com/vi/qrUCxvpaRc8/2.jpg
    - https://i3.ytimg.com/vi/qrUCxvpaRc8/3.jpg
---
