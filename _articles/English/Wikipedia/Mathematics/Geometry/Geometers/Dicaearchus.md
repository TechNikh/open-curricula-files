---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dicaearchus
offline_file: ""
offline_thumbnail: ""
uuid: 5731238f-4006-4434-8eea-989f5a9c6ccf
updated: 1484309078
title: Dicaearchus
tags:
    - life
    - Writings
    - Sources
categories:
    - Geometers
---
Dicaearchus of Messana (/ˌdɪsiˈɑːrkəs əv məˈsænə/; Greek: Δικαίαρχος Dikaiarkhos; c. 350 – c. 285 BC), also written Dicearchus or Dicearch (/ˈdɪsiˌɑːrk/), was a Greek philosopher, cartographer, geographer, mathematician and author. Dicaearchus was Aristotle's student in the Lyceum. Very little of his work remains extant. He wrote on the history and geography of Greece, of which his most important work was his Life of Greece. He made important contributions to the field of cartography, where he was among the first to use geographical coordinates. He also wrote books on philosophy and politics.
