---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uric_acid
offline_file: ""
offline_thumbnail: ""
uuid: 8852dec5-7144-482e-a6f5-5eacc91810de
updated: 1484308358
title: Uric acid
tags:
    - chemistry
    - Solubility
    - Biology
    - Genetics
    - Medicine
    - High uric acid
    - Causes of high uric acid
    - Gout
    - Lesch-Nyhan syndrome
    - Cardiovascular disease
    - Type 2 diabetes
    - Metabolic syndrome
    - Uric acid stone formation
    - Low uric acid
    - Causes of low uric acid
    - Multiple sclerosis
    - Normalizing low uric acid
    - Oxidative stress
    - Sources
    - Creative output
categories:
    - Uncategorized
---
Uric acid is a heterocyclic compound of carbon, nitrogen, oxygen, and hydrogen with the formula C5H4N4O3. It forms ions and salts known as urates and acid urates, such as ammonium acid urate. Uric acid is a product of the metabolic breakdown of purine nucleotides, and it is a normal component of urine. High blood concentrations of uric acid can lead to gout and are associated with other medical conditions including diabetes and the formation of ammonium acid urate kidney stones.
