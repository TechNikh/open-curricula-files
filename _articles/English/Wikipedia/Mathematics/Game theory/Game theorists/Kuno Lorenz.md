---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kuno_Lorenz
offline_file: ""
offline_thumbnail: ""
uuid: 224903b2-15e6-4309-8c7e-3da30a7680e2
updated: 1484308513
title: Kuno Lorenz
tags:
    - Career
    - Dialogue and predication
    - Dialogical constructivism
    - Publications
    - Bibliography
categories:
    - Game theorists
---
Kuno Lorenz (born September 17, 1932 in Vachdorf, Thüringen) is a German philosopher. He developed a philosophy of dialogue, in connection with the pragmatic theory of action of the Erlangen constructivist school. Lorenz is married to the literary scholar Karin Lorenz-Lindemann.
