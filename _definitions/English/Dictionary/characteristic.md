---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/characteristic
offline_file: ""
offline_thumbnail: ""
uuid: e524fdde-c856-4d2a-9315-2ac0e9d20e67
updated: 1484310303
title: characteristic
categories:
    - Dictionary
---
characteristic
     adj : typical or distinctive; "heard my friend's characteristic
           laugh"; "red and gold are the characteristic colors of
           autumn"; "stripes characteristic of the zebra" [ant: {uncharacteristic}]
     n 1: a prominent aspect of something; "the map showed roads and
          other features"; "generosity is one of his best
          characteristics" [syn: {feature}]
     2: a distinguishing quality
     3: the integer part (positive or negative) of the
        representation of a logarithm; in the expression log 643 =
        2.808 the characteristic is 2
     4: any measurable property of a device measured under closely
        specified conditions [syn: ...
