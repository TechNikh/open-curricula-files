---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sole
offline_file: ""
offline_thumbnail: ""
uuid: 39ee988f-a0b6-4340-b09e-22a135038a39
updated: 1484310220
title: sole
categories:
    - Dictionary
---
sole
     adj 1: not divided or shared with others; "they have exclusive use
            of the machine"; "sole rights of publication" [syn: {exclusive},
             {sole(a)}]
     2: being the only one; single and isolated from others; "the
        lone doctor in the entire county"; "a lonesome pine"; "an
        only child"; "the sole heir"; "the sole example"; "a
        solitary instance of cowardice"; "a solitary speck in the
        sky" [syn: {lone(a)}, {lonesome(a)}, {only(a)}, {sole(a)},
         {solitary(a)}]
     n 1: the underside of footwear or a golfclub
     2: lean flesh of any of several flatfish [syn: {fillet of sole}]
     3: the underside of the foot
     4: ...
