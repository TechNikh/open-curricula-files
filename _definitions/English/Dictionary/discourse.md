---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discourse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484629381
title: discourse
categories:
    - Dictionary
---
discourse
     n 1: extended verbal expression in speech or writing
     2: an address of a religious nature (usually delivered during a
        church service) [syn: {sermon}, {preaching}]
     3: an extended communication (often interactive) dealing with
        some particular topic; "the book contains an excellent
        discussion of modal logic"; "his treatment of the race
        question is badly biased" [syn: {discussion}, {treatment}]
     v 1: to consider or examine in speech or writing; "The article
          covered all the different aspects of this question";
          "The class discussed Dante's `Inferno'" [syn: {talk
          about}, {discuss}]
     2: carry on a ...
