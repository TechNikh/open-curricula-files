---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/father
offline_file: ""
offline_thumbnail: ""
uuid: c2cc83fd-dc06-4d87-add8-1e8bb96078f6
updated: 1484310309
title: father
categories:
    - Dictionary
---
father
     n 1: a male parent (also used as a term of address to your
          father); "his father was born in Atlanta" [syn: {male
          parent}, {begetter}] [ant: {mother}, {mother}]
     2: the founder of a family; "keep the faith of our forefathers"
        [syn: {forefather}, {sire}]
     3: `Father' is a term of address for priests in some churches
        (especially the Roman Catholic Church or the Orthodox
        Catholic Church); `Padre' is frequently used in the
        military [syn: {Padre}]
     4: (Christianity) any of about 70 theologians in the period
        from the 2nd to the 7th century whose writing established
        and confirmed official church doctrine; in ...
