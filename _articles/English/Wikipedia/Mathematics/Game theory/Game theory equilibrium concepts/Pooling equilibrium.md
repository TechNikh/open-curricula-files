---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pooling_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 7cdc5485-066c-4a21-8875-626f2ae84a46
updated: 1484309032
title: Pooling equilibrium
categories:
    - Game theory equilibrium concepts
---
A pooling equilibrium in game theory is an equilibria result of a signaling game. In a signaling game, players send actions called "signals" to other players in the game. Signaling actions are chosen based on privately held information (not known by other players in the game). These actions do not reveal a player's "type" to other players in the game, and other players will choose strategies accordingly. Under this equilibria, all types of a given sender will send the same signal, some representing their true type, some correctly mimicking the type of others, as they have no incentive to differentiate themselves. The receiver therefore acts like having received no information/message ...
