---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rent
offline_file: ""
offline_thumbnail: ""
uuid: 8a7e2bb5-395a-4633-b7eb-3425ad3d046c
updated: 1484310471
title: rent
categories:
    - Dictionary
---
rent
     n 1: a regular payment by a tenant to a landlord for use of some
          property
     2: an opening made forcibly as by pulling apart; "there was a
        rip in his pants"; "she had snags in her stockings" [syn:
        {rip}, {snag}, {split}, {tear}]
     3: the return derived from cultivated land in excess of that
        derived from the poorest land cultivated under similar
        conditions [syn: {economic rent}]
     4: the act of rending or ripping or splitting something; "he
        gave the envelope a vigorous rip" [syn: {rip}, {split}]
     v 1: let for money; "We rented our apartment to friends while we
          were abroad" [syn: {lease}]
     2: grant use or ...
