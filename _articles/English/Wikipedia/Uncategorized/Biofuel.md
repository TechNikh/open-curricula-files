---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biofuels
offline_file: ""
offline_thumbnail: ""
uuid: 34862c3a-5581-4f88-802b-d7cb7d6758d2
updated: 1484308333
title: Biofuel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Soybeanbus.jpg
tags:
    - Liquid fuels for transportation
    - First-generation biofuels
    - Ethanol
    - biodiesel
    - Other bioalcohols
    - Green diesel
    - Biofuel gasoline
    - vegetable oil
    - Bioethers
    - Biogas
    - Syngas
    - Solid biofuels
    - Second-generation (advanced) biofuels
    - Sustainable biofuels
    - Biofuels by region
    - Air pollution
    - Debates regarding the production and use of biofuel
    - Current research
    - Ethanol biofuels
    - Algae biofuels
    - jatropha
    - Fungi
    - Animal gut bacteria
    - Greenhouse gas emissions
    - Water use
categories:
    - Uncategorized
---
A biofuel is a fuel that is produced through contemporary biological processes, such as agriculture and anaerobic digestion, rather than a fuel produced by geological processes such as those involved in the formation of fossil fuels, such as coal and petroleum, from prehistoric biological matter. Biofuels can be derived directly from plants, or indirectly from agricultural, commercial, domestic, and/or industrial wastes.[1] Renewable biofuels generally involve contemporary carbon fixation, such as those that occur in plants or microalgae through the process of photosynthesis. Other renewable biofuels are made through the use or conversion of biomass (referring to recently living organisms, ...
