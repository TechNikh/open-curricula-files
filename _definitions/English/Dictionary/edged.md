---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/edged
offline_file: ""
offline_thumbnail: ""
uuid: 7ce3b8a3-f1d8-480d-935e-5fd99f4bdc2c
updated: 1484310609
title: edged
categories:
    - Dictionary
---
edged
     adj 1: having a specified kind of border or edge; "a black-edged
            card"; "dried sweat left salt-edged patches"
     2: (of speech) harsh or hurtful in tone or character; "cutting
        remarks"; "edged satire"; "a stinging comment" [syn: {cutting},
         {stinging}]
     3: having a cutting edge or especially an edge or edges as
        specified; often used in combination; "an edged knife"; "a
        two-edged sword"
