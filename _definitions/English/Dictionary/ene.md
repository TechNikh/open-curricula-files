---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ene
offline_file: ""
offline_thumbnail: ""
uuid: 0ff83fb4-a9d0-4b8e-8198-ab0055a507eb
updated: 1484310423
title: ene
categories:
    - Dictionary
---
ENE
     n : the compass point midway between northeast and east [syn: {east
         northeast}]
