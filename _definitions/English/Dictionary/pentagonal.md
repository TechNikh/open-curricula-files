---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pentagonal
offline_file: ""
offline_thumbnail: ""
uuid: 7193ee83-b820-43f5-8f6f-6e301b6cfeac
updated: 1484310414
title: pentagonal
categories:
    - Dictionary
---
pentagonal
     adj : of or relating to or shaped like a pentagon [syn: {pentangular}]
