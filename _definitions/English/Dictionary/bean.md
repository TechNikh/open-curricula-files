---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bean
offline_file: ""
offline_thumbnail: ""
uuid: 32fceb21-3616-4529-8239-51e85134b4d8
updated: 1484310307
title: bean
categories:
    - Dictionary
---
bean
     n 1: any of various edible seeds of plants of the family
          Leguminosae [syn: {edible bean}]
     2: any of various seeds or fruits suggestive of beans
     3: any of various leguminous plants grown for their edible
        seeds and pods [syn: {bean plant}]
     4: informal terms for a human head [syn: {attic}, {bonce}, {noodle},
         {noggin}, {dome}]
     v : hit on the head, esp. with a pitched baseball
