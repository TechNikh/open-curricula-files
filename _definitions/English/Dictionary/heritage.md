---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heritage
offline_file: ""
offline_thumbnail: ""
uuid: 6d007e6d-4ebd-456c-a5e8-e9ab12e80647
updated: 1484310181
title: heritage
categories:
    - Dictionary
---
heritage
     n 1: practices that are handed down from the past by tradition;
          "a heritage of freedom"
     2: any attribute or immaterial possession that is inherited
        from ancestors; "my only inheritance was my mother's
        blessing"; "the world's heritage of knowledge" [syn: {inheritance}]
     3: that which is inherited; a title or property or estate that
        passes by law to the heir on the death of the owner [syn:
        {inheritance}]
     4: hereditary succession to a title or an office or property
        [syn: {inheritance}]
