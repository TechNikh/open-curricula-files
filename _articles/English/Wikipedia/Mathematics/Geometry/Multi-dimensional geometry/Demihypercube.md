---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Demihypercube
offline_file: ""
offline_thumbnail: ""
uuid: c4892f64-50bb-4834-b93f-8c1e8bd61445
updated: 1484309285
title: Demihypercube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-CubeAndStel.svg.png
tags:
    - discovery
    - Constructions
    - Symmetry group
    - Orthotopic constructions
categories:
    - Multi-dimensional geometry
---
In geometry, demihypercubes (also called n-demicubes, n-hemicubes, and half measure polytopes) are a class of n-polytopes constructed from alternation of an n-hypercube, labeled as hγn for being half of the hypercube family, γn. Half of the vertices are deleted and new facets are formed. The 2n facets become 2n (n-1)-demicubes, and 2n (n-1)-simplex facets are formed in place of the deleted vertices.[1]
