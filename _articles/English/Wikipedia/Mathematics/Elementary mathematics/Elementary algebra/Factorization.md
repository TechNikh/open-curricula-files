---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Factorization
offline_file: ""
offline_thumbnail: ""
uuid: 6c376d6a-72c9-4ecf-9078-1f33109517e8
updated: 1484308905
title: Factorization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Factorisatie.svg.png
tags:
    - Integers
    - Polynomials
    - History of factoring polynomials
    - General methods
    - Highest common factor
    - Factoring by grouping
    - Using the factor theorem
    - Univariate case, using the properties of roots
    - Finding rational roots
    - Recognizable patterns
    - Difference of two squares
    - Sum/difference of two cubes
    - Difference of two fourth powers
    - Sum/difference of two nth powers
    - >
        Sum/difference of two nth powers over the field of the
        algebraic numbers
    - Binomial expansions
    - Other factorization formulas
    - Using formulas for polynomial roots
    - Factoring over the complex numbers
    - Sum of two squares
    - Matrices
    - Unique factorization domains
    - Euclidean domains
    - Notes
categories:
    - Elementary algebra
---
In mathematics, factorization (also factorisation in some forms of British English) or factoring is the decomposition of an object (for example, a number, a polynomial, or a matrix) into a product of other objects, or factors, which when multiplied together give the original. For example, the number 15 factors into primes as 3 × 5, and the polynomial x2 − 4 factors as (x − 2)(x + 2). In all cases, a product of simpler objects is obtained.
