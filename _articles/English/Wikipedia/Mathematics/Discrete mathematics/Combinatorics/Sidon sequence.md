---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sidon_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 8f2e5242-af89-4d2d-92f4-21b5499174f7
updated: 1484308590
title: Sidon sequence
tags:
    - Early results
    - Infinite Sidon sequences
    - Relationship to Golomb rulers
categories:
    - Combinatorics
---
In number theory, a Sidon sequence (or Sidon set), named after the Hungarian mathematician Simon Sidon, is a sequence A = {a0, a1, a2, ...} of natural numbers in which all pairwise sums ai + aj (i ≤ j) are different. Sidon introduced the concept in his investigations of Fourier series.
