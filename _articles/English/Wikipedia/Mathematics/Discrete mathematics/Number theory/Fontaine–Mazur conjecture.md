---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fontaine%E2%80%93Mazur_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 9705c7c0-3405-4422-8239-fbc5f2307897
updated: 1484308639
title: Fontaine–Mazur conjecture
categories:
    - Number theory
---
In mathematics, the Fontaine–Mazur conjectures are some conjectures introduced by Fontaine and Mazur (1995) about when p-adic representations of Galois groups of number fields can be constructed from representations on étale cohomology groups of a varieties.
