---
version: 1
type: video
provider: YouTube
id: tjK4N0CxtFU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fd6c292c-01b8-42a4-9614-861fbeee2398
updated: 1486069609
title: Independent Events 3
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/tjK4N0CxtFU/default.jpg
    - https://i3.ytimg.com/vi/tjK4N0CxtFU/1.jpg
    - https://i3.ytimg.com/vi/tjK4N0CxtFU/2.jpg
    - https://i3.ytimg.com/vi/tjK4N0CxtFU/3.jpg
---
