---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rudimentary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484419441
title: rudimentary
categories:
    - Dictionary
---
rudimentary
     adj 1: being or involving basic facts or principles; "the
            fundamental laws of the universe"; "a fundamental
            incompatibility between them"; "these rudimentary
            truths"; "underlying principles" [syn: {fundamental},
            {underlying}]
     2: being in the earliest stages of development; "rudimentary
        plans"
     3: not fully developed in mature animals; "rudimentary wings"
        [syn: {vestigial}]
