---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/porter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485801
title: porter
categories:
    - Dictionary
---
porter
     n 1: a person employed to carry luggage and supplies
     2: someone who guards an entrance [syn: {doorkeeper}, {doorman},
         {door guard}, {hall porter}, {gatekeeper}, {ostiary}]
     3: United States writer of novels and short stories (1890-1980)
        [syn: {Katherine Anne Porter}]
     4: United States composer and lyricist of musical comedies
        (1891-1946) [syn: {Cole Porter}, {Cole Albert Porter}]
     5: United States writer of short stories whose pen name was O.
        Henry (1862-1910) [syn: {William Sydney Porter}, {O. Henry}]
     6: a railroad employee who assists passengers (especially on
        sleeping cars) [syn: {Pullman porter}]
     7: a very ...
