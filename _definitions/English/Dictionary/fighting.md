---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fighting
offline_file: ""
offline_thumbnail: ""
uuid: cfc341c4-4506-458c-b12b-7b870bc731a0
updated: 1484310543
title: fighting
categories:
    - Dictionary
---
fighting
     adj 1: engaged in or ready for military or naval operations; "on
            active duty"; "the platoon is combat-ready"; "review
            the fighting forces" [syn: {active}, {combat-ready}, {fighting(a)}]
     2: engaged in war; "belligerent (or warring) nations"; "a
        fighting war" [syn: {belligerent}, {militant}, {war-ridden},
         {warring}]
     3: disposed to loud disagreements and fighting [syn: {brawling}]
     n : the act of fighting; any contest or struggle; "a fight broke
         out at the hockey game"; "there was fighting in the
         streets"; "the unhappy couple got into a terrible scrap"
         [syn: {fight}, {combat}, {scrap}]
