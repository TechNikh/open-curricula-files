---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excelled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442181
title: excelled
categories:
    - Dictionary
---
excel
     v : distinguish oneself; "She excelled in math" [syn: {stand out},
          {surpass}]
     [also: {excelling}, {excelled}]
