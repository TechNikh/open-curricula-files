---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_theology
offline_file: ""
offline_thumbnail: ""
uuid: 330e54df-448f-4cfd-99b3-f5b758e25044
updated: 1484309483
title: Computational theology
categories:
    - Computability theory
---
Computational Theology or Computational Religion or Computational Creationism is a study that attempts to link religion and god to computer science or similar academic field. Computational Theology tries to understand and explain religion and god by understanding computer science and other related fields.[1][2][3]
