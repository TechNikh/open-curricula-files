---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydride
offline_file: ""
offline_thumbnail: ""
uuid: 022c5b6e-b980-4dce-9888-d2f577800293
updated: 1484310423
title: hydride
categories:
    - Dictionary
---
hydride
     n : any binary compound formed by the union of hydrogen and
         other elements
