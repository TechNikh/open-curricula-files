---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anti-clockwise
offline_file: ""
offline_thumbnail: ""
uuid: 65d0ef1f-4e62-42e0-88fc-834b5fa54de7
updated: 1484310395
title: anti-clockwise
categories:
    - Dictionary
---
anticlockwise
     adj : in the direction opposite to the rotation of the hands of a
           clock [syn: {counterclockwise}, {contraclockwise}]
           [ant: {clockwise}]
     adv : in a direction opposite to the direction in which the hands
           of a clock move; "please move counterclockwise in a
           circle!" [syn: {counterclockwise}] [ant: {clockwise}]
