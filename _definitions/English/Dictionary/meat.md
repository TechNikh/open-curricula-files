---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meat
offline_file: ""
offline_thumbnail: ""
uuid: 265cc1d8-83cf-439d-b888-6dbbf12cca7d
updated: 1484310477
title: meat
categories:
    - Dictionary
---
meat
     n 1: the flesh of animals (including fishes and birds and snails)
          used as food
     2: the inner and usually edible part of a seed or grain or nut
        or fruit stone; "black walnut kernels are difficult to get
        out of the shell" [syn: {kernel}]
     3: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {substance}, {core},
         {center}, {essence}, {gist}, {heart}, {heart and soul}, {inwardness},
         {marrow}, {nub}, {pith}, {sum}, {nitty-gritty}]
