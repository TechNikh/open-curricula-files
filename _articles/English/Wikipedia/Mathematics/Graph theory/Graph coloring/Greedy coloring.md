---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Greedy_coloring
offline_file: ""
offline_thumbnail: ""
uuid: e14bfd85-d815-447e-b35d-329eea15000b
updated: 1484309384
title: Greedy coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Greedy_colourings.svg.png
tags:
    - Greed is not always good
    - Optimal ordering
    - Heuristic ordering strategies
    - Alternative color selection schemes
    - Notes
categories:
    - Graph coloring
---
In the study of graph coloring problems in mathematics and computer science, a greedy coloring is a coloring of the vertices of a graph formed by a greedy algorithm that considers the vertices of the graph in sequence and assigns each vertex its first available color. Greedy colorings do not in general use the minimum number of colors possible. However, they have been used in mathematics as a technique for proving other results about colorings and in computer science as a heuristic to find colorings with few colors.
