---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/La_G%C3%A9om%C3%A9trie'
offline_file: ""
offline_thumbnail: ""
uuid: edb66785-7a72-44c8-a06d-8e9dc39ef779
updated: 1484309245
title: La Géométrie
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-GeometryDescartes.JPG
categories:
    - History of geometry
---
La Géométrie was published in 1637 as an appendix to Discours de la méthode (Discourse on Method), written by René Descartes. In the Discourse, he presents his method for obtaining clarity on any subject. La Géométrie and two other appendices also by Descartes, the Optics and the Meteorology, were published with the Discourse to give examples of the kinds of successes he had achieved following his method[1] (as well as, perhaps, considering the contemporary European social climate of intellectual competitiveness, to show off a bit to a wider audience).
