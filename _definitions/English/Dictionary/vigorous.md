---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vigorous
offline_file: ""
offline_thumbnail: ""
uuid: 28e2c76f-3bdf-4ec1-a823-d22adb2d23bd
updated: 1484310333
title: vigorous
categories:
    - Dictionary
---
vigorous
     adj 1: characterized by forceful and energetic action or activity;
            "a vigorous hiker"; "gave her skirt a vigorous shake";
            "a vigorous campaign"; "a vigorous foreign policy";
            "vigorous opposition to the war"
     2: strong and active physically or mentally; "a vigorous old
        man who spent half of his day on horseback"- W.H.Hudson
