---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Long_code_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 1d39d183-a152-48ac-82c1-57f0ca9998cf
updated: 1484308563
title: Long code (mathematics)
categories:
    - Coding theory
---
In theoretical computer science and coding theory, the long code is an error-correcting code that is locally decodable. Long codes have an extremely poor rate, but play a fundamental role in the theory of hardness of approximation.
