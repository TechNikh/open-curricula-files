---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rank_of_a_partition
offline_file: ""
offline_thumbnail: ""
uuid: 736503ab-0991-4cb5-bbe6-98fd5f0fdde5
updated: 1484308588
title: Rank of a partition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rank_of_a_partition.svg.png
tags:
    - Definition
    - Notations
    - Some basic results
    - "Ramanujan's congruences and Dyson's conjecture"
    - Generating functions
    - Alternate definition
categories:
    - Combinatorics
---
In mathematics, particularly in the fields of number theory and combinatorics, the rank of a partition of a positive integer is a certain integer associated with the partition. In fact at least two different definitions of rank appear in the literature. The first definition, with which most of this article is concerned, is that the rank of a partition is the number obtained by subtracting the number of parts in the partition from the largest part in the partition. The concept was introduced by Freeman Dyson in a paper published in the journal Eureka.[1] It was presented in the context of a study of certain congruence properties of the partition function discovered by the Indian mathematical ...
