---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequential_game
offline_file: ""
offline_thumbnail: ""
uuid: dcd0b713-30c3-48bc-98aa-1a4dac5f7b15
updated: 1484309038
title: Sequential game
categories:
    - Game theory game classes
---
In game theory, a sequential game is a game where one player chooses their action before the others choose theirs. Importantly, the later players must have some information of the first's choice, otherwise the difference in time would have no strategic effect. Sequential games hence are governed by the time axis, and represented in the form of decision trees.
