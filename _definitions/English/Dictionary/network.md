---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/network
offline_file: ""
offline_thumbnail: ""
uuid: 2c254237-2498-4610-ab39-c64192bf3610
updated: 1484310325
title: network
categories:
    - Dictionary
---
network
     n 1: an interconnected system of things or people; "he owned a
          network of shops"; "retirement meant dropping out of a
          whole network of people who had been part of my life";
          "tangled in a web of cloth" [syn: {web}]
     2: (broadcasting) a communication system consisting of a group
        of broadcasting stations that all transmit the same
        programs; "the networks compete to broadcast important
        sports events"
     3: an open fabric of string or rope or wire woven together at
        regular intervals [syn: {net}, {mesh}, {meshing}, {meshwork}]
     4: a system of intersecting lines or channels; "a railroad
        network"; "a ...
