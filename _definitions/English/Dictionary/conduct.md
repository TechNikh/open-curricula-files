---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conduct
offline_file: ""
offline_thumbnail: ""
uuid: 978b0c5f-374b-40ba-b515-2e504ea05042
updated: 1484310240
title: conduct
categories:
    - Dictionary
---
conduct
     n 1: manner of acting or conducting yourself [syn: {behavior}, {behaviour},
           {doings}]
     2: (behavioral attributes) the way a person behaves toward
        other people [syn: {demeanor}, {demeanour}, {behavior}, {behaviour},
         {deportment}]
     v 1: direct the course of; manage or control; "You cannot conduct
          business like this" [syn: {carry on}, {deal}]
     2: lead, as in the performance of a composition; "conduct an
        orchestra; Bairenboim conducted the Chicago symphony for
        years" [syn: {lead}, {direct}]
     3: behave in a certain manner; "She carried herself well"; "he
        bore himself with dignity"; "They conducted ...
