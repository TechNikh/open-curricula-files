---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fashioned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461081
title: fashioned
categories:
    - Dictionary
---
fashioned
     adj : planned and made or fashioned artistically; "beautifully
           fashioned dresses"
