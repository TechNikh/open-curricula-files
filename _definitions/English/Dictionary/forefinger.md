---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forefinger
offline_file: ""
offline_thumbnail: ""
uuid: dad40018-6e1f-4658-8874-052138debb02
updated: 1484310601
title: forefinger
categories:
    - Dictionary
---
forefinger
     n : the finger next to the thumb [syn: {index}, {index finger}]
