---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pursuit-evasion
offline_file: ""
offline_thumbnail: ""
uuid: dee84c9f-5493-40c3-a7fc-40be5ffeae72
updated: 1484309059
title: Pursuit-evasion
tags:
    - Discrete formulation
    - Problem definition
    - Variants
    - Complexity
    - Multi-Player Pursuit-Evasion Games
    - Continuous formulation
    - Applications
    - Notes
categories:
    - Pursuit-evasion
---
Pursuit-evasion (variants of which are referred to as cops and robbers and graph searching) is a family of problems in mathematics and computer science in which one group attempts to track down members of another group in an environment. Early work on problems of this type modeled the environment geometrically.[1] In 1976, Torrence Parsons introduced a formulation whereby movement is constrained by a graph.[2] The geometric formulation is sometimes called continuous pursuit-evasion, and the graph formulation discrete pursuit-evasion (also called graph searching). Current research is typically limited to one of these two formulations.
