---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bunsen_burner
offline_file: ""
offline_thumbnail: ""
uuid: 667460d6-8016-4a38-b783-94d814349f9e
updated: 1484308372
title: Bunsen burner
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bunsen_burner.jpg
tags:
    - History
    - Operation
    - Variants
categories:
    - Uncategorized
---
A Bunsen burner, named after Robert Bunsen, is a common piece of laboratory equipment that produces a single open gas flame, which is used for heating, sterilization, and combustion.[1][2][3][4][5]
