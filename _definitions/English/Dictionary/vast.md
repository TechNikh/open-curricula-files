---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vast
offline_file: ""
offline_thumbnail: ""
uuid: 578a1b28-bd38-4fbd-a14f-c984cb27f068
updated: 1484310323
title: vast
categories:
    - Dictionary
---
vast
     adj : unusually great in size or amount or degree or especially
           extent or scope; "huge government spending"; "huge
           country estates"; "huge popular demand for higher
           education"; "a huge wave"; "the Los Angeles aqueduct
           winds like an immense snake along the base of the
           mountains"; "immense numbers of birds"; "at vast (or
           immense) expense"; "the vast reaches of outer space";
           "the vast accumulation of knowledge...which we call
           civilization"- W.R.Inge [syn: {huge}, {immense}, {Brobdingnagian}]
