---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noticed
offline_file: ""
offline_thumbnail: ""
uuid: ad4fe2e0-e9f3-4d7a-8c49-5a50988e6255
updated: 1484310286
title: noticed
categories:
    - Dictionary
---
noticed
     adj : being perceived or observed; "an easily noticed effect on
           the rate of growth" [ant: {unnoticed}]
