---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/induced
offline_file: ""
offline_thumbnail: ""
uuid: b1710b71-eecb-4672-82f7-54f4f6cc290e
updated: 1484310369
title: induced
categories:
    - Dictionary
---
induced
     adj : brought about or caused; not spontaneous; "a case of
           steroid-induced weakness" [ant: {spontaneous}]
