---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bancroft_point
offline_file: ""
offline_thumbnail: ""
uuid: cfb4eaa2-a48f-4eaf-898f-88480134e6b6
updated: 1484308834
title: Bancroft point
categories:
    - Thermodynamics
---
A Bancroft point is the temperature where an azeotrope occurs in a binary system. Although vapor liquid azeotropy is impossible for binary systems which are rigorously described by Raoult's law, for real systems, azeotropy is inevitable at temperatures where the saturation vapor pressure of the components are equal. Such a temperature is called a Bancroft point. However, not all binary systems exhibit such a point. Also, a Bancroft point must lie in the valid temperature ranges of the Antoine equation.
