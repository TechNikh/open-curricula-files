---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trader
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484394301
title: trader
categories:
    - Dictionary
---
trader
     n : someone who purchases and maintains an inventory of goods to
         be sold [syn: {bargainer}, {dealer}, {monger}]
