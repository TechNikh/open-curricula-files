---
title: Alternative fuel
article_ref: https://en.wikipedia.org/wiki/Alternative_fuel
tags:
    - Background
    - Biofuel
    - Biomass
    - Algae-based fuels
    - biodiesel
    - Alcohol fuels
    - Ammonia
    - Carbon-neutral and negative fuels
    - hydrogen
    - Hydrogen/compressed natural gas mixture
    - Liquid nitrogen
    - Compressed air
    - Propane autogas
    - Natural gas vehicles
    - Compressed natural gas fuel types
    - Practicality
    - Environmental analysis
    - Nuclear power and radiothermal generators
    - Nuclear reactors
    - Thorium fuelled nuclear reactors
    - Radiothermal generators
categories:
    - Conservation
---
