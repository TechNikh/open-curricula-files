---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dissipative_system
offline_file: ""
offline_thumbnail: ""
uuid: ac386f38-fee2-4957-bb9c-4d83a92c2bee
updated: 1484308797
title: Dissipative system
tags:
    - Overview
    - Dissipative systems in control theory
    - Quantum dissipative systems
categories:
    - Non-equilibrium thermodynamics
---
A dissipative system is a thermodynamically open system which is operating out of, and often far from, thermodynamic equilibrium in an environment with which it exchanges energy and matter.
