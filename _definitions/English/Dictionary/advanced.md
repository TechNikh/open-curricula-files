---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advanced
offline_file: ""
offline_thumbnail: ""
uuid: 9e68b72f-764d-458f-a3d7-53d73100f3b5
updated: 1484310447
title: advanced
categories:
    - Dictionary
---
advanced
     adj 1: farther along in physical or mental development; "the
            child's skeletal age was classified as `advanced'";
            "children in the advanced classes in elementary school
            read far above grade average"
     2: comparatively late in a course of development; "the illness
        had reached an advanced stage"; "an advanced state of
        exhaustion" [syn: {advanced(a)}]
     3: ahead of the times; "the advanced teaching methods"; "had
        advanced views on the subject"; "a forward-looking
        corporation"; "is British industry innovative enough?"
        [syn: {forward-looking}, {innovative}, {modern}]
     4: at a higher level in ...
