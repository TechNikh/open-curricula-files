---
version: 1
type: article
id: https://en.wikipedia.org/wiki/House_fly
offline_file: ""
offline_thumbnail: ""
uuid: 8eeec6b4-7f59-4c37-be91-3244a03c817d
updated: 1484308368
title: Housefly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fly_Eye_30wd_3x3.JPG
tags:
    - Description
    - Life cycle
    - Aging
    - Sex determination
    - Evolution
    - Relationship with humans
    - As a transmitter of disease
    - Potential in waste management
categories:
    - Uncategorized
---
The housefly (also house fly, house-fly or common housefly), Musca domestica, is a fly of the suborder Cyclorrhapha. It is believed to have evolved in the Cenozoic era, possibly in the Middle East, and has spread all over the world. It is the most common fly species found in habitations. Adult insects are grey to black with four dark longitudinal lines on the thorax, slightly hairy bodies and a single pair of membranous wings. They have red eyes, and the slightly larger female has these set further apart than the male.
