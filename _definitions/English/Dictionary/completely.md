---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/completely
offline_file: ""
offline_thumbnail: ""
uuid: a08a2fc4-b62d-4edb-8df1-4f7c808e7b47
updated: 1484310313
title: completely
categories:
    - Dictionary
---
completely
     adv 1: to a complete degree or to the full or entire extent
            (`whole' is often used informally for `wholly'); "he
            was wholly convinced"; "entirely satisfied with the
            meal"; "it was completely different from what we
            expected"; "was completely at fault"; "a totally new
            situation"; "the directions were all wrong"; "it was
            not altogether her fault"; "an altogether new
            approach"; "a whole new idea" [syn: {wholly}, {entirely},
             {totally}, {all}, {altogether}, {whole}] [ant: {partially}]
     2: so as to be complete; with everything necessary; "he had
        filled out the form ...
