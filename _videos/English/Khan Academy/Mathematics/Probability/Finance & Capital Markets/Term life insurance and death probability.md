---
version: 1
type: video
provider: YouTube
id: NSSoMafbBqQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Finance%20%26%20Capital%20Markets/Term%20life%20insurance%20and%20death%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 99c13c82-1a15-42ca-9bfa-2964a5769434
updated: 1486069688
title: Term life insurance and death probability
categories:
    - 'Finance & Capital Markets'
thumbnail_urls:
    - https://i3.ytimg.com/vi/NSSoMafbBqQ/default.jpg
    - https://i3.ytimg.com/vi/NSSoMafbBqQ/1.jpg
    - https://i3.ytimg.com/vi/NSSoMafbBqQ/2.jpg
    - https://i3.ytimg.com/vi/NSSoMafbBqQ/3.jpg
---
