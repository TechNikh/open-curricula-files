---
version: 1
type: video
provider: YouTube
id: 0lY4PcCYoyE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/Domain%20and%20range%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5f4a0da1-e977-4322-ad3a-ab4b422b1163
updated: 1486069563
title: Domain and range 2
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/0lY4PcCYoyE/default.jpg
    - https://i3.ytimg.com/vi/0lY4PcCYoyE/1.jpg
    - https://i3.ytimg.com/vi/0lY4PcCYoyE/2.jpg
    - https://i3.ytimg.com/vi/0lY4PcCYoyE/3.jpg
---
