---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vessel
offline_file: ""
offline_thumbnail: ""
uuid: 3bad8f61-812a-4622-a3a8-3508debffa05
updated: 1484310316
title: vessel
categories:
    - Dictionary
---
vessel
     n 1: a tube in which a body fluid circulates [syn: {vas}]
     2: a craft designed for water transportation [syn: {watercraft}]
     3: an object used as a container (especially for liquids)
