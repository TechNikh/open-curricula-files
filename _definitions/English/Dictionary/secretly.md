---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secretly
offline_file: ""
offline_thumbnail: ""
uuid: 84198e04-dc09-4237-bf5e-0b78fc98d3f4
updated: 1484310587
title: secretly
categories:
    - Dictionary
---
secretly
     adv 1: in secrecy; not openly; "met secretly to discuss the
            invasion plans"; "the children secretly went to the
            movies when they were supposed to be at the library";
            "they arranged to meet in secret" [syn: {in secret}, {on
            the Q.T.}, {on the QT}]
     2: not openly; inwardly; "they were secretly delighted at his
        embarrassment"; "hoped secretly she would change her mind"
