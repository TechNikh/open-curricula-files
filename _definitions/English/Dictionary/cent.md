---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cent
offline_file: ""
offline_thumbnail: ""
uuid: f92bdb66-cf3b-4c4f-abc8-ef41ecdeb058
updated: 1484310254
title: cent
categories:
    - Dictionary
---
cent
     n 1: a fractional monetary unit of several countries
     2: a coin worth one-hundredth of the value of the basic unit
        [syn: {penny}, {centime}]
