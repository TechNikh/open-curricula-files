---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cemetery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484581561
title: cemetery
categories:
    - Dictionary
---
cemetery
     n : a tract of land used for burials [syn: {graveyard}, {burial
         site}, {burial ground}, {burying ground}, {memorial park},
          {necropolis}]
