---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Derrick%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5cb476f3-1984-4b74-accc-3dad383f78ba
updated: 1484308838
title: "Derrick's theorem"
tags:
    - Original argument
    - "Pohozaev's identity"
    - Interpretation in the Hamiltonian form
    - Stability of localized time-periodic solutions
categories:
    - Stability theory
---
Derrick's theorem is an argument due to a physicist G.H. Derrick which shows that stationary localized solutions to a nonlinear wave equation or nonlinear Klein–Gordon equation in spatial dimensions three and higher are unstable.
