---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/also
offline_file: ""
offline_thumbnail: ""
uuid: b2220dd6-777e-4804-9b4d-c41bdf1c6254
updated: 1484310344
title: also
categories:
    - Dictionary
---
also
     adv : in addition; "he has a Mercedes, too" [syn: {besides}, {too},
            {likewise}, {as well}]
