---
version: 1
type: video
provider: YouTube
id: qfQv8GzyjB4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Fractions/Number%20sets%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c9e3fcdb-6bb7-4f02-948a-969b819c5f1e
updated: 1486069557
title: Number sets 2
categories:
    - Fractions
thumbnail_urls:
    - https://i3.ytimg.com/vi/qfQv8GzyjB4/default.jpg
    - https://i3.ytimg.com/vi/qfQv8GzyjB4/1.jpg
    - https://i3.ytimg.com/vi/qfQv8GzyjB4/2.jpg
    - https://i3.ytimg.com/vi/qfQv8GzyjB4/3.jpg
---
