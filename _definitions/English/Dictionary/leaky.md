---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leaky
offline_file: ""
offline_thumbnail: ""
uuid: 509b4889-956d-4992-9a94-429cac37faf8
updated: 1484310238
title: leaky
categories:
    - Dictionary
---
leaky
     adj 1: permitting leaks or leakage; "a leaky roof"; "a leaky
            defense system" [ant: {tight}]
     2: (of ships and boats) permitting leakage [syn: {leaking}]
     3: permitting liquids to penetrate; "a tarpaulin too leaky to
        be of much use"
     4: used informally; unable to retain urine
     5: prone to communicate confidential information [syn: {blabbermouthed},
         {talebearing(a)}, {tattling(a)}]
     [also: {leakiest}, {leakier}]
