---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aspire
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572021
title: aspire
categories:
    - Dictionary
---
aspire
     v : have an ambitious plan or a lofty goal [syn: {draw a bead on},
          {aim}, {shoot for}]
