---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philosophical
offline_file: ""
offline_thumbnail: ""
uuid: 68ddc512-a3f0-4b8e-b21a-935d7d2bcc25
updated: 1484310148
title: philosophical
categories:
    - Dictionary
---
philosophical
     adj 1: of or relating to philosophy or philosophers; "philosophical
            writing"; "a considerable knowledge of philosophical
            terminology" [syn: {philosophic}]
     2: characterized by the attitude of a philosopher; meeting
        trouble with level-headed detachment; "philosophical
        resignation"; "a philosophic attitude toward life" [syn: {philosophic}]
     3: characteristic of or imbued with the attitude of a
        philosopher or based on philosophy; "that breadth of
        outlook that distinguishes the philosophic mind"; "their
        differences were philosophical" [syn: {philosophic}] [ant:
         {nonphilosophical}]
