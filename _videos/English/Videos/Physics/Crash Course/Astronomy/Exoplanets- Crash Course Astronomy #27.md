---
version: 1
type: video
provider: YouTube
id: 7ATtD8x7vV0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Exoplanets-%20Crash%20Course%20Astronomy%20%2327.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a608a233-5489-46cd-a1f4-67d72d1ed04d
updated: 1486069684
title: 'Exoplanets- Crash Course Astronomy #27'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/7ATtD8x7vV0/default.jpg
    - https://i3.ytimg.com/vi/7ATtD8x7vV0/1.jpg
    - https://i3.ytimg.com/vi/7ATtD8x7vV0/2.jpg
    - https://i3.ytimg.com/vi/7ATtD8x7vV0/3.jpg
---
