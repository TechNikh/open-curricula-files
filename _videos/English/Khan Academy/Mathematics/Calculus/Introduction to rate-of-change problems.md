---
version: 1
type: video
provider: YouTube
id: Zyq6TmQVBxk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Introduction%20to%20rate-of-change%20problems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c8585450-2a05-4736-bc7d-020decc1b7cd
updated: 1486069694
title: Introduction to rate-of-change problems
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Zyq6TmQVBxk/default.jpg
    - https://i3.ytimg.com/vi/Zyq6TmQVBxk/1.jpg
    - https://i3.ytimg.com/vi/Zyq6TmQVBxk/2.jpg
    - https://i3.ytimg.com/vi/Zyq6TmQVBxk/3.jpg
---
