---
version: 1
type: video
provider: YouTube
id: rQkciLCmnPk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/3D%20standing%20waves%20in%20a%20sphere.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3203328-e9a8-40af-a4f5-155310d37211
updated: 1486069652
title: 3D standing waves in a sphere
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/rQkciLCmnPk/default.jpg
    - https://i3.ytimg.com/vi/rQkciLCmnPk/1.jpg
    - https://i3.ytimg.com/vi/rQkciLCmnPk/2.jpg
    - https://i3.ytimg.com/vi/rQkciLCmnPk/3.jpg
---
