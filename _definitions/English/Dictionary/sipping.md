---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sipping
offline_file: ""
offline_thumbnail: ""
uuid: 5c57c34f-ad9b-4552-b888-3edc4cb43253
updated: 1484310346
title: sipping
categories:
    - Dictionary
---
sip
     n : a small drink [syn: {nip}]
     v : drink in sips; "She was sipping her tea"
     [also: {sipping}, {sipped}]
