---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Berry_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: 560487e4-fa61-4e22-a647-bf55d6b99753
updated: 1484309275
title: Berry mechanism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Trigonal-bipyramidal-3D-balls-ax-eq.png
tags:
    - Berry mechanism in trigonal bipyramidal structure
    - Berry mechanism in square pyramidal structure
categories:
    - Molecular geometry
---
The Berry mechanism, or Berry pseudorotation mechanism, is a type of vibration causing molecules of certain geometries to isomerize by exchanging the two axial ligands (see Figure at right) for two of the equatorial ones. It is the most widely accepted mechanism for pseudorotation. It most commonly occurs in trigonal bipyramidal molecules, such as PF5, though it can also occur in molecules with a square pyramidal geometry. The Berry mechanism is named after R. Stephen Berry, who first described this mechanism in 1960.[1][2]
