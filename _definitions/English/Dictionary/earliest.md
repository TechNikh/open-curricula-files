---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earliest
offline_file: ""
offline_thumbnail: ""
uuid: 1aa12c0e-0fd1-4286-a152-7a51d11cbc36
updated: 1484310281
title: earliest
categories:
    - Dictionary
---
earliest
     adj : (comparative and superlative of `early') more early than;
           most early; "a fashion popular in earlier times"; "his
           earlier work reflects the influence of his teacher";
           "Verdi's earliest and most raucous opera" [syn: {earlier}]
     adv : with the least delay; "the soonest I can arrive is 3 P.M."
           [syn: {soonest}]
