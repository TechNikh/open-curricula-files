---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Annual_plant
offline_file: ""
offline_thumbnail: ""
uuid: eadaf1b4-207d-4961-9397-1d36b803bf41
updated: 1484308377
title: Annual plant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Doperwt_rijserwt_peulen_Pisum_sativum.jpg
tags:
    - Cultivation
    - Summer
    - Winter
    - Molecular genetics
categories:
    - Uncategorized
---
An annual plant is a plant that completes its life cycle, from germination to the production of seed, within one year, and then dies. Summer annuals germinate during spring or early summer and mature by autumn of the same year. Winter annuals germinate during the autumn and mature during the spring or summer of the following calendar year.[1]
