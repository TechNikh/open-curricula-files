---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intelligent
offline_file: ""
offline_thumbnail: ""
uuid: 394b3467-40ff-46a0-a551-3a4188b473eb
updated: 1484310445
title: intelligent
categories:
    - Dictionary
---
intelligent
     adj 1: having the capacity for thought and reason especially to a
            high degree; "is there intelligent life in the
            universe?"; "an intelligent question" [ant: {unintelligent}]
     2: possessing sound knowledge; "well-informed readers" [syn: {well-informed}]
     3: exercising or showing good judgment; "healthy scepticism";
        "a healthy fear of rattlesnakes"; "the healthy attitude of
        French laws"; "healthy relations between labor and
        management"; "an intelligent solution"; "a sound approach
        to the problem"; "sound advice"; "no sound explanation for
        his decision" [syn: {healthy}, {levelheaded}, {sound}]
     4: ...
