---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_hypergraph_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2a5aebfa-8f1b-4e26-8192-a210acd106be
updated: 1484309387
title: Symmetric hypergraph theorem
tags:
    - Statement
    - Applications
    - Notes
categories:
    - Graph coloring
---
The Symmetric hypergraph theorem is a theorem in combinatorics that puts an upper bound on the chromatic number of a graph (or hypergraph in general). The original reference for this paper is unknown at the moment, and has been called folklore.[1]
