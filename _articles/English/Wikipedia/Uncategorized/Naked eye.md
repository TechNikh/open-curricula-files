---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Naked_eye
offline_file: ""
offline_thumbnail: ""
uuid: 709a25ba-fce8-4170-b1d4-3654efd4706d
updated: 1484308371
title: Naked eye
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Eye_iris.jpg
tags:
    - Basic accuracies
    - Naked eye in astronomy
    - Space, geodesy and navigation
    - Small objects and maps
    - Environmental pollution
    - Literature
categories:
    - Uncategorized
---
Naked eye, also called bare eye or unaided eye, is the practice of engaging in visual perception unaided by a magnifying or light-collecting optical device, such as a telescope or microscope. Vision corrected to normal acuity using corrective lenses is considered "naked". In astronomy, the naked eye may be used to observe events that can be viewed without equipment, such as an astronomical conjunction, the passage of a comet, or a meteor shower. Sky lore and various tests demonstrate an impressive wealth of phenomena that can be seen with the unaided eye.
