---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mussorie
offline_file: ""
offline_thumbnail: ""
uuid: 73c887cc-abbd-4187-91ab-9e9bf922c266
updated: 1484308415
title: Mussoorie
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mussoorie_View_from_top_of_the_hill.jpg
tags:
    - History
    - Geography and Climate
    - Demographics
    - Accessibility
    - Tourism
    - Institutions
categories:
    - Uncategorized
---
Mussoorie (Garhwali/Hindi: Masūrī) is a hill station and a municipal board in the Dehradun District of the northern Indian state of Uttarakhand. It is located about 35 km from the state capital of Dehradun and 290 km north from the national capital of New Delhi. This hill station is situated in the foothills of the Garhwal Himalayan range. The adjoining town of Landour, which includes a military cantonment, is considered part of 'greater Mussoorie', as are the townships of Barlowganj and Jharipani. The pin code for Mussoorie is 248179. [1] Being at an average altitude of 1,880 metres (6,170 ft), Mussoorie, with its green hills and varied flora and fauna, is a fascinating hill resort. ...
