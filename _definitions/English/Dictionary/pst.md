---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pst
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484345881
title: pst
categories:
    - Dictionary
---
PST
     n : standard time in the 8th time zone west of Greenwich,
         reckoned at the 120th meridian west; used in far western
         states of the United States [syn: {Pacific Time}, {Pacific
         Standard Time}]
