---
version: 1
type: video
provider: YouTube
id: QqjcCvzWwww
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Ionic%20and%20covalent%20bonding%20animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d2e158b-584b-45bf-94fd-7deffba8b5c7
updated: 1486069662
title: Ionic and covalent bonding animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/QqjcCvzWwww/default.jpg
    - https://i3.ytimg.com/vi/QqjcCvzWwww/1.jpg
    - https://i3.ytimg.com/vi/QqjcCvzWwww/2.jpg
    - https://i3.ytimg.com/vi/QqjcCvzWwww/3.jpg
---
