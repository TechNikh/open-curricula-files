---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cauchy_sequence
offline_file: ""
offline_thumbnail: ""
uuid: eb11af56-55bc-42d1-8f1b-9e59213877d4
updated: 1484309262
title: Cauchy sequence
tags:
    - In real numbers
    - In a metric space
    - Completeness
    - Examples
    - 'Counter-example: rational numbers'
    - 'Counter-example: open interval'
    - Other properties
    - Generalizations
    - In topological vector spaces
    - In topological groups
    - In groups
    - In constructive mathematics
    - In a hyperreal continuum
categories:
    - Metric geometry
---
In mathematics, a Cauchy sequence (French pronunciation: ​[koʃi]; English pronunciation: /ˈkoʊʃiː/ KOH-shee), named after Augustin-Louis Cauchy, is a sequence whose elements become arbitrarily close to each other as the sequence progresses.[1] More precisely, given any small positive distance, all but a finite number of elements of the sequence are less than that given distance from each other.
