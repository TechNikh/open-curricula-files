---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kshetra
offline_file: ""
offline_thumbnail: ""
uuid: 3c5e33c0-429a-4200-b8dd-c95813093d58
updated: 1484308435
title: Tirtha (Hinduism)
tags:
    - Tirtha
    - Holy sites
    - Reasons
    - Method
    - Kshetra
    - Another definition of Tirtha
    - Bibliography
categories:
    - Uncategorized
---
Tirtha (IAST: Tīrtha) is a Sanskrit word that means "crossing place, ford", and refers to any place, text or person that is holy.[1][2] It particularly refers to pilgrimage sites and holy places in Hinduism as well as Jainism.[1][2][3]
