---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ladder
offline_file: ""
offline_thumbnail: ""
uuid: d3c4a898-723f-4cdf-b7be-abdbd7bff0a8
updated: 1484310154
title: ladder
categories:
    - Dictionary
---
ladder
     n 1: steps consisting of two parallel members connected by rungs;
          for climbing up or down
     2: ascending stages by which somebody or something can
        progress; "he climbed the career ladder"
     3: a row of unravelled stitches; "she got a run in her
        stocking" [syn: {run}, {ravel}]
     v : come unraveled or undone as if by snagging; "Her nylons were
         running" [syn: {run}]
