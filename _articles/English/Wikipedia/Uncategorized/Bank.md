---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banking
offline_file: ""
offline_thumbnail: ""
uuid: 9106f2da-9bda-4a97-9f9d-80918e0c611d
updated: 1484308460
title: Bank
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Prologue_Hammurabi_Code_Louvre_AO10237.jpg
tags:
    - History
    - Etymology
    - Definition
    - Bank
    - Standard business
    - Range of activities
    - Channels
    - Business models
    - Products
    - Retail banking
categories:
    - Uncategorized
---
A bank is a financial institution that accepts deposits from the public and creates credit.[1] Lending activities can be performed either directly or indirectly through capital markets. Due to their importance in the financial stability of a country, banks are highly regulated in most countries. Most nations have institutionalized a system known as fractional reserve banking under which banks hold liquid assets equal to only a portion of their current liabilities. In addition to other regulations intended to ensure liquidity, banks are generally subject to minimum capital requirements based on an international set of capital standards, known as the Basel Accords.
