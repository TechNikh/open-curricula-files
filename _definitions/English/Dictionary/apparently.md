---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apparently
offline_file: ""
offline_thumbnail: ""
uuid: 94d7ce22-d46e-4c5b-bebb-8044a316e172
updated: 1484310455
title: apparently
categories:
    - Dictionary
---
apparently
     adv 1: from appearances alone; "irrigation often produces bumper
            crops from apparently desert land"; "the child is
            seemingly healthy but the doctor is concerned"; "had
            been ostensibly frank as to his purpose while really
            concealing it"-Thomas Hardy; "on the face of it the
            problem seems minor" [syn: {seemingly}, {ostensibly},
            {on the face of it}]
     2: unmistakably (`plain' is often used informally for
        `plainly'); "the answer is obviously wrong"; "she was in
        bed and evidently in great pain"; "he was manifestly too
        important to leave off the guest list"; "it is all
        ...
