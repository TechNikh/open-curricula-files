---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coding_theory
offline_file: ""
offline_thumbnail: ""
uuid: 7ca0f949-8ec0-4b81-8a97-ce2c61b9ba7e
updated: 1484308551
title: Coding theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hamming.jpg
tags:
    - History of coding theory
    - Source coding
    - Definition
    - Properties
    - Principle
    - Example
    - Channel coding
    - Linear codes
    - Linear block codes
    - Convolutional codes
    - Cryptographical coding
    - Line coding
    - Other applications of coding theory
    - Group testing
    - Analog coding
    - Neural coding
    - Notes
categories:
    - Coding theory
---
Coding theory is the study of the properties of codes and their fitness for a specific application. Codes are used for data compression, cryptography, error-correction, and networking. Codes are studied by various scientific disciplines—such as information theory, electrical engineering, mathematics, linguistics, and computer science—for the purpose of designing efficient and reliable data transmission methods. This typically involves the removal of redundancy and the correction or detection of errors in the transmitted data.
