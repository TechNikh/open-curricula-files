---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kachori
offline_file: ""
offline_thumbnail: ""
uuid: a3cb96e5-518c-4612-8c1e-2bacdab818e5
updated: 1484308458
title: Kachori
categories:
    - Uncategorized
---
Kachori (pronounced [kətʃɔːɽiː]) is a spicy snack from the Indian subcontinent; popular in India, Pakistan and other parts of South Asia. It is also common in places with South Asian diaspora, such as Trinidad and Tobago, Guyana, and Suriname. Alternative names for the snack include Kachauri, Kachodi and Katchuri.
