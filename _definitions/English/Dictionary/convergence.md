---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convergence
offline_file: ""
offline_thumbnail: ""
uuid: 6324e426-ddca-4543-9642-abd0761578e5
updated: 1484310208
title: convergence
categories:
    - Dictionary
---
convergence
     n 1: the occurrence of two or more things coming together
     2: the approach of an infinite series to a finite limit [syn: {convergency}]
        [ant: {divergence}, {divergence}]
     3: a representation of common ground between theories or
        phenomena; "there was no overlap between their proposals"
        [syn: {overlap}, {intersection}]
     4: the act of converging (coming closer) [syn: {converging}, {convergency}]
