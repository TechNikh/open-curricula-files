---
version: 1
type: video
provider: YouTube
id: 55RA4h6IT0E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1a1aaed1-3043-4bee-9bab-1ce6be746dc4
updated: 1486069609
title: 2 digit times 1 digit example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/55RA4h6IT0E/default.jpg
    - https://i3.ytimg.com/vi/55RA4h6IT0E/1.jpg
    - https://i3.ytimg.com/vi/55RA4h6IT0E/2.jpg
    - https://i3.ytimg.com/vi/55RA4h6IT0E/3.jpg
---
