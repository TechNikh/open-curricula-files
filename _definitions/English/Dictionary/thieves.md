---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thieves
offline_file: ""
offline_thumbnail: ""
uuid: 038b366d-b70e-4258-ab6b-3e32af62e132
updated: 1484310144
title: thieves
categories:
    - Dictionary
---
thief
     n : a criminal who takes property belonging to someone else with
         the intention of keeping it or selling it [syn: {stealer}]
     [also: {thieves} (pl)]
