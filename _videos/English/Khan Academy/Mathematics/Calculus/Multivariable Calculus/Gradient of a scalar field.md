---
version: 1
type: video
provider: YouTube
id: OB8b8aDGLgE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Gradient%20of%20a%20scalar%20field.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ad505046-a002-4919-a3cc-48c6452f01ea
updated: 1486069690
title: Gradient of a scalar field
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/OB8b8aDGLgE/default.jpg
    - https://i3.ytimg.com/vi/OB8b8aDGLgE/1.jpg
    - https://i3.ytimg.com/vi/OB8b8aDGLgE/2.jpg
    - https://i3.ytimg.com/vi/OB8b8aDGLgE/3.jpg
---
