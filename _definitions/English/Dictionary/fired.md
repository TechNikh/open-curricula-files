---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fired
offline_file: ""
offline_thumbnail: ""
uuid: 877ebe72-e3b5-4410-b3c5-5eb0d7138e61
updated: 1484310315
title: fired
categories:
    - Dictionary
---
fired
     adj : having lost your job [syn: {discharged}, {dismissed}, {laid-off},
            {pink-slipped}]
