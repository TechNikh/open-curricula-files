---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Secant_line
offline_file: ""
offline_thumbnail: ""
uuid: dbd6638f-95da-4194-9363-32920a2780a6
updated: 1484309347
title: Secant line
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-CIRCLE_LINES.svg.png
categories:
    - Trigonometry
---
In geometry, a secant line of a curve is a line that (locally) intersects two points on the curve.[1] A chord is an interval of a secant line, the portion of the line that lies within the curve.[2] The word secant comes from the Latin word secare, meaning to cut.[3]
