---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supersolvable_arrangement
offline_file: ""
offline_thumbnail: ""
uuid: f241bac5-60d7-4bd1-a0f9-2642bd5cc4b9
updated: 1484308620
title: Supersolvable arrangement
categories:
    - Discrete geometry
---
Examples include arrangements associated with Coxeter groups of type A and B.
