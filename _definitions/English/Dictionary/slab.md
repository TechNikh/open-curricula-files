---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slab
offline_file: ""
offline_thumbnail: ""
uuid: b43c6f81-ceb5-404e-96f9-3589c3c726f8
updated: 1484310212
title: slab
categories:
    - Dictionary
---
slab
     n : block consisting of a thick piece of something
     [also: {slabbing}, {slabbed}]
