---
version: 1
type: video
provider: YouTube
id: e87rcej6V9A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de8803ba-9518-44bc-9f49-0055e17f7588
updated: 1486069641
title: 'Burning Ship Zoom - Colorful Crystals (Kalles Fraktaler)'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/e87rcej6V9A/default.jpg
    - https://i3.ytimg.com/vi/e87rcej6V9A/1.jpg
    - https://i3.ytimg.com/vi/e87rcej6V9A/2.jpg
    - https://i3.ytimg.com/vi/e87rcej6V9A/3.jpg
---
