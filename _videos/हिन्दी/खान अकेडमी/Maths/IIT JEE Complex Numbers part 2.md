---
version: 1
type: video
provider: YouTube
id: xIhZmSmRlbs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 311f76d2-f700-4db0-9366-e38e05bbf183
updated: 1486069616
title: IIT JEE Complex Numbers part 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/xIhZmSmRlbs/default.jpg
    - https://i3.ytimg.com/vi/xIhZmSmRlbs/1.jpg
    - https://i3.ytimg.com/vi/xIhZmSmRlbs/2.jpg
    - https://i3.ytimg.com/vi/xIhZmSmRlbs/3.jpg
---
