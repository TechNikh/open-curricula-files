---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intolerance
offline_file: ""
offline_thumbnail: ""
uuid: c9cd53a7-a39c-4b5c-9e64-dc3d13030d8d
updated: 1484310593
title: intolerance
categories:
    - Dictionary
---
intolerance
     n 1: impatience with annoyances; "his intolerance of
          interruptions"
     2: unwillingness to recognize and respect differences in
        opinions or beliefs [ant: {tolerance}]
