---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infinitesimal_transformation
offline_file: ""
offline_thumbnail: ""
uuid: ae61a241-23a2-4146-9ca7-a559a4029d47
updated: 1484309334
title: Infinitesimal transformation
tags:
    - History
    - Examples
    - "Operator version of Taylor's theorem"
categories:
    - Transformation (function)
---
In mathematics, an infinitesimal transformation is a limiting form of small transformation. For example one may talk about an infinitesimal rotation of a rigid body, in three-dimensional space. This is conventionally represented by a 3×3 skew-symmetric matrix A. It is not the matrix of an actual rotation in space; but for small real values of a parameter ε
