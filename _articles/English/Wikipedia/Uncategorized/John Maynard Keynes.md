---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Keynes
offline_file: ""
offline_thumbnail: ""
uuid: d8479a8c-2132-48c3-91d9-b7f5e0114ef9
updated: 1484308479
title: John Maynard Keynes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-David_Lloyd_George_1902.jpg
tags:
    - Early life and education
    - Career
    - First World War
    - Versailles peace conference
    - In the 1920s
    - During the Great Depression
    - Second World War
    - Postwar
    - Legacy
    - Keynesian ascendancy 1939–79
categories:
    - Uncategorized
---
John Maynard Keynes, 1st Baron Keynes[2] CB FBA (/ˈkeɪnz/ KAYNZ; 5 June 1883 – 21 April 1946), was a British economist. His ideas fundamentally changed the theory and practice of macroeconomics and the economic policies of governments. He built on and greatly refined earlier work on the causes of business cycles, and is widely considered to be one of the most influential economists of the 20th century and the founder of modern macroeconomics.[3][4][5][6] His ideas are the basis for the school of thought known as Keynesian economics and its various offshoots.
