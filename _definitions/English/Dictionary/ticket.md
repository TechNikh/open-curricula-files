---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ticket
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501041
title: ticket
categories:
    - Dictionary
---
ticket
     n 1: a commercial document showing that the holder is entitled to
          something (as to ride on public transportation or to
          enter a public entertainment)
     2: a summons issued to an offender (especially to someone who
        violates a traffic regulation)
     3: a list of candidates nominated by a political party to run
        for election to public offices [syn: {slate}]
     4: the appropriate or desirable thing; "this car could be just
        the ticket for a small family" [syn: {just the ticket}]
     v 1: issue a ticket or a fine to as a penalty; "I was fined for
          parking on the wrong side of the street"; "Move your car
          or else you ...
