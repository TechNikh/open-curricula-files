---
version: 1
type: video
provider: YouTube
id: 0RdI3-8G4Fs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Introduction%20to%20definite%20integrals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f7c6b03e-1731-4476-9d0e-8b5204995ec8
updated: 1486069694
title: Introduction to definite integrals
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/0RdI3-8G4Fs/default.jpg
    - https://i3.ytimg.com/vi/0RdI3-8G4Fs/1.jpg
    - https://i3.ytimg.com/vi/0RdI3-8G4Fs/2.jpg
    - https://i3.ytimg.com/vi/0RdI3-8G4Fs/3.jpg
---
