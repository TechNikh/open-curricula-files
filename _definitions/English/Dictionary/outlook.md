---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outlook
offline_file: ""
offline_thumbnail: ""
uuid: 2e1faf2f-6e30-4025-8e7e-a234c0b494da
updated: 1484310473
title: outlook
categories:
    - Dictionary
---
outlook
     n 1: a habitual or characteristic mental attitude that determines
          how you will interpret and respond to situations [syn: {mentality},
           {mindset}, {mind-set}]
     2: belief about (or mental picture of) the future [syn: {expectation},
         {prospect}]
     3: the act of looking out [syn: {lookout}]
