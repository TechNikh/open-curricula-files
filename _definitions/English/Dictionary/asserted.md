---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asserted
offline_file: ""
offline_thumbnail: ""
uuid: 4cf9b13b-b909-476b-882e-390c1a4a9188
updated: 1484310467
title: asserted
categories:
    - Dictionary
---
asserted
     adj : confidently declared to be so; "the asserted value of the
           painting"
