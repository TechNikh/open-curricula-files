---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geometric
offline_file: ""
offline_thumbnail: ""
uuid: 85647458-cff6-4ede-b728-97ab36990eae
updated: 1484310271
title: geometric
categories:
    - Dictionary
---
geometric
     adj 1: characterized by simple geometric forms in design and
            decoration; "a buffalo hide painted with red and black
            geometric designs" [syn: {geometrical}]
     2: of or relating to or determined by geometry [syn: {geometrical}]
