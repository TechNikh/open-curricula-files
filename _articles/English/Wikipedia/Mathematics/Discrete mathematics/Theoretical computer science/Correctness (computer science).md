---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correctness_(computer_science)
offline_file: ""
offline_thumbnail: ""
uuid: 906943ee-3c1b-4a84-8c12-ab6b70fc4946
updated: 1484308671
title: Correctness (computer science)
categories:
    - Theoretical computer science
---
In theoretical computer science, correctness of an algorithm is asserted when it is said that the algorithm is correct with respect to a specification. Functional correctness refers to the input-output behaviour of the algorithm (i.e., for each input it produces the expected output).[1]
