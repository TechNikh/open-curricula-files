---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bifurcation_diagram
offline_file: ""
offline_thumbnail: ""
uuid: 3d74f2a1-e80f-460e-a63c-946c3e6ed5f0
updated: 1484308696
title: Bifurcation diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-LogisticMap_BifurcationDiagram.png
tags:
    - Bifurcations in 1D discrete dynamical systems
    - Logistic map
    - Symmetry breaking in bifurcation sets
categories:
    - Bifurcation theory
---
In mathematics, particularly in dynamical systems, a bifurcation diagram shows the values visited or approached asymptotically (fixed points, periodic orbits, or chaotic attractors) of a system as a function of a bifurcation parameter in the system. It is usual to represent stable values with a solid line and unstable values with a dotted line, although often the unstable points are omitted. Bifurcation diagrams enable the visualization of bifurcation theory.
