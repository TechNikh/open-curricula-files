---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chiefly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484431141
title: chiefly
categories:
    - Dictionary
---
chiefly
     adv : for the most part; "he is mainly interested in butterflies"
           [syn: {principally}, {primarily}, {mainly}, {in the
           main}]
