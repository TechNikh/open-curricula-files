---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Universal_approximation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 664f8c3e-10d9-4b0d-9bc7-23c9261cb4a7
updated: 1484308669
title: Universal approximation theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_2.png
tags:
    - Formal statement
categories:
    - Theorems in discrete mathematics
---
In the mathematical theory of artificial neural networks, the universal approximation theorem states[1] that a feed-forward network with a single hidden layer containing a finite number of neurons (i.e., a multilayer perceptron), can approximate continuous functions on compact subsets of Rn, under mild assumptions on the activation function. The theorem thus states that simple neural networks can represent a wide variety of interesting functions when given appropriate parameters; however, it does not touch upon the algorithmic learnability of those parameters.
