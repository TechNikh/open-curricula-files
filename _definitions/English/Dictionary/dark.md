---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dark
offline_file: ""
offline_thumbnail: ""
uuid: 5ab61580-d7e1-43cd-99dc-04ecf3112883
updated: 1484310305
title: dark
categories:
    - Dictionary
---
dark
     adj 1: devoid or partially devoid of light or brightness; shadowed
            or black or somber-colored; "sitting in a dark
            corner"; "a dark day"; "dark shadows"; "the theater is
            dark on Mondays"; "dark as the inside of a black cat"
            [ant: {light}]
     2: (used of color) having a dark hue; "dark green"; "dark
        glasses"; "dark colors like wine red or navy blue" [ant: {light}]
     3: brunet (used of hair or skin or eyes); "dark eyes"
     4: stemming from evil characteristics or forces; wicked or
        dishonorable; "black deeds"; "a black lie"; "his black
        heart has concocted yet another black deed"; "Darth Vader
        of the ...
