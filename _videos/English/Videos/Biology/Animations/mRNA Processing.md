---
version: 1
type: video
provider: YouTube
id: YjWuVrzvZYA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/mRNA%20Processing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0bf4ecf7-a062-4803-84a7-4bed85348907
updated: 1486069666
title: mRNA Processing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YjWuVrzvZYA/default.jpg
    - https://i3.ytimg.com/vi/YjWuVrzvZYA/1.jpg
    - https://i3.ytimg.com/vi/YjWuVrzvZYA/2.jpg
    - https://i3.ytimg.com/vi/YjWuVrzvZYA/3.jpg
---
