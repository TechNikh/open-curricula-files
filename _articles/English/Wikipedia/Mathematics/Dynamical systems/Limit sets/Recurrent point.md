---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recurrent_point
offline_file: ""
offline_thumbnail: ""
uuid: 456bae94-948f-4241-92f2-5f23226351a7
updated: 1484308797
title: Recurrent point
categories:
    - Limit sets
---
In mathematics, a recurrent point for a function f is a point that is in its own limit set by f. Any neighborhood containing the recurrent point will also contain (a countable number of) iterates of it as well.
