---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jean-Christophe_Yoccoz
offline_file: ""
offline_thumbnail: ""
uuid: 8bc4ffd4-aede-4a81-a432-a08b7b4c4f53
updated: 1484308690
title: Jean-Christophe Yoccoz
categories:
    - Dynamical systems theorists
---
Jean-Christophe Yoccoz (May 29, 1957 – September 3, 2016) was a French mathematician. He was awarded a Fields Medal in 1994, for his work on dynamical systems.[1][2]
