---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurdles
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442121
title: hurdles
categories:
    - Dictionary
---
hurdles
     n : a footrace in which contestant must negotiate a series of
         hurdles [syn: {hurdling}, {hurdle race}]
