---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/predominantly
offline_file: ""
offline_thumbnail: ""
uuid: e907aee4-5e5b-414e-b3cc-ce6520064c31
updated: 1484310479
title: predominantly
categories:
    - Dictionary
---
predominantly
     adv : much greater in number or influence; "the patients are
           predominantly indigenous" [syn: {preponderantly}]
