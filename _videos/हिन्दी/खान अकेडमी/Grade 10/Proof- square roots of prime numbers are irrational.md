---
version: 1
type: video
provider: YouTube
id: JPwAieGdLRo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f1f55dd5-04e7-43ba-ba52-8c0f3aa08264
updated: 1486069618
title: 'Proof- square roots of prime numbers are irrational'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/JPwAieGdLRo/default.jpg
    - https://i3.ytimg.com/vi/JPwAieGdLRo/1.jpg
    - https://i3.ytimg.com/vi/JPwAieGdLRo/2.jpg
    - https://i3.ytimg.com/vi/JPwAieGdLRo/3.jpg
---
