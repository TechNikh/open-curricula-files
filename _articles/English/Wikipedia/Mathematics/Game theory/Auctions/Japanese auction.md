---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Japanese_auction
offline_file: ""
offline_thumbnail: ""
uuid: 573d3373-4a5a-445b-8bcd-8bd94c0facd7
updated: 1484309011
title: Japanese auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Japan.svg.png
tags:
    - Strategies
    - Comparison to Vickrey auction
    - Comparison to English auction
categories:
    - Auctions
---
