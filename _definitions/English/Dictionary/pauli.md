---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pauli
offline_file: ""
offline_thumbnail: ""
uuid: 7417e664-fbab-42ea-855b-abfd30f05168
updated: 1484310393
title: pauli
categories:
    - Dictionary
---
Pauli
     n : United States physicist (born in Austria) who proposed the
         exclusion principle (thus providing a theoretical basis
         for the periodic table) (1900-1958) [syn: {Wolfgang Pauli}]
