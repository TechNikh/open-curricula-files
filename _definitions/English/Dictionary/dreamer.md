---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dreamer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484609701
title: dreamer
categories:
    - Dictionary
---
dreamer
     n 1: someone who is dreaming
     2: someone guided more by ideals than by practical
        considerations [syn: {idealist}]
     3: a person who escapes into a world of fantasy [syn: {escapist},
         {wishful thinker}]
