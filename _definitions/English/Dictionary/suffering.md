---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffering
offline_file: ""
offline_thumbnail: ""
uuid: f884b501-c35a-4d90-9e60-b9c33297d77a
updated: 1484310379
title: suffering
categories:
    - Dictionary
---
suffering
     adj 1: troubled by pain or loss; "suffering refugees"
     2: very unhappy; full of misery; "he felt depressed and
        miserable"; "a message of hope for suffering humanity";
        "wretched prisoners huddled in stinking cages" [syn: {miserable},
         {wretched}]
     n 1: a state of acute pain [syn: {agony}, {excruciation}]
     2: misery resulting from affliction [syn: {woe}]
     3: psychological suffering; "the death of his wife caused him
        great distress" [syn: {distress}, {hurt}]
     4: feelings of mental or physical pain [syn: {hurt}]
