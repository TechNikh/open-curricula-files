---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Karlsruhe_metric
offline_file: ""
offline_thumbnail: ""
uuid: 071969e0-1e51-4dc9-9a85-f007b77623c4
updated: 1484309268
title: Karlsruhe metric
categories:
    - Metric geometry
---
In metric geometry, the Karlsruhe metric is a measure of distance that assumes travel is only possible along rays through the origin and circular arcs centered at the origin.The name alludes to the layout of the city of Karlsruhe, which has radial streets and circular avenues around a central point. This metric is also called Moscow metric.[1]
