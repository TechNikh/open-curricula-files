---
version: 1
type: video
provider: YouTube
id: U-7URcoVXDw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5fe4f343-e4f4-4dd4-9026-0cbf382cd7fc
updated: 1486069661
title: Amazing Glowing Liquid! (How to)
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/U-7URcoVXDw/default.jpg
    - https://i3.ytimg.com/vi/U-7URcoVXDw/1.jpg
    - https://i3.ytimg.com/vi/U-7URcoVXDw/2.jpg
    - https://i3.ytimg.com/vi/U-7URcoVXDw/3.jpg
---
