---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mortality
offline_file: ""
offline_thumbnail: ""
uuid: 83ab3181-5360-480d-9a39-11a93de8d1f6
updated: 1484310445
title: mortality
categories:
    - Dictionary
---
mortality
     n 1: the quality or state of being mortal [ant: {immortality}]
     2: the ratio of deaths in an area to the population of that
        area; expressed per 1000 per year [syn: {deathrate}, {death
        rate}, {morbidity}, {mortality rate}, {fatality rate}]
