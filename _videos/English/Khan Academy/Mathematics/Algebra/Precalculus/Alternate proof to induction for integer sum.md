---
version: 1
type: video
provider: YouTube
id: 1wnIsgUivEQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Alternate%20proof%20to%20induction%20for%20integer%20sum.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d64b5399-faf4-4b12-b6ea-f9e6e8bf73da
updated: 1486069571
title: Alternate proof to induction for integer sum
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/1wnIsgUivEQ/default.jpg
    - https://i3.ytimg.com/vi/1wnIsgUivEQ/1.jpg
    - https://i3.ytimg.com/vi/1wnIsgUivEQ/2.jpg
    - https://i3.ytimg.com/vi/1wnIsgUivEQ/3.jpg
---
Another way to prove the expression for the sum of all positive integers up to and including n

Watch the next lesson: https://www.khanacademy.org/math/precalculus/seq_induction/seq_and_series/v/explicit-and-recursive-definitions-of-sequences?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Missed the previous lesson? 
https://www.khanacademy.org/math/precalculus/seq_induction/proof_by_induction/v/proof-by-induction?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Precalculus on Khan Academy: You may think that precalculus is simply the course you take before calculus. You would be right, of course, but that definition doesn't mean anything unless you have some knowledge of what calculus is. Let's keep it simple, shall we? Calculus is a conceptual framework which provides systematic techniques for solving problems. These problems are appropriately applicable to analytic geometry and algebra. Therefore....precalculus gives you the background for the mathematical concepts, problems, issues and techniques that appear in calculus, including trigonometry, functions, complex numbers, vectors, matrices, and others. There you have it ladies and gentlemen....an introduction to precalculus!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Precalculus channel:
https://www.youtube.com/channel/UCBeHztHRWuVvnlwm20u2hNA?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
