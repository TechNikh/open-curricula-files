---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woollen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576221
title: woollen
categories:
    - Dictionary
---
woollen
     adj : of or related to or made of wool; "a woolen sweater" [syn: {woolen}]
     n : a fabric made from the hair of sheep [syn: {wool}, {woolen}]
