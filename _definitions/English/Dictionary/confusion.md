---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confusion
offline_file: ""
offline_thumbnail: ""
uuid: 6c0b7825-3000-4353-8c2f-629125161aa6
updated: 1484310469
title: confusion
categories:
    - Dictionary
---
confusion
     n 1: disorder resulting from a failure to behave predictably;
          "the army retreated in confusion"
     2: a mental state characterized by a lack of clear and orderly
        thought and behavior; "a confusion of impressions" [syn: {mental
        confusion}, {confusedness}, {disarray}]
     3: a feeling of embarrassment that leaves you confused [syn: {discombobulation}]
     4: an act causing a disorderly combination of elements with
        identities lost and distinctions blended; "the confusion
        of tongues at the Tower of Babel"
     5: a mistake that results from taking one thing to be another;
        "he changed his name in order to avoid confusion with ...
