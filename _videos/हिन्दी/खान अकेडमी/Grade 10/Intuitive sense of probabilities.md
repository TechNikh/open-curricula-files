---
version: 1
type: video
provider: YouTube
id: URW4RYYye7s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6607828c-fd61-4023-a5df-8f62da9cfe22
updated: 1486069618
title: Intuitive sense of probabilities
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/URW4RYYye7s/default.jpg
    - https://i3.ytimg.com/vi/URW4RYYye7s/1.jpg
    - https://i3.ytimg.com/vi/URW4RYYye7s/2.jpg
    - https://i3.ytimg.com/vi/URW4RYYye7s/3.jpg
---
