---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Devprayag
offline_file: ""
offline_thumbnail: ""
uuid: 00832e63-197a-4dd6-9bfd-b76c35e88366
updated: 1484308432
title: Devprayag
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AjitHota_BirthPlaceOfGanges.jpg
tags:
    - Overview
    - geography
    - Demographics
    - How to reach
    - air
    - Railway
    - Road
    - Gallery
categories:
    - Uncategorized
---
Devprayag (Devanagari: देव प्रयाग Deva prayāga) is a town and a nagar panchayat (municipality) in Tehri Garhwal district[1][2] in the state of Uttarakhand, India, and is one of the Panch Prayag (five confluences) of Alaknanda River where Alaknanda and Bhagirathi rivers meet and take the name Ganga or Ganges River.
