---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monohybrid
offline_file: ""
offline_thumbnail: ""
uuid: 5066e238-7035-4cef-a9f2-df1069ff3fc5
updated: 1484310301
title: monohybrid
categories:
    - Dictionary
---
monohybrid
     n : a hybrid produced by crossing parents that are homozygous
         except for a single gene locus that has two alleles (as
         in Mendel's experiments with garden peas)
