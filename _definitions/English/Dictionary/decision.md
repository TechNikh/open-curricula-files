---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decision
offline_file: ""
offline_thumbnail: ""
uuid: cb7a50e2-98f3-47aa-8dd0-fb6b5eb1ac9b
updated: 1484310527
title: decision
categories:
    - Dictionary
---
decision
     n 1: a position or opinion or judgment reached after
          consideration; "a decision unfavorable to the
          opposition"; "his conclusion took the evidence into
          account"; "satisfied with the panel's determination"
          [syn: {determination}, {conclusion}]
     2: the act of making up your mind about something; "the burden
        of decision was his"; "he drew his conclusions quickly"
        [syn: {determination}, {conclusion}]
     3: (boxing) a victory won on points when no knockout has
        occurred; "had little trouble in taking a unanimous
        decision over his opponent"
     4: the outcome of a game or contest; "the team dropped three
    ...
