---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phloem
offline_file: ""
offline_thumbnail: ""
uuid: 3bc767ac-abef-4e81-8539-2d5b12a960e5
updated: 1484310158
title: phloem
categories:
    - Dictionary
---
phloem
     n : (botany) tissue that conducts synthesized food substances
         (e.g., from leaves) to parts where needed; consists
         primarily of sieve tubes [syn: {bast}]
