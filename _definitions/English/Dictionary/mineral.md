---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mineral
offline_file: ""
offline_thumbnail: ""
uuid: 2287af9a-8712-42b3-af2b-9f23a30957a4
updated: 1484310339
title: mineral
categories:
    - Dictionary
---
mineral
     adj 1: relating to minerals; "mineral elements"; "mineral deposits"
     2: of or containing or derived from minerals; "a mineral
        deposit"; "mineral water" [ant: {animal}, {vegetable}]
     3: composed of matter other than plant or animal; "the
        inorganic mineral world"
     n : solid homogeneous inorganic substances occurring in nature
         having a definite chemical composition
