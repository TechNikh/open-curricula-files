---
version: 1
type: article
id: https://en.wikipedia.org/wiki/R
offline_file: ""
offline_thumbnail: ""
uuid: 75f61ac9-5203-4243-b8b4-8cd762c9545c
updated: 1484308323
title: R
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-R_cursiva.gif
tags:
    - History
    - Antiquity
    - Cursive
    - Name
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Other uses
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Calligraphic variants in the Latin alphabet
    - Ancestors and siblings in other alphabets
    - Abbreviations, signs and symbols
    - Encoding
    - Other representations
categories:
    - Uncategorized
---
