---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_distance
offline_file: ""
offline_thumbnail: ""
uuid: d6f0f76d-82e2-408c-a94f-f39562b51dd8
updated: 1484308547
title: Hamming distance
tags:
    - Examples
    - Properties
    - Error detection and error correction
    - History and applications
    - Algorithm example
    - Notes
categories:
    - Coding theory
---
In information theory, the Hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different. In another way, it measures the minimum number of substitutions required to change one string into the other, or the minimum number of errors that could have transformed one string into the other.
