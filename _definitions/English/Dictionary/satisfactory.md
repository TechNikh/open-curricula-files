---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satisfactory
offline_file: ""
offline_thumbnail: ""
uuid: 29e60704-3524-4435-a056-1c9687e029c0
updated: 1484310393
title: satisfactory
categories:
    - Dictionary
---
satisfactory
     adj 1: giving satisfaction; "satisfactory living conditions"; "his
            grades were satisfactory" [ant: {unsatisfactory}]
     2: meeting requirements; "the step makes a satisfactory seat"
        [syn: {acceptable}]
