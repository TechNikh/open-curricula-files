---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fulani
offline_file: ""
offline_thumbnail: ""
uuid: 3bb11cf8-3aee-4397-a89e-5a5742f0bc8b
updated: 1484310579
title: fulani
categories:
    - Dictionary
---
Fulani
     n 1: a member of a pastoral and nomadic people of western Africa;
          they are traditionally cattle herders of Muslim faith
          [syn: {Fula}, {Fulah}, {Fellata}, {Fulbe}]
     2: a family of languages of the Fulani people of West Africa
        and used as a lingua franca in the sub-Saharan regions
        from Senegal to Chad; the best known of the West African
        languages [syn: {Fula}, {Ful}, {Peul}]
