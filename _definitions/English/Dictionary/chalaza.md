---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chalaza
offline_file: ""
offline_thumbnail: ""
uuid: 5035b0e7-73ba-48a8-9dfe-103a5a77c34b
updated: 1484310160
title: chalaza
categories:
    - Dictionary
---
chalaza
     n 1: basal part of a plant ovule opposite the micropyle; where
          integument and nucellus are joined
     2: one of two spiral bands of tissue connecting the egg yolk to
        the enclosing membrane at either end of the shell
     [also: {chalazae} (pl)]
