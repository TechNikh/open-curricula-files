---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tangent_cone
offline_file: ""
offline_thumbnail: ""
uuid: 880b3409-94cc-432a-8dd0-f86bc067ed8e
updated: 1484309197
title: Tangent cone
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Node_%2528algebraic_geometry%2529.png'
tags:
    - Definitions in nonlinear analysis
    - Definition in convex geometry
    - Definition in algebraic geometry
categories:
    - Convex geometry
---
