---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shyly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434921
title: shyly
categories:
    - Dictionary
---
shyly
     adv : in a shy or timid or bashful manner; "he smiled shyly" [syn:
            {timidly}, {bashfully}]
