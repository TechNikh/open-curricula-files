---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eric_Maskin
offline_file: ""
offline_thumbnail: ""
uuid: a20f65a2-5a2b-48f1-90d3-dc6d7414b945
updated: 1484308513
title: Eric Maskin
tags:
    - Biography
    - Software patents
categories:
    - Game theorists
---
Eric Stark Maskin (born December 12, 1950) is an American economist and 2007 Nobel laureate recognized with Leonid Hurwicz and Roger Myerson "for having laid the foundations of mechanism design theory".[2] He is the Adams University Professor at Harvard University. Until 2011, he was the Albert O. Hirschman Professor of Social Science at the Institute for Advanced Study, and a visiting lecturer with the rank of professor at Princeton University.[3]
