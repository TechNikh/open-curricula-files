---
version: 1
type: video
provider: YouTube
id: lpXCvX6ZenM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%233%20%28b%20%26%20c%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a4fed4b9-3f31-47e9-8e9c-9ee5c16311b4
updated: 1486069690
title: '2011 Calculus BC Free Response #3 (b & c)'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/lpXCvX6ZenM/default.jpg
    - https://i3.ytimg.com/vi/lpXCvX6ZenM/1.jpg
    - https://i3.ytimg.com/vi/lpXCvX6ZenM/2.jpg
    - https://i3.ytimg.com/vi/lpXCvX6ZenM/3.jpg
---
