---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/picnic
offline_file: ""
offline_thumbnail: ""
uuid: 4ab3d0f4-5c09-421b-87a0-4d882f11bef2
updated: 1484310445
title: picnic
categories:
    - Dictionary
---
picnic
     n 1: a day devoted to an outdoor social gathering [syn: {field
          day}, {outing}]
     2: any undertaking that is easy to do; "marketing this product
        will be no picnic" [syn: {cinch}, {breeze}, {snap}, {duck
        soup}, {child's play}, {pushover}, {walkover}, {piece of
        cake}]
     3: any informal meal eaten outside or on an excursion
     v : eat alfresco, in the open air; "We picnicked near the lake
         on this gorgeous Sunday"
     [also: {picnicking}, {picnicked}]
