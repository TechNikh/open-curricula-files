---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Quasi-empiricism_in_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 48c5cdd5-a18a-4742-ad15-7d0e1d079385
updated: 1484308681
title: Quasi-empiricism in mathematics
tags:
    - Primary arguments
    - Operational aspects
categories:
    - Theoretical computer science
---
Quasi-empiricism in mathematics is the attempt in the philosophy of mathematics to direct philosophers' attention to mathematical practice, in particular, relations with physics, social sciences, and computational mathematics, rather than solely to issues in the foundations of mathematics. Of concern to this discussion are several topics: the relationship of empiricism (see Maddy) with mathematics, issues related to realism, the importance of culture, necessity of application, etc.
