---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/northern
offline_file: ""
offline_thumbnail: ""
uuid: 79d1fdaf-0667-435e-8f33-a4f16c59781c
updated: 1484310433
title: northern
categories:
    - Dictionary
---
northern
     adj 1: in or characteristic of a region of the United States north
            of (approximately) the Mason-Dixon line; "Northern
            liberals"; "northern industry"; "northern cities"
            [ant: {southern}]
     2: situated in or oriented toward the north; "the northern
        suburbs"; "going in a northerly direction" [syn: {northerly}]
     3: coming from the north; used especially of wind; "the north
        wind doth blow"; "a northern snowstorm"; "the winds are
        northerly" [syn: {northerly}]
     4: situated in or coming from regions of the north; "the
        northern hemisphere"; "northern autumn colors" [ant: {southern}]
     n : a dialect of ...
