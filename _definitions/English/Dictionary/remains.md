---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remains
offline_file: ""
offline_thumbnail: ""
uuid: 6b8c0159-9b13-4dcc-890a-3351d8763985
updated: 1484310281
title: remains
categories:
    - Dictionary
---
remains
     n 1: any object that is left unused or still extant; "I threw out
          the remains of my dinner"
     2: the dead body of a human being [syn: {cadaver}, {corpse}, {stiff},
         {clay}]
