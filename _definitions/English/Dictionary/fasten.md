---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fasten
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447821
title: fasten
categories:
    - Dictionary
---
fasten
     v 1: cause to be firmly attached; "fasten the lock onto the
          door"; "she fixed her gaze on the man" [syn: {fix}, {secure}]
          [ant: {unfasten}]
     2: become fixed or fastened; "This dress fastens in the back"
        [ant: {unfasten}]
     3: attach to; "They fastened various nicknames to each other"
     4: make tight or tighter; "Tighten the wire" [syn: {tighten}]
