---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_F._Banzhaf_III
offline_file: ""
offline_thumbnail: ""
uuid: 5e450c4d-3e0c-40ca-b7bf-71f89faa5707
updated: 1484308513
title: John F. Banzhaf III
tags:
    - Life and education
    - Career
    - Contributions to law and public policy
    - Copyright of computer software
    - Measuring the power of voting blocs in a committee
    - Teaching
    - Tobacco
    - Television advertising
    - Passive smoking
    - Obesity
    - Criticisms
    - Notes
categories:
    - Game theorists
---
John Francis Banzhaf III (/ˈbænz.hɑːf/;[1] born July 2, 1940) is an American public interest lawyer, legal activist and a law professor at George Washington University Law School. He is the founder of an antismoking advocacy group, Action on Smoking and Health.[2] He is noted for his advocacy and use of lawsuits as a method to promote what he believes is the public interest.
