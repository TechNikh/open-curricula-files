---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Varian%27s_theorems'
offline_file: ""
offline_thumbnail: ""
uuid: 60e8a334-6bcd-47a3-8e6d-0eb8dbfedb79
updated: 1484308517
title: "Varian's theorems"
categories:
    - Fair division
---
In welfare economics, Varian's theorems are several theorems related to fair allocation of homogeneous divisible resources. They describe conditions under which there exists a Pareto efficient (PE) envy-free (EF) allocation. They were published by Hal Varian in the 1970s.[1][2]
