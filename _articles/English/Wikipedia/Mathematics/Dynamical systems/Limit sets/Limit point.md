---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Limit_point
offline_file: ""
offline_thumbnail: ""
uuid: fbe3643b-ee41-41ad-bbc1-55630fe13c6b
updated: 1484308797
title: Limit point
tags:
    - Definition
    - Types of limit points
    - Some facts
categories:
    - Limit sets
---
In mathematics, a limit point of a set S in a topological space X is a point x (which is in X, but not necessarily in S) that can be "approximated" by points of S in the sense that every neighbourhood of x with respect to the topology on X also contains a point of S other than x itself. Note that x does not have to be an element of S. This concept profitably generalizes the notion of a limit and is the underpinning of concepts such as closed set and topological closure. Indeed, a set is closed if and only if it contains all of its limit points, and the topological closure operation can be thought of as an operation that enriches a set by uniting it with its limit points.
