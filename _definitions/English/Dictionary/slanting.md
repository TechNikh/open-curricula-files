---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slanting
offline_file: ""
offline_thumbnail: ""
uuid: d389b852-0af7-478a-b445-5490b1ef25e9
updated: 1484310162
title: slanting
categories:
    - Dictionary
---
slanting
     adj : having an oblique or slanted direction [syn: {aslant}, {aslope},
            {diagonal}, {slanted}, {sloped}, {sloping}]
