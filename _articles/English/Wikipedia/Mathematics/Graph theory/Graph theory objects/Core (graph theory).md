---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Core_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: faae7685-a195-421f-993e-f8c12925694d
updated: 1484309442
title: Core (graph theory)
tags:
    - Definition
    - Examples
    - Properties
    - Computational complexity
categories:
    - Graph theory objects
---
