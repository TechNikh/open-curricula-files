---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furrow
offline_file: ""
offline_thumbnail: ""
uuid: 9b03e336-0246-4123-80b2-8baffb99196e
updated: 1484310254
title: furrow
categories:
    - Dictionary
---
furrow
     n 1: a long shallow trench in the ground (especially one made by
          a plow)
     2: a slight depression in the smoothness of a surface; "his
        face has many lines"; "ironing gets rid of most wrinkles"
        [syn: {wrinkle}, {crease}, {crinkle}, {seam}, {line}]
     v 1: hollow out in the form of a furrow or groove; "furrow soil"
          [syn: {rut}, {groove}]
     2: make wrinkled or creased; "furrow one's brow" [syn: {wrinkle},
         {crease}]
     3: cut a furrow into a columns [syn: {chamfer}, {chase}]
