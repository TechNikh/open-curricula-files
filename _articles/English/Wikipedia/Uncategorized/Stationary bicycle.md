---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stationary_bicycle
offline_file: ""
offline_thumbnail: ""
uuid: fd0a4a26-6d77-4206-92da-cf2be4872c84
updated: 1484308375
title: Stationary bicycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Stationary_bicycle.jpg
tags:
    - History
    - Types
    - Uses
categories:
    - Uncategorized
---
A stationary bicycle (also known as exercise bicycle, exercise bike, or exercycle) is a device with saddle, pedals, and some form of handlebars arranged as on a bicycle, but used as exercise equipment rather than transportation.
