---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polar_sine
offline_file: ""
offline_thumbnail: ""
uuid: e4097273-a3a8-468d-937a-71fcf1b52eb2
updated: 1484309345
title: Polar sine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-3dvol.svg.png
tags:
    - Definition
    - n vectors in n-dimensional space
    - n vectors in m-dimensional space for m ≥ n
    - Properties
    - History
categories:
    - Trigonometry
---
