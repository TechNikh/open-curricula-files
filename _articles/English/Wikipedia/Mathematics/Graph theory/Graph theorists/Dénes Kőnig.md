---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/D%C3%A9nes_K%C5%91nig'
offline_file: ""
offline_thumbnail: ""
uuid: 2539b7b1-c38b-4b3a-82d5-9ac2798566d0
updated: 1484309468
title: Dénes Kőnig
tags:
    - Biography
    - Accomplishments
    - Bibliography
    - Notes
categories:
    - Graph theorists
---
Dénes Kőnig (September 21, 1884 – October 19, 1944) was a Jewish Hungarian mathematician who worked in and wrote the first textbook on the field of graph theory.
