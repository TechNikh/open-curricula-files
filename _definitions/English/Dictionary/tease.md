---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tease
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442901
title: tease
categories:
    - Dictionary
---
tease
     n 1: someone given to teasing (as by mocking or stirring
          curiosity) [syn: {teaser}, {annoyer}, {vexer}]
     2: a seductive woman who uses her sex appeal to exploit men
        [syn: {coquette}, {flirt}, {vamp}, {vamper}, {minx}, {prickteaser}]
     3: the act of harassing someone playfully or maliciously
        (especially by ridicule); provoking someone with
        persistent annoyances; "he ignored their teases"; "his
        ribbing was gentle but persistent" [syn: {teasing}, {ribbing}]
     v 1: annoy persistently; "The children teased the boy because of
          his stammer" [syn: {badger}, {pester}, {bug}, {beleaguer}]
     2: harass with persistent criticism ...
