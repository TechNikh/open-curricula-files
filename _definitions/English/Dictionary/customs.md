---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/customs
offline_file: ""
offline_thumbnail: ""
uuid: 8ea2cf2a-e561-40eb-8005-a3032a0334f8
updated: 1484310543
title: customs
categories:
    - Dictionary
---
customs
     n : money collected under a tariff [syn: {customs duty}, {custom},
          {impost}]
