---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detected
offline_file: ""
offline_thumbnail: ""
uuid: a7f5b7e5-acd5-47fe-b8d7-1d7a97da6980
updated: 1484310346
title: detected
categories:
    - Dictionary
---
detected
     adj 1: perceived or discerned; "the detected micrometeoritic
            material" [ant: {undetected}]
     2: perceived with the mind; "he winced at the detected flicker
        of irony in her voice"
