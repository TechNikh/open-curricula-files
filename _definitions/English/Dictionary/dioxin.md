---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dioxin
offline_file: ""
offline_thumbnail: ""
uuid: ecb3bde0-4fac-4d52-b72a-03a2952be191
updated: 1484310577
title: dioxin
categories:
    - Dictionary
---
dioxin
     n : any of several toxic or carcinogenic hydrocarbons that occur
         as impurities in herbicides
