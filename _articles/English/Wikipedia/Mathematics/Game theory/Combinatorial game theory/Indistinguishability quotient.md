---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indistinguishability_quotient
offline_file: ""
offline_thumbnail: ""
uuid: aaafd9ca-661c-4e89-a352-fb43cb394bde
updated: 1484308538
title: Indistinguishability quotient
tags:
    - 'Example: The misere quotient of Nim played with heaps of size 1 and 2 only'
    - Normal-play analysis
    - Misere-play analysis
    - Comparison with the conventional misere Nim strategy
    - Formal definition
    - Algorithms for computing misere quotients
categories:
    - Combinatorial game theory
---
In combinatorial game theory, and particularly in the theory of impartial games in misère play, an indistinguishability quotient is a commutative monoid that generalizes and localizes the Sprague-Grundy theorem for a specific game's rule set.
