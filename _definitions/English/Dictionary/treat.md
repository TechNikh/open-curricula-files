---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/treat
offline_file: ""
offline_thumbnail: ""
uuid: 391b94b7-e3d3-4738-81c7-b96b9af7c044
updated: 1484310383
title: treat
categories:
    - Dictionary
---
treat
     n 1: something considered choice to eat [syn: {dainty}, {delicacy},
           {goody}, {kickshaw}]
     2: an occurrence that cause special pleasure or delight
     v 1: interact in a certain way; "Do right by her"; "Treat him
          with caution, please"; "Handle the press reporters
          gently" [syn: {handle}, {do by}]
     2: subject to a process or treatment, with the aim of readying
        for some purpose, improving, or remedying a condition;
        "process cheese"; "process hair"; "treat the water so it
        can be drunk"; "treat the lawn with chemicals" ; "treat an
        oil spill" [syn: {process}]
     3: provide treatment for; "The doctor treated my ...
