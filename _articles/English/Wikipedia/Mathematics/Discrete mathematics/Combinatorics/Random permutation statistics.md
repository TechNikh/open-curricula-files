---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_permutation_statistics
offline_file: ""
offline_thumbnail: ""
uuid: fc795602-93b4-4d0b-aa01-1b06032f368d
updated: 1484308601
title: Random permutation statistics
tags:
    - The fundamental relation
    - Number of permutations that are involutions
    - Number of permutations that are mth roots of unity
    - Number of permutations of order exactly k
    - Number of permutations that are derangements
    - Derangements containing an even and an odd number of cycles
    - One hundred prisoners
    - A variation on the 100 prisoners problem (keys and boxes)
    - Number of permutations containing m cycles
    - Expected number of cycles of a given size m
    - Moments of fixed points
    - >
        Expected number of fixed points in random permutation raised
        to some power k
    - >
        Expected number of cycles of any length of a random
        permutation
    - >
        Number of permutations with a cycle of length larger than
        n/2
    - Expected number of transpositions of a random permutation
    - Expected cycle size of a random element
    - Probability that a random element lies on a cycle of size m
    - 'Probability that a random subset of [n] lies on the same cycle'
    - >
        Number of permutations containing an even number of even
        cycles
    - Permutations that are squares
    - Odd cycle invariants
    - >
        Permutations where the sum of the lengths of the even cycles
        is six
    - Permutations where all even cycles have the same length
    - >
        Permutations where the maximum length of an even cycle is
        four
    - The recurrence
    - A problem from the 2005 Putnam competition
    - >
        The difference between the number of cycles in even and odd
        permutations
    - 100 prisoners
categories:
    - Combinatorics
---
The statistics of random permutations, such as the cycle structure of a random permutation are of fundamental importance in the analysis of algorithms, especially of sorting algorithms, which operate on random permutations. Suppose, for example, that we are using quickselect (a cousin of quicksort) to select a random element of a random permutation. Quickselect will perform a partial sort on the array, as it partitions the array according to the pivot. Hence a permutation will be less disordered after quickselect has been performed. The amount of disorder that remains may be analysed with generating functions. These generating functions depend in a fundamental way on the generating ...
