---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/define
offline_file: ""
offline_thumbnail: ""
uuid: c9df5d9d-356e-49d9-9ac1-65421229a263
updated: 1484310234
title: define
categories:
    - Dictionary
---
define
     v 1: give a definition for the meaning of a word; "Define
          `sadness'"
     2: determine the essential quality of [syn: {specify}, {delineate},
         {delimit}, {delimitate}]
     3: determine the nature of; "What defines a good wine?"
     4: delineate the form or outline of; "The tree was clearly
        defined by the light"; "The camera could define the
        smallest object" [syn: {delineate}]
