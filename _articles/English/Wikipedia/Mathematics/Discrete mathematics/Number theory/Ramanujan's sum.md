---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ramanujan%27s_sum'
offline_file: ""
offline_thumbnail: ""
uuid: 2be7b3a3-fe88-48b5-898c-354d4515fd8c
updated: 1484308654
title: "Ramanujan's sum"
tags:
    - Notation
    - Formulas for cq(n)
    - Trigonometry
    - Kluyver
    - von Sterneck
    - Other properties of cq(n)
    - Table
    - Ramanujan expansions
    - Generating functions
    - σk(n)
    - d(n)
    - φ(n)
    - Λ(n)
    - Zero
    - r2s(n) (sums of squares)
    - r′2s(n) (sums of triangles)
    - Sums
    - Notes
categories:
    - Number theory
---
In number theory, a branch of mathematics, Ramanujan's sum, usually denoted cq(n), is a function of two positive integer variables q and n defined by the formula
