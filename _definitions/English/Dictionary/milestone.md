---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/milestone
offline_file: ""
offline_thumbnail: ""
uuid: 4efa1c67-0cbe-4232-87dc-ea6e974c46b6
updated: 1484310158
title: milestone
categories:
    - Dictionary
---
milestone
     n 1: stone post at side of a road to show distances [syn: {milepost}]
     2: a significant event in your life (or in a project)
