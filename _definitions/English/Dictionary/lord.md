---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lord
offline_file: ""
offline_thumbnail: ""
uuid: 57455824-3bf6-40b6-8725-e13394f8e81f
updated: 1484310186
title: lord
categories:
    - Dictionary
---
Lord
     n 1: terms referring to the Judeo-Christian God [syn: {Godhead},
          {Creator}, {Maker}, {Divine}, {God Almighty}, {Almighty},
           {Jehovah}]
     2: a person who has general authority over others [syn: {overlord},
         {master}]
     3: a titled peer of the realm [syn: {noble}, {nobleman}] [ant:
        {Lady}, {Lady}]
     v : make a lord of someone
