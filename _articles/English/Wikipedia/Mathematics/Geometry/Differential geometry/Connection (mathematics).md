---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connection_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: c4e09b90-d4cd-4375-a98f-3e06a8f2f482
updated: 1484309215
title: Connection (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Connection-on-sphere.png
tags:
    - 'Motivation: the unsuitability of coordinates'
    - Resolution
    - Historical survey of connections
    - Possible approaches
categories:
    - Differential geometry
---
In geometry, the notion of a connection makes precise the idea of transporting data along a curve or family of curves in a parallel and consistent manner. There are a variety of kinds of connections in modern geometry, depending on what sort of data one wants to transport. For instance, an affine connection, the most elementary type of connection, gives a means for transporting tangent vectors to a manifold from one point to another along a curve. An affine connection is typically given in the form of a covariant derivative, which gives a means for taking directional derivatives of vector fields: the infinitesimal transport of a vector field in a given direction.
