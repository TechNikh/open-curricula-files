---
version: 1
type: video
provider: YouTube
id: 6Jjl2xjuzGk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: eb601582-5f6e-44ac-a9f4-b56c03303e88
updated: 1486069658
title: グラスハープ　ハリーポッター HarryPotter
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/6Jjl2xjuzGk/default.jpg
    - https://i3.ytimg.com/vi/6Jjl2xjuzGk/1.jpg
    - https://i3.ytimg.com/vi/6Jjl2xjuzGk/2.jpg
    - https://i3.ytimg.com/vi/6Jjl2xjuzGk/3.jpg
---
