---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/millet
offline_file: ""
offline_thumbnail: ""
uuid: 7c4dc686-c781-41af-b4ff-3dc4b9099b54
updated: 1484310462
title: millet
categories:
    - Dictionary
---
millet
     n 1: any of various small-grained annual cereal and forage
          grasses of the genera Panicum, Echinochloa, Setaria,
          Sorghum, and Eleusine
     2: French painter of rural scenes (1814-1875) [syn: {Jean
        Francois Millet}]
     3: small seed of any of various annual cereal grasses
        especially Setaria italica
