---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bogey
offline_file: ""
offline_thumbnail: ""
uuid: a500e7be-e39c-4e1b-9205-fc163ff12ff7
updated: 1484310522
title: bogey
categories:
    - Dictionary
---
bogey
     n 1: an evil spirit [syn: {bogy}, {bogie}]
     2: (golf) a score of one stroke over par on a hole
     3: an unidentified (and possibly enemy) aircraft [syn: {bogy},
        {bogie}]
     v : to shoot in one stroke over par
