---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integer_triangle
offline_file: ""
offline_thumbnail: ""
uuid: f68bc8c4-152d-4794-9fbc-df26d5ba817d
updated: 1484308595
title: Integer triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangle-heronian.svg.png
tags:
    - General properties for an integer triangle
    - Integer triangles with given perimeter
    - Integer triangles with given largest side
    - Area of an integer triangle
    - Angles of an integer triangle
    - Side split by an altitude
    - Medians
    - Circumradius and inradius
    - Heronian triangles
    - General formula
    - Pythagorean triangles
    - >
        Pythagorean triangles with integer altitude from the
        hypotenuse
    - Heronian triangles with sides in arithmetic progression
    - Heronian triangles with one angle equal to twice another
    - Isosceles Heronian triangles
    - Heronian triangles as faces of a tetrahedron
    - Properties of Heronian triangles
    - Integer automedian triangles
    - Integer triangles in a 2D lattice
    - Integer triangles with specific angle properties
    - Integer triangles with a rational angle bisector
    - Integer triangles with integer n-sectors of all angles
    - >
        Integer triangles with one angle with a given rational
        cosine
    - >
        Integer triangles with a 60° angle (angles in arithmetic
        progression)
    - Integer triangles with a 120° angle
    - >
        Integer triangles with one angle equal to an arbitrary
        rational number times another angle
    - Integer triangles with one angle equal to twice another
    - Integer triangles with one angle equal to 3/2 times another
    - Integer triangles with one angle three times another
    - >
        Integer triangles with integer ratio of circumradius to
        inradius
    - Particular integer triangles
categories:
    - Discrete geometry
---
An integer triangle or integral triangle is a triangle all of whose sides have lengths that are integers. A rational triangle can be defined as one having all sides with rational length; any such rational triangle can be integrally rescaled (can have all sides multiplied by the same integer, namely a common multiple of their denominators) to obtain an integer triangle, so there is no substantive difference between integer triangles and rational triangles in this sense. Note however, that other definitions of the term "rational triangle" also exist: In 1914 Carmichael[1] used the term in the sense that we today use the term Heronian triangle; Somos[2] uses it to refer to triangles whose ...
