---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sensible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605201
title: sensible
categories:
    - Dictionary
---
sensible
     adj 1: showing reason or sound judgment; "a sensible choice"; "a
            sensible person" [syn: {reasonable}] [ant: {unreasonable}]
     2: able to feel or perceive; "even amoeba are sensible
        creatures"; "the more sensible p{ enveloping(a),
        shrouding(a), concealing,& (concealing by enclosing or
        wrapping as if in something that is not solid; "the
        enveloping darkness"; "hills concealed by shrouding
        mists") }arts of the skin" [syn: {sensitive}] [ant: {insensible}]
     3: acting with or showing thought and good sense; "a sensible
        young man" [syn: {thoughtful}]
     4: marked by the exercise of good judgment or common sense in
   ...
