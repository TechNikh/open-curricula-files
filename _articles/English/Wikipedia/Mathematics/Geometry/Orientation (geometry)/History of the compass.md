---
version: 1
type: article
id: https://en.wikipedia.org/wiki/History_of_the_compass
offline_file: ""
offline_thumbnail: ""
uuid: 734fe484-369c-4b9a-a5fb-c22088999ec9
updated: 1484309295
title: History of the compass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_4.png
tags:
    - Navigation prior to the compass
    - Geomancy and feng shui
    - Early navigational compass
    - China
    - Medieval Europe
    - Muslim world
    - India
    - Medieval Africa
    - Dry compass
    - Bearing compass
    - Liquid compass
    - Non-navigational uses
    - Astronomy
    - Building orientation
    - mining
    - Sun compass
    - Notes
categories:
    - Orientation (geometry)
---
The history of the compass extends back for more than 2000 years. The first compasses were made of lodestone, a naturally magnetized ore of iron, in Han dynasty China between 300 and 200 BC.[1] The compass was later used for navigation by the Song Dynasty.[2] Later compasses were made of iron needles, magnetized by striking them with a lodestone. Dry compasses begin appearing around 1300 in Medieval Europe.[3] This was supplanted in the early 20th century by the liquid-filled magnetic compass.[4]
