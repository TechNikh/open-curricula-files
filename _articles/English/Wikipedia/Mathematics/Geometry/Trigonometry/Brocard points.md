---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brocard_points
offline_file: ""
offline_thumbnail: ""
uuid: e0829dcf-b5b9-40da-a2ab-b514d73989ff
updated: 1484309340
title: Brocard points
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Brocard_point.svg.png
tags:
    - Definition
    - Construction
    - Trilinears and barycentrics of the first two Brocard points
    - The segment between the first two Brocard points
    - Distance from circumcenter
    - Similarities and congruences
    - Notes
categories:
    - Trigonometry
---
