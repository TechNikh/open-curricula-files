---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coca-cola
offline_file: ""
offline_thumbnail: ""
uuid: 8ea5df1d-7d59-46e5-a19d-9183214cebd3
updated: 1484308463
title: Coca-Cola
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Flasche_Coca-Cola_0%252C2_Liter.jpg'
tags:
    - History
    - 19th-century historical origins
    - The Coca-Cola Company
    - Origins of bottling
    - 20th century
    - New Coke
    - 21st century
    - Production
    - Ingredients
    - Formula of natural flavorings
categories:
    - Uncategorized
---
Coca-Cola (often referred to simply as Coke) is an American carbonated soft drink[1] produced by The Coca-Cola Company in Atlanta, Georgia, United States. Originally intended as a patent medicine, it was invented in the late 19th century by John Pemberton. Coca-Cola was bought out by businessman Asa Griggs Candler, whose marketing tactics led Coke to its dominance of the world soft-drink market throughout the 20th century. The drink's name refers to two of its original ingredients, which were kola nuts (a source of caffeine) and coca leaves. The current formula of Coca-Cola remains a trade secret, although a variety of reported recipes and experimental recreations have been published.
