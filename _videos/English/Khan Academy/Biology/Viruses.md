---
version: 1
type: video
provider: YouTube
id: 0h5Jd7sgQWY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Viruses.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7e409cbc-082c-4092-bf38-bedbdfcee66e
updated: 1486069579
title: Viruses
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/0h5Jd7sgQWY/default.jpg
    - https://i3.ytimg.com/vi/0h5Jd7sgQWY/1.jpg
    - https://i3.ytimg.com/vi/0h5Jd7sgQWY/2.jpg
    - https://i3.ytimg.com/vi/0h5Jd7sgQWY/3.jpg
---
Introduction to viruses
More free lessons at: http://www.khanacademy.org/video?v=0h5Jd7sgQWY
