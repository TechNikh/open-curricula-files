---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/branch
offline_file: ""
offline_thumbnail: ""
uuid: 1ea61df1-a2ca-47f3-8577-0c9df77a4010
updated: 1484310198
title: branch
categories:
    - Dictionary
---
branch
     n 1: an administrative division of some larger or more complex
          organization; "a branch of Congress" [syn: {subdivision},
           {arm}]
     2: a division of a stem, or secondary stem arising from the
        main stem of a plant
     3: a part of a forked or branching shape; "he broke off one of
        the branches"; "they took the south fork" [syn: {fork}, {leg},
         {ramification}]
     4: a natural consequence of development [syn: {outgrowth}, {offshoot},
         {offset}]
     5: a stream or river connected to a larger one
     6: any projection that is thought to resemble an arm; "the arm
        of the record player"; "an arm of the sea"; "a branch of
 ...
