---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thrust
offline_file: ""
offline_thumbnail: ""
uuid: a670bafe-f02f-40a5-b8b9-6d414f38aa41
updated: 1484310335
title: thrust
categories:
    - Dictionary
---
thrust
     n 1: the force used in pushing; "the push of the water on the
          walls of the tank"; "the thrust of the jet engines"
          [syn: {push}]
     2: a thrusting blow with a knife or other sharp pointed
        instrument; "one strong stab to the heart killed him"
        [syn: {stab}, {knife thrust}]
     3: the act of applying force to propel something; "after
        reaching the desired velocity the drive is cut off" [syn:
        {drive}, {driving force}]
     4: verbal criticism; "he enlivened his editorials with barbed
        thrusts at politicians"
     5: a sharp hand gesture (resembling a blow); "he warned me with
        a jab with his finger"; "he made a ...
