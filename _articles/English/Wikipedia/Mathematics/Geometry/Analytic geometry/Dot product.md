---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dot_product
offline_file: ""
offline_thumbnail: ""
uuid: 8de7cfc1-4ebc-4313-b633-c718b5800c34
updated: 1484309165
title: Dot product
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dot_Product.svg.png
tags:
    - Definition
    - Algebraic definition
    - Geometric definition
    - Scalar projection and first properties
    - Equivalence of the definitions
    - Properties
    - Application to the law of cosines
    - Triple product expansion
    - Physics
    - Generalizations
    - Complex vectors
    - Inner product
    - Functions
    - Weight function
    - Dyadics and matrices
    - Tensors
    - Computation
    - Algorithms
    - Libraries
categories:
    - Analytic geometry
---
In mathematics, the dot product or scalar product[1] (sometimes inner product in the context of Euclidean space, or rarely projection product for emphasizing the geometric significance), is an algebraic operation that takes two equal-length sequences of numbers (usually coordinate vectors) and returns a single number. This operation can be defined either algebraically or geometrically. Algebraically, it is the sum of the products of the corresponding entries of the two sequences of numbers. Geometrically, it is the product of the Euclidean magnitudes of the two vectors and the cosine of the angle between them. The name "dot product" is derived from the centered dot " · " that is often ...
