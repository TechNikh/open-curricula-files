---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Series-parallel_graph
offline_file: ""
offline_thumbnail: ""
uuid: 01f2d1fd-f632-4e54-b158-c9eadf9d0b25
updated: 1484309448
title: Series-parallel graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Series_parallel_composition.svg.png
tags:
    - Definition and terminology
    - Alternative definition
    - Properties
    - Research involving series-parallel graphs
    - Generalization
categories:
    - Graph operations
---
In graph theory, series-parallel graphs are graphs with two distinguished vertices called terminals, formed recursively by two simple composition operations. They can be used to model series and parallel electric circuits.
