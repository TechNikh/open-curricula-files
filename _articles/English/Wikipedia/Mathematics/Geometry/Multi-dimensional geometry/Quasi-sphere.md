---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quasi-sphere
offline_file: ""
offline_thumbnail: ""
uuid: 75546d76-7fdd-4a05-9f96-4f2d36c1fe24
updated: 1484309289
title: Quasi-sphere
tags:
    - Notation and terminology
    - Definition
    - Geometric characterizations
    - Centre and radius squared
    - Diameter and radius
    - Partitioning
    - Notes
categories:
    - Multi-dimensional geometry
---
In mathematics and theoretical physics, a quasi-sphere is a generalization of the hypersphere and the hyperplane to the context of a pseudo-Euclidean space. It may be described as the set of points for which the quadratic form for the space applied to the displacement vector from a centre point is a constant value, with the inclusion of hyperplanes as a limiting case.
