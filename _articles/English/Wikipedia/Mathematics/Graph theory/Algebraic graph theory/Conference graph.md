---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conference_graph
offline_file: ""
offline_thumbnail: ""
uuid: 810a1711-e336-4055-aa1c-442e2b111e1f
updated: 1484309371
title: Conference graph
categories:
    - Algebraic graph theory
---
In the mathematical area of graph theory, a conference graph is a strongly regular graph with parameters v, k = (v − 1)/2, λ = (v − 5)/4, and μ = (v − 1)/4. It is the graph associated with a symmetric conference matrix, and consequently its order v must be 1 (modulo 4) and a sum of two squares.
