---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orbit
offline_file: ""
offline_thumbnail: ""
uuid: 34c30845-4c5f-4686-9702-41fe3947eaab
updated: 1484310391
title: orbit
categories:
    - Dictionary
---
orbit
     n 1: the (usually elliptical) path described by one celestial
          body in its revolution about another; "he plotted the
          orbit of the moon" [syn: {celestial orbit}]
     2: a particular environment or walk of life; "his social sphere
        is limited"; "it was a closed area of employment"; "he's
        out of my orbit" [syn: {sphere}, {domain}, {area}, {field},
         {arena}]
     3: an area in which something acts or operates or has power or
        control: "the range of a supersonic jet"; "the ambit of
        municipal legislation"; "within the compass of this
        article"; "within the scope of an investigation"; "outside
        the reach of the ...
