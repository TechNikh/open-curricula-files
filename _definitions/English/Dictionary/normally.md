---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/normally
offline_file: ""
offline_thumbnail: ""
uuid: 48ad838e-d2fb-439d-b56e-1f45cfaa6cb2
updated: 1484310266
title: normally
categories:
    - Dictionary
---
normally
     adv : under normal conditions; "usually she was late" [syn: {usually},
            {unremarkably}, {commonly}, {ordinarily}] [ant: {unusually}]
