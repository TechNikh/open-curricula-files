---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sullen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414101
title: sullen
categories:
    - Dictionary
---
sullen
     adj 1: showing a brooding ill humor; "a dark scowl"; "the
            proverbially dour New England Puritan"; "a glum,
            hopeless shrug"; "he sat in moody silence"; "a morose
            and unsociable manner"; "a saturnine, almost
            misanthropic young genius"- Bruce Bliven; "a sour
            temper"; "a sullen crowd" [syn: {dark}, {dour}, {glowering},
             {glum}, {moody}, {morose}, {saturnine}, {sour}]
     2: darkened by clouds; "a heavy sky" [syn: {heavy}, {lowering},
         {threatening}]
