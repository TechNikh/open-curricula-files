---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cause
offline_file: ""
offline_thumbnail: ""
uuid: 5d6b8513-ca73-4809-9ac1-7248e873f21d
updated: 1484310359
title: cause
categories:
    - Dictionary
---
cause
     n 1: events that provide the generative force that is the origin
          of something; "they are trying to determine the cause of
          the crash"
     2: a justification for something existing or happening; "he had
        no cause to complain"; "they had good reason to rejoice"
        [syn: {reason}, {grounds}]
     3: a series of actions advancing a principle or tending toward
        a particular end; "he supported populist campaigns"; "they
        worked in the cause of world peace"; "the team was ready
        for a drive toward the pennant"; "the movement to end
        slavery"; "contributed to the war effort" [syn: {campaign},
         {crusade}, {drive}, ...
