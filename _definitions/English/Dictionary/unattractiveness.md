---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unattractiveness
offline_file: ""
offline_thumbnail: ""
uuid: 11a7bb54-3b4c-445e-924f-fa6c6c5b39c6
updated: 1484310527
title: unattractiveness
categories:
    - Dictionary
---
unattractiveness
     n : ugliness that is not appealing [ant: {attractiveness}]
