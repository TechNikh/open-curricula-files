---
version: 1
type: video
provider: YouTube
id: 96ZEmUbnuU8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/7th%20grade/Setting%20up%20proportions%20to%20solve%20word%20problems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7cdd722e-0bcc-441d-822f-ebc1eb5a63ea
updated: 1486069571
title: Setting up proportions to solve word problems
categories:
    - 7th grade
thumbnail_urls:
    - https://i3.ytimg.com/vi/96ZEmUbnuU8/default.jpg
    - https://i3.ytimg.com/vi/96ZEmUbnuU8/1.jpg
    - https://i3.ytimg.com/vi/96ZEmUbnuU8/2.jpg
    - https://i3.ytimg.com/vi/96ZEmUbnuU8/3.jpg
---
Some examples of writing two ratios and setting them equal to each other to solve proportion word problems

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/cc-seventh-grade-math/cc-7th-ratio-proportion/cc-7th-write-and-solve-proportions/e/writing_proportions?utm_source=YT&utm_medium=Desc&utm_campaign=7thgrade

Watch the next lesson: https://www.khanacademy.org/math/cc-seventh-grade-math/cc-7th-ratio-proportion/cc-7th-write-and-solve-proportions/v/find-an-unknown-in-a-proportion-2?utm_source=YT&utm_medium=Desc&utm_campaign=7thgrade

Missed the previous lesson? 
https://www.khanacademy.org/math/cc-seventh-grade-math/cc-7th-ratio-proportion/cc-7th-write-and-solve-proportions/v/find-an-unknown-in-a-proportion?utm_source=YT&utm_medium=Desc&utm_campaign=7thgrade

Grade 7th on Khan Academy: 7th grade takes much of what you learned in 6th grade to an entirely new level. In particular, you'll now learn to do everything with negative numbers (we're talking everything--adding, subtracting, multiplying, dividing, fractions, decimals... everything!). You'll also take your algebraic skills to new heights by tackling two-step equations. 7th grade is also when you start thinking about probability (which is super important for realizing that casinos and lotteries are really just ways of taking money away from people who don't know probability) and dig deeper into the world of data and statistics. Onward! (Content was selected for this grade level based on a typical curriculum in the United States.)

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan AcademyÂÃÂªs 7th grade channel:
https://www.youtube.com/channel/UCzKsXcrLSLDG7VN1LODlRkw?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
