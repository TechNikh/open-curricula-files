---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628661
title: feast
categories:
    - Dictionary
---
feast
     n 1: a ceremonial dinner party for many people [syn: {banquet}]
     2: something experienced with great delight; "a feast for the
        eyes"
     3: a meal that is well prepared and greatly enjoyed; "a banquet
        for the graduating seniors"; "the Thanksgiving feast";
        "they put out quite a spread" [syn: {banquet}, {spread}]
     4: an elaborate party (often outdoors) [syn: {fete}, {fiesta}]
     v 1: partake in a feast or banquet [syn: {banquet}, {junket}]
     2: provide a feast or banquet for [syn: {banquet}, {junket}]
     3: gratify; "feed one's eyes on a gorgeous view" [syn: {feed}]
