---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reply
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436121
title: reply
categories:
    - Dictionary
---
reply
     n 1: a statement (either spoken or written) that is made in reply
          to a question or request or criticism or accusation; "I
          waited several days for his answer"; "he wrote replies
          to several of his critics" [syn: {answer}, {response}]
     2: the speech act of continuing a conversational exchange; "he
        growled his reply" [syn: {response}]
     v : reply or respond to; "She didn't want to answer"; "answer
         the question"; "We answered that we would accept the
         invitation" [syn: {answer}, {respond}]
     [also: {replied}]
