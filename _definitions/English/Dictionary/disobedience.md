---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disobedience
offline_file: ""
offline_thumbnail: ""
uuid: 057c5513-8bf7-48ba-be9a-feb8efe68867
updated: 1484310172
title: disobedience
categories:
    - Dictionary
---
disobedience
     n 1: the failure to obey [syn: {noncompliance}] [ant: {conformity},
           {obedience}]
     2: the trait of being unwilling to obey [ant: {obedience}]
