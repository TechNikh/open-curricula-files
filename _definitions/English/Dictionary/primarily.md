---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/primarily
offline_file: ""
offline_thumbnail: ""
uuid: f89e5c76-7ad5-42f5-93df-04e38d15b0b7
updated: 1484310391
title: primarily
categories:
    - Dictionary
---
primarily
     adv 1: for the most part; "he is mainly interested in butterflies"
            [syn: {chiefly}, {principally}, {mainly}, {in the main}]
     2: of primary import; "this is primarily a question of
        economics"; "it was in the first place a local matter"
        [syn: {in the first place}] [ant: {secondarily}]
