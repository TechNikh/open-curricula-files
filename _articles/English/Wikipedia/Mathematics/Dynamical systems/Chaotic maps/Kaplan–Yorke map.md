---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kaplan%E2%80%93Yorke_map'
offline_file: ""
offline_thumbnail: ""
uuid: e71cfb7a-db9f-4fb1-bb52-729f5a65c422
updated: 1484308722
title: Kaplan–Yorke map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kaplan-Yorke_map.png
categories:
    - Chaotic maps
---
The Kaplan–Yorke map is a discrete-time dynamical system. It is an example of a dynamical system that exhibits chaotic behavior. The Kaplan–Yorke map takes a point (xn, yn ) in the plane and maps it to a new point given by
