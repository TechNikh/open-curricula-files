---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/independently
offline_file: ""
offline_thumbnail: ""
uuid: 7f13f502-8664-4384-9073-da73906aa2dc
updated: 1484310325
title: independently
categories:
    - Dictionary
---
independently
     adv 1: on your own; without outside help; "the children worked on
            the project independently"
     2: apart from others; "the clothes were hung severally" [syn: {severally}]
