---
title: Pulmonary Volumes & Capacities
youtube_id: ndf7Mn_eB0I
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
