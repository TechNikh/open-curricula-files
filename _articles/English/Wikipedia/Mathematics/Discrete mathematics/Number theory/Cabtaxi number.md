---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cabtaxi_number
offline_file: ""
offline_thumbnail: ""
uuid: d945e65d-e766-4863-b193-89fad5b707a0
updated: 1484308636
title: Cabtaxi number
categories:
    - Number theory
---
In mathematics, the n-th cabtaxi number, typically denoted Cabtaxi(n), is defined as the smallest positive integer that can be written as the sum of two positive or negative or 0 cubes in n ways. Such numbers exist for all n (since taxicab numbers exist for all n); however, only 10 are known (sequence A047696 in the OEIS):
