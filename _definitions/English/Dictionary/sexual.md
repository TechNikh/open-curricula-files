---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sexual
offline_file: ""
offline_thumbnail: ""
uuid: 12792cb0-647f-46db-9edd-ac9258593c80
updated: 1484310293
title: sexual
categories:
    - Dictionary
---
sexual
     adj 1: of or relating to or characterized by sexuality; "sexual
            orientation"; "sexual distinctions"
     2: having or involving sex; "sexual reproduction"; "sexual
        spores" [ant: {asexual}]
     3: involved in a sexual relationship; "the intimate (or sexual)
        relations between husband and wife"; "she had been
        intimate with many men" [syn: {intimate}]
