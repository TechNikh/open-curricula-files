---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crush
offline_file: ""
offline_thumbnail: ""
uuid: 93cde2cd-293f-4701-be52-a823a3c0c5d8
updated: 1484310339
title: crush
categories:
    - Dictionary
---
crush
     n 1: leather that has had its grain pattern accentuated [syn: {crushed
          leather}]
     2: a dense crowd of people [syn: {jam}, {press}]
     3: temporary love of an adolescent [syn: {puppy love}, {calf
        love}, {infatuation}]
     4: the act of crushing [syn: {crunch}, {compaction}]
     v 1: come down on or keep down by unjust use of one's authority;
          "The government oppresses political activists" [syn: {oppress},
           {suppress}]
     2: to compress with violence, out of natural shape or
        condition; "crush an aluminum can"; "squeeze a lemon"
        [syn: {squash}, {squelch}, {mash}, {squeeze}]
     3: come out better in a competition, race, ...
