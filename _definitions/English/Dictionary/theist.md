---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440381
title: theist
categories:
    - Dictionary
---
theist
     adj : of or relating to theism [syn: {theistical}, {theistic}]
     n : one who believes in the existence of a god or gods
