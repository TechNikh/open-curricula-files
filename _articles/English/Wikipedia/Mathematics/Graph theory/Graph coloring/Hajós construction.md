---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Haj%C3%B3s_construction'
offline_file: ""
offline_thumbnail: ""
uuid: 0dd36373-3baf-46ff-8b97-374b563d5dd5
updated: 1484309382
title: Hajós construction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hajos_construction.svg.png
tags:
    - The construction
    - Constructible graphs
    - Connection to coloring
    - Constructibility of critical graphs
    - The Hajós number
    - Other applications
    - Notes
categories:
    - Graph coloring
---
In graph theory, a branch of mathematics, the Hajós construction is an operation on graphs named after György Hajós (1961) that may be used to construct any critical graph or any graph whose chromatic number is at least some given threshold.
