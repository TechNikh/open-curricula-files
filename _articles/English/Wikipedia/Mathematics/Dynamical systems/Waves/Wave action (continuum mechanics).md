---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Wave_action_(continuum_mechanics)
offline_file: ""
offline_thumbnail: ""
uuid: 21687c47-720b-4850-8ba9-853a503f4ba3
updated: 1484308905
title: Wave action (continuum mechanics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NOAA_Wavewatch_III_Sample_Forecast.gif
categories:
    - Waves
---
In continuum mechanics, wave action refers to a conservable measure of the wave part of a motion.[2] For small-amplitude and slowly varying waves, the wave action density is:[3]
