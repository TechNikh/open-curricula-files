---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Face_diagonal
offline_file: ""
offline_thumbnail: ""
uuid: 93c249f0-8290-423b-af77-76ec986b86ad
updated: 1484308932
title: Face diagonal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Cube_diagonals.svg_0.png
categories:
    - Elementary geometry
---
In geometry, a face diagonal of a polyhedron is a diagonal on one of the faces, in contrast to a space diagonal passing through the interior of the polyhedron.[1]
