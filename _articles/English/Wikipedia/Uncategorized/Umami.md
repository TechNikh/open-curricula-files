---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Umami
offline_file: ""
offline_thumbnail: ""
uuid: 4769fa8b-430b-4b0a-9964-a7d8cee262fc
updated: 1484308378
title: Umami
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ripe_tomatoes.JPG
tags:
    - Background
    - discovery
    - Properties of umami taste
    - Foods rich in umami components
    - Taste receptors
    - In popular culture
categories:
    - Uncategorized
---
Umami (/uˈmɑːmi/), or savory taste,[1][2][3] is one of the five basic tastes (together with sweetness, sourness, bitterness, and saltiness). It has been described as brothy or meaty.
