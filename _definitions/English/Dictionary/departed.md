---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/departed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484444161
title: departed
categories:
    - Dictionary
---
departed
     adj 1: not present; having left; "he's away right now"; "you must
            not allow a stranger into the house when your mother
            is away"; "everyone is gone now"; "the departed
            guests" [syn: {away(p)}, {gone(p)}, {departed(a)}]
     2: well in the past; former; "bygone days"; "dreams of foregone
        times"; "sweet memories of gone summers"; "relics of a
        departed era" [syn: {bygone}, {bypast}, {foregone}, {gone}]
     3: dead; "he is deceased"; "our dear departed friend" [syn: {asleep(p)},
         {at peace(p)}, {at rest(p)}, {deceased}, {gone}]
     n : someone who is no longer alive; "I wonder what the dead
         person would have ...
