---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purchase
offline_file: ""
offline_thumbnail: ""
uuid: a5ae554b-c56a-4d02-954a-47c295894b79
updated: 1484310240
title: purchase
categories:
    - Dictionary
---
purchase
     n 1: the acquisition of something for payment; "they closed the
          purchase with a handshake"
     2: something acquired by purchase
     3: a means of exerting influence or gaining advantage; "he
        could get no purchase on the situation"
     4: the mechanical advantage gained by being in a position to
        use a lever [syn: {leverage}]
     v : obtain by purchase; acquire by means of a financial
         transaction; "The family purchased a new car"; "The
         conglomerate acquired a new company"; "She buys for the
         big department store" [syn: {buy}] [ant: {sell}]
