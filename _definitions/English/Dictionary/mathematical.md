---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mathematical
offline_file: ""
offline_thumbnail: ""
uuid: 5403f9b1-4e85-4d38-9986-722ca7c0b452
updated: 1484310224
title: mathematical
categories:
    - Dictionary
---
mathematical
     adj 1: of or pertaining to or of the nature of mathematics; "a
            mathematical textbook"; "slide rules and other
            mathematical instruments"; "a mathematical solution to
            a problem"; "mathematical proof"
     2: relating to or having ability to think in or work with
        numbers; "tests for rating numerical aptitude"; "a
        mathematical whiz" [syn: {numerical}] [ant: {verbal}]
     3: beyond question; "a mathematical certainty"
     4: statistically possible though highly improbable; "have a
        mathematical chance of making the playoffs"
     5: characterized by the exactness or precision of mathematics;
        "mathematical ...
