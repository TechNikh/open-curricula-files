---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calf
offline_file: ""
offline_thumbnail: ""
uuid: 3af7fd7b-fd8b-4307-9ba8-a6b32c95d5ee
updated: 1484310283
title: calf
categories:
    - Dictionary
---
calf
     n 1: young of domestic cattle
     2: the muscular back part of the shank [syn: {sura}]
     3: fine leather from the skin of a calf [syn: {calfskin}]
     4: young of various large placental mammals e.g. whale or
        giraffe or elephant or buffalo
     [also: {calves} (pl)]
