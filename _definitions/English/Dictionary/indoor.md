---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indoor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484549641
title: indoor
categories:
    - Dictionary
---
indoor
     adj 1: located, suited for, or taking place within a building;
            "indoor activities for a rainy day"; "an indoor pool"
            [syn: {indoor(a)}, {inside}] [ant: {outdoor(a)}]
     2: within doors; "an indoor setting"
