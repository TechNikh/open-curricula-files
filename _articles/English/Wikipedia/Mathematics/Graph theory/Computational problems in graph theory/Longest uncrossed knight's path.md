---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Longest_uncrossed_knight%27s_path'
offline_file: ""
offline_thumbnail: ""
uuid: 2e637036-49e7-42ee-8859-03650c3d971a
updated: 1484309394
title: "Longest uncrossed knight's path"
tags:
    - Known solutions
    - Generalizations
categories:
    - Computational problems in graph theory
---
The longest uncrossed (or nonintersecting) knight's path is a mathematical problem involving a knight on the standard 8×8 chessboard or, more generally, on a square n×n board. The problem is to find the longest path the knight can take on the given board, such that the path does not intersect itself. A further distinction can be made between a closed path, which ends on the same field as where it begins, and an open path, which ends on a different field from where it begins.
