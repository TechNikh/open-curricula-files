---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terror
offline_file: ""
offline_thumbnail: ""
uuid: e545c55d-bfd7-474e-a887-fad971055983
updated: 1484310174
title: terror
categories:
    - Dictionary
---
terror
     n 1: an overwhelming feeling of fear and anxiety [syn: {panic}]
     2: a person who inspires fear or dread; "he was the terror of
        the neighborhood" [syn: {scourge}, {threat}]
     3: a very troublesome child [syn: {brat}, {little terror}, {holy
        terror}]
