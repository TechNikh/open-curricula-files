---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caterpillar_tree
offline_file: ""
offline_thumbnail: ""
uuid: b1398a04-fb2a-4020-a091-56da837df7da
updated: 1484309431
title: Caterpillar tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Caterpillar_tree.svg.png
tags:
    - Equivalent characterizations
    - Generalizations
    - Enumeration
    - Computational complexity
    - Applications
categories:
    - Mathematical chemistry
---
Caterpillars were first studied in a series of papers by Harary and Schwenk. The name was suggested by A. Hobbs.[1][2] As Harary & Schwenk (1973) colorfully write, "A caterpillar is a tree which metamorphoses into a path when its cocoon of endpoints is removed."[1]
