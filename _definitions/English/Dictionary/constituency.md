---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constituency
offline_file: ""
offline_thumbnail: ""
uuid: 9d94faec-06f1-456f-a18d-08dc1ac77de3
updated: 1484310585
title: constituency
categories:
    - Dictionary
---
constituency
     n : the body of voters who elect a representative for their area
