---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distribution
offline_file: ""
offline_thumbnail: ""
uuid: 543da720-9fd6-435d-9071-093efabedd3c
updated: 1484310357
title: distribution
categories:
    - Dictionary
---
distribution
     n 1: (statistics) an arrangement of values of a variable showing
          their observed or theoretical frequency of occurrence
          [syn: {statistical distribution}]
     2: the spatial property of being scattered about over an area
        or volume [syn: {dispersion}] [ant: {concentration}]
     3: the act of distributing or spreading or apportioning
     4: the commercial activity of transporting and selling goods
        from a producer to a consumer
