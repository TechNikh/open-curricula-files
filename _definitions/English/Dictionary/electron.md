---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electron
offline_file: ""
offline_thumbnail: ""
uuid: ea517a94-9d3a-4093-9e2c-dff072f8304d
updated: 1484310200
title: electron
categories:
    - Dictionary
---
electron
     n : an elementary particle with negative charge [syn: {negatron}]
