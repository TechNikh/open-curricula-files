---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shiny
offline_file: ""
offline_thumbnail: ""
uuid: acae4d82-1d84-4fc9-8d58-4258c08218e1
updated: 1484310373
title: shiny
categories:
    - Dictionary
---
shiny
     adj 1: reflecting light; "glistening bodies of swimmers"; "the
            horse's glossy coat"; "lustrous auburn hair"; "saw the
            moon like a shiny dime on a deep blue velvet carpet";
            "shining white enamel" [syn: {glistening}, {glossy}, {lustrous},
             {sheeny}, {shining}]
     2: having a shiny surface or coating; "glazed fabrics"; "glazed
        doughnuts" [syn: {glazed}] [ant: {unglazed}]
     3: made smooth and bright by or as if by rubbing; reflecting a
        sheen or glow; "bright silver candlesticks"; "a burnished
        brass knocker"; "she brushed her hair until it fell in
        lustrous auburn waves"; "rows of shining glasses"; ...
