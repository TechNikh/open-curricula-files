---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Curtis%E2%80%93Hedlund%E2%80%93Lyndon_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: a2a67974-7634-495e-9740-1dfb71a52bcd
updated: 1484308662
title: Curtis–Hedlund–Lyndon theorem
tags:
    - Definitions
    - Proof
    - Counterexample for infinite alphabets
    - Application to reversible cellular automata
    - Generalization
categories:
    - Theorems in discrete mathematics
---
The Curtis–Hedlund–Lyndon theorem is a mathematical characterization of cellular automata in terms of their symbolic dynamics. It is named after Morton L. Curtis, Gustav A. Hedlund, and Roger Lyndon; in his 1969 paper stating the theorem, Hedlund credited Curtis and Lyndon as co-discoverers.[1] It has been called "one of the fundamental results in symbolic dynamics".[2]
