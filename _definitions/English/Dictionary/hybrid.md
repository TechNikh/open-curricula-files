---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hybrid
offline_file: ""
offline_thumbnail: ""
uuid: bfe5cbcb-63b6-4143-bf47-0d2bedf1aaa3
updated: 1484310297
title: hybrid
categories:
    - Dictionary
---
hybrid
     adj : produced by crossbreeding [syn: {crossed}, {interbred}, {intercrossed}]
     n 1: a word that is composed of parts from different languages
          (e.g., `monolingual' has a Greek prefix and a Latin
          root) [syn: {loanblend}, {loan-blend}]
     2: a composite of mixed origin; "the vice-presidency is a
        hybrid of administrative and legislative offices"
     3: an organism that is the offspring of genetically dissimilar
        parents or stock; especially offspring produced by
        breeding plants or animals of different varieties or
        breeds or species; "a mule is a cross between a horse and
        a donkey" [syn: {crossbreed}, {cross}]
