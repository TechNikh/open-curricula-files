---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curved_space
offline_file: ""
offline_thumbnail: ""
uuid: 3dd2a7e3-efe8-4984-814b-eecaddfdd458
updated: 1484309212
title: Curved space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Pythagorean_triangle.png
tags:
    - Simple two-dimensional example
    - Embedding
    - Without embedding
    - Open, flat, closed
categories:
    - Differential geometry
---
Curved space often refers to a spatial geometry which is not “flat” where a flat space is described by Euclidean geometry. Curved spaces can generally be described by Riemannian geometry though some simple cases can be described in other ways. Curved spaces play an essential role in General Relativity where gravity is often visualized as curved space. The Friedmann-Lemaître-Robertson-Walker metric is a curved metric which forms the current foundation for the description of the expansion of space and shape of the universe.
