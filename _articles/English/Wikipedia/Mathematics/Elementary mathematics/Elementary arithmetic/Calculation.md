---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calculation
offline_file: ""
offline_thumbnail: ""
uuid: 9f747658-e645-4321-91b9-5e4038cb4e18
updated: 1484308918
title: Calculation
tags:
    - Comparison to computation
categories:
    - Elementary arithmetic
---
A calculation is a deliberate process that transforms one or more inputs into one or more results, with variable change. The term is used in a variety of senses, from the very definite arithmetical calculation of using an algorithm, to the vague heuristics of calculating a strategy in a competition, or calculating the chance of a successful relationship between two people.
