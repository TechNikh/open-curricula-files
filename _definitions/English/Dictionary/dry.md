---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dry
offline_file: ""
offline_thumbnail: ""
uuid: bb388748-91e6-484e-99ac-c6a18c3bbcf7
updated: 1484310346
title: dry
categories:
    - Dictionary
---
dry
     adj 1: free from liquid or moisture; lacking natural or normal
            moisture or depleted of water; or no longer wet; "dry
            land"; "dry clothes"; "a dry climate"; "dry splintery
            boards"; "a dry river bed"; "the paint is dry" [ant: {wet}]
     2: humorously sarcastic or mocking; "dry humor"; "an ironic
        remark often conveys an intended meaning obliquely"; "an
        ironic novel"; "an ironical smile"; "with a wry Scottish
        wit" [syn: {ironic}, {ironical}, {wry}]
     3: opposed to or prohibiting the production and sale of
        alcoholic beverages; "the dry vote led by preachers and
        bootleggers"; "a dry state" [ant: {wet}]
     ...
