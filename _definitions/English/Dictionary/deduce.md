---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deduce
offline_file: ""
offline_thumbnail: ""
uuid: a0855fa1-2877-4410-b396-e8c161856544
updated: 1484310220
title: deduce
categories:
    - Dictionary
---
deduce
     v 1: reason by deduction; establish by deduction [syn: {infer}, {deduct},
           {derive}]
     2: conclude by reasoning; in logic [syn: {infer}]
