---
version: 1
type: video
provider: YouTube
id: HEH_oKNLgUU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/The%20derivative%20of%20f%28x%29%3Dx%5E2%20for%20any%20x.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2c4f626e-f1a6-4447-a482-24f378ddb505
updated: 1486069693
title: The derivative of f(x)=x^2 for any x
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/HEH_oKNLgUU/default.jpg
    - https://i3.ytimg.com/vi/HEH_oKNLgUU/1.jpg
    - https://i3.ytimg.com/vi/HEH_oKNLgUU/2.jpg
    - https://i3.ytimg.com/vi/HEH_oKNLgUU/3.jpg
---
