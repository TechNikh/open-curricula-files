---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydrides
offline_file: ""
offline_thumbnail: ""
uuid: 6853a791-4444-4052-9934-3ad74092d6ad
updated: 1484308312
title: Hydride
tags:
    - Bonds
    - Applications
    - Hydride ion
    - Types of hydrides
    - Ionic hydrides
    - Covalent hydrides
    - Interstitial hydrides or metallic hydrides
    - Transition metal hydride complexes
    - Deuterides
    - Appendix on nomenclature
    - Precedence convention
    - Bibliography
categories:
    - Uncategorized
---
In chemistry, a hydride is the anion of hydrogen, H−, or, more commonly, it is a compound in which one or more hydrogen centres have nucleophilic, reducing, or basic properties. In compounds that are regarded as hydrides, the hydrogen atom is bonded to a more electropositive element or group. Compounds containing hydrogen bonded to metals or metalloid may also be referred to as hydrides, even though in this case the hydrogen atoms can have a protic[clarification needed] character. Almost all of the elements form binary compounds with hydrogen, the exceptions being He, Ne, Ar, Kr, Pm, Os, Ir, Rn, Fr, and Ra.[1][2][3][4]
