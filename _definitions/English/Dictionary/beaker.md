---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beaker
offline_file: ""
offline_thumbnail: ""
uuid: baaaafd1-a8e6-4c93-b9c1-41a5dca3f45e
updated: 1484310343
title: beaker
categories:
    - Dictionary
---
beaker
     n 1: a flatbottomed jar made of glass or plastic; used for
          chemistry
     2: a cup (usually without a handle)
