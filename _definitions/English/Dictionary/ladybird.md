---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ladybird
offline_file: ""
offline_thumbnail: ""
uuid: 3ba22aff-ecde-47aa-976c-0dce7a825a44
updated: 1484310266
title: ladybird
categories:
    - Dictionary
---
ladybird
     n : small round bright-colored and spotted beetle that usually
         feeds on aphids and other insect pests [syn: {ladybug}, {ladybeetle},
          {lady beetle}, {ladybird beetle}]
