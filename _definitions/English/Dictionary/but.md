---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/but
offline_file: ""
offline_thumbnail: ""
uuid: f7414783-c292-4312-8729-9c442501eafe
updated: 1484310366
title: but
categories:
    - Dictionary
---
but
     adv : and nothing more; "I was merely asking"; "it is simply a
           matter of time"; "just a scratch"; "he was only a
           child"; "hopes that last but a moment" [syn: {merely},
           {simply}, {just}, {only}]
