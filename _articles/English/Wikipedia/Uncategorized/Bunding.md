---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bunding
offline_file: ""
offline_thumbnail: ""
uuid: b0bd50c5-4013-4870-b86c-2b2dfd29780b
updated: 1484308333
title: Bunding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tanks_in_bund.jpg
tags:
    - Liquid containment
    - Regulations
    - Holding capacity
    - Unwanted water build-up and removal
    - Anti-noise bunds
    - Bunding failures
    - Notable containment failures
categories:
    - Uncategorized
---
Bunding, also called a bund wall, is a constructed retaining wall around storage "where potentially polluting substances are handled, processed or stored, for the purposes of containing any unintended escape of material from that area until such time as remedial action can be taken."[1]
