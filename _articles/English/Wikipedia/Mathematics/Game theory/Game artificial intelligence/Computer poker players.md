---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_poker_players
offline_file: ""
offline_thumbnail: ""
uuid: e3bc8683-fe85-4d11-bf5d-3e9dafef01a8
updated: 1484309002
title: Computer poker players
tags:
    - On the Internet
    - Player bots
    - House enforcement
    - House bots
    - Artificial intelligence
    - Research Groups
    - >
        Computer Poker Research Group (University of Alberta,
        Canada)
    - School of Computer Science from Carnegie Mellon University
    - The University of Auckland Game AI Group
    - Neo Poker Laboratory
    - Historic contests
    - ICCM 2004 PokerBot competition
    - ACM competitions
    - The 2005 World Series of Poker Robots
    - "University of Alberta's Man V Machine experiments"
    - >
        The 2015 Brains vs AI competition by Rivers Casino, CMU and
        Microsoft
    - The Annual Computer Poker Competition
    - Results
categories:
    - Game artificial intelligence
---
Computer poker players are computer programs designed to play the game of poker against human opponents or other computer opponents. They are commonly referred to as pokerbots or just simply bots.
