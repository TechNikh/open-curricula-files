---
version: 1
type: video
provider: YouTube
id: 1_-PYoJKOwM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7fb4386f-2d0c-4821-a54c-9d89d4d7516a
updated: 1486069603
title: అయస్కాంతత్వం 3
tags:
    - telugu
    - Physics
    - magnetism
    - electricity
categories:
    - భౌతిక శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/1_-PYoJKOwM/default.jpg
    - https://i3.ytimg.com/vi/1_-PYoJKOwM/1.jpg
    - https://i3.ytimg.com/vi/1_-PYoJKOwM/2.jpg
    - https://i3.ytimg.com/vi/1_-PYoJKOwM/3.jpg
---
Translated from original video
Title -Magnetism 3 
URL -https://www.khanacademy.org/science/physics/electricity-and-magnetism/v/magnetism-3
