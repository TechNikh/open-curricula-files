---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dwork_family
offline_file: ""
offline_thumbnail: ""
uuid: b6d2504b-ec9a-40d4-aa71-52b377e19992
updated: 1484309142
title: Dwork family
categories:
    - Algebraic geometry
---
In algebraic geometry, a Dwork family is a one-parameter family of hypersurfaces depending on an integer n, studied by Bernard Dwork. Originally considered by Dwork in the context of local zeta-functions, such families have been shown to have relationships with mirror symmetry and extensions of the modularity theorem.[1]
