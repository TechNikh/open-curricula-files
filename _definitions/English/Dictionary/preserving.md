---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preserving
offline_file: ""
offline_thumbnail: ""
uuid: b6447e9e-7012-4e0f-9730-cf0ccb4fcd85
updated: 1484310181
title: preserving
categories:
    - Dictionary
---
preserving
     adj : saving from harm or loss; "serves a conserving function"
           [syn: {conserving}]
