---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Dominance-based_rough_set_approach
offline_file: ""
offline_thumbnail: ""
uuid: 46c3e87f-92c1-4624-8f5b-03ee0ba0710f
updated: 1484308671
title: Dominance-based rough set approach
tags:
    - Multicriteria classification (sorting)
    - Data representation
    - Decision table
    - Outranking relation
    - Decision classes and class unions
    - Main concepts
    - Dominance
    - Rough approximations
    - Quality of approximation and reducts
    - Decision rules
    - Example
    - Extensions
    - Multicriteria choice and ranking problems
    - Variable-consistency DRSA
    - Stochastic DRSA
    - Software
categories:
    - Theoretical computer science
---
Dominance-based rough set approach (DRSA) is an extension of rough set theory for multi-criteria decision analysis (MCDA), introduced by Greco, Matarazzo and Słowiński.[1][2][3] The main change comparing to the classical rough sets is the substitution of the indiscernibility relation by a dominance relation, which permits to deal with inconsistencies typical to consideration of criteria and preference-ordered decision classes.
