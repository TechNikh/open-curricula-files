---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possess
offline_file: ""
offline_thumbnail: ""
uuid: 8b9bc626-f08f-4656-8328-df903518bdc2
updated: 1484310234
title: possess
categories:
    - Dictionary
---
possess
     v 1: have as an attribute, knowledge, or skill; "he possesses
          great knowledge about the Middle East"
     2: have ownership or possession of; "He owns three houses in
        Florida"; "How many cars does she have?" [syn: {own}, {have}]
     3: enter into and control, as of emotions or ideas; "What
        possessed you to buy this house?"; "A terrible rage
        possessed her"
