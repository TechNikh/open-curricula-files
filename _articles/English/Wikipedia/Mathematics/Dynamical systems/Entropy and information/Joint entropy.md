---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Joint_entropy
offline_file: ""
offline_thumbnail: ""
uuid: c6d42439-2bde-47fc-a87a-3a4bca5f2481
updated: 1484308770
title: Joint entropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Entropy-mutual-information-relative-entropy-relation-diagram.svg_0.png
tags:
    - Definition
    - Properties
    - Greater than individual entropies
    - Less than or equal to the sum of individual entropies
    - Relations to other entropy measures
categories:
    - Entropy and information
---
