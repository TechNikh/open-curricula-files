---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pine
offline_file: ""
offline_thumbnail: ""
uuid: 4daa0a62-8bef-4b63-bd38-37fb89d509ac
updated: 1484310409
title: pine
categories:
    - Dictionary
---
pine
     n 1: a coniferous tree [syn: {pine tree}, {true pine}]
     2: straight-grained durable and often resinous white to
        yellowish timber of any of numerous trees of the genus
        Pinus
     v : have a desire for something or someone who is not present;
         "She ached for a cigarette"; "I am pining for my lover"
         [syn: {ache}, {yearn}, {yen}, {languish}]
