---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artyom_Shneyerov
offline_file: ""
offline_thumbnail: ""
uuid: ccfc715a-a914-4a1e-870b-70e578d96e55
updated: 1484309029
title: Artyom Shneyerov
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_9.jpg
categories:
    - Game theorists
---
Artyom Shneyerov is a microeconomist working at Concordia University in Montreal, Canada. He is also an Associate Editor of the International Journal of Industrial Organization.[1] His current research is in the fields of Game theory, Industrial organization and applied Econometrics. His list of contributions to these and other areas of economics includes the following:
