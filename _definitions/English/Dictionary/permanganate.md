---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permanganate
offline_file: ""
offline_thumbnail: ""
uuid: 4c5e2b64-e045-41d4-81a2-9c5866e7baa5
updated: 1484310423
title: permanganate
categories:
    - Dictionary
---
permanganate
     n : a dark purple salt of permanganic acid; in water solution it
         is used as a disinfectant and antiseptic
