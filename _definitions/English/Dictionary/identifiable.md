---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identifiable
offline_file: ""
offline_thumbnail: ""
uuid: ae45c08f-d82b-41e8-8e35-b019fd33c523
updated: 1484310535
title: identifiable
categories:
    - Dictionary
---
identifiable
     adj : possible to identify [ant: {unidentifiable}]
