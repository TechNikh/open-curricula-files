---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/screening
offline_file: ""
offline_thumbnail: ""
uuid: 6ad7cb45-21fe-4e35-8722-54373d149838
updated: 1484310401
title: screening
categories:
    - Dictionary
---
screening
     n 1: the display of a motion picture [syn: {showing}, {viewing}]
     2: fabric of metal or plastic mesh
     3: the act of concealing the existence of something by
        obstructing the view of it; "the cover concealed their
        guns from enemy aircraft" [syn: {cover}, {covering}, {masking}]
     4: testing objects or persons in order to identify those with
        particular characteristics
