---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regular_solution
offline_file: ""
offline_thumbnail: ""
uuid: ea1f8b8c-a7b1-4fe6-9905-acc935281bf2
updated: 1484308855
title: Regular solution
categories:
    - Thermodynamics
---
In chemistry, a regular solution is a solution that diverges from the behavior of an ideal solution only moderately.[1] Its entropy of mixing is equal to that of an ideal solution with the same composition, due to random mixing without strong specific interactions.[2][3] For two components
