---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Periodic_point
offline_file: ""
offline_thumbnail: ""
uuid: c1cae87d-06f2-42a1-aa21-8f7b38eb4d77
updated: 1484308795
title: Periodic point
tags:
    - Iterated functions
    - Examples
    - Dynamical system
    - Properties
    - Examples
categories:
    - Limit sets
---
In mathematics, in the study of iterated functions and dynamical systems, a periodic point of a function is a point which the system returns to after a certain number of function iterations or a certain amount of time.
