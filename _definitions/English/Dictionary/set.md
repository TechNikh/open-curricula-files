---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/set
offline_file: ""
offline_thumbnail: ""
uuid: c8d1deda-3a70-41f9-926a-13eae968869e
updated: 1484310339
title: set
categories:
    - Dictionary
---
set
     adj 1: (usually followed by `to' or `for') on the point of or
            strongly disposed; "in no fit state to continue"; "fit
            to drop"; "laughing fit to burst"; "she was fit to
            scream"; "primed for a fight"; "we are set to go at
            any time" [syn: {fit(p)}, {primed(p)}, {set(p)}]
     2: fixed and unmoving; "with eyes set in a fixed glassy stare";
        "his bearded face already has a set hollow look"- Connor
        Cruise O'Brien; "a face rigid with pain" [syn: {fixed}, {rigid}]
     3: situated in a particular spot or position; "valuable
        centrally located urban land"; "strategically placed
        artillery"; "a house set on a ...
