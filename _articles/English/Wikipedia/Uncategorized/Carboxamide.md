---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carboxamide
offline_file: ""
offline_thumbnail: ""
uuid: e498c153-b990-4fa2-8f59-b7874da56b29
updated: 1484308405
title: Carboxamide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Acetamide-2D-flat.png
categories:
    - Uncategorized
---
In organic chemistry carboxamides (or amino carbonyls) are functional groups with the general structure R-CO-NR'R′′ with R, R', and R′′ as organic substituents, or hydrogen.[1]
