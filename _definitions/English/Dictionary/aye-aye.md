---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aye-aye
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494621
title: aye-aye
categories:
    - Dictionary
---
aye-aye
     n : nocturnal lemur with long bony fingers and rodent-like
         incisor teeth closely related to the lemurs [syn: {Daubentonia
         madagascariensis}]
