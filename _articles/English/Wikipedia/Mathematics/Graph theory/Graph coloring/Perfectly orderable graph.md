---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfectly_orderable_graph
offline_file: ""
offline_thumbnail: ""
uuid: 70bf6871-0e15-404d-8003-7c75b0d2ba38
updated: 1484309387
title: Perfectly orderable graph
tags:
    - Definition
    - Computational complexity
    - Related graph classes
    - Notes
categories:
    - Graph coloring
---
In graph theory, a perfectly orderable graph is a graph whose vertices can be ordered in such a way that a greedy coloring algorithm with that ordering optimally colors every induced subgraph of the given graph. Perfectly orderable graphs form a special case of the perfect graphs, and they include the chordal graphs, comparability graphs, and distance-hereditary graphs. However, testing whether a graph is perfectly orderable is NP-complete.
