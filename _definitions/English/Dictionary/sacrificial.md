---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sacrificial
offline_file: ""
offline_thumbnail: ""
uuid: ee5e8174-7476-44fe-b6c9-b3101b94950c
updated: 1484310411
title: sacrificial
categories:
    - Dictionary
---
sacrificial
     adj : used in or connected with a sacrifice; "sacrificial lamb"
