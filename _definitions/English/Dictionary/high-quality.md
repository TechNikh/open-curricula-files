---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/high-quality
offline_file: ""
offline_thumbnail: ""
uuid: 040bce62-1b18-4ab5-861e-2d6cd0f12462
updated: 1484310244
title: high-quality
categories:
    - Dictionary
---
high quality
     n : the quality of being superior [syn: {superiority}] [ant: {inferiority},
          {inferiority}]
