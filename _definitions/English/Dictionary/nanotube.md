---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nanotube
offline_file: ""
offline_thumbnail: ""
uuid: 14f7313b-e5a0-41ca-bbb7-99620e78a852
updated: 1484310416
title: nanotube
categories:
    - Dictionary
---
nanotube
     n : a fullerene molecule having a cylindirical or toroidal shape
         [syn: {carbon nanotube}]
