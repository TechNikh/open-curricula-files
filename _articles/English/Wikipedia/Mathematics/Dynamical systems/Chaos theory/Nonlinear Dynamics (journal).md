---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonlinear_Dynamics_(journal)
offline_file: ""
offline_thumbnail: ""
uuid: a37edcd7-0432-4efa-b582-add826ce1508
updated: 1484308722
title: Nonlinear Dynamics (journal)
categories:
    - Chaos theory
---
Nonlinear Dynamics, An International Journal of Nonlinear Dynamics and Chaos in Engineering Systems is a monthly peer-reviewed scientific journal covering all nonlinear dynamic phenomena associated with mechanical, structural, civil, aeronautical, ocean, electrical, and control systems. It is published by Springer Science+Business Media and the editor-in-chief of the journal is Ali H. Nayfeh (Virginia Polytechnic Institute).
