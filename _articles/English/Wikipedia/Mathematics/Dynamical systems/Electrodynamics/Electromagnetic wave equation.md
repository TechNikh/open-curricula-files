---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromagnetic_wave_equation
offline_file: ""
offline_thumbnail: ""
uuid: 2bc22127-89df-4930-b61f-307b8a40ab87
updated: 1484308754
title: Electromagnetic wave equation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-Postcard-from-Maxwell-to-Tait.jpg
tags:
    - The origin of the electromagnetic wave equation
    - Covariant form of the homogeneous wave equation
    - Homogeneous wave equation in curved spacetime
    - Inhomogeneous electromagnetic wave equation
    - Solutions to the homogeneous electromagnetic wave equation
    - Monochromatic, sinusoidal steady-state
    - Plane wave solutions
    - Spectral decomposition
    - Multipole expansion
    - Other solutions
    - Theory and experiment
    - Applications
    - Biographies
    - Notes
    - Electromagnetism
    - Journal articles
    - Undergraduate-level textbooks
    - Graduate-level textbooks
    - Vector calculus
categories:
    - Electrodynamics
---
The electromagnetic wave equation is a second-order partial differential equation that describes the propagation of electromagnetic waves through a medium or in a vacuum. It is a three-dimensional form of the wave equation. The homogeneous form of the equation, written in terms of either the electric field E or the magnetic field B, takes the form:
