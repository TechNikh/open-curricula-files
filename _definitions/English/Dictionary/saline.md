---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saline
offline_file: ""
offline_thumbnail: ""
uuid: 89ee1aea-13cb-4db2-957a-f2ae9fc70f30
updated: 1484310258
title: saline
categories:
    - Dictionary
---
saline
     adj : containing salt; "a saline solution"; "salty tears" [syn: {salty}]
     n : an isotonic solution of sodium chloride and distilled water
         [syn: {saline solution}]
