---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broken
offline_file: ""
offline_thumbnail: ""
uuid: 702b1059-4ef4-4d90-b85f-90a9fe272076
updated: 1484310359
title: broken
categories:
    - Dictionary
---
broken
     adj 1: physically and forcibly separated into pieces or cracked or
            split; or legally or emotionally destroyed; "a broken
            mirror"; "a broken tooth"; "a broken leg"; "his neck
            is broken"; "children from broken homes"; "a broken
            marriage"; "a broken heart" [ant: {unbroken}]
     2: not continuous in space, time, or sequence or varying
        abruptly; "broken lines of defense"; "a broken cable
        transmission"; "broken sleep"; "tear off the stub above
        the broken line"; "a broken note"; "broken sobs" [ant: {unbroken}]
     3: subdued or brought low in condition or status; "brought
        low"; "a broken man"; "his broken ...
