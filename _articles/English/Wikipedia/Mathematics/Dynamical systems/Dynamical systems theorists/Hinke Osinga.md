---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hinke_Osinga
offline_file: ""
offline_thumbnail: ""
uuid: 483687a2-f29c-49e3-884e-e0be4a8b0dcf
updated: 1484308690
title: Hinke Osinga
tags:
    - Education and career
    - Mathematical art
    - Awards and honours
categories:
    - Dynamical systems theorists
---
Hinke Maria Osinga (born 25 December 1969, Dokkum)[1] is a Dutch mathematician and an expert in dynamical systems. She works as a professor of applied mathematics at the University of Auckland in New Zealand.[2] As well as for her research, she is known as a creator of mathematical art.
