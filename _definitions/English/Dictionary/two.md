---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/two
offline_file: ""
offline_thumbnail: ""
uuid: 13745dbd-2e31-4205-86a3-538efd184272
updated: 1484310343
title: two
categories:
    - Dictionary
---
two
     adj : being one more than one; "he received two messages" [syn: {2},
            {ii}]
     n : the cardinal number that is the sum of one and one or a
         numeral representing this number [syn: {2}, {II}, {deuce}]
