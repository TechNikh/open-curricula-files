---
version: 1
type: video
provider: YouTube
id: MS7x2hDEhrw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Biodiversity%20flourishes%20in%20Phanerozoic%20eon.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d2f1e2bf-ed7f-44aa-976e-c2dde761c6aa
updated: 1486069685
title: Biodiversity flourishes in Phanerozoic eon
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/MS7x2hDEhrw/default.jpg
    - https://i3.ytimg.com/vi/MS7x2hDEhrw/1.jpg
    - https://i3.ytimg.com/vi/MS7x2hDEhrw/2.jpg
    - https://i3.ytimg.com/vi/MS7x2hDEhrw/3.jpg
---
