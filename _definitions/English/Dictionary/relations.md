---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relations
offline_file: ""
offline_thumbnail: ""
uuid: 3d5e296d-5b5f-40a6-a286-bd6f74afcc4f
updated: 1484310471
title: relations
categories:
    - Dictionary
---
relations
     n : mutual dealings or connections or communications among
         persons or groups [syn: {dealings}]
