---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jammu
offline_file: ""
offline_thumbnail: ""
uuid: 5709c462-d3e3-48df-a19f-7057f4b0640a
updated: 1484308415
title: Jammu
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jammu_Tawi_to_Delhi_-_Rail_side_views_02.JPG
tags:
    - geography
    - Etymology
    - History
    - Climate
    - Transport
    - Administration
    - Economy
    - Tourism
    - Bahu Fort
    - Raghunath Temple
    - Peer Kho Cave
    - Shri Mata Vaishno Devi
    - Demographics
    - Education
    - Refugees and migration
    - Cuisine
categories:
    - Uncategorized
---
Jammu  pronunciation (help·info) is the largest city in the Jammu Division and the winter capital of state of Jammu and Kashmir in India. It is situated on the banks of the Tawi River. It is administered by a municipal corporation.[1]
