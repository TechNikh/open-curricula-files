---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/astonish
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651041
title: astonish
categories:
    - Dictionary
---
astonish
     v : affect with wonder; "Your ability to speak six languages
         amazes me!" [syn: {amaze}, {astound}]
