---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vadim_Kaloshin
offline_file: ""
offline_thumbnail: ""
uuid: ff3b028f-ae10-4fbc-95d0-ed46e6f8e884
updated: 1484308684
title: Vadim Kaloshin
categories:
    - Dynamical systems theorists
---
Vadim Kaloshin is a Russian mathematician, known for his contributions to dynamical systems. He was a student of John N Mather at Princeton University, obtaining a PhD in 2001.[1] He was subsequently a C. L. E. Moore instructor at the Massachusetts Institute of Technology, and a faculty member at the California Institute of Technology and Pennsylvania State University. He is now the Michael Brin Chair at the University of Maryland, College Park and mathematics professor for the University of Maryland College of Computer, Mathematical, and Natural Sciences.[2]
