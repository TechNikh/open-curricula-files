---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affected
offline_file: ""
offline_thumbnail: ""
uuid: 449adb30-fda5-470e-9388-5c3b78758322
updated: 1484310353
title: affected
categories:
    - Dictionary
---
affected
     adj 1: acted upon; influenced [ant: {unaffected}]
     2: speaking or behaving in an artificial way to make an
        impression [syn: {unnatural}] [ant: {unaffected}]
     3: emotionally affected; "very touched by the stranger's
        kindness" [syn: {affected(p)}, {stirred(p)}, {touched(p)}]
