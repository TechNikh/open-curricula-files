---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trapping
offline_file: ""
offline_thumbnail: ""
uuid: 55b80e8d-85f4-4a08-9520-abac76412978
updated: 1484310268
title: trapping
categories:
    - Dictionary
---
trap
     n 1: a device in which something (usually an animal) can be
          caught and penned
     2: drain consisting of a U-shaped section of drainpipe that
        holds liquid and so prevents a return flow of sewer gas
     3: something (often something deceptively attractive) that
        catches you unawares; "the exam was full of trap
        questions"; "it was all a snare and delusion" [syn: {snare}]
     4: a device to hurl clay pigeons into the air for trapshooters
     5: the act of concealing yourself and lying in wait to attack
        by surprise [syn: {ambush}, {ambuscade}, {lying in wait}]
     6: informal terms for the mouth [syn: {cakehole}, {hole}, {maw},
         ...
