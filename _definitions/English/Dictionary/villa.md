---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/villa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508121
title: villa
categories:
    - Dictionary
---
Villa
     n 1: Mexican revolutionary leader (1877-1923) [syn: {Pancho Villa},
           {Francisco Villa}, {Doroteo Arango}]
     2: detached or semi-detached suburban house
     3: country house in ancient Rome consisting of residential
        quarters and farm buildings around a courtyard
     4: pretentious and luxurious country residence with extensive
        grounds
