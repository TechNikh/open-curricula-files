---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dip
offline_file: ""
offline_thumbnail: ""
uuid: 4676ae04-ce6c-45e1-bab5-609f5dd501ac
updated: 1484310204
title: dip
categories:
    - Dictionary
---
dip
     n 1: a depression in an otherwise level surface; "there was a dip
          in the road"
     2: (physics) the angle that a magnetic needle makes with the
        plane of the horizon [syn: {angle of dip}, {magnetic dip},
         {magnetic inclination}, {inclination}]
     3: a thief who steals from the pockets or purses of others in
        public places [syn: {pickpocket}, {cutpurse}]
     4: tasty mixture or liquid into which bite-sized foods are
        dipped
     5: a brief immersion
     6: a sudden sharp decrease in some quantity; "a drop of 57
        points on the Dow Jones index"; "there was a drop in
        pressure in the pulmonary artery"; "a dip in prices";
        ...
