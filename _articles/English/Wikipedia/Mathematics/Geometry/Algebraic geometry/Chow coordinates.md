---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chow_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: ece1ae9e-569f-4357-b3ab-9313cdb3d3e1
updated: 1484309136
title: Chow coordinates
tags:
    - Definition
    - Chow variety
categories:
    - Algebraic geometry
---
In mathematics, and more particularly in the field of algebraic geometry, Chow coordinates are a generalization of Plücker coordinates, applying to m-dimensional algebraic varieties of degree d in Pn, that is, n-dimensional projective space. They are named for W. L. Chow.
