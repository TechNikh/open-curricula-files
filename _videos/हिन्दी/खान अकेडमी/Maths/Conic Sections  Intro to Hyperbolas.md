---
version: 1
type: video
provider: YouTube
id: -AB5gXQWaJE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 785a01c6-d890-443e-98f6-5d3de4069d68
updated: 1486069609
title: 'Conic Sections  Intro to Hyperbolas'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/-AB5gXQWaJE/default.jpg
    - https://i3.ytimg.com/vi/-AB5gXQWaJE/1.jpg
    - https://i3.ytimg.com/vi/-AB5gXQWaJE/2.jpg
    - https://i3.ytimg.com/vi/-AB5gXQWaJE/3.jpg
---
