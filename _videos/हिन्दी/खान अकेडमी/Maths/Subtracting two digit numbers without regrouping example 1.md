---
version: 1
type: video
provider: YouTube
id: r3JYFW5WOw0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 99cce2a3-2365-46e6-bb93-07be3f84b860
updated: 1486069605
title: Subtracting two digit numbers without regrouping example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/r3JYFW5WOw0/default.jpg
    - https://i3.ytimg.com/vi/r3JYFW5WOw0/1.jpg
    - https://i3.ytimg.com/vi/r3JYFW5WOw0/2.jpg
    - https://i3.ytimg.com/vi/r3JYFW5WOw0/3.jpg
---
