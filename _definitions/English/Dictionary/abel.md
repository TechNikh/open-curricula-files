---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abel
offline_file: ""
offline_thumbnail: ""
uuid: 748a6c54-5f89-4be6-8102-5a3647033524
updated: 1484310148
title: abel
categories:
    - Dictionary
---
Abel
     n 1: Norwegian mathematician (1802-1829) [syn: {Niels Abel}, {Niels
          Henrik Abel}]
     2: (Old Testament) Cain and Abel were the first children of
        Adam and Eve born after the Fall of Man; Abel was killed
        by Cain
