---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hearth
offline_file: ""
offline_thumbnail: ""
uuid: eeb17940-3d27-4d08-86b5-21067d8a4f60
updated: 1484310413
title: hearth
categories:
    - Dictionary
---
hearth
     n 1: an open recess in a wall at the base of a chimney where a
          fire can be built; "the fireplace was so large you could
          walk inside it"; "he laid a fire in the hearth and lit
          it"; "the hearth was black with the charcoal of many
          fires" [syn: {fireplace}, {open fireplace}]
     2: home symbolized as a part of the fireplace; "driven from
        hearth and home"; "fighting in defense of their firesides"
        [syn: {fireside}]
     3: an area near a fireplace (usually paved and extending out
        into a room); "they sat on the hearth and warmed
        themselves before the fire" [syn: {fireside}]
