---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ferric
offline_file: ""
offline_thumbnail: ""
uuid: a93e10a7-a31d-4751-a456-de2efa9110f3
updated: 1484310409
title: ferric
categories:
    - Dictionary
---
ferric
     adj : of or relating to or containing iron [syn: {ferrous}]
