---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centerpoint_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 975d7106-5aac-4c9d-b2a8-69b9f8496a5b
updated: 1484309283
title: Centerpoint (geometry)
tags:
    - Related concepts
    - Existence
    - Algorithms
    - Citations
    - Sources
categories:
    - Multi-dimensional geometry
---
In statistics and computational geometry, the notion of centerpoint is a generalization of the median to data in higher-dimensional Euclidean space. Given a set of points in d-dimensional space, a centerpoint of the set is a point such that any hyperplane that goes through that point divides the set of points in two roughly equal subsets: the smaller part should have at least a 1/(d + 1) fraction of the points. Like the median, a centerpoint need not be one of the data points. Every non-empty set of points (with no duplicates) has at least one centerpoint.
