---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enforce
offline_file: ""
offline_thumbnail: ""
uuid: d6fd7043-8c95-4b18-b243-6d510704d30c
updated: 1484310236
title: enforce
categories:
    - Dictionary
---
enforce
     v 1: ensure observance of laws and rules; "Apply the rules to
          everyone"; [syn: {implement}, {apply}] [ant: {exempt}]
     2: compel to behave in a certain way; "Social relations impose
        courtesy" [syn: {impose}]
