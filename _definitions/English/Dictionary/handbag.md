---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handbag
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484512621
title: handbag
categories:
    - Dictionary
---
handbag
     n : a bag used for carrying money and small personal items or
         accessories (especially by women); "she reached into her
         bag and found a comb" [syn: {bag}, {pocketbook}, {purse}]
