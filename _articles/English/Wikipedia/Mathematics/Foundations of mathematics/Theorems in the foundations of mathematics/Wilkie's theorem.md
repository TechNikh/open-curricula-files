---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wilkie%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2dbc1eb1-5191-468b-87d1-1b1538fea6be
updated: 1484309001
title: "Wilkie's theorem"
tags:
    - Formulations
    - "Gabrielov's theorem"
    - Intermediate results
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, Wilkie's theorem is a result by Alex Wilkie about the theory of ordered fields with an exponential function, or equivalently about the geometric nature of exponential varieties.
