---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyloric
offline_file: ""
offline_thumbnail: ""
uuid: f54872e5-664b-4f60-98e2-5bc3fb8b75da
updated: 1484310333
title: pyloric
categories:
    - Dictionary
---
pyloric
     adj : relating to or near the pylorus
