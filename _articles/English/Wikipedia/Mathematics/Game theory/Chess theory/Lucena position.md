---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lucena_position
offline_file: ""
offline_thumbnail: ""
uuid: eedf153e-2be7-4ce8-bd41-eea1df5e6f68
updated: 1484309017
title: Lucena position
tags:
    - Introduction
    - 'The winning method: building a bridge'
    - Black to move
    - Bridge on the fifth rank
    - Alternate plan for the defense
    - Rook pawn
    - Similar positions may be drawn
    - Examples from praxis
    - Conclusion
categories:
    - Chess theory
---
The Lucena position is one of the most famous and important positions in chess endgame theory, where one side has a rook and a pawn and the defender has a rook. It is fundamental in the rook and pawn versus rook endgame. If the side with the pawn can reach this type of position, he can forcibly win the game. Most rook and pawn versus rook endgames reach either the Lucena position or the Philidor position if played accurately (de la Villa 2008:125). The side with the pawn will try to reach the Lucena position to win; the other side will try to reach the Philidor position to draw.
