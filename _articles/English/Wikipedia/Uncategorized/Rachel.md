---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rachel
offline_file: ""
offline_thumbnail: ""
uuid: 011c004f-16b9-4df0-9937-d700f3087fcf
updated: 1484308466
title: Rachel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Giovanni_Battista_Tiepolo_066.jpg
tags:
    - Marriage to Jacob
    - "Rachel's children"
    - Death and burial
    - Family tree
    - Additional references in the Bible
    - In Islam
categories:
    - Uncategorized
---
Rachel (Hebrew: רָחֵל, Modern Rakhél, Tiberian Rāḥēl) (Arabic: راحيل‎‎) was the favorite of Biblical patriarch Jacob's two wives as well as the mother of Joseph and Benjamin, two of the twelve progenitors of the tribes of Israel. The name "Rachel" means ewe.[2][3] Rachel was the daughter of Laban and the younger sister of Leah, Jacob's first wife. Rachel was a niece of Rebekah (Jacob's mother), Laban being Rebekah's brother,[4] making Jacob her first cousin.
