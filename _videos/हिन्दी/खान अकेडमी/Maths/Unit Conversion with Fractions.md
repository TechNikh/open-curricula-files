---
version: 1
type: video
provider: YouTube
id: LpBfML3Ge54
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3251cee3-5c58-4b5c-840a-f2814b0ab52d
updated: 1486069613
title: Unit Conversion with Fractions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/LpBfML3Ge54/default.jpg
    - https://i3.ytimg.com/vi/LpBfML3Ge54/1.jpg
    - https://i3.ytimg.com/vi/LpBfML3Ge54/2.jpg
    - https://i3.ytimg.com/vi/LpBfML3Ge54/3.jpg
---
