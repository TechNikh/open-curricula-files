---
version: 1
type: video
provider: YouTube
id: 3mier94pbnU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Work%20and%20energy/Work%20and%20energy%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ecc87cea-31d8-4233-a3eb-e01d99c460cb
updated: 1486069591
title: Work and energy (part 2)
categories:
    - Work and energy
thumbnail_urls:
    - https://i3.ytimg.com/vi/3mier94pbnU/default.jpg
    - https://i3.ytimg.com/vi/3mier94pbnU/1.jpg
    - https://i3.ytimg.com/vi/3mier94pbnU/2.jpg
    - https://i3.ytimg.com/vi/3mier94pbnU/3.jpg
---
More on work. Introduction to Kinetic and Potential Energies. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/work-and-energy/work-and-energy-tutorial/v/conservation-of-energy?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/work-and-energy/work-and-energy-tutorial/v/introduction-to-work-and-energy?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
