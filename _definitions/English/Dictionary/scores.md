---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scores
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484402041
title: scores
categories:
    - Dictionary
---
scores
     n : a large number or amount; "made lots of new friends"; "she
         amassed a mountain of newspapers" [syn: {tons}, {dozens},
          {heaps}, {lots}, {mountain}, {piles}, {stacks}, {loads},
          {rafts}, {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
