---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Clifford%27s_circle_theorems'
offline_file: ""
offline_thumbnail: ""
uuid: 68fb3516-e917-46bd-a07c-c113f15bb0f3
updated: 1484309111
title: "Clifford's circle theorems"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Clifford_circle_theorems.svg.png
categories:
    - Theorems in geometry
---
In geometry, Clifford's theorems, named after the English geometer William Kingdon Clifford, are a sequence of theorems relating to intersections of circles.
