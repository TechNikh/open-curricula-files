---
version: 1
type: video
provider: YouTube
id: f21t7DRKlg8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Singular%20They/BONUS%20VIDEO.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 22ca0035-29a5-43d0-bf53-fc5510930df7
updated: 1486069599
title: BONUS VIDEO
tags:
    - Education
    - online learning
    - learning
    - lessons
    - grammar
    - pronouns
categories:
    - Singular They
thumbnail_urls:
    - https://i3.ytimg.com/vi/f21t7DRKlg8/default.jpg
    - https://i3.ytimg.com/vi/f21t7DRKlg8/1.jpg
    - https://i3.ytimg.com/vi/f21t7DRKlg8/2.jpg
    - https://i3.ytimg.com/vi/f21t7DRKlg8/3.jpg
---
You may have been hearing a lot about the "singular they" recently. But what is it? How does it work, what is its history, and is it grammatical? Let's find out. 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/intro-to-adjectives-v1?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/emphatic-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
