---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solidarity
offline_file: ""
offline_thumbnail: ""
uuid: e28dc13f-aaf2-4c33-86fa-d9b8a8a3d2e8
updated: 1484310258
title: solidarity
categories:
    - Dictionary
---
solidarity
     n : a union of interests or purposes or sympathies among members
         of a group
