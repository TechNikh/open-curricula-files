---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strictly
offline_file: ""
offline_thumbnail: ""
uuid: 6a0e3b9b-ca8c-4190-8140-69ef028f394c
updated: 1484310397
title: strictly
categories:
    - Dictionary
---
strictly
     adv 1: restricted to something; "we talked strictly business" [syn:
             {purely}]
     2: in a stringent manner; "the laws are stringently enforced";
        "stringently controlled" [syn: {stringently}]
     3: in a rigorous manner; "he had been trained rigorously by the
        monks" [syn: {rigorously}]
