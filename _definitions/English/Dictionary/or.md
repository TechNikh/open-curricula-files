---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/or
offline_file: ""
offline_thumbnail: ""
uuid: be4ae0e2-ea1e-4c65-928e-74916bca639a
updated: 1484310359
title: or
categories:
    - Dictionary
---
OR
     n 1: a state in northwestern United States on the Pacific [syn: {Oregon},
           {Beaver State}]
     2: a room in a hospital equipped for the performance of
        surgical operations; "great care is taken to keep the
        operating rooms aseptic" [syn: {operating room}, {operating
        theater}, {operating theatre}, {surgery}]
