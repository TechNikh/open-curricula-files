---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Thue%E2%80%93Morse_sequence'
offline_file: ""
offline_thumbnail: ""
uuid: de3d076c-4b82-478f-873e-65e5d0c9fb26
updated: 1484308794
title: Thue–Morse sequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Morse-Thue_sequence.gif
tags:
    - Definition
    - Direct definition
    - Recurrence relation
    - L-system
    - Characterization using bitwise negation
    - Infinite product
    - Some properties
    - In combinatorial game theory
    - The Prouhet–Tarry–Escott problem
    - Fractals and Turtle graphics
    - Equitable sequencing
    - History
    - Notes
categories:
    - Fixed points (mathematics)
---
In mathematics, the Thue–Morse sequence, or Prouhet–Thue–Morse sequence, is the binary sequence (an infinite sequence of 0s and 1s) obtained by starting with 0 and successively appending the Boolean complement of the sequence obtained thus far. The first few steps of this procedure yield the strings 0 then 01, 0110, 01101001, 0110100110010110, and so on, which are prefixes of the Thue–Morse sequence. The full sequence begins:
