---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fix
offline_file: ""
offline_thumbnail: ""
uuid: 28ff9d33-b8a6-4835-9b93-f79c79a22682
updated: 1484310221
title: fix
categories:
    - Dictionary
---
fix
     n 1: informal terms for a difficult situation; "he got into a
          terrible fix"; "he made a muddle of his marriage" [syn:
          {hole}, {jam}, {mess}, {muddle}, {pickle}, {kettle of
          fish}]
     2: something craved, especially an intravenous injection of a
        narcotic drug; "she needed a fix of chocolate"
     3: the act of putting something in working order again [syn: {repair},
         {fixing}, {fixture}, {mend}, {mending}, {reparation}]
     4: an exemption granted after influence (e.g., money) is
        brought to bear; "collusion resulted in tax fixes for
        gamblers"
     5: a determination of the location of something; "he got a good
        ...
