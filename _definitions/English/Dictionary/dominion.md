---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominion
offline_file: ""
offline_thumbnail: ""
uuid: 6739c999-2a5e-4c00-b3cc-6933aa8d3250
updated: 1484310583
title: dominion
categories:
    - Dictionary
---
dominion
     n 1: dominance or power through legal authority; "France held
          undisputed dominion over vast areas of Africa"; "the
          rule of Caesar" [syn: {rule}]
     2: a region marked off for administrative or other purposes
        [syn: {district}, {territory}, {territorial dominion}]
     3: one of the self-governing nations in the British
        Commonwealth
