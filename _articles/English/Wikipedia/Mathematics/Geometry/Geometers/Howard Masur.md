---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Howard_Masur
offline_file: ""
offline_thumbnail: ""
uuid: 7c223ae1-8d52-452d-a154-bc158f900618
updated: 1484309093
title: Howard Masur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Masur_howard.jpg
categories:
    - Geometers
---
The Hubbard–Masur theorem is named after Masur and John H. Hubbard.[3] Masur was an invited speaker at the 1994 International Congress of Mathematicians in Zürich.[4] and is a fellow of the American Mathematical Society.[5]
