---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auction_sniping
offline_file: ""
offline_thumbnail: ""
uuid: 5ea0c5c6-6dd5-48a0-aee0-c0267e44c17e
updated: 1484309010
title: Auction sniping
tags:
    - Usage
    - Strategy
    - Avoidance of maximum bid fishing
    - Shill avoidance
    - Objections
    - Banning by auction sites
    - Deterrents
    - CAPTCHAs
    - Final extension
    - Buy It Now
categories:
    - Auctions
---
Auction sniping is the practice, in a timed online auction, of placing a bid likely to exceed the current highest bid (which may be hidden) as late as possible—usually seconds before the end of the auction—giving other bidders no time to outbid the sniper. This can be done manually, by software on the bidder's computer,[1] or by an online sniping service.[2][3] Use of an online service is said to decrease the failure rate of the snipe, because the website has more reliable servers and a faster Internet connection with a less variable delay in sending data (jitter), allowing the bid to be placed closer to the deadline, even arriving within the last second.
