---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contented
offline_file: ""
offline_thumbnail: ""
uuid: 44cb2ddf-91a2-48b5-8f71-aa6f462ac53e
updated: 1484310543
title: contented
categories:
    - Dictionary
---
contented
     adj : satisfied or showing satisfaction with things as they are;
           "a contented smile" [syn: {content}] [ant: {discontented}]
