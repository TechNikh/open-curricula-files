---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/such
offline_file: ""
offline_thumbnail: ""
uuid: 499d7292-8b29-4106-a873-0e2c80623c06
updated: 1484310337
title: such
categories:
    - Dictionary
---
such
     adj 1: of a kind specified or understood; "it's difficult to please
            such people"; "on such a night as this"; "animals such
            as lions and tigers" [syn: {such(a)}, {such as}]
     2: of a degree or quality specified (by the `that' clause);
        "their anxiety was such that they could not sleep" [syn: {such(p)},
         {such that}]
     3: of so extreme a degree or extent; "such weeping"; "so much
        weeping"; "such a help"; "such grief"; "never dreamed of
        such beauty" [syn: {such(a)}, {so much}]
     adv : to so extreme a degree; "he is such a baby"; "Such rich
           people!"
