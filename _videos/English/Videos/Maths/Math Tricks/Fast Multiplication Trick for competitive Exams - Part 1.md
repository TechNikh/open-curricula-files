---
version: 1
type: video
provider: YouTube
id: 3ZUrSqxps8Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d51ef438-9e6c-47f1-bc77-7a8c3d05805a
updated: 1486069621
title: 'Fast Multiplication Trick for competitive Exams - Part 1'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/3ZUrSqxps8Y/default.jpg
    - https://i3.ytimg.com/vi/3ZUrSqxps8Y/1.jpg
    - https://i3.ytimg.com/vi/3ZUrSqxps8Y/2.jpg
    - https://i3.ytimg.com/vi/3ZUrSqxps8Y/3.jpg
---
