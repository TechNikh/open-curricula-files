---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radial_line
offline_file: ""
offline_thumbnail: ""
uuid: 7f0a44d3-8901-41db-8a2c-f687f8e5732d
updated: 1484309167
title: Radial line
categories:
    - Analytic geometry
---
A radial line is a line passing through the center of a circle, cylinder or sphere. The correct "direction" of the radial line is from the radius point to a point on the arc or circle. This applies to its use in surveying.
