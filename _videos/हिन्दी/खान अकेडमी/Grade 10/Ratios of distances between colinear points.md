---
version: 1
type: video
provider: YouTube
id: wPBYpXKH-lg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 078a6c57-03d2-4d80-b8b8-4c6b4d9acc73
updated: 1486069618
title: Ratios of distances between colinear points
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/wPBYpXKH-lg/default.jpg
    - https://i3.ytimg.com/vi/wPBYpXKH-lg/1.jpg
    - https://i3.ytimg.com/vi/wPBYpXKH-lg/2.jpg
    - https://i3.ytimg.com/vi/wPBYpXKH-lg/3.jpg
---
