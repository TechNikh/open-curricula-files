---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrogant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412901
title: arrogant
categories:
    - Dictionary
---
arrogant
     adj : having or showing feelings of unwarranted importance out of
           overbearing pride; "an arrogant official"; "arrogant
           claims"; "chesty as a peacock" [syn: {chesty}, {self-important}]
