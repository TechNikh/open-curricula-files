---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Non-Euclidean_crystallographic_group
offline_file: ""
offline_thumbnail: ""
uuid: 03c6c61e-6c5e-43ed-911e-73caa70e0fab
updated: 1484309315
title: Non-Euclidean crystallographic group
categories:
    - Symmetry
---
In mathematics, a non-Euclidean crystallographic group, NEC group or N.E.C. group is a discrete group of isometries of the hyperbolic plane. These symmetry groups correspond to the wallpaper groups in euclidean geometry. A NEC group which contains only orientation-preserving elements is called a Fuchsian group, and any non-Fuchsian NEC group has an index 2 Fuchsian subgroup of orientation-preserving elements.
