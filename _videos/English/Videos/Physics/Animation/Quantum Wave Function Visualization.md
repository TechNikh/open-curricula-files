---
version: 1
type: video
provider: YouTube
id: KKr91v7yLcM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Quantum%20Wave%20Function%20Visualization.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8240b4ff-b709-498a-b7c8-215b4c61ffef
updated: 1486069657
title: Quantum Wave Function Visualization
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/KKr91v7yLcM/default.jpg
    - https://i3.ytimg.com/vi/KKr91v7yLcM/1.jpg
    - https://i3.ytimg.com/vi/KKr91v7yLcM/2.jpg
    - https://i3.ytimg.com/vi/KKr91v7yLcM/3.jpg
---
