---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rebelling
offline_file: ""
offline_thumbnail: ""
uuid: 3b751f8e-46b3-4b8a-b8c8-76971c4e8ff5
updated: 1484310587
title: rebelling
categories:
    - Dictionary
---
rebelling
     adj : participating in organized resistance to a constituted
           government; "the rebelling confederacy" [syn: {rebel(a)},
            {rebelling(a)}, {rebellious}]
