---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/show
offline_file: ""
offline_thumbnail: ""
uuid: 860f7085-6062-4cb7-ad4b-7d24cca2d9c5
updated: 1484310341
title: show
categories:
    - Dictionary
---
show
     n 1: a public exhibition of entertainment; "a remarkable show of
          skill"
     2: something intended to communicate a particular impression;
        "made a display of strength"; "a show of impatience"; "a
        good show of looking interested" [syn: {display}]
     3: a public exhibition or entertainment; "they wanted to see
        some of the shows on Broadway"
     4: pretending that something is the case in order to make a
        good impression; "they try to keep up appearances"; "that
        ceremony is just for show" [syn: {appearance}]
     v 1: show or demonstrate something to an interested audience;
          "She shows her dogs frequently"; "We will demo ...
