---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Julia_set
offline_file: ""
offline_thumbnail: ""
uuid: 869b4554-c71c-49da-b4d3-63f8893668e0
updated: 1484308732
title: Julia set
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Julia_set_%2528ice%2529.png'
tags:
    - Formal definition
    - Equivalent descriptions of the Julia set
    - Properties of the Julia set and Fatou set
    - Examples
    - Quadratic polynomials
    - Examples of Julia sets
    - Generalizations
    - The potential function and the real iteration number
    - Field lines
    - Plotting the Julia set
    - Using backwards (inverse) iteration (IIM)
    - Using DEM/J
categories:
    - Complex dynamics
---
In the context of complex dynamics, a topic of mathematics, the Julia set and the Fatou set are two complementary sets (Julia 'laces' and Fatou 'dusts') defined from a function. Informally, the Fatou set of the function consists of values with the property that all nearby values behave similarly under repeated iteration of the function, and the Julia set consists of values such that an arbitrarily small perturbation can cause drastic changes in the sequence of iterated function values. Thus the behavior of the function on the Fatou set is 'regular', while on the Julia set its behavior is 'chaotic'.
