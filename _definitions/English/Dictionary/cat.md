---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cat
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587561
title: cat
categories:
    - Dictionary
---
cat
     n 1: feline mammal usually having thick soft fur and being unable
          to roar; domestic cats; wildcats [syn: {true cat}]
     2: an informal term for a youth or man; "a nice guy"; "the
        guy's only doing it for some doll" [syn: {guy}, {hombre},
        {bozo}]
     3: a spiteful woman gossip; "what a cat she is!"
     4: the leaves of the shrub Catha edulis which are chewed like
        tobacco or used to make tea; has the effect of a euphoric
        stimulant; "in Yemen kat is used daily by 85% of adults"
        [syn: {kat}, {khat}, {qat}, {quat}, {Arabian tea}, {African
        tea}]
     5: a whip with nine knotted cords; "British sailors feared the
        cat" ...
