---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mahler_volume
offline_file: ""
offline_thumbnail: ""
uuid: 5e9f773f-b8b3-46e6-a4ee-c082763f1975
updated: 1484309193
title: Mahler volume
tags:
    - Definition
    - Examples
    - Extreme shapes
    - Notes
categories:
    - Convex geometry
---
In convex geometry, the Mahler volume of a centrally symmetric convex body is a dimensionless quantity that is associated with the body and is invariant under linear transformations. It is named after German-English mathematician Kurt Mahler. It is known that the shapes with the largest possible Mahler volume are the spheres and ellipsoids; this is now known as the Blaschke–Santaló inequality. The still-unsolved Mahler conjecture states that the minimum possible Mahler volume is attained by a hypercube.
