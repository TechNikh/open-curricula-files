---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shear_waves
offline_file: ""
offline_thumbnail: ""
uuid: 3de6188b-fcd0-4fff-b556-8faf7324211f
updated: 1484308898
title: Shear waves
tags:
    - Shear Waves in Elastography
    - Modes of Shear Wave Generation
    - Tissue Characterization
categories:
    - Waves
---
Shear waves and compressional waves are the two main modes of propagation of acoustic energy in solids. With shear waves, also called transverse waves, the particles of the medium oscillate at a right angle to the direction of propagation. In compressional waves the oscillations occur in the longitudinal direction or the direction of wave propagation. The wave speeds of these different kinds of waves are governed by two different types of moduli. The compressional wave speed is related to the bulk elasticity modulus of the medium while the shear wave speed is related to the shear elasticity modulus. In soft biological tissues the bulk modulus varies no more than 10% while variation of the ...
