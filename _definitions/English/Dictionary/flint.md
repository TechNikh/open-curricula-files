---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flint
offline_file: ""
offline_thumbnail: ""
uuid: 569f5e75-b43f-4aa3-95b6-86555b9ea840
updated: 1484310232
title: flint
categories:
    - Dictionary
---
flint
     n 1: a hard kind of stone; a form of silica more opaque than
          chalcedony
     2: a river in western Georgia that flows generally south to
        join the Chattahoochee River at the Florida border where
        they form the Apalachicola River [syn: {Flint River}]
     3: a city in southeast central Michigan near Detroit;
        automobile manufacturing
