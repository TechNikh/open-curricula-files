---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Template:Analogous_fixed-point_theorems
offline_file: ""
offline_thumbnail: ""
uuid: b62fca2a-4267-4635-993e-4f1cfe256a51
updated: 1484308775
title: Template:Analogous fixed-point theorems
categories:
    - Fixed points (mathematics)
---
There are several fixed-point theorems which come in three equivalent variants: an algebraic topology variant, a combinatorial variant and a set-covering variant. Each variant can be proved separately using totally different arguments, but each variant can also be reduced to the other variants in its row. Additionally, each result can be reduced to the other result in its column.[1]
