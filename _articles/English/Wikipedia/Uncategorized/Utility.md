---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Usefull
offline_file: ""
offline_thumbnail: ""
uuid: cf6a2783-85a2-4fba-b645-1d1a77768c77
updated: 1484308345
title: Utility
tags:
    - Applications
    - Revealed preference
    - Functions
    - Cardinal
    - Ordinal
    - Preferences
    - Examples
    - Expected
    - von Neumann–Morgenstern
    - As probability of success
    - Indirect
    - money
    - Discussion and criticism
categories:
    - Uncategorized
---
In economics, utility is a measure of preferences over some set of goods (including services: something that satisfies human wants); it represents satisfaction experienced by the consumer of a good. The concept is an important underpinning of rational choice theory in economics and game theory: since one cannot directly measure benefit, satisfaction or happiness from a good or service, economists instead have devised ways of representing and measuring utility in terms of measurable economic choices. Economists have attempted to perfect highly abstract methods of comparing utilities by observing and calculating economic choices; in the simplest sense, economists consider utility to be ...
