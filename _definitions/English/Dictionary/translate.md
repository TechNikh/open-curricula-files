---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/translate
offline_file: ""
offline_thumbnail: ""
uuid: e56c7972-7da0-409d-bb61-0546798ac089
updated: 1484310448
title: translate
categories:
    - Dictionary
---
translate
     v 1: restate (words) from one language into another language; "I
          have to translate when my in-laws from Austria visit the
          U.S."; "Can you interpret the speech of the visiting
          dignitaries?"; "She rendered the French poem into
          English"; "He translates for the U.N." [syn: {interpret},
           {render}]
     2: change from one form or medium into another; "Braque
        translated collage into oil" [syn: {transform}]
     3: make sense of a language; "She understands French"; "Can you
        read Greek?" [syn: {understand}, {read}, {interpret}]
     4: bring to a certain spiritual state
     5: change the position of (figures or ...
