---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isolation
offline_file: ""
offline_thumbnail: ""
uuid: 8b26f6e7-73ed-47af-af40-d0d3be6d9e1c
updated: 1484310363
title: isolation
categories:
    - Dictionary
---
isolation
     n 1: a state of separation between persons or groups
     2: the act of isolating something; setting something apart from
        others [syn: {closing off}]
     3: a feeling of being disliked and alone
     4: preference for seclusion or isolation [syn: {reclusiveness}]
     5: (psychiatry) a defense mechanism in which memory of an
        unacceptable act or impulse is separated from the emotion
        originally associated with it
     6: a country's withdrawal from internal politics; "he opposed a
        policy of American isolation"
