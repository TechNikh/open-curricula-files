---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sind
offline_file: ""
offline_thumbnail: ""
uuid: af66c917-c330-4a9c-8294-6114562fb35c
updated: 1484308435
title: Sindh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mohenjodaro_Sindh.jpeg
tags:
    - Etymology
    - History
    - Prehistoric period
    - Arrival of Islam
    - Soomra dynasty period
    - Samma Dynasty period
    - Mughal era
    - Talpurs
    - British Raj
    - Pakistan Resolution in the Sindh Assembly
    - Independence of Pakistan
    - population
    - Demographics
    - Religions
    - Languages
    - Sindhi language
    - Geography and nature
    - Flora
    - Fauna
    - Climate
    - Major Cities
    - government
    - Sindh province
    - Divisions
    - Districts
    - Economy
    - Education
    - culture
    - Arts and crafts
    - Cultural heritage
    - Places of interest
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Sindh /sɪnd/ (Sindhi: سنڌ‎ ; Urdu: سندھ‎) is one of the four provinces of Pakistan, in the southeast of the country. Historically home to the Sindhi people, it is also locally known as the Mehran.[6][7] It was formerly known as Sind until 1956. Sindh is the third largest province of Pakistan by area, and second largest province by population after Punjab. Sindh is bordered by Balochistan province to the west, and Punjab province to the north. Sindh also borders the Indian states of Gujarat and Rajasthan to the east, and Arabian Sea to the south. Sindh's landscape consists mostly of alluvial plains flanking the Indus River, the Thar desert in the eastern portion of the province ...
