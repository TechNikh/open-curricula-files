---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intrinsic_flat_distance
offline_file: ""
offline_thumbnail: ""
uuid: 2698d68f-8fbb-4228-a4cd-1dca85bcfa49
updated: 1484309268
title: Intrinsic flat distance
tags:
    - Intrinsic flat distance
    - Riemannian setting
    - Integral current spaces
    - Applications
    - Citations
categories:
    - Metric geometry
---
In mathematics, the intrinsic flat distance is a notion for distance between two Riemannian manifolds which is a generalization of Federer and Fleming's flat distance between submanifolds and integral currents lying in Euclidean space.
