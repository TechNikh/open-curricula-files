---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abandoned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484595841
title: abandoned
categories:
    - Dictionary
---
abandoned
     adj 1: no longer inhabited; "weed-grown yard of an abandoned
            farmhouse"
     2: left desolate or empty; "an abandoned child"; "their
        deserted wives and children"; "an abandoned shack";
        "deserted villages" [syn: {deserted}]
     3: free from constraint; "an abandoned sadness born of grief"-
        Liam O'Flaherty
