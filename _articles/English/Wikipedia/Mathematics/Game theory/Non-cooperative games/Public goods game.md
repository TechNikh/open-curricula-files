---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Public_goods_game
offline_file: ""
offline_thumbnail: ""
uuid: 8ba5a8e8-0afe-4c95-b5b5-cdcf4353d78a
updated: 1484309059
title: Public goods game
tags:
    - Results
    - Variants
    - Iterated public goods games
    - Open public goods games (Transparency)
    - Public goods games with punishment and/or reward
    - Asymmetric costs and/or benefits
    - Income variation
    - Framing
    - Multiplication factor
    - Implications
    - Game theory
    - Applications to sociology
categories:
    - Non-cooperative games
---
The public goods game is a standard of experimental economics. In the basic game, subjects secretly choose how many of their private tokens to put into a public pot. The tokens in this pot are multiplied by a factor (greater than one and less than the number of players, N) and this "public good" payoff is evenly divided among players. Each subject also keeps the tokens they do not contribute.
