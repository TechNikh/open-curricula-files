---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scheduled
offline_file: ""
offline_thumbnail: ""
uuid: 1c39704f-7fbc-452d-938b-3cd14c745aa2
updated: 1484310459
title: scheduled
categories:
    - Dictionary
---
scheduled
     adj : planned or scheduled for some certain time or times; "the
           scheduled meeting"; "the scheduled flights had to be
           cancelled because of snow" [ant: {unscheduled}]
