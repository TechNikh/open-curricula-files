---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/highlight
offline_file: ""
offline_thumbnail: ""
uuid: ec6bea5e-f09f-455a-b033-478908e90228
updated: 1484310611
title: highlight
categories:
    - Dictionary
---
highlight
     n 1: the most interesting or memorable part; "the highlight of
          the tour was our visit to the Vatican" [syn: {high spot}]
     2: an area of lightness in a picture [syn: {highlighting}]
     v : move into the foreground to make more visible or prominent;
         "The introduction highlighted the speaker's distinguished
         career in linguistics" [syn: {foreground}, {spotlight}, {play
         up}] [ant: {background}, {background}]
