---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conserving
offline_file: ""
offline_thumbnail: ""
uuid: 4773be6c-89be-4cb8-a833-d10b7ee094fb
updated: 1484310238
title: conserving
categories:
    - Dictionary
---
conserving
     adj : saving from harm or loss; "serves a conserving function"
           [syn: {preserving}]
