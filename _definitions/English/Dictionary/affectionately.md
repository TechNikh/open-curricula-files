---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affectionately
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518741
title: affectionately
categories:
    - Dictionary
---
affectionately
     adv : with affection; "she loved him dearly"; "he treats her
           affectionately" [syn: {dearly}, {dear}]
