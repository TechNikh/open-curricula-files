---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bay
offline_file: ""
offline_thumbnail: ""
uuid: b2b00ea7-8316-4b4f-99ab-32985dd674be
updated: 1484310437
title: bay
categories:
    - Dictionary
---
bay
     adj : (used of animals especially a horse) of a moderate
           reddish-brown color
     n 1: an indentation of a shoreline larger than a cove but smaller
          than a gulf
     2: the sound of a hound on the scent
     3: small Mediterranean evergreen tree with small blackish
        berries and glossy aromatic leaves used for flavoring in
        cooking; also used by ancient Greeks to crown victors
        [syn: {true laurel}, {bay laurel}, {bay tree}, {Laurus
        nobilis}]
     4: a compartment on a ship between decks; often used as a
        hospital; "they put him in the sick bay"
     5: a compartment in an aircraft used for some specific purpose;
        "he ...
