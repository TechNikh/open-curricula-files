---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isolated_system
offline_file: ""
offline_thumbnail: ""
uuid: ff80b7df-7b81-43f7-ab2a-168bb371d113
updated: 1484308846
title: Isolated system
categories:
    - Thermodynamics
---
Though subject internally to its own gravity, an isolated system is usually taken to be outside the reach of external gravitational and other long-range forces.
