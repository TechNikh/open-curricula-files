---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/combined
offline_file: ""
offline_thumbnail: ""
uuid: fb45ccc4-c3c7-4050-aeaa-297e95eb6b64
updated: 1484310200
title: combined
categories:
    - Dictionary
---
combined
     adj 1: involving the joint activity of two or more; "the attack was
            met by the combined strength of two divisions";
            "concerted action"; "the conjunct influence of fire
            and strong dring"; "the conjunctive focus of political
            opposition"; "a cooperative effort"; "a united
            effort"; "joint military activities" [syn: {concerted},
             {conjunct}, {conjunctive}, {cooperative}, {united}]
     2: made or joined or united into one [ant: {uncombined}]
