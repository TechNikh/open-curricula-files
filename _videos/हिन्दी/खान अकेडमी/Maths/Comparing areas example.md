---
version: 1
type: video
provider: YouTube
id: nth08GfBRpQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 15492000-323e-431b-ae21-3fa30ddc6bfc
updated: 1486069607
title: Comparing areas example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nth08GfBRpQ/default.jpg
    - https://i3.ytimg.com/vi/nth08GfBRpQ/1.jpg
    - https://i3.ytimg.com/vi/nth08GfBRpQ/2.jpg
    - https://i3.ytimg.com/vi/nth08GfBRpQ/3.jpg
---
