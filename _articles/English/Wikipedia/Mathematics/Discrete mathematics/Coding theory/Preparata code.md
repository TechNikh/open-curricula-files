---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Preparata_code
offline_file: ""
offline_thumbnail: ""
uuid: 46d0af8b-9714-466e-8031-07da8259b47d
updated: 1484308563
title: Preparata code
categories:
    - Coding theory
---
In coding theory, the Preparata codes form a class of non-linear double-error-correcting codes. They are named after Franco P. Preparata who first described them in 1968.
