---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Karl_Georg_Christian_von_Staudt
offline_file: ""
offline_thumbnail: ""
uuid: a701974c-c3f0-405f-9014-2db2f3ec7554
updated: 1484309100
title: Karl Georg Christian von Staudt
tags:
    - Life and influence
    - Algebra of throws
    - Works
categories:
    - Geometers
---
Karl Georg Christian von Staudt (24 January 1798 – 1 June 1867) was a German mathematician who used synthetic geometry to provide a foundation for arithmetic.
