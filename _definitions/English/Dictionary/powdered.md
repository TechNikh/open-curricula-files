---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/powdered
offline_file: ""
offline_thumbnail: ""
uuid: 18d2e3d1-42aa-4497-b9e4-50a26904e4c6
updated: 1484310409
title: powdered
categories:
    - Dictionary
---
powdered
     adj : consisting of fine particles; "powdered cellulose"; "powdery
           snow"; "pulverized sugar is prepared from granulated
           sugar by grinding" [syn: {powdery}, {pulverized}, {pulverised},
            {small-grained}, {fine-grained}]
