---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/festival
offline_file: ""
offline_thumbnail: ""
uuid: d995d66c-623d-47ab-9153-fbf7135d04d7
updated: 1484310517
title: festival
categories:
    - Dictionary
---
festival
     n 1: a day or period of time set aside for feasting and
          celebration
     2: an organized series of acts and performances (usually in one
        place); "a drama festival" [syn: {fete}]
