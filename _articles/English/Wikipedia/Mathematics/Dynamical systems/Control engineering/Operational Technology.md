---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Operational_Technology
offline_file: ""
offline_thumbnail: ""
uuid: 84efce9e-a172-47e0-a7a2-d4590bc0f247
updated: 1484308739
title: Operational Technology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Operalogo.svg.png
tags:
    - Technology
    - Systems
    - Protocols
    - Security
    - Critical Infrastructure
    - Governance
    - Sectors
    - Description
categories:
    - Control engineering
---
Operational Technology (OT) – the hardware and software dedicated to detecting or causing changes in physical processes through direct monitoring and/or control of physical devices such as valves, pumps, etc.
