---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/economically
offline_file: ""
offline_thumbnail: ""
uuid: 4e8a9044-5906-4609-ab2b-456920806b82
updated: 1484310407
title: economically
categories:
    - Dictionary
---
economically
     adv 1: with respect to economic science; "economically this
            proposal makes no sense"
     2: with respect to the economic system; "economically the
        country is worse off"
