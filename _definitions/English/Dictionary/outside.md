---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outside
offline_file: ""
offline_thumbnail: ""
uuid: 2109c6d4-dc1a-45c9-a738-91d3ae729b1f
updated: 1484310313
title: outside
categories:
    - Dictionary
---
outside
     adj 1: relating to or being on or near the outer side or limit; "an
            outside margin" [ant: {inside}]
     2: coming from the outside; "extraneous light in the camera
        spoiled the photograph"; "relying upon an extraneous
        income"; "disdaining outside pressure groups" [syn: {external},
         {extraneous}]
     3: originating or belonging beyond some bounds:"the outside
        world"; "outside interests"; "an outside job"
     4: located, suited for, or taking place in the open air;
        "outdoor clothes"; "badminton and other outdoor games"; "a
        beautiful outdoor setting for the wedding" [syn: {outdoor(a)},
         {out-of-door}] [ant: ...
