---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mightiest
offline_file: ""
offline_thumbnail: ""
uuid: d3fbea0f-03a4-4051-a9e8-29492c876fd9
updated: 1484310579
title: mightiest
categories:
    - Dictionary
---
mighty
     adj : having or showing great strength or force or intensity;
           "struck a mighty blow"; "the mighty logger Paul
           Bunyan"; "the pen is mightier than the sword"-
           Bulwer-Lytton
     adv : (Southern regional intensive) very; "the baby is mighty
           cute"; "he's mighty tired"; "it is powerful humid";
           "that boy is powerful big now"; "they have a right nice
           place" [syn: {powerful}, {right}]
     [also: {mightiest}, {mightier}]
