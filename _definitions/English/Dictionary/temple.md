---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/temple
offline_file: ""
offline_thumbnail: ""
uuid: df1d453d-cc1e-4463-b04a-2f8ae98d1a84
updated: 1484310188
title: temple
categories:
    - Dictionary
---
temple
     n 1: place of worship consisting of an edifice for the worship of
          a deity
     2: the flat area on either side of the forehead; "the veins in
        his temple throbbed"
     3: an edifice devoted to special or exalted purposes
     4: (Judaism) the place of worship for a Jewish congregation
        [syn: {synagogue}, {tabernacle}]
