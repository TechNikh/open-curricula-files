---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extreme
offline_file: ""
offline_thumbnail: ""
uuid: fa68b07e-dd57-4018-ac30-8ca62c410cca
updated: 1484310462
title: extreme
categories:
    - Dictionary
---
extreme
     adj 1: of the greatest possible degree or extent or intensity;
            "extreme cold"; "extreme caution"; "extreme pleasure";
            "utmost contempt"; "to the utmost degree"; "in the
            uttermost distress" [syn: {utmost(a)}, {uttermost(a)}]
     2: far beyond a norm in quantity or amount or degree; to an
        utmost degree; "an extreme example"; "extreme
        temperatures"; "extreme danger"
     3: beyond a norm in views or actions; "an extreme
        conservative"; "an extreme liberal"; "extreme views on
        integration"; "extreme opinions"
     4: most distant in any direction; "the extreme edge of town"
     n 1: the furthest or highest degree ...
