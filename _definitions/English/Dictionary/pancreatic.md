---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pancreatic
offline_file: ""
offline_thumbnail: ""
uuid: daa609f1-41cf-4766-9362-7281d160f450
updated: 1484310323
title: pancreatic
categories:
    - Dictionary
---
pancreatic
     adj : of or involving the pancreas; "pancreatic cancer"
