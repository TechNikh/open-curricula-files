---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paste
offline_file: ""
offline_thumbnail: ""
uuid: 16d1bca8-89b9-47e6-ae24-d1ec06884235
updated: 1484310389
title: paste
categories:
    - Dictionary
---
paste
     n 1: any mixture of a soft and malleable consistency
     2: an adhesive made from water and flour or starch; used on
        paper and paperboard [syn: {library paste}]
     3: a tasty mixture to be spread on bread or crackers [syn: {spread}]
     v 1: join or attach with or as if with glue; "paste the sign ont
          the wall"; "cut and paste the sentence in the text"
          [syn: {glue}]
     2: hit with the fists; "He pasted his opponent"
     3: cover the surface of; "paste the wall with burlap"
