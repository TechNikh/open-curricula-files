---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Baire_space
offline_file: ""
offline_thumbnail: ""
uuid: fd1c52f4-13ff-45d4-b9f3-f48fbd8ec83b
updated: 1484309501
title: Baire space
tags:
    - Motivation
    - Definition
    - Modern definition
    - Historical definition
    - Examples
    - Baire category theorem
    - Properties
    - Sources
categories:
    - Set theory
---
In mathematics, a Baire space is a topological space such that every intersection of a countable collection of open dense sets in the space is also dense. Complete metric spaces and locally compact Hausdorff spaces are examples of Baire spaces according to the Baire category theorem. The spaces are named in honor of René-Louis Baire who introduced the concept.
