---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stage
offline_file: ""
offline_thumbnail: ""
uuid: b0aef915-46a7-4a9b-99ad-c8d009a7ae04
updated: 1484310299
title: stage
categories:
    - Dictionary
---
stage
     n 1: any distinct time period in a sequence of events; "we are in
          a transitional stage in which many former ideas must be
          revised or rejected" [syn: {phase}]
     2: a specific identifiable position in a continuum or series or
        especially in a process; "a remarkable degree of
        frankness"; "at what stage are the social sciences?" [syn:
         {degree}, {level}, {point}]
     3: a large platform on which people can stand and can be seen
        by an audience; "he clambered up onto the stage and got
        the actors to help him into the box"
     4: the theater as a profession (usually `the stage'); "an early
        movie simply showed a long ...
