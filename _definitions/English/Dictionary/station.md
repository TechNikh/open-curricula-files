---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/station
offline_file: ""
offline_thumbnail: ""
uuid: 3e237ace-5a55-46e0-97d1-b16ccbb44173
updated: 1484310234
title: station
categories:
    - Dictionary
---
station
     n 1: a facility equipped with special equipment and personnel for
          a particular purpose; "he started looking for a gas
          station"; "the train pulled into the station"
     2: proper or designated social situation; "he overstepped his
        place"; "the responsibilities of a man in his station";
        "married above her station" [syn: {place}]
     3: (nautical) the location to which a ship or fleet is assigned
        for duty
     4: the position where someone (as a guard or sentry) stands or
        is assigned to stand; "a soldier manned the entrance
        post"; "a sentry station" [syn: {post}]
     v : assign to a station [syn: {post}, {base}, ...
