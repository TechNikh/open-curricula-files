---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Droz-Farny_line_theorem
offline_file: ""
offline_thumbnail: ""
uuid: fcc1fef7-c5eb-4ae5-a43c-6937e7d935f1
updated: 1484309116
title: Droz-Farny line theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Droz-Farny_line.svg.png
categories:
    - Theorems in geometry
---
Let 
  
    
      
        T
      
    
    {\displaystyle T}
  
 be a triangle with vertices 
  
    
      
        A
      
    
    {\displaystyle A}
  
, 
  
    
      
        B
      
    
    {\displaystyle B}
  
, and 
  
    
      
        C
      
    
    {\displaystyle C}
  
, and let 
  
    
      
        H
      
    
    {\displaystyle H}
  
 be its orthocenter (the common point of its three altitude lines. Let 
  
    
      
        
          L
          
            1
          
        
      
    
    {\displaystyle L_{1}}
  
 and 
  
    
      
        
          L
          
            2
          
        
      
    
    {\displaystyle L_{2}}
  
 be any two ...
