---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exceptionally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484417821
title: exceptionally
categories:
    - Dictionary
---
exceptionally
     adv : to an exceptional degree; "it worked exceptionally well"
