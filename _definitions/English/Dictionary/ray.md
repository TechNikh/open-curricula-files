---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ray
offline_file: ""
offline_thumbnail: ""
uuid: b91fa517-4cd6-4a14-846c-c0e1c9f5c789
updated: 1484310221
title: ray
categories:
    - Dictionary
---
ray
     n 1: a column of light (as from a beacon) [syn: {beam}, {beam of
          light}, {light beam}, {ray of light}, {shaft}, {shaft of
          light}, {irradiation}]
     2: a branch of an umbel or an umbelliform inflorescence
     3: (mathematics) a straight line extending from a point
     4: a group of nearly parallel lines of electromagnetic
        radiation [syn: {beam}, {electron beam}]
     5: the syllable naming the second (supertonic) note of any
        major scale in solmization [syn: {re}]
     6: any of the stiff bony rods in the fin of a fish
     7: cartilaginous fishes having horizontally flattened bodies
        and enlarged winglike pectoral fins with gills on ...
