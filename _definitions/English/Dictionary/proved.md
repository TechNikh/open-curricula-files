---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proved
offline_file: ""
offline_thumbnail: ""
uuid: 3674941f-a861-4a32-995d-9a6647d63ed6
updated: 1484310290
title: proved
categories:
    - Dictionary
---
proved
     adj : established beyond doubt; "a proven liar"; "a Soviet leader
           of proven shrewdness" [syn: {proven}] [ant: {unproved}]
