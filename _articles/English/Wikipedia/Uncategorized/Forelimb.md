---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forelimb
offline_file: ""
offline_thumbnail: ""
uuid: 15002138-0739-4eb8-bb5a-b5eb075e60bf
updated: 1484308345
title: Forelimb
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Handskelett_MK1888.png
categories:
    - Uncategorized
---
A forelimb is an anterior limb (arm, leg, or similar appendage) on a terrestrial vertebrate's body. For quadrupeds, the term foreleg is often used instead. (A forearm however is the part of the arm or forelimb between the elbow and the wrist.)
