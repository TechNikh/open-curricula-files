---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idle
offline_file: ""
offline_thumbnail: ""
uuid: f85b8ed1-7d06-47e4-89f0-37c0587b7608
updated: 1484310453
title: idle
categories:
    - Dictionary
---
idle
     adj 1: not in action or at work; "an idle laborer"; "idle
            drifters"; "the idle rich"; "an idle mind" [ant: {busy}]
     2: without a basis in reason or fact; "baseless gossip"; "the
        allegations proved groundless"; "idle fears"; "unfounded
        suspicions"; "unwarranted jealousy" [syn: {baseless}, {groundless},
         {unfounded}, {unwarranted}]
     3: not in active use; "the machinery sat idle during the
        strike"; "idle hands" [syn: {unused}]
     4: silly or trivial; "idle pleasure"; "light banter"; "light
        idle chatter" [syn: {light}]
     5: lacking a sense of restraint or responsibility; "idle talk";
        "a loose tongue" [syn: ...
