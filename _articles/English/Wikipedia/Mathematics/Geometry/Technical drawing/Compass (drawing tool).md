---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Compass_(drawing_tool)
offline_file: ""
offline_thumbnail: ""
uuid: bad46493-922d-4459-80c4-2d72c54041f4
updated: 1484309324
title: Compass (drawing tool)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zirkel.jpg
tags:
    - Construction and parts
    - Handle
    - Legs
    - Hinge
    - Needle point
    - Pencil lead
    - Adjusting nut
    - Uses
    - Compasses and straightedge
    - Variants
    - As a symbol
categories:
    - Technical drawing
---
A pair of compasses, also known simply as a compass, is a technical drawing instrument that can be used for inscribing circles or arcs. As dividers, they can also be used as tools to measure distances, in particular on maps. Compasses can be used for mathematics, drafting, navigation, and other purposes.
