---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/est
offline_file: ""
offline_thumbnail: ""
uuid: 5088fe7a-d32f-4843-a804-bfa18cd2de15
updated: 1484310475
title: est
categories:
    - Dictionary
---
EST
     n : standard time in the 5th time zone west of Greenwich,
         reckoned at the 75th meridian; used in the eastern United
         States [syn: {Eastern Time}, {Eastern Standard Time}]
