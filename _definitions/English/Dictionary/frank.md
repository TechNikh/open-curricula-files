---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frank
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484419081
title: frank
categories:
    - Dictionary
---
frank
     adj 1: characterized by directness in manner or speech; without
            subtlety or evasion; "blunt talking and straight
            shooting"; "a blunt New England farmer"; "I gave them
            my candid opinion"; "forthright criticism"; "a
            forthright approach to the problem"; "tell me what you
            think--and you may just as well be frank"; "it is
            possible to be outspoken without being rude";
            "plainspoken and to the point"; "a point-blank
            accusation" [syn: {blunt}, {candid}, {forthright}, {free-spoken},
             {outspoken}, {plainspoken}, {point-blank}, {straight-from-the-shoulder}]
     2: clearly manifest; ...
