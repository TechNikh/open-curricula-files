---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/climate
offline_file: ""
offline_thumbnail: ""
uuid: ec107365-952c-4464-97cc-6783539a82bf
updated: 1484310381
title: climate
categories:
    - Dictionary
---
climate
     n 1: the weather in some location averaged over some long period
          of time; "the dank climate of southern Wales"; "plants
          from a cold clime travel best in winter" [syn: {clime}]
     2: the prevailing psychological state; "the climate of
        opinion"; "the national mood had changed radically since
        the last election" [syn: {mood}]
