---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moral
offline_file: ""
offline_thumbnail: ""
uuid: f2d55ebd-7044-4ec9-85e7-23fb65ecd0ea
updated: 1484310172
title: moral
categories:
    - Dictionary
---
moral
     adj 1: relating to principles of right and wrong; i.e. to morals or
            ethics; "moral philosophy"
     2: concerned with principles of right and wrong or conforming
        to standards of behavior and character based on those
        principles; "moral sense"; "a moral scrutiny"; "a moral
        lesson"; "a moral quandary"; "moral convictions"; "a moral
        life" [ant: {immoral}, {amoral}]
     3: adhering to ethical and moral principles; "it seems ethical
        and right"; "followed the only honorable course of
        action"; "had the moral courage to stand alone" [syn: {ethical},
         {honorable}, {honourable}]
     4: arising from the sense of right and ...
