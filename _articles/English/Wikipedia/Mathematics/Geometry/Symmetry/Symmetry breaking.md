---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_breaking
offline_file: ""
offline_thumbnail: ""
uuid: 4af2d110-43a6-49ec-ae0d-b616c0d7a13b
updated: 1484309317
title: Symmetry breaking
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Spontaneous_symmetry_breaking_from_an_instable_equilibrium.svg.png
tags:
    - Explicit symmetry breaking
    - Spontaneous symmetry breaking
    - Examples
categories:
    - Symmetry
---
In physics, symmetry breaking is a phenomenon in which (infinitesimally) small fluctuations acting on a system crossing a critical point decide the system's fate, by determining which branch of a bifurcation is taken. To an outside observer unaware of the fluctuations (or "noise"), the choice will appear arbitrary. This process is called symmetry "breaking", because such transitions usually bring the system from a symmetric but disorderly state into one or more definite states. Symmetry breaking is supposed to[clarification needed] play a major role in pattern formation.
