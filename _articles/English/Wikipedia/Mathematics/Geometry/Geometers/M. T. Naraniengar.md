---
version: 1
type: article
id: https://en.wikipedia.org/wiki/M._T._Naraniengar
offline_file: ""
offline_thumbnail: ""
uuid: 0dec295c-7e42-40f5-a31e-a6bc4368ff25
updated: 1484309093
title: M. T. Naraniengar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-M._T._Naraniengar.jpg
categories:
    - Geometers
---
Mandyam Tondanur Naraniengar (1871–1940)[1] was an Indian mathematician. He first proved in 1909 the Morley's trisector theorem after it was posed in 1899 by Frank Morley.[2]
