---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stochastic_game
offline_file: ""
offline_thumbnail: ""
uuid: 18a2bb51-b072-428e-b9f8-6c6adcd7322e
updated: 1484309040
title: Stochastic game
tags:
    - Two-player games
    - Theory
    - Applications
    - Notes
categories:
    - Game theory game classes
---
In game theory, a stochastic game, introduced by Lloyd Shapley in the early 1950s, is a dynamic game with probabilistic transitions played by one or more players. The game is played in a sequence of stages. At the beginning of each stage the game is in some state. The players select actions and each player receives a payoff that depends on the current state and the chosen actions. The game then moves to a new random state whose distribution depends on the previous state and the actions chosen by the players. The procedure is repeated at the new state and play continues for a finite or infinite number of stages. The total payoff to a player is often taken to be the discounted sum of the ...
