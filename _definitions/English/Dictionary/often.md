---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/often
offline_file: ""
offline_thumbnail: ""
uuid: 4424d9d8-4905-47f5-a8e9-36f4b548e7c8
updated: 1484310355
title: often
categories:
    - Dictionary
---
often
     adv 1: many times at short intervals; "we often met over a cup of
            coffee" [syn: {frequently}, {oftentimes}, {oft}, {ofttimes}]
            [ant: {rarely}, {infrequently}]
     2: frequently or in great quantities; "I don't drink much"; "I
        don't travel much" [syn: {much}, {a great deal}]
