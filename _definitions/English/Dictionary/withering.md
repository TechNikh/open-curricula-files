---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/withering
offline_file: ""
offline_thumbnail: ""
uuid: 1606265f-3efc-4228-b8d4-39e7c603b613
updated: 1484310172
title: withering
categories:
    - Dictionary
---
withering
     adj 1: wreaking or capable of wreaking complete destruction;
            "possessing annihilative power"; "a devastating
            hurricane"; "the guns opened a withering fire" [syn: {annihilative},
             {annihilating}, {devastating}]
     2: making light of; "afire with annihilating invective"; "a
        devastating portrait of human folly"; "to compliments
        inflated I've a withering reply"- W.S.Gilbert [syn: {annihilating},
         {devastating}]
     n : any weakening or degeneration (especially through lack of
         use) [syn: {atrophy}]
