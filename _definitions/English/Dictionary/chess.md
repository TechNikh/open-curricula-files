---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chess
offline_file: ""
offline_thumbnail: ""
uuid: 4245f54a-8eab-4689-bc51-97ace615ea92
updated: 1484310146
title: chess
categories:
    - Dictionary
---
chess
     n 1: weedy annual native to Europe but widely distributed as a
          weed especially in wheat [syn: {cheat}, {Bromus
          secalinus}]
     2: a game for two players who move their 16 pieces according to
        specific rules; the object is to checkmate the opponent's
        king [syn: {chess game}]
