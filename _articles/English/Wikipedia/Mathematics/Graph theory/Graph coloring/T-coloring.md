---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-coloring
offline_file: ""
offline_thumbnail: ""
uuid: d276a1e1-d3b8-42a4-8f8a-eaca10035927
updated: 1484309386
title: T-coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-T-colorings.png
tags:
    - T-chromatic number
    - Proof
    - T-span
categories:
    - Graph coloring
---
In graph theory, a T-Coloring of a graph 
  
    
      
        G
        =
        (
        V
        ,
        E
        )
      
    
    {\displaystyle G=(V,E)}
  
, given the set T of nonnegative integers containing 0, is a function 
  
    
      
        c
        :
        V
        (
        G
        )
        →
        
          N
        
      
    
    {\displaystyle c:V(G)\rightarrow \mathbb {N} }
  
 that maps each vertex of G to a positive integer (color) such that 
  
    
      
        (
        u
        ,
        w
        )
        ∈
        E
        (
        G
        )
        ⇒
        
          |
          c
          (
          u
          )
         ...
