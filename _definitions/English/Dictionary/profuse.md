---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profuse
offline_file: ""
offline_thumbnail: ""
uuid: 1ac9555a-1278-4667-8cc1-46ce4401478c
updated: 1484310313
title: profuse
categories:
    - Dictionary
---
profuse
     adj : produced or growing in extreme abundance; "their riotous
           blooming" [syn: {exuberant}, {lush}, {luxuriant}, {riotous}]
