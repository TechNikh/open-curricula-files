---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lightly
offline_file: ""
offline_thumbnail: ""
uuid: c817d84e-d41b-40e1-8ec6-6bdace2b269a
updated: 1484310146
title: lightly
categories:
    - Dictionary
---
lightly
     adv 1: without good reason; "one cannot say such things lightly"
     2: with few burdens; "experienced travellers travel light"
        [syn: {light}]
     3: with little weight or force; "she kissed him lightly on the
        forehead" [syn: {softly}, {gently}]
     4: indulging with temperance; "we eat lightly in the morning"
        [ant: {heavily}]
     5: with indifference or without dejection; "he took it lightly"
     6: in a small quantity or extent; "spread the margarine thinly
        over the meat"; "apply paint lightly" [syn: {thinly}]
        [ant: {thickly}]
     7: to a slight degree; "her speech is only lightly accented"
