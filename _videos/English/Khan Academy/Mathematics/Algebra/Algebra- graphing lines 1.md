---
version: 1
type: video
provider: YouTube
id: 2UrcUfBizyw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20graphing%20lines%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d268d1cd-15b3-4186-b0d5-df9b9695d55f
updated: 1486069571
title: 'Algebra- graphing lines 1'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/2UrcUfBizyw/default.jpg
    - https://i3.ytimg.com/vi/2UrcUfBizyw/1.jpg
    - https://i3.ytimg.com/vi/2UrcUfBizyw/2.jpg
    - https://i3.ytimg.com/vi/2UrcUfBizyw/3.jpg
---
Graphing linear equations
More free lessons at: http://www.khanacademy.org/video?v=2UrcUfBizyw

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/e/graph-from-slope-intercept-equation?utm_source=YTdescription&utm_medium=YTdescription&utm_campaign=YTdescription
