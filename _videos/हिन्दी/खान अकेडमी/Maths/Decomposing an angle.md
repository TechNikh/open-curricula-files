---
version: 1
type: video
provider: YouTube
id: EHsH0jFt9Aw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 561278d8-10a5-4b30-9a89-e9274aaecffb
updated: 1486069616
title: Decomposing an angle
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/EHsH0jFt9Aw/default.jpg
    - https://i3.ytimg.com/vi/EHsH0jFt9Aw/1.jpg
    - https://i3.ytimg.com/vi/EHsH0jFt9Aw/2.jpg
    - https://i3.ytimg.com/vi/EHsH0jFt9Aw/3.jpg
---
