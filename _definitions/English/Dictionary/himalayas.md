---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/himalayas
offline_file: ""
offline_thumbnail: ""
uuid: 30a114b8-4f9e-47f3-aa5c-e10d06dfb4ae
updated: 1484310433
title: himalayas
categories:
    - Dictionary
---
Himalayas
     n : a mountain range extending 1500 miles on the border between
         India and Tibet; this range contains the world's highest
         mountain [syn: {Himalaya Mountains}, {the Himalaya}]
