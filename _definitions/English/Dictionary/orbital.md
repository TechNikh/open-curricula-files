---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orbital
offline_file: ""
offline_thumbnail: ""
uuid: 550cac29-d441-4643-bc44-c589c622177f
updated: 1484310393
title: orbital
categories:
    - Dictionary
---
orbital
     adj 1: of or relating to an orbit; "orbital revolution"; "orbital
            velocity"
     2: of or relating to the eye socket; "orbital scale"; "orbital
        arch"
