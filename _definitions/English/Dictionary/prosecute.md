---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prosecute
offline_file: ""
offline_thumbnail: ""
uuid: be10a6ab-8210-4238-937b-35becba19a15
updated: 1484310170
title: prosecute
categories:
    - Dictionary
---
prosecute
     v 1: conduct a prosecution in a court of law
     2: bring a criminal action against (in a trial); "The State of
        California prosecuted O.J. Simpson" [ant: {defend}]
     3: carry out or participate in an activity; be involved in;
        "She pursued many activities"; "They engaged in a
        discussion" [syn: {engage}, {pursue}]
