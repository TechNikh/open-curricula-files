---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484531281
title: accent
categories:
    - Dictionary
---
accent
     n 1: distinctive manner of oral expression; "he couldn't suppress
          his contemptuous accent"; "she had a very clear speech
          pattern" [syn: {speech pattern}]
     2: special importance or significance; "the red light gave the
        central figure increased emphasis"; "the room was
        decorated in shades of gray with distinctive red accents"
        [syn: {emphasis}]
     3: the usage or vocabulary that is characteristic of a specific
        group of people; "the immigrants spoke an odd dialect of
        English"; "he has a strong German accent" [syn: {dialect},
         {idiom}]
     4: the relative prominence of a syllable or musical note
        ...
