---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hilbert_scheme
offline_file: ""
offline_thumbnail: ""
uuid: dfc79bda-0859-4144-b35f-00abca7f0def
updated: 1484309153
title: Hilbert scheme
tags:
    - Hilbert scheme of projective space
    - Construction
    - Variations
    - Properties
    - Hilbert scheme of points on a manifold
    - Hilbert schemes and hyperkähler geometry
categories:
    - Algebraic geometry
---
In algebraic geometry, a branch of mathematics, a Hilbert scheme is a scheme that is the parameter space for the closed subschemes of some projective space (or a more general projective scheme), refining the Chow variety. The Hilbert scheme is a disjoint union of projective subschemes corresponding to Hilbert polynomials. The basic theory of Hilbert schemes was developed by (Alexander Grothendieck 1961). Hironaka's example shows that non-projective varieties need not have Hilbert schemes.
