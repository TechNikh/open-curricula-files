---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/box
offline_file: ""
offline_thumbnail: ""
uuid: 8aa38d41-833a-47cb-9603-5e6d20d8a47d
updated: 1484310232
title: box
categories:
    - Dictionary
---
box
     n 1: a (usually rectangular) container; may have a lid; "he
          rummaged through a box of spare parts"
     2: private area in a theater or grandstand where a small group
        can watch the performance; "the royal box was empty" [syn:
         {loge}]
     3: the quantity contained in a box; "he gave her a box of
        chocolates" [syn: {boxful}]
     4: a predicament from which a skillful or graceful escape is
        impossible; "his lying got him into a tight corner" [syn:
        {corner}]
     5: a rectangular drawing; "the flowchart contained many boxes"
     6: evergreen shrubs or small trees [syn: {boxwood}]
     7: any one of several designated areas on a ball ...
