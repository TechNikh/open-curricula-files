---
version: 1
type: video
provider: YouTube
id: 2439OIVBgPg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Angles%20and%20intersecting%20lines/Introduction%20to%20angles%20%28old%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e2e1dec3-eb4f-4f25-a1d2-8f8cb924dd46
updated: 1486069577
title: Introduction to angles (old)
categories:
    - Angles and intersecting lines
thumbnail_urls:
    - https://i3.ytimg.com/vi/2439OIVBgPg/default.jpg
    - https://i3.ytimg.com/vi/2439OIVBgPg/1.jpg
    - https://i3.ytimg.com/vi/2439OIVBgPg/2.jpg
    - https://i3.ytimg.com/vi/2439OIVBgPg/3.jpg
---
What an angle is. Angles in a circle. Complementary and supplementary angles.

Watch the next lesson: https://www.khanacademy.org/math/geometry/parallel-and-perpendicular-lines/old_angles/v/angles-part-2?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/parallel-and-perpendicular-lines/triang_prop_tut/v/sum-of-the-exterior-angles-of-convex-polygon?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
