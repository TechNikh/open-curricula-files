---
version: 1
type: video
provider: YouTube
id: 3BmT8n61fsY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d55cdd4c-c1e8-45f9-91f5-b87e2cdfccb2
updated: 1486069636
title: Schematic example of a PALM experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/3BmT8n61fsY/default.jpg
    - https://i3.ytimg.com/vi/3BmT8n61fsY/1.jpg
    - https://i3.ytimg.com/vi/3BmT8n61fsY/2.jpg
    - https://i3.ytimg.com/vi/3BmT8n61fsY/3.jpg
---
