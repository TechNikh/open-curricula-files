---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realization
offline_file: ""
offline_thumbnail: ""
uuid: 2f2bfb93-d3db-4704-989d-b9c294dd5f16
updated: 1484310249
title: realization
categories:
    - Dictionary
---
realization
     n 1: coming to understand something clearly and distinctly; "a
          growing realization of the risk involved"; "a sudden
          recognition of the problem he faced"; "increasing
          recognition that diabetes frequently coexists with other
          chronic diseases" [syn: {realisation}, {recognition}]
     2: making real or giving the appearance of reality [syn: {realisation},
         {actualization}, {actualisation}]
     3: a musical composition that has been completed or enriched by
        someone other than the composer [syn: {realisation}]
     4: a sale in order to obtain money (as a sale of stock or a
        sale of the estate of a bankrupt person) ...
