---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/committee
offline_file: ""
offline_thumbnail: ""
uuid: 477fb577-1639-4dc9-bda0-1505508915d9
updated: 1484310431
title: committee
categories:
    - Dictionary
---
committee
     n 1: a special group delegated to consider some matter; "a
          committee is a group that keeps minutes and loses hours"
          - Milton Berle [syn: {commission}]
     2: a self-constituted organization to promote something [syn: {citizens
        committee}]
