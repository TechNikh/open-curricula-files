---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clever
offline_file: ""
offline_thumbnail: ""
uuid: afb7e066-2702-40cf-af8c-b5dde18dc683
updated: 1484310154
title: clever
categories:
    - Dictionary
---
clever
     adj 1: skillful (or showing skill) in adapting means to ends; "cool
            prudence and sensitive selfishness along with quick
            perception of what is possible--these distinguish an
            adroit politician"; "came up with a clever story"; "an
            ingenious press agent"; "an ingenious scheme" [syn: {adroit},
             {ingenious}]
     2: showing self-interest and shrewdness in dealing with others;
        "a cagey lawyer"; "too clever to be sound" [syn: {cagey},
        {cagy}, {canny}]
     3: mentally quick and resourceful; "an apt pupil"; "you are a
        clever man...you reason well and your wit is bold"-Bram
        Stoker [syn: {apt}]
     ...
