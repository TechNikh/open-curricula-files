---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Origen
offline_file: ""
offline_thumbnail: ""
uuid: 6f8db7de-54f7-43c2-b36e-1e532e5fa10d
updated: 1484308345
title: Origen
tags:
    - Etymology
    - life
    - Early years
    - Asceticism and castration
    - Travels
    - Conflict with Demetrius and removal to Caesarea
    - Works
    - Exegetical writings
    - Extant commentaries of Origen
    - Dogmatic, practical, and apologetic writings
    - Views
    - Philosophical and religious
    - Theological and dogmatic
    - The Logos doctrine and cosmology
    - Christology
    - Eschatology
    - Character
    - "Origen's influence on the later church"
    - Anathemas (544, 553)
    - Today
    - Sources
    - Translations
categories:
    - Uncategorized
---
Origen (/ˈɒrɪdʒən/; Greek: Ὠριγένης, Ōrigénēs), or Origen Adamantius (Ὠριγένης Ἀδαμάντιος, Ōrigénēs Adamántios; 184/185 – 253/254),[1] was a scholar, ascetic,[2] and early Christian theologian who was born and spent the first half of his career in Alexandria. He was a prolific writer in multiple branches of theology, including textual criticism, biblical exegesis and hermeneutics, philosophical theology, preaching, and spirituality written in Greek. He was anathematised at the Second Council of Constantinople. He was one of the most influential figures in early Christian asceticism.[2][3]
