---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicative_partition
offline_file: ""
offline_thumbnail: ""
uuid: 32994bb0-f511-465c-b171-5d5ae5de83c4
updated: 1484308657
title: Multiplicative partition
tags:
    - Examples
    - Application
    - Bounds on the number of partitions
    - Additional results
categories:
    - Number theory
---
In number theory, a multiplicative partition or unordered factorization of an integer n that is greater than 1 is a way of writing n as a product of integers greater than 1, treating two products as equivalent if they differ only in the ordering of the factors. The number n is itself considered one of these products. Multiplicative partitions closely parallel the study of multipartite partitions, discussed in Andrews (1976), which are additive partitions of finite sequences of positive integers, with the addition made pointwise. Although the study of multiplicative partitions has been ongoing since at least 1923, the name "multiplicative partition" appears to have been introduced by Hughes ...
