---
version: 1
type: video
provider: YouTube
id: LqjR58G-0WQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 85ffdee0-eac2-472b-b11f-4282024751eb
updated: 1486069639
title: 'Experiment- Image formed by plane mirror 01'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/LqjR58G-0WQ/default.jpg
    - https://i3.ytimg.com/vi/LqjR58G-0WQ/1.jpg
    - https://i3.ytimg.com/vi/LqjR58G-0WQ/2.jpg
    - https://i3.ytimg.com/vi/LqjR58G-0WQ/3.jpg
---
