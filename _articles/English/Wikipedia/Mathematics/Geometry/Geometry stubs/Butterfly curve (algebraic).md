---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butterfly_curve_(algebraic)
offline_file: ""
offline_thumbnail: ""
uuid: 17998d95-96b7-4518-ac2b-088d5448febf
updated: 1484309356
title: Butterfly curve (algebraic)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Butterfly_curve.png
categories:
    - Geometry stubs
---
The butterfly curve has a single singularity with delta invariant three, which means it is a curve of genus seven. The only plane curves of genus seven are singular, since seven is not a triangular number, and the minimum degree for such a curve is six, so the butterfly curve aside from its appearance is possibly interesting as an example.
