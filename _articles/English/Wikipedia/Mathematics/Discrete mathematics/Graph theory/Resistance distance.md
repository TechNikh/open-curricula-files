---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resistance_distance
offline_file: ""
offline_thumbnail: ""
uuid: c10c0048-4eb2-451d-9afd-834a97c1c401
updated: 1484308632
title: Resistance distance
tags:
    - Definition
    - Properties of resistance distance
    - General sum rule
    - Relationship to the number of spanning trees of a graph
    - As a squared Euclidean distance
    - Connection with Fibonacci numbers
categories:
    - Graph theory
---
In graph theory, the resistance distance between two vertices of a simple connected graph, G, is equal to the resistance between two equivalent points on an electrical network, constructed so as to correspond to G, with each edge being replaced by a 1 ohm resistance. It is a metric on graphs.
