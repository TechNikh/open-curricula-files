---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Shortest_common_supersequence_problem
offline_file: ""
offline_thumbnail: ""
uuid: 78b13e46-fc08-403a-ba1b-7146eca406ae
updated: 1484308580
title: Shortest common supersequence problem
categories:
    - Combinatorics
---
In computer science, the shortest common supersequence problem is a problem closely related to the longest common subsequence problem. Given two sequences X = < x1,...,xm > and Y = < y1,...,yn >, a sequence U = < u1,...,uk > is a common supersequence of X and Y if U is a supersequence of both X and Y. In other words, a shortest common supersequence of strings x and y is a shortest string z such that both x and y are subsequences of z.
