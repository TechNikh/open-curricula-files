---
version: 1
type: video
provider: YouTube
id: FRcRTDJxF1E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b0ecef3a-c3f0-4836-95d6-d73fe7e71300
updated: 1486069618
title: Multiplying Fractions and Mixed Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/FRcRTDJxF1E/default.jpg
    - https://i3.ytimg.com/vi/FRcRTDJxF1E/1.jpg
    - https://i3.ytimg.com/vi/FRcRTDJxF1E/2.jpg
    - https://i3.ytimg.com/vi/FRcRTDJxF1E/3.jpg
---
