---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prior
offline_file: ""
offline_thumbnail: ""
uuid: aad8d5a2-0bd4-4b6e-941b-eaed7eaee1c3
updated: 1484310409
title: prior
categories:
    - Dictionary
---
prior
     adj : earlier in time [syn: {anterior}, {prior(a)}]
     n : the head of a religious order; in an abbey the prior is next
         below the abbot
