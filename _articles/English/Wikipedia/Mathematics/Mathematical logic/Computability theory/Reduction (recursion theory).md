---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reduction_(recursion_theory)
offline_file: ""
offline_thumbnail: ""
uuid: b03e526c-1254-4a19-bdc7-adb2e625bc67
updated: 1484309490
title: Reduction (recursion theory)
tags:
    - Reducibility relations
    - Degrees of a reducibility relation
    - Turing reducibility
    - Reductions stronger than Turing reducibility
    - Bounded reducibilities
    - Strong reductions in computational complexity
    - Reductions weaker than Turing reducibility
categories:
    - Computability theory
---
In computability theory, many reducibility relations (also called reductions, reducibilities, and notions of reducibility) are studied. They are motivated by the question: given sets A and B of natural numbers, is it possible to effectively convert a method for deciding membership in B into a method for deciding membership in A? If the answer to this question is affirmative then A is said to be reducible to B.
