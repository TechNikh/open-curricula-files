---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compliments
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484502121
title: compliments
categories:
    - Dictionary
---
compliments
     n : (usually plural) a polite expression of desire for someone's
         welfare; "give him my kind regards"; "my best wishes"
         [syn: {regard}, {wish}]
