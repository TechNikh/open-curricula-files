---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bang
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484562901
title: bang
categories:
    - Dictionary
---
bang
     n 1: a vigorous blow; "the sudden knock floored him"; "he took a
          bash right in his face"; "he got a bang on the head"
          [syn: {knock}, {bash}, {smash}, {belt}]
     2: a sudden very loud noise [syn: {clap}, {eruption}, {blast},
        {loud noise}]
     3: a fringe of banged hair (cut short squarely across the
        forehead)
     4: the swift release of a store of affective force; "they got a
        great bang out of it"; "what a boot!"; "he got a quick
        rush from injecting heroin"; "he does it for kicks" [syn:
        {boot}, {charge}, {rush}, {flush}, {thrill}, {kick}]
     5: a conspicuous success; "that song was his first hit and
        marked ...
