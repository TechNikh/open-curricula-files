---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clock_angle_problem
offline_file: ""
offline_thumbnail: ""
uuid: f9ba2097-2a22-49b8-b658-c4f0686143ee
updated: 1484308927
title: Clock angle problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ClockAngles.jpg
tags:
    - Math problem
    - Equation for the angle of the hour hand
    - Equation for the angle of the minute hand
    - Example
    - Equation for the angle between the hands
    - Example 1
    - Example 2
    - When are the hour and minute hands of a clock superimposed?
categories:
    - Elementary geometry
---
