---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cake
offline_file: ""
offline_thumbnail: ""
uuid: fe6b7c09-e775-4af9-a446-4471eff8c55f
updated: 1484310387
title: cake
categories:
    - Dictionary
---
cake
     n 1: a block of solid substance (such as soap or wax); "a bar of
          chocolate" [syn: {bar}]
     2: small flat mass of chopped food [syn: {patty}]
     3: made from or based on a mixture of flour and sugar and eggs
     v : form a coat over; "Dirt had coated her face" [syn: {coat}]
