---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/state
offline_file: ""
offline_thumbnail: ""
uuid: 8aa12c8d-f1fe-4f04-9718-17f32e4e3429
updated: 1484310323
title: state
categories:
    - Dictionary
---
state
     n 1: the group of people comprising the government of a sovereign
          state; "the state has lowered its income tax"
     2: the territory occupied by one of the constituent
        administrative districts of a nation; "his state is in the
        deep south" [syn: {province}]
     3: a politically organized body of people under a single
        government; "the state has elected a new president";
        "African nations"; "students who had come to the nation's
        capitol"; "the country's largest manufacturer"; "an
        industrialized land" [syn: {nation}, {country}, {land}, {commonwealth},
         {res publica}, {body politic}]
     4: the way something is with ...
