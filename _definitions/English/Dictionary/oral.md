---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oral
offline_file: ""
offline_thumbnail: ""
uuid: 07ceaba1-678a-4df5-9b4d-c3d34e2e96c0
updated: 1484310343
title: oral
categories:
    - Dictionary
---
oral
     adj 1: using speech rather than writing; "an oral tradition"; "an
            oral agreement" [syn: {unwritten}]
     2: of or relating to or affecting or for use in the mouth;
        "oral hygiene"; "an oral thermometer"; "an oral vaccine"
     3: of or involving the mouth or mouth region or the surface on
        which the mouth is located; "the oral cavity"; "the oral
        mucous membrane"; "the oral surface of a starfish" [ant: {aboral}]
     4: a stage in psychosexual development when the child's
        interest is concentrated in the mouth; fixation at this
        stage is said to result in dependence, selfishness, and
        aggression [ant: {anal}]
     n : an ...
