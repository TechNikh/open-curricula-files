---
version: 1
type: video
provider: YouTube
id: Saf9fYXHnyA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Electric%20Field%20Plotting%20Lab%20Process.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c17e14fb-893d-4e73-932a-bcaf32f7c63e
updated: 1486069652
title: Electric Field Plotting Lab Process
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Saf9fYXHnyA/default.jpg
    - https://i3.ytimg.com/vi/Saf9fYXHnyA/1.jpg
    - https://i3.ytimg.com/vi/Saf9fYXHnyA/2.jpg
    - https://i3.ytimg.com/vi/Saf9fYXHnyA/3.jpg
---
