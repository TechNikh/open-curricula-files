---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/astronomy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484374081
title: astronomy
categories:
    - Dictionary
---
astronomy
     n : the branch of physics that studies celestial bodies and the
         universe as a whole [syn: {uranology}]
