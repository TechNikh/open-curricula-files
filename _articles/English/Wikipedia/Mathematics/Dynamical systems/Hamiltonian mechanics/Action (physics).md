---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Action_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 12bd1cf2-3e91-4d4f-bf26-994847c9d480
updated: 1484308791
title: Action (physics)
tags:
    - Introduction
    - Solution of differential equation
    - Minimization of action integral
    - History
    - Mathematical definition
    - Action in classical physics (disambiguation)
    - Action (functional)
    - Abbreviated action (functional)
    - "Hamilton's principal function"
    - "Hamilton's characteristic function"
    - Other solutions of Hamilton–Jacobi equations
    - Action of a generalized coordinate
    - Action for a Hamiltonian flow
    - Euler–Lagrange equations for the action integral
    - 'Example: free particle in polar coordinates'
    - The action principle
    - Classical fields
    - Conservation laws
    - Quantum mechanics and quantum field theory
    - Single relativistic particle
    - Modern extensions
    - Sources and further reading
categories:
    - Hamiltonian mechanics
---
In physics, action is an attribute of the dynamics of a physical system from which the equations of motion of the system can be derived. It is a mathematical functional which takes the trajectory, also called path or history, of the system as its argument and has a real number as its result. Generally, the action takes different values for different paths.[1] Action has the dimensions of [energy]·[time] or [momentum]·[length], and its SI unit is joule-second.
