---
version: 1
type: video
provider: YouTube
id: sU-zz1wU_9g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fecbfe4c-4b4c-4bb9-ac04-0dbb358f1135
updated: 1486069622
title: >
    EASY Method Multiply Two Numbers Ending In 5 (Fast Math
    Trick)
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/sU-zz1wU_9g/default.jpg
    - https://i3.ytimg.com/vi/sU-zz1wU_9g/1.jpg
    - https://i3.ytimg.com/vi/sU-zz1wU_9g/2.jpg
    - https://i3.ytimg.com/vi/sU-zz1wU_9g/3.jpg
---
