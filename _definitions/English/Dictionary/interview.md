---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interview
offline_file: ""
offline_thumbnail: ""
uuid: 327c0942-a8e4-403a-8ea9-1183fc152af8
updated: 1484310236
title: interview
categories:
    - Dictionary
---
interview
     n 1: the questioning of a person (or a conversation in which
          information is elicited); often conducted by
          journalists; "my interviews with teen-agers revealed a
          weakening of religious bonds"
     2: a conference (usually with someone important); "he had a
        consultation with the judge"; "he requested an audience
        with the king" [syn: {consultation}, {audience}]
     v 1: conduct an interview in television, newspaper, and radio
          reporting [syn: {question}]
     2: discuss formally with (somebody) for the purpose of an
        evaluation; "We interviewed the job candidates"
     3: go for an interview in the hope of being ...
