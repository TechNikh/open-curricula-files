---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flight_envelope_protection
offline_file: ""
offline_thumbnail: ""
uuid: 15bbbcf2-ce56-4848-9849-4152eff0de21
updated: 1484308739
title: Flight envelope protection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/327px-Damaged_empennage_of_China_Airlines_Flight_006-N4522V.JPG
tags:
    - Function
    - Airbus and Boeing
    - Incidents
    - China Airlines Flight 006
    - FedEx Flight 705
    - American Airlines Flight 587
    - US Airways Flight 1549
    - Notes
categories:
    - Control engineering
---
Flight envelope protection is a human machine interface extension of an aircraft’s control system that prevents the pilot of an aircraft from making control commands that would force the aircraft to exceed its structural and aerodynamic operating limits.[1][2][3] It is used in some form in all modern commercial fly-by-wire aircraft.[4] Its advantage is that it restricts pilots in emergency situations so they can react quickly without endangering the safety of their aircraft.[5][6]
