---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Derived_stack
offline_file: ""
offline_thumbnail: ""
uuid: 57da0c6b-97d1-4e93-9905-607d76c87012
updated: 1484309142
title: Derived stack
categories:
    - Algebraic geometry
---
In algebraic geometry, a derived stack is, roughly, a stack together with a sheaf of commutative ring spectra.[1] It generalizes a derived scheme. Derived stacks are the "spaces" studied in derived algebraic geometry.[2]
