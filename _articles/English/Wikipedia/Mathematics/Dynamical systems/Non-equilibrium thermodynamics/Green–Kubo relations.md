---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Green%E2%80%93Kubo_relations'
offline_file: ""
offline_thumbnail: ""
uuid: ddc6a704-f8a0-4127-b5bd-efe8f72e7bde
updated: 1484308808
title: Green–Kubo relations
tags:
    - Thermal and mechanical transport processes
    - Linear constitutive relation
    - Nonlinear response and transient time correlation functions
    - 'Derivation from the fluctuation theorem and the central limit theorem [clarifications needed]'
    - Summary
categories:
    - Non-equilibrium thermodynamics
---
The Green–Kubo relations (Melville S. Green 1954, Ryogo Kubo 1957) give the exact mathematical expression for transport coefficients 
  
    
      
        γ
      
    
    {\displaystyle \gamma }
  
 in terms of integrals of time correlation functions:
