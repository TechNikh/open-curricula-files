---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yair_Minsky
offline_file: ""
offline_thumbnail: ""
uuid: f560385f-9245-4385-ab9b-1ee84c10200a
updated: 1484309090
title: Yair Minsky
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Minsky_yair.jpg
tags:
    - Biography
    - Honors and awards
    - Selected invited talks
    - Selected publications
    - Quotes
categories:
    - Geometers
---
Yair Nathan Minsky (born in 1962) is an American mathematician whose research concerns three-dimensional topology, differential geometry, group theory and holomorphic dynamics. He is a professor at Yale University.[1] He is known for having proved Thurston's ending lamination conjecture and as a pioneer in the study of curve complex geometry.
