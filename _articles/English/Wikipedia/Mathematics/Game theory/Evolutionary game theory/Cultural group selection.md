---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cultural_group_selection
offline_file: ""
offline_thumbnail: ""
uuid: 88ccb46a-962e-4758-99df-8b0332ef4f50
updated: 1484308527
title: Cultural group selection
tags:
    - Human adaptations for culture
    - Joint attention
    - Imitative learning
    - Linguistic symbols and cognitive representation
    - Mechanisms that maintain between-group variation
    - Conformist transmission
    - Prestige-biased and self-similarity transmission
    - Punishment of non-conformists
    - Normative conformity
    - Mechanisms for cultural group selection
    - Demographic swamping
    - Direct Intergroup competition
    - Prestige-biased group selection
categories:
    - Evolutionary game theory
---
Cultural group selection is an explanatory model within cultural evolution of how cultural traits evolve according to the competitive advantage they bestow upon a group. This multidisciplinary approach to the question of human culture engages research from the fields of anthropology, behavioural economics, evolutionary biology, evolutionary game theory, sociology, and psychology.
