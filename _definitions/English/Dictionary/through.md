---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/through
offline_file: ""
offline_thumbnail: ""
uuid: baef5393-38ef-4591-9983-c767721d5ed2
updated: 1484310335
title: through
categories:
    - Dictionary
---
through
     adj 1: having finished or arrived at completion; "certain to make
            history before he's done"; "it's a done deed"; "after
            the treatment, the patient is through except for
            follow-up"; "almost through with his studies" [syn: {done},
             {through with(p)}]
     2: of a route or journey etc.; continuing without requiring
        stops or changes; "a through street"; "a through bus";
        "through traffic" [syn: {through(a)}]
     adv 1: from one end or side to the other; "jealousy pierced her
            through"
     2: from beginning to end; "read this book through"
     3: over the whole distance; "this bus goes through to New York"
 ...
