---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deposition
offline_file: ""
offline_thumbnail: ""
uuid: 14edff64-7cd8-46e2-89bf-9c0e92090b01
updated: 1484310433
title: deposition
categories:
    - Dictionary
---
deposition
     n 1: the natural process of laying down a deposit of something
          [syn: {deposit}]
     2: (law) a pretrial interrogation of a witness; usually done in
        a lawyer's office
     3: the act of putting something somewhere [syn: {deposit}]
     4: the act of deposing someone; removing a powerful person from
        a position or office [syn: {dethronement}]
