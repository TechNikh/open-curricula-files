---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_capacity
offline_file: ""
offline_thumbnail: ""
uuid: 7502ef38-2a9d-45c4-9df5-df358e71d798
updated: 1484308675
title: Quantum capacity
categories:
    - Theoretical computer science
---
In the theory of quantum communication, the quantum capacity is the highest rate at which quantum information can be communicated over many independent uses of a noisy quantum channel from a sender to a receiver. It is also equal to the highest rate at which entanglement can be generated over the channel, and forward classical communication cannot improve it. The quantum capacity theorem is important for the theory of quantum error correction, and more broadly for the theory of quantum computation. The theorem giving a lower bound on the quantum capacity of any channel is colloquially known as the LSD theorem, after the authors Lloyd,[1] Shor,[2] and Devetak[3] who proved it with increasing ...
