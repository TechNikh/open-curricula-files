---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethene
offline_file: ""
offline_thumbnail: ""
uuid: e8e03c91-b99b-45f8-89a2-96216fc77082
updated: 1484310413
title: ethene
categories:
    - Dictionary
---
ethene
     n : a flammable colorless gaseous alkene; obtained from
         petroleum and natural gas and used in manufacturing many
         other chemicals; sometimes used as an anesthetic [syn: {ethylene}]
