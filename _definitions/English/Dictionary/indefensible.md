---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indefensible
offline_file: ""
offline_thumbnail: ""
uuid: c3990e41-921e-4f78-8903-4b162d4a3ec1
updated: 1484310579
title: indefensible
categories:
    - Dictionary
---
indefensible
     adj 1: (of theories etc) incapable of being defended or justified
            [syn: {untenable}]
     2: incapable of being justified or explained [syn: {insupportable},
         {unjustifiable}, {unwarrantable}, {unwarranted}]
