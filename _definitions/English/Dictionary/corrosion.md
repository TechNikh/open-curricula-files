---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corrosion
offline_file: ""
offline_thumbnail: ""
uuid: a55a399c-003d-4ed1-8947-ec3f7d0c6d9f
updated: 1484310375
title: corrosion
categories:
    - Dictionary
---
corrosion
     n 1: a state of deterioration in metals caused by oxidation or
          chemical action
     2: erosion by chemical action [syn: {corroding}, {erosion}]
