---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exuberant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414461
title: exuberant
categories:
    - Dictionary
---
exuberant
     adj 1: joyously unrestrained [syn: {ebullient}, {high-spirited}]
     2: unrestrained in especially feelings; "extravagant praise";
        "exuberant compliments"; "overweening ambition";
        "overweening greed" [syn: {excessive}, {extravagant}, {overweening}]
     3: produced or growing in extreme abundance; "their riotous
        blooming" [syn: {lush}, {luxuriant}, {profuse}, {riotous}]
