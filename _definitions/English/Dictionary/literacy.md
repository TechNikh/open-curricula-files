---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/literacy
offline_file: ""
offline_thumbnail: ""
uuid: 205bbbd6-3939-46f6-8e37-c9950f500a64
updated: 1484310258
title: literacy
categories:
    - Dictionary
---
literacy
     n : the ability to read and write [ant: {illiteracy}]
