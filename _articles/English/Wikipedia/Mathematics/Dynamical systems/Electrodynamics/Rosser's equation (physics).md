---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rosser%27s_equation_(physics)'
offline_file: ""
offline_thumbnail: ""
uuid: 00e9e8a8-4c86-45c3-b3ac-f4bdeafcfbe9
updated: 1484308757
title: "Rosser's equation (physics)"
categories:
    - Electrodynamics
---
In physics, Rosser's equation aids in understanding the role of displacement current in Maxwell's equations, given that there is no ether in empty space as initially assumed by Maxwell. Due originally to William G.V. Rosser,[1] the equation was labeled by Selvan:[2]
