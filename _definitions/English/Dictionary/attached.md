---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attached
offline_file: ""
offline_thumbnail: ""
uuid: bc4a1f93-f6cd-4253-b146-c48c95d9a5dc
updated: 1484310315
title: attached
categories:
    - Dictionary
---
attached
     adj 1: fastened together; "a picnic table with attached benches"
     2: being joined in close association; "affiliated clubs"; "all
        art schools whether independent or attached to
        universities" [syn: {affiliated}, {connected}]
     3: used of buildings joined by common sidewalls; "a block of
        attached houses" [ant: {detached}]
     4: permanently attached to a substrate; not free to move about;
        "an attached oyster"; "sessile marine animals and plants"
        [syn: {sessile}] [ant: {vagile}]
     5: associated in an exclusive sexual relationship [syn: {committed}]
        [ant: {unattached}]
