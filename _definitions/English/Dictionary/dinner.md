---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dinner
offline_file: ""
offline_thumbnail: ""
uuid: 6f8aa43a-26d8-409f-b4dc-801314cfec1f
updated: 1484310311
title: dinner
categories:
    - Dictionary
---
dinner
     n 1: the main meal of the day served in the evening or at midday;
          "dinner will be at 8"; "on Sundays they had a large
          dinner when they returned from church"
     2: a party of people assembled to have dinner together; "guests
        should never be late to a dinner party" [syn: {dinner
        party}]
