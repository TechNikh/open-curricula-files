---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eric
offline_file: ""
offline_thumbnail: ""
uuid: 4320878c-e838-428b-a9c4-56be33d173b5
updated: 1484308473
title: Eric
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Eric_little_by_little_title_page.jpg
tags:
    - people
    - Danish royalty
    - Norwegian royalty
    - Swedish royalty
    - Other people
    - Fictional characters
    - People with the surname
categories:
    - Uncategorized
---
The given name Eric, Erik, or Erick is derived from the Old Norse name Eiríkr (or Æinrikr in Eastern Scandinavia due to monophthongization). The first element, ei- is derived either from the older Proto-Norse *aina(z) meaning "one" or "alone"[1] or from Proto-Norse *aiwa(z) meaning "ever" or "eternal".[2] The second element -ríkr derives either from *rík(a)z meaning "ruler" or "prince" (cf. Gothic reiks) or from an even older Proto-Germanic *ríkiaz which meant "powerful" and "rich".[3] The name is thus usually taken to mean "one ruler", "autocrat", "eternal ruler" or "ever powerful", "warrior", and "government".[4]
