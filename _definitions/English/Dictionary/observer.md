---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observer
offline_file: ""
offline_thumbnail: ""
uuid: abb18eee-2802-4a58-8091-ece27f48a5f4
updated: 1484310220
title: observer
categories:
    - Dictionary
---
observer
     n 1: a person who becomes aware (of things or events) through the
          senses [syn: {perceiver}, {beholder}]
     2: an expert who observes and comments on something [syn: {commentator}]
