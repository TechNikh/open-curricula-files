---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_formula
offline_file: ""
offline_thumbnail: ""
uuid: 6ad23be4-b4f5-43ec-94ef-91508c7aafd0
updated: 1484308913
title: Quadratic formula
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Quadratic_formula.svg_0.png
tags:
    - Derivation of the formula
    - Geometrical significance
    - Historical development
    - Other derivations
    - Alternate method of completing the square
    - By substitution
    - By using algebraic identities
    - By Lagrange resolvents
    - By extrema
    - Dimensional analysis
categories:
    - Elementary algebra
---
In elementary algebra, the quadratic formula is the solution of the quadratic equation. There are other ways to solve the quadratic equation instead of using the quadratic formula, such as factoring, completing the square, or graphing. Using the quadratic formula is often the most convenient way.
