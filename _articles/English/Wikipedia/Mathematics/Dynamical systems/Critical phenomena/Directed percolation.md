---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Directed_percolation
offline_file: ""
offline_thumbnail: ""
uuid: e27c744d-d127-402f-bd3d-db243d111ba3
updated: 1484308743
title: Directed percolation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bond_Directed_Percolation.svg.png
tags:
    - Lattice models of directed percolation
    - Directed percolation as a dynamical process
    - Universal scaling behavior
    - Other examples
    - Experimental realizations
    - Sources
    - Literature
categories:
    - Critical phenomena
---
In statistical physics, directed percolation (DP) refers to a class of models that mimic filtering of fluids through porous materials along a given direction. Varying the microscopic connectivity of the pores, these models display a phase transition from a macroscopically permeable (percolating) to an impermeable (non-percolating) state. Directed percolation is also used as a simple model for epidemic spreading with a transition between survival and extinction of the disease depending on the infection rate.
