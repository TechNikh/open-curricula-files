---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shiranui
offline_file: ""
offline_thumbnail: ""
uuid: b0d4d245-8ac3-4f70-929b-4f5fdb35a10f
updated: 1484308337
title: Shiranui (optical phenomenon)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tenryo_Shiranui.jpg
tags:
    - Summary
    - History
    - Hypotheses of Shiranui
    - Sources
categories:
    - Uncategorized
---
Shiranui (不知火, unknown fire?, Shiranuhi in the historical kana orthography) is a atmospheric ghost light told about in Kyushu. They are said to appear on days of the noon moon such the kaijitsu (29th or 30th day) of the seventh month of the lunisolar calendary when the wind is weak, in the Yatsushiro Sea and the Ariake Sea.[1] Furthermore, they can be seen in modern times, but they have been determined to be an atmospheric optical phenomena.
