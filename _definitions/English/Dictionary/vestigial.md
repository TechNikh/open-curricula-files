---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vestigial
offline_file: ""
offline_thumbnail: ""
uuid: a13c2192-4364-4285-9809-97155468087f
updated: 1484310277
title: vestigial
categories:
    - Dictionary
---
vestigial
     adj : not fully developed in mature animals; "rudimentary wings"
           [syn: {rudimentary}]
