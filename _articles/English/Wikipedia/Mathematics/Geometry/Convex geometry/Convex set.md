---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_set
offline_file: ""
offline_thumbnail: ""
uuid: 66dc37bf-8b84-4dc9-bb0a-3b17fb288c22
updated: 1484309192
title: Convex set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convex_polygon_illustration1.svg_0.png
tags:
    - In vector spaces
    - Non-convex set
    - Properties
    - Intersections and unions
    - Closed convex sets
    - Convex sets and rectangles
    - Convex hulls and Minkowski sums
    - Convex hulls
    - Minkowski addition
    - Convex hulls of Minkowski sums
    - Minkowski sums of convex sets
    - Generalizations and extensions for convexity
    - Star-convex sets
    - Orthogonal convexity
    - Non-Euclidean geometry
    - Order topology
    - Convexity spaces
categories:
    - Convex geometry
---
In Euclidean space, a convex set is the region such that, for every pair of points within the region, every point on the straight line segment that joins the pair of points is also within the region. For example, a solid cube is a convex set, but anything that is hollow or has an indent, for example, a crescent shape, is not convex.
