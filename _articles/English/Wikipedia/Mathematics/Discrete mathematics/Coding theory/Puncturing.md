---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Puncturing
offline_file: ""
offline_thumbnail: ""
uuid: 5eded80e-4999-42f9-90f7-123e53b3d871
updated: 1484308565
title: Puncturing
categories:
    - Coding theory
---
In coding theory, puncturing is the process of removing some of the parity bits after encoding with an error-correction code. This has the same effect as encoding with an error-correction code with a higher rate, or less redundancy. However, with puncturing the same decoder can be used regardless of how many bits have been punctured, thus puncturing considerably increases the flexibility of the system without significantly increasing its complexity.
