---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stability_theory
offline_file: ""
offline_thumbnail: ""
uuid: 5fa2abe8-6cbd-4887-ae4f-064e544f65d6
updated: 1484308801
title: Stability theory
tags:
    - Overview in dynamical systems
    - Stability of fixed points
    - Maps
    - Linear autonomous systems
    - Non-linear autonomous systems
    - Lyapunov function for general dynamical systems
categories:
    - Limit sets
---
In mathematics, stability theory addresses the stability of solutions of differential equations and of trajectories of dynamical systems under small perturbations of initial conditions. The heat equation, for example, is a stable partial differential equation because small perturbations of initial data lead to small variations in temperature at a later time as a result of the maximum principle. In partial differential equations one may measure the distances between functions using Lp norms or the sup norm, while in differential geometry one may measure the distance between spaces using the Gromov–Hausdorff distance.
