---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hans_L._Bodlaender
offline_file: ""
offline_thumbnail: ""
uuid: 8b99ea79-3696-4add-9922-3b4719b01bde
updated: 1484309457
title: Hans L. Bodlaender
tags:
    - Life and work
    - Selected publications
categories:
    - Graph theorists
---
Hans Leo Bodlaender (born April 21, 1960)[1] is a Dutch computer scientist, a professor of computer science at Utrecht University. Bodlaender is known for his work on graph algorithms and in particular for algorithms relating to tree decomposition of graphs.
