---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identify
offline_file: ""
offline_thumbnail: ""
uuid: 6b4f1691-519f-45d1-9b73-b68065bd8f5a
updated: 1484310359
title: identify
categories:
    - Dictionary
---
identify
     v 1: recognize as being; establish the identity of someone or
          something; "She identified the man on the 'wanted'
          poster" [syn: {place}]
     2: give the name or identifying characteristics of; refer to by
        name or some other identifying characteristic property;
        "Many senators were named in connection with the scandal";
        "The almanac identifies the auspicious months" [syn: {name}]
     3: consider (oneself) as similar to somebody else; "He
        identified with the refugees"
     4: conceive of as united or associated; "Sex activity is
        closely identified with the hypothalamus"
     5: identify as in botany or biology, for ...
