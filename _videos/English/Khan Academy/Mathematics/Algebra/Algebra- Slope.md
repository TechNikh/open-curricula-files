---
version: 1
type: video
provider: YouTube
id: hXP1Gv9IMBo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Slope.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 663572c2-d140-4076-96fd-8743d989e42b
updated: 1486069569
title: 'Algebra- Slope'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/hXP1Gv9IMBo/default.jpg
    - https://i3.ytimg.com/vi/hXP1Gv9IMBo/1.jpg
    - https://i3.ytimg.com/vi/hXP1Gv9IMBo/2.jpg
    - https://i3.ytimg.com/vi/hXP1Gv9IMBo/3.jpg
---
Figuring out the slope of a line
