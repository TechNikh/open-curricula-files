---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dudley%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3c25dec8-2c98-4d1f-813c-8b701853d37e
updated: 1484308760
title: "Dudley's theorem"
categories:
    - Entropy
---
In probability theory, Dudley’s theorem is a result relating the expected upper bound and regularity properties of a Gaussian process to its entropy and covariance structure.
