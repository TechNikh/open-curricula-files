---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Four_color_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 1070d1d1-9f64-429c-a3c9-153b37ed9d63
updated: 1484309382
title: Four color theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Four_Colour_Map_Example.svg.png
tags:
    - Precise formulation of the theorem
    - History
    - Early proof attempts
    - Proof by computer
    - Simplification and verification
    - Summary of proof ideas
    - False disproofs
    - Three-coloring
    - Generalizations
    - Notes
categories:
    - Graph coloring
---
In mathematics, the four color theorem, or the four color map theorem, states that, given any separation of a plane into contiguous regions, producing a figure called a map, no more than four colors are required to color the regions of the map so that no two adjacent regions have the same color. Two regions are called adjacent if they share a common boundary that is not a corner, where corners are the points shared by three or more regions.[1]
