---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adapted
offline_file: ""
offline_thumbnail: ""
uuid: 6381f8db-4978-4de7-a0b7-800f518de5c3
updated: 1484310286
title: adapted
categories:
    - Dictionary
---
adapted
     adj : changed in order to improve or made more fit for a
           particular purpose; "seeds precisely adapted to the
           area"; "instructions altered to suit the children's
           different ages" [syn: {altered}]
