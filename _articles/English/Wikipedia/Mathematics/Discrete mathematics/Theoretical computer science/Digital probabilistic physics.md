---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digital_probabilistic_physics
offline_file: ""
offline_thumbnail: ""
uuid: cac9d9ea-4dcb-4b1d-8dbf-32ed12ae4215
updated: 1484308664
title: Digital probabilistic physics
tags:
    - Criticism
categories:
    - Theoretical computer science
---
Digital probabilistic physics is a branch of digital philosophy which holds that the universe exists as a nondeterministic state machine. The notion of the universe existing as a state machine was first postulated by Konrad Zuse's book Rechnender Raum. Adherents hold that the universe state machine can move between more and less probable states, with the less probable states containing more information. This theory is in contrast to digital physics, which holds that the history of the universe is computable and is deterministically unfolding from initial conditions.
