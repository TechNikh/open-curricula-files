---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484558041
title: demon
categories:
    - Dictionary
---
demon
     n 1: one of the evil spirits of traditional Jewish and Christian
          belief [syn: {devil}, {fiend}, {daemon}, {daimon}]
     2: a cruel wicked and inhuman person [syn: {monster}, {fiend},
        {devil}, {ogre}]
     3: someone extremely diligent or skillful; "he worked like a
        demon to finish the job on time"; "she's a demon at math"
