---
version: 1
type: video
provider: YouTube
id: gC_gl65l2lo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: df3e98ce-04d0-4a35-a3d5-6097ee80c66a
updated: 1486069637
title: 'Total internal reflection breakdown - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/gC_gl65l2lo/default.jpg
    - https://i3.ytimg.com/vi/gC_gl65l2lo/1.jpg
    - https://i3.ytimg.com/vi/gC_gl65l2lo/2.jpg
    - https://i3.ytimg.com/vi/gC_gl65l2lo/3.jpg
---
