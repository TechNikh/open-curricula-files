---
version: 1
type: video
provider: YouTube
id: EgtOBlHb-8c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a64ba486-ae23-40aa-832f-a851ef08d8b5
updated: 1486069630
title: >
    Hindi Varnamala, Hindi Alphabet Learn Reading and Writing
    Class 1 Hindi
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/EgtOBlHb-8c/default.jpg
    - https://i3.ytimg.com/vi/EgtOBlHb-8c/1.jpg
    - https://i3.ytimg.com/vi/EgtOBlHb-8c/2.jpg
    - https://i3.ytimg.com/vi/EgtOBlHb-8c/3.jpg
---
