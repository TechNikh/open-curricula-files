---
version: 1
type: video
provider: YouTube
id: pps3iDTXRFI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a18f17fc-9e3c-48ae-9c58-f6c36ec9add0
updated: 1486069632
title: Indefinite Pronoun
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/pps3iDTXRFI/default.jpg
    - https://i3.ytimg.com/vi/pps3iDTXRFI/1.jpg
    - https://i3.ytimg.com/vi/pps3iDTXRFI/2.jpg
    - https://i3.ytimg.com/vi/pps3iDTXRFI/3.jpg
---
