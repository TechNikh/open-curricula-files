---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hjelmslev%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3a795682-e6f2-4f12-bafe-09aba32bf999
updated: 1484309117
title: "Hjelmslev's theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hjelmslev%2527s_theorem.svg.png'
categories:
    - Theorems in geometry
---
In geometry, Hjelmslev's theorem, named after Johannes Hjelmslev, is the statement that if points P, Q, R... on a line are isometrically mapped to points P´, Q´, R´... of another line in the same plane, then the midpoints of the segments PP`, QQ´, RR´... also lie on a line.
