---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clinic
offline_file: ""
offline_thumbnail: ""
uuid: 363542f2-f83f-483b-9f7e-4805eecbd77d
updated: 1484310607
title: clinic
categories:
    - Dictionary
---
clinic
     n 1: a medical establishment run by a group of medical
          specialists
     2: meeting for diagnosis of problems and instruction or
        remedial work in a particular activity
     3: a healthcare facility for outpatient care
