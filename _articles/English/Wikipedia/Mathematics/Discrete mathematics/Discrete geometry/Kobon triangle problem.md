---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kobon_triangle_problem
offline_file: ""
offline_thumbnail: ""
uuid: 17f1f127-87a8-415f-8007-452a559bf573
updated: 1484308611
title: Kobon triangle problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Kobon_triangles.gif
tags:
    - Examples
categories:
    - Discrete geometry
---
The Kobon triangle problem is an unsolved problem in combinatorial geometry first stated by Kobon Fujimura. The problem asks for the largest number N(k) of nonoverlapping triangles whose sides lie on an arrangement of k lines. Variations of the problem consider the projective plane rather than the Euclidean plane, and require that the triangles not be crossed by any other lines of the arrangement.[1]
