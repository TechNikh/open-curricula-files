---
version: 1
type: video
provider: YouTube
id: fOl7lrNuOnk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/%E2%9C%94%20Transcription%20and%20Translation%20ANIMATION%20-%20MADE%20EASY.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 53368dfb-3d12-4482-9763-d408690edf6f
updated: 1486069672
title: '✔ Transcription and Translation ANIMATION - MADE EASY'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/fOl7lrNuOnk/default.jpg
    - https://i3.ytimg.com/vi/fOl7lrNuOnk/1.jpg
    - https://i3.ytimg.com/vi/fOl7lrNuOnk/2.jpg
    - https://i3.ytimg.com/vi/fOl7lrNuOnk/3.jpg
---
