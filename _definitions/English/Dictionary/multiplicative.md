---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multiplicative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484371981
title: multiplicative
categories:
    - Dictionary
---
multiplicative
     adj : tending or having the power to multiply or increase in
           number or quantity or degree; "the multiplicative
           tendency of proportional representation"
