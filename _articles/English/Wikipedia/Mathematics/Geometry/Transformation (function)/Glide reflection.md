---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Glide_reflection
offline_file: ""
offline_thumbnail: ""
uuid: c216abc4-0167-4103-80f6-0bc3d8fe07c3
updated: 1484309334
title: Glide reflection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Glide_reflection.svg_0.png
tags:
    - Description
    - Wallpaper groups
    - Glide reflection in nature and games
categories:
    - Transformation (function)
---
In 2-dimensional geometry, a glide reflection (or transflection) is a type of opposite isometry of the Euclidean plane: the composition of a reflection in a line and a translation along that line.
