---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philosophically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484521321
title: philosophically
categories:
    - Dictionary
---
philosophically
     adv 1: in a philosophic manner; "she took it philosophically"
     2: with respect to philosophy; "the movement is philosphically
        indebted to Rousseau"
