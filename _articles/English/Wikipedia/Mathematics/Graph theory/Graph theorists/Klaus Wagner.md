---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Klaus_Wagner
offline_file: ""
offline_thumbnail: ""
uuid: 72963b78-1069-4e50-b915-fc262040ff44
updated: 1484309476
title: Klaus Wagner
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Wagner_and_Harary.jpg
categories:
    - Graph theorists
---
Klaus Wagner (March 31, 1910 – February 6, 2000) was a German mathematician. He studied topology at the University of Cologne under the supervision of Karl Dörge,(de) who had been a student of Issai Schur. Wagner received his Ph.D. in 1937, and taught at Cologne for many years himself.[1] In 1970, he moved to the University of Duisburg, where he remained until his retirement in 1978.
