---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isometry
offline_file: ""
offline_thumbnail: ""
uuid: 9a6b25e2-be47-4280-9bf0-9e0e3e2cf033
updated: 1484309269
title: Isometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/310px-Academ_Reflections_with_parallel_axis_on_wallpaper.svg.png
tags:
    - Introduction
    - Formal definitions
    - Examples
    - Linear isometry
    - Generalizations
    - Bibliography
categories:
    - Metric geometry
---
In mathematics, an isometry (or congruence, or congruent transformation) is a distance-preserving transformation between metric spaces, usually assumed to be bijective.[1]
