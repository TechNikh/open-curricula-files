---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bloch%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 93df4f5e-41a5-4c5d-a865-5039d43424d1
updated: 1484309138
title: "Bloch's formula"
categories:
    - Algebraic geometry
---
In algebraic K-theory, a branch of mathematics, Bloch's formula, introduced by Spencer Bloch for 
  
    
      
        
          K
          
            2
          
        
      
    
    {\displaystyle K_{2}}
  
, states that the Chow group of a smooth variety X over a field is isomorphic to the cohomology of X with coefficients in the K-theory of the structure sheaf 
  
    
      
        
          
            
              O
            
          
          
            X
          
        
      
    
    {\displaystyle {\mathcal {O}}_{X}}
  
; that is,
