---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/begged
offline_file: ""
offline_thumbnail: ""
uuid: e64c1819-b3d4-42af-8cd7-afa34b804f4a
updated: 1484310144
title: begged
categories:
    - Dictionary
---
beg
     v 1: call upon in supplication; entreat; "I beg you to stop!"
          [syn: {implore}, {pray}]
     2: make a solicitation or entreaty for something; request
        urgently or persistently; "Henry IV solicited the Pope for
        a divorce"; "My neighbor keeps soliciting money for
        different charities" [syn: {solicit}, {tap}]
     3: ask to obtain free; "beg money and food"
     [also: {begging}, {begged}]
