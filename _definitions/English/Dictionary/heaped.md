---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heaped
offline_file: ""
offline_thumbnail: ""
uuid: 0787f66b-cf92-4d03-9e45-de25dbcd1713
updated: 1484310148
title: heaped
categories:
    - Dictionary
---
heaped
     adj : thrown together in a pile; "a desk heaped with books";
           "heaped-up ears of corn"; "ungraded papers piled high"
           [syn: {heaped-up}, {piled}, {cumulous}]
