---
version: 1
type: article
id: https://en.wikipedia.org/wiki/IAPWS
offline_file: ""
offline_thumbnail: ""
uuid: 487880ed-82ca-472a-9a7e-73c2dd048128
updated: 1484308849
title: IAPWS
categories:
    - Thermodynamics
---
The International Association for the Properties of Water and Steam (IAPWS) is an international non-profit association of national organizations concerned with the properties of water and steam, particularly thermophysical properties and other aspects of high-temperature steam, water and aqueous mixtures that are relevant to thermal power cycles and other industrial applications.
