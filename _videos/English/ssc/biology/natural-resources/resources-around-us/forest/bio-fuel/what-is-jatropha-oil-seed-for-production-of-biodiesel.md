---
title: What is Jatropha, oil seed for production of biodiesel
youtube_id: lRl7JwS0j5Q
tags:
    - Oil Press
    - Oil Extraction
    - screw press
    - Biofuel
    - biodiesel
    - seed oil
    - oil seed
    - jatropha
    - vegetable oil
    - oil press machine
    - jatropha oil
    - pressing oil
    - sunflower oil
    - sunflower
    - cold press
    - jatropha consulting
    - algae
    - production of biodiesel
    - flax seed
    - canola
    - sunflower seed
    - Cropland
    - agoil
    - david gair
    - pennycress
categories:
    - Bio fuel
---
