---
version: 1
type: video
provider: YouTube
id: glZBJ-vL1hM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 461bbd89-481e-4686-953c-e9aa819c2766
updated: 1486069647
title: 'The Sun - The Solar System - Animation Educational Videos For Kids'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/glZBJ-vL1hM/default.jpg
    - https://i3.ytimg.com/vi/glZBJ-vL1hM/1.jpg
    - https://i3.ytimg.com/vi/glZBJ-vL1hM/2.jpg
    - https://i3.ytimg.com/vi/glZBJ-vL1hM/3.jpg
---
