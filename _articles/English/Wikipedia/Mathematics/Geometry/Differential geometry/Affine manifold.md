---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 88100269-1747-4df1-8ba5-951455e37396
updated: 1484309202
title: Affine manifold
tags:
    - Formal definition
    - Important longstanding conjectures
    - Notes
categories:
    - Differential geometry
---
Equivalently, it is a manifold that is (if connected) covered by an open subset of 
  
    
      
        
          
            
              R
            
          
          
            n
          
        
      
    
    {\displaystyle {\mathbb {R}}^{n}}
  
, with monodromy acting by affine transformations. This equivalence is an easy corollary of Cartan–Ambrose–Hicks theorem.
