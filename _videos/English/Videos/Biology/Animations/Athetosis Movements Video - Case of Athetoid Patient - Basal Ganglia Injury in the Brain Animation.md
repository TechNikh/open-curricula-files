---
version: 1
type: video
provider: YouTube
id: 8DLcS6fx_WI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Athetosis%20Movements%20Video%20-%20Case%20of%20Athetoid%20Patient%20-%20Basal%20Ganglia%20Injury%20in%20the%20Brain%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 51118c9c-2f84-426a-a5e9-c4b3ff88f2e6
updated: 1486069670
title: 'Athetosis Movements Video - Case of Athetoid Patient - Basal Ganglia Injury in the Brain Animation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8DLcS6fx_WI/default.jpg
    - https://i3.ytimg.com/vi/8DLcS6fx_WI/1.jpg
    - https://i3.ytimg.com/vi/8DLcS6fx_WI/2.jpg
    - https://i3.ytimg.com/vi/8DLcS6fx_WI/3.jpg
---
