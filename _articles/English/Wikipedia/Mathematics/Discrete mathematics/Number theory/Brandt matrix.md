---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brandt_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 04b7eddf-3f88-4412-bac2-7b44b1ec9df7
updated: 1484308632
title: Brandt matrix
categories:
    - Number theory
---
In mathematics, Brandt matrices are matrices, introduced by Brandt (1943), that are related to the number of ideals of given norm in an ideal class of a definite quaternion algebra over the rationals, and that give a representation of the Hecke algebra. Eichler (1955) calculated the traces of the Brandt matrices.
