---
version: 1
type: video
provider: YouTube
id: hdhApCM3z1k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 91997a05-a5a9-47e2-8a5a-34a029d9f21d
updated: 1486069629
title: 'Learn Hindi Grammar  कारक (Karak)'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/hdhApCM3z1k/default.jpg
    - https://i3.ytimg.com/vi/hdhApCM3z1k/1.jpg
    - https://i3.ytimg.com/vi/hdhApCM3z1k/2.jpg
    - https://i3.ytimg.com/vi/hdhApCM3z1k/3.jpg
---
