---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_computing
offline_file: ""
offline_thumbnail: ""
uuid: a48db557-c2a9-491a-b54e-944e827896e3
updated: 1484308683
title: Quantum computing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bloch_Sphere.svg.png
tags:
    - Basis
    - Mechanics
    - Operation
    - Potential
    - Quantum decoherence
    - Developments
    - Timeline
    - Relation to computational complexity theory
categories:
    - Theoretical computer science
---
Quantum computing studies theoretical computation systems (quantum computers) that make direct use of quantum-mechanical phenomena, such as superposition and entanglement, to perform operations on data.[1] Quantum computers are different from binary digital electronic computers based on transistors. Whereas common digital computing requires that the data are encoded into binary digits (bits), each of which is always in one of two definite states (0 or 1), quantum computation is analog and uses quantum bits, which can be in an infinite number of superpositions of states. A quantum Turing machine is a theoretical model of such a computer, and is also known as the universal quantum computer. ...
