---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/less
offline_file: ""
offline_thumbnail: ""
uuid: 29d24c72-f90e-43e5-aefc-61f92c2d6f9e
updated: 1484310307
title: less
categories:
    - Dictionary
---
less
     adj 1: (comparative of `little' usually used with mass nouns) a
            quantifier meaning not as great in amount or degree;
            "of less importance"; "less time to spend with the
            family"; "a shower uses less water"; "less than three
            years old" [syn: {less(a)}] [ant: {more(a)}]
     2: (usually preceded by `no') lower in quality; "no less than
        perfect"
     3: (usually preceded by `no') lower in esteem; "no less a
        person than the king himself" [syn: {lower}]
     4: (nonstandard in some uses but often idiomatic with measure
        phrases) fewer; "less than three weeks"; "no less than 50
        people attended"; "in 25 words or ...
