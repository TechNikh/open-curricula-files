---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stainless
offline_file: ""
offline_thumbnail: ""
uuid: 09f03bf6-18b6-4656-b8d4-63ad6082e068
updated: 1484310377
title: stainless
categories:
    - Dictionary
---
stainless
     adj : of reputation; "his unsullied name"; "an untarnished
           reputation" [syn: {unstained}, {unsullied}, {untainted},
            {untarnished}]
