---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/private
offline_file: ""
offline_thumbnail: ""
uuid: 4e7b1b5a-9d7f-40ec-94fd-7354947904af
updated: 1484310238
title: private
categories:
    - Dictionary
---
private
     adj 1: confined to particular persons or groups or providing
            privacy; "a private place"; "private discussions";
            "private lessons"; "a private club"; "a private
            secretary"; "private property"; "the former President
            is now a private citizen"; "public figures struggle to
            maintain a private life" [ant: {public}]
     2: concerning things deeply private and personal; "private
        correspondence"; "private family matters"
     3: concerning one person exclusively; "we all have individual
        cars"; "each room has a private bath" [syn: {individual(a)}]
     4: not expressed; "secret (or private) thoughts" [syn: ...
