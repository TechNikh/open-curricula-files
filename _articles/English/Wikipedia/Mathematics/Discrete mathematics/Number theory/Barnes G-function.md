---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barnes_G-function
offline_file: ""
offline_thumbnail: ""
uuid: e01a5600-a3b2-4cdc-990d-94eb51bd1ab6
updated: 1484308635
title: Barnes G-function
tags:
    - Functional equation and integer arguments
    - Reflection formula 1.0
    - Reflection formula 2.0
    - Taylor series expansion
    - Multiplication formula
    - Asymptotic expansion
    - Relation to the Loggamma integral
categories:
    - Number theory
---
In mathematics, the Barnes G-function G(z) is a function that is an extension of superfactorials to the complex numbers. It is related to the Gamma function, the K-function and the Glaisher–Kinkelin constant, and was named after mathematician Ernest William Barnes.[1] Up to elementary factors, it is a special case of the double gamma function.
