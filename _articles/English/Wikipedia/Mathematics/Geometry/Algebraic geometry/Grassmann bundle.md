---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grassmann_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 3b54f63c-bc63-45a9-8c09-dc242e8f27b3
updated: 1484309149
title: Grassmann bundle
categories:
    - Algebraic geometry
---
such that the fiber 
  
    
      
        
          p
          
            −
            1
          
        
        (
        x
        )
        =
        
          G
          
            d
          
        
        (
        
          E
          
            x
          
        
        )
      
    
    {\displaystyle p^{-1}(x)=G_{d}(E_{x})}
  
 is the Grassmannian of the d-dimensional vector subspaces of 
  
    
      
        
          E
          
            x
          
        
      
    
    {\displaystyle E_{x}}
  
. For example, 
  
    
      
        
          G
          
            1
          
        
        (
        E
        )
        =
        
 ...
