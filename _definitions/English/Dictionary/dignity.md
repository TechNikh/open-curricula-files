---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dignity
offline_file: ""
offline_thumbnail: ""
uuid: 97879830-bf2d-4001-9ed9-90db3eea8359
updated: 1484310443
title: dignity
categories:
    - Dictionary
---
dignity
     n 1: the quality of being worthy of esteem or respect; "it was
          beneath his dignity to cheat"; "showed his true dignity
          when under pressure" [syn: {self-respect}, {self-esteem},
           {self-regard}]
     2: formality in bearing and appearance; "he behaved with great
        dignity" [syn: {lordliness}, {gravitas}]
     3: high office or rank or station; "he respected the dignity of
        the emissaries"
