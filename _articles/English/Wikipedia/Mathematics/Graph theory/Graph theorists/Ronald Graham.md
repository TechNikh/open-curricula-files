---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ronald_Graham
offline_file: ""
offline_thumbnail: ""
uuid: adde86b4-0fee-49e2-8395-e55095c535cc
updated: 1484309466
title: Ronald Graham
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ronald_graham_juggling.jpg
tags:
    - Biography
    - Awards and honors
    - Books
categories:
    - Graph theorists
---
Ronald (Ron) Lewis Graham (born October 31, 1935)[1] is a mathematician credited by the American Mathematical Society as being "one of the principal architects of the rapid development worldwide of discrete mathematics in recent years".[2] He has done important work in scheduling theory, computational geometry, Ramsey theory, and quasi-randomness.[3]
