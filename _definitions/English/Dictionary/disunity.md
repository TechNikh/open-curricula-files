---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disunity
offline_file: ""
offline_thumbnail: ""
uuid: b2e8c249-06a8-4af8-895f-e33459b2062b
updated: 1484310192
title: disunity
categories:
    - Dictionary
---
disunity
     n : lack of unity (usually resulting from dissension)
