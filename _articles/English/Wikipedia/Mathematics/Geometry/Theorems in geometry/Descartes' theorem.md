---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Descartes%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 662390f8-d6ea-4eab-a74f-277092603c2b
updated: 1484309111
title: "Descartes' theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Descartes_Circles.svg.png
tags:
    - History
    - Definition of curvature
    - Special cases
    - Complex Descartes theorem
    - Generalizations
    - Notes
categories:
    - Theorems in geometry
---
In geometry, Descartes' theorem states that for every four kissing, or mutually tangent, circles, the radii of the circles satisfy a certain quadratic equation. By solving this equation, one can construct a fourth circle tangent to three given, mutually tangent circles. The theorem is named after René Descartes, who stated it in 1643.
