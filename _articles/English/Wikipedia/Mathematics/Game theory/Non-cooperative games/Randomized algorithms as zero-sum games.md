---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Randomized_algorithms_as_zero-sum_games
offline_file: ""
offline_thumbnail: ""
uuid: 19f6bef6-aa00-4f45-bdd7-83eb2bdafe32
updated: 1484309055
title: Randomized algorithms as zero-sum games
categories:
    - Non-cooperative games
---
Randomized algorithms are algorithms that employ a degree of randomness as part of their logic. These algorithms can be used to give good average-case results (complexity-wise) to problems which are hard to solve deterministically, or display poor worst-case complexity. An algorithmic game theoretic approach can help explain why in the average case randomized algorithms may work better than deterministic algorithms.
