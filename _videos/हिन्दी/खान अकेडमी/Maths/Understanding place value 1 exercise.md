---
version: 1
type: video
provider: YouTube
id: PlX1pfS74xA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 001c2016-efd2-4927-9d49-a063ae85400e
updated: 1486069613
title: Understanding place value 1 exercise
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/PlX1pfS74xA/default.jpg
    - https://i3.ytimg.com/vi/PlX1pfS74xA/1.jpg
    - https://i3.ytimg.com/vi/PlX1pfS74xA/2.jpg
    - https://i3.ytimg.com/vi/PlX1pfS74xA/3.jpg
---
