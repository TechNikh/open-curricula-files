---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Additive_identity
offline_file: ""
offline_thumbnail: ""
uuid: 6bb6874b-5cbc-4670-be6e-0a7ac6058b26
updated: 1484308900
title: Additive identity
tags:
    - Elementary examples
    - Formal definition
    - Further examples
    - Proofs
    - The additive identity is unique in a group
    - The additive identity annihilates ring elements
    - >
        The additive and multiplicative identities are different in
        a non-trivial ring
categories:
    - Elementary algebra
---
In mathematics the additive identity of a set which is equipped with the operation of addition is an element which, when added to any element x in the set, yields x. One of the most familiar additive identities is the number 0 from elementary mathematics, but additive identities occur in other mathematical structures where addition is defined, such as in groups and rings.
