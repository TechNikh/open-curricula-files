---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concerted
offline_file: ""
offline_thumbnail: ""
uuid: 43cbe87e-9cad-46c2-ba5a-2c2951f32ba3
updated: 1484310467
title: concerted
categories:
    - Dictionary
---
concerted
     adj : involving the joint activity of two or more; "the attack was
           met by the combined strength of two divisions";
           "concerted action"; "the conjunct influence of fire and
           strong dring"; "the conjunctive focus of political
           opposition"; "a cooperative effort"; "a united effort";
           "joint military activities" [syn: {combined}, {conjunct},
            {conjunctive}, {cooperative}, {united}]
