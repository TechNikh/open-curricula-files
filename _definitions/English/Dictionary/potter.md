---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/potter
offline_file: ""
offline_thumbnail: ""
uuid: b7dd5f81-a302-46c7-b808-cdec01b2b02e
updated: 1484310451
title: potter
categories:
    - Dictionary
---
potter
     n : a craftsman who shapes pottery on a potter's wheel and bakes
         them it a kiln [syn: {thrower}, {ceramicist}, {ceramist}]
     v 1: do random, unplanned work or activities or spend time idly;
          "The old lady is usually mucking about in her little
          house" [syn: {putter}, {mess around}, {tinker}, {monkey},
           {monkey around}, {muck about}, {muck around}]
     2: work lightly; "The old lady is pottering around in the
        garden" [syn: {putter}]
     3: move around aimlessly [syn: {putter}, {potter around}, {putter
        around}]
