---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beer_distribution_game
offline_file: ""
offline_thumbnail: ""
uuid: fb5221ec-0b6a-4f03-9433-54031401909c
updated: 1484309052
title: Beer distribution game
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Projet_bi%25C3%25A8re_logo_v2.png'
tags:
    - Gameplay
    - Influence
    - Additional reading
    - Computer-based tools
    - Non computer-based kits
categories:
    - Non-cooperative games
---
The beer distribution game (also known as the beer game) is an experiential learning business simulation game created by a group of professors at MIT Sloan School of Management in early 1960s to demonstrate a number of key principles of supply chain management. The game is played by teams of at least four players, often in heated competition, and takes at least one hour to complete. A debriefing session of roughly equivalent length typically follows to review the results of each team and discuss the lessons involved.
