---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kg
offline_file: ""
offline_thumbnail: ""
uuid: b98b9803-f654-4b3e-9d59-dbfe87c789ba
updated: 1484310268
title: kg
categories:
    - Dictionary
---
kg
     n : one thousand grams; the basic unit of mass adopted under the
         Systeme International d'Unites; "a kilogram is
         approximately 2.2 pounds" [syn: {kilogram}, {kilo}]
