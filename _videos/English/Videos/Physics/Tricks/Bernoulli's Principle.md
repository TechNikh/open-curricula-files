---
version: 1
type: video
provider: YouTube
id: WDGNcmEOjs4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c7836dc9-ae55-44c2-9357-54ff2db5fb16
updated: 1486069658
title: "Bernoulli's Principle"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/WDGNcmEOjs4/default.jpg
    - https://i3.ytimg.com/vi/WDGNcmEOjs4/1.jpg
    - https://i3.ytimg.com/vi/WDGNcmEOjs4/2.jpg
    - https://i3.ytimg.com/vi/WDGNcmEOjs4/3.jpg
---
