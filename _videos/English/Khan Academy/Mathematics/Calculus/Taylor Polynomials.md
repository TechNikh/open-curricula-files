---
version: 1
type: video
provider: YouTube
id: 8SsC5st4LnI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Taylor%20Polynomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 574c89c4-430c-4933-bb1f-23c33f2b208a
updated: 1486069690
title: Taylor Polynomials
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/8SsC5st4LnI/default.jpg
    - https://i3.ytimg.com/vi/8SsC5st4LnI/1.jpg
    - https://i3.ytimg.com/vi/8SsC5st4LnI/2.jpg
    - https://i3.ytimg.com/vi/8SsC5st4LnI/3.jpg
---
