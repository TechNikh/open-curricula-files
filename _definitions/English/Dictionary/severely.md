---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/severely
offline_file: ""
offline_thumbnail: ""
uuid: 1179bf4d-5dc8-478d-85c7-aefee1a92ec2
updated: 1484310535
title: severely
categories:
    - Dictionary
---
severely
     adv 1: to a severe or serious degree; "fingers so badly frozen they
            had to be amputated"; "badly injured"; "a severely
            impaired heart"; "is gravely ill"; "was seriously ill"
            [syn: {badly}, {gravely}, {seriously}]
     2: with sternness; in a severe manner; "`No,' she said
        sternly"; "peered severely over her glasses" [syn: {sternly}]
     3: causing great damage or hardship; "industries hit hard by
        the depression"; "she was severely affected by the bank's
        failure" [syn: {hard}]
