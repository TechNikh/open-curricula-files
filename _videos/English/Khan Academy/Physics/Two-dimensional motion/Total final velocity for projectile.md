---
version: 1
type: video
provider: YouTube
id: sTp4cI9VyCU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Two-dimensional%20motion/Total%20final%20velocity%20for%20projectile.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a6590ef-2da6-45b7-86ce-355c89516e89
updated: 1486069593
title: Total final velocity for projectile
categories:
    - Two-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/sTp4cI9VyCU/default.jpg
    - https://i3.ytimg.com/vi/sTp4cI9VyCU/1.jpg
    - https://i3.ytimg.com/vi/sTp4cI9VyCU/2.jpg
    - https://i3.ytimg.com/vi/sTp4cI9VyCU/3.jpg
---
Calculating the total final velocity for a projectile landing at a different altitude (mistake near end: I write 29.03 when it should be 26.03 m/s and the final total magnitude should be 26.55 m/s 78.7 degrees below horizontal. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/two-dimensional-motion/two-dimensional-projectile-mot/v/correction-to-total-final-velocity-for-projectile?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/two-dimensional-motion/two-dimensional-projectile-mot/v/total-displacement-for-projectile?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
