---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carriage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500921
title: carriage
categories:
    - Dictionary
---
carriage
     n 1: a railcar where passengers ride [syn: {passenger car}, {coach}]
     2: a vehicle with four wheels drawn by two or more horses [syn:
         {equipage}, {rig}]
     3: characteristic way of bearing one's body; "stood with good
        posture" [syn: {bearing}, {posture}]
     4: a machine part that carries something else
     5: a small vehicle with four wheels in which a baby or child is
        pushed around [syn: {baby buggy}, {baby carriage}, {perambulator},
         {pram}, {stroller}, {go-cart}, {pushchair}, {pusher}]
