---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Occam_learning
offline_file: ""
offline_thumbnail: ""
uuid: 598d279b-015d-486a-8135-c8e4a4d03767
updated: 1484308681
title: Occam learning
tags:
    - Introduction
    - Definition of Occam learning
    - The relation between Occam and PAC learning
    - Theorem (Occam learning implies PAC learning)
    - >
        Theorem (Occam learning implies PAC learning, cardinality
        version)
    - Proof that Occam learning implies PAC learning
    - Improving sample complexity for common problems
    - Extensions
categories:
    - Theoretical computer science
---
In computational learning theory, Occam learning is a model of algorithmic learning where the objective of the learner is to output a succinct representation of received training data. This is closely related to probably approximately correct (PAC) learning, where the learner is evaluated on its predictive power of a test set.
