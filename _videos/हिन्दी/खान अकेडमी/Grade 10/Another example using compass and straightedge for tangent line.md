---
version: 1
type: video
provider: YouTube
id: jmGBsrhaHUo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 06ce2b05-bd4a-4865-9261-a95c896b5332
updated: 1486069619
title: >
    Another example using compass and straightedge for tangent
    line
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/jmGBsrhaHUo/default.jpg
    - https://i3.ytimg.com/vi/jmGBsrhaHUo/1.jpg
    - https://i3.ytimg.com/vi/jmGBsrhaHUo/2.jpg
    - https://i3.ytimg.com/vi/jmGBsrhaHUo/3.jpg
---
