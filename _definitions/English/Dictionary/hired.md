---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hired
offline_file: ""
offline_thumbnail: ""
uuid: ff41e7e2-ee7d-4bf2-883c-7f27fb68a804
updated: 1484310521
title: hired
categories:
    - Dictionary
---
hired
     adj 1: having services engaged for a fee; "hired hands"; "a hired
            gun"
     2: hired for the exclusive temporary use of a group of
        travelers; "a chartered plane"; "the chartered buses
        arrived on time" [syn: {chartered}, {leased}] [ant: {unchartered}]
