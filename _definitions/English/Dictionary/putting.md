---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/putting
offline_file: ""
offline_thumbnail: ""
uuid: de86b4f2-af46-4d77-ba89-2a8274b186b6
updated: 1484310337
title: putting
categories:
    - Dictionary
---
put
     n : the option to sell a given stock (or stock index or
         commodity future) at a given price before a given date
         [syn: {put option}] [ant: {call option}]
     v 1: put into a certain place or abstract location; "Put your
          things here"; "Set the tray down"; "Set the dogs on the
          scent of the missing children"; "Place emphasis on a
          certain point" [syn: {set}, {place}, {pose}, {position},
           {lay}]
     2: cause to be in a certain state; cause to be in a certain
        relation; "That song put me in awful good humor"
     3: formulate in a particular style or language; "I wouldn't put
        it that way"; "She cast her request in ...
