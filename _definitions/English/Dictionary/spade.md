---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spade
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484392021
title: spade
categories:
    - Dictionary
---
spade
     n 1: a playing card in the major suit of spades
     2: a sturdy hand shovel that can be pushed into the earth with
        the foot
     3: (ethnic slur) offensive name for a Black person; "only a
        Black can call another Black a nigga" [syn: {nigger}, {nigga},
         {coon}, {jigaboo}, {nigra}]
     v : dig (up) with a spade; "I spade compost into the flower
         beds"
