---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Connection_(algebraic_framework)
offline_file: ""
offline_thumbnail: ""
uuid: ec083439-7dad-4ac4-9e01-3c123e7bfe74
updated: 1484309292
title: Connection (algebraic framework)
tags:
    - Commutative algebra
    - Graded commutative algebra
    - Noncommutative algebra
    - Notes
categories:
    - Noncommutative geometry
---
Geometry of quantum systems (e.g., noncommutative geometry and supergeometry) is mainly phrased in algebraic terms of modules and algebras. Connections on modules are generalization of a linear connection on a smooth vector bundle 
  
    
      
        E
        →
        X
      
    
    {\displaystyle E\to X}
  
 written as a Koszul connection on the 
  
    
      
        
          C
          
            ∞
          
        
        (
        X
        )
      
    
    {\displaystyle C^{\infty }(X)}
  
-module of sections of 
  
    
      
        E
        →
        X
      
    
    {\displaystyle E\to X}
  
.[1]
