---
version: 1
type: video
provider: YouTube
id: zScn3tV9YPU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Minkowski%20Space-Time-%20%20Spacetime%20in%20Special%20Relativity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5d490a08-a03e-4348-9f3e-1b2be7a61238
updated: 1486069656
title: 'Minkowski Space-Time-  Spacetime in Special Relativity'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/zScn3tV9YPU/default.jpg
    - https://i3.ytimg.com/vi/zScn3tV9YPU/1.jpg
    - https://i3.ytimg.com/vi/zScn3tV9YPU/2.jpg
    - https://i3.ytimg.com/vi/zScn3tV9YPU/3.jpg
---
