---
version: 1
type: video
provider: YouTube
id: 0rHUDWjR5gg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Introduction%20to%20Astronomy-%20Crash%20Course%20Astronomy%20%231.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 027fbc79-cde5-407c-8219-78b52ac028e3
updated: 1486069684
title: 'Introduction to Astronomy- Crash Course Astronomy #1'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/0rHUDWjR5gg/default.jpg
    - https://i3.ytimg.com/vi/0rHUDWjR5gg/1.jpg
    - https://i3.ytimg.com/vi/0rHUDWjR5gg/2.jpg
    - https://i3.ytimg.com/vi/0rHUDWjR5gg/3.jpg
---
