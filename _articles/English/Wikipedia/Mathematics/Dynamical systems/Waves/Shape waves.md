---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shape_waves
offline_file: ""
offline_thumbnail: ""
uuid: b7248984-c52a-4586-b452-74f54d47883e
updated: 1484308894
title: Shape waves
categories:
    - Waves
---
Shape waves are excitations propagating along Josephson vortices or fluxons. In the case of two-dimensional Josephson junctions (thick long Josephson junctions with an extra dimension) described by the 2D sine-Gordon equation, shape waves are distortions of a Josephson vortex line of an arbitrary profile. Shape waves have remarkable properties exhibiting Lorentz contraction and time dilation similar to that in special relativity. Position of the shape wave excitation on a Josephson vortex acts like a “minute-hand” showing the time in the rest-frame associated with the vortex. At some conditions, a moving vortex with the shape excitation can have less energy than the same vortex without ...
