---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transportation
offline_file: ""
offline_thumbnail: ""
uuid: 4765c2cc-89fd-49c9-aaa5-3460ee8b2d46
updated: 1484310249
title: transportation
categories:
    - Dictionary
---
transportation
     n 1: a facility consisting of the means and equipment necessary
          for the movement of passengers or goods [syn: {transportation
          system}, {transit}]
     2: the act of transporting something from one location to
        another [syn: {transfer}, {transferral}, {conveyance}]
     3: the sum charged for riding in a public conveyance [syn: {fare}]
     4: the United States federal department that institutes and
        coordinates national transportation programs; created in
        1966 [syn: {Department of Transportation}, {DoT}]
     5: the commercial enterprise of transporting goods and
        materials [syn: {shipping}, {transport}]
     6: the act of ...
