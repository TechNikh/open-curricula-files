---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acquired
offline_file: ""
offline_thumbnail: ""
uuid: a24c8fc6-7e15-4af5-8696-a35007eafb3f
updated: 1484310293
title: acquired
categories:
    - Dictionary
---
acquired
     adj : gotten through environmental forces; "acquired
           characteristics (such as a suntan or a broken nose)
           cannot be passed on"
