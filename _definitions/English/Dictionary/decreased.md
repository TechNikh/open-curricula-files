---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decreased
offline_file: ""
offline_thumbnail: ""
uuid: 36b44918-8961-499c-b955-7f6dfab44540
updated: 1484310273
title: decreased
categories:
    - Dictionary
---
decreased
     adj : made less in size or amount or degree [syn: {reduced}] [ant:
            {increased}]
