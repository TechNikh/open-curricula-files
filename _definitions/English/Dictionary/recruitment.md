---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recruitment
offline_file: ""
offline_thumbnail: ""
uuid: a2bcd719-dcb9-42da-8a94-4674dc142072
updated: 1484310517
title: recruitment
categories:
    - Dictionary
---
recruitment
     n : the act of getting recruits; enlisting people for the army
         (or for a job or a cause etc.) [syn: {enlisting}]
