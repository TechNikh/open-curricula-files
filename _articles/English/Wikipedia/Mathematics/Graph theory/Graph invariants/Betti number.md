---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Betti_number
offline_file: ""
offline_thumbnail: ""
uuid: 1fbf8504-1a9c-414a-be5c-c1d155168ef3
updated: 1484309414
title: Betti number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Torus_0.png
tags:
    - Definitions
    - 'Example 1: Betti numbers of a simplicial complex K'
    - 'Example 2: the first Betti number in graph theory'
    - Properties
    - Examples
    - Relationship with dimensions of spaces of differential forms
categories:
    - Graph invariants
---
In algebraic topology, the Betti numbers are used to distinguish topological spaces based on the connectivity of n-dimensional simplicial complexes. For the most reasonable finite-dimensional spaces (such as compact manifolds, finite simplicial complexes or CW complexes), the sequence of Betti numbers is 0 from some points onward (Betti numbers vanish above the dimension of a space), and they are all finite.
