---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perception
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524561
title: perception
categories:
    - Dictionary
---
perception
     n 1: the representation of what is perceived; basic component in
          the formation of a concept [syn: {percept}, {perceptual
          experience}]
     2: a way of conceiving something; "Luther had a new perception
        of the Bible"
     3: the process of perceiving
     4: knowledge gained by perceiving; "a man admired for the depth
        of his perception"
     5: becoming aware of something via the senses [syn: {sensing}]
