---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: c26f97c3-e853-492d-b2f7-52eafd7db8c2
updated: 1484309367
title: Möller–Trumbore intersection algorithm
categories:
    - Geometry stubs
---
The Möller–Trumbore ray-triangle intersection algorithm, named after its inventors Tomas Möller and Ben Trumbore, is a fast method for calculating the intersection of a ray and a triangle in three dimensions without needing precomputation of the plane equation of the plane containing the triangle.[1] Among other uses, it can be used in computer graphics to implement ray tracing computations involving triangle meshes.[2]
