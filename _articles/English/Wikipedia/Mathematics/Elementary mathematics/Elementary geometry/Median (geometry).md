---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Median_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: ad45e612-3624-45d3-bbe9-5c6a15deb754
updated: 1484308941
title: Median (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/55px-Icon_Camera.svg_1.png
tags:
    - Relation to center of mass
    - Equal-area division
    - Proof of equal-area property
    - Three congruent triangles
    - "Formulas involving the medians' lengths"
    - Other properties
    - Tetrahedron
categories:
    - Elementary geometry
---
In geometry, a median of a triangle is a line segment joining a vertex to the midpoint of the opposing side. Every triangle has exactly three medians, one from each vertex, and they all intersect each other at the triangle's centroid. In the case of isosceles and equilateral triangles, a median bisects any angle at a vertex whose two adjacent sides are equal in length.
