---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circumference
offline_file: ""
offline_thumbnail: ""
uuid: 4f2c716b-be56-4fd5-b783-5affda339b22
updated: 1484309242
title: Circumference
tags:
    - Circumference of a circle
    - Relationship with Pi
    - Circumference of an ellipse
    - Circumference of a graph
categories:
    - Geometric measurement
---
The circumference (from Latin circumferentia, meaning "carrying around") of a closed curve or circular object is the linear distance around its edge.[1] The circumference of a circle is of special importance in geometry and trigonometry. Informally "circumference" may also refer to the edge itself rather than to the length of the edge. Circumference is a special case of perimeter: the perimeter is the length around any closed figure, but conventionally "perimeter" is typically used in reference to a polygon while "circumference" typically refers to a continuously differentiable curve.
