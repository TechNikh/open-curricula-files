---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissolution
offline_file: ""
offline_thumbnail: ""
uuid: 6ab053f0-d9cf-487a-b80f-5b36d4ea0f92
updated: 1484310411
title: dissolution
categories:
    - Dictionary
---
dissolution
     n 1: the process of going into solution; "the dissolving of salt
          in water" [syn: {dissolving}]
     2: separation into component parts [syn: {disintegration}]
     3: dissolute indulgence in sensual pleasure [syn: {profligacy},
         {dissipation}, {licentiousness}]
     4: the termination of a meeting [syn: {adjournment}]
     5: the termination of a relationship [syn: {breakup}]
