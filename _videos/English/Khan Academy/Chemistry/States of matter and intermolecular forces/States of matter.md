---
version: 1
type: video
provider: YouTube
id: pKvo0XWZtjo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/States%20of%20matter%20and%20intermolecular%20forces/States%20of%20matter.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 902f8a3c-f730-44fb-9766-57f9b61d6e46
updated: 1486069589
title: States of matter
categories:
    - States of matter and intermolecular forces
thumbnail_urls:
    - https://i3.ytimg.com/vi/pKvo0XWZtjo/default.jpg
    - https://i3.ytimg.com/vi/pKvo0XWZtjo/1.jpg
    - https://i3.ytimg.com/vi/pKvo0XWZtjo/2.jpg
    - https://i3.ytimg.com/vi/pKvo0XWZtjo/3.jpg
---
Introduction to the states or phases of matter.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/states-of-matter-and-intermolecular-forces/states-of-matter/v/states-of-matter-follow-up?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/maxwell-boltzmann-distribution?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
