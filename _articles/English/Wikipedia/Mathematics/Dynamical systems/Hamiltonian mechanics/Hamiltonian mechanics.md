---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_mechanics
offline_file: ""
offline_thumbnail: ""
uuid: 3f96d674-3ec8-41ff-8cc6-91ef0c55f498
updated: 1484308787
title: Hamiltonian mechanics
tags:
    - Overview
    - Basic physical interpretation
    - Calculating a Hamiltonian from a Lagrangian
    - "Deriving Hamilton's equations"
    - As a reformulation of Lagrangian mechanics
    - Geometry of Hamiltonian systems
    - Generalization to quantum mechanics through Poisson bracket
    - Mathematical formalism
    - Riemannian manifolds
    - Sub-Riemannian manifolds
    - Poisson algebras
    - Charged particle in an electromagnetic field
    - Relativistic charged particle in an electromagnetic field
    - Footnotes
    - Sources
categories:
    - Hamiltonian mechanics
---
Hamiltonian mechanics is a theory developed as a reformulation of classical mechanics and predicts the same outcomes as non-Hamiltonian classical mechanics. It uses a different mathematical formalism, providing a more abstract understanding of the theory. Historically, it was an important reformulation of classical mechanics, which later contributed to the formulation of statistical mechanics and quantum mechanics.
