---
version: 1
type: video
provider: YouTube
id: i8Wtu-kdDC4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Optimization%20with%20Calculus%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e3e0fd88-ce78-487c-82f1-2d660b7c5eda
updated: 1486069694
title: Optimization with Calculus 3
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/i8Wtu-kdDC4/default.jpg
    - https://i3.ytimg.com/vi/i8Wtu-kdDC4/1.jpg
    - https://i3.ytimg.com/vi/i8Wtu-kdDC4/2.jpg
    - https://i3.ytimg.com/vi/i8Wtu-kdDC4/3.jpg
---
