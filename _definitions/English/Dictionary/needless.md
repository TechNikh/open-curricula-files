---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needless
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557981
title: needless
categories:
    - Dictionary
---
needless
     adj : unnecessary and unwarranted; "a strikers' tent camp...was
           burned with needless loss of life" [syn: {gratuitous},
           {uncalled-for}]
