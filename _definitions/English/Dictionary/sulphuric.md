---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sulphuric
offline_file: ""
offline_thumbnail: ""
uuid: a352089c-d665-49bf-b122-ecc4d48cb277
updated: 1484310373
title: sulphuric
categories:
    - Dictionary
---
sulphuric
     adj : of or relating to or containing sulfur; "sulphuric esters"
           [syn: {sulfuric}]
