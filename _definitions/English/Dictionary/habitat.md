---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/habitat
offline_file: ""
offline_thumbnail: ""
uuid: 928f5b6a-d0fb-40fe-aad1-45809ef1f946
updated: 1484310275
title: habitat
categories:
    - Dictionary
---
habitat
     n : the type of environment in which an organism or group
         normally lives or occurs; "a marine habitat"; "he felt
         safe on his home grounds" [syn: {home ground}]
