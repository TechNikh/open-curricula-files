---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freezing
offline_file: ""
offline_thumbnail: ""
uuid: 7b1e7c52-0142-4339-9a4d-7cf8efb5fe5e
updated: 1484310224
title: freezing
categories:
    - Dictionary
---
freezing
     n : the withdrawal of heat to change something from a liquid to
         a solid [syn: {freeze}]
