---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compensation
offline_file: ""
offline_thumbnail: ""
uuid: 84a7294e-79a9-4e77-8816-c23f4137053e
updated: 1484310457
title: compensation
categories:
    - Dictionary
---
compensation
     n 1: something (such as money) given or received as payment or
          reparation (as for a service or loss or injury)
     2: (psychiatry) a defense mechanism that conceals your
        undesirable shortcomings by exaggerating desirable
        behaviors
     3: the act of compensating for service or loss or injury [syn:
        {recompense}]
