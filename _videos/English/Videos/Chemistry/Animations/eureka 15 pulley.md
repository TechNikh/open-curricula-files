---
version: 1
type: video
provider: YouTube
id: nMJgexvovx0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/eureka%2015%20pulley.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8b1b30c1-9040-4e06-b5b1-12dd1d85840d
updated: 1486069666
title: eureka 15 pulley
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/nMJgexvovx0/default.jpg
    - https://i3.ytimg.com/vi/nMJgexvovx0/1.jpg
    - https://i3.ytimg.com/vi/nMJgexvovx0/2.jpg
    - https://i3.ytimg.com/vi/nMJgexvovx0/3.jpg
---
