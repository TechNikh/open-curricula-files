---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solar_observation
offline_file: ""
offline_thumbnail: ""
uuid: 6bd1841d-ac1d-4e6c-b11f-98abe5f1afe4
updated: 1484308883
title: Solar observation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-ChroniclesofJohnofWorcester.jpg
tags:
    - Prehistory
    - Early observations
    - 17th and 18th centuries
    - 19th century
    - Solar spectroscopy
    - Solar cycle
    - Photography
    - Rotation
    - Space weather
    - 20th century
    - Observatories
    - Coronagraph
    - Spectroheliograph
    - Solar radio bursts
    - Satellites
    - Measurement proxies
    - Other developments
    - 21st century
categories:
    - Vortices
---
Solar observation is the scientific endeavor of studying the Sun and its behavior and relation to the Earth and the remainder of the Solar System. Deliberate solar observation began thousands of years ago. That initial era of direct observation gave way to telescopes in the 1600s followed by satellites in the twentieth century.
