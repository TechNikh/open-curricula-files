---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/century
offline_file: ""
offline_thumbnail: ""
uuid: 22031866-4b39-47cc-8c6b-a13f8e7085db
updated: 1484310309
title: century
categories:
    - Dictionary
---
century
     n 1: 100 years
     2: ten 10s [syn: {hundred}, {100}, {C}, {one C}, {centred}]
