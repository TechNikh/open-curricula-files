---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eventually_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 9aaef19d-ee8a-4255-9642-a8916abb6b7f
updated: 1484308645
title: Eventually (mathematics)
tags:
    - Motivation and definition
    - Notation
    - Other uses in mathematics
    - Examples
categories:
    - Number theory
---
In the mathematical areas of number theory and analysis, an infinite sequence (an) is said eventually to have a certain property if all terms beyond some (finite) point in the sequence have that property. This can be extended to the class of properties P that apply to elements of any ordered set (sequences and subsets of R are ordered, for example).
