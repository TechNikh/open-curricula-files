---
version: 1
type: video
provider: YouTube
id: ItA_hhRtUuw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/More%20advanced%20ratio%20problem-with%20Algebra%20%28HD%20version%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 14e37b8e-871d-4e61-b84a-52d5f9bf1229
updated: 1486069571
title: More advanced ratio problem--with Algebra (HD version)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/ItA_hhRtUuw/default.jpg
    - https://i3.ytimg.com/vi/ItA_hhRtUuw/1.jpg
    - https://i3.ytimg.com/vi/ItA_hhRtUuw/2.jpg
    - https://i3.ytimg.com/vi/ItA_hhRtUuw/3.jpg
---
More advance ratio problem with algebra.
More free lessons at: http://www.khanacademy.org/video?v=ItA_hhRtUuw
