---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resistivity
offline_file: ""
offline_thumbnail: ""
uuid: 4fa4fc80-5529-4cfb-9f7c-32ff6ca1bd58
updated: 1484310198
title: resistivity
categories:
    - Dictionary
---
resistivity
     n : a material's opposition to the flow of electric current;
         measured in ohms [syn: {electric resistance}, {electrical
         resistance}, {impedance}, {resistance}, {ohmic resistance}]
