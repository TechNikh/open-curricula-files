---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flush
offline_file: ""
offline_thumbnail: ""
uuid: f9129f7e-217f-4809-82ae-8d871a221e0a
updated: 1484310377
title: flush
categories:
    - Dictionary
---
flush
     adj 1: of a surface exactly even with an adjoining one, forming the
            same plane; "a door flush with the wall"; "the bottom
            of the window is flush with the floor" [syn: {flush(p)}]
     2: having an abundant supply of money or possessions of value;
        "an affluent banker"; "a speculator flush with cash"; "not
        merely rich but loaded"; "moneyed aristocrats"; "wealthy
        corporations" [syn: {affluent}, {loaded}, {moneyed}, {wealthy}]
     n 1: the period of greatest prosperity or productivity [syn: {flower},
           {prime}, {peak}, {heyday}, {bloom}, {blossom}, {efflorescence}]
     2: a rosy color (especially in the cheeks) taken as a ...
