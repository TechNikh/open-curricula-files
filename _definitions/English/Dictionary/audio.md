---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/audio
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492221
title: audio
categories:
    - Dictionary
---
audio
     n 1: the audible part of a transmitted signal; "they always raise
          the audio for commercials" [syn: {sound}]
     2: an audible acoustic wave frequency [syn: {audio frequency}]
     3: the sound elements of television
