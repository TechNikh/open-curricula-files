---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rooted
offline_file: ""
offline_thumbnail: ""
uuid: 09336b00-fdad-412a-9e02-b91b197e36dd
updated: 1484310181
title: rooted
categories:
    - Dictionary
---
rooted
     adj : absolutely still; "frozen with horror"; "they stood rooted
           in astonishment" [syn: {frozen(p)}, {rooted(p)}, {stock-still}]
