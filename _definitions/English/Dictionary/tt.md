---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tt
offline_file: ""
offline_thumbnail: ""
uuid: b2648640-2403-4746-8c0d-806d0f14b614
updated: 1484310277
title: tt
categories:
    - Dictionary
---
TT
     n 1: (astronomy) a measure of time defined by Earth's orbital
          motion; terrestrial time is mean solar time corrected
          for the irregularities of the Earth's motions [syn: {terrestrial
          time}, {terrestrial dynamical time}, {TDT}, {ephemeris
          time}]
     2: a republic in the western central Pacific Ocean in
        association with the United States [syn: {Palau}, {Republic
        of Palau}]
     3: a country scattered over Micronesia with a constitutional
        government in free association with the United States;
        achieved independence in 1986 [syn: {Micronesia}, {Federated
        States of Micronesia}]
