---
version: 1
type: video
provider: YouTube
id: _o4ScgRZtNI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Op%20Amp%20Circuits-%20Analog%20Computers%20from%20operational%20amplifiers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3a333332-9809-41ec-b75c-e61a58ea0a37
updated: 1486069657
title: 'Op Amp Circuits- Analog Computers from operational amplifiers'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/_o4ScgRZtNI/default.jpg
    - https://i3.ytimg.com/vi/_o4ScgRZtNI/1.jpg
    - https://i3.ytimg.com/vi/_o4ScgRZtNI/2.jpg
    - https://i3.ytimg.com/vi/_o4ScgRZtNI/3.jpg
---
