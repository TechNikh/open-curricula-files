---
version: 1
type: video
provider: YouTube
id: fokb_ts194k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: eb1f4447-14e4-4734-9eaf-9655d0fed1b2
updated: 1486069619
title: Volume of rectangles inside rectangles
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/fokb_ts194k/default.jpg
    - https://i3.ytimg.com/vi/fokb_ts194k/1.jpg
    - https://i3.ytimg.com/vi/fokb_ts194k/2.jpg
    - https://i3.ytimg.com/vi/fokb_ts194k/3.jpg
---
