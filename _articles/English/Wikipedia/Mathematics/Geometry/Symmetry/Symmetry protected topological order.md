---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Symmetry_protected_topological_order
offline_file: ""
offline_thumbnail: ""
uuid: f0126f40-f80d-4fcf-94bd-632ba36c52ff
updated: 1484309321
title: Symmetry protected topological order
tags:
    - Characteristic properties of SPT order
    - Relation between SPT order and (intrinsic) topological order
    - Examples of SPT order
    - Group cohomology theory for SPT phases
    - >
        A complete classification of 1D gapped quantum phases (with
        interactions)
categories:
    - Symmetry
---
Symmetry Protected Topological order (SPT order)[1] is a kind of order in zero-temperature quantum-mechanical states of matter that have a symmetry and a finite energy gap.
