---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/basin
offline_file: ""
offline_thumbnail: ""
uuid: 915726c3-0865-4cdc-977c-ac44330c55e1
updated: 1484310305
title: basin
categories:
    - Dictionary
---
basin
     n 1: a bowl-shaped vessel; usually used for holding food or
          liquids; "she mixed the dough in a large basin"
     2: the quantity that a basin will hold; "a basinful of water"
        [syn: {basinful}]
     3: a natural depression in the surface of the land often with a
        lake at the bottom of it; "the basin of the Great Salt
        Lake"
     4: the entire geographical area drained by a river and its
        tributaries; "flood control in the Missouri basin" [syn: {river
        basin}]
     5: a bathroom or lavatory sink that is permanently installed
        and connected to a water supply and drainpipe; where you
        wash your hands and face; "he ran some ...
