---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helium
offline_file: ""
offline_thumbnail: ""
uuid: ef444a87-8785-4da6-bf16-e51740de3718
updated: 1484310391
title: helium
categories:
    - Dictionary
---
helium
     n : a very light colorless element that is one of the six inert
         gasses; the most difficult gas to liquefy; occurs in
         economically extractable amounts in certain natural gases
         (as those found in Texas and Kansas) [syn: {He}, {atomic
         number 2}]
