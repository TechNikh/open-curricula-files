---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gale%E2%80%93Ryser_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 559bd399-8d5d-4cd6-ba49-ac52efaee7de
updated: 1484309457
title: Gale–Ryser theorem
tags:
    - Theorem statement
    - Remark
    - Other notations
    - Proofs
    - Stronger version
    - Generalization
    - Characterizations for similar problems
    - Notes
categories:
    - Theorems in graph theory
---
The Gale–Ryser theorem is a result in graph theory and combinatorial matrix theory, two branches of combinatorics. It provides one of two known approaches to solving the bipartite realization problem, i.e. it gives a necessary and sufficient condition for two finite sequences of natural numbers to be the degree sequence of a labeled simple bipartite graph; a sequence obeying these conditions is called "bigraphic". It is an analog of the Erdős–Gallai theorem for simple graphs. The theorem was published in 1957 by H. J. Ryser and also by David Gale.
