---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deviate
offline_file: ""
offline_thumbnail: ""
uuid: 1837e6dd-c479-4978-a51f-ab91b161fd89
updated: 1484310210
title: deviate
categories:
    - Dictionary
---
deviate
     n : a person whose behavior deviates from what is acceptable
         especially in sexual behavior [syn: {pervert}, {deviant},
          {degenerate}]
     v 1: be at variance with; be out of line with [syn: {vary}, {diverge},
           {depart}] [ant: {conform}]
     2: turn aside; turn away from [syn: {divert}]
     3: cause to turn away from a previous or expected course; "The
        river was deviated to prevent flooding"
     4: turn aside [syn: {deflect}]
