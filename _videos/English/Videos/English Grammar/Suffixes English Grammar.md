---
version: 1
type: video
provider: YouTube
id: pYdIjc85tRc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Suffixes%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5f82120a-3e0f-4149-983c-af6028e5af96
updated: 1486069631
title: Suffixes English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/pYdIjc85tRc/default.jpg
    - https://i3.ytimg.com/vi/pYdIjc85tRc/1.jpg
    - https://i3.ytimg.com/vi/pYdIjc85tRc/2.jpg
    - https://i3.ytimg.com/vi/pYdIjc85tRc/3.jpg
---
