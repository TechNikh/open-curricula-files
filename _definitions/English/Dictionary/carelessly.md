---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carelessly
offline_file: ""
offline_thumbnail: ""
uuid: f6fd926a-efd8-4420-8b24-a87a9cbe6636
updated: 1484310249
title: carelessly
categories:
    - Dictionary
---
carelessly
     adv 1: without care or concern; "carelessly raised the children's
            hopes without thinking of their possible
            disappointment" [syn: {heedlessly}]
     2: without caution or prudence; "one unfortunately sees
        historic features carelessly lost when estates fall into
        unsympathetic hands" [syn: {incautiously}] [ant: {cautiously},
         {cautiously}]
     3: in a rakish manner; "she wore her hat rakishly at an angle"
        [syn: {rakishly}, {raffishly}]
