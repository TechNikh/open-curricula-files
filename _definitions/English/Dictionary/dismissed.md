---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dismissed
offline_file: ""
offline_thumbnail: ""
uuid: 21a71801-eaf6-48ac-9b9f-98e912a7247b
updated: 1484310585
title: dismissed
categories:
    - Dictionary
---
dismissed
     adj : having lost your job [syn: {discharged}, {fired}, {laid-off},
            {pink-slipped}]
