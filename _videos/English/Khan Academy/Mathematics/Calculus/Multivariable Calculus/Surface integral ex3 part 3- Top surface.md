---
version: 1
type: video
provider: YouTube
id: wiVWafc3JBM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20ex3%20part%203-%20Top%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 17918758-e9bc-44ed-956a-7faa7f741acf
updated: 1486069690
title: 'Surface integral ex3 part 3- Top surface'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/wiVWafc3JBM/default.jpg
    - https://i3.ytimg.com/vi/wiVWafc3JBM/1.jpg
    - https://i3.ytimg.com/vi/wiVWafc3JBM/2.jpg
    - https://i3.ytimg.com/vi/wiVWafc3JBM/3.jpg
---
