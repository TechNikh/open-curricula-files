---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/avoid
offline_file: ""
offline_thumbnail: ""
uuid: c2c01e25-a2a0-4428-a9d7-3d84c1a51e66
updated: 1484310383
title: avoid
categories:
    - Dictionary
---
avoid
     v 1: stay clear from; keep away from; keep out of the way of
          someone or something; "Her former friends now avoid her"
          [ant: {confront}]
     2: prevent the occurrence of; prevent from happening; "Let's
        avoid a confrontation"; "head off a confrontation"; "avert
        a strike" [syn: {debar}, {obviate}, {deflect}, {avert}, {head
        off}, {stave off}, {fend off}, {ward off}]
     3: refrain from doing something; "She refrains from calling her
        therapist too often"; "He should avoid publishing his
        wife's memoires"
     4: refrain from certain foods or beverages; "I keep off drugs";
        "During Ramadan, Muslims avoid tobacco during ...
