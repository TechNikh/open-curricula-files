---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perceived
offline_file: ""
offline_thumbnail: ""
uuid: 30f02f34-36e3-4e6d-8ac6-c86ffa8c55cf
updated: 1484310192
title: perceived
categories:
    - Dictionary
---
perceived
     adj 1: detected by instinct or inference rather than by recognized
            perceptual cues; "the felt presence of an intruder";
            "a sensed presence in the room raised goosebumps on
            her arms"; "a perceived threat" [syn: {sensed}]
     2: detected by means of the senses; "a perceived difference in
        temperature"
