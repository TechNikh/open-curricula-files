---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chalk
offline_file: ""
offline_thumbnail: ""
uuid: a6749673-6b1d-4664-b5df-e593892f38ca
updated: 1484310343
title: chalk
categories:
    - Dictionary
---
chalk
     n 1: a soft whitish calcite
     2: a pure flat white with little reflectance
     3: amphetamine used in the form of a crystalline hydrochloride;
        used as a stimulant to the nervous system and as an
        appetite suppressant [syn: {methamphetamine}, {methamphetamine
        hydrochloride}, {Methedrine}, {meth}, {deoxyephedrine}, {chicken
        feed}, {crank}, {glass}, {ice}, {shabu}, {trash}]
     4: a piece of chalk (or similar substance) used for writing on
        blackboards or other surfaces
     v : write, draw, or trace with chalk
