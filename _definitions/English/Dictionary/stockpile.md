---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stockpile
offline_file: ""
offline_thumbnail: ""
uuid: 251f6e49-1669-4dd4-96ce-c8440bee4577
updated: 1484310176
title: stockpile
categories:
    - Dictionary
---
stockpile
     n 1: something kept back or saved for future use or a special
          purpose [syn: {reserve}, {backlog}]
     2: a storage pile accumulated for future use
     v : have on hand; "Do you carry kerosene heaters?" [syn: {stock},
          {carry}]
