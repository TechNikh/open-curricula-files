---
version: 1
type: video
provider: YouTube
id: NjgBnx1jVIU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Inside%20the%20Brain-%20Unraveling%20the%20Mystery%20of%20Alzheimer%27s%20Disease%20%5BHQ%5D.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 82c76920-be56-45a0-95e3-3581e569e3b0
updated: 1486069669
title: "Inside the Brain- Unraveling the Mystery of Alzheimer's Disease [HQ]"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/NjgBnx1jVIU/default.jpg
    - https://i3.ytimg.com/vi/NjgBnx1jVIU/1.jpg
    - https://i3.ytimg.com/vi/NjgBnx1jVIU/2.jpg
    - https://i3.ytimg.com/vi/NjgBnx1jVIU/3.jpg
---
