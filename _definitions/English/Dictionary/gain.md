---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gain
offline_file: ""
offline_thumbnail: ""
uuid: 2c37900c-5648-4045-8f8f-de6da26d6b16
updated: 1484310260
title: gain
categories:
    - Dictionary
---
gain
     n 1: a quantity that is added; "there was an addition to property
          taxes this year"; "they recorded the cattle's gain in
          weight over a period of weeks" [syn: {addition}, {increase}]
     2: the advantageous quality of being beneficial [syn: {profit}]
     3: the amount of increase in signal power or voltage or current
        expressed as the ratio of output to input [syn: {amplification}]
     4: the amount by which the revenue of a business exceeds its
        cost of operating [ant: {loss}]
     v 1: obtain; "derive pleasure from one's garden" [syn: {derive}]
     2: win something through one's efforts; "I acquired a passing
        knowledge of Chinese"; ...
