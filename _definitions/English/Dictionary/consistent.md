---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consistent
offline_file: ""
offline_thumbnail: ""
uuid: dbe421a5-6253-4d13-b3b6-fcfd4003855a
updated: 1484310264
title: consistent
categories:
    - Dictionary
---
consistent
     adj 1: (sometimes followed by `with') in agreement or consistent or
            reliable; "testimony consistent with the known facts";
            "I have decided that the course of conduct which I am
            following is consistent with my sense of
            responsibility as president in time of war"- FDR [ant:
             {inconsistent}]
     2: marked by an orderly, logical, and aesthetically consistent
        relation of parts; "a logical argument"; "the orderly
        presentation" [syn: {logical}, {ordered}, {orderly}]
     3: capable of being reproduced; "astonishingly reproducible
        results can be obtained" [syn: {reproducible}] [ant: ...
