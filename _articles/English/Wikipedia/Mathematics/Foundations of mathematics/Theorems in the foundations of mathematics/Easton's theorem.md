---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Easton%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 775dc586-012e-40b3-889b-4ffbdcc29bc0
updated: 1484308999
title: "Easton's theorem"
tags:
    - Statement of the theorem
    - No extension to singular cardinals
categories:
    - Theorems in the foundations of mathematics
---
In set theory, Easton's theorem is a result on the possible cardinal numbers of powersets. Easton (1970) (extending a result of Robert M. Solovay) showed via forcing that
