---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strikingly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556301
title: strikingly
categories:
    - Dictionary
---
strikingly
     adv : in a striking manner; "this was strikingly demonstrated";
           "the evidence was strikingly absent"
