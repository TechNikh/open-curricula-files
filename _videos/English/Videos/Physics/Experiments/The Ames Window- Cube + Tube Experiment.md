---
version: 1
type: video
provider: YouTube
id: Fh-l19dxC2s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a6e2cebb-0210-402c-b0a0-a03669b95a9d
updated: 1486069637
title: 'The Ames Window- Cube + Tube Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Fh-l19dxC2s/default.jpg
    - https://i3.ytimg.com/vi/Fh-l19dxC2s/1.jpg
    - https://i3.ytimg.com/vi/Fh-l19dxC2s/2.jpg
    - https://i3.ytimg.com/vi/Fh-l19dxC2s/3.jpg
---
