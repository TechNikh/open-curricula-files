---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metal
offline_file: ""
offline_thumbnail: ""
uuid: b0cd61c1-97ce-4f2b-aea9-dfed02332035
updated: 1484310232
title: metal
categories:
    - Dictionary
---
metal
     adj : containing or made of or resembling or characteristic of a
           metal; "a metallic compound"; "metallic luster"; "the
           strange metallic note of the meadow lark, suggesting
           the clash of vibrant blades"- Ambrose Bierce [syn: {metallic},
            {metal(a)}] [ant: {nonmetallic}]
     n 1: any of several chemical elements that are usually shiny
          solids that conduct heat or electricity and can be
          formed into sheets etc. [syn: {metallic element}]
     2: a mixture containing two or more metallic elements or
        metallic and nonmetallic elements usually fused together
        or dissolving into each other when molten; "brass is ...
