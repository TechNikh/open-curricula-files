---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Hadwiger_conjecture_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: fde330de-8c83-4620-85e6-31e0cf572199
updated: 1484309382
title: Hadwiger conjecture (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Hadwiger_conjecture.svg.png
tags:
    - Equivalent forms
    - Special cases and partial results
    - Generalizations
    - Notes
categories:
    - Graph coloring
---
In graph theory, the Hadwiger conjecture (or Hadwiger's conjecture) states that, if all proper colorings of an undirected graph G use k or more colors, then one can find k disjoint connected subgraphs of G such that each subgraph is connected by an edge to each other subgraph. Contracting the edges within each of these subgraphs so that each subgraph collapses to a single vertex produces a complete graph Kk on k vertices as a minor of G.
