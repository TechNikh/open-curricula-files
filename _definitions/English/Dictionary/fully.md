---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fully
offline_file: ""
offline_thumbnail: ""
uuid: 612404c0-094e-4dbb-8073-d921ea734956
updated: 1484310264
title: fully
categories:
    - Dictionary
---
fully
     adv 1: to the greatest degree or extent; completely or entirely;
            (`full' in this sense is used as a combining form);
            "fully grown"; "he didn't fully understand"; "knew
            full well"; "full-grown"; "full-fledged" [syn: {to the
            full}, {full}]
     2: sufficiently; more than adequately; "the evidence amply (or
        fully) confirms our suspicions"; "they were fully (or
        amply) fed" [syn: {amply}] [ant: {meagerly}]
     3: referring to a quantity; "the amount was paid in full" [syn:
         {in full}]
