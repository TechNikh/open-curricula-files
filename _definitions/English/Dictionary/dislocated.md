---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dislocated
offline_file: ""
offline_thumbnail: ""
uuid: ffaea8c6-7170-4a43-884c-9a7ea1cc5766
updated: 1484310551
title: dislocated
categories:
    - Dictionary
---
dislocated
     adj : separated at the joint; "a dislocated knee"; "a separated
           shoulder" [syn: {disjointed}, {separated}]
