---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earlier
offline_file: ""
offline_thumbnail: ""
uuid: 6ee0d29d-b320-4d39-a703-d5f640e35000
updated: 1484310343
title: earlier
categories:
    - Dictionary
---
early
     adj 1: at or near the beginning of a period of time or course of
            events or before the usual or expected time; "early
            morning"; "an early warning"; "early diagnosis"; "an
            early death"; "took early retirement"; "an early
            spring"; "early varieties of peas and tomatoes mature
            before most standard varieties" [ant: {middle}, {late}]
     2: being or occurring at an early stage of development; "in an
        early stage"; "early forms of life"; "early man"; "an
        early computer" [ant: {late}]
     3: of the distant past; "the early inhabitants of Europe";
        "former generations"; "in other times" [syn: {early(a)}, ...
