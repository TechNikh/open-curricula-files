---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Volume_entropy
offline_file: ""
offline_thumbnail: ""
uuid: df0e2938-c21d-40cc-b6b1-c5e2443ae108
updated: 1484308764
title: Volume entropy
tags:
    - Definition
    - Properties
    - Application in differential geometry of surfaces
categories:
    - Entropy
---
The volume entropy is an asymptotic invariant of a compact Riemannian manifold that measures the exponential growth rate of the volume of metric balls in its universal cover. This concept is closely related with other notions of entropy found in dynamical systems and plays an important role in differential geometry and geometric group theory. If the manifold is nonpositively curved then its volume entropy coincides with the topological entropy of the geodesic flow. It is of considerable interest in differential geometry to find the Riemannian metric on a given smooth manifold which minimizes the volume entropy, with locally symmetric spaces forming a basic class of examples.
