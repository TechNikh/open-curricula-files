---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homogenous
offline_file: ""
offline_thumbnail: ""
uuid: 5c1103ae-98e8-4b3c-b4b1-4195ac35dd07
updated: 1484308409
title: Homogeneity and heterogeneity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-The_FireHouse_Grill_-_Lunch.jpg
tags:
    - Etymology and spelling
    - Scaling
    - Examples
    - chemistry
    - Homogeneous and heterogeneous reactions
    - Geology
    - Information technology
    - Mathematics and statistics
    - Medicine
    - Physics
    - Sociology
categories:
    - Uncategorized
---
Homogeneity and heterogeneity are concepts often used in the sciences and statistics relating to the uniformity in a substance or organism. A material or image that is homogeneous is uniform in composition or character (i.e. color, shape, size, weight, height, distribution, texture, language, income, disease, temperature, radioactivity, architectural design, etc.); one that is heterogeneous is distinctly nonuniform in one of these qualities.[1][2]
