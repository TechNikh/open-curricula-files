---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regeneration
offline_file: ""
offline_thumbnail: ""
uuid: 4255bc30-a43d-45b0-ba4f-5a5ee4defb4b
updated: 1484310249
title: regeneration
categories:
    - Dictionary
---
regeneration
     n 1: (biology) growth anew of lost tissue or destroyed parts or
          organs
     2: feedback in phase with (augmenting) the input [syn: {positive
        feedback}]
     3: the activity of spiritual or physical renewal
     4: forming again (especially with improvements or removal of
        defects); renewing and reconstituting [syn: {re-formation}]
