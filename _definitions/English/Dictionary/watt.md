---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watt
offline_file: ""
offline_thumbnail: ""
uuid: 5d2a991b-5159-4c86-aaf4-90b3615cdd79
updated: 1484310196
title: watt
categories:
    - Dictionary
---
watt
     n 1: a unit of power equal to 1 joule per second; the power
          dissipated by a current of 1 ampere flowing across a
          resistance of 1 ohm [syn: {W}]
     2: Scottish engineer and inventor whose improvements in the
        steam engine led to its wide use in industry (1736-1819)
        [syn: {James Watt}]
