---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Order-2_apeirogonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: eb70730e-49b3-46c4-87d8-c5aa671bc07c
updated: 1484309367
title: Order-2 apeirogonal tiling
tags:
    - Related tilings and polyhedra
    - Notes
categories:
    - Geometry stubs
---
In geometry, an order-2 apeirogonal tiling, apeirogonal dihedron, or infinite dihedron[1] is a tiling of the plane consisting of two apeirogons. It may be considered an improper regular tiling of the Euclidean plane, with Schläfli symbol {∞, 2}. Two apeirogons, joined along all their edges, can completely fill the entire plane as an apeirogon is infinite in size and has an interior angle of 180°, which is half of a full 360°.
