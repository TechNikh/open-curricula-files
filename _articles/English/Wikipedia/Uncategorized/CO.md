---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Co
offline_file: ""
offline_thumbnail: ""
uuid: 6b0af001-8b57-4972-863d-02600b805482
updated: 1484308335
title: CO
tags:
    - Mathematics, science, and technology
    - Biology and medicine
    - chemistry
    - Electronics and computing
    - Other uses in mathematics, science, and technology
    - Places
    - Ranks and titles
    - Transport
    - Other uses
categories:
    - Uncategorized
---
CO, Co, co, .co, c/o ℅ may refer to:
