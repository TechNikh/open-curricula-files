---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jacobi_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: e050a1a1-8176-4700-880b-49d045dde736
updated: 1484308790
title: Jacobi coordinates
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Two-body_Jacobi_coordinates.JPG
categories:
    - Hamiltonian mechanics
---
In the theory of many-particle systems, Jacobi coordinates often are used to simplify the mathematical formulation. These coordinates are particularly common in treating polyatomic molecules and chemical reactions,[3] and in celestial mechanics.[4] An algorithm for generating the Jacobi coordinates for N bodies may be based upon binary trees.[5] In words, the algorithm is described as follows:[5]
