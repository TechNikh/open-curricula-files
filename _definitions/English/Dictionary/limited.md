---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limited
offline_file: ""
offline_thumbnail: ""
uuid: 1ec72d78-e3f8-4a37-b1b3-e5476ff41b32
updated: 1484310264
title: limited
categories:
    - Dictionary
---
limited
     adj 1: small in range or scope; "limited war"; "a limited success";
            "a limited circle of friends" [ant: {unlimited}]
     2: subject to limits or subjected to limits [syn: {circumscribed}]
     3: including only a part
     4: mediocre [syn: {modified}]
     5: not excessive
     6: having a specific function or scope; "a special (or
        specific) role in the mission" [syn: {special}]
     7: not unlimited; "a limited list of choices"
