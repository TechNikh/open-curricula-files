---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/settlement
offline_file: ""
offline_thumbnail: ""
uuid: c5db169f-8018-4e0f-a383-59abb94c37ab
updated: 1484310437
title: settlement
categories:
    - Dictionary
---
settlement
     n 1: a body of people who settle far from home but maintain ties
          with their homeland; inhabitants remain nationals of
          their home state but are not literally under the home
          state's system of government [syn: {colony}]
     2: a community of people smaller than a town [syn: {village}, {small
        town}]
     3: a conclusive resolution of a matter and disposition of it
     4: the act of colonizing; the establishment of colonies; "the
        British colonization of America" [syn: {colonization}, {colonisation}]
     5: something settled or resolved; the outcome of decision
        making; "the finally reached a settlement with the union";
      ...
