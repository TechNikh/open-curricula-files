---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adiabatic_accessibility
offline_file: ""
offline_thumbnail: ""
uuid: 436b0cb7-ef73-4116-96dd-36734a643c7f
updated: 1484308842
title: Adiabatic accessibility
categories:
    - Thermodynamics
---
Adiabatic accessibility denotes a certain relation between two equilibrium states of a thermodynamic system (or of different such systems). The concept was coined by Constantin Carathéodory[1] in 1909 ("adiabatische Erreichbarkeit") and taken up 90 years later by Elliott Lieb and J. Yngvason in their axiomatic approach to the foundations of thermodynamics.[2] [3] It was also used by R. Giles in his 1964 monograph.[4]
