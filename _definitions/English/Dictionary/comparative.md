---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comparative
offline_file: ""
offline_thumbnail: ""
uuid: cb284cc1-4508-4ddb-aeb5-5b8460dd3805
updated: 1484310266
title: comparative
categories:
    - Dictionary
---
comparative
     adj 1: relating to or based on or involving comparison;
            "comparative linguistics"
     2: having significance only in relation to something else; "a
        comparative newcomer"
     n : the comparative form of an adjective; "`better' is the
         comparative of `good'"
