---
version: 1
type: video
provider: YouTube
id: 4tctB1wZNiI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Magnetic%20force%20between%20two%20currents%20going%20in%20opposite%20directions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a9a165c9-4e2e-4dd1-bc2a-5e614931f8ca
updated: 1486069593
title: >
    Magnetic force between two currents going in opposite
    directions
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/4tctB1wZNiI/default.jpg
    - https://i3.ytimg.com/vi/4tctB1wZNiI/1.jpg
    - https://i3.ytimg.com/vi/4tctB1wZNiI/2.jpg
    - https://i3.ytimg.com/vi/4tctB1wZNiI/3.jpg
---
Sal shows how to determine the magnetic force between two currents going in opposite directions. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/magnetic-field-current-carrying-wire/v/magnetism-12-induced-current-in-a-wire?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/magnetic-field-current-carrying-wire/v/magnetism-7?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
