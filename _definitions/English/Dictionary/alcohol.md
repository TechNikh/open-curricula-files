---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alcohol
offline_file: ""
offline_thumbnail: ""
uuid: 63631487-507a-42ac-8a6a-7bd3b5df9582
updated: 1484310381
title: alcohol
categories:
    - Dictionary
---
alcohol
     n 1: a liquor or brew containing alcohol as the active agent;
          "alcohol (or drink) ruined him" [syn: {alcoholic
          beverage}, {intoxicant}, {inebriant}]
     2: any of a series of volatile hydroxyl compounds that are made
        from hydrocarbons by distillation
