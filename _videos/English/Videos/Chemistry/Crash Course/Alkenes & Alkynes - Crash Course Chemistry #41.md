---
version: 1
type: video
provider: YouTube
id: CEH3O6l1pbw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Alkenes%20%26%20Alkynes%20-%20Crash%20Course%20Chemistry%20%2341.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0514c10f-a257-4ef1-96e8-20137077709a
updated: 1486069680
title: 'Alkenes & Alkynes - Crash Course Chemistry #41'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/CEH3O6l1pbw/default.jpg
    - https://i3.ytimg.com/vi/CEH3O6l1pbw/1.jpg
    - https://i3.ytimg.com/vi/CEH3O6l1pbw/2.jpg
    - https://i3.ytimg.com/vi/CEH3O6l1pbw/3.jpg
---
