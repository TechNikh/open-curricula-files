---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fluorine
offline_file: ""
offline_thumbnail: ""
uuid: 7991a344-5c3d-4be4-b626-d7679e7acb18
updated: 1484310397
title: fluorine
categories:
    - Dictionary
---
fluorine
     n : a nonmetallic univalent element belonging to the halogens;
         usually a yellow irritating toxic flammable gas; a
         powerful oxidizing agent; recovered from fluorite or
         cryolite or fluorapatite [syn: {F}, {atomic number 9}]
