---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perplexity
offline_file: ""
offline_thumbnail: ""
uuid: f94b4fec-65b1-49f0-8fa6-ceda0ac21505
updated: 1484308773
title: Perplexity
tags:
    - Perplexity of a probability distribution
    - Perplexity of a probability model
    - Perplexity per word
categories:
    - Entropy and information
---
In information theory, perplexity is a measurement of how well a probability distribution or probability model predicts a sample. It may be used to compare probability models. A low perplexity indicates the probability distribution is good at predicting the sample.
