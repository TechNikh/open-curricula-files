---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supporting
offline_file: ""
offline_thumbnail: ""
uuid: 9a0f0577-0547-40a9-a00f-292989b788fa
updated: 1484310389
title: supporting
categories:
    - Dictionary
---
supporting
     adj 1: furnishing support and encouragement; "the anxious child
            needs supporting and accepting treatment from the
            teacher" [syn: {encouraging}]
     2: capable of bearing a structural load; "a supporting wall"
        [syn: {load-bearing(a)}, {supporting(a)}]
     n : the act of bearing the weight of or strengthening; "he
         leaned against the wall for support" [syn: {support}]
