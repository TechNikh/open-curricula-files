---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tsallis_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 34c9296b-27a1-4c6e-9c43-5c8477fc3eec
updated: 1484308773
title: Tsallis entropy
tags:
    - Various relationships
    - Non-additivity
    - Exponential families
    - Generalised entropies
categories:
    - Entropy and information
---
In physics, the Tsallis entropy is a generalization of the standard Boltzmann–Gibbs entropy. It was introduced in 1988 by Constantino Tsallis[1] as a basis for generalizing the standard statistical mechanics, and is identical in form to Havrda–Charvát structural α-entropy[2][3] within Information Theory. In the scientific literature, the physical relevance of the Tsallis entropy has been debated.[4][5][6] However, from the years 2000 on, an increasingly wide spectrum of natural, artificial and social complex systems have been identified which confirm the predictions and consequences that are derived from this nonadditive entropy, such as nonextensive statistical mechanics,[7] which ...
