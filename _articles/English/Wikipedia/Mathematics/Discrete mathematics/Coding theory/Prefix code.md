---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prefix_code
offline_file: ""
offline_thumbnail: ""
uuid: 1699d5be-a09f-40c9-a245-c0ebf9788ef7
updated: 1484308567
title: Prefix code
tags:
    - Techniques
    - Related concepts
    - Prefix codes in use today
    - Techniques
    - Notes
categories:
    - Coding theory
---
A prefix code is a type of code system (typically a variable-length code) distinguished by its possession of the "prefix property", which requires that there is no whole code word in the system that is a prefix (initial segment) of any other code word in the system. For example, a code with code words {9, 55} has the prefix property; a code consisting of {9, 5, 59, 55} does not, because "5" is a prefix of "59" and also of "55". A prefix code is a uniquely decodable code: a receiver can identify each word without requiring a special marker between words.
