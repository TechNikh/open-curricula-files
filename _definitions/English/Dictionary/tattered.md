---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tattered
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596981
title: tattered
categories:
    - Dictionary
---
tattered
     adj 1: worn to shreds; or wearing torn or ragged clothing; "a man
            in a tattered shirt"; "the tattered flag"; "tied up in
            tattered brown paper"; "a tattered barefoot boy"; "a
            tatterdemalion prince" [syn: {tatterdemalion}]
     2: ruined or disrupted; "our shattered dreams of peace and
        prosperity"; "a tattered remnant of its former strength";
        "my torn and tattered past" [syn: {shattered}]
