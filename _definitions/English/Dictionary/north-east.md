---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/north-east
offline_file: ""
offline_thumbnail: ""
uuid: 09c00b42-ca23-4b6d-b81f-88df7676d053
updated: 1484310433
title: north-east
categories:
    - Dictionary
---
north-east
     adv : to, toward, or in the northeast [syn: {northeast}, {nor'-east}]
