---
version: 1
type: article
id: https://en.wikipedia.org/wiki/S._B._Rao
offline_file: ""
offline_thumbnail: ""
uuid: 868fa7d1-a0d0-4c85-87de-ff206a61a728
updated: 1484309475
title: S. B. Rao
categories:
    - Graph theorists
---
Siddani Bhaskara Rao a Graph theorist was a professor and director of the Indian Statistical Institute (ISI), Calcutta. Rao is the first director of the CR Rao Advanced Institute of Mathematics, Statistics and Computer Science.[2] S. B. Rao is known for his work on line graphs, frequency partitions and degree sequences.
