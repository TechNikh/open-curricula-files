---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/test
offline_file: ""
offline_thumbnail: ""
uuid: 6279b187-11d1-4add-a1dc-bdd33f11c5e3
updated: 1484310344
title: test
categories:
    - Dictionary
---
test
     n 1: any standardized procedure for measuring sensitivity or
          memory or intelligence or aptitude or personality etc;
          "the test was standardized on a large sample of
          students" [syn: {mental test}, {mental testing}, {psychometric
          test}]
     2: the act of testing something; "in the experimental trials
        the amount of carbon was measured separately"; "he called
        each flip of the coin a new trial" [syn: {trial}, {run}]
     3: the act of undergoing testing; "he survived the great test
        of battle"; "candidates must compete in a trial of skill"
        [syn: {trial}]
     4: trying something to find out about it; "a sample for ...
