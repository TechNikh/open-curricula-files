---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/male
offline_file: ""
offline_thumbnail: ""
uuid: 1e423bdf-bd16-4913-999f-83f800e407d9
updated: 1484310297
title: male
categories:
    - Dictionary
---
male
     adj 1: being the sex (of plant or animal) that produces gametes
            (spermatozoa) that perform the fertilizing function in
            generation; "a male infant"; "a male holly tree" [ant:
             {female}, {androgynous}]
     2: for or composed of men or boys; "the male lead"; "masculine
        attire"
     3: characteristic of a man; "a deep male voice"; "manly sports"
        [syn: {manful}, {manlike}, {manly}, {virile}]
     n 1: an animal that produces gametes (spermatozoa) that can
          fertilize female gametes (ova) [ant: {female}]
     2: a person who belongs to the sex that cannot have babies
        [syn: {male person}] [ant: {female}]
     3: the ...
