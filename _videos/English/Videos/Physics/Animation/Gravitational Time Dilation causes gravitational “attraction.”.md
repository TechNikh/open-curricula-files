---
version: 1
type: video
provider: YouTube
id: gcvq1DAM-DE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Gravitational%20Time%20Dilation%20causes%20gravitational%20%E2%80%9Cattraction.%E2%80%9D.webm"
offline_file: ""
offline_thumbnail: ""
uuid: baad0ed8-4e61-4851-a870-cb6f60c0d074
updated: 1486069654
title: >
    Gravitational Time Dilation causes gravitational
    “attraction.”
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/gcvq1DAM-DE/default.jpg
    - https://i3.ytimg.com/vi/gcvq1DAM-DE/1.jpg
    - https://i3.ytimg.com/vi/gcvq1DAM-DE/2.jpg
    - https://i3.ytimg.com/vi/gcvq1DAM-DE/3.jpg
---
