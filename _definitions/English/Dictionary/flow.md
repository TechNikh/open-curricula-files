---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flow
offline_file: ""
offline_thumbnail: ""
uuid: 43c3d284-77aa-47bc-94db-2a479b17a434
updated: 1484310366
title: flow
categories:
    - Dictionary
---
flow
     n 1: the motion characteristic of fluids (liquids or gases) [syn:
           {flowing}]
     2: the amount of fluid that flows in a given time [syn: {flow
        rate}, {rate of flow}]
     3: the act of flowing or streaming; continuous progression
        [syn: {stream}]
     4: any uninterrupted stream or discharge
     5: something that resembles a flowing stream in moving
        continuously; "a stream of people emptied from the
        terminal"; "the museum had planned carefully for the flow
        of visitors" [syn: {stream}]
     6: dominant course (suggestive of running water) of successive
        events or ideas; "two streams of development run through
        ...
