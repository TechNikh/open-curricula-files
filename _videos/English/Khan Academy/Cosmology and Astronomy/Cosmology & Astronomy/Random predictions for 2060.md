---
version: 1
type: video
provider: YouTube
id: E4rbS0oxg30
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Random%20predictions%20for%202060.webm"
offline_file: ""
offline_thumbnail: ""
uuid: acc53fc4-0819-4598-a584-e1d02f78cca5
updated: 1486069686
title: Random predictions for 2060
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/E4rbS0oxg30/default.jpg
    - https://i3.ytimg.com/vi/E4rbS0oxg30/1.jpg
    - https://i3.ytimg.com/vi/E4rbS0oxg30/2.jpg
    - https://i3.ytimg.com/vi/E4rbS0oxg30/3.jpg
---
