---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delta
offline_file: ""
offline_thumbnail: ""
uuid: e964c6f1-79a8-4ed4-817b-ac0e5155f02c
updated: 1484310439
title: delta
categories:
    - Dictionary
---
delta
     n 1: a low triangular area where a river divides before entering
          a larger body of water
     2: the 4th letter of the Greek alphabet
