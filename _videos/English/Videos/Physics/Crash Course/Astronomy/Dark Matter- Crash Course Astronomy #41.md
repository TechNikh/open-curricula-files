---
version: 1
type: video
provider: YouTube
id: 9W3RsaWuCuE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Dark%20Matter-%20Crash%20Course%20Astronomy%20%2341.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92b8f6ac-c06f-425b-96f4-3097a35b84e5
updated: 1486069684
title: 'Dark Matter- Crash Course Astronomy #41'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/9W3RsaWuCuE/default.jpg
    - https://i3.ytimg.com/vi/9W3RsaWuCuE/1.jpg
    - https://i3.ytimg.com/vi/9W3RsaWuCuE/2.jpg
    - https://i3.ytimg.com/vi/9W3RsaWuCuE/3.jpg
---
