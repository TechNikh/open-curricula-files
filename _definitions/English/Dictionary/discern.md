---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discern
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484643121
title: discern
categories:
    - Dictionary
---
discern
     v : detect with the senses; "The fleeing convicts were picked
         out of the darkness by the watchful prison guards"; "I
         can't make out the faces in this photograph" [syn: {recognize},
          {recognise}, {distinguish}, {pick out}, {make out}, {tell
         apart}]
