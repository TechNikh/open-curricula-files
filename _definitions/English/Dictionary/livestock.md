---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/livestock
offline_file: ""
offline_thumbnail: ""
uuid: 10b55512-2a40-4cf5-9da7-d1550d2c962e
updated: 1484310464
title: livestock
categories:
    - Dictionary
---
livestock
     n : not used technically; any animals kept for use or profit
         [syn: {stock}, {farm animal}]
