---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strategic
offline_file: ""
offline_thumbnail: ""
uuid: 4736dec6-c9bd-4ab9-9744-97591da9fca4
updated: 1484310597
title: strategic
categories:
    - Dictionary
---
strategic
     adj 1: relating to or concerned with strategy; "strategic weapon";
            "the islands are of strategic importance"; "strategic
            considerations" [syn: {strategical}]
     2: highly important to or an integral part of a strategy or
        plan of action especially in war; "a strategic chess
        move"; "strategic withdrawal"; "strategic bombing
        missions"
