---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pollinate
offline_file: ""
offline_thumbnail: ""
uuid: abd4f58b-df45-4402-ba43-c0423b49711e
updated: 1484310279
title: pollinate
categories:
    - Dictionary
---
pollinate
     v : fertilize by transfering pollen [syn: {pollenate}, {cross-pollinate}]
