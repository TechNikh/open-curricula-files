---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Set_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: f26d5be4-6096-4cae-b5f9-903b9e3bce95
updated: 1484309499
title: Set (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Example_of_a_set.svg.png
tags:
    - Definition
    - Describing sets
    - Membership
    - Subsets
    - Power sets
    - Cardinality
    - Special sets
    - Basic operations
    - Unions
    - Intersections
    - Complements
    - Cartesian product
    - Applications
    - Axiomatic set theory
    - Principle of inclusion and exclusion
    - "De Morgan's Law"
    - Notes
categories:
    - Set theory
---
In mathematics, a set is a well-defined collection of distinct objects, considered as an object in its own right. For example, the numbers 2, 4, and 6 are distinct objects when considered separately, but when they are considered collectively they form a single set of size three, written {2,4,6}. Sets are one of the most fundamental concepts in mathematics. Developed at the end of the 19th century, set theory is now a ubiquitous part of mathematics, and can be used as a foundation from which nearly all of mathematics can be derived. In mathematics education, elementary topics such as Venn diagrams are taught at a young age, while more advanced concepts are taught as part of a university ...
