---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/munch
offline_file: ""
offline_thumbnail: ""
uuid: ee50951b-fbfc-4620-9d66-e3da2254d691
updated: 1484310344
title: munch
categories:
    - Dictionary
---
Munch
     n 1: Norwegian painter (1863-1944) [syn: {Edvard Munch}]
     2: a large bite; "he tried to talk between munches on the
        sandwich"
     v : chew noisily; "The children crunched the celery sticks"
         [syn: {crunch}]
