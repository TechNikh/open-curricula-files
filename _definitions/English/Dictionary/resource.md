---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resource
offline_file: ""
offline_thumbnail: ""
uuid: 7d5f74bc-93d8-4c68-8373-ad4c1e713bb0
updated: 1484310262
title: resource
categories:
    - Dictionary
---
resource
     n 1: available source of wealth; a new or reserve supply that can
          be drawn upon when needed
     2: a source of aid or support that may be drawn upon when
        needed; "the local library is a valuable resource"
     3: the ability to deal resourcefully with unusual problems; "a
        man of resource" [syn: {resourcefulness}, {imagination}]
