---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrive
offline_file: ""
offline_thumbnail: ""
uuid: 21870a29-26d7-4526-b9e2-2a85b0db9c3f
updated: 1484310307
title: arrive
categories:
    - Dictionary
---
arrive
     v 1: reach a destination; arrive by movement or progress; "She
          arrived home at 7 o'clock"; "She didn't get to Chicago
          until after midnight" [syn: {get}, {come}] [ant: {leave}]
     2: succeed in a big way; get to the top; "After he published
        his book, he had arrived"; "I don't know whether I can
        make it in science!"; "You will go far, my boy!" [syn: {make
        it}, {get in}, {go far}]
