---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bochner%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 18495797-6118-4a48-a65b-e149032d7b0f
updated: 1484309204
title: "Bochner's formula"
categories:
    - Differential geometry
---
In mathematics, Bochner's formula is a statement relating harmonic functions on a Riemannian manifold 
  
    
      
        (
        M
        ,
        g
        )
      
    
    {\displaystyle (M,g)}
  
 to the Ricci curvature.
