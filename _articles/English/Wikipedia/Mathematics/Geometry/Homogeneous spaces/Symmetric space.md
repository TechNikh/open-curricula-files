---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_space
offline_file: ""
offline_thumbnail: ""
uuid: 59067105-0c2c-4bd5-b671-17c85a61c210
updated: 1484309247
title: Symmetric space
tags:
    - Definition using geodesic symmetries
    - Basic properties
    - Examples
    - General definition
    - >
        Riemannian symmetric spaces satisfy the Lie-theoretic
        characterization
    - Classification of Riemannian symmetric spaces
    - Classification scheme
    - Classification result
    - As Grassmannians
    - Symmetric spaces in general
    - Classification results
    - Tables
    - Weakly symmetric Riemannian spaces
    - Applications and special cases
    - Symmetric spaces and holonomy
    - Hermitian symmetric spaces
    - Quaternion-Kähler symmetric spaces
    - Bott periodicity theorem
categories:
    - Homogeneous spaces
---
In differential geometry, representation theory and harmonic analysis, a symmetric space is a pseudo-Riemannian manifold whose group of symmetries contains an inversion symmetry about every point. This can be made more precise, in either the language of Riemannian geometry or of Lie theory. The Riemannian definition is more geometric, and plays a deep role in the theory of holonomy. The Lie-theoretic definition is more algebraic.
