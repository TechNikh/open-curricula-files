---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gang
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484586301
title: gang
categories:
    - Dictionary
---
gang
     n 1: an association of criminals; "police tried to break up the
          gang"; "a pack of thieves" [syn: {pack}, {ring}, {mob}]
     2: an informal body of friends; "he still hangs out with the
        same crowd" [syn: {crowd}, {crew}, {bunch}]
     3: an organized group of workmen [syn: {crew}, {work party}]
     4: tool consisting of a combination of implements arranged to
        work together
     v : act as an organized group [syn: {gang up}]
