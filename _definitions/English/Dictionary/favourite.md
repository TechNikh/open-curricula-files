---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/favourite
offline_file: ""
offline_thumbnail: ""
uuid: 129069db-b2f7-4eb8-8f61-2dd0053886f2
updated: 1484310144
title: favourite
categories:
    - Dictionary
---
favourite
     adj 1: appealing to the general public; "a favorite tourist
            attraction" [syn: {favorite}]
     2: preferred above all others and treated with partiality; "the
        favored child" [syn: {favored}, {favorite(a)}, {favourite(a)},
         {pet}, {preferred}]
     n 1: a competitor thought likely to win [syn: {front-runner}, {favorite}]
     2: a special loved one [syn: {darling}, {favorite}, {pet}, {dearie},
         {deary}, {ducky}]
     3: something regarded with special favor or liking; "that book
        is one of my favorites" [syn: {favorite}]
