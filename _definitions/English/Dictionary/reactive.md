---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reactive
offline_file: ""
offline_thumbnail: ""
uuid: 50c736af-c861-434b-a180-a09113b8e7de
updated: 1484310375
title: reactive
categories:
    - Dictionary
---
reactive
     adj 1: participating readily in reactions; "sodium is a reactive
            metal"; "free radicals are very reactive" [ant: {unreactive}]
     2: tending to react to a stimulus; "the skin of the geriatric
        is less reactive than that of younger persons"- Louis Tuft
