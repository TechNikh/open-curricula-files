---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coil
offline_file: ""
offline_thumbnail: ""
uuid: 835d015f-a419-4323-a6f4-a022658a34cd
updated: 1484310365
title: coil
categories:
    - Dictionary
---
coil
     n 1: a structure consisting of something wound in a continuous
          series of loops; "a coil of rope" [syn: {spiral}, {volute},
           {whorl}, {helix}]
     2: a round shape formed by a series of concentric circles [syn:
         {whorl}, {roll}, {curl}, {curlicue}, {ringlet}, {gyre}, {scroll}]
     3: a transformer that supplies high voltage to spark plugs in a
        gasoline engine
     4: a contraceptive device placed inside a woman's womb
     5: tubing that is wound in a spiral
     6: reactor consisting of a spiral of insulated wire that
        introduces inductance into a circuit
     v 1: to wind or move in a spiral course; "the muscles and nerves
          of ...
