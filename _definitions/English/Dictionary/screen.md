---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/screen
offline_file: ""
offline_thumbnail: ""
uuid: ee7f3ae0-7a30-4cc5-8125-bc0618edf52a
updated: 1484310221
title: screen
categories:
    - Dictionary
---
screen
     n 1: a white or silvered surface where pictures can be projected
          for viewing [syn: {silver screen}, {projection screen}]
     2: something that keeps things out or hinders sight; "they had
        just moved in and had not put up blinds yet" [syn: {blind}]
     3: display on the surface of the large end of a cathode-ray
        tube on which is electronically created [syn: {CRT screen}]
     4: a covering that serves to conceal or shelter something;
        "they crouched behind the screen"; "under cover of
        darkness" [syn: {cover}, {covert}, {concealment}]
     5: protective covering consisting of a metallic netting mounted
        in a frame and covering ...
