---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_Kelly_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: c545238b-6fc9-4501-8a8c-0e87b618c044
updated: 1484309468
title: Paul Kelly (mathematician)
tags:
    - Education and career
    - Contributions
    - Selected articles
categories:
    - Graph theorists
---
