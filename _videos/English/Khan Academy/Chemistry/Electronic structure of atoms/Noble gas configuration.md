---
version: 1
type: video
provider: YouTube
id: tlSJCG5DqzI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Electronic%20structure%20of%20atoms/Noble%20gas%20configuration.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31ad7c9d-ddc5-40d2-a1a4-eeb1051521bc
updated: 1486069585
title: Noble gas configuration
tags:
    - Education
    - online learning
    - learning
    - lessons
    - electron configuration
    - chemistry
    - noble gas
categories:
    - Electronic structure of atoms
thumbnail_urls:
    - https://i3.ytimg.com/vi/tlSJCG5DqzI/default.jpg
    - https://i3.ytimg.com/vi/tlSJCG5DqzI/1.jpg
    - https://i3.ytimg.com/vi/tlSJCG5DqzI/2.jpg
    - https://i3.ytimg.com/vi/tlSJCG5DqzI/3.jpg
---
How to write electron configurations for atoms and monatomic ions using noble gas configuration.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/electron-configurations-2?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/more-on-orbitals-and-electron-configuration?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
