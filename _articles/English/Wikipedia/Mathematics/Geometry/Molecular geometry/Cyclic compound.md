---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_compound
offline_file: ""
offline_thumbnail: ""
uuid: 55b877b1-ca1c-4423-b563-2edfab667b7d
updated: 1484309275
title: Cyclic compound
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dieckmann_Condensation_Scheme.png
tags:
    - Structural introduction
    - Nomenclature
    - Carbocycles
    - Inorganic cyclic compounds
    - Heterocyclic compounds
    - Aromaticity
    - Simple, mono-cyclic examples
    - Stereochemistry
    - Conformational isomerism
    - Macrocycles
    - Principle uses of cyclic structures
    - Complex and polycyclic examples
    - Synthetic reactions altering rings
    - Important general reactions for forming rings
    - Ring-closing reactions
    - Ring-opening reactions
    - Ring expansion and ring contraction reactions
categories:
    - Molecular geometry
---
A cyclic compound (ring compound) is a term for a compound in the field of chemistry in which one or more series of atoms in the compound is connected to form a ring. Rings may vary in size from three to many atoms, and include examples where all the atoms are carbon (i.e., are carbocycles), none of the atoms are carbon (inorganic cyclic compounds), or where both carbon and non-carbon atoms are present (heterocyclic compounds). Depending on the ring size, the bond order of the individual links between ring atoms, and their arrangements within the rings, carbocyclic and heterocyclic compounds may be aromatic or non-aromatic, in the latter case, they may vary from being fully saturated to ...
