---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sacred
offline_file: ""
offline_thumbnail: ""
uuid: 625b1099-45d0-4182-8d45-42dff14fb0a3
updated: 1484310593
title: sacred
categories:
    - Dictionary
---
sacred
     adj 1: concerned with religion or religious purposes; "sacred
            texts"; "sacred rites"; "sacred music" [ant: {profane}]
     2: worthy of respect or dedication; "saw motherhood as woman's
        sacred calling"
     3: made or declared or believed to be holy; devoted to a deity
        or some religious ceremony or use; "a consecrated chursh";
        "the sacred mosque"; "sacred elephants"; "sacred bread and
        wine"; "sanctified wine" [syn: {consecrated}, {sanctified}]
     4: worthy of religious veneration; "the sacred name of Jesus";
        "Jerusalem's hallowed soil" [syn: {hallowed}]
     5: (often followed by `to') devoted exclusively to a single use
     ...
