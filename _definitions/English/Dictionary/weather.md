---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weather
offline_file: ""
offline_thumbnail: ""
uuid: 1e4039fd-9a21-4696-96d7-5d02701eeec0
updated: 1484310518
title: weather
categories:
    - Dictionary
---
weather
     adj : towards the side exposed to wind [syn: {upwind}, {weather(a)}]
     n : the meteorological conditions: temperature and wind and
         clouds and precipitation; "they were hoping for good
         weather"; "every day we have weather conditions and
         yesterday was no exception" [syn: {weather condition}, {atmospheric
         condition}]
     v 1: face or endure with courage; "She braved the elements" [syn:
           {endure}, {brave}, {brave out}]
     2: cause to slope
     3: sail to the windward of
     4: change under the action or influence of the weather; "A
        weathered old hut"
