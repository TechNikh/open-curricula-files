---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appeasing
offline_file: ""
offline_thumbnail: ""
uuid: 5e738db4-cb17-4c4d-a448-7c326aff4ddc
updated: 1484310183
title: appeasing
categories:
    - Dictionary
---
appeasing
     adj : tending or intended to pacify by acceding to demands or
           granting concessions; "the appeasing concessions to the
           Nazis at Munich"; "placating (or placative) gestures";
           "an astonishingly placatory speech" [syn: {appeasing(a)},
            {placating}, {placative}, {placatory}]
