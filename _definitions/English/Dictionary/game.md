---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/game
offline_file: ""
offline_thumbnail: ""
uuid: 0f6ef96a-8b5f-48d3-93ac-2d2ccee2d2f7
updated: 1484310301
title: game
categories:
    - Dictionary
---
game
     adj 1: disabled in the feet or legs; "a crippled soldier"; "a game
            leg" [syn: {crippled}, {halt}, {halting}, {lame}]
     2: willing to face danger [syn: {gamy}, {gamey}, {gritty}, {mettlesome},
         {spirited}, {spunky}]
     n 1: a single play of a game; "the game lasted 2 hours"
     2: a contest with rules to determine a winner; "you need four
        people to play this game"
     3: an amusement or pastime; "they played word games"; "he
        thought of his painting as a game that filled his empty
        time"; "his life was all fun and games"
     4: animal hunted for food or sport
     5: the game equipment needed to play a game; "the child
        ...
