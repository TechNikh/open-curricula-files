---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frightened
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477761
title: frightened
categories:
    - Dictionary
---
frightened
     adj 1: thrown into a state of intense fear or desperation; "became
            panicky as the snow deepened"; "felt panicked before
            each exam"; "trying to keep back the panic-stricken
            crowd"; "the terrified horse bolted" [syn: {panicky},
            {panicked}, {panic-stricken}, {panic-struck}, {terrified}]
     2: made afraid; "the frightened child cowered in the corner";
        "too shocked and scared to move" [syn: {scared}]
