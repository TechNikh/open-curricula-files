---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tangential_angle
offline_file: ""
offline_thumbnail: ""
uuid: 87979683-840f-4897-921e-35c89e2f57ef
updated: 1484309168
title: Tangential angle
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Intrinsic_coordinates_%2528Whewell_equation%2529.png'
tags:
    - Equations
    - 'Polar tangential angle[4]'
categories:
    - Analytic geometry
---
In geometry, the tangential angle of a curve in the Cartesian plane, at a specific point, is the angle between the tangent line to the curve at the given point and the x-axis.[1] (Note that some authors define the angle as the deviation from the direction of the curve at some fixed starting point. This is equivalent to the definition given here by the addition of a constant to the angle or by rotating the curve.[2])
