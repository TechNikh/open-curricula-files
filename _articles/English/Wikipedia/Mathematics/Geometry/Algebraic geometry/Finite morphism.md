---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Finite_morphism
offline_file: ""
offline_thumbnail: ""
uuid: 4b41dac6-8c7c-4825-a7ef-c3f8572e9afe
updated: 1484309147
title: Finite morphism
tags:
    - Properties of finite morphisms
    - Morphisms of finite type
    - Notes
categories:
    - Algebraic geometry
---
such that for each i,
