---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Systempunkt
offline_file: ""
offline_thumbnail: ""
uuid: d686803a-4abc-4801-ace4-4a827563dcac
updated: 1484309065
title: Systempunkt
tags:
    - Examples
    - Etymology
    - Criticism
categories:
    - Strategy
---
A systempunkt ("system point") is a type of critical, vulnerable point in a system. The system may be a physical system (such as computer network) but the term is usually used when referring to a market (e. g., the French steel market, international flax market, etc.)
