---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/youth
offline_file: ""
offline_thumbnail: ""
uuid: 2ac18433-901e-44f9-a15f-27a260db39fa
updated: 1484310439
title: youth
categories:
    - Dictionary
---
youth
     n 1: a young person (especially a young man or boy) [syn: {young
          person}, {younker}, {spring chicken}]
     2: young people collectively; "rock music appeals to the
        young"; "youth everywhere rises in revolt" [syn: {young}]
        [ant: {aged}]
     3: the time of life between childhood and maturity
     4: early maturity; the state of being young or immature or
        inexperienced
     5: an early period of development; "during the youth of the
        project" [syn: {early days}]
     6: the freshness and vitality characteristic of a young person
        [syn: {youthfulness}, {juvenility}]
