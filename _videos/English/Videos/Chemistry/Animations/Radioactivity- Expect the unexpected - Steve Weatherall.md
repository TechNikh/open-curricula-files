---
version: 1
type: video
provider: YouTube
id: TJgc28csgV0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Radioactivity-%20Expect%20the%20unexpected%20-%20Steve%20Weatherall.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5be6278b-0a30-4fb3-ab38-b068987b99d5
updated: 1486069665
title: 'Radioactivity- Expect the unexpected - Steve Weatherall'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TJgc28csgV0/default.jpg
    - https://i3.ytimg.com/vi/TJgc28csgV0/1.jpg
    - https://i3.ytimg.com/vi/TJgc28csgV0/2.jpg
    - https://i3.ytimg.com/vi/TJgc28csgV0/3.jpg
---
