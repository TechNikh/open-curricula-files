---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noble
offline_file: ""
offline_thumbnail: ""
uuid: 8a5cb2b0-b878-4a6c-9462-e8e0920a3ca3
updated: 1484310399
title: noble
categories:
    - Dictionary
---
noble
     adj 1: having high moral qualities; "a noble spirit"; "a solid
            citizen"; "an upstanding man"; "a worthy successor"
            [syn: {solid}, {upstanding}, {worthy}]
     2: impressive in appearance; "a baronial mansion"; "an imposing
        residence"; "a noble tree"; "severe-looking policemen sat
        astride noble horses"; "stately columns" [syn: {baronial},
         {imposing}, {stately}]
     3: of or belonging to or constituting the hereditary
        aristocracy especially as derived from feudal times; "of
        noble birth" [ant: {lowborn}]
     4: having or showing or indicative of high or elevated
        character; "a noble spirit"; "noble deeds" ...
