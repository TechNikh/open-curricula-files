---
version: 1
type: video
provider: YouTube
id: Fb4Xb3OCqfI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5bb2ccec-cec5-4038-9185-873cf0d9e998
updated: 1486069621
title: 'Factorization of polynomials Shortcut Trick - Vedic Maths'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Fb4Xb3OCqfI/default.jpg
    - https://i3.ytimg.com/vi/Fb4Xb3OCqfI/1.jpg
    - https://i3.ytimg.com/vi/Fb4Xb3OCqfI/2.jpg
    - https://i3.ytimg.com/vi/Fb4Xb3OCqfI/3.jpg
---
