---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loading
offline_file: ""
offline_thumbnail: ""
uuid: 5ec987ae-473f-431b-8e2d-fd4244b8522d
updated: 1484310196
title: loading
categories:
    - Dictionary
---
loading
     adj : designed for use in loading e.g. cargo; "a loading dock"; "a
           loading chute is used to drive cattle into a truck or
           other conveyance" [syn: {loading(a)}] [ant: {unloading}]
     n 1: weight to be borne or conveyed [syn: {load}, {burden}]
     2: a quantity that can be processed or transported at one time;
        "the system broke down under excessive loads" [syn: {load}]
     3: the ratio of the gross weight of an airplane to some factor
        determining its lift
     4: goods carried by a large vehicle [syn: {cargo}, {lading}, {freight},
         {load}, {payload}, {shipment}, {consignment}]
     5: the labor of loading something; "the loading ...
