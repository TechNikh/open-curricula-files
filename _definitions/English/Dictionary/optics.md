---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optics
offline_file: ""
offline_thumbnail: ""
uuid: b9183546-8805-4fd6-acd6-72e73636f64d
updated: 1484310208
title: optics
categories:
    - Dictionary
---
optics
     n : the branch of physics that studies the physical properties
         of light
