---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Production_control
offline_file: ""
offline_thumbnail: ""
uuid: fcc9958d-8497-41c5-a055-07ac5583696b
updated: 1484308739
title: Production control
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fotothek_df_n-34_0000193_Maschinist_f%25C3%25BCr_W%25C3%25A4rmekraftwerke.jpg'
tags:
    - Overview
    - Types of production control
    - Related types of control in organizations
categories:
    - Control engineering
---
Production control is the activity of monitoring and controlling any particular production or operation. Production control is often run from a specific control room or operations room
