---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkali
offline_file: ""
offline_thumbnail: ""
uuid: 7b9b8023-8044-4bfe-91dc-be2580ddf1a1
updated: 1484310383
title: alkali
categories:
    - Dictionary
---
alkali
     n 1: any of various water-soluble compounds capable of turning
          litmus blue and reacting with an acid to form a salt and
          water; "bases include oxides and hydroxides of metals
          and ammonia" [syn: {base}]
     2: a mixture of soluble salts found in arid soils and some
        bodies of water; detrimental to agriculture
     [also: {alkalies} (pl)]
