---
version: 1
type: video
provider: YouTube
id: rHxxLYzJ8Sw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2fa6adf2-28dc-4d80-bc55-6bd60115283f
updated: 1486069664
title: 'Polymers - Crash Course Chemistry #45'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rHxxLYzJ8Sw/default.jpg
    - https://i3.ytimg.com/vi/rHxxLYzJ8Sw/1.jpg
    - https://i3.ytimg.com/vi/rHxxLYzJ8Sw/2.jpg
    - https://i3.ytimg.com/vi/rHxxLYzJ8Sw/3.jpg
---
