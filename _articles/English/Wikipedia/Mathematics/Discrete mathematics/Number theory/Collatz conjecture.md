---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Collatz_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: ef2cf459-c3b2-4fc7-bb31-4b98e58ba1cf
updated: 1484308632
title: Collatz conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Collatz-stopping-time.svg.png
tags:
    - Statement of the problem
    - Examples
    - Visualizations
    - Cycles
    - Supporting arguments
    - Experimental evidence
    - A probabilistic heuristic
    - Rigorous bounds
    - Other formulations of the conjecture
    - In reverse
    - As an abstract machine that computes in base two
    - Example
    - As a parity sequence
    - As a tag system
    - Extensions to larger domains
    - Iterating on all integers
    - Iterating with odd denominators or 2-adic integers
    - Cycle bounds
    - Iterating on real or complex numbers
    - Collatz fractal
    - Optimizations
    - Time-space tradeoff
    - Modular restrictions
    - Syracuse function
    - Undecidable generalizations
categories:
    - Number theory
---
The Collatz conjecture is a conjecture in mathematics named after Lothar Collatz. The conjecture is also known as the 3n + 1 conjecture, the Ulam conjecture (after Stanisław Ulam), Kakutani's problem (after Shizuo Kakutani), the Thwaites conjecture (after Sir Bryan Thwaites), Hasse's algorithm (after Helmut Hasse), or the Syracuse problem;[1][3] the sequence of numbers involved is referred to as the hailstone sequence or hailstone numbers (because the values are usually subject to multiple descents and ascents like hailstones in a cloud),[4][5] or as wondrous numbers.[6]
