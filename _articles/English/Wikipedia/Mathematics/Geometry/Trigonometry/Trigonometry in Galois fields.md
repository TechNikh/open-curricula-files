---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometry_in_Galois_fields
offline_file: ""
offline_thumbnail: ""
uuid: d0e114a4-0668-419d-b752-18ae96566422
updated: 1484309350
title: Trigonometry in Galois fields
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/GI%252811%2529_trigonometric_circle.png'
tags:
    - Trigonometry over a Galois field
    - Examples
    - Unimodular groups
    - Example
    - Polar form
    - The Z plane in a Galois field
    - Examples
    - Back to the GF(p)-trigonometry
    - Example
    - Trajectories over the Galois Z plane in GF(p)
categories:
    - Trigonometry
---
In mathematics, trigonometry analogies are supported by the theory of quadratic extensions of finite fields, also known as Galois fields. The main motivation to deal with a finite field trigonometry is the power of the discrete transforms, which play an important role in engineering and mathematics. Significant examples are the well-known discrete trigonometric transforms (DTT), namely the discrete cosine transform and discrete sine transform, which have found many applications in the fields of digital signal and image processing. In the real DTTs, inevitably, rounding is necessary, because the elements of its transformation matrices are derived from the calculation of sines and cosines. ...
