---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Applied_general_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 67533aea-bdab-4a8b-b965-19d86926e2fb
updated: 1484308786
title: Applied general equilibrium
categories:
    - Fixed points (mathematics)
---
In mathematical economics, applied general equilibrium (AGE) models were pioneered by Herbert Scarf at Yale University in 1967, in two papers, and a follow-up book with Terje Hansen in 1973, with the aim of empirically estimating the Arrow–Debreu model of general equilibrium theory with empirical data, to provide "“a general method for the explicit numerical solution of the neoclassical model” (Scarf with Hansen 1973: 1)
