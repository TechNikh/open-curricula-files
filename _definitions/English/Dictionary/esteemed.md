---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/esteemed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484550301
title: esteemed
categories:
    - Dictionary
---
esteemed
     adj : having an illustrious reputation; respected; "our esteemed
           leader"; "a prestigious author" [syn: {honored}, {prestigious}]
