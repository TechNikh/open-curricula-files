---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equality
offline_file: ""
offline_thumbnail: ""
uuid: 6888fd8e-4f10-4a68-ae87-b82d51ac094e
updated: 1484310439
title: equality
categories:
    - Dictionary
---
equality
     n 1: the quality of being the same in quantity or measure or
          value or status [ant: {inequality}]
     2: a state of being essentially equal or equivalent; equally
        balanced; "on a par with the best" [syn: {equivalence}, {equation},
         {par}]
