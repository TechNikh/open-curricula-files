---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/omelette
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469001
title: omelette
categories:
    - Dictionary
---
omelette
     n : beaten eggs or an egg mixture cooked until just set; may be
         folded around e.g. ham or cheese or jelly [syn: {omelet}]
