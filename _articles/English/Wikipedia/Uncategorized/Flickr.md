---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flickr
offline_file: ""
offline_thumbnail: ""
uuid: a11a39d7-ddef-4d58-86bc-ef5938b4497e
updated: 1484308458
title: Flickr
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Flickr_Licenses_edit.svg.png
tags:
    - History
    - Corporate changes
    - features
    - Accounts
    - Organization
    - Access control
    - Interaction and compatibility
    - Filtering
    - Licensing
    - Controversy
    - Censorship
    - Copyright enforcement
    - Sale of Creative Commons-licensed photos
categories:
    - Uncategorized
---
Flickr (pronounced "flicker") is an image hosting and video hosting website and web services suite that was created by Ludicorp in 2004 and acquired by Yahoo on March 20, 2005.[3] In addition to being a popular website for users to share and embed personal photographs, and effectively an online community, the service is widely used by photo researchers and by bloggers to host images that they embed in blogs and social media.[4]
