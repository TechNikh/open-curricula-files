---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circle_packing_theorem
offline_file: ""
offline_thumbnail: ""
uuid: f3221ea5-4702-46a9-9c49-f9ccb39dff94
updated: 1484309110
title: Circle packing theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Circle_packing_theorem_K5_minus_edge_example.svg.png
tags:
    - Uniqueness
    - Relations with conformal mapping theory
    - Proofs
    - Applications
    - Algorithmic aspects
    - Generalizations
    - History
    - Notes
categories:
    - Theorems in geometry
---
The circle packing theorem (also known as the Koebe–Andreev–Thurston theorem) describes the possible tangency relations between circles in the plane whose interiors are disjoint. A circle packing is a connected collection of circles (in general, on any Riemann surface) whose interiors are disjoint. The intersection graph of a circle packing is the graph having a vertex for each circle, and an edge for every pair of circles that are tangent. If the circle packing is on the plane, or, equivalently, on the sphere, then its intersection graph is called a coin graph; more generally, intersection graphs of interior-disjoint geometric objects are called tangency graphs or contact graphs. Coin ...
