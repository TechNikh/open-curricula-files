---
version: 1
type: video
provider: YouTube
id: fLkiuugcrVY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7ab6c1b6-d266-4ac1-a270-ce19c582e85f
updated: 1486069621
title: >
    Examples finding terms of arithmetic sequences from
    definition
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/fLkiuugcrVY/default.jpg
    - https://i3.ytimg.com/vi/fLkiuugcrVY/1.jpg
    - https://i3.ytimg.com/vi/fLkiuugcrVY/2.jpg
    - https://i3.ytimg.com/vi/fLkiuugcrVY/3.jpg
---
