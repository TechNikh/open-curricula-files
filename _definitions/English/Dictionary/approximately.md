---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approximately
offline_file: ""
offline_thumbnail: ""
uuid: 188d7ec1-8855-4094-ad8f-6d3a7b580403
updated: 1484310301
title: approximately
categories:
    - Dictionary
---
approximately
     adv : (of quantities) imprecise but fairly close to correct;
           "lasted approximately an hour"; "in just about a
           minute"; "he's about 30 years old"; "I've had about all
           I can stand"; "we meet about once a month"; "some forty
           people came"; "weighs around a hundred pounds";
           "roughly $3,000"; "holds 3 gallons, more or less"; "20
           or so people were at the party" [syn: {about}, {close
           to}, {just about}, {some}, {roughly}, {more or less}, {around},
            {or so}]
