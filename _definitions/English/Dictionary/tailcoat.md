---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tailcoat
offline_file: ""
offline_thumbnail: ""
uuid: 96cb4c09-5824-48dc-bf8d-507efcb4273b
updated: 1484310150
title: tailcoat
categories:
    - Dictionary
---
tail coat
     n : formalwear consisting of full evening dress for men [syn: {dress
         suit}, {full dress}, {tailcoat}, {tails}, {white tie}, {white
         tie and tails}]
