---
version: 1
type: video
provider: YouTube
id: qO0avvi_Vz4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 57a84fc6-18ee-4585-91ff-17d3074d6d94
updated: 1486069656
title: 'Nuclear War & Fermi Paradox – Mutually Assured Destruction'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qO0avvi_Vz4/default.jpg
    - https://i3.ytimg.com/vi/qO0avvi_Vz4/1.jpg
    - https://i3.ytimg.com/vi/qO0avvi_Vz4/2.jpg
    - https://i3.ytimg.com/vi/qO0avvi_Vz4/3.jpg
---
