---
version: 1
type: article
id: https://en.wikipedia.org/wiki/SYZ_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 3db10b76-3398-4a9b-b9c5-94dbe642d6a7
updated: 1484309239
title: SYZ conjecture
categories:
    - Duality theories
---
The SYZ conjecture is an attempt to understand the mirror symmetry conjecture, an issue in theoretical physics and mathematics. The original conjecture was proposed in a paper by Strominger, Yau, and Zaslow, entitled "Mirror Symmetry is T-duality".[1]
