---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satellite
offline_file: ""
offline_thumbnail: ""
uuid: a9a71ec3-9b17-46ef-a4c9-daf82f373ee8
updated: 1484310433
title: satellite
categories:
    - Dictionary
---
satellite
     adj : surrounding and dominated by a central authority or power;
           "a city and its satellite communities"
     n 1: man-made equipment that orbits around the earth or the moon
          [syn: {artificial satellite}, {orbiter}]
     2: a person who follows or serves another [syn: {planet}]
     3: any celestial body orbiting around a planet or star
     v : broadcast or disseminate via satellite
