---
version: 1
type: video
provider: YouTube
id: uY2ZOsCnXIA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Anatomy%20of%20a%20muscle%20cell.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3530bb47-4489-4afd-8892-04f9be811960
updated: 1486069583
title: Anatomy of a muscle cell
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/uY2ZOsCnXIA/default.jpg
    - https://i3.ytimg.com/vi/uY2ZOsCnXIA/1.jpg
    - https://i3.ytimg.com/vi/uY2ZOsCnXIA/2.jpg
    - https://i3.ytimg.com/vi/uY2ZOsCnXIA/3.jpg
---
Understanding the structure of a muscle cell
More free lessons at: http://www.khanacademy.org/video?v=uY2ZOsCnXIA
