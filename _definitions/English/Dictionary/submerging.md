---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submerging
offline_file: ""
offline_thumbnail: ""
uuid: 716c3fb4-9863-4adb-8235-7cbc785122e8
updated: 1484310168
title: submerging
categories:
    - Dictionary
---
submerging
     n : sinking until covered completely with water [syn: {submergence},
          {submersion}, {immersion}]
