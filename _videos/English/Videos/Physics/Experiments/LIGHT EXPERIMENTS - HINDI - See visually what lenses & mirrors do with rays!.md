---
version: 1
type: video
provider: YouTube
id: A1Fxa_JI1HQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dffa90af-7b3f-4252-9746-0af67481a626
updated: 1486069635
title: 'LIGHT EXPERIMENTS - HINDI - See visually what lenses & mirrors do with rays!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/A1Fxa_JI1HQ/default.jpg
    - https://i3.ytimg.com/vi/A1Fxa_JI1HQ/1.jpg
    - https://i3.ytimg.com/vi/A1Fxa_JI1HQ/2.jpg
    - https://i3.ytimg.com/vi/A1Fxa_JI1HQ/3.jpg
---
