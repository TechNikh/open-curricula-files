---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ignored
offline_file: ""
offline_thumbnail: ""
uuid: c2c912a1-08e5-4ed4-bc25-5b005b41fcd3
updated: 1484310443
title: ignored
categories:
    - Dictionary
---
ignored
     adj : disregarded; "his cries were unheeded"; "Shaw's neglected
           one-act comedy, `A Village Wooing'"; "her ignored
           advice" [syn: {neglected}, {unheeded}]
