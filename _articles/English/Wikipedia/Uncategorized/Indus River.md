---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sindhu
offline_file: ""
offline_thumbnail: ""
uuid: fff99655-5e43-4a21-9bbd-3a8905b9ca5e
updated: 1484308451
title: Indus River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-River_Sindh_0.jpg
tags:
    - Etymology and names
    - Rigveda and the Indus
    - Other names
    - Description
    - History
    - geography
    - Tributaries
    - Geology
    - wildlife
    - Mammals
    - Fish
    - Economy
    - people
    - Modern issues
    - Effects of climate change on the river
    - pollution
    - 2010 floods
    - 2011 floods
    - Barrages, Bridges and Dams
    - Gallery
    - Citations
    - Sources
categories:
    - Uncategorized
---
The Indus River, also called the Sindhū River or Abāsīn, is a major south-flowing river in South Asia. The total length of the river is 3,180 km (1,980 mi) which makes it one of longest rivers in Asia. Originating in the western part of Tibet in the vicinity of Mount Kailash and Lake Manasarovar, the river runs a course through Ladakh, Gilgit-Baltistan, and Khyber Pakhtunkhwa, and then flows along the entire length of Punjab to merge into the Arabian Sea near the port city of Karachi in Sindh. It is the longest river of Pakistan.
