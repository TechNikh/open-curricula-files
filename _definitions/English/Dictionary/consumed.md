---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consumed
offline_file: ""
offline_thumbnail: ""
uuid: 2ceae139-59f4-455a-9da4-40e226436b2b
updated: 1484310323
title: consumed
categories:
    - Dictionary
---
consumed
     adj 1: completely used up [syn: {used-up(a)}, {used up(p)}]
     2: eaten or drunk up
