---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_speed
offline_file: ""
offline_thumbnail: ""
uuid: 725e0f48-f717-43bb-9530-9f6e12f65144
updated: 1484308368
title: V speeds
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-ASI01b.jpg
tags:
    - Regulation
    - Regulatory V-speeds
    - Other V-speeds
    - Mach numbers
    - V1 definitions
categories:
    - Uncategorized
---
In aviation, V-speeds are standard terms used to define airspeeds important or useful to the operation of all aircraft.[1] These speeds are derived from data obtained by aircraft designers and manufacturers during flight testing and verified in most countries by government flight inspectors during aircraft type-certification testing. Using them is considered a best practice to maximize aviation safety, aircraft performance or both.[2]
