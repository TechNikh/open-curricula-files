---
version: 1
type: video
provider: YouTube
id: ntCqXOD9a2s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3a44e7bb-fff7-402d-8442-ea9435c9834a
updated: 1486069617
title: Square Roots and Real Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ntCqXOD9a2s/default.jpg
    - https://i3.ytimg.com/vi/ntCqXOD9a2s/1.jpg
    - https://i3.ytimg.com/vi/ntCqXOD9a2s/2.jpg
    - https://i3.ytimg.com/vi/ntCqXOD9a2s/3.jpg
---
