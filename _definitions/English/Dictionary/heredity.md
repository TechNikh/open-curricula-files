---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heredity
offline_file: ""
offline_thumbnail: ""
uuid: 121b97e6-f288-4293-9756-893486db71e3
updated: 1484310309
title: heredity
categories:
    - Dictionary
---
heredity
     n 1: the biological process whereby genetic factors are
          transmitted from one generation to the next
     2: the total of inherited attributes [syn: {genetic endowment}]
