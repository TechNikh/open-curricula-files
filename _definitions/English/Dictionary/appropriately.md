---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appropriately
offline_file: ""
offline_thumbnail: ""
uuid: 9b5b2591-8945-4e0d-b66a-25e546ecc5b7
updated: 1484310210
title: appropriately
categories:
    - Dictionary
---
appropriately
     adv : in an appropriate manner; "he was appropriately dressed"
           [syn: {suitably}, {fittingly}, {befittingly}, {fitly}]
           [ant: {inappropriately}, {inappropriately}]
