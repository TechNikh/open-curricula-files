---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comparable
offline_file: ""
offline_thumbnail: ""
uuid: 95aa36b0-b0b4-49fe-88eb-e5274c07000c
updated: 1484310275
title: comparable
categories:
    - Dictionary
---
comparable
     adj 1: able to be compared or worthy of comparison [ant: {incomparable}]
     2: conforming in every respect; "boxes with corresponding
        dimensions"; "the like period of the preceding year" [syn:
         {corresponding}, {like}]
