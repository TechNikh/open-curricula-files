---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Energy_accounting
offline_file: ""
offline_thumbnail: ""
uuid: b7d0819a-d4e7-4e8d-bdbf-f54b65a9e3de
updated: 1484308840
title: Energy accounting
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/130px-Crystal_energy.svg_0.png
tags:
    - Energy management
    - Energy balance
categories:
    - Thermodynamics
---
Energy accounting is a system used to measure, analyze and report the energy consumption of different activities on a regular basis.[1] It is done to improve energy efficiency,[2] and to monitor the environment impact of energy consumption.
