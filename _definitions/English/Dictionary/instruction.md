---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instruction
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651821
title: instruction
categories:
    - Dictionary
---
instruction
     n 1: a message describing how something is to be done; "he gave
          directions faster than she could follow them" [syn: {direction}]
     2: the activities of educating or instructing or teaching;
        activities that impart knowledge or skill; "he received no
        formal education"; "our instruction was carefully
        programmed"; "good teaching is seldom rewarded" [syn: {education},
         {teaching}, {pedagogy}, {educational activity}]
     3: the profession of a teacher; "he prepared for teaching while
        still in college"; "pedagogy is recognized as an important
        profession" [syn: {teaching}, {pedagogy}]
     4: (computer science) a line of ...
