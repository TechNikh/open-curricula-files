---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shedding
offline_file: ""
offline_thumbnail: ""
uuid: 23a97a18-0952-4748-a292-837b047b9587
updated: 1484310160
title: shedding
categories:
    - Dictionary
---
shedding
     n 1: the process whereby something is shed [syn: {sloughing}]
     2: loss of bits of outer skin by peeling or shedding or coming
        off in scales [syn: {desquamation}, {peeling}]
