---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Designing_Economic_Mechanisms
offline_file: ""
offline_thumbnail: ""
uuid: a7a7baf6-24cb-4190-8a3f-43935fcbf56d
updated: 1484309046
title: Designing Economic Mechanisms
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Design_portal_logo.jpg
categories:
    - Mechanism design
---
Designing Economic Mechanisms is a 2006 book by economists Leonid Hurwicz and Stanley Reiter. Hurwicz received the 2007 Nobel Memorial Prize in Economic Sciences with Eric Maskin and Roger Myerson for their work on mechanism design. In this book, Hurwicz and Reiter presented systematic methods for designing decentralized economics mechanisms whose performance attains specified goals.
