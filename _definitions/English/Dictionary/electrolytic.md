---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrolytic
offline_file: ""
offline_thumbnail: ""
uuid: 1d97eb32-6170-4440-b61c-b2d3f670d706
updated: 1484310409
title: electrolytic
categories:
    - Dictionary
---
electrolytic
     adj 1: of or concerned with or produced by electrolysis
     2: of or relating to or containing an electrolyte
     n : a fixed capacitor consisting of two electrodes separated by
         an electrolyte [syn: {electrolytic capacitor}, {electrolytic
         condenser}]
