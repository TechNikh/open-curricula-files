---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biconvex
offline_file: ""
offline_thumbnail: ""
uuid: d3221261-6c31-44eb-b9fd-9e384a0c7f40
updated: 1484310206
title: biconvex
categories:
    - Dictionary
---
biconvex
     adj : convex on both sides; lentil-shaped [syn: {convexo-convex},
           {lenticular}, {lentiform}]
