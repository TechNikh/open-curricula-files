---
version: 1
type: video
provider: YouTube
id: a8LF7JEb0IA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 23330ea8-068b-4d8f-a818-0473dbc5021f
updated: 1486069680
title: 'Bonding Models and Lewis Structures- Crash Course Chemistry #24'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/a8LF7JEb0IA/default.jpg
    - https://i3.ytimg.com/vi/a8LF7JEb0IA/1.jpg
    - https://i3.ytimg.com/vi/a8LF7JEb0IA/2.jpg
    - https://i3.ytimg.com/vi/a8LF7JEb0IA/3.jpg
---
