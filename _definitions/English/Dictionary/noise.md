---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noise
offline_file: ""
offline_thumbnail: ""
uuid: 90e0a3ad-a9b5-4346-bb8f-73d5c1eaf6ab
updated: 1484310150
title: noise
categories:
    - Dictionary
---
noise
     n 1: sound of any kind (especially unintelligible or dissonant
          sound); "he enjoyed the street noises"; "they heard
          indistinct noises of people talking"; "during the
          firework display that ended the gala the noise reached
          98 decibels"
     2: the auditory experience of sound that lacks musical quality;
        sound that is a disagreeable auditory experience; "modern
        music is just noise to me" [syn: {dissonance}, {racket}]
     3: electrical or acoustic activity that can disturb
        communication [syn: {interference}, {disturbance}]
     4: a loud outcry of protest or complaint; "the announcement of
        the election recount ...
