---
version: 1
type: video
provider: YouTube
id: JAXyLhvZ-Vg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Divergence%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1bcbfbff-921e-45c7-98f6-3c80ae9bbd2d
updated: 1486069690
title: Divergence 1
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/JAXyLhvZ-Vg/default.jpg
    - https://i3.ytimg.com/vi/JAXyLhvZ-Vg/1.jpg
    - https://i3.ytimg.com/vi/JAXyLhvZ-Vg/2.jpg
    - https://i3.ytimg.com/vi/JAXyLhvZ-Vg/3.jpg
---
