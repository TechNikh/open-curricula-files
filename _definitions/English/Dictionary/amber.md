---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amber
offline_file: ""
offline_thumbnail: ""
uuid: 50cfe1ff-e2fc-411a-b4f8-0b8a6828266c
updated: 1484310158
title: amber
categories:
    - Dictionary
---
amber
     adj : a medium to dark brownish yellow color [syn: {brownish-yellow},
            {yellow-brown}]
     n 1: a deep yellow color; "an amber light illuminated the room";
          "he admired the gold of her hair" [syn: {gold}]
     2: a hard yellowish to brownish translucent fossil resin; used
        for jewelry
