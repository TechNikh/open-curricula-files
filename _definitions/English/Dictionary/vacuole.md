---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vacuole
offline_file: ""
offline_thumbnail: ""
uuid: 3f24002d-d293-48a9-aa5d-dc1d879e255c
updated: 1484310162
title: vacuole
categories:
    - Dictionary
---
vacuole
     n : a tiny cavity filled with fluid in the cytoplasm of a cell
