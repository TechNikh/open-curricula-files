---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interacting_particle_system
offline_file: ""
offline_thumbnail: ""
uuid: 221022ce-6f6c-4a94-843e-13d92375ccf7
updated: 1484308828
title: Interacting particle system
categories:
    - Self-organization
---
In probability theory, an interacting particle system (IPS) is a stochastic process 
  
    
      
        (
        X
        (
        t
        )
        
          )
          
            t
            ∈
            
              
                R
              
              
                +
              
            
          
        
      
    
    {\displaystyle (X(t))_{t\in \mathbb {R} ^{+}}}
  
 on some configuration space 
  
    
      
        Ω
        =
        
          S
          
            G
          
        
      
    
    {\displaystyle \Omega =S^{G}}
  
 given by a site space, a countable-infinite graph 
  
    
      
        G
      
    
    ...
