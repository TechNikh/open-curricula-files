---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cube
offline_file: ""
offline_thumbnail: ""
uuid: 667072d0-2865-41a8-b4f0-6a76edc7c1e2
updated: 1484310226
title: cube
categories:
    - Dictionary
---
cube
     n 1: a three-dimensional shape with six square or rectangular
          sides [syn: {block}]
     2: a hexahedron with six equal squares as faces [syn: {regular
        hexahedron}]
     3: the product of three equal terms [syn: {third power}]
     4: any of several tropical American woody plants of the genus
        Lonchocarpus whose roots are used locally as a fish poison
        and commercially as a source of rotenone
     5: a block in the (approximate) shape of a cube [syn: {square
        block}]
     v 1: raise to the third power
     2: cut into cubes; "cube the cheese" [syn: {dice}]
