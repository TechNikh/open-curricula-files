---
version: 1
type: video
provider: YouTube
id: CWMh61yutjU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Distances-%20Crash%20Course%20Astronomy%20%2325.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 95c28162-a451-4f69-bd04-ebfc47bcd521
updated: 1486069684
title: 'Distances- Crash Course Astronomy #25'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/CWMh61yutjU/default.jpg
    - https://i3.ytimg.com/vi/CWMh61yutjU/1.jpg
    - https://i3.ytimg.com/vi/CWMh61yutjU/2.jpg
    - https://i3.ytimg.com/vi/CWMh61yutjU/3.jpg
---
