---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ingestion
offline_file: ""
offline_thumbnail: ""
uuid: 62d02bd7-9cb9-4608-85d7-b81306c24002
updated: 1484310349
title: ingestion
categories:
    - Dictionary
---
ingestion
     n : the process of taking food into the body through the mouth
         (as by eating) [syn: {consumption}, {intake}, {uptake}]
