---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sunset
offline_file: ""
offline_thumbnail: ""
uuid: d52bc89f-85cb-4895-b6b6-b78a6b47d0a8
updated: 1484310433
title: sunset
categories:
    - Dictionary
---
sunset
     adj 1: of a declining industry or technology; "sunset industries"
     2: providing for termination; "a program with a sunset
        provision" [syn: {sunset(a)}]
     n 1: the time in the evening at which the sun begins to fall
          below the horizon [syn: {sundown}] [ant: {dawn}]
     2: atmospheric phenomena accompanying the daily disappearance
        of the sun
     3: the daily event of the sun sinking below the horizon
