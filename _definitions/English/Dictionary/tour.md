---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tour
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484503021
title: tour
categories:
    - Dictionary
---
tour
     n 1: a journey or route all the way around a particular place or
          area; "they took an extended tour of Europe"; "we took a
          quick circuit of the park"; "a ten-day coach circuit of
          the island" [syn: {circuit}]
     2: a time for working (after which you will be relieved by
        someone else); "it's my go"; "a spell of work" [syn: {go},
         {spell}, {turn}]
     3: a period of time spent in military service [syn: {enlistment},
         {hitch}, {term of enlistment}, {tour of duty}, {duty tour}]
     v : make a tour of a certain place; "We toured the Provence this
         summer"
