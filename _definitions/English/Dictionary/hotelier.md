---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hotelier
offline_file: ""
offline_thumbnail: ""
uuid: 90a16391-9d4f-4f6b-b45a-2b867b3fb8c3
updated: 1484310453
title: hotelier
categories:
    - Dictionary
---
hotelier
     n : an owner or manager of hotels [syn: {hotelkeeper}, {hotel
         manager}, {hotelman}, {hosteller}]
