---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revise
offline_file: ""
offline_thumbnail: ""
uuid: 741e0577-5dba-43c8-b093-b5d9c4997dc9
updated: 1484310140
title: revise
categories:
    - Dictionary
---
revise
     n : the act of rewriting something [syn: {revision}, {revisal},
         {rescript}]
     v 1: make revisions in; "revise a thesis"
     2: revise or reorganize, especially for the purpose of updating
        and improving; "We must retool the town's economy" [syn: {retool}]
