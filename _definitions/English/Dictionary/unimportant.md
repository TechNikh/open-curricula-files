---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unimportant
offline_file: ""
offline_thumbnail: ""
uuid: e7ebeeed-63c3-4330-abbd-fef1863238e0
updated: 1484310587
title: unimportant
categories:
    - Dictionary
---
unimportant
     adj 1: not important; "a relatively unimportant feature of the
            system"; "the question seems unimportant" [ant: {important}]
     2: not important or noteworthy [syn: {insignificant}] [ant: {significant}]
