---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tucker%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 38ebb717-807b-4315-af1a-e87b572da396
updated: 1484308599
title: "Tucker's lemma"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-TuckerLemExample_0.png
tags:
    - Proofs
    - Run-time
    - Equivalent results
categories:
    - Combinatorics
---
Let T be a triangulation of the closed n-dimensional ball 
  
    
      
        
          B
          
            n
          
        
      
    
    {\displaystyle B_{n}}
  
. Assume T is antipodally symmetric on the boundary sphere 
  
    
      
        
          S
          
            n
            −
            1
          
        
      
    
    {\displaystyle S_{n-1}}
  
. That means that the subset of simplices of T which are in 
  
    
      
        
          S
          
            n
            −
            1
          
        
      
    
    {\displaystyle S_{n-1}}
  
 provides a triangulation of 
  
    
      
        
          S
          
            ...
