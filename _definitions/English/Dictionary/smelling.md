---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smelling
offline_file: ""
offline_thumbnail: ""
uuid: 89b6e143-16eb-48f0-b4f6-58aa1538d59e
updated: 1484310429
title: smelling
categories:
    - Dictionary
---
smelling
     adj : (used with `of' or `with') noticeably odorous; "the hall was
           redolent of floor wax"; "air redolent with the fumes of
           beer and whiskey" [syn: {redolent(p)}, {smelling(p)}]
     n : the act of perceiving the odor of something [syn: {smell}]
