---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capital
offline_file: ""
offline_thumbnail: ""
uuid: d92ddb6a-c108-4877-8763-1114cab3729a
updated: 1484310262
title: capital
categories:
    - Dictionary
---
capital
     adj 1: first-rate; "a capital fellow"; "a capital idea"
     2: punishable by death; "a capital offense"
     3: of primary important; "our capital concern was to avoid
        defeat"
     4: uppercase; "capital A"; "great A"; "many medieval
        manuscripts are in majuscule script" [syn: {great}, {majuscule}]
     n 1: assets available for use in the production of further assets
          [syn: {working capital}]
     2: wealth in the form of money or property owned by a person or
        business and human resources of economic value
     3: a seat of government
     4: one of the large alphabetic characters used as the first
        letter in writing or printing proper ...
