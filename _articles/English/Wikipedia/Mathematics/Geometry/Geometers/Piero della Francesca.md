---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Piero_della_Francesca
offline_file: ""
offline_thumbnail: ""
uuid: 372692eb-1a24-4ff1-a2bc-308c1a762e87
updated: 1484309078
title: Piero della Francesca
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Piero_della_Francesca_Malatesta.jpg
tags:
    - Biography
    - Early years
    - Mature work
    - Frescoes in San Francesco at Arezzo
    - "Piero's activity in Urbino"
    - His later years
    - Criticism and interpretation
    - Work in mathematics and geometry
    - Inspirations
    - Selected works
    - Footnotes
categories:
    - Geometers
---
Piero della Francesca (Italian pronunciation: [ˈpjɛːro della franˈtʃeska]  listen (help·info); c. 1415[1] – 12 October 1492) was an Italian painter of the Early Renaissance. As testified by Giorgio Vasari in his Lives of the Most Excellent Painters, Sculptors, and Architects, to contemporaries he was also known as a mathematician and geometer. Nowadays Piero della Francesca is chiefly appreciated for his art. His painting was characterized by its serene humanism, its use of geometric forms and perspective. His most famous work is the cycle of frescoes The History of the True Cross in the church of San Francesco in the Tuscan town of Arezzo.
