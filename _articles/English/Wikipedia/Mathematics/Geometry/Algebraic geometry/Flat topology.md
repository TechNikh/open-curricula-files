---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flat_topology
offline_file: ""
offline_thumbnail: ""
uuid: 1f843b03-fb5d-4a2f-8d60-c9a708734a52
updated: 1484309149
title: Flat topology
tags:
    - The big and small fppf sites
    - The big and small fpqc sites
    - Flat cohomology
    - Example
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, the flat topology is a Grothendieck topology used in algebraic geometry. It is used to define the theory of flat cohomology; it also plays a fundamental role in the theory of descent (faithfully flat descent).[1] The term flat here comes from flat modules.
