---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transcriptor
offline_file: ""
offline_thumbnail: ""
uuid: 6ffb4bf0-ccfc-456c-8c82-02332a959ad0
updated: 1484308688
title: Transcriptor
tags:
    - Background
    - Invention and description
    - Impact
categories:
    - Theoretical computer science
---
A transcriptor is a transistor-like device composed of DNA and RNA rather than a semiconducting material such as silicon. Prior to its invention in 2013, the transcriptor was considered the "final component required to build biological computers."[1]
