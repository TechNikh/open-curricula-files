---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promising
offline_file: ""
offline_thumbnail: ""
uuid: d793fa21-1a79-4361-b946-203033f9a337
updated: 1484310583
title: promising
categories:
    - Dictionary
---
promising
     adj 1: showing possibility of achievement or excellence; "a
            promising young man"
     2: full or promise; "had a bright future in publishing"; "the
        scandal threatened an abrupt end to a promising political
        career" [syn: {bright}]
