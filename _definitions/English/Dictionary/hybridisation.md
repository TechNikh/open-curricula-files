---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hybridisation
offline_file: ""
offline_thumbnail: ""
uuid: 9d694992-4c14-4715-ba06-1bfcbade37b0
updated: 1484310407
title: hybridisation
categories:
    - Dictionary
---
hybridisation
     n : (genetics) the act of mixing different species or varieties
         of animals or plants and thus to produce hybrids [syn: {hybridization},
          {crossbreeding}, {crossing}, {cross}, {interbreeding}, {hybridizing}]
