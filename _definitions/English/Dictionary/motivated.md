---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motivated
offline_file: ""
offline_thumbnail: ""
uuid: d6cb920f-10a6-42bd-9e03-02d9133ed9d0
updated: 1484310262
title: motivated
categories:
    - Dictionary
---
motivated
     adj : provided with a motive or given incentive for action; "a
           highly motivated child can learn almost anything"; "a
           group of politically motivated men" [ant: {unmotivated}]
