---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zinc
offline_file: ""
offline_thumbnail: ""
uuid: d40453da-31a1-47e0-8d7e-312b6d90254d
updated: 1484310228
title: zinc
categories:
    - Dictionary
---
zinc
     n : a bluish-white lustrous metallic element; brittle at
         ordinary temperatures but malleable when heated; used in
         a wide variety of alloys and in galvanizing iron; it
         occurs as zinc sulphide in zinc blende [syn: {Zn}, {atomic
         number 30}]
