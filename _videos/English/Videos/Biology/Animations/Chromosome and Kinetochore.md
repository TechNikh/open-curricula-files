---
version: 1
type: video
provider: YouTube
id: 0JpOJ4F4984
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Chromosome%20and%20Kinetochore.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ed66e597-7019-45fb-a4e7-4621ff72f651
updated: 1486069673
title: Chromosome and Kinetochore
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/0JpOJ4F4984/default.jpg
    - https://i3.ytimg.com/vi/0JpOJ4F4984/1.jpg
    - https://i3.ytimg.com/vi/0JpOJ4F4984/2.jpg
    - https://i3.ytimg.com/vi/0JpOJ4F4984/3.jpg
---
