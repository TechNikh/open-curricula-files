---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humid
offline_file: ""
offline_thumbnail: ""
uuid: 66525f24-5e54-45ad-b714-a054351b151c
updated: 1484310226
title: humid
categories:
    - Dictionary
---
humid
     adj : containing or characterized by a great deal of water vapor;
           "humid air"; "humid weather"
