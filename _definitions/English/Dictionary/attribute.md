---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attribute
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413981
title: attribute
categories:
    - Dictionary
---
attribute
     n 1: a construct whereby objects or individuals can be
          distinguished; "self-confidence is not an endearing
          property" [syn: {property}, {dimension}]
     2: an abstraction belonging to or characteristic of an entity
     v 1: attribute or credit to; "We attributed this quotation to
          Shakespeare"; "People impute great cleverness to cats"
          [syn: {impute}, {ascribe}, {assign}]
     2: decide as to where something belongs in a scheme; "The
        biologist assigned the mushroom to the proper class" [syn:
         {assign}]
