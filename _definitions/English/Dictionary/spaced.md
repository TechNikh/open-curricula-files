---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spaced
offline_file: ""
offline_thumbnail: ""
uuid: 98a4e4c9-ae71-4994-b155-49cb53451208
updated: 1484310196
title: spaced
categories:
    - Dictionary
---
spaced
     adj 1: spaced apart [syn: {separated}]
     2: arranged with spaces between; often used as a combining
        form; "widely spaced eyes" [ant: {unspaced}]
