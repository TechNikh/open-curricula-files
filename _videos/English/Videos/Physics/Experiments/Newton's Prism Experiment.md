---
version: 1
type: video
provider: YouTube
id: uucYGK_Ymp0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7a7516d0-14e7-4501-95ea-37754778267c
updated: 1486069636
title: "Newton's Prism Experiment"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/uucYGK_Ymp0/default.jpg
    - https://i3.ytimg.com/vi/uucYGK_Ymp0/1.jpg
    - https://i3.ytimg.com/vi/uucYGK_Ymp0/2.jpg
    - https://i3.ytimg.com/vi/uucYGK_Ymp0/3.jpg
---
