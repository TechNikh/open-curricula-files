---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_variety
offline_file: ""
offline_thumbnail: ""
uuid: 77ecc074-6634-489d-be39-6bbe1b49f32c
updated: 1484309130
title: Algebraic variety
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_11.png
tags:
    - Introduction and definitions
    - Affine varieties
    - Projective varieties and quasi-projective varieties
    - Abstract varieties
    - >
        Existence of non-quasiprojective abstract algebraic
        varieties
    - Examples
    - Subvariety
    - Affine variety
    - Example 1
    - Example 2
    - Example 3
    - Projective variety
    - Example 1
    - Example 2
    - Non-affine and non-projective example
    - Basic Results
    - Isomorphism of algebraic varieties
    - Discussion and generalizations
    - Algebraic manifolds
    - Footnotes
categories:
    - Algebraic geometry
---
Algebraic varieties are the central objects of study in algebraic geometry. Classically, an algebraic variety is defined as the set of solutions of a system of polynomial equations over the real or complex numbers. Modern definitions generalize this concept in several different ways, while attempting to preserve the geometric intuition behind the original definition.[1]:58
