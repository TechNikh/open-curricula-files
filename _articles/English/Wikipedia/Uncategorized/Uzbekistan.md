---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uzbekistan
offline_file: ""
offline_thumbnail: ""
uuid: 24b53992-6f16-499f-82bc-df6f2aea48d5
updated: 1484308476
title: Uzbekistan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kaunakes_Bactria_Louvre_AO31917.jpg
tags:
    - geography
    - Environment
    - History
    - Politics
    - Human rights
    - Administrative divisions
    - Economy
    - Demographics
    - Largest cities
    - religion
categories:
    - Uncategorized
---
Uzbekistan (US i/ʊzˈbɛkᵻˌstæn, -ˌstɑːn/, UK /ʊzˌbɛkᵻˈstɑːn, ʌz-, -ˈstæn/), also known as Uzbekia, officially the Republic of Uzbekistan (Uzbek: Oʻzbekiston Respublikasi, Ўзбекистон Республикаси), is a doubly landlocked country in Central Asia. It is a unitary, constitutional, presidential republic, comprising twelve provinces, one autonomous republic and a capital city. Uzbekistan is bordered by five landlocked countries: Kazakhstan to the north; Tajikistan to the southeast; Kyrgyzstan to the northeast; Afghanistan to the south; and Turkmenistan to the southwest.
