---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jack
offline_file: ""
offline_thumbnail: ""
uuid: 89626b2b-4a86-48c6-ae94-51d7ea9b0a22
updated: 1484310154
title: jack
categories:
    - Dictionary
---
jack
     n 1: a small worthless amount; "you don't know jack" [syn: {diddly-squat},
           {diddlysquat}, {diddly-shit}, {diddlyshit}, {diddly}, {diddley},
           {squat}, {shit}]
     2: a man who serves as a sailor [syn: {mariner}, {seaman}, {tar},
         {Jack-tar}, {old salt}, {seafarer}, {gob}, {sea dog}]
     3: someone who works with their hands; someone engaged in
        manual labor [syn: {laborer}, {manual laborer}, {labourer}]
     4: immense East Indian fruit resembling breadfruit of; its
        seeds are commonly roasted [syn: {jackfruit}, {jak}]
     5: an electrical device consisting of a connector socket
        designed for the insertion of a plug
     6: game ...
