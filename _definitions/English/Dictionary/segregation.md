---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/segregation
offline_file: ""
offline_thumbnail: ""
uuid: 14ed4a72-8138-42f9-a41a-6e2fc88013d5
updated: 1484310297
title: segregation
categories:
    - Dictionary
---
segregation
     n 1: (genetics) the separation of paired alleles during meiosis
          so that members of each pair of alleles appear in
          different gametes
     2: a social system that provides separate facilities for
        minority groups [syn: {separatism}]
     3: the act of segregating or sequestering; "sequestration of
        the jury" [syn: {sequestration}] [ant: {integration}]
