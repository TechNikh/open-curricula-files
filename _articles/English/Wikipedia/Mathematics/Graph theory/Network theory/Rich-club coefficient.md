---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rich-club_coefficient
offline_file: ""
offline_thumbnail: ""
uuid: ac1de143-0a24-4f7f-bae3-a5a51d9975fb
updated: 1484309438
title: Rich-club coefficient
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Disassortative_network_demonstrating_the_Rich_Club_effect.png
tags:
    - Definition
    - Non-normalized Form
    - Normalized for topology randomization
    - Generalizations
    - General richness properties
    - Related Metrics
    - Assortativity
    - Applications
    - Implementations
categories:
    - Network theory
---
The rich-club coefficient is a metric on graphs and networks, designed to measure the extent to which well-connected nodes also connect to each other. Networks which have a relatively high rich-club coefficient are said to demonstrate the rich-club effect and will have many connections between nodes of high degree. This effect has been measured and noted on scientific collaboration networks and air transportation networks. It has been shown to be significantly lacking on protein interaction networks.
