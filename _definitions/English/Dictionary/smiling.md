---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smiling
offline_file: ""
offline_thumbnail: ""
uuid: 1609aa44-fdeb-40ea-9116-acfadb194708
updated: 1484310591
title: smiling
categories:
    - Dictionary
---
smiling
     adj : smiling with happiness or optimism; "Come to my arms, my
           beamish boy!"- Lewis Carroll; "a room of smiling
           faces"; "a round red twinkly Santa Claus" [syn: {beamish},
            {smiling(a)}, {twinkly}]
     n : a facial expression characterized by turning up the corners
         of the mouth; usually shows pleasure or amusement [syn: {smile},
          {grin}, {grinning}]
