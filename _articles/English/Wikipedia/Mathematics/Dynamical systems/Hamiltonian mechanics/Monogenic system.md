---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monogenic_system
offline_file: ""
offline_thumbnail: ""
uuid: b200a208-69f7-4f7c-a3e6-e9e986255683
updated: 1484308797
title: Monogenic system
categories:
    - Hamiltonian mechanics
---
In classical mechanics, a physical system is termed a monogenic system if the force acting on the system can be modelled in an especially convenient mathematical form (see mathematical definition below). In physics, among the most studied physical systems are monogenic systems.
