---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Campbell%27s_theorem_(geometry)'
offline_file: ""
offline_thumbnail: ""
uuid: 8ae77c27-ef42-4cb9-9bd5-03ad3367b650
updated: 1484309110
title: "Campbell's theorem (geometry)"
categories:
    - Theorems in geometry
---
Campbell's theorem, also known as Campbell’s embedding theorem and the Campbell-Magaarrd theorem, is a mathematical theorem that evaluates the asymptotic distribution of random impulses acting with a determined intensity on a damped system.[citation needed] The theorem guarantees that any n-dimensional Riemannian manifold can be locally embedded in an (n + 1)-dimensional Ricci-flat Riemannian manifold.[1]
