---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organ
offline_file: ""
offline_thumbnail: ""
uuid: 2b4aacac-ac47-4b28-8c0f-d0d75e695f81
updated: 1484310349
title: organ
categories:
    - Dictionary
---
organ
     n 1: a fully differentiated structural and functional unit in an
          animal that is specialized for some particular function
     2: a government agency or instrument devoted to the performance
        of some specific function; "The Census Bureau is an organ
        of the Commerce Department"
     3: (music) an electronic simulation of a pipe organ [syn: {electric
        organ}, {electronic organ}, {Hammond organ}]
     4: a periodical that is published by a special interest group;
        "the organ of the communist party"
     5: wind instrument whose sound is produced by means of pipes
        arranged in sets supplied with air from a bellows and
        controlled ...
