---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vertical
offline_file: ""
offline_thumbnail: ""
uuid: b35e06e3-3418-4685-9e7f-0a1326b58c93
updated: 1484310218
title: vertical
categories:
    - Dictionary
---
vertical
     adj 1: at right angles to the plane of the horizon or a base line;
            "a vertical camera angle"; "the monument consists of
            two vertical pillars supporting a horizontal slab";
            "measure the perpendicular height" [syn: {perpendicular}]
            [ant: {inclined}, {horizontal}]
     2: upright in position or posture; "an erect stature"; "erect
        flower stalks"; "for a dog, an erect tail indicates
        aggression"; "a column still vertical amid the ruins"; "he
        sat bolt upright" [syn: {erect}, {upright}] [ant: {unerect}]
     n 1: something that is oriented vertically
     2: a vertical structural member as a post or stake; "the ...
