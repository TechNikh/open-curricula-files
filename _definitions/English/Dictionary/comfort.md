---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comfort
offline_file: ""
offline_thumbnail: ""
uuid: ae9780f0-ed2a-472a-819f-9882c17e01e4
updated: 1484310480
title: comfort
categories:
    - Dictionary
---
comfort
     n 1: a state of being relaxed and feeling no pain; "he is a man
          who enjoys his comfort"; "she longed for the
          comfortableness of her armchair" [syn: {comfortableness}]
          [ant: {discomfort}]
     2: a feeling of freedom from worry or disappointment
     3: the act of consoling; giving relief in affliction; "his
        presence was a consolation to her" [syn: {consolation}, {solace}]
     4: a freedom from financial difficulty that promotes a
        comfortable state; "a life of luxury and ease"; "he had
        all the material comforts of this world" [syn: {ease}]
     v 1: give moral or emotional strength to [syn: {soothe}, {console},
           ...
