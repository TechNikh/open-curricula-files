---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cake_theory
offline_file: ""
offline_thumbnail: ""
uuid: 6ba56bcb-5954-4f85-a74e-bd9eb650630a
updated: 1484308527
title: Cake theory
categories:
    - Fair division
---
Cake theory (simplified Chinese: 蛋糕论; traditional Chinese: 蛋糕論) is a metaphor about economic development and the redistribution of wealth in the political discourse of China. It emerged in 2010 as problems with an increased wealth gap became gradually more apparent. If economic development is seen as analogous to baking a cake, one side of the debate suggests that development should focus on 'dividing the cake more fairly,' while the other says development should be focused on 'baking a bigger cake.'
