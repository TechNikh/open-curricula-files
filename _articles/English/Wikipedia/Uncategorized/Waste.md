---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Waste_product
offline_file: ""
offline_thumbnail: ""
uuid: 9e0f0b95-e9ee-497a-a565-734123b13c95
updated: 1484308371
title: Waste
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shredded_solid_waste.jpg
tags:
    - Definitions
    - United Nations Environment Program
    - >
        United Nations Statistics Division, Glossary of Environment
        Statistics
    - European Union
    - Types
    - Reporting
    - Costs
    - Environmental costs
    - Social costs
    - Economic costs
    - Resource recovery
    - Energy recovery
    - Education and awareness
    - Gallery
categories:
    - Uncategorized
---
Waste and wastes are unwanted or unusable materials. Waste is any substance which is discarded after primary use, or it is worthless, defective and of no use.
