---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/martial
offline_file: ""
offline_thumbnail: ""
uuid: a3b99b6c-ff88-497e-a33b-c07e57abd8d1
updated: 1484310587
title: martial
categories:
    - Dictionary
---
martial
     adj 1: (of persons) befitting a warrior; "a military bearing" [syn:
             {soldierly}, {soldierlike}, {warriorlike}]
     2: suggesting war or military life [syn: {warlike}]
     3: of or relating to the armed forces; "martial law" [syn: {martial(a)}]
     n : Roman poet noted for epigrams (first century BC)
