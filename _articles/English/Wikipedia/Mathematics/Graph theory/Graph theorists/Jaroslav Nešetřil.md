---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jaroslav_Ne%C5%A1et%C5%99il'
offline_file: ""
offline_thumbnail: ""
uuid: 3614c43d-b64e-43e4-b7f4-c71e6a3d63cf
updated: 1484309470
title: Jaroslav Nešetřil
tags:
    - Education and career
    - Awards and honors
    - Books
categories:
    - Graph theorists
---
Jaroslav (Jarik) Nešetřil (Czech pronunciation: [ˈjaroslaf ˈnɛʃɛtr̝̊ɪl]; born March 13, 1946 in Brno) is a Czech mathematician, working at Charles University in Prague. His research areas include combinatorics (structural combinatorics, Ramsey theory), graph theory (coloring problems, sparse structures), algebra (representation of structures, categories, homomorphisms), posets (diagram and dimension problems), computer science (complexity, NP-completeness).
