---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revolve
offline_file: ""
offline_thumbnail: ""
uuid: e1571276-e0f2-4ff8-a07a-319ec5949060
updated: 1484310393
title: revolve
categories:
    - Dictionary
---
revolve
     v 1: turn on or around an axis or a center; "The Earth revolves
          around the Sun"; "The lamb roast rotates on a spit over
          the fire" [syn: {go around}, {rotate}]
     2: move in an orbit; "The moon orbits around the Earth"; "The
        planets are orbiting the sun"; "electrons orbit the
        nucleus" [syn: {orbit}]
     3: cause to move by turning over or in a circular manner of as
        if on an axis; "She rolled the ball"; "They rolled their
        eyes at his words" [syn: {roll}]
