---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narration
offline_file: ""
offline_thumbnail: ""
uuid: 46e41174-553b-4232-97b8-f5d8c537ebe9
updated: 1484310148
title: narration
categories:
    - Dictionary
---
narration
     n 1: a message that tells the particulars of an act or occurrence
          or course of events; presented in writing or drama or
          cinema or as a radio or television program; "his
          narrative was interesting"; "Disney's stories entertain
          adults as well as children" [syn: {narrative}, {story},
          {tale}]
     2: the act of giving an account describing incidents or a
        course of events; "his narration was hesitant" [syn: {recital},
         {yarn}]
     3: (rhetoric) the second section of an oration in which the
        facts are set forth
