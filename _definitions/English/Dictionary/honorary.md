---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honorary
offline_file: ""
offline_thumbnail: ""
uuid: 9005bcb2-533b-4346-a592-787d7e375d0d
updated: 1484310144
title: honorary
categories:
    - Dictionary
---
honorary
     adj : given as an honor without the normal duties; "an honorary
           degree"
