---
version: 1
type: video
provider: YouTube
id: DP-vWN1yXrY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Equilibrium%20Equations-%20Crash%20Course%20Chemistry%20%2329.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d5e1341-c258-4c8b-9ec7-6c2c3b9c6bfc
updated: 1486069680
title: 'Equilibrium Equations- Crash Course Chemistry #29'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/DP-vWN1yXrY/default.jpg
    - https://i3.ytimg.com/vi/DP-vWN1yXrY/1.jpg
    - https://i3.ytimg.com/vi/DP-vWN1yXrY/2.jpg
    - https://i3.ytimg.com/vi/DP-vWN1yXrY/3.jpg
---
