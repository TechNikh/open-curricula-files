---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemistries
offline_file: ""
offline_thumbnail: ""
uuid: 72de197a-5377-4729-b62d-84115a9e94a0
updated: 1484308380
title: Chemistry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/100px-C%25C3%25ADn.png'
tags:
    - Etymology
    - Definition
    - History
    - Chemistry as science
    - Chemical structure
    - Principles of modern chemistry
    - Matter
    - Atom
    - Element
    - Compound
    - Molecule
    - Substance and mixture
    - Mole and amount of substance
    - Phase
    - Bonding
    - Energy
    - Reaction
    - Ions and salts
    - Acidity and basicity
    - Redox
    - Equilibrium
    - Chemical laws
    - Practice
    - Subdisciplines
    - Chemical industry
    - Professional societies
    - Bibliography
categories:
    - Uncategorized
---
Chemistry is a branch of physical science that studies the composition, structure, properties and change of matter.[1][2] Chemistry includes topics such as the properties of individual atoms, how atoms form chemical bonds to create chemical compounds, the interactions of substances through intermolecular forces that give matter its general properties, and the interactions between substances through chemical reactions to form different substances.
