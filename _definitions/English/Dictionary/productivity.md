---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/productivity
offline_file: ""
offline_thumbnail: ""
uuid: 16551282-267f-42d5-beb5-c2b2641421e7
updated: 1484310258
title: productivity
categories:
    - Dictionary
---
productivity
     n 1: the quality of being productive or having the power to
          produce [syn: {productiveness}] [ant: {unproductiveness}]
     2: (economics) the ratio of the quantity and quality of units
        produced to the labor per unit of time
