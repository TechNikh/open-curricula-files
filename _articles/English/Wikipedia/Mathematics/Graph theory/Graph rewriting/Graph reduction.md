---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_reduction
offline_file: ""
offline_thumbnail: ""
uuid: 8b94b997-af46-4c0f-ba18-080d6dff93b4
updated: 1484309454
title: Graph reduction
tags:
    - Motivation
    - Combinator graph reduction
    - History
    - Notes
categories:
    - Graph rewriting
---
In computer science, graph reduction implements an efficient version of non-strict evaluation, an evaluation strategy where the arguments to a function are not immediately evaluated. This form of non-strict evaluation is also known as lazy evaluation and used in functional programming languages. The technique was first developed by Chris Wadsworth in 1971.
