---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Massieu_function
offline_file: ""
offline_thumbnail: ""
uuid: 44b479ef-f794-412d-9bba-132ebad02459
updated: 1484308853
title: Massieu function
categories:
    - Thermodynamics
---
In thermodynamics, Massieu function, symbol 
  
    
      
        Ψ
      
    
    {\displaystyle \Psi }
  
 (Psi), is defined by the following relation:
