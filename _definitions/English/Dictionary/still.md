---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/still
offline_file: ""
offline_thumbnail: ""
uuid: 03b248aa-2563-4c39-bfbe-b4a1bd220645
updated: 1484310311
title: still
categories:
    - Dictionary
---
still
     adj 1: not in physical motion; "the inertia of an object at rest"
            [syn: {inactive}, {motionless}, {static}]
     2: marked by absence of sound; "a silent house"; "soundless
        footsteps on the grass"; "the night was still" [syn: {silent},
         {soundless}]
     3: free from disturbance; "a ribbon of sand between the angry
        sea and the placid bay"; "the quiet waters of a lagoon";
        "a lake of tranquil blue water reflecting a tranquil blue
        sky"; "a smooth channel crossing"; "scarcely a ripple on
        the still water"; "unruffled water" [syn: {placid}, {quiet},
         {tranquil}, {unruffled}]
     4: used of pictures; of a single or ...
