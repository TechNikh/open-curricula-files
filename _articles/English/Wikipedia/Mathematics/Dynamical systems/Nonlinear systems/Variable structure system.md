---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Variable_structure_system
offline_file: ""
offline_thumbnail: ""
uuid: c5eacd94-45bf-4fff-b11e-f19cfa7a96d1
updated: 1484308811
title: Variable structure system
categories:
    - Nonlinear systems
---
where 
  
    
      
        
          x
        
        ≜
        [
        
          x
          
            1
          
        
        ,
        
          x
          
            2
          
        
        ,
        …
        ,
        
          x
          
            n
          
        
        
          ]
          
            T
          
        
        ∈
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbf {x} \triangleq [x_{1},x_{2},\ldots ,x_{n}]^{\operatorname {T} }\in \mathbb {R} ^{n}}
  
 is the state vector, 
  
    
      
        t
        ∈
        
          R
        
 ...
