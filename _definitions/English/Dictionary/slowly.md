---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slowly
offline_file: ""
offline_thumbnail: ""
uuid: 2d0f6c9a-39e0-4e74-9d02-66ea17e383a5
updated: 1484310315
title: slowly
categories:
    - Dictionary
---
slowly
     adv 1: without speed (`slow' is sometimes used informally for
            `slowly'); "he spoke slowly"; "go easy here--the road
            is slippery"; "glaciers move tardily"; "please go slow
            so I can see the sights" [syn: {slow}, {easy}, {tardily}]
            [ant: {quickly}]
     2: in music; "Play this lento, please" [syn: {lento}]
