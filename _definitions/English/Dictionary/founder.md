---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/founder
offline_file: ""
offline_thumbnail: ""
uuid: 6343abe1-3d1f-42fe-98b3-711845871c4d
updated: 1484310569
title: founder
categories:
    - Dictionary
---
founder
     n 1: inflammation of the laminated tissue that attaches the hoof
          to the foot of a horse [syn: {laminitis}]
     2: a person who founds or establishes some institution; "George
        Washington is the father of his country" [syn: {beginner},
         {founding father}, {father}]
     3: a worker who makes metal castings
     v 1: fail utterly; collapse; "The project foundered" [syn: {fall
          through}, {fall flat}, {flop}]
     2: sink below the surface
     3: break down, literally or metaphorically; "The wall
        collapsed"; "The business collapsed"; "The dam broke";
        "The roof collapsed"; "The wall gave in"; "The roof
        finally gave under ...
