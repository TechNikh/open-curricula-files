---
version: 1
type: video
provider: YouTube
id: GZx3U0dbASg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20earth%20and%20sun.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5c730b78-4e0f-4f6b-abad-2f2f5b21d45f
updated: 1486069685
title: Scale of earth and sun
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/GZx3U0dbASg/default.jpg
    - https://i3.ytimg.com/vi/GZx3U0dbASg/1.jpg
    - https://i3.ytimg.com/vi/GZx3U0dbASg/2.jpg
    - https://i3.ytimg.com/vi/GZx3U0dbASg/3.jpg
---
