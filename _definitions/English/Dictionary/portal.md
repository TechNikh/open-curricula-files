---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/portal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516761
title: portal
categories:
    - Dictionary
---
portal
     n 1: a grand and imposing entrance (often extended
          metaphorically); "the portals of the cathedral"; "the
          portals of heaven"; "the portals of success"
     2: a site that the owner positions as an entrance to other
        sites on the internet; "a portal typically has search
        engines and free email and chat rooms etc." [syn: {portal
        site}]
     3: a short vein that carries blood into the liver [syn: {portal
        vein}, {hepatic portal vein}, {vena portae}]
