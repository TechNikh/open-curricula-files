---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diffusion
offline_file: ""
offline_thumbnail: ""
uuid: 806a280f-b0e9-413d-968e-2feb9ec8e0f5
updated: 1484310162
title: diffusion
categories:
    - Dictionary
---
diffusion
     n 1: (physics) the process of diffusing; the intermingling of
          molecules in gases and liquids as a result of random
          thermal agitation
     2: the spread of social institutions (and myths and skills)
        from one society to another
     3: the property of being diffused or dispersed [syn: {dissemination}]
     4: the act of dispersing or diffusing something; "the
        dispersion of the troops"; "the diffusion of knowledge"
        [syn: {dispersion}, {dispersal}, {dissemination}]
