---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stack_effect
offline_file: ""
offline_thumbnail: ""
uuid: 19f57c7c-3f80-4693-8bfa-782cb7971ed3
updated: 1484308866
title: Stack effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Chimney_effect.svg.png
tags:
    - Stack effect in buildings
    - Stack effect in flue gas stacks and chimneys
    - Cause for the stack effect
    - Induced flow
categories:
    - Thermodynamics
---
Stack effect is the movement of air into and out of buildings, chimneys, flue gas stacks, or other containers, resulting from air buoyancy. Buoyancy occurs due to a difference in indoor-to-outdoor air density resulting from temperature and moisture differences. The result is either a positive or negative buoyancy force. The greater the thermal difference and the height of the structure, the greater the buoyancy force, and thus the stack effect. The stack effect is also referred to as the "chimney effect", and it helps drive natural ventilation, infiltration, and fires (see Kaprun disaster and King's Cross fire).
