---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simply
offline_file: ""
offline_thumbnail: ""
uuid: dc2f6b18-0a88-4636-8150-a6632703d9a3
updated: 1484310311
title: simply
categories:
    - Dictionary
---
simply
     adv 1: and nothing more; "I was merely asking"; "it is simply a
            matter of time"; "just a scratch"; "he was only a
            child"; "hopes that last but a moment" [syn: {merely},
             {just}, {only}, {but}]
     2: absolutely; altogether; really; "we are simply broke"
     3: in a simple manner; without extravagance or embellishment;
        "she was dressed plainly"; "they lived very simply" [syn:
        {plainly}]
     4: absolutely; "I just can't take it anymore"; "he was just
        grand as Romeo"; "it's simply beautiful!" [syn: {just}]
