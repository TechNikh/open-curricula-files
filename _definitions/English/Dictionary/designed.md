---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/designed
offline_file: ""
offline_thumbnail: ""
uuid: 7d4b0ef1-d955-40ce-93b4-7e590403f23e
updated: 1484310307
title: designed
categories:
    - Dictionary
---
designed
     adj 1: planned or created in an artistic or skilled manner; "a
            beautifully designed dress"; "some Italian designed
            sandals have cushioning"
     2: done or made or performed with purpose and intent;
        "style...is more than the deliberate and designed
        creation"- Havelock Ellis; "games designed for all ages";
        "well-designed houses" [syn: {intentional}] [ant: {undesigned}]
     3: carefully practiced or designed or premeditated; "a studied
        reply" [syn: {designed(a)}, {studied(a)}]
     4: organized so as to give configuration to; "a magnet is
        surrounded by a configured field"; "a vehicle designed for
        rough ...
