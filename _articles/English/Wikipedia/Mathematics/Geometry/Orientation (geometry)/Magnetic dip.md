---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_dip
offline_file: ""
offline_thumbnail: ""
uuid: 00b13a65-d3ce-4c72-b919-60912ddcf63e
updated: 1484309295
title: Magnetic dip
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Norman_Robert_dip_circle.jpg
tags:
    - Explanation
    - Practical importance
categories:
    - Orientation (geometry)
---
Magnetic dip, dip angle, or magnetic inclination is the angle made with the horizontal by the Earth's magnetic field lines. This angle varies at different points on the Earth's surface. Positive values of inclination indicate that the magnetic field of the Earth is pointing downward, into the Earth, at the point of measurement, and negative values indicate that it is pointing upward. The dip angle is in principle the angle made by the needle of a vertically held compass, though in practice ordinary compass needles may be weighted against dip or may be unable to move freely in the correct plane. The value can be measured more reliably with a special instrument typically known as a dip circle.
