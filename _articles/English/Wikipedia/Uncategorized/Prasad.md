---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prasad
offline_file: ""
offline_thumbnail: ""
uuid: 235c9339-6f6b-403f-832a-d6bd92d2672c
updated: 1484308463
title: Prasad
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Prasadam_on_banana_leaves.jpg
categories:
    - Uncategorized
---
Prasada (in Sanskrit and Kannada), Prasadam (in Malayalam, Telugu and Tamil and Sanskrit), Prasad Hindustani pronunciation: [prəsaːd̪] (in Marathi), Prasad (in Bengali and Hindi) or Mahaprasada (in Odia) is a material substance of food that is a religious offering in both Hinduism and Sikhism. It is normally consumed by worshippers after worship. It is derived from the same verb सद् (to sit, dwell)prefixed with प्र. (before, afore, in front) and used as finite verb प्रसीदति - dwells, presides, pleases or favours etc. (To preside and president from Latin praesidere thus have the same etymological derivation in Latin but with slightly different meaning).[1]
