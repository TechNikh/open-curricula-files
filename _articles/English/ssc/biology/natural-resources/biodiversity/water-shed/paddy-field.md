---
title: Paddy field
article_ref: https://en.wikipedia.org/wiki/Paddy_field
tags:
    - History
    - China
    - Korea
    - Japan
    - Philippines
    - Vietnam
    - culture
    - China
    - India
    - Indonesia
    - Italy
    - Japan
    - Korea
    - Madagascar
    - Malaysia
    - Myanmar
    - Nepal
    - Philippines
    - Sri Lanka
    - Thailand
    - Vietnam
    - Bibliography
categories:
    - Water Shed
---
