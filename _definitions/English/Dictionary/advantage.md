---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advantage
offline_file: ""
offline_thumbnail: ""
uuid: ae9935bd-026b-4b5d-9e82-89d70d95dddd
updated: 1484310295
title: advantage
categories:
    - Dictionary
---
advantage
     n 1: the quality of having a superior or more favorable position;
          "the experience gave him the advantage over me" [syn: {vantage}]
          [ant: {disadvantage}]
     2: first point scored after deuce
     3: benefit resulting from some event or action; "it turned out
        to my advantage"; "reaping the rewards of generosity"
        [syn: {reward}] [ant: {penalty}]
     v : give an advantage to; "This system advantages the rich"
         [ant: {disadvantage}]
