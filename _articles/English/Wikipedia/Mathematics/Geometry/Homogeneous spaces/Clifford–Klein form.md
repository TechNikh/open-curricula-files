---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Clifford%E2%80%93Klein_form'
offline_file: ""
offline_thumbnail: ""
uuid: b576ad12-1c9c-44ed-b912-888691374f07
updated: 1484309249
title: Clifford–Klein form
categories:
    - Homogeneous spaces
---
where G is a reductive Lie group, H a closed subgroup of G, and Γ a discrete subgroup of G that acts properly discontinuously on the homogeneous space G/H. A suitable discrete subgroup Γ may or may not exist, for a given G and H. If Γ exists, there is the question of whether Γ\G/H can be taken to be a compact space, called a compact Clifford–Klein form.
