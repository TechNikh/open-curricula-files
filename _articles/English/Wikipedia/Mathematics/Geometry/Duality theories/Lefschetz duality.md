---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lefschetz_duality
offline_file: ""
offline_thumbnail: ""
uuid: 06d52c9c-1a91-4e79-9114-128b1c6a5649
updated: 1484309236
title: Lefschetz duality
categories:
    - Duality theories
---
In mathematics, Lefschetz duality is a version of Poincaré duality in geometric topology, applying to a manifold with boundary. Such a formulation was introduced by Lefschetz (1926), at the same time introducing relative homology, for application to the Lefschetz fixed-point theorem.[1] There are now numerous formulations of Lefschetz duality or Poincaré-Lefschetz duality, or Alexander-Lefschetz duality.
