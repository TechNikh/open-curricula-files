---
version: 1
type: video
provider: YouTube
id: 4u7OwGkWqrk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f594ad88-444c-4113-8fe0-7185e4b17711
updated: 1486069657
title: 'Candle swing - physics experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/4u7OwGkWqrk/default.jpg
    - https://i3.ytimg.com/vi/4u7OwGkWqrk/1.jpg
    - https://i3.ytimg.com/vi/4u7OwGkWqrk/2.jpg
    - https://i3.ytimg.com/vi/4u7OwGkWqrk/3.jpg
---
