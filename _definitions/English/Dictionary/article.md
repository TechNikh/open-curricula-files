---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/article
offline_file: ""
offline_thumbnail: ""
uuid: ba7e6565-2d17-49a0-b8fb-6cf42474a97f
updated: 1484310284
title: article
categories:
    - Dictionary
---
article
     n 1: nonfictional prose forming an independent part of a
          publication
     2: one of a class of artifacts; "an article of clothing"
     3: a separate section of a legal document (as a statute or
        contract or will) [syn: {clause}]
     4: (grammar) a determiner that may indicate the specificity of
        reference of a noun phrase
     v : bind by a contract; especially for a training period
