---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Continuous_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: cc6a41cd-1978-4d2f-b961-db67877f9aac
updated: 1484309305
title: Continuous symmetry
tags:
    - Formalization
    - One-parameter subgroups
    - "Noether's theorem"
categories:
    - Symmetry
---
In mathematics, continuous symmetry is an intuitive idea corresponding to the concept of viewing some symmetries as motions, as opposed to discrete symmetry, e.g. reflection symmetry, which is invariant under a kind of flip from one state to another.
