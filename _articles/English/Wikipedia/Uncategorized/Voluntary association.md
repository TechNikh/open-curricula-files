---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Voluntary_organisations
offline_file: ""
offline_thumbnail: ""
uuid: f0acf6a0-d2ae-40b9-a8b9-50823b914270
updated: 1484308366
title: Voluntary association
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AGM_Annual_General_Meeting_of_a_typical_small_%2528141_member%2529_volunteer_organisation.jpg'
tags:
    - Differences by jurisdictions
    - History
    - Legal status
    - Common law
    - England and Wales
    - Scotland
    - United States
    - Australia
    - Canada
    - Civil law
    - France
    - Germany
    - Freedom of association
    - Notes
categories:
    - Uncategorized
---
A voluntary group or union (also sometimes called a voluntary organization, common-interest association,[1]:266 or just an association) is a group of individuals who enter into an agreement, usually as volunteers, to form a body (or organization) to accomplish a purpose.[2] Common examples include trade associations, trade unions, learned societies, professional associations, and environmental groups.
