---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tip
offline_file: ""
offline_thumbnail: ""
uuid: 1513dc3f-0923-4d70-91ba-e9ac83579340
updated: 1484310305
title: tip
categories:
    - Dictionary
---
tip
     n 1: the extreme end of something; especially something pointed
     2: a relatively small amount of money given for services
        rendered (as by a waiter) [syn: {gratuity}, {pourboire}, {baksheesh},
         {bakshish}, {bakshis}, {backsheesh}]
     3: an indication of potential opportunity; "he got a tip on the
        stock market"; "a good lead for a job" [syn: {lead}, {steer},
         {confidential information}, {wind}, {hint}]
     4: a V shape; "the cannibal's teeth were filed to sharp points"
        [syn: {point}, {peak}]
     5: the top point of a mountain or hill; "the view from the peak
        was magnificent"; "they clambered to the summit of
        Monadnock" ...
