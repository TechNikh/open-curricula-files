---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destination
offline_file: ""
offline_thumbnail: ""
uuid: e6fce3a1-43ac-438f-b73f-08b4bebc6da0
updated: 1484310486
title: destination
categories:
    - Dictionary
---
destination
     n 1: the place designated as the end (as of a race or journey);
          "a crowd assembled at the finish"; "he was nearly
          exhuasted as their destination came into view" [syn: {finish},
           {goal}]
     2: the ultimate goal for which something is done [syn: {terminus}]
     3: written directions for finding some location; written on
        letters or packages that are to be delivered to that
        location [syn: {address}, {name and address}]
