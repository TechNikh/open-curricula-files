---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/southern
offline_file: ""
offline_thumbnail: ""
uuid: f377f5a3-cac5-4b08-bee6-c5131ab13268
updated: 1484310303
title: southern
categories:
    - Dictionary
---
southern
     adj 1: in or characteristic of a region of the United States south
            of (approximately) the Mason-Dixon line; "southern
            hospitality"; "southern cooking"; "southern
            plantations" [ant: {northern}]
     2: situated in or oriented toward the south; "a southern
        exposure"; "took a southerly course" [syn: {southerly}]
     3: situated in or coming from regions of the south; "the
        southern hemisphere"; "southern constellations" [ant: {northern}]
     4: from the south; used especially of wind; "a hot south wind";
        "southern breezes"; "the winds are southerly" [syn: {southerly}]
