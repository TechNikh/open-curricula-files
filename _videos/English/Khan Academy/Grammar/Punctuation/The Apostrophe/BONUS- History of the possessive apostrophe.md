---
version: 1
type: video
provider: YouTube
id: BjIPqJ0paXI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/BONUS-%20History%20of%20the%20possessive%20apostrophe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a6b29416-5d88-4e2c-9532-b839b469d5b3
updated: 1486069601
title: 'BONUS- History of the possessive apostrophe'
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/BjIPqJ0paXI/default.jpg
    - https://i3.ytimg.com/vi/BjIPqJ0paXI/1.jpg
    - https://i3.ytimg.com/vi/BjIPqJ0paXI/2.jpg
    - https://i3.ytimg.com/vi/BjIPqJ0paXI/3.jpg
---
David, Paige, and Jake cover the fascinating history of the possessive apostrophe. Stay tuned for this bonus video!


Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/bonus-history-of-the-apostrophe

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
