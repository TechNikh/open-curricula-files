---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zugzwang
offline_file: ""
offline_thumbnail: ""
uuid: 88018d74-548a-4ce6-8b94-cc49730873e9
updated: 1484309022
title: Zugzwang
tags:
    - Etymology
    - History
    - Zugzwang in chess
    - Examples from games
    - Fischer versus Taimanov, second match game
    - Fischer versus Taimanov, fourth match game
    - Tseshkovsky versus Flear, 1988
    - Reciprocal zugzwang
    - Trébuchet
    - Mined squares
    - Zugzwang helps the defense
    - Zugzwang in the middlegame and complex endgames
    - Sämisch versus Nimzowitsch
    - Steinitz versus Lasker
    - Podgaets versus Dvoretsky
    - Fischer versus Rossetto
    - Zugzwang Lite
categories:
    - Chess theory
---
Zugzwang (German for "compulsion to move", pronounced [ˈtsuːktsvaŋ]) is a situation found in chess and other games wherein one player is put at a disadvantage because they must make a move when they would prefer to pass and not move. The fact that the player is compelled to move means that their position will become significantly weaker. A player is said to be "in zugzwang" when any possible move will worsen their position.[1]
