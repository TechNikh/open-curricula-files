---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arranged
offline_file: ""
offline_thumbnail: ""
uuid: 0fa75771-9bb6-40f9-9ade-b4ba560972b3
updated: 1484310339
title: arranged
categories:
    - Dictionary
---
arranged
     adj 1: disposed or placed in a particular kind of order; "the
            carefully arranged chessmen"; "haphazardly arranged
            interlobular septa"; "comfortable chairs arranged
            around the fireplace" [syn: {ordered}] [ant: {disarranged}]
     2: planned in advance; "an arranged marriage"
     3: deliberately arranged for effect; "one of those artfully
        staged photographs" [syn: {staged}]
