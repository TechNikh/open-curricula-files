---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chrysanthemum
offline_file: ""
offline_thumbnail: ""
uuid: 7144452f-9d7b-48a6-8592-75e0530d1f92
updated: 1484310160
title: chrysanthemum
categories:
    - Dictionary
---
chrysanthemum
     n 1: the flower of a chrysanthemum plant
     2: any of numerous perennial Old World herbs having showy
        brightly colored flower heads of the genera Chrysanthemum;
        Argyranthemum; Dendranthema; Tanacetum; widely cultivated
