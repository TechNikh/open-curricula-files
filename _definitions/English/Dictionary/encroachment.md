---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encroachment
offline_file: ""
offline_thumbnail: ""
uuid: 8410f610-0a39-4ec7-9508-297ba2438bac
updated: 1484310462
title: encroachment
categories:
    - Dictionary
---
encroachment
     n 1: any entry into an area not previously occupied; "an invasion
          of tourists"; "an invasion of locusts" [syn: {invasion},
           {intrusion}]
     2: entry to another's property without right or permission
        [syn: {trespass}, {violation}, {intrusion}, {usurpation}]
     3: influencing strongly; "they resented the impingement of
        American values on European culture" [syn: {impingement},
        {impact}]
