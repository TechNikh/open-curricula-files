---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/timely
offline_file: ""
offline_thumbnail: ""
uuid: 5d925c83-e8fe-44d7-85fb-12173eca1308
updated: 1484310457
title: timely
categories:
    - Dictionary
---
timely
     adj 1: before a time limit expires; "the timely filing of his
            income tax return"
     2: done or happening at the appropriate or proper time; "a
        timely warning"; "with timely treatment the patient has a
        good chance of recovery"; "a seasonable time for
        discussion"; "the book's publication was well timed" [syn:
         {seasonable}, {well-timed(a)}, {well timed(p)}]
     adv : at an opportune time; "your letter arrived apropos" [syn: {seasonably},
            {well-timed}, {apropos}]
     [also: {timeliest}, {timelier}]
