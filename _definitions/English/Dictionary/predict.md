---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/predict
offline_file: ""
offline_thumbnail: ""
uuid: 56e367c5-213d-46a3-8f16-552215e5721d
updated: 1484310204
title: predict
categories:
    - Dictionary
---
predict
     v 1: make a prediction about; tell in advance; "Call the outcome
          of an election" [syn: {foretell}, {prognosticate}, {call},
           {forebode}, {anticipate}, {promise}]
     2: indicate by signs; "These signs bode bad news" [syn: {bode},
         {portend}, {auspicate}, {prognosticate}, {omen}, {presage},
         {betoken}, {foreshadow}, {augur}, {foretell}, {prefigure},
         {forecast}]
