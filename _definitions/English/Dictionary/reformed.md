---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reformed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484622721
title: reformed
categories:
    - Dictionary
---
re-formed
     adj : formed again or anew; "the re-formed scout troop has been
           very active"
