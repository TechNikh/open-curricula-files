---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interference
offline_file: ""
offline_thumbnail: ""
uuid: da860664-59af-4054-b04d-d6c25346954b
updated: 1484310192
title: interference
categories:
    - Dictionary
---
interference
     n 1: a policy of intervening in the affairs of other countries
          [syn: {intervention}] [ant: {nonintervention}, {nonintervention}]
     2: the act of hindering or obstructing or impeding [syn: {hindrance}]
     3: electrical or acoustic activity that can disturb
        communication [syn: {noise}, {disturbance}]
     4: (American football) the act of obstructing someone's path
        with your body; "he threw a rolling block into the line
        backer" [syn: {blocking}, {block}]
     5: any obstruction that impedes or is burdensome [syn: {hindrance},
         {hitch}, {preventive}, {preventative}, {encumbrance}, {incumbrance}]
