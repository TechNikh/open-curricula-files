---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enact
offline_file: ""
offline_thumbnail: ""
uuid: 91a7406c-ffa9-4891-81f9-d988043fd4de
updated: 1484310238
title: enact
categories:
    - Dictionary
---
enact
     v 1: order by virtue of superior authority; decree; "The King
          ordained the persecution and expulsion of the Jews";
          "the legislature enacted this law in 1985" [syn: {ordain}]
     2: act out; represent or perform as if in a play; "She
        reenacted what had happened earlier that day" [syn: {reenact},
         {act out}]
