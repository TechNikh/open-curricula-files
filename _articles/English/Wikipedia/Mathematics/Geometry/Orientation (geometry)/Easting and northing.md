---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Easting_and_northing
offline_file: ""
offline_thumbnail: ""
uuid: 0be810ba-a0eb-4498-8154-08c00c8001a4
updated: 1484309293
title: Easting and northing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Utm-zones.jpg
tags:
    - Notation and conventions
    - Other usage
categories:
    - Orientation (geometry)
---
The terms easting and northing are geographic Cartesian coordinates for a point. Easting refers to the eastward-measured distance (or the x-coordinate), while northing refers to the northward-measured distance (or the y-coordinate). When using common projections such as the transverse Mercator projection, these are distances projected on an imaginary surface similar to a bent sheet of paper, and are not the same as distances measured on the curved surface of the Earth.
