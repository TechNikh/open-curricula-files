---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abused
offline_file: ""
offline_thumbnail: ""
uuid: 43f2a5d7-03bb-438b-84e9-c4e8ae4c3fc4
updated: 1484310166
title: abused
categories:
    - Dictionary
---
abused
     adj 1: used improperly or excessively especially drugs; "an abused
            substance"
     2: abused with language [ant: {unabused}]
     3: physically abused; "an abused wife" [syn: {ill-treated}, {maltreated},
         {mistreated}] [ant: {unabused}]
