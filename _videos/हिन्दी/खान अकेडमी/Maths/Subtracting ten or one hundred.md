---
version: 1
type: video
provider: YouTube
id: eM0ciTEZSPk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 23138025-c709-4e73-87fb-cc4b1194fb65
updated: 1486069614
title: Subtracting ten or one hundred
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/eM0ciTEZSPk/default.jpg
    - https://i3.ytimg.com/vi/eM0ciTEZSPk/1.jpg
    - https://i3.ytimg.com/vi/eM0ciTEZSPk/2.jpg
    - https://i3.ytimg.com/vi/eM0ciTEZSPk/3.jpg
---
