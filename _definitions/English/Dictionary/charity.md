---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640481
title: charity
categories:
    - Dictionary
---
charity
     n 1: a foundation created to promote the public good (not for
          assistance to any particular individuals)
     2: a kindly and lenient attitude toward people [syn: {brotherly
        love}]
     3: an activity or gift that benefits the public at large
     4: pinnate-leaved European perennial having bright blue or
        white flowers [syn: {Jacob's ladder}, {Greek valerian}, {Polemonium
        caeruleum}, {Polemonium van-bruntiae}, {Polymonium
        caeruleum van-bruntiae}]
     5: an institution set up to provide help to the needy
