---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/currently
offline_file: ""
offline_thumbnail: ""
uuid: e949ba4e-a8b4-47d9-8677-a1b7471f52ac
updated: 1484310325
title: currently
categories:
    - Dictionary
---
currently
     adv : at this time or period; now; "he is presently our ambassador
           to the United Nations"; "currently they live in
           Connecticut" [syn: {presently}]
