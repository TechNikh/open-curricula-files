---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equilibrium_selection
offline_file: ""
offline_thumbnail: ""
uuid: 4e558a90-c221-4c18-9da2-eb1517074803
updated: 1484309032
title: Equilibrium selection
categories:
    - Game theory equilibrium concepts
---
Equilibrium selection is a concept from game theory which seeks to address reasons for players of a game to select a certain equilibrium over another. The concept is especially relevant in evolutionary game theory, where the different methods of equilibrium selection respond to different ideas of what equilibria will be stable and persistent for one player to play even in the face of deviations (and mutations) of the other players. This is important because there are various equilibrium concepts, and for many particular concepts, such as the Nash equilibrium, many games have multiple equilibria.
