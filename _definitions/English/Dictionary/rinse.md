---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rinse
offline_file: ""
offline_thumbnail: ""
uuid: 75e1d41d-a870-4a1b-85a4-5031494d9ba2
updated: 1484310381
title: rinse
categories:
    - Dictionary
---
rinse
     n 1: a liquid preparation used on wet hair to give it a tint
     2: the removal of soap with clean water in the final stage of
        washing [syn: {rinsing}]
     3: the act of giving a light tint to the hair
     4: washing lightly without soap
     v 1: wash off soap or remaining dirt [syn: {rinse off}]
     2: clean with some chemical process [syn: {wash}]
     3: rinse one's mouth and throat with mouthwash; "gargle with
        this liquid" [syn: {gargle}]
