---
version: 1
type: video
provider: YouTube
id: LZie2QC5Jbc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83709050-83a7-4caf-8d97-c1d3067b5ce3
updated: 1486069657
title: Quantum Operators
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/LZie2QC5Jbc/default.jpg
    - https://i3.ytimg.com/vi/LZie2QC5Jbc/1.jpg
    - https://i3.ytimg.com/vi/LZie2QC5Jbc/2.jpg
    - https://i3.ytimg.com/vi/LZie2QC5Jbc/3.jpg
---
