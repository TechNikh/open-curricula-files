---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fixed-point_combinator
offline_file: ""
offline_thumbnail: ""
uuid: 74cf7762-0e8c-43f3-9d30-31cbd185d917
updated: 1484308784
title: Fixed-point combinator
tags:
    - Introduction
    - Values and domains
    - Function versus implementation
    - What is a "combinator"?
    - Usage
    - The factorial function
    - Fixed point combinators in lambda calculus
    - Equivalent definition of a fixed-point combinator
    - Derivation of the Y combinator
    - Other fixed-point combinators
    - Strict fixed point combinator
    - Non-standard fixed-point combinators
    - Implementation in other languages
    - Lazy functional implementation
    - Strict functional implementation
    - Imperative language implementation
    - Typing
    - Type for the Y combinator
    - General information
    - Notes
categories:
    - Fixed points (mathematics)
---
or in words: y, when applied to an arbitrary function f, yields the same result as f applied to the result of applying y to f. It is so named because, by setting 
  
    
      
        x
        =
        y
         
        f
      
    
    {\displaystyle x=y\ f}
  
, it represents a solution to the fixed point equation,
