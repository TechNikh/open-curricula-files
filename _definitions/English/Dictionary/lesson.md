---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lesson
offline_file: ""
offline_thumbnail: ""
uuid: 3d2bc42a-9ce3-49e8-8f01-09f728921031
updated: 1484310221
title: lesson
categories:
    - Dictionary
---
lesson
     n 1: a unit of instruction; "he took driving lessons"
     2: punishment intended as a warning to others; "they decided to
        make an example of him" [syn: {example}, {deterrent
        example}, {object lesson}]
     3: the significance of a story or event; "the moral of the
        story is to love thy neighbor" [syn: {moral}]
     4: a task assigned for individual study; "he did the lesson for
        today"
