---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hamilton%E2%80%93Jacobi%E2%80%93Einstein_equation'
offline_file: ""
offline_thumbnail: ""
uuid: 32eaeea2-f4d1-4015-b296-ea9e3019dc10
updated: 1484308795
title: Hamilton–Jacobi–Einstein equation
tags:
    - Background and motivation
    - Correspondence between classical and quantum physics
    - Shortcomings of four-dimensional spacetime
    - Equation
    - General equation (free curved space)
    - Applications
    - Notes
    - Books
    - Selected papers
categories:
    - Hamiltonian mechanics
---
In general relativity, the Hamilton–Jacobi–Einstein equation (HJEE) or Einstein–Hamilton–Jacobi equation (EHJE) is an equation in the Hamiltonian formulation of geometrodynamics in superspace, cast in the "geometrodynamics era" around the 1960s, by A. Peres[1] in 1962 and others. It is an attempt to reformulate general relativity in such a way that it resembles quantum theory within a semiclassical approximation, much like the correspondence between quantum mechanics and classical mechanics.
