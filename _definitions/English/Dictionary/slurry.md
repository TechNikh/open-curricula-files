---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slurry
offline_file: ""
offline_thumbnail: ""
uuid: dc2a41df-440a-4f3f-87fc-b5a9353a1cd4
updated: 1484310339
title: slurry
categories:
    - Dictionary
---
slurry
     n : a suspension of insoluble particles (as plaster of paris or
         lime or clay etc.) usually in water
