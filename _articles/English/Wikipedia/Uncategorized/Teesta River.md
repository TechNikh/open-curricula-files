---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Teesta
offline_file: ""
offline_thumbnail: ""
uuid: ae7fb6a5-44a2-4627-aae1-8e6f62728026
updated: 1484308422
title: Teesta River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Teesta_Rangeet_Confluence.jpg
tags:
    - Course
    - geography
    - Changes in course of rivers
    - Hydroelectric projects
    - Other dams
    - Seismic concerns
    - Climate and tectonics
categories:
    - Uncategorized
---
The Teesta River or Tista (Bengali তিস্তা) is a 309 km (192 mi) long river flowing through the Indian state of Sikkim. It carves out from the verdant Himalayas in temperate and tropical river valleys and forms the border between Sikkim and West Bengal. It flows through the cities of Rangpo, Jalpaiguri and Kalimpong and joins the Jamuna in Bangladesh. [1] It drains an area of 12,540 km2 (4,840 sq mi).
