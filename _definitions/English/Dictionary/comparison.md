---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comparison
offline_file: ""
offline_thumbnail: ""
uuid: 84774504-5011-468c-a232-9b18a36a5526
updated: 1484310271
title: comparison
categories:
    - Dictionary
---
comparison
     n 1: examining resemblances or differences [syn: {comparing}]
     2: relation based on similarities and differences
     3: qualities that are comparable; "no comparison between the
        two books"; "beyond compare" [syn: {compare}, {equivalence},
         {comparability}]
