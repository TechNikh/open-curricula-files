---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rectangular
offline_file: ""
offline_thumbnail: ""
uuid: 188eb5f6-8411-4170-bb49-e06832a012c2
updated: 1484310254
title: rectangular
categories:
    - Dictionary
---
rectangular
     adj 1: having four right angles; "a rectangular figure twice as
            long as it is wide"
     2: having a set of mutually perpendicular axes; meeting at
        right angles; "wind and sea may displace the ship's center
        of gravity along three orthogonal axes"; "a rectangular
        Cartesian coordinate system" [syn: {orthogonal}]
