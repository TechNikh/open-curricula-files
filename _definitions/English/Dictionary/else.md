---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/else
offline_file: ""
offline_thumbnail: ""
uuid: 7cd74397-b8cf-41f5-80af-73ab8bcbd914
updated: 1484310333
title: else
categories:
    - Dictionary
---
else
     adj 1: other than what is under consideration or implied; "ask
            somebody else"; "I don't know what else to do"; "where
            else can we look?"
     2: more; "would you like anything else?"; "I have nothing else
        to say" [syn: {else(ip)}, {additional}]
     adv 1: additional to or different from this one or place or time or
            manner; "nobody else is here"; "she ignored everything
            else"; "I don't know where else to look"; "when else
            can we have the party?"; "couldn't decide how else it
            could be done"
     2: (usually used with `or') if not, then; "watch your step or
        else you may fall"; "leave or else I'll ...
