---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_pushout_graph_rewriting
offline_file: ""
offline_thumbnail: ""
uuid: 71f717b9-3ff8-4290-a582-fff5de7dbd40
updated: 1484309452
title: Double pushout graph rewriting
tags:
    - Definition
    - Uses
    - Generalization
    - Notes
categories:
    - Graph rewriting
---
In computer science, double pushout graph rewriting or (DPO graph rewriting) refers to a mathematical framework for graph rewriting. It was introduced as one of the first algebraic approaches to graph rewriting in the article "Graph-grammars: An algebraic approach" (1973).[1] It has since been generalized to allow rewriting structures which are not graphs, and to handle negative application conditions,[2] among other extensions.
