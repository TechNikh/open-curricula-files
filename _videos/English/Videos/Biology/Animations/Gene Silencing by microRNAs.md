---
version: 1
type: video
provider: YouTube
id: t5jroSCBBwk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Gene%20Silencing%20by%20microRNAs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 56a24b9b-cf09-4aec-8dc5-466def3bb6d3
updated: 1486069672
title: Gene Silencing by microRNAs
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/t5jroSCBBwk/default.jpg
    - https://i3.ytimg.com/vi/t5jroSCBBwk/1.jpg
    - https://i3.ytimg.com/vi/t5jroSCBBwk/2.jpg
    - https://i3.ytimg.com/vi/t5jroSCBBwk/3.jpg
---
