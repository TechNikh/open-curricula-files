---
version: 1
type: video
provider: YouTube
id: DmdGIuYqyZI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c8b50d26-e656-40f1-857c-40011e0107ce
updated: 1486069607
title: Differentiability implies continuity
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/DmdGIuYqyZI/default.jpg
    - https://i3.ytimg.com/vi/DmdGIuYqyZI/1.jpg
    - https://i3.ytimg.com/vi/DmdGIuYqyZI/2.jpg
    - https://i3.ytimg.com/vi/DmdGIuYqyZI/3.jpg
---
