---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elongated
offline_file: ""
offline_thumbnail: ""
uuid: 225ebd55-d50d-4fae-9699-f9a4417074e0
updated: 1484310283
title: elongated
categories:
    - Dictionary
---
elongated
     adj 1: drawn out or made longer spatially; "Picasso's elongated Don
            Quixote"; "lengthened skirts are fashionable this
            year"; "the extended airport runways can accommodate
            larger planes"; "a prolonged black line across the
            page" [syn: {extended}, {lengthened}, {prolonged}]
     2: having notably more length than width; being long and
        slender; "an elongate tail tapering to a point"; "the old
        man's gaunt and elongated frame" [syn: {elongate}]
