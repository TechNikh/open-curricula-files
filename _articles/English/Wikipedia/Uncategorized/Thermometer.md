---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laboratory_thermometer
offline_file: ""
offline_thumbnail: ""
uuid: 6810e3fc-684a-47ee-afdb-98705b9c0c40
updated: 1484308375
title: Thermometer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mercury_Thermometer.jpg
tags:
    - Temperature
    - History and development
    - Registering
    - Physical principles of thermometry
    - Thermometric materials
    - Constant volume thermometry
    - Radiometric thermometry
    - Primary and secondary thermometers
    - Calibration
    - Precision, accuracy, and reproducibility
    - Nanothermometry
    - Uses
    - Types of thermometer
categories:
    - Uncategorized
---
A thermometer is a device that measures temperature or a temperature gradient. A thermometer has two important elements: (1) a temperature sensor (e.g. the bulb of a mercury-in-glass thermometer) in which some physical change occurs with temperature, and (2) some means of converting this physical change into a numerical value (e.g. the visible scale that is marked on a mercury-in-glass thermometer). Thermometers are widely used in industry to control and regulate processes, in the study of weather, in medicine, and in scientific research.
