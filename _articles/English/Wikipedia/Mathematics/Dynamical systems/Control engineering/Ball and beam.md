---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ball_and_beam
offline_file: ""
offline_thumbnail: ""
uuid: 78c5c66d-ee67-48db-8fc7-f9a091141a39
updated: 1484308728
title: Ball and beam
categories:
    - Control engineering
---
The ball and beam system consists of a long beam which can be tilted by a servo or electric motor together with a ball rolling back and forth on top of the beam. It is a popular textbook example in control theory.
