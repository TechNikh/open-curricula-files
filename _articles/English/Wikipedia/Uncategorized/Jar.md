---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Glass_jar
offline_file: ""
offline_thumbnail: ""
uuid: 787a9333-dbf5-4672-a677-35527f28c5fe
updated: 1484308368
title: Jar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Branston_Pickle_jar_1.jpg
tags:
    - Gallery
categories:
    - Uncategorized
---
A jar is a rigid, approximately cylindrical container with a wide mouth or opening. Jars are typically made of glass, ceramic, or plastic. They are used for foods, cosmetics, medications, and chemicals that are relatively thick or viscous: pourable liquids are more often packaged in a bottle. They are also used for items too large to be removed from a narrow neck bottle.[1]
