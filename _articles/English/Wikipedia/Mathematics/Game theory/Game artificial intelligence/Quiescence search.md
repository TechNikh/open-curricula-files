---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quiescence_search
offline_file: ""
offline_thumbnail: ""
uuid: a1026847-8cbd-486f-a220-24e0ff7298d2
updated: 1484309006
title: Quiescence search
categories:
    - Game artificial intelligence
---
Quiescence search is an algorithm typically used to evaluate minimax game trees in game-playing computer programs. It is a remedy for the horizon problem faced by AI engines for various games like chess and Go.
