---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pay
offline_file: ""
offline_thumbnail: ""
uuid: f32a0b85-3b6b-4af9-a59e-0e429345ee0a
updated: 1484310443
title: pay
categories:
    - Dictionary
---
pay
     n : something that remunerates; "wages were paid by check"; "he
         wasted his pay on drink"; "they saved a quarter of all
         their earnings" [syn: {wage}, {earnings}, {remuneration},
          {salary}]
     v 1: give money, usually in exchange for goods or services; "I
          paid four dollars for this sandwich"; "Pay the waitress,
          please"
     2: convey, as of a compliment, regards, attention, etc.;
        bestow; "Don't pay him any mind"; "give the orders"; "Give
        him my best regards"; "pay attention" [syn: {give}]
     3: do or give something to somebody in return; "Does she pay
        you for the work you are doing?" [syn: {pay off}, {make ...
