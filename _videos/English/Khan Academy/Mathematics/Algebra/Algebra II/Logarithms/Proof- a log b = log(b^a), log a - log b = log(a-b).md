---
version: 1
type: video
provider: YouTube
id: yEAxG_D1HDw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Logarithms/Proof-%20a%20log%20b%20%3D%20log%28b%5Ea%29%2C%20log%20a%20-%20log%20b%20%3D%20log%28a-b%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 03fc5f39-8acc-4857-a727-f86659667947
updated: 1486069571
title: 'Proof- a log b = log(b^a), log a - log b = log(a/b)'
categories:
    - Logarithms
thumbnail_urls:
    - https://i3.ytimg.com/vi/yEAxG_D1HDw/default.jpg
    - https://i3.ytimg.com/vi/yEAxG_D1HDw/1.jpg
    - https://i3.ytimg.com/vi/yEAxG_D1HDw/2.jpg
    - https://i3.ytimg.com/vi/yEAxG_D1HDw/3.jpg
---
Proofs of the logarithm properties: A(log B) = log (B^A) and log A - log B = log (A/B)

Watch the next lesson: https://www.khanacademy.org/math/algebra2/logarithms-tutorial/logarithm_properties/v/change-of-base-formula-proof?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/logarithms-tutorial/logarithm_properties/v/proof-log-a-log-b-log-ab?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
