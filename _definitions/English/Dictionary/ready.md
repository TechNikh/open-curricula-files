---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ready
offline_file: ""
offline_thumbnail: ""
uuid: 99a12ae0-8def-4459-8e2e-cfa7c1d92581
updated: 1484310216
title: ready
categories:
    - Dictionary
---
ready
     adj 1: completely prepared or in condition for immediate action or
            use or progress; "get ready"; "she is ready to
            resign"; "the bridge is ready to collapse"; "I am
            ready to work"; "ready for action"; "ready for use";
            "the soup will be ready in a minute"; "ready to learn
            to read" [ant: {unready}]
     2: (of especially money) immediately available; "he seems to
        have ample ready money"; "a ready source of cash" [syn: {ready(a)}]
     3: mentally disposed; "he was ready to believe her"
     4: brought into readiness; "dinner is ready"
     5: apprehending and responding with speed and sensitivity; "a
        quick ...
