---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sentiment
offline_file: ""
offline_thumbnail: ""
uuid: 5b4bcc28-7dce-4c55-adac-51d8ad03d539
updated: 1484310607
title: sentiment
categories:
    - Dictionary
---
sentiment
     n 1: tender, romantic, or nostalgic feeling or emotion
     2: a personal belief or judgment that is not founded on proof
        or certainty; "my opinion differs from yours"; "what are
        your thoughts on Haiti?" [syn: {opinion}, {persuasion}, {view},
         {thought}]
