---
version: 1
type: video
provider: YouTube
id: 00MWVEwWOG4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 37ac4287-6bcf-41a9-8da8-767074197104
updated: 1486069662
title: 'Valence Bond Theory - Part 3 | Chemistry Animation Video | Hybridization Lecture and Hybridization'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/00MWVEwWOG4/default.jpg
    - https://i3.ytimg.com/vi/00MWVEwWOG4/1.jpg
    - https://i3.ytimg.com/vi/00MWVEwWOG4/2.jpg
    - https://i3.ytimg.com/vi/00MWVEwWOG4/3.jpg
---
