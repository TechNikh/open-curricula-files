---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saudi
offline_file: ""
offline_thumbnail: ""
uuid: c24cbd8f-3bb3-4e4a-ab32-4bdadea73fed
updated: 1484310517
title: saudi
categories:
    - Dictionary
---
Saudi
     adj : of or relating to Saudi Arabia or its people; "the
           Saudi-Arabian desert"; "the Saudi Royal family" [syn: {Saudi-Arabian}]
     n : a native or inhabitant of Saudi Arabia [syn: {Saudi Arabian}]
