---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/showcase
offline_file: ""
offline_thumbnail: ""
uuid: 2608c126-ed12-4700-9126-9e3df859e852
updated: 1484310607
title: showcase
categories:
    - Dictionary
---
showcase
     n 1: a setting in which something can be displayed to best
          effect; "it was a showcase for democracy in Africa"
          [syn: {show window}]
     2: a glass container used to store and display items in a shop
        or museum or home [syn: {case}, {display case}]
