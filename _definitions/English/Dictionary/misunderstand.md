---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misunderstand
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572321
title: misunderstand
categories:
    - Dictionary
---
misunderstand
     v : interpret in the wrong way; "Don't misinterpret my comments
         as criticism"; "She misconstrued my remarks" [syn: {misconstrue},
          {misinterpret}, {misconceive}, {misapprehend}, {be amiss}]
     [also: {misunderstood}]
