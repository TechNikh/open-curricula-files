---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grocery
offline_file: ""
offline_thumbnail: ""
uuid: bcfb4d69-c583-409e-8333-f8f03bfe14b5
updated: 1484310393
title: grocery
categories:
    - Dictionary
---
grocery
     n 1: a marketplace where groceries are sold; "the grocery store
          included a meat market" [syn: {grocery store}, {food
          market}, {market}]
     2: (usually plural) consumer goods sold by a grocer [syn: {foodstuff}]
