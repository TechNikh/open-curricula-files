---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consultation
offline_file: ""
offline_thumbnail: ""
uuid: f774ce86-3c0c-4f87-94ff-8b3813b65b06
updated: 1484310593
title: consultation
categories:
    - Dictionary
---
consultation
     n 1: a conference (usually with someone important); "he had a
          consultation with the judge"; "he requested an audience
          with the king" [syn: {audience}, {interview}]
     2: a conference between two or more people to consider a
        particular question; "frequent consultations with his
        lawyer"; "a consultation of several medical specialists"
     3: the act of referring or consulting; "reference to an
        encyclopedia produced the answer" [syn: {reference}]
