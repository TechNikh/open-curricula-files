---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outgoing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414701
title: outgoing
categories:
    - Dictionary
---
outgoing
     adj 1: going out or away or of the past; "an outgoing steamship";
            "the outgoing president" [ant: {incoming}]
     2: at ease in talking to others [syn: {extroverted}, {forthcoming}]
     3: somewhat extroverted [syn: {extrovertish}]
