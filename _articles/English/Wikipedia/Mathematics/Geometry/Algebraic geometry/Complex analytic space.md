---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_analytic_space
offline_file: ""
offline_thumbnail: ""
uuid: df9a06c6-bc7f-4985-bd84-43a93ab1e22c
updated: 1484309138
title: Complex analytic space
categories:
    - Algebraic geometry
---
In mathematics, a complex analytic space is a generalization of a complex manifold which allows the presence of singularities. Complex analytic spaces are locally ringed spaces which are locally isomorphic to local model spaces, where a local model space is an open subset of the vanishing locus of a finite set of holomorphic functions.
