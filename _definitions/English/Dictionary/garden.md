---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/garden
offline_file: ""
offline_thumbnail: ""
uuid: 578a8fd2-33ab-4394-912f-07371f57d6ee
updated: 1484310307
title: garden
categories:
    - Dictionary
---
garden
     n 1: a plot of ground where plants are cultivated
     2: the flowers or vegetables or fruits or herbs that are
        cultivated in a garden
     3: a yard or lawn adjoining a house
     v : work in the garden; "My hobby is gardening"
