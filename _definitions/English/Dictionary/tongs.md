---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tongs
offline_file: ""
offline_thumbnail: ""
uuid: 58906728-88c3-477d-8bb1-e6553555ba0b
updated: 1484310373
title: tongs
categories:
    - Dictionary
---
tongs
     n : any of various devices for taking hold of objects; usually
         have two hinged legs with handles above and pointed hooks
         below [syn: {pair of tongs}]
