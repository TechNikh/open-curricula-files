---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conceive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425141
title: conceive
categories:
    - Dictionary
---
conceive
     v 1: have the idea for; "He conceived of a robot that would help
          paralyzed patients"; "This library was well conceived"
          [syn: {gestate}, {conceptualize}, {conceptualise}]
     2: judge or regard; look upon; judge; "I think he is very
        smart"; "I believe her to be very smart"; "I think that he
        is her boyfriend"; "The racist conceives such people to be
        inferior" [syn: {think}, {believe}, {consider}]
     3: become pregnant; undergo conception; "She cannot conceive";
        "My daughter was conceived in Christmas Day"
