---
version: 1
type: video
provider: YouTube
id: 8hb2jJx-JHw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: df460036-e676-494a-bd50-bf6c41c4105e
updated: 1486069647
title: 'mindSpiral - Hiraku Opens The Screen'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/8hb2jJx-JHw/default.jpg
    - https://i3.ytimg.com/vi/8hb2jJx-JHw/1.jpg
    - https://i3.ytimg.com/vi/8hb2jJx-JHw/2.jpg
    - https://i3.ytimg.com/vi/8hb2jJx-JHw/3.jpg
---
