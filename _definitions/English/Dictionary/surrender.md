---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surrender
offline_file: ""
offline_thumbnail: ""
uuid: 0679d106-8d53-4b48-ae57-b6f18e953f15
updated: 1484310573
title: surrender
categories:
    - Dictionary
---
surrender
     n 1: acceptance of despair [syn: {resignation}]
     2: a verbal act of admitting defeat [syn: {giving up}, {yielding}]
     3: the delivery of a principal into lawful custody
     4: the act of surrendering (under agreed conditions); "they
        were protected until the capitulation of the fort" [syn: {capitulation},
         {fall}]
     v 1: give up or agree to forego to the power or possession of
          another; "The last Taleban fighters finally surrendered"
          [syn: {give up}] [ant: {resist}]
     2: relinquish possession or control over; "The squatters had to
        surrender the building after the police moved in" [syn: {cede},
         {deliver}, {give ...
