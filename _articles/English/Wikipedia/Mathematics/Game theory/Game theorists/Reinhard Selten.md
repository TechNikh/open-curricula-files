---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reinhard_Selten
offline_file: ""
offline_thumbnail: ""
uuid: 716d2b74-8785-40b0-a220-83420393bb2a
updated: 1484309029
title: Reinhard Selten
tags:
    - Biography
    - Work
    - Bibliography
categories:
    - Game theorists
---
Reinhard Justus Reginald Selten (5 October 1930 – 23 August 2016) was a German economist, who won the 1994 Nobel Memorial Prize in Economic Sciences (shared with John Harsanyi and John Nash). He is also well known for his work in bounded rationality, and can be considered as one of the founding fathers of experimental economics.
