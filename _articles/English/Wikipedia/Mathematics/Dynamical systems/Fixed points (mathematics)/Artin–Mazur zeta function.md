---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Artin%E2%80%93Mazur_zeta_function'
offline_file: ""
offline_thumbnail: ""
uuid: 02c0ea33-1cbc-49e8-ba16-bac350f6da23
updated: 1484308782
title: Artin–Mazur zeta function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_5.jpg
categories:
    - Fixed points (mathematics)
---
In mathematics, the Artin–Mazur zeta function, named after Michael Artin and Barry Mazur, is a function that is used for studying the iterated functions that occur in dynamical systems and fractals.
