---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distressing
offline_file: ""
offline_thumbnail: ""
uuid: 4a829271-6797-42fe-989c-2643e586d7d4
updated: 1484310471
title: distressing
categories:
    - Dictionary
---
distressing
     adj 1: causing distress or worry or anxiety; "distressing (or
            disturbing) news"; "lived in heroic if something
            distressful isolation"; "a disturbing amount of
            crime"; "a revelation that was most perturbing"; "a
            new and troubling thought"; "in a particularly
            worrisome predicament"; "a worrying situation"; "a
            worrying time" [syn: {distressful}, {disturbing}, {perturbing},
             {troubling}, {worrisome}, {worrying}]
     2: bad; unfortunate; "my finances were in a deplorable state";
        "a lamentable decision"; "her clothes were in sad shape";
        "a sorry state of affairs" [syn: ...
