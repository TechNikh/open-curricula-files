---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/effectively
offline_file: ""
offline_thumbnail: ""
uuid: f9ed22c5-9cda-4933-9a99-40f0954d0124
updated: 1484310475
title: effectively
categories:
    - Dictionary
---
effectively
     adv 1: in an effective manner; "these are real problems that can be
            dealt with most effectively by rational discussion"
            [syn: {efficaciously}] [ant: {inefficaciously}, {inefficaciously}]
     2: in actuality or reality or fact; "she is effectively his
        wife"; "in effect, they had no choice" [syn: {in effect}]
