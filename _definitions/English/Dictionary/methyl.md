---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/methyl
offline_file: ""
offline_thumbnail: ""
uuid: e9be3255-f2ff-4b50-b3df-6464abe9dab4
updated: 1484310381
title: methyl
categories:
    - Dictionary
---
methyl
     n : the univalent radical CH3- derived from methane [syn: {methyl
         group}, {methyl radical}]
