---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/route
offline_file: ""
offline_thumbnail: ""
uuid: dc12e1cc-78e8-470e-8a21-351b8deeb61f
updated: 1484310212
title: route
categories:
    - Dictionary
---
route
     n 1: an established line of travel or access [syn: {path}, {itinerary}]
     2: an open way (generally public) for travel or transportation
        [syn: {road}]
     v 1: send documents or materials to appropriate destinations
     2: send via a specific route
     3: divert in a specified direction; "divert the low voltage to
        the engine cylinders"
