---
version: 1
type: video
provider: YouTube
id: 9iqN12hCn10
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Polar%20coordinates%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e44c677-020c-420e-82a1-20e59a4b6cf0
updated: 1486069696
title: Polar coordinates 3
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/9iqN12hCn10/default.jpg
    - https://i3.ytimg.com/vi/9iqN12hCn10/1.jpg
    - https://i3.ytimg.com/vi/9iqN12hCn10/2.jpg
    - https://i3.ytimg.com/vi/9iqN12hCn10/3.jpg
---
