---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
offline_file: ""
offline_thumbnail: ""
uuid: 8bae04ed-ab4f-4578-8a40-00759a9409b8
updated: 1484308573
title: Longest common subsequence problem
tags:
    - Complexity
    - Solution for two sequences
    - Prefixes
    - First property
    - Second property
    - LCS function defined
    - Worked example
    - Traceback approach
    - Relation to other problems
    - Code for the dynamic programming solution
    - Computing the length of the LCS
    - Reading out an LCS
    - Reading out all LCSs
    - Print the diff
    - Example
    - Code optimization
    - Reduce the problem set
    - Reduce the comparison time
    - Reduce strings to hashes
    - Reduce the required space
    - Further optimized algorithms
    - Behavior on random strings
categories:
    - Combinatorics
---
The longest common subsequence (LCS) problem is the problem of finding the longest subsequence common to all sequences in a set of sequences (often just two sequences). It differs from problems of finding common substrings: unlike substrings, subsequences are not required to occupy consecutive positions within the original sequences. The longest common subsequence problem is a classic computer science problem, the basis of data comparison programs such as the diff utility, and has applications in bioinformatics. It is also widely used by revision control systems such as Git for reconciling multiple changes made to a revision-controlled collection of files.
