---
version: 1
type: video
provider: YouTube
id: JX52Ox2u2AE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 018deac5-439f-452e-b1f3-8098fa6a3b3d
updated: 1486069617
title: Discriminant for types of solution for a quadratic equations
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/JX52Ox2u2AE/default.jpg
    - https://i3.ytimg.com/vi/JX52Ox2u2AE/1.jpg
    - https://i3.ytimg.com/vi/JX52Ox2u2AE/2.jpg
    - https://i3.ytimg.com/vi/JX52Ox2u2AE/3.jpg
---
