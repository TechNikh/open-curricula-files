---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Borsuk%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: c090238b-0648-467c-b338-cac3bdce84f0
updated: 1484308598
title: "Borsuk's conjecture"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Borsuk_Hexagon.svg.png
tags:
    - Problem
categories:
    - Discrete geometry
---
