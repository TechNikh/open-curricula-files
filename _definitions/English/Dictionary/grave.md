---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grave
offline_file: ""
offline_thumbnail: ""
uuid: e7f0dc9a-69a9-4624-a20b-3c4ecf99d71f
updated: 1484310563
title: grave
categories:
    - Dictionary
---
grave
     adj 1: dignified and somber in manner or character and committed to
            keeping promises; "a grave God-fearing man"; "a quiet
            sedate nature"; "as sober as a judge"; "a solemn
            promise"; "the judge was solemn as he pronounced
            sentence" [syn: {sedate}, {sober}, {solemn}]
     2: causing fear or anxiety by threatening great harm; "a
        dangerous operation"; "a grave situation"; "a grave
        illness"; "grievous bodily harm"; "a serious wound"; "a
        serious turn of events"; "a severe case of pneumonia"; "a
        life-threatening disease" [syn: {dangerous}, {grievous}, {serious},
         {severe}, {life-threatening}]
     3: ...
