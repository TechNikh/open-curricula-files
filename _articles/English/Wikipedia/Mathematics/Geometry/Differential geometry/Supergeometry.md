---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supergeometry
offline_file: ""
offline_thumbnail: ""
uuid: a75f2f58-3f60-428b-ac71-b005fb83e143
updated: 1484309202
title: Supergeometry
categories:
    - Differential geometry
---
Supergeometry is differential geometry of modules over graded commutative algebras, supermanifolds and graded manifolds. Supergeometry is part and parcel of many classical and quantum field theories involving odd fields, e.g., SUSY field theory, BRST theory, or supergravity.
