---
version: 1
type: video
provider: YouTube
id: ZzwuHS9ldbY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Work%20and%20energy/Intro%20to%20springs%20and%20Hooke%27s%20law.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bee1cd8f-8ee4-42ba-a6e2-3d14b600c328
updated: 1486069591
title: "Intro to springs and Hooke's law"
categories:
    - Work and energy
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZzwuHS9ldbY/default.jpg
    - https://i3.ytimg.com/vi/ZzwuHS9ldbY/1.jpg
    - https://i3.ytimg.com/vi/ZzwuHS9ldbY/2.jpg
    - https://i3.ytimg.com/vi/ZzwuHS9ldbY/3.jpg
---
Introduction to Hooke's Law. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/work-and-energy/hookes-law/v/potential-energy-stored-in-a-spring?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/work-and-energy/work-and-energy-tutorial/v/power?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
