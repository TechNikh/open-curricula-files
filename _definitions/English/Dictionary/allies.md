---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allies
offline_file: ""
offline_thumbnail: ""
uuid: ed156e1d-27a6-4225-8167-661138ef4c1d
updated: 1484310551
title: allies
categories:
    - Dictionary
---
Allies
     n 1: the alliance of nations that fought the Axis in World War II
          and which (with subsequent additions) signed the charter
          of the United Nations in 1945
     2: in World War I the alliance of Great Britain and France and
        Russia and all the other nations that became allied with
        them in opposing the Central Powers
     3: an alliance of nations joining together to fight a common
        enemy
