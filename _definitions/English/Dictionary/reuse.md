---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reuse
offline_file: ""
offline_thumbnail: ""
uuid: cdf78bce-8b98-44c3-84eb-6ee1508eb3a4
updated: 1484310246
title: reuse
categories:
    - Dictionary
---
reuse
     v : use again after processing; "We must recycle the cardboard
         boxes" [syn: {recycle}, {reprocess}]
