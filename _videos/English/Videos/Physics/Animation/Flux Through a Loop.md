---
version: 1
type: video
provider: YouTube
id: RIf7wVo9Trg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Flux%20Through%20a%20Loop.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a98dcd0f-ac9f-4a6d-8327-2ee572009c30
updated: 1486069649
title: Flux Through a Loop
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/RIf7wVo9Trg/default.jpg
    - https://i3.ytimg.com/vi/RIf7wVo9Trg/1.jpg
    - https://i3.ytimg.com/vi/RIf7wVo9Trg/2.jpg
    - https://i3.ytimg.com/vi/RIf7wVo9Trg/3.jpg
---
