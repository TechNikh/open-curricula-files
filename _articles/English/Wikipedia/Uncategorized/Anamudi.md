---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anaimudi
offline_file: ""
offline_thumbnail: ""
uuid: 0cc3be2c-dcd4-4280-bc15-234ebf67233d
updated: 1484308422
title: Anamudi
tags:
    - Climatic zones and biomes
    - Citations
categories:
    - Uncategorized
---
Anamudi or Anai Mudi[5] (Malayalam pronunciation: ​[aːnɐmʊɖi]) is a mountain located in the Indian state of Kerala. It is the highest peak in the Western Ghats[6] and South India, at an elevation of 2,695 metres (8,842 ft),[1][2] and a topographic prominence of 2,479 metres (8,133 ft).[3] The name Anamudi literally translates to "elephant's forehead," a reference to the resemblance of the mountain to an elephant's head.[7]
