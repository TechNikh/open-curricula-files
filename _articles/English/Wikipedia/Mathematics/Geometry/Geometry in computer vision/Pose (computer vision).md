---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pose_(computer_vision)
offline_file: ""
offline_thumbnail: ""
uuid: 227d2d71-3da5-4dc7-ae0d-15a8e6167ed7
updated: 1484309181
title: Pose (computer vision)
categories:
    - Geometry in computer vision
---
In computer vision and in robotics, a typical task is to identify specific objects in an image and to determine each object's position and orientation relative to some coordinate system. This information can then be used, for example, to allow a robot to manipulate an object or to avoid moving into the object. The combination of position and orientation is referred to as the pose of an object, even though this concept is sometimes used only to describe the orientation. Exterior orientation and Translation are also used as synonyms to pose.
