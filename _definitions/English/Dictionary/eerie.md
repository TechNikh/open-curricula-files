---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eerie
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596261
title: eerie
categories:
    - Dictionary
---
eerie
     adj 1: suggestive of the supernatural; mysterious; "an eerie
            feeling of deja vu" [syn: {eery}, {spooky}]
     2: so strange as to inspire a feeling of fear; "an
        uncomfortable and eerie stillness in the woods"; "an eerie
        midnight howl" [syn: {eery}]
     [also: {eeriest}, {eerier}]
