---
version: 1
type: article
id: https://en.wikipedia.org/wiki/J.C.C._McKinsey
offline_file: ""
offline_thumbnail: ""
uuid: a587302b-b777-462c-988e-208c66008dd2
updated: 1484308506
title: J.C.C. McKinsey
tags:
    - Biography
    - Bibliography
    - Notes
categories:
    - Game theorists
---
John Charles Chenoweth McKinsey (30 April 1908 – 26 October 1953) (also known as J. C. C. McKinsey or Chen McKinsey)[1]:p. 141) was an American mathematician known for his work on mathematical logic and game theory.[2] He also made significant contributions to modal logic.[3]
