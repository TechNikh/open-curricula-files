---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diametrically
offline_file: ""
offline_thumbnail: ""
uuid: bc42eaee-acae-48a9-8260-99f7f29713dc
updated: 1484310192
title: diametrically
categories:
    - Dictionary
---
diametrically
     adv : as from opposite ends of a diameter; "when two honest
           witnesses give accounts of the same event that differ
           diametrically, how can anyone prove that the evidence
           you gave was deliberately false?"; "three of these
           brushes were approximately 120 feet apart and the
           fourth diametrically opposite to one of the three"
