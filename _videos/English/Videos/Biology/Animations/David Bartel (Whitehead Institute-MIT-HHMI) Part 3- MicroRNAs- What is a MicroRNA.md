---
version: 1
type: video
provider: YouTube
id: ACwOtzTiFQQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4252a858-8816-4fc9-bc67-5c5f9e54f808
updated: 1486069670
title: 'David Bartel (Whitehead Institute/MIT/HHMI) Part 3- MicroRNAs- What is a MicroRNA?'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ACwOtzTiFQQ/default.jpg
    - https://i3.ytimg.com/vi/ACwOtzTiFQQ/1.jpg
    - https://i3.ytimg.com/vi/ACwOtzTiFQQ/2.jpg
    - https://i3.ytimg.com/vi/ACwOtzTiFQQ/3.jpg
---
