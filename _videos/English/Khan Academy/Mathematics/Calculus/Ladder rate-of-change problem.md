---
version: 1
type: video
provider: YouTube
id: hD3U65CcZ0Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Ladder%20rate-of-change%20problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 212cbad9-eeb9-45a3-974c-1400e09044b5
updated: 1486069694
title: Ladder rate-of-change problem
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/hD3U65CcZ0Q/default.jpg
    - https://i3.ytimg.com/vi/hD3U65CcZ0Q/1.jpg
    - https://i3.ytimg.com/vi/hD3U65CcZ0Q/2.jpg
    - https://i3.ytimg.com/vi/hD3U65CcZ0Q/3.jpg
---
