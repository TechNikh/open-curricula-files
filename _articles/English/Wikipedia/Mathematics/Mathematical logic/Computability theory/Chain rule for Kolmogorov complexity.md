---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Chain_rule_for_Kolmogorov_complexity
offline_file: ""
offline_thumbnail: ""
uuid: d07a025e-fec8-4083-94a4-c607351d7719
updated: 1484309481
title: Chain rule for Kolmogorov complexity
categories:
    - Computability theory
---
That is, the combined randomness of two sequences X and Y is the sum of the randomness of X plus whatever randomness is left in Y once we know X. This follows immediately from the definitions of conditional and joint entropy fact from probability theory that the joint probability is the product of the marginal and conditional probability:
