---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diet
offline_file: ""
offline_thumbnail: ""
uuid: b75b1ef4-aa54-4173-985e-e2eff2a1fdaf
updated: 1484310535
title: diet
categories:
    - Dictionary
---
diet
     n 1: a prescribed selection of foods
     2: a legislative assembly in certain countries (e.g., Japan)
     3: the usual food and drink consumed by an organism (person or
        animal)
     4: the act of restricting your food intake (or your intake of
        particular foods) [syn: {dieting}]
     v 1: follow a regimen or a diet, as for health reasons; "He has
          high blood pressure and must stick to a low-salt diet"
     2: eat sparingly, for health reasons or to lose weight
