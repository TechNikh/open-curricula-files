---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mno
offline_file: ""
offline_thumbnail: ""
uuid: ca71c7ae-dbf8-45c2-b98b-4666ce43bf03
updated: 1484308313
title: mno
categories:
    - Uncategorized
---
mno is a file extension created by some web designing software like UltraDev or Dreamweaver. It often goes after the existing extension of the webpage (.html, .asp, .php), in effect creating a double extension, for instance, filename.html.mno.
