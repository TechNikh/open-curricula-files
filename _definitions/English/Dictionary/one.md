---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/one
offline_file: ""
offline_thumbnail: ""
uuid: 13f13686-47c7-4925-97a4-27f31b14f521
updated: 1484310359
title: one
categories:
    - Dictionary
---
one
     adj 1: used of a single unit or thing; not two or more; "`ane' is
            Scottish" [syn: {1}, {i}, {ane}]
     2: particular but unspecified; "early one evening" [syn: {one(a)}]
     3: having the indivisible character of a unit; "a unitary
        action"; "spoke with one voice" [syn: {one(a)}, {unitary}]
     4: of the same kind or quality; "two animals of one species"
        [syn: {one(a)}]
     5: used informally as an intensifier; "that is one fine dog"
        [syn: {one(a)}]
     6: indefinite in time or position; "he will come one day"; "one
        place or another" [syn: {one(a)}]
     7: being the single appropriate individual of a kind; only;
        "the one ...
