---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eugene_M._Luks
offline_file: ""
offline_thumbnail: ""
uuid: 5184226c-9b6e-4f38-80e5-8f16d38b74cb
updated: 1484309470
title: Eugene M. Luks
tags:
    - Professional career
    - Awards and honors
    - Selected publications
categories:
    - Graph theorists
---
Eugene Michael Luks (born circa 1940)[1] is an American mathematician and computer scientist, a professor emeritus of computer and information science at the University of Oregon. He is known for his research on the graph isomorphism problem and on algorithms for computational group theory.
