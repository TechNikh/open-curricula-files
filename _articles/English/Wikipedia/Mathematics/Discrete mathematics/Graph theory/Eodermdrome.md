---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eodermdrome
offline_file: ""
offline_thumbnail: ""
uuid: c8e4ece4-1b6d-42a6-b533-4f88dab7bbca
updated: 1484308616
title: Eodermdrome
categories:
    - Graph theory
---
An eodermdrome is a form of word play wherein a word (or phrase) is formed from a set of letters (or words) in such a way that it has a non-planar spelling net. The eodermdrome was conceived and described by Gary S. Bloom, John W. Kennedy, and Peter J. Wexler in Word Ways: The Journal of Recreational Linguistics in 1980.[1]
