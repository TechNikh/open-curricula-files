---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clock_position
offline_file: ""
offline_thumbnail: ""
uuid: 49f1302b-de32-4d9d-9090-963222527c49
updated: 1484309293
title: Clock position
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Horlo%25C4%259Do_001.svg.png'
categories:
    - Orientation (geometry)
---
A clock position is the relative direction of an object described using the analogy of a 12-hour clock to describe angles and directions. One imagines a clock face lying either upright or flat in front of oneself, and identifies the twelve hour markings with the directions in which they point.
