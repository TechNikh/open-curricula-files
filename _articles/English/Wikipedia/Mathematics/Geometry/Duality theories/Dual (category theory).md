---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_(category_theory)
offline_file: ""
offline_thumbnail: ""
uuid: e8443b0e-1b2d-4b6b-ba8a-c24c23ed0407
updated: 1484309232
title: Dual (category theory)
tags:
    - Formal definition
    - Examples
categories:
    - Duality theories
---
In category theory, a branch of mathematics, duality is a correspondence between the properties of a category C and the dual properties of the opposite category Cop. Given a statement regarding the category C, by interchanging the source and target of each morphism as well as interchanging the order of composing two morphisms, a corresponding dual statement is obtained regarding the opposite category Cop. Duality, as such, is the assertion that truth is invariant under this operation on statements. In other words, if a statement is true about C, then its dual statement is true about Cop. Also, if a statement is false about C, then its dual has to be false about Cop.
