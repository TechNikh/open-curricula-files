---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/astonishing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636701
title: astonishing
categories:
    - Dictionary
---
astonishing
     adj 1: surprising greatly; "she does an amazing amount of work";
            "the dog was capable of astonishing tricks" [syn: {amazing}]
     2: so surprisingly impressive as to stun or overwhelm; "such an
        enormous response was astonishing"; "an astounding
        achievement"; "the amount of money required was
        staggering"; "suffered a staggering defeat"; "the figure
        inside the boucle dress was stupefying" [syn: {astounding},
         {staggering}, {stupefying}]
