---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/however
offline_file: ""
offline_thumbnail: ""
uuid: b48a8e85-ad39-488b-9e6d-0e3974b23edb
updated: 1484310346
title: however
categories:
    - Dictionary
---
however
     adv 1: despite anything to the contrary (usually following a
            concession); "although I'm a little afraid, however
            I'd like to try it"; "while we disliked each other,
            nevertheless we agreed"; "he was a stern yet fair
            master"; "granted that it is dangerous, all the same I
            still want to go" [syn: {nevertheless}, {withal}, {still},
             {yet}, {all the same}, {even so}, {nonetheless}, {notwithstanding}]
     2: by contrast; on the other hand; "the first part was easy;
        the second, however, took hours"
     3: to whatever degree or extent; "The results, however general,
        are important"; "they have ...
