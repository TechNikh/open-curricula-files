---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hide-and-seek
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500501
title: hide-and-seek
categories:
    - Dictionary
---
hide-and-seek
     n : a game in which a child covers his eyes while the other
         players hide then tries to find them [syn: {hide and go
         seek}]
