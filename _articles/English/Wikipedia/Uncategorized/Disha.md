---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disha
offline_file: ""
offline_thumbnail: ""
uuid: 787c21fe-f7f8-4f97-9912-48e6481cabff
updated: 1484308438
title: Disha
categories:
    - Uncategorized
---
Disha (The Uprooted) was a 1990 Hindi film directed by Sai Paranjpye, based on the plight of immigrant workers in urban India, and starring Shabana Azmi, Nana Patekar, Om Puri in lead roles.
