---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inundation
offline_file: ""
offline_thumbnail: ""
uuid: 71fba210-8474-4936-b4ee-e1a74c331c93
updated: 1484310581
title: inundation
categories:
    - Dictionary
---
inundation
     n 1: the rising of a body of water and its overflowing onto
          normally dry land; "plains fertilized by annual
          inundations" [syn: {flood}, {deluge}, {alluvion}]
     2: an overwhelming number or amount; "a flood of requests"; "a
        torrent of abuse" [syn: {flood}, {deluge}, {torrent}]
