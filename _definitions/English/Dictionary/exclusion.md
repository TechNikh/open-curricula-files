---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exclusion
offline_file: ""
offline_thumbnail: ""
uuid: 91c6f4c5-1f69-4f15-9f5b-68035fd90e56
updated: 1484310393
title: exclusion
categories:
    - Dictionary
---
exclusion
     n 1: the state of being excluded [ant: {inclusion}]
     2: the state of being excommunicated [syn: {excommunication}, {censure}]
     3: a deliberate act of omission; "with the exception of the
        children, everyone was told the news" [syn: {exception}, {elision}]
     4: the act of forcing out someone or something; "the ejection
        of troublemakers by the police"; "the child's expulsion
        from school" [syn: {ejection}, {expulsion}, {riddance}]
