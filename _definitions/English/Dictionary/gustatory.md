---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gustatory
offline_file: ""
offline_thumbnail: ""
uuid: 7d4f393b-2e98-4d90-820e-6f7493b228ab
updated: 1484310363
title: gustatory
categories:
    - Dictionary
---
gustatory
     adj : of or relating to gustation [syn: {gustative}, {gustatorial}]
