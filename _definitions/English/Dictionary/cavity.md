---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cavity
offline_file: ""
offline_thumbnail: ""
uuid: e3aefdcb-cd30-4ba7-b7d3-709a485efb2a
updated: 1484310337
title: cavity
categories:
    - Dictionary
---
cavity
     n 1: a sizeable hole (usually in the ground); "they dug a pit to
          bury the body" [syn: {pit}]
     2: space that is surrounded by something [syn: {enclosed space}]
     3: soft decayed area in a tooth; progressive decay can lead to
        the death of a tooth [syn: {caries}, {dental caries}, {tooth
        decay}]
     4: (anatomy) a natural hollow or sinus within the body [syn: {bodily
        cavity}, {cavum}]
