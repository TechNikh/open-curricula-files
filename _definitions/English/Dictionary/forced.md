---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forced
offline_file: ""
offline_thumbnail: ""
uuid: 35388893-2be7-4d43-8ec4-4eb1d8373f94
updated: 1484310249
title: forced
categories:
    - Dictionary
---
forced
     adj 1: produced by or subjected to forcing; "forced-air heating";
            "furnaces of the forced-convection type"; "forced
            convection in plasma generators"
     2: forced or compelled; "promised to abolish forced labor"
     3: made necessary by an unexpected situation or emergency; "a
        forced landing" [syn: {unexpected}]
     4: lacking spontaneity; not natural; "a constrained smile";
        "forced heartiness"; "a strained smile" [syn: {constrained},
         {strained}]
