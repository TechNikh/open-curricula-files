---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digvijay
offline_file: ""
offline_thumbnail: ""
uuid: 653bedfa-3e02-48d8-aac5-7fc401f1515b
updated: 1484308466
title: Digvijay (Kerala cricketer)
categories:
    - Uncategorized
---
Digvijay (full name unknown; born in Palghat) was an Indian cricketer. He was a right-handed batsman and right-arm medium-pace bowler who played for Kerala. He was born in Palghat.
