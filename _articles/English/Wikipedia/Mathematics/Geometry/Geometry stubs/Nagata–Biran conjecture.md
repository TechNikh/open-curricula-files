---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Nagata%E2%80%93Biran_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 87f9fe70-e240-45ca-a5da-214aa449eba5
updated: 1484309365
title: Nagata–Biran conjecture
categories:
    - Geometry stubs
---
In mathematics, the Nagata–Biran conjecture, named after Masayoshi Nagata and Paul Biran, is a generalisation of the Nagata conjecture to arbitrary polarised surfaces.
