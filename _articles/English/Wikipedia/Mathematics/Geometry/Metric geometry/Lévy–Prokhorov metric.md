---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A9vy%E2%80%93Prokhorov_metric'
offline_file: ""
offline_thumbnail: ""
uuid: 372b2d53-332a-4698-84a1-8abf8dd630bf
updated: 1484309270
title: Lévy–Prokhorov metric
tags:
    - Definition
    - Properties
categories:
    - Metric geometry
---
In mathematics, the Lévy–Prokhorov metric (sometimes known just as the Prokhorov metric) is a metric (i.e., a definition of distance) on the collection of probability measures on a given metric space. It is named after the French mathematician Paul Lévy and the Soviet mathematician Yuri Vasilyevich Prokhorov; Prokhorov introduced it in 1956 as a generalization of the earlier Lévy metric.
