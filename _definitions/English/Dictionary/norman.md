---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/norman
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484426401
title: norman
categories:
    - Dictionary
---
Norman
     adj 1: of or relating to or characteristic of Normandy; "Norman
            beaches"
     2: of or relating to or characteristic of the Normans; "the
        Norman Invasion in 1066"
     n 1: United States operatic soprano (born in 1945) [syn: {Jessye
          Norman}]
     2: Australian golfer (born in 1955) [syn: {Greg Norman}, {Gregory
        John Norman}]
     3: an inhabitant of Normandy
