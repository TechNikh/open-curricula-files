---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/variation
offline_file: ""
offline_thumbnail: ""
uuid: ad3c8819-c900-4665-b860-9fb6115d0923
updated: 1484310309
title: variation
categories:
    - Dictionary
---
variation
     n 1: an instance of change; the rate or magnitude of change [syn:
           {fluctuation}]
     2: an activity that varies from a norm or standard; "any
        variation in his routine was immediately reported" [syn: {variance}]
     3: a repetition of a musical theme in which it is modified or
        embellished
     4: something a little different from others of the same type;
        "an experimental version of the night fighter"; "an emery
        wheel is a modern variant of the grindstone"; "the boy is
        a younger edition of his father" [syn: {version}, {variant},
         {edition}]
     5: an artifact that deviates from a norm or standard; "he
        ...
