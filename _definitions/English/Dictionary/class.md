---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/class
offline_file: ""
offline_thumbnail: ""
uuid: a807596b-e0ec-4381-ae71-a5182f0e74ba
updated: 1484310361
title: class
categories:
    - Dictionary
---
class
     n 1: people having the same social or economic status; "the
          working class"; "an emerging professional class" [syn: {social
          class}, {socio-economic class}]
     2: a body of students who are taught together; "early morning
        classes are always sleepy" [syn: {form}, {grade}]
     3: education imparted in a series of lessons or class meetings;
        "he took a course in basket weaving"; "flirting is not
        unknown in college classes" [syn: {course}, {course of
        study}, {course of instruction}]
     4: a collection of things sharing a common attribute; "there
        are two classes of detergents" [syn: {category}, {family}]
     5: a body of ...
