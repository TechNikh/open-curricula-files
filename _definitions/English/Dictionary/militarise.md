---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/militarise
offline_file: ""
offline_thumbnail: ""
uuid: e3fe8bb0-655a-4b14-8a2c-6059f464e7f1
updated: 1484310571
title: militarise
categories:
    - Dictionary
---
militarise
     v 1: lend a military character to (a country), as by building up
          a military force; "militarize Germany again after the
          war" [syn: {militarize}] [ant: {demilitarize}, {demilitarize}]
     2: adopt for military use; "militarize the Civil Service" [syn:
         {militarize}]
