---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lidded
offline_file: ""
offline_thumbnail: ""
uuid: ca315f16-b56f-40cf-811f-8025d8379670
updated: 1484310226
title: lidded
categories:
    - Dictionary
---
lidded
     adj 1: having or covered with a lid or lids; often used in
            combination; "milk is left in a large lidded mug";
            "heavy-lidded eyes" [ant: {lidless}]
     2: having a lid; "milk in a heavy lidded mug"
