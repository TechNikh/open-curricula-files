---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usury
offline_file: ""
offline_thumbnail: ""
uuid: f1f684eb-7612-493f-b820-dd24eae8e23a
updated: 1484310573
title: usury
categories:
    - Dictionary
---
usury
     n 1: an exorbitant or unlawful rate of interest [syn: {vigorish}]
     2: the act of lending money at an exorbitant rate of interest
