---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dough
offline_file: ""
offline_thumbnail: ""
uuid: ed229a7a-a5b1-460e-8934-df13460a75bf
updated: 1484310377
title: dough
categories:
    - Dictionary
---
dough
     n 1: a flour mixture stiff enough to knead or roll
     2: informal terms for money [syn: {boodle}, {bread}, {cabbage},
         {clams}, {dinero}, {gelt}, {kale}, {lettuce}, {lolly}, {lucre},
         {loot}, {moolah}, {pelf}, {scratch}, {shekels}, {simoleons},
         {sugar}, {wampum}]
