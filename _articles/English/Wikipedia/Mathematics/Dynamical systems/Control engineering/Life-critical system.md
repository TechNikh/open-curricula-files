---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Life-critical_system
offline_file: ""
offline_thumbnail: ""
uuid: a5deacda-d7c9-4380-a5bd-a246bd1fa963
updated: 1484308742
title: Life-critical system
tags:
    - Reliability regimes
    - Software engineering for safety-critical systems
    - Examples of safety-critical systems
    - Infrastructure
    - 'Medicine[6]'
    - 'Nuclear engineering[7]'
    - Recreation
    - Transport
    - 'Railway[8]'
    - 'Automotive[10]'
    - 'Aviation[11]'
    - 'Spaceflight[12]'
categories:
    - Control engineering
---
A life-critical system or more commonly a safety-critical system[1] is a system whose failure or malfunction may result in one (or more) of the following outcomes:[citation needed]
