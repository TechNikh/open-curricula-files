---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Knuckle_Bash
offline_file: ""
offline_thumbnail: ""
uuid: 68dc9266-457a-473f-91eb-ef2a51f113a7
updated: 1484308531
title: Knuckle Bash
categories:
    - Cooperative games
---
Knuckle Bash (ナックルバッシュ?) is a 1993 side-scrolling beat 'em up arcade game developed and published by Toaplan in Japan, later released in North America and Europe by Atari.
