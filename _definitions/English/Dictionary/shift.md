---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shift
offline_file: ""
offline_thumbnail: ""
uuid: 78a41d1e-96e0-4685-bb1c-4fe10c443762
updated: 1484310208
title: shift
categories:
    - Dictionary
---
shift
     n 1: an event in which something is displaced without rotation
          [syn: {displacement}]
     2: a qualitative change [syn: {transformation}, {transmutation}]
     3: the time period during which you are at work [syn: {work
        shift}, {duty period}]
     4: the act of changing one thing or position for another; "his
        switch on abortion cost him the election" [syn: {switch},
        {switching}]
     5: the act of moving from one place to another; "his constant
        shifting disrupted the class" [syn: {shifting}]
     6: (geology) a crack in the earth's crust resulting from the
        displacement of one side with respect to the other; "they
        built it ...
