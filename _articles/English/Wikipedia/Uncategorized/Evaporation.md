---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vapourization
offline_file: ""
offline_thumbnail: ""
uuid: 989364eb-3fe5-47ba-889c-245fb31bb6c0
updated: 1484308329
title: Evaporation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Watervapor_cup.jpg
tags:
    - Theory
    - Evaporative equilibrium
    - Factors influencing the rate of evaporation
    - Thermodynamics
    - Applications
    - Combustion vaporization
    - Pre-combustion vaporization
    - Film deposition
categories:
    - Uncategorized
---
Evaporation is a type of vaporization of a liquid that occurs from the surface of a liquid into a gaseous phase that is not saturated with the evaporating substance. The other type of vaporization is boiling, which is characterized by bubbles of saturated vapor forming in the liquid phase. Steam produced in a boiler is another example of evaporation occurring in a saturated vapor phase. Evaporation that occurs directly from the solid phase below the melting point, as commonly observed with ice at or below freezing or moth crystals (napthalene or paradichlorobenzene), is called sublimation.
