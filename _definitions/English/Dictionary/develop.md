---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/develop
offline_file: ""
offline_thumbnail: ""
uuid: 2e0553a7-9320-4397-b2c6-bc43b5f63dae
updated: 1484310290
title: develop
categories:
    - Dictionary
---
develop
     v 1: make something new, such as a product or a mental or
          artistic creation; "Her company developed a new kind of
          building material that withstands all kinds of weather";
          "They developed a new technique"
     2: work out; "We have developed a new theory of evolution"
        [syn: {evolve}, {germinate}]
     3: gain through experience; "I acquired a strong aversion to
        television"; "Children must develop a sense of right and
        wrong"; "Dave developed leadership qualities in his new
        position"; "develop a passion for painting" [syn: {acquire},
         {evolve}]
     4: come to have or undergo a change of (physical features and
  ...
