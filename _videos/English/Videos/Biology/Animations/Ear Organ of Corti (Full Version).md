---
version: 1
type: video
provider: YouTube
id: 1JE8WduJKV4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Ear%20Organ%20of%20Corti%20%28Full%20Version%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 180a52ce-ce95-4050-8b9f-cedb5e2e5369
updated: 1486069670
title: Ear Organ of Corti (Full Version)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/1JE8WduJKV4/default.jpg
    - https://i3.ytimg.com/vi/1JE8WduJKV4/1.jpg
    - https://i3.ytimg.com/vi/1JE8WduJKV4/2.jpg
    - https://i3.ytimg.com/vi/1JE8WduJKV4/3.jpg
---
