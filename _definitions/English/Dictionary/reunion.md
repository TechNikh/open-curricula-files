---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reunion
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547421
title: reunion
categories:
    - Dictionary
---
reunion
     n 1: a party of former associates who have come together again
     2: the act of coming together again; "monetary unification
        precipitated the reunification of the German state in
        October 1990" [syn: {reunification}]
