---
version: 1
type: video
provider: YouTube
id: M_ADc9jkBig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%234c.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 53c823e0-7cb8-4360-9542-4b1d5ecdb9fa
updated: 1486069692
title: '2011 Calculus AB Free Response #4c'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/M_ADc9jkBig/default.jpg
    - https://i3.ytimg.com/vi/M_ADc9jkBig/1.jpg
    - https://i3.ytimg.com/vi/M_ADc9jkBig/2.jpg
    - https://i3.ytimg.com/vi/M_ADc9jkBig/3.jpg
---
