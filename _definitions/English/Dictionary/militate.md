---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/militate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484592901
title: militate
categories:
    - Dictionary
---
militate
     v : have force or influence; bring about an effect or change;
         "Politeness militated against this opinion being
         expressed"
