---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gland
offline_file: ""
offline_thumbnail: ""
uuid: db28a49a-8aed-44bb-bd22-26a0a69909ee
updated: 1484310611
title: gland
categories:
    - Dictionary
---
gland
     n : any of various organs that synthesize substances needed by
         the body and release it through ducts or directly into
         the bloodstream [syn: {secretory organ}, {secretor}, {secreter}]
