---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malt
offline_file: ""
offline_thumbnail: ""
uuid: 5086ea29-27e4-4f29-ad93-365d433d269e
updated: 1484310424
title: malt
categories:
    - Dictionary
---
malt
     n 1: a milkshake made with malt powder [syn: {malted}, {malted
          milk}]
     2: a lager of high alcohol content; by law it is considered too
        alcoholic to be sold as lager or beer [syn: {malt liquor}]
     3: a cereal grain that is kiln-dried after having been
        germinated by soaking in water; used especially in brewing
        and distilling
     v 1: treat with malt or malt extract; "malt beer"
     2: turn into malt, become malt
     3: convert grain into malt
     4: convert into malt
