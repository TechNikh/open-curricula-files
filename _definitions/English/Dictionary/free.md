---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/free
offline_file: ""
offline_thumbnail: ""
uuid: fe0a9c8b-bd79-49c3-863e-3f922a56532d
updated: 1484310355
title: free
categories:
    - Dictionary
---
free
     adj 1: able to act at will; not hampered; not under compulsion or
            restraint; "free enterprise"; "a free port"; "a free
            country"; "I have an hour free"; "free will"; "free of
            racism"; "feel free to stay as long as you wish"; "a
            free choice" [ant: {unfree}]
     2: unconstrained or not chemically bound in a molecule or not
        fixed and capable of relatively unrestricted motion; "free
        expansion"; "free oxygen"; "a free electron" [ant: {bound}]
     3: costing nothing; "complimentary tickets" [syn: {complimentary},
         {costless}, {gratis(p)}, {gratuitous}]
     4: not occupied or in use; "a free locker"; "a free lane"
 ...
