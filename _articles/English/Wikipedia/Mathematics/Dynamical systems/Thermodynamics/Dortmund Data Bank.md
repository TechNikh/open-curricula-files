---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dortmund_Data_Bank
offline_file: ""
offline_thumbnail: ""
uuid: b09c2e05-8a4d-4ed0-ba55-d4fba14fee25
updated: 1484308838
title: Dortmund Data Bank
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Benzene%252BHexafluorobenzene_%2528Double_Azeotrope%2529.png'
tags:
    - Contents
    - Mixture properties
    - Pure component properties
    - Data sources
    - History
    - Availability
categories:
    - Thermodynamics
---
The Dortmund Data Bank[1] (short DDB) is a factual data bank for thermodynamic and thermophysical data. Its main usage is the data supply for process simulation where experimental data are the basis for the design, analysis, synthesis, and optimization of chemical processes. The DDB is used for fitting parameters for thermodynamic models like NRTL or UNIQUAC and for many different equations describing pure component properties like e. g. the Antoine equation for vapor pressures. The DDB is also used for the development and revision of predictive methods like UNIFAC and PSRK.
