---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discharged
offline_file: ""
offline_thumbnail: ""
uuid: 7a790e58-4d42-4635-9ccf-588d22827a24
updated: 1484310202
title: discharged
categories:
    - Dictionary
---
discharged
     adj 1: set free as from prison or duty [syn: {released}]
     2: having lost your job [syn: {dismissed}, {fired}, {laid-off},
         {pink-slipped}]
