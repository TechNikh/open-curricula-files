---
version: 1
type: video
provider: YouTube
id: rcLnMe1ELPA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20the%20galaxy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dd1bb882-c294-414b-aa0a-0d664af963f4
updated: 1486069686
title: Scale of the galaxy
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/rcLnMe1ELPA/default.jpg
    - https://i3.ytimg.com/vi/rcLnMe1ELPA/1.jpg
    - https://i3.ytimg.com/vi/rcLnMe1ELPA/2.jpg
    - https://i3.ytimg.com/vi/rcLnMe1ELPA/3.jpg
---
