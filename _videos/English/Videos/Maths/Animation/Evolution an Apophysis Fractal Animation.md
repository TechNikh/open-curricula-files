---
version: 1
type: video
provider: YouTube
id: cxOPEb6NQp0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c53cfa8d-3e48-4c7b-9776-de665ee0ac25
updated: 1486069643
title: Evolution an Apophysis Fractal Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/cxOPEb6NQp0/default.jpg
    - https://i3.ytimg.com/vi/cxOPEb6NQp0/1.jpg
    - https://i3.ytimg.com/vi/cxOPEb6NQp0/2.jpg
    - https://i3.ytimg.com/vi/cxOPEb6NQp0/3.jpg
---
