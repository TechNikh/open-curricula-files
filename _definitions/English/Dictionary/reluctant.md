---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reluctant
offline_file: ""
offline_thumbnail: ""
uuid: dba8c38a-2827-4f76-ae61-92333e83346c
updated: 1484310407
title: reluctant
categories:
    - Dictionary
---
reluctant
     adj 1: unwillingness to do something contrary to your custom; "a
            reluctant smile"; "loath to admit a mistake";
            "unwilling to face facts" [syn: {loath}, {loth}]
     2: unwilling to become involved; "they were usually reluctant
        to socialize"; "reluctant to help"
     3: not eager; "foreigners stubbornly uneager to accept our
        ways"; "fresh from college and uneager for the moment to
        marry him"; "reluctant to help"
