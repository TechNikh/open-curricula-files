---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fair_coin
offline_file: ""
offline_thumbnail: ""
uuid: b3194637-288b-488f-9a15-ddd1b68e4eaa
updated: 1484309040
title: Fair coin
tags:
    - Role in statistical teaching and theory
    - Fair results from a biased coin
categories:
    - Games (probability)
---
In probability theory and statistics, a sequence of independent Bernoulli trials with probability 1/2 of success on each trial is metaphorically called a fair coin. One for which the probability is not 1/2 is called a biased or unfair coin. In theoretical studies, the assumption that a coin is fair is often made by referring to an ideal coin.
