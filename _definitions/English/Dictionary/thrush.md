---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thrush
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484610601
title: thrush
categories:
    - Dictionary
---
thrush
     n 1: candidiasis of the oral cavity; seen mostly in infants or
          debilitated adults
     2: a woman who sings popular songs
     3: songbirds characteristically having brownish upper plumage
        with a spotted breast
