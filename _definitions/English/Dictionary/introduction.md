---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/introduction
offline_file: ""
offline_thumbnail: ""
uuid: f2b747c0-3d4a-492d-be4e-cf7458de2656
updated: 1484310583
title: introduction
categories:
    - Dictionary
---
introduction
     n 1: the first section of a communication
     2: the act of beginning something new; "they looked forward to
        the debut of their new product line" [syn: {debut}, {first
        appearance}, {launching}, {unveiling}, {entry}]
     3: formally making a person known to another or to the public
        [syn: {presentation}, {intro}]
     4: a basic or elementary instructional text
     5: a new proposal; "they resisted the introduction of
        impractical alternatives"
     6: the act of putting one thing into another [syn: {insertion},
         {intromission}]
     7: the act of starting something for the first time;
        introducing something new; "she looked ...
