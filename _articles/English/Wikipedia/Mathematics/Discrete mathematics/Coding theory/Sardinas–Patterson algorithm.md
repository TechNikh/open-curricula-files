---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sardinas%E2%80%93Patterson_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 78904975-2779-401e-b3be-0219de1d7964
updated: 1484308556
title: Sardinas–Patterson algorithm
tags:
    - Idea of the algorithm
    - Precise description of the algorithm
    - Termination and correctness of the algorithm
    - Notes
categories:
    - Coding theory
---
In coding theory, the Sardinas–Patterson algorithm is a classical algorithm for determining in polynomial time whether a given variable-length code is uniquely decodable, named after August Albert Sardinas and George W. Patterson, who published it in 1953.[1] The algorithm carries out a systematic search for a string which admits two different decompositions into codewords. As Knuth reports, the algorithm was rediscovered about ten years later in 1963 by Floyd, despite the fact that it was at the time already well known in coding theory.[2]
