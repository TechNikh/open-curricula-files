---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fold-and-cut_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 72222126-4198-40f8-88a9-99b378a1e40c
updated: 1484309116
title: Fold-and-cut theorem
tags:
    - History
    - Solutions
    - Straight skeleton
    - Disk packing
categories:
    - Theorems in geometry
---
The fold-and-cut theorem states that any shape with straight sides can be cut from a single (idealized) sheet of paper by folding it flat and making a single straight complete cut.[1] Such shapes include polygons, which may be concave, shapes with holes, and collections of such shapes (i.e. the regions need not be connected).
