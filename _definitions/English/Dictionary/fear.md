---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fear
offline_file: ""
offline_thumbnail: ""
uuid: 0a3069da-2be5-4a09-b828-76e2b7fdc0f3
updated: 1484310518
title: fear
categories:
    - Dictionary
---
fear
     n 1: an emotion experienced in anticipation of some specific pain
          or danger (usually accompanied by a desire to flee or
          fight) [syn: {fearfulness}, {fright}] [ant: {fearlessness}]
     2: an anxious feeling; "care had aged him"; "they hushed it up
        out of fear of public reaction" [syn: {concern}, {care}]
     3: a profound emotion inspired by a deity; "the fear of God"
        [syn: {reverence}, {awe}, {veneration}]
     v 1: be afraid or feel anxious or apprehensive about a possible
          or probable situation or event; "I fear she might get
          aggressive"
     2: be afraid or scared of; be frightened of; "I fear the
        winters in ...
