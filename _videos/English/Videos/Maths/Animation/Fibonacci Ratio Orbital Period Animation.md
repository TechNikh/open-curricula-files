---
version: 1
type: video
provider: YouTube
id: HR9-hKXIrT4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 62e7dbb1-3017-4dd6-a4ee-c4e0fd25f410
updated: 1486069648
title: Fibonacci Ratio Orbital Period Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/HR9-hKXIrT4/default.jpg
    - https://i3.ytimg.com/vi/HR9-hKXIrT4/1.jpg
    - https://i3.ytimg.com/vi/HR9-hKXIrT4/2.jpg
    - https://i3.ytimg.com/vi/HR9-hKXIrT4/3.jpg
---
