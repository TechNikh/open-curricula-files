---
version: 1
type: video
provider: YouTube
id: W3MScyZbb64
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8b9b358f-f908-435f-8348-e4db95c8e848
updated: 1486069637
title: S2 Refraction Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/W3MScyZbb64/default.jpg
    - https://i3.ytimg.com/vi/W3MScyZbb64/1.jpg
    - https://i3.ytimg.com/vi/W3MScyZbb64/2.jpg
    - https://i3.ytimg.com/vi/W3MScyZbb64/3.jpg
---
