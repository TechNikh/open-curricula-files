---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kolmogorov_complexity
offline_file: ""
offline_thumbnail: ""
uuid: 737a6365-9bac-49cb-a8cb-816fa01d9e7b
updated: 1484309487
title: Kolmogorov complexity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/310px-Mandelpart2_red.png
tags:
    - Definition
    - Invariance theorem
    - Informal treatment
    - A more formal treatment
    - History and context
    - Basic Results
    - Uncomputability of Kolmogorov complexity
    - Chain rule for Kolmogorov complexity
    - Compression
    - "Chaitin's incompleteness theorem"
    - Minimum message length
    - Kolmogorov randomness
    - Relation to entropy
    - Conditional versions
    - Notes
categories:
    - Computability theory
---
In algorithmic information theory (a subfield of computer science and mathematics), the Kolmogorov complexity of an object, such as a piece of text, is the length of the shortest computer program (in a predetermined programming language) that produces the object as output. It is a measure of the computational resources needed to specify the object, and is also known as descriptive complexity, Kolmogorov–Chaitin complexity, algorithmic entropy, or program-size complexity. It is named after Andrey Kolmogorov, who first published on the subject in 1963.[1][2]
