---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Law_of_tangents
offline_file: ""
offline_thumbnail: ""
uuid: f2411afe-bff3-4bd4-a49c-6844fd9e67c4
updated: 1484309347
title: Law of tangents
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/512px-Johnny-automatic-scales-of-justice.svg_2.png
tags:
    - Proof
    - Application
    - Spherical version
    - History
    - Notes
categories:
    - Trigonometry
---
In trigonometry, the law of tangents[1] is a statement about the relationship between the tangents of two angles of a triangle and the lengths of the opposing sides.
