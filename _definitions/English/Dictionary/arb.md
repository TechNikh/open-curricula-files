---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arb
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484335141
title: arb
categories:
    - Dictionary
---
arb
     n : someone who engages in arbitrage (who purchases securities
         in one market for immediate resale in another in the hope
         of profiting from the price differential) [syn: {arbitrageur},
          {arbitrager}]
