---
version: 1
type: video
provider: YouTube
id: OSE3OiRGlgE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Subject%2C%20direct%20object%2C%20and%20indirect%20object.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9606e6da-bb17-46f5-9d1d-25784c49f575
updated: 1486069601
title: Subject, direct object, and indirect object
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/OSE3OiRGlgE/default.jpg
    - https://i3.ytimg.com/vi/OSE3OiRGlgE/1.jpg
    - https://i3.ytimg.com/vi/OSE3OiRGlgE/2.jpg
    - https://i3.ytimg.com/vi/OSE3OiRGlgE/3.jpg
---
A subject is the noun phrase that drives the action of a sentence; in the sentence “Jake ate cereal,” Jake is the subject.  The direct object is the thing that the subject acts upon, so in that last sentence, “cereal” is the direct object; it’s the thing Jake ate.  An indirect object is an optional part of a sentence; it’s the recipient of an action.  In the sentence “Jake gave me some cereal,” the word “me” is the indirect object; I’m the person who got cereal from Jake.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/identifying-subject--direct-object--and-indirect-object 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/dangling-modifiers-syntax-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/subjects-and-predicates-syntax-khan-academy 


Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
