---
version: 1
type: video
provider: YouTube
id: GtaoP0skPWc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Finance%20%26%20Capital%20Markets/Interest%20and%20debt/Introduction%20to%20interest.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bf1a0485-4ebc-426e-948d-5a19aa35334b
updated: 1486069694
title: Introduction to interest
categories:
    - Interest and debt
thumbnail_urls:
    - https://i3.ytimg.com/vi/GtaoP0skPWc/default.jpg
    - https://i3.ytimg.com/vi/GtaoP0skPWc/1.jpg
    - https://i3.ytimg.com/vi/GtaoP0skPWc/2.jpg
    - https://i3.ytimg.com/vi/GtaoP0skPWc/3.jpg
---
