---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bench
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484315401
title: bench
categories:
    - Dictionary
---
bench
     n 1: a long seat for more than one person
     2: the reserve players on a team; "our team has a strong bench"
     3: a level shelf of land interrupting a declivity (with steep
        slopes above and below) [syn: {terrace}]
     4: persons who administer justice [syn: {judiciary}]
     5: a strong worktable for a carpenter or mechanic [syn: {workbench},
         {work bench}]
     6: the magistrate or judge or judges sitting in court in
        judicial capacity to compose the court collectively
     v 1: take out of a game; of players
     2: exhibit on a bench; "bench the poodles at the dog show"
