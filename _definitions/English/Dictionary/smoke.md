---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smoke
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424181
title: smoke
categories:
    - Dictionary
---
smoke
     n 1: a cloud of fine particles suspended in a gas [syn: {fume}]
     2: a hot vapor containing fine particles of carbon being
        produced by combustion; "the fire produced a tower of
        black smoke that could be seen for miles" [syn: {smoking}]
     3: an indication of some hidden activity; "with all that smoke
        there must be a fire somewhere"
     4: something with no concrete substance; "his dreams all turned
        to smoke"; "it was just smoke and mirrors"
     5: tobacco leaves that have been made into a cylinder [syn: {roll
        of tobacco}]
     6: street names for marijuana [syn: {pot}, {grass}, {green
        goddess}, {dope}, {weed}, {gage}, {sess}, ...
