---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Participation_constraint_(mechanism_design)
offline_file: ""
offline_thumbnail: ""
uuid: df43772d-0908-4087-b61a-041f5bc5a81f
updated: 1484309046
title: Participation constraint (mechanism design)
categories:
    - Mechanism design
---
In game theory, and particularly mechanism design, participation constraints or rational participation constraints are said to be satisfied if a mechanism leaves all participants at least as well off as they would have been if they hadn't participated.
