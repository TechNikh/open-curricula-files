---
version: 1
type: video
provider: YouTube
id: pqgcEIaXGME
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 39b34839-0644-41a1-abfe-18e8a92360ad
updated: 1486069681
title: 'Digestive System, part 2- Crash Course A&P #34'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/pqgcEIaXGME/default.jpg
    - https://i3.ytimg.com/vi/pqgcEIaXGME/1.jpg
    - https://i3.ytimg.com/vi/pqgcEIaXGME/2.jpg
    - https://i3.ytimg.com/vi/pqgcEIaXGME/3.jpg
---
