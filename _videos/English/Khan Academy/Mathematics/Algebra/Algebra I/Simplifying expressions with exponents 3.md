---
version: 1
type: video
provider: YouTube
id: zwUnRQbWPJU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Simplifying%20expressions%20with%20exponents%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e88172f9-3730-40d0-b8c3-b2a5e36c59ba
updated: 1486069563
title: Simplifying expressions with exponents 3
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/zwUnRQbWPJU/default.jpg
    - https://i3.ytimg.com/vi/zwUnRQbWPJU/1.jpg
    - https://i3.ytimg.com/vi/zwUnRQbWPJU/2.jpg
    - https://i3.ytimg.com/vi/zwUnRQbWPJU/3.jpg
---
