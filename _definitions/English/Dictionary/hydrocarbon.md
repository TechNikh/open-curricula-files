---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrocarbon
offline_file: ""
offline_thumbnail: ""
uuid: e123eb38-d8ae-4fed-90ea-359a8a347034
updated: 1484310419
title: hydrocarbon
categories:
    - Dictionary
---
hydrocarbon
     n : an organic compound containing only carbon and hydrogen
