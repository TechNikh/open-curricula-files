---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acceptable
offline_file: ""
offline_thumbnail: ""
uuid: 52ff848f-1041-4b83-815e-4b594f0fa180
updated: 1484310258
title: acceptable
categories:
    - Dictionary
---
acceptable
     adj 1: worthy of acceptance or satisfactory; "acceptable levels of
            radiation"; "performances varied from acceptable to
            excellent" [ant: {unacceptable}]
     2: judged to be in conformity with approved usage; "acceptable
        English usage" [syn: {accepted}]
     3: meeting requirements; "the step makes a satisfactory seat"
        [syn: {satisfactory}]
     4: adequate for the purpose; "the water was acceptable for
        drinking"
