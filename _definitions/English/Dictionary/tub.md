---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tub
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484366701
title: tub
categories:
    - Dictionary
---
tub
     n 1: a relatively large open container that you fill with water
          and use to wash the body [syn: {bathtub}, {bathing tub},
           {bath}]
     2: a large open vessel for holding or storing liquids [syn: {vat}]
     3: the amount that a tub will hold; "a tub of water" [syn: {tubful}]
