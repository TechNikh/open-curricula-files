---
version: 1
type: video
provider: YouTube
id: 1gR0kQ_rrgI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1ea5292e-4849-4777-8ebe-8616f193d96f
updated: 1486069621
title: 'Mixture Alligation  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/1gR0kQ_rrgI/default.jpg
    - https://i3.ytimg.com/vi/1gR0kQ_rrgI/1.jpg
    - https://i3.ytimg.com/vi/1gR0kQ_rrgI/2.jpg
    - https://i3.ytimg.com/vi/1gR0kQ_rrgI/3.jpg
---
