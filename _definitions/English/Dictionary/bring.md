---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bring
offline_file: ""
offline_thumbnail: ""
uuid: f01ec7e1-d41d-41e7-a37d-457c438d5c14
updated: 1484310335
title: bring
categories:
    - Dictionary
---
bring
     v 1: take something or somebody with oneself somewhere; "Bring me
          the box from the other room"; "Take these letters to the
          boss"; "This brings me to the main point" [syn: {convey},
           {take}]
     2: cause to come into a particular state or condition; "Long
        hard years of on the job training had brought them to
        their competence"; "bring water to the boiling point"
     3: cause to happen or to occur as a consequence; "I cannot work
        a miracle"; "wreak havoc"; "bring comments"; "play a
        joke"; "The rain brought relief to the drought-stricken
        area" [syn: {work}, {play}, {wreak}, {make for}]
     4: go or come after ...
