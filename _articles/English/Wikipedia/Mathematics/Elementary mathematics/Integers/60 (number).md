---
version: 1
type: article
id: https://en.wikipedia.org/wiki/60_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 31bff858-668b-4495-926a-8683928ca174
updated: 1484308961
title: 60 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-C60a.png
tags:
    - In mathematics
    - In science and technology
    - Cultural number systems
    - In religion
    - In other fields
    - In sports
categories:
    - Integers
---
60 (sixty) ( Listen (help·info)) is the natural number following 59 and preceding 61. Being three times 20, it is called "three score" in older literature.
