---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aspect
offline_file: ""
offline_thumbnail: ""
uuid: aada26b8-078a-4b6b-bd6c-5cef4238b6ec
updated: 1484310447
title: aspect
categories:
    - Dictionary
---
aspect
     n 1: a distinct feature or element in a problem; "he studied
          every facet of the question" [syn: {facet}]
     2: a characteristic to be considered
     3: the visual percept of a region; "the most desirable feature
        of the park are the beautiful views" [syn: {view}, {prospect},
         {scene}, {vista}, {panorama}]
     4: the beginning or duration or completion or repetition of the
        action of a verb
     5: the expression on a person's face; "a sad expression"; "a
        look of triumph"; "an angry face" [syn: {expression}, {look},
         {facial expression}, {face}]
