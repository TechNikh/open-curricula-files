---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hand
offline_file: ""
offline_thumbnail: ""
uuid: b6f1776d-9729-401a-8932-aeb3656e98f1
updated: 1484310351
title: hand
categories:
    - Dictionary
---
hand
     n 1: the (prehensile) extremity of the superior limb; "he had the
          hands of a surgeon"; "he extended his mitt" [syn: {manus},
           {mitt}, {paw}]
     2: a hired laborer on a farm or ranch; "the hired hand fixed
        the railing"; "a ranch hand" [syn: {hired hand}, {hired
        man}]
     3: something written by hand; "she recognized his handwriting";
        "his hand was illegible" [syn: {handwriting}, {script}]
     4: ability; "he wanted to try his hand at singing"
     5: a position given by its location to the side of an object;
        "objections were voiced on every hand"
     6: the cards held in a card game by a given player at any given
        ...
