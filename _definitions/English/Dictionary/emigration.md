---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emigration
offline_file: ""
offline_thumbnail: ""
uuid: 3311777f-7be1-46c4-9fe4-1ae3b3e4c983
updated: 1484310517
title: emigration
categories:
    - Dictionary
---
emigration
     n : migration from a place (especially migration from your
         native country in order to settle in another) [syn: {out-migration},
          {expatriation}]
