---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fused
offline_file: ""
offline_thumbnail: ""
uuid: bed6802e-40ea-47af-ba39-b4a8f0dffdf3
updated: 1484310315
title: fused
categories:
    - Dictionary
---
fused
     adj : joined together into a whole; "United Industries"; "the
           amalgamated colleges constituted a university"; "a
           consolidated school" [syn: {amalgamate}, {amalgamated},
            {coalesced}, {consolidated}]
