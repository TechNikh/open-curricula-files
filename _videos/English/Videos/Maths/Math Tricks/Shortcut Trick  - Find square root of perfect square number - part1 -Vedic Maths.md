---
version: 1
type: video
provider: YouTube
id: vlfsduXbIHg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e4cd738c-0058-41e0-b380-1fa528542b36
updated: 1486069621
title: 'Shortcut Trick  - Find square root of perfect square number - part1 -Vedic Maths'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/vlfsduXbIHg/default.jpg
    - https://i3.ytimg.com/vi/vlfsduXbIHg/1.jpg
    - https://i3.ytimg.com/vi/vlfsduXbIHg/2.jpg
    - https://i3.ytimg.com/vi/vlfsduXbIHg/3.jpg
---
