---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bargain
offline_file: ""
offline_thumbnail: ""
uuid: b0391bc6-7272-41b4-b80e-dde1a7966755
updated: 1484310189
title: bargain
categories:
    - Dictionary
---
bargain
     n 1: an agreement between parties (usually arrived at after
          discussion) fixing obligations of each; "he made a
          bargain with the devil"; "he rose to prominence through
          a series of shady deals" [syn: {deal}]
     2: an advantageous purchase; "she got a bargain at the
        auction"; "the stock was a real buy at that price" [syn: {buy},
         {steal}]
     v 1: negotiate the terms of an exchange; "We bargained for a
          beautiful rug in the bazaar" [syn: {dicker}]
     2: come to terms; arrive at an agreement
