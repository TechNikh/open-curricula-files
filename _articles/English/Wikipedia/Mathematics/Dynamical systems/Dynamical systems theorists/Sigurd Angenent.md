---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sigurd_Angenent
offline_file: ""
offline_thumbnail: ""
uuid: 75608068-7c68-4831-86de-1182651be9f2
updated: 1484308684
title: Sigurd Angenent
categories:
    - Dynamical systems theorists
---
Sigurd Bernardus Angenent (born 1960)[1] is a Dutch-born mathematician and professor at the University of Wisconsin–Madison. Angenent works on partial differential equations and dynamical systems, with his recent research focusing on heat equation and diffusion equation.[2][3] The Angenent torus and Angenent ovals are special solutions to the mean curvature flow published by Angenent in 1992;[4] the Angenent torus remains self-similar as it collapses to a point under the flow, and the Angenent ovals are the only compact ancient solutions other than circles for the curve-shortening flow.[5]
