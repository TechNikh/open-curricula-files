---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ahmedabad
offline_file: ""
offline_thumbnail: ""
uuid: f150c768-1a52-4994-abe7-c3a390fc8641
updated: 1484308413
title: Ahmedabad
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-City_Walls_of_Ahmedabad_1866.jpg
tags:
    - History
    - geography
    - Climate
    - Cityscape
    - Civic administration
    - Economy
    - Demographics
    - culture
    - Transport
    - Education
    - Media
    - Sports
categories:
    - Uncategorized
---
Ahmedabad (i/ˈɑːmᵻdəbɑːd/; also known as Amdavad Gujarati pronunciation: [ˈəmdɑːvɑːd]) is the largest city and former capital of Gujarat, which is a state in India. It is the administrative headquarters of the Ahmedabad district and the seat of the Gujarat High Court. With a population of more than 6.3 million and an extended population of 7.8 million, it is the sixth largest city and seventh largest metropolitan area of India. Ahmedabad is located on the banks of the Sabarmati River, 30 km (19 mi) from the state capital Gandhinagar, which is its twin city.[8]
