---
version: 1
type: video
provider: YouTube
id: oiqfNHC0lG0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20The%20Heart%20Works%20Animation%20Video%20-%20How%20Does%20the%20Circulatory%20System%20Work-%20Cardiovascular%20Anatomy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0442bd04-d65a-40f8-9ee3-9b37fccd8f95
updated: 1486069669
title: 'How The Heart Works Animation Video - How Does the Circulatory System Work? Cardiovascular Anatomy'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/oiqfNHC0lG0/default.jpg
    - https://i3.ytimg.com/vi/oiqfNHC0lG0/1.jpg
    - https://i3.ytimg.com/vi/oiqfNHC0lG0/2.jpg
    - https://i3.ytimg.com/vi/oiqfNHC0lG0/3.jpg
---
