---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lambda_calculus
offline_file: ""
offline_thumbnail: ""
uuid: be27d43d-422e-4385-a884-8541a541b58d
updated: 1484308683
title: Lambda calculus
tags:
    - Explanation and applications
    - Lambda calculus in history of mathematics
    - Informal description
    - Motivation
    - The lambda calculus
    - Lambda terms
    - Functions that operate on functions
    - Alpha equivalence
    - Free variables
    - Capture-avoiding substitutions
    - Beta reduction
    - Formal definition
    - Definition
    - Notation
    - Free and bound variables
    - Reduction
    - α-conversion
    - Substitution
    - β-reduction
    - η-conversion
    - Normal forms and confluence
    - Encoding datatypes
    - Arithmetic in lambda calculus
    - Logic and predicates
    - Pairs
    - Recursion and fixed points
    - Standard terms
    - Typed lambda calculus
    - Computable functions and lambda calculus
    - Undecidability of equivalence
    - Lambda calculus and programming languages
    - Anonymous functions
    - Reduction strategies
    - A note about complexity
    - Parallelism and concurrency
    - Semantics
categories:
    - Theoretical computer science
---
Lambda calculus (also written as λ-calculus) is a formal system in mathematical logic for expressing computation based on function abstraction and application using variable binding and substitution. It is a universal model of computation that can be used to simulate any single-taped Turing machine and was first introduced by mathematician Alonzo Church in the 1930s as part of an investigation into the foundations of mathematics.
