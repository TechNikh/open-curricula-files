---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suggestion
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484455201
title: suggestion
categories:
    - Dictionary
---
suggestion
     n 1: an idea that is suggested; "the picnic was her suggestion"
     2: a proposal offered for acceptance or rejection; "it was a
        suggestion we couldn't refuse" [syn: {proposition}, {proffer}]
     3: a just detectable amount; "he speaks French with a trace of
        an accent" [syn: {trace}, {hint}]
     4: persuasion formulated as a suggestion [syn: {prompting}]
     5: the sequential mental process in which one thought leads to
        another by association
     6: the act of inducing hypnosis [syn: {hypnotism}, {mesmerism}]
