---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expel
offline_file: ""
offline_thumbnail: ""
uuid: 5f0dd0f5-6fba-48c5-9185-d8e4ed6250b2
updated: 1484310327
title: expel
categories:
    - Dictionary
---
expel
     v 1: force to leave or move out; "He was expelled from his native
          country" [syn: {throw out}, {kick out}]
     2: put out or expel from a place; "The child was expelled from
        the classroom" [syn: {eject}, {chuck out}, {exclude}, {throw
        out}, {kick out}, {turf out}, {boot out}, {turn out}]
     3: remove from a position or office; "The chairman was ousted
        after he misappropriated funds" [syn: {oust}, {throw out},
         {drum out}, {boot out}, {kick out}]
     4: cause to flee; "rout out the fighters from their caves"
        [syn: {rout}, {rout out}]
     5: eliminate (substances) from the body [syn: {discharge}, {eject},
         {release}]
    ...
