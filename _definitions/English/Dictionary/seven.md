---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seven
offline_file: ""
offline_thumbnail: ""
uuid: f3ee341f-33a7-406b-8559-2bf52cc1522d
updated: 1484310307
title: seven
categories:
    - Dictionary
---
seven
     adj : being one more than six [syn: {7}, {vii}]
     n : the cardinal number that is the sum of six and one [syn: {7},
          {VII}, {sevener}, {heptad}, {septet}]
