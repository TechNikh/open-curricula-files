---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boiling
offline_file: ""
offline_thumbnail: ""
uuid: 38733461-0e76-432f-8f5f-b6216b2dcce9
updated: 1484310232
title: boiling
categories:
    - Dictionary
---
boiling
     n 1: the application of heat to change something from a liquid to
          a gas
     2: cooking in a boiling liquid [syn: {stewing}, {simmering}]
     adv : extremely; "boiling mad"
