---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/november
offline_file: ""
offline_thumbnail: ""
uuid: 23996d6b-7763-46f9-8b13-bceaa2d2c060
updated: 1484310555
title: november
categories:
    - Dictionary
---
November
     n : the month following October and preceding December [syn: {Nov}]
