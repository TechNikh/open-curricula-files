---
version: 1
type: video
provider: YouTube
id: j2xftFt7aDo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 048bf877-ed99-46bc-a1ca-bf724b797b9e
updated: 1486069640
title: fresnel biprism experiment -3d simulation
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/j2xftFt7aDo/default.jpg
    - https://i3.ytimg.com/vi/j2xftFt7aDo/1.jpg
    - https://i3.ytimg.com/vi/j2xftFt7aDo/2.jpg
    - https://i3.ytimg.com/vi/j2xftFt7aDo/3.jpg
---
