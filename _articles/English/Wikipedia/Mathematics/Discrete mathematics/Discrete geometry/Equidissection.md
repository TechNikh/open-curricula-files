---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equidissection
offline_file: ""
offline_thumbnail: ""
uuid: 97449a13-75c7-4df4-88a5-8abda0242f89
updated: 1484308598
title: Equidissection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Square_dissected_into_6_equal_area_triangles_no_border.svg.png
tags:
    - Overview
    - Definitions
    - Preliminaries
    - Best results
    - History
    - "Monsky's theorem"
    - Generalizations
    - Related problems
    - Bibliography
categories:
    - Discrete geometry
---
In geometry, an equidissection is a partition of a polygon into triangles of equal area. The study of equidissections began in the late 1960s with Monsky's theorem, which states that a square cannot be equidissected into an odd number of triangles.[1] In fact, most polygons cannot be equidissected at all.[2]
