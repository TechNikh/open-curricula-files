---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zener_ratio
offline_file: ""
offline_thumbnail: ""
uuid: b8c9f68f-bc12-4473-8864-68bf9f7e83c4
updated: 1484309302
title: Zener ratio
categories:
    - Orientation (geometry)
---
The Zener ratio is a dimensionless number that is used to quantify the anisotropy for cubic crystals. It is sometimes referred as anisotropy ratio and is named after Clarence Zener.[1] Conceptually, it quantifies how far a material is from being isotropic (where the value of 1 means an isotropic material).
