---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continuing
offline_file: ""
offline_thumbnail: ""
uuid: b5787df7-33c6-4654-8dad-7bb83f1615b0
updated: 1484310577
title: continuing
categories:
    - Dictionary
---
continuing
     adj : remaining in force or being carried on without letup; "the
           act provided a continuing annual appropriation"; "the
           continuing struggle to put food on the table"
