---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unilateral
offline_file: ""
offline_thumbnail: ""
uuid: e333e4ec-da8c-43db-9ee2-acb2323e1289
updated: 1484310176
title: unilateral
categories:
    - Dictionary
---
unilateral
     adj 1: involving only one part or side; "unilateral paralysis"; "a
            unilateral decision" [syn: {one-sided}] [ant: {multilateral},
             {bilateral}]
     2: tracing descent from either the paternal or the maternal
        line only [syn: {unilateralist}]
