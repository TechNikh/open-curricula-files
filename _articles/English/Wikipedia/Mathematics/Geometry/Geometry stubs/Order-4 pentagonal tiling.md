---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Order-4_pentagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: f682a853-ac6b-4f4a-afb5-9c08e240dd97
updated: 1484309369
title: Order-4 pentagonal tiling
tags:
    - Symmetry
    - Related polyhedra and tiling
categories:
    - Geometry stubs
---
In geometry, the order-4 pentagonal tiling is a regular tiling of the hyperbolic plane. It has Schläfli symbol of {5,4}. It can also be called a pentapentagonal tiling in a bicolored quasiregular form.
