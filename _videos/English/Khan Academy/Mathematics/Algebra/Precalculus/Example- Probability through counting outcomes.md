---
version: 1
type: video
provider: YouTube
id: 3_otNr9kRuY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Example-%20Probability%20through%20counting%20outcomes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 143c260e-fccf-4ec8-8d9d-1571191ab372
updated: 1486069563
title: 'Example- Probability through counting outcomes'
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/3_otNr9kRuY/default.jpg
    - https://i3.ytimg.com/vi/3_otNr9kRuY/1.jpg
    - https://i3.ytimg.com/vi/3_otNr9kRuY/2.jpg
    - https://i3.ytimg.com/vi/3_otNr9kRuY/3.jpg
---
