---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suitcase
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484502961
title: suitcase
categories:
    - Dictionary
---
suitcase
     n : a portable rectangular traveling bag for carrying clothes;
         "he carried his small bag onto the plane with him" [syn:
         {bag}, {traveling bag}, {grip}]
