---
version: 1
type: video
provider: YouTube
id: Mq-PF1vo9QA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1dde2159-f0f3-4a3f-ae4d-29f61846bf11
updated: 1486069656
title: 'Resonance Circuits-  LC Inductor-Capacitor Resonating Circuits'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Mq-PF1vo9QA/default.jpg
    - https://i3.ytimg.com/vi/Mq-PF1vo9QA/1.jpg
    - https://i3.ytimg.com/vi/Mq-PF1vo9QA/2.jpg
    - https://i3.ytimg.com/vi/Mq-PF1vo9QA/3.jpg
---
