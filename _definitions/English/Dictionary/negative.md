---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negative
offline_file: ""
offline_thumbnail: ""
uuid: c4fbdb9a-1426-4af1-bda9-ca9c730ac585
updated: 1484310260
title: negative
categories:
    - Dictionary
---
negative
     adj 1: characterized by or displaying negation or denial or
            opposition or resistance; having no positive features;
            "a negative outlook on life"; "a colorless negative
            personality"; "a negative evaluation"; "a negative
            reaction to an advertising campaign" [ant: {neutral},
            {positive}]
     2: reckoned in a direction opposite to that regarded as
        positive
     3: having a negative electric charge; "electrons are negative"
        [syn: {electronegative}] [ant: {neutral}, {positive}]
     4: expressing or consisting of a negation or refusal or denial
        [ant: {affirmative}]
     5: having the quality of ...
