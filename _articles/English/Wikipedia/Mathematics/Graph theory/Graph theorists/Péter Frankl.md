---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/P%C3%A9ter_Frankl'
offline_file: ""
offline_thumbnail: ""
uuid: 38f87b76-be75-477e-8aa4-57781a65f98b
updated: 1484309461
title: Péter Frankl
tags:
    - Personality
    - Adolescence and abilities
    - activities
    - Frankl conjecture
categories:
    - Graph theorists
---
Péter Frankl (born 26 March 1953 in Kaposvár, Somogy County, Hungary) is a mathematician, street performer, columnist and educator, active in Japan.[4] Frankl studied Mathematics at Eötvös University in Budapest and submitted his PhD thesis while still an undergraduate. He holds PhD degree from University Paris Diderot as well. He has lived in Japan since 1988, where he is a well-known personnality and often appears in the media. He keeps travelling around Japan performing (juggling and giving public lectures on various topics). Frankl won a gold medal at the International Mathematical Olympiad in 1971. He has seven joint papers with Paul Erdős,[5] and eleven joint papers with Ronald ...
