---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lowest_common_denominator
offline_file: ""
offline_thumbnail: ""
uuid: ed65067e-eeca-4652-be72-a5b087399cb9
updated: 1484308915
title: Lowest common denominator
tags:
    - Description
    - Role in arithmetic and algebra
categories:
    - Elementary arithmetic
---
In mathematics, the lowest common denominator or least common denominator (abbreviated LCD) is the least common multiple of the denominators of a set of fractions. It simplifies adding, subtracting, and comparing fractions.
