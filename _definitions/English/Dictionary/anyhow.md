---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anyhow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451181
title: anyhow
categories:
    - Dictionary
---
anyhow
     adv 1: used to indicate that a statement explains or supports a
            previous statement; "Anyhow, he is dead now"; "I think
            they're asleep; anyhow, they're quiet"; "I don't know
            what happened to it; anyway, it's gone"; "anyway,
            there is another factor to consider"; "I don't know
            how it started; in any case, there was a brief
            scuffle"; "in any event, the government faced a
            serious protest"; "but at any rate he got a knighthood
            for it" [syn: {anyway}, {in any case}, {at any rate},
            {in any event}]
     2: in any way whatsoever; "they came anyhow they could"; "get
        it done ...
