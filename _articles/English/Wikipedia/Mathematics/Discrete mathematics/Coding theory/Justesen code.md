---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Justesen_code
offline_file: ""
offline_thumbnail: ""
uuid: 42891158-a8fc-4482-be93-050ad93dd7db
updated: 1484308558
title: Justesen code
tags:
    - Definition
    - Property of Justesen code
    - Theorem
    - Proof
    - Comments
    - An example of a Justesen code
categories:
    - Coding theory
---
In coding theory, Justesen codes form a class of error-correcting codes that have a constant rate, constant relative distance, and a constant alphabet size.
