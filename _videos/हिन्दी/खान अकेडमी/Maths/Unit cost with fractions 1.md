---
version: 1
type: video
provider: YouTube
id: _d3jYBApn-I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b29042e6-afad-4e40-92bd-64473f2f32fb
updated: 1486069618
title: Unit cost with fractions 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/_d3jYBApn-I/default.jpg
    - https://i3.ytimg.com/vi/_d3jYBApn-I/1.jpg
    - https://i3.ytimg.com/vi/_d3jYBApn-I/2.jpg
    - https://i3.ytimg.com/vi/_d3jYBApn-I/3.jpg
---
