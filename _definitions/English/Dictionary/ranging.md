---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ranging
offline_file: ""
offline_thumbnail: ""
uuid: fe31d957-0843-4a95-9a8e-6fe23e262892
updated: 1484310437
title: ranging
categories:
    - Dictionary
---
ranging
     adj : wandering freely; "at night in bed...his slowly ranging
           thoughts...encountered her"
