---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/piece
offline_file: ""
offline_thumbnail: ""
uuid: ba1d8caf-e8d0-468a-9fc3-b3f5085284ff
updated: 1484310351
title: piece
categories:
    - Dictionary
---
piece
     n 1: a separate part of a whole; "an important piece of the
          evidence"
     2: an item that is an instance of some type; "he designed a new
        piece of equipment"; "she bought a lovely piece of china";
     3: a portion of a natural object; "they analyzed the river into
        three parts"; "he needed a piece of granite" [syn: {part}]
     4: a musical work that has been created; "the composition is
        written in four movements" [syn: {musical composition}, {opus},
         {composition}, {piece of music}]
     5: an instance of some kind; "it was a nice piece of work"; "he
        had a bit of good luck" [syn: {bit}]
     6: an artistic or literary ...
