---
version: 1
type: video
provider: YouTube
id: Tal_fgREll0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Similarity/Similarity%20example%20where%20same%20side%20plays%20different%20roles.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f0b57edc-958a-4b39-a936-49a6f85306f4
updated: 1486069573
title: Similarity example where same side plays different roles
categories:
    - Similarity
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tal_fgREll0/default.jpg
    - https://i3.ytimg.com/vi/Tal_fgREll0/1.jpg
    - https://i3.ytimg.com/vi/Tal_fgREll0/2.jpg
    - https://i3.ytimg.com/vi/Tal_fgREll0/3.jpg
---
The same side not corresponding to itself in two similar triangles

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/geometry/similarity/triangle_similarlity/e/solving_similar_triangles_2?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Watch the next lesson: https://www.khanacademy.org/math/geometry/similarity/cc-solving-prob-similar-tri/v/finding-area-using-similarity-and-congruence?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/similarity/triangle_similarlity/v/similarity-example-problems?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
