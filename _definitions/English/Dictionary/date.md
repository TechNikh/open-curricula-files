---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/date
offline_file: ""
offline_thumbnail: ""
uuid: bce7c4d4-be7c-4c31-b4ca-3c0229698a71
updated: 1484310433
title: date
categories:
    - Dictionary
---
date
     n 1: the specified day of the month; "what is the date today?"
          [syn: {day of the month}]
     2: a particular day specified as the time something will
        happen; "the date of the election is set by law"
     3: a meeting arranged in advance; "she asked how to avoid
        kissing at the end of a date" [syn: {appointment}, {engagement}]
     4: a particular but unspecified point in time; "they hoped to
        get together at an early date"
     5: the present; "they are up to date"; "we haven't heard from
        them to date"
     6: a participant in a date; "his date never stopped talking"
        [syn: {escort}]
     7: the particular day, month, or year ...
