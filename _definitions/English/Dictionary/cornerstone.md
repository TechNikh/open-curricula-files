---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cornerstone
offline_file: ""
offline_thumbnail: ""
uuid: e555fc0d-167d-4332-b3f1-8fa80bb359b3
updated: 1484310429
title: cornerstone
categories:
    - Dictionary
---
cornerstone
     n 1: the fundamental assumptions from which something is begun or
          developed or calculated or explained; "the whole
          argument rested on a basis of conjecture" [syn: {basis},
           {base}, {foundation}, {fundament}, {groundwork}]
     2: a stone in the exterior of a large and important building;
        usually carved with a date and laid with appropriate
        ceremonies
     3: a stone at the outer corner of two intersecting masonry
        walls
