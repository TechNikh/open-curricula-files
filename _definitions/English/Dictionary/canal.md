---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/canal
offline_file: ""
offline_thumbnail: ""
uuid: ba96e675-de0d-4585-bb3e-44d11d97739e
updated: 1484310357
title: canal
categories:
    - Dictionary
---
canal
     n 1: (astronomy) an indistinct surface feature of Mars once
          thought to be a system of channels; they are now
          believed to be an optical illusion
     2: a bodily passage or tube lined with epithelial cells and
        conveying a secretion or other substance; "the tear duct
        was obstructed"; "the alimentary canal"; "poison is
        released through a channel in the snake's fangs" [syn: {duct},
         {epithelial duct}, {channel}]
     3: long and narrow strip of water made for boats or for
        irrigation
     v : provide (a city) with a canal [syn: {canalize}, {canalise}]
     [also: {canalling}, {canalled}]
