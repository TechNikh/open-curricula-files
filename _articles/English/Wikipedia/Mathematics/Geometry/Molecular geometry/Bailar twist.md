---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bailar_twist
offline_file: ""
offline_thumbnail: ""
uuid: 949692ad-bc3c-4f61-b559-16167263fd29
updated: 1484309277
title: Bailar twist
categories:
    - Molecular geometry
---
The Bailar twist is a mechanism proposed for the racemization of octahedral complexes containing three bidentate chelate rings. Such complexes typically adopt an octahedral molecular geometry and are thus chiral.[1] One pathway by which these compounds can racemize is via the formation of a trigonal prismatic intermediate with D3h point group symmetry. An alternative pathway is called the Ray-Dutt twist. This pathway is called the Bailar twist in honor of John C. Bailar, Jr., an inorganic chemist who investigated this process.[2]
