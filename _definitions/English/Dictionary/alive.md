---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alive
offline_file: ""
offline_thumbnail: ""
uuid: 77f15a80-225a-4a7c-9acf-93c6bfac89ed
updated: 1484310313
title: alive
categories:
    - Dictionary
---
alive
     adj 1: possessing life; "the happiest person alive"; "the nerve is
            alive"; "doctors are working hard to keep him alive";
            "burned alive" [syn: {alive(p)}] [ant: {dead}]
     2: (often followed by `with') full of life and spirit; "she was
        wonderfully alive for her age"; "a face alive with
        mischief" [syn: {alive(p)}]
     3: having life or vigor or spirit; "an animated and expressive
        face"; "animated conversation"; "became very animated when
        he heard the good news" [syn: {animated}] [ant: {unanimated}]
     4: (followed by `to' or `of') aware of; "is alive to the moods
        of others" [syn: {alive(p)}]
     5: in operation; ...
