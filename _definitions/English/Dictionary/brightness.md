---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brightness
offline_file: ""
offline_thumbnail: ""
uuid: ec3a64ca-c687-49f1-b744-99a830573364
updated: 1484310156
title: brightness
categories:
    - Dictionary
---
brightness
     n 1: the location of a visual perception along the black-to-white
          continuum [ant: {dullness}]
     2: intelligence as manifested in being quick and witty [syn: {cleverness},
         {smartness}]
     3: the quality of being luminous; emitting or reflecting light;
        "its luminosity is measured relative to that of our sun"
        [syn: {luminosity}, {brightness level}, {luminance}, {luminousness},
         {light}]
