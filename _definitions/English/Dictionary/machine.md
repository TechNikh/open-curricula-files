---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/machine
offline_file: ""
offline_thumbnail: ""
uuid: 6bf3faa8-6192-43f1-8a2b-98f79cfc3389
updated: 1484310363
title: machine
categories:
    - Dictionary
---
machine
     n 1: any mechanical or electrical device that transmits or
          modifies energy to perform or assist in the performance
          of human tasks
     2: an intricate organization that accomplishes its goals
        efficiently; "the war machine"
     3: an efficient person; "the boxer was a magnificent fighting
        machine"
     4: 4-wheeled motor vehicle; usually propelled by an internal
        combustion engine; "he needs a car to get to work" [syn: {car},
         {auto}, {automobile}, {motorcar}]
     5: a group that controls the activities of a political party;
        "he was endorsed by the Democratic machine" [syn: {political
        machine}]
     6: a device ...
