---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zigzag
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494981
title: zigzag
categories:
    - Dictionary
---
zigzag
     adj : having short sharp turns or angles
     n : an angular shape characterized by sharp turns in alternating
         directions [syn: {zig}, {zag}]
     adv : in a zigzag course or on a zigzag path; "birds flew zigzag
           across the blue sky"
     v : travel along a zigzag path; "The river zigzags through the
         countryside" [syn: {crank}]
     [also: {zigzagging}, {zigzagged}]
