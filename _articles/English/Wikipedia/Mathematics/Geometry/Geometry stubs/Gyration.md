---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gyration
offline_file: ""
offline_thumbnail: ""
uuid: 262f6be8-87f2-4bda-b6a3-49e50bd5c636
updated: 1484309363
title: Gyration
categories:
    - Geometry stubs
---
In geometry, a gyration is a rotation in a discrete subgroup of symmetries of the Euclidean plane such that the subgroup does not also contain a reflection symmetry whose axis passes through the center of rotational symmetry. In the orbifold corresponding to the subgroup, a gyration corresponds to a rotation point that does not lie on a mirror, called a gyration point.[1]
