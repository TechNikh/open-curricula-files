---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triad
offline_file: ""
offline_thumbnail: ""
uuid: bb27c3b4-7092-42fb-afe0-b08c5c6ca7b6
updated: 1484310397
title: triad
categories:
    - Dictionary
---
triad
     n 1: the cardinal number that is the sum of one and one and one
          [syn: {three}, {3}, {III}, {trio}, {threesome}, {tierce},
           {leash}, {troika}, {trine}, {trinity}, {ternary}, {ternion},
           {triplet}, {tercet}, {terzetto}, {trey}, {deuce-ace}]
     2: a set of three similar things considered as a unit [syn: {trio},
         {triplet}]
     3: three people considered as a unit [syn: {trio}, {threesome},
         {trinity}]
     4: a 3-note major or minor chord; a note and its third and
        fifth tones [syn: {common chord}]
