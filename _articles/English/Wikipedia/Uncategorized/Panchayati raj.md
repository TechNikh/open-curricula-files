---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Panchayat
offline_file: ""
offline_thumbnail: ""
uuid: 9e717eca-681e-4649-ab19-8835b8eb7c4f
updated: 1484308440
title: Panchayati raj
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Muhamma_grama_panchayath.jpg
categories:
    - Uncategorized
---
The Panchayat raj is a South Asian political system found mainly in India, Pakistan, Bangladesh, Trinidad and Tobago, and Nepal. It is the oldest system of local government in the Indian subcontinent. The word raj means "rule" and panchayat means "assembly" (ayat) of five (panch). Traditionally panchayats consisted of wise and respected elders chosen and accepted by the local community. However, there were varying forms of such assemblies. Traditionally, these assemblies settled disputes between individuals and between villages.
