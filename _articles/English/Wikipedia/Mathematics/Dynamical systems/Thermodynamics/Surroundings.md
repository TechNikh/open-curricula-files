---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surroundings
offline_file: ""
offline_thumbnail: ""
uuid: 79bc69cc-6977-4241-8a70-1560430b77d9
updated: 1484308860
title: Surroundings
categories:
    - Thermodynamics
---
Surroundings are the area around a given physical or geographical point or place. The exact definition depends on the field. Surroundings can also be used in geography (when it is more precisely known as vicinity, or vicinage) and mathematics, as well as philosophy, with the literal or metaphorically extended definition.
