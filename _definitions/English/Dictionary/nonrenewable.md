---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonrenewable
offline_file: ""
offline_thumbnail: ""
uuid: 1af0a570-dbc4-46a3-89cd-3a4de14a137c
updated: 1484310254
title: nonrenewable
categories:
    - Dictionary
---
nonrenewable
     adj : that can not be renewed; "books on that shelf are
           unrenewable"; "gas and oil are nonrenewable resources"
           [syn: {unrenewable}] [ant: {renewable}]
