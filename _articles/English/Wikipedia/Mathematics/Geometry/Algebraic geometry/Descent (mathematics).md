---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Descent_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 1003a007-7df7-401d-a214-0a129bcad3a1
updated: 1484309142
title: Descent (mathematics)
tags:
    - Descent of vector bundles
    - History
    - Fully faithful descent
categories:
    - Algebraic geometry
---
In mathematics, the idea of descent extends the intuitive idea of 'gluing' in topology. Since the topologists' glue is actually the use of equivalence relations on topological spaces, the theory starts with some ideas on identification.
