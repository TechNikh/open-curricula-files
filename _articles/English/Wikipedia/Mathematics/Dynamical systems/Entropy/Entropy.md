---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy
offline_file: ""
offline_thumbnail: ""
uuid: 735d643d-de00-4201-b255-566b4a7555d2
updated: 1484308767
title: Entropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Clausius.jpg
tags:
    - History
    - Definitions and descriptions
    - Function of state
    - Reversible process
    - Carnot cycle
    - Classical thermodynamics
    - Statistical mechanics
    - Entropy of a system
    - Second law of thermodynamics
    - Applications
    - The fundamental thermodynamic relation
    - Entropy in chemical thermodynamics
    - Entropy balance equation for open systems
    - Entropy change formulas for simple processes
    - Isothermal expansion or compression of an ideal gas
    - Cooling and heating
    - Phase transitions
    - Approaches to understanding entropy
    - Standard textbook definitions
    - Order and disorder
    - Energy dispersal
    - Relating entropy to energy usefulness
    - Entropy and adiabatic accessibility
    - Entropy in quantum mechanics
    - Information theory
    - Interdisciplinary applications of entropy
    - Thermodynamic and statistical mechanics concepts
    - The arrow of time
    - Cosmology
    - Economics
    - Notes
categories:
    - Entropy
---
In statistical thermodynamics, entropy (usual symbol S) is a measure of the number of microscopic configurations Ω that correspond to a thermodynamic system in a state specified by certain macroscopic variables. Specifically, assuming that each of the microscopic configurations is equally probable, the entropy of the system is the natural logarithm of that number of configurations, multiplied by the Boltzmann constant kB (which provides consistency with the original thermodynamic concept of entropy discussed below, and gives entropy the dimension of energy divided by temperature). Formally,
