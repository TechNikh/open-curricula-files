---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tifr
offline_file: ""
offline_thumbnail: ""
uuid: 69da0e3f-5682-4179-b30b-9d5034277175
updated: 1484308409
title: Tata Institute of Fundamental Research
tags:
    - History
    - Research
    - School of Mathematics
    - School of Natural Sciences
    - School of Technology and Computer Science
    - Department of Biological Sciences
    - Research facilities
    - Other facilities
    - Canteens
    - Lecture Halls
    - Artwork
    - Lawns
    - Noted alumni
    - Affiliated research institutes
    - Visiting Students Research Programme
categories:
    - Uncategorized
---
The Tata Institute of Fundamental Research (TIFR) is a public research institution in Mumbai and Hyderabad, India, dedicated to basic research in mathematics and the sciences. It is a Deemed University and works under the umbrella of the Department of Atomic Energy of the Government of India. It is located at Navy Nagar, Colaba, Mumbai and Narsingi, Hyderabad . TIFR conducts research primarily in the natural sciences, mathematics, the biological sciences and theoretical computer science and is considered to be one among India's outstanding research centres.[1] TIFR has a graduate programme leading to a PhD in all the major fields of study. TIFR is rated with an "A" grade, as determined by ...
