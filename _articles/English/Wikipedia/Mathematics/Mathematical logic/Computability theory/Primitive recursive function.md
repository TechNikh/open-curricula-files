---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primitive_recursive_function
offline_file: ""
offline_thumbnail: ""
uuid: b35562c7-185c-48e1-87b2-d7a7be15daab
updated: 1484309490
title: Primitive recursive function
tags:
    - Definition
    - Role of the projection functions
    - Converting predicates to numeric functions
    - Computer language definition
    - Examples
    - Addition
    - Subtraction
    - Other operations on natural numbers
    - Operations on integers and rational numbers
    - Use in first-order Peano arithmetic
    - Relationship to recursive functions
    - Limitations
    - Some common primitive recursive functions
    - Additional primitive recursive forms
    - Finitism and consistency results
    - History
categories:
    - Computability theory
---
In computability theory, primitive recursive functions are a class of functions that are defined using primitive recursion and composition as central operations and are a strict subset of the total µ-recursive functions (µ-recursive functions are also called partial recursive). Primitive recursive functions form an important building block on the way to a full formalization of computability. These functions are also important in proof theory.
