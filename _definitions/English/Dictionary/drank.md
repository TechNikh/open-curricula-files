---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drank
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479261
title: drank
categories:
    - Dictionary
---
drink
     n 1: a single serving of a beverage; "I asked for a hot drink";
          "likes a drink before dinner"
     2: the act of drinking alcoholic beverages to excess; "drink
        was his downfall" [syn: {drinking}, {boozing}, {drunkenness},
         {crapulence}]
     3: any liquid suitable for drinking; "may I take your beverage
        order?" [syn: {beverage}, {drinkable}, {potable}]
     4: any large deep body of water; "he jumped into the drink and
        had to be rescued"
     5: the act of swallowing; "one swallow of the liquid was
        enough"; "he took a drink of his beer and smacked his
        lips" [syn: {swallow}, {deglutition}]
     v 1: take in liquids; "The ...
