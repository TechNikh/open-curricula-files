---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/escape
offline_file: ""
offline_thumbnail: ""
uuid: 4307e5b5-c8cf-432c-88c7-0655295ede36
updated: 1484310559
title: escape
categories:
    - Dictionary
---
escape
     n 1: the act of escaping physically; "he made his escape from the
          mental hospital"; "the canary escaped from its cage";
          "his flight was an indication of his guilt" [syn: {flight}]
     2: an inclination to retreat from unpleasant realities through
        diversion or fantasy; "romantic novels were her escape
        from the stress of daily life"; "his alcohol problem was a
        form of escapism" [syn: {escapism}]
     3: the unwanted discharge of a fluid from some container; "they
        tried to stop the escape of gas from the damaged pipe";
        "he had to clean up the leak" [syn: {leak}, {leakage}, {outflow}]
     4: a valve in a container in ...
