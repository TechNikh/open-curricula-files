---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Schrodinger
offline_file: ""
offline_thumbnail: ""
uuid: 6ba76c6d-7f0b-4491-8545-47574803ab27
updated: 1484308307
title: Erwin Schrödinger
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Erwin_Schrodinger_at_U_Vienna.JPG
tags:
    - Biography
    - Early years
    - Middle years
    - Later years
    - Personal life
    - Scientific activities
    - Early career
    - Quantum mechanics
    - New quantum theory
    - Creation of wave mechanics
    - Work on a unified field theory
    - Colour
    - Legacy
    - Honors and awards
    - Published works
categories:
    - Uncategorized
---
Erwin Rudolf Josef Alexander Schrödinger (German: [ˈɛɐ̯viːn ˈʃʁøːdɪŋɐ]; 12 August 1887 – 4 January 1961), sometimes written as Erwin Schrodinger or Erwin Schroedinger, was a Nobel Prize-winning Austrian physicist who developed a number of fundamental results in the field of quantum theory, which formed the basis of wave mechanics: he formulated the wave equation (stationary and time-dependent Schrödinger equation) and revealed the identity of his development of the formalism and matrix mechanics. Schrödinger proposed an original interpretation of the physical meaning of the wave function.
