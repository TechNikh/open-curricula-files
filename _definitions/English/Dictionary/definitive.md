---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/definitive
offline_file: ""
offline_thumbnail: ""
uuid: 814b6096-3a83-49ce-8031-ba06fc799c49
updated: 1484310469
title: definitive
categories:
    - Dictionary
---
definitive
     adj 1: clearly defined or formulated; "the plain and unequivocal
            language of the laws"- R.B.Taney [syn: {unequivocal}]
     2: of recognized authority or excellence; "the definitive work
        on Greece"; "classical methods of navigation" [syn: {authoritative},
         {classical}]
     3: supplying or being a final or conclusive settlement; "a
        definitive verdict"; "a determinate answer to the problem"
        [syn: {determinate}]
