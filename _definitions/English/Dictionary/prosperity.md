---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prosperity
offline_file: ""
offline_thumbnail: ""
uuid: 4861f89d-905a-4ebb-93be-58ffda8ca1e9
updated: 1484310533
title: prosperity
categories:
    - Dictionary
---
prosperity
     n 1: an economic state of growth with rising profits and full
          employment
     2: the condition of prospering; having good fortune [syn: {successfulness}]
