---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Tom_(pattern_matching_language)
offline_file: ""
offline_thumbnail: ""
uuid: 757ddc4d-c45d-4462-941d-b6cebdfdff0a
updated: 1484309450
title: Tom (pattern matching language)
categories:
    - Graph rewriting
---
Tom is a programming language[1] particularly well-suited for programming various transformations[2] on tree structures and XML based documents. Tom is a language extension which adds new matching primitives to C and Java[3] as well as support for rewrite rules systems.[4] The rules can be controlled using a strategy[5] language.
