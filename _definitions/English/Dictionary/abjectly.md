---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abjectly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647321
title: abjectly
categories:
    - Dictionary
---
abjectly
     adv : in a hopeless resigned manner; "she shrugged her shoulders
           abjectly" [syn: {resignedly}]
