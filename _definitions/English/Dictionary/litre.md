---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/litre
offline_file: ""
offline_thumbnail: ""
uuid: 7910e450-4f7e-4058-97b9-0466e011bfe5
updated: 1484310230
title: litre
categories:
    - Dictionary
---
litre
     n : a metric unit of capacity equal to the volume of 1 kilogram
         of pure water at 4 degrees centigrade and 760 mm of
         mercury (or approximately 1.76 pints) [syn: {liter}, {l},
          {cubic decimeter}, {cubic decimetre}]
