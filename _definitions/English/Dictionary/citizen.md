---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/citizen
offline_file: ""
offline_thumbnail: ""
uuid: 8090e377-b861-4776-a383-d11d8443df91
updated: 1484310445
title: citizen
categories:
    - Dictionary
---
citizen
     n : a native or naturalized member of a state or other political
         community [ant: {foreigner}]
