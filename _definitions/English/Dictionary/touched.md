---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/touched
offline_file: ""
offline_thumbnail: ""
uuid: 9dc49ab7-8d20-45ba-832b-55a9a619f44d
updated: 1484310206
title: touched
categories:
    - Dictionary
---
touched
     adj 1: being colored slightly; sometimes used in combination;
            "white petals touched with pink"; "the resplendent
            sun-touched flag"; "pink-tinged apple blossoms" [syn:
            {tinged}]
     2: having come into contact [ant: {untouched}]
     3: slightly insane [syn: {fey}, {touched(p)}]
     4: emotionally affected; "very touched by the stranger's
        kindness" [syn: {affected(p)}, {stirred(p)}, {touched(p)}]
