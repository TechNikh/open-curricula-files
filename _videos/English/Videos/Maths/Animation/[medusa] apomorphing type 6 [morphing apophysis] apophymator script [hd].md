---
version: 1
type: video
provider: YouTube
id: qtuI0SDAh5c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b3c833ee-1d19-49a3-83eb-ba306b518dd9
updated: 1486069645
title: '[medusa] apomorphing type 6 [morphing apophysis] apophymator script [hd]'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qtuI0SDAh5c/default.jpg
    - https://i3.ytimg.com/vi/qtuI0SDAh5c/1.jpg
    - https://i3.ytimg.com/vi/qtuI0SDAh5c/2.jpg
    - https://i3.ytimg.com/vi/qtuI0SDAh5c/3.jpg
---
