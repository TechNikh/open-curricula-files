---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/page
offline_file: ""
offline_thumbnail: ""
uuid: f10c3021-fadb-4feb-a154-c791a7fa3335
updated: 1484310365
title: page
categories:
    - Dictionary
---
page
     n 1: one side of one leaf (of a book or magasine or newspaper or
          letter etc.) or the written or pictorial matter it
          contains
     2: English industrialist who pioneered in the design and
        manufacture of aircraft (1885-1962) [syn: {Sri Frederick
        Handley Page}]
     3: United States diplomat and writer about the Old South
        (1853-1922) [syn: {Thomas Nelson Page}]
     4: a boy who is employed to run errands [syn: {pageboy}]
     5: a youthful attendant at official functions or ceremonies
        such as legislative functions and weddings
     6: in medieval times a youth acting as a knight's attendant as
        the first stage in training ...
