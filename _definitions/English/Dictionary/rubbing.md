---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rubbing
offline_file: ""
offline_thumbnail: ""
uuid: 0c21f7c0-894e-4980-a3be-3e501f203f6e
updated: 1484310375
title: rubbing
categories:
    - Dictionary
---
rub
     n 1: an unforeseen obstacle [syn: {hang-up}, {hitch}, {snag}]
     2: the act of rubbing or wiping; "he gave the hood a quick rub"
        [syn: {wipe}]
     v 1: move over something with pressure; "rub my hands"; "rub oil
          into her skin"
     2: cause friction; "my sweater scratches" [syn: {fray}, {fret},
         {chafe}, {scratch}]
     3: scrape or rub as if to relieve itching; "Don't scratch your
        insect bites!" [syn: {scratch}, {itch}]
     [also: {rubbing}, {rubbed}]
