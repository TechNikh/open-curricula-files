---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/influence
offline_file: ""
offline_thumbnail: ""
uuid: 0c8bb3f0-c134-4d3c-9b93-e7a9de754f3c
updated: 1484310327
title: influence
categories:
    - Dictionary
---
influence
     n 1: a power to affect persons or events especially power based
          on prestige etc; "used her parents' influence to get the
          job"
     2: causing something without any direct or apparent effort
     3: a cognitive factor that tends to have an effect on what you
        do; "her wishes had a great influence on his thinking"
     4: the effect of one thing (or person) on another; "the
        influence of mechanical action"
     5: one having power to influence another; "she was the most
        important influence in my life"; "he was a bad influence
        on the children"
     v 1: have and exert influence or effect; "The artist's work
          influenced ...
