---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haemoglobin
offline_file: ""
offline_thumbnail: ""
uuid: f3b46bb0-c6cc-4f99-bf72-b8be3bff782d
updated: 1484310160
title: haemoglobin
categories:
    - Dictionary
---
haemoglobin
     n : a hemoprotein composed of globin and heme that gives red
         blood cells their characteristic color; function
         primarily to transport oxygen from the lungs to the body
         tissues; "fish have simpler hemoglobin than mammals"
         [syn: {hemoglobin}, {Hb}]
