---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inseparable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484585281
title: inseparable
categories:
    - Dictionary
---
inseparable
     adj : not capable of being separated; "inseparable pieces of rock"
