---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/implantation
offline_file: ""
offline_thumbnail: ""
uuid: cb0570d8-5120-4ffe-b40c-4f5f6d356ba6
updated: 1484310158
title: implantation
categories:
    - Dictionary
---
implantation
     n 1: (embryology) the organic process whereby a fertilized egg
          becomes implanted in the lining of the uterus of
          placental mammals [syn: {nidation}]
     2: the act of planting or setting in the ground
     3: a surgical procedure that places something in the human
        body; "the implantation of radioactive pellets in the
        prostate gland"
