---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stay
offline_file: ""
offline_thumbnail: ""
uuid: 4b3cb7a6-8ea6-441c-bfe7-342a2fcfc12f
updated: 1484310252
title: stay
categories:
    - Dictionary
---
stay
     n 1: continuing or remaining in a place or state; "they had a
          nice stay in Paris"; "a lengthy hospital stay"; "a
          four-month stay in bankruptcy court"
     2: a judicial order forbidding some action until an event
        occurs or the order is lifted; "the Supreme Court has the
        power to stay an injunction pending an appeal to the whole
        Court"
     3: the state of inactivity following an interruption; "the
        negotiations were in arrest"; "held them in check";
        "during the halt he got some lunch"; "the momentary stay
        enabled him to escape the blow"; "he spent the entire stop
        in his seat" [syn: {arrest}, {check}, ...
