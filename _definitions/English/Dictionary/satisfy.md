---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satisfy
offline_file: ""
offline_thumbnail: ""
uuid: 0539d599-7aef-4e96-90ed-d4a8334e564a
updated: 1484310212
title: satisfy
categories:
    - Dictionary
---
satisfy
     v 1: fulfil the requirements or expectations of [syn: {fulfill},
          {fulfil}, {live up to}] [ant: {fall short of}]
     2: make happy or satisfied [syn: {gratify}] [ant: {dissatisfy}]
     3: fill or meet a want or need [syn: {meet}, {fill}, {fulfill},
         {fulfil}]
     [also: {satisfied}]
