---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_network_aggregation
offline_file: ""
offline_thumbnail: ""
uuid: fc688761-6a2f-4cf4-98b7-00235dc86eda
updated: 1484308825
title: Social network aggregation
tags:
    - Social network aggregators
    - Overlap between multiple social-network services
    - Social networks as source of financial news
    - Notes and references
categories:
    - Self-organization
---
Social network aggregation is the process of collecting content from multiple social network services, such as Instagram, Tumblr, Flickr, LinkedIn, Twitch, YouTube, etc. into one unified presentation. The task is often performed by a social network aggregator (such as Taggbox Meta Rime Hootsuite and FriendFeed), which pulls together information into a single location,[1] or helps a user consolidate multiple social networking profiles into one profile.[2] Various aggregation services provide tools or widgets to allow users to consolidate messages, track friends, combine bookmarks, search across multiple social networking sites, read RSS feeds for multiple social networks, see when their name ...
