---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stocks
offline_file: ""
offline_thumbnail: ""
uuid: 20a0c8f4-6c95-4cd4-806c-6e86734b4b36
updated: 1484310461
title: stocks
categories:
    - Dictionary
---
stocks
     n : a wooden instrument of punishment on a post with holes for
         the neck and hands; offenders were locked in and so
         exposed to public scorn [syn: {pillory}]
