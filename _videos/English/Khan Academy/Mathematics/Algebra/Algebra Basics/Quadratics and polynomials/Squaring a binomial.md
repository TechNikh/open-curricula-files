---
version: 1
type: video
provider: YouTube
id: 4GcNzvILqtM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20Basics/Quadratics%20and%20polynomials/Squaring%20a%20binomial.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 93641f37-99b8-4187-b6c5-1c5c0d702039
updated: 1486069559
title: Squaring a binomial
categories:
    - Quadratics and polynomials
thumbnail_urls:
    - https://i3.ytimg.com/vi/4GcNzvILqtM/default.jpg
    - https://i3.ytimg.com/vi/4GcNzvILqtM/1.jpg
    - https://i3.ytimg.com/vi/4GcNzvILqtM/2.jpg
    - https://i3.ytimg.com/vi/4GcNzvILqtM/3.jpg
---
