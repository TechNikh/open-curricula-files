---
version: 1
type: video
provider: YouTube
id: qSTQF-RycYc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 51474f75-6e1f-41b8-aff3-9dc215758dac
updated: 1486069619
title: Solving a quadratic equation by factoring
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/qSTQF-RycYc/default.jpg
    - https://i3.ytimg.com/vi/qSTQF-RycYc/1.jpg
    - https://i3.ytimg.com/vi/qSTQF-RycYc/2.jpg
    - https://i3.ytimg.com/vi/qSTQF-RycYc/3.jpg
---
