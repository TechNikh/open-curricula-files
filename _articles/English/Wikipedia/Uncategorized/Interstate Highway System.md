---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interstate
offline_file: ""
offline_thumbnail: ""
uuid: 2ac4950e-c39d-41dd-9845-1d7f3a4b86e0
updated: 1484308331
title: Interstate Highway System
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/70px-I-80.svg.png
tags:
    - History
    - Planning
    - Construction
    - 1992–present
    - Discontinuities
    - Expansion
    - Urban Interstates abandoned because of local opposition
    - Standards
    - Speed limits
    - Other uses
    - Numbering system
    - 'Primary (one- and two-digit) routes (contiguous U.S.)'
    - Auxiliary (three-digit) Interstates (contiguous U.S.)
    - Alaska, Hawaii, and Puerto Rico
    - Mile markers and exit numbers
    - Business routes
    - Financing
    - Toll Interstate Highways
    - Chargeable and non-chargeable Interstate routes
    - Signage
    - Interstate shield
    - Exit numbering
    - Sign locations
    - Statistics
    - Volume
    - Elevation
    - Length
    - States
    - Notes
    - Footnotes
    - Works cited
categories:
    - Uncategorized
---
The Dwight D. Eisenhower National System of Interstate and Defense Highways (commonly known as the Interstate Highway System, Interstate Freeway System, Interstate System, or simply the Interstate) is a network of controlled-access highways that forms a part of the National Highway System of the United States. The system is named for President Dwight D. Eisenhower, who championed its formation. Construction was authorized by the Federal Aid Highway Act of 1956, and the original portion was completed 35 years later, although some urban routes were cancelled and never built. The network has since been extended and, as of 2013[update], it had a total length of 47,856 miles (77,017 km).[2] As ...
