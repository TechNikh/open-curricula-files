---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tedious
offline_file: ""
offline_thumbnail: ""
uuid: 2f0ceef5-7349-4a9d-b878-01c9df00e7e8
updated: 1484310216
title: tedious
categories:
    - Dictionary
---
tedious
     adj 1: so lacking in interest as to cause mental weariness; "a
            boring evening with uninteresting people"; "the
            deadening effect of some routine tasks"; "a dull
            play"; "his competent but dull performance"; "a ho-hum
            speaker who couldn't capture their attention"; "what
            an irksome task the writing of long letters is"-
            Edmund Burke; "tedious days on the train"; "the
            tiresome chirping of a cricket"- Mark Twain; "other
            people's dreams are dreadfully wearisome" [syn: {boring},
             {deadening}, {dull}, {ho-hum}, {irksome}, {slow}, {tiresome},
             {wearisome}]
     2: using ...
