---
version: 1
type: video
provider: YouTube
id: p4DTormtEG0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d2b2c25-7f0c-4320-94bc-ea459ad137de
updated: 1486069591
title: Airbus A380 take-off time
categories:
    - One-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/p4DTormtEG0/default.jpg
    - https://i3.ytimg.com/vi/p4DTormtEG0/1.jpg
    - https://i3.ytimg.com/vi/p4DTormtEG0/2.jpg
    - https://i3.ytimg.com/vi/p4DTormtEG0/3.jpg
---
Figuring how long it takes an a380 to take off given a constant acceleration. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/acceleration-tutorial/v/airbus-a380-take-off-distance?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/acceleration-tutorial/v/acceleration?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
