---
version: 1
type: video
provider: YouTube
id: p_61XhXdlxI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%202-%20Multiplying%20two%20monomials%20with%20two%20variables.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 042741bb-ca5e-465d-8dd8-d864cedd60d9
updated: 1486069563
title: 'Example 2- Multiplying two monomials with two variables'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/p_61XhXdlxI/default.jpg
    - https://i3.ytimg.com/vi/p_61XhXdlxI/1.jpg
    - https://i3.ytimg.com/vi/p_61XhXdlxI/2.jpg
    - https://i3.ytimg.com/vi/p_61XhXdlxI/3.jpg
---
