---
version: 1
type: video
provider: YouTube
id: LPQVlWTtQKU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 73f8f865-8395-447f-89ca-91f452a7d9d1
updated: 1486069619
title: Solving systems of two linear equations example
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/LPQVlWTtQKU/default.jpg
    - https://i3.ytimg.com/vi/LPQVlWTtQKU/1.jpg
    - https://i3.ytimg.com/vi/LPQVlWTtQKU/2.jpg
    - https://i3.ytimg.com/vi/LPQVlWTtQKU/3.jpg
---
