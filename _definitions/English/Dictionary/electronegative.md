---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electronegative
offline_file: ""
offline_thumbnail: ""
uuid: 457068a9-fd82-4b83-855b-948b9e4c91ec
updated: 1484310403
title: electronegative
categories:
    - Dictionary
---
electronegative
     adj : having a negative electric charge; "electrons are negative"
           [syn: {negative}] [ant: {neutral}, {positive}]
