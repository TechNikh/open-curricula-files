---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kwh
offline_file: ""
offline_thumbnail: ""
uuid: f0565374-f5ee-4e7f-88f5-135fe73592d1
updated: 1484308321
title: Kilowatt hour
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hydro_quebec_meter.JPG
tags:
    - Definition
    - Examples
    - Symbol and abbreviations for kilowatt hour
    - Conversions
    - Watt hour multiples and billing units
    - Confusion of kilowatt hours (energy) and kilowatts (power)
    - Misuse of watts per hour
    - Other energy-related units
categories:
    - Uncategorized
---
The kilowatt hour (symbol kWh[1]) is a derived unit of energy equal to 3.6 megajoules.[2][3] If the energy is being transmitted or used at a constant rate (power) over a period of time, the total energy in kilowatt hours is the power in kilowatts multiplied by the time in hours. The kilowatt hour is commonly used as a billing unit for energy delivered to consumers by electric utilities.
