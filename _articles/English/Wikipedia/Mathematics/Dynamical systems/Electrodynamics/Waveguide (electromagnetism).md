---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Waveguide_(electromagnetism)
offline_file: ""
offline_thumbnail: ""
uuid: a8369ba5-b42e-4081-9150-75e91b4fd0ee
updated: 1484308767
title: Waveguide (electromagnetism)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Waveguide_collection.jpg
tags:
    - History
    - Principle of operation
    - Analysis
    - Hollow metallic waveguides
    - Waveguide in practice
    - Dielectric waveguides
categories:
    - Electrodynamics
---
In electromagnetics and communications engineering, the term waveguide may refer to any linear structure that conveys electromagnetic waves between its endpoints. However, the original[1] and most common[1] meaning is a hollow metal pipe used to carry radio waves. This type of waveguide is used as a transmission line mostly at microwave frequencies, for such purposes as connecting microwave transmitters and receivers to their antennas, in equipment such as microwave ovens, radar sets, satellite communications, and microwave radio links.
