---
version: 1
type: video
provider: YouTube
id: PJHCADY-Bio
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32a032bd-2f97-4b5c-8d98-926d07328575
updated: 1486069640
title: Polarized Light Explained + Experiments
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/PJHCADY-Bio/default.jpg
    - https://i3.ytimg.com/vi/PJHCADY-Bio/1.jpg
    - https://i3.ytimg.com/vi/PJHCADY-Bio/2.jpg
    - https://i3.ytimg.com/vi/PJHCADY-Bio/3.jpg
---
