---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/molasses
offline_file: ""
offline_thumbnail: ""
uuid: 701c6496-5e56-4174-ae3e-ff47efeb977e
updated: 1484310462
title: molasses
categories:
    - Dictionary
---
molasses
     n : thick dark syrup produced by boiling down juice from sugar
         cane; especially during sugar refining
