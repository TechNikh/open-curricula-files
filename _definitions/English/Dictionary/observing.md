---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observing
offline_file: ""
offline_thumbnail: ""
uuid: d35a6e9e-07d1-4535-b68c-89d7fc687fa9
updated: 1484310303
title: observing
categories:
    - Dictionary
---
observing
     adj : quick to notice; showing quick and keen perception [syn: {observant}]
