---
version: 1
type: video
provider: YouTube
id: ABC_AU1QsBw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2ab8061b-4c15-406a-90d4-b90ab9a126cf
updated: 1486069618
title: Reflecting line across another line example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ABC_AU1QsBw/default.jpg
    - https://i3.ytimg.com/vi/ABC_AU1QsBw/1.jpg
    - https://i3.ytimg.com/vi/ABC_AU1QsBw/2.jpg
    - https://i3.ytimg.com/vi/ABC_AU1QsBw/3.jpg
---
