---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turnstile_(symbol)
offline_file: ""
offline_thumbnail: ""
uuid: 6494071c-a552-4470-87db-ca64a990ec4b
updated: 1484309497
title: Turnstile (symbol)
tags:
    - Interpretations
    - Notes
categories:
    - Proof theory
---
In mathematical logic and computer science the symbol 
  
    
      
        ⊢
      
    
    {\displaystyle \vdash }
  
 has taken the name turnstile because of its resemblance to a typical turnstile if viewed from above. It is also referred to as tee and is often read as "yields", "proves", "satisfies" or "entails". The symbol was first used by Gottlob Frege in his 1879 book on logic, Begriffsschrift.[1]
