---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turpentine
offline_file: ""
offline_thumbnail: ""
uuid: 879040d0-71a6-441c-9d9f-c543c67643ea
updated: 1484310216
title: turpentine
categories:
    - Dictionary
---
turpentine
     n 1: obtained from conifers (especially pines) [syn: {gum
          terpentine}]
     2: volatile liquid distilled from turpentine oleoresin; used as
        paint thinner and solvent and medicinally [syn: {oil of
        turpentine}, {spirit of turpentine}, {turps}]
