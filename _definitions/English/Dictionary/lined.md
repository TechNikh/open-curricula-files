---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lined
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484344861
title: lined
categories:
    - Dictionary
---
lined
     adj 1: bordered by a line of things; "tree lined streets"
     2: (used especially of skin) marked by lines or seams; "their
        lined faces were immeasurably sad"; "a seamed face" [syn:
        {seamed}]
     3: having a lining or liner or a liner; often used in
        combination; "a lined skirt"; "a silk-lined jacket" [ant:
        {unlined}]
