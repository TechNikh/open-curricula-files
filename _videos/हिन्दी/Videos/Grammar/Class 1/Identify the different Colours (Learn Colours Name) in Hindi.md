---
version: 1
type: video
provider: YouTube
id: 3_3JkaDxGu4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 383899c3-cd35-4109-a11f-fd6e10515112
updated: 1486069629
title: Identify the different Colours (Learn Colours Name) in Hindi
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/3_3JkaDxGu4/default.jpg
    - https://i3.ytimg.com/vi/3_3JkaDxGu4/1.jpg
    - https://i3.ytimg.com/vi/3_3JkaDxGu4/2.jpg
    - https://i3.ytimg.com/vi/3_3JkaDxGu4/3.jpg
---
