---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bursting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615581
title: bursting
categories:
    - Dictionary
---
bursting
     adj : (of munitions) going off; "bursting bombs"; "an exploding
           nuclear device"; "a spectacular display of detonating
           anti-tank mines" [syn: {detonating}, {exploding}]
