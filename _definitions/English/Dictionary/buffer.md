---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buffer
offline_file: ""
offline_thumbnail: ""
uuid: 4079201b-b10d-4b39-bbde-7890089ebe60
updated: 1484310535
title: buffer
categories:
    - Dictionary
---
buffer
     n 1: an ionic compound that resists changes in its pH
     2: an inclined metal frame at the front of a locomotive to
        clear the track [syn: {fender}, {cowcatcher}, {pilot}]
     3: (computer science) a part of RAM used for temporary storage
        of data that is waiting to be sent to a device; used to
        compensate for differences in the rate of flow of data
        between components of a computer system [syn: {buffer
        storage}, {buffer store}]
     4: a power tool used to buff surfaces [syn: {polisher}]
     5: a cushion-like device that reduces shock due to contact
        [syn: {fender}]
     6: an implement consisting of soft material mounted on a ...
