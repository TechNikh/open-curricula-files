---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weighing_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 91b7c865-3f5b-4ca3-84da-2b74ae96f806
updated: 1484308608
title: Weighing matrix
tags:
    - Properties
    - Examples
    - Equivalence
    - Open Questions
categories:
    - Combinatorics
---
In mathematics, a weighing matrix W of order n and weight w is an n × n (0,1,-1)-matrix such that 
  
    
      
        W
        
          W
          
            T
          
        
        =
        w
        
          I
          
            n
          
        
      
    
    {\displaystyle WW^{T}=wI_{n}}
  
, where 
  
    
      
        
          W
          
            T
          
        
      
    
    {\displaystyle W^{T}}
  
 is the transpose of 
  
    
      
        W
      
    
    {\displaystyle W}
  
 and 
  
    
      
        
          I
          
            n
          
        
      
    
    {\displaystyle I_{n}}
  
 is the identity matrix of ...
