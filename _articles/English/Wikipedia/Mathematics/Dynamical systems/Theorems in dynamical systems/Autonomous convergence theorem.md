---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Autonomous_convergence_theorem
offline_file: ""
offline_thumbnail: ""
uuid: e19f2cf0-fa44-40ef-bfb7-e33ac67a2964
updated: 1484308699
title: Autonomous convergence theorem
tags:
    - History
    - An example autonomous convergence theorem
    - How autonomous convergence works
    - Notes
categories:
    - Theorems in dynamical systems
---
In mathematics, an autonomous convergence theorem is one of a family of related theorems which specify conditions guaranteeing global asymptotic stability of a continuous autonomous dynamical system.
