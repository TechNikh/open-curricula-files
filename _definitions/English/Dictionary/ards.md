---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ards
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484387461
title: ards
categories:
    - Dictionary
---
ARDS
     n : acute lung injury characterized by coughing and rales;
         inflammation of the lungs which become stiff and fibrous
         and cannot exchange oxygen; occurs among persons exposed
         to irritants such as corrosive chemical vapors or ammonia
         or chlorine etc. [syn: {adult respiratory distress
         syndrome}, {wet lung}, {white lung}]
