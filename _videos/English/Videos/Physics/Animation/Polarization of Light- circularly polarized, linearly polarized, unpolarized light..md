---
version: 1
type: video
provider: YouTube
id: 8YkfEft4p-w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3380e95b-75f4-4203-af9d-aa5a4dd35bee
updated: 1486069656
title: 'Polarization of Light- circularly polarized, linearly polarized, unpolarized light.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/8YkfEft4p-w/default.jpg
    - https://i3.ytimg.com/vi/8YkfEft4p-w/1.jpg
    - https://i3.ytimg.com/vi/8YkfEft4p-w/2.jpg
    - https://i3.ytimg.com/vi/8YkfEft4p-w/3.jpg
---
