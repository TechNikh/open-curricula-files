---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hedetniemi%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 6455f45b-5126-4faf-b92d-00a56a6a6690
updated: 1484309382
title: "Hedetniemi's conjecture"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hdetniemi_conjecture_example.svg.png
tags:
    - Example
    - Special cases
    - Related problems
categories:
    - Graph coloring
---
In graph theory, Hedetniemi's conjecture, named after Stephen T. Hedetniemi, concerns the connection between graph coloring and the tensor product of graphs. This conjecture states that
