---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relay
offline_file: ""
offline_thumbnail: ""
uuid: cd4a88f3-30f4-4670-a699-3df84c37a661
updated: 1484310166
title: relay
categories:
    - Dictionary
---
relay
     n 1: the act of relaying something
     2: electrical device such that current flowing through it in
        one circuit can switch on and off a current in a second
        circuit [syn: {electrical relay}]
     v 1: pass along; "Please relay the news to the villagers"
     2: control or operate by relay
