---
version: 1
type: video
provider: YouTube
id: I82ADyJC7wE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Galaxies%2C%20part%201-%20Crash%20Course%20Astronomy%20%2338.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 922b8502-4501-4db1-8595-922716881fdf
updated: 1486069684
title: 'Galaxies, part 1- Crash Course Astronomy #38'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/I82ADyJC7wE/default.jpg
    - https://i3.ytimg.com/vi/I82ADyJC7wE/1.jpg
    - https://i3.ytimg.com/vi/I82ADyJC7wE/2.jpg
    - https://i3.ytimg.com/vi/I82ADyJC7wE/3.jpg
---
