---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anywhere
offline_file: ""
offline_thumbnail: ""
uuid: d9ab3b70-3477-4a8b-b779-0b31a01935c2
updated: 1484310244
title: anywhere
categories:
    - Dictionary
---
anywhere
     adv : at or in or to any place; "you can find this food anywhere";
           (`anyplace' is used informally for `anywhere') [syn: {anyplace}]
