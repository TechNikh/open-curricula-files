---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 7753676a-64b6-4a36-adb4-023ee3bfddb9
updated: 1484308683
title: Quantum algorithm
tags:
    - Overview
    - Algorithms based on the quantum Fourier transform
    - Deutsch–Jozsa algorithm
    - "Simon's algorithm"
    - Quantum phase estimation algorithm
    - "Shor's algorithm"
    - Hidden subgroup problem
    - Boson sampling problem
    - Estimating Gauss sums
    - Fourier fishing and Fourier checking
    - Algorithms based on amplitude amplification
    - "Grover's algorithm"
    - Quantum counting
    - Algorithms based on quantum walks
    - Element distinctness problem
    - Triangle-finding problem
    - Formula evaluation
    - Group commutativity
    - BQP-complete problems
    - Computing knot invariants
    - Quantum simulation
    - Surveys
categories:
    - Theoretical computer science
---
In quantum computing, a quantum algorithm is an algorithm which runs on a realistic model of quantum computation, the most commonly used model being the quantum circuit model of computation.[1][2] A classical (or non-quantum) algorithm is a finite sequence of instructions, or a step-by-step procedure for solving a problem, where each step or instruction can be performed on a classical computer. Similarly, a quantum algorithm is a step-by-step procedure, where each of the steps can be performed on a quantum computer. Although all classical algorithms can also be performed on a quantum computer,[3] the term quantum algorithm is usually used for those algorithms which seem inherently quantum, ...
