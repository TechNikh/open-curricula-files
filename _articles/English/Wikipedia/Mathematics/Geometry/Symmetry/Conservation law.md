---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conservation_law
offline_file: ""
offline_thumbnail: ""
uuid: a68276ee-fe04-4813-93c9-8ca2dfb9a595
updated: 1484309304
title: Conservation law
tags:
    - Conservation laws as fundamental laws of nature
    - Exact laws
    - Approximate laws
    - Differential forms
    - Integral and weak forms
    - Examples and applications
    - Notes
categories:
    - Symmetry
---
In physics, a conservation law states that a particular measurable property of an isolated physical system does not change as the system evolves over time. Exact conservation laws include conservation of energy, conservation of linear momentum, conservation of angular momentum, and conservation of electric charge. There are also many approximate conservation laws, which apply to such quantities as mass, parity, lepton number, baryon number, strangeness, hypercharge, etc. These quantities are conserved in certain classes of physics processes, but not in all.
