---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thin
offline_file: ""
offline_thumbnail: ""
uuid: a41e92e8-1728-4f6c-98bb-a5c883cd45ec
updated: 1484310325
title: thin
categories:
    - Dictionary
---
thin
     adj 1: of relatively small extent from one surface to the opposite
            or in cross section; "thin wire"; "a thin chiffon
            blouse"; "a thin book"; "a thin layer of paint" [ant:
            {thick}]
     2: lacking excess flesh; "you can't be too rich or too thin";
        "Yon Cassius has a lean and hungry look"-Shakespeare [syn:
         {lean}] [ant: {fat}]
     3: very narrow; "a thin line across the page" [syn: {slender}]
     4: having little substance or significance; "a flimsy excuse";
        "slight evidence"; "a tenuous argument"; "a thin plot"
        [syn: {flimsy}, {slight}, {tenuous}]
     5: not dense; "a thin beard"; "trees were sparse" [syn: ...
