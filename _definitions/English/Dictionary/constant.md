---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constant
offline_file: ""
offline_thumbnail: ""
uuid: a0ee3636-e627-4f44-87ab-5f97451e2620
updated: 1484310311
title: constant
categories:
    - Dictionary
---
constant
     adj 1: persistent in occurrence and unvarying in nature;
            "maintained a constant temperature"; "a constant
            beat"; "principles of unvarying validity"; "a steady
            breeze" [syn: {changeless}, {invariant}, {steady}, {unvarying}]
     2: continually recurring or continuing without interruption;
        "constant repetition of the exercise"; "constant chatter
        of monkeys"
     3: steadfast in purpose or devotion or affection; "a man
        constant in adherence to his ideals"; "a constant lover";
        "constant as the northern star" [ant: {inconstant}]
     4: uninterrupted in time and indefinitely long continuing; "the
        ceaseless ...
