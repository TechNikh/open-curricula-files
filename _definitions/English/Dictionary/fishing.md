---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fishing
offline_file: ""
offline_thumbnail: ""
uuid: 35f5629b-dc02-4f86-aef0-020313c8a346
updated: 1484310242
title: fishing
categories:
    - Dictionary
---
fishing
     n 1: the act of someone who fishes as a diversion [syn: {sportfishing}]
     2: the occupation of catching fish for a living
