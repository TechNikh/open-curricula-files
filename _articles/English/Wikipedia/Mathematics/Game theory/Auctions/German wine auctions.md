---
version: 1
type: article
id: https://en.wikipedia.org/wiki/German_wine_auctions
offline_file: ""
offline_thumbnail: ""
uuid: af1178c9-65b8-42f3-b1a0-6d295690e913
updated: 1484309011
title: German wine auctions
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/452px-Der_junge_Goethe%252C_gemalt_von_Angelica_Kauffmann_1787.JPG'
tags:
    - Focus of the auctions
    - Auction procedure
    - List of auctions
    - Highest prices
categories:
    - Auctions
---
A number of German wine auctions are held each year, where the premier German wine producers auction off some of the best young wines, as well as some older wines. Most auctions are arranged by the regional associations of Verband Deutscher Prädikats- und Qualitätsweingüter (VDP). These auctions differ from wine auctions on the second-hand market held by auction houses, where collectible wines are sold by private or corporate owners, since it is "first hand" wines that are sold.
