---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_sample_consensus
offline_file: ""
offline_thumbnail: ""
uuid: 3c4840b5-14d8-4b55-8720-ced395a46489
updated: 1484309181
title: Random sample consensus
tags:
    - Example
    - Overview
    - Algorithm
    - Matlab implementation
    - Parameters
    - Advantages and disadvantages
    - Applications
    - Development and improvements
    - Related methods
    - Notes
categories:
    - Geometry in computer vision
---
Random sample consensus (RANSAC) is an iterative method to estimate parameters of a mathematical model from a set of observed data that contains outliers, when outliers are to be accorded no influence on the values of the estimates. Therefore, it also can be interpreted as an outlier detection method.[1] It is a non-deterministic algorithm in the sense that it produces a reasonable result only with a certain probability, with this probability increasing as more iterations are allowed. The algorithm was first published by Fischler and Bolles at SRI International in 1981.They used RANSAC to solve the Location Determination Problem (LDP), where the goal is to determine the points in the space ...
