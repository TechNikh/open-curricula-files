---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Glossary_of_arithmetic_and_diophantine_geometry
offline_file: ""
offline_thumbnail: ""
uuid: c1b034a8-7a8a-4b42-9ed7-5fbcb2d7d530
updated: 1484309149
title: Glossary of arithmetic and diophantine geometry
categories:
    - Algebraic geometry
---
This is a glossary of arithmetic and diophantine geometry in mathematics, areas growing out of the traditional study of Diophantine equations to encompass large parts of number theory and algebraic geometry. Much of the theory is in the form of proposed conjectures, which can be related at various levels of generality.
