---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Petroleum_jelly
offline_file: ""
offline_thumbnail: ""
uuid: 7d31d68c-58d5-458e-b18d-983dc1025eb2
updated: 1484308375
title: Petroleum jelly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-White_Petrolatum1.jpg
tags:
    - History
    - Physical properties
    - Comparison with glycerol
    - Uses
    - Medical treatment
    - Skin and hair care
    - Preventing moisture loss
    - Hair grooming
    - Skin lubrication
    - Product care and protection
    - Coating
    - Finishing
    - Lubrication
    - Production processes
    - Other
    - Mechanical, barrier functions
    - Surface cleansing
    - Pet care
    - Clean-up
categories:
    - Uncategorized
---
Petroleum jelly, petrolatum, white petrolatum, soft paraffin/paraffin wax or multi-hydrocarbon, CAS number 8009-03-8, is a semi-solid mixture of hydrocarbons (with carbon numbers mainly higher than 25),[1] originally promoted as a topical ointment for its healing properties.
