---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%9C-recursive_function'
offline_file: ""
offline_thumbnail: ""
uuid: c4519a02-8e74-4556-a435-882dfb9c63f7
updated: 1484309489
title: Μ-recursive function
tags:
    - Definition
    - Equivalence with other models of computability
    - Normal form theorem
    - Symbolism
    - Examples
categories:
    - Computability theory
---
In mathematical logic and computer science, the μ-recursive functions are a class of partial functions from natural numbers to natural numbers that are "computable" in an intuitive sense. In fact, in computability theory it is shown that the μ-recursive functions are precisely the functions that can be computed by Turing machines. The μ-recursive functions are closely related to primitive recursive functions, and their inductive definition (below) builds upon that of the primitive recursive functions. However, not every μ-recursive function is a primitive recursive function—the most famous example is the Ackermann function.
