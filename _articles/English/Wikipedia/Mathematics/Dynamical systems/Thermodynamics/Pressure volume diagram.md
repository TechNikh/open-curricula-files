---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pressure_volume_diagram
offline_file: ""
offline_thumbnail: ""
uuid: 61fea52f-ec31-4cd8-84b8-0f18514ffaae
updated: 1484308855
title: Pressure volume diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Indicator_diagram.png
tags:
    - Description
    - History
    - Applications
    - Thermodynamics
    - Medicine
    - Bibliography
categories:
    - Thermodynamics
---
A pressure volume diagram (or PV diagram, or volume-pressure loop)[1] is used to describe corresponding changes in volume and pressure in a system. They are commonly used in thermodynamics, cardiovascular physiology, and respiratory physiology.
