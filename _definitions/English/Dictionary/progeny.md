---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/progeny
offline_file: ""
offline_thumbnail: ""
uuid: 2f17a2e7-140f-4e4c-b64d-491ac0136494
updated: 1484310309
title: progeny
categories:
    - Dictionary
---
progeny
     n : the immediate descendants of a person; "she was the mother
         of many offspring"; "he died without issue" [syn: {offspring},
          {issue}]
