---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/senior
offline_file: ""
offline_thumbnail: ""
uuid: 9f993819-f1eb-4306-96b2-88e3bbc61b54
updated: 1484310467
title: senior
categories:
    - Dictionary
---
senior
     adj 1: older; higher in rank; longer in length of tenure or
            service; "senior officer" [ant: {junior}]
     2: used of the fourth and final year in United States high
        school or college; "the senior prom" [syn: {senior(a)}, {fourth-year}]
     3: advanced in years; (`aged' is pronounced as two syllables);
        "aged members of the society"; "elderly residents could
        remember the construction of the first skyscraper";
        "senior citizen" [syn: {aged}, {elderly}, {older}]
     n 1: an undergraduate student during the year preceding
          graduation
     2: a person who is older than you are [syn: {elder}]
