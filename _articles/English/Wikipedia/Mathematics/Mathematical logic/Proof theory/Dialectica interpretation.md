---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dialectica_interpretation
offline_file: ""
offline_thumbnail: ""
uuid: a046a77d-fd17-4b8d-9773-1d4502ad24f9
updated: 1484309492
title: Dialectica interpretation
tags:
    - Motivation
    - Dialectica interpretation of intuitionistic logic
    - Formula translation
    - Proof translation (soundness)
    - Characterisation principles
    - Extensions of basic interpretation
    - Induction
    - Classical logic
    - Comprehension
    - Dialectica interpretation of linear logic
    - Variants of the Dialectica interpretation
categories:
    - Proof theory
---
In proof theory, the Dialectica interpretation[1] is a proof interpretation of intuitionistic arithmetic (Heyting arithmetic) into a finite type extension of primitive recursive arithmetic, the so-called System T. It was developed by Kurt Gödel to provide a consistency proof of arithmetic. The name of the interpretation comes from the journal Dialectica, where Gödel's paper was published in a special issue dedicated to Paul Bernays on his 70th birthday.
