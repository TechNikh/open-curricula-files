---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absorption
offline_file: ""
offline_thumbnail: ""
uuid: 9d4ebf32-eb7e-4ea4-a284-1f9db126e181
updated: 1484310328
title: absorption
categories:
    - Dictionary
---
absorption
     n 1: (chemistry) a process in which one substance permeates
          another; a fluid permeates or is dissolved by a liquid
          or solid [syn: {soaking up}]
     2: (physics) the process in which incident radiated energy is
        retained without reflection or transmission on passing
        through a medium; "the absorption of photons by atoms or
        molecules"
     3: the social process of absorbing one cultural group into
        harmony with another [syn: {assimilation}]
     4: the process of absorbing nutrients into the body after
        digestion [syn: {assimilation}]
     5: complete attention; intense mental effort [syn: {concentration},
         ...
