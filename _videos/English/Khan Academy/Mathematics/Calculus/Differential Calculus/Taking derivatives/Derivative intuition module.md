---
version: 1
type: video
provider: YouTube
id: HtvikVD9aa0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Derivative%20intuition%20module.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8184ffe3-aa34-4efb-9e86-58269bec546e
updated: 1486069689
title: Derivative intuition module
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/HtvikVD9aa0/default.jpg
    - https://i3.ytimg.com/vi/HtvikVD9aa0/1.jpg
    - https://i3.ytimg.com/vi/HtvikVD9aa0/2.jpg
    - https://i3.ytimg.com/vi/HtvikVD9aa0/3.jpg
---
