---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resolve
offline_file: ""
offline_thumbnail: ""
uuid: 29f93dbb-bee5-43ed-a013-5469a1d5c09c
updated: 1484310541
title: resolve
categories:
    - Dictionary
---
resolve
     n 1: the trait of being resolute; firmness of purpose; "his
          resoluteness carried him through the battle"; "it was
          his unshakeable resolution to finish the work" [syn: {resoluteness},
           {firmness}, {resolution}] [ant: {irresoluteness}]
     2: a formal expression by a meeting; agreed to by a vote [syn:
        {resolution}, {declaration}]
     v 1: bring to an end; settle conclusively; "The case was
          decided"; "The judge decided the case in favor of the
          plaintiff"; "The father adjudicated when the sons were
          quarreling over their inheritance" [syn: {decide}, {settle},
           {adjudicate}]
     2: reach a conclusion ...
