---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alienation
offline_file: ""
offline_thumbnail: ""
uuid: 2e7e01f5-49dc-42eb-abbb-44b0a0b65a1f
updated: 1484310188
title: alienation
categories:
    - Dictionary
---
alienation
     n 1: the feeling of being alienated from other people [syn: {disaffection},
           {estrangement}]
     2: separation resulting from hostility [syn: {estrangement}]
     3: (law) the voluntary and absolute transfer of title and
        possession of real property from one person to another;
        "the power of alienation is an essential ingredient of
        ownership"
     4: the action of alienating; the action of causing to become
        unfriendly; "his behavior alienated the other students"
