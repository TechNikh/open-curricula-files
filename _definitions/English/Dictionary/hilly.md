---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hilly
offline_file: ""
offline_thumbnail: ""
uuid: e3cc4b1b-8b9c-4175-9f22-4f81f1fb1222
updated: 1484310437
title: hilly
categories:
    - Dictionary
---
hilly
     adj : having hills and crags; "hilly terrain" [syn: {cragged}, {craggy},
            {mountainous}]
