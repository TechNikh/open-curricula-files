---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/congruent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484341261
title: congruent
categories:
    - Dictionary
---
congruent
     adj 1: corresponding in character or kind [syn: {congruous}] [ant:
            {incongruous}]
     2: coinciding when superimposed [ant: {incongruent}]
