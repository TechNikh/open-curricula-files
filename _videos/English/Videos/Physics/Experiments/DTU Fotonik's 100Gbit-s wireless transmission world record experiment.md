---
version: 1
type: video
provider: YouTube
id: J8bgRFhfzeY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a8a2b10c-0237-426f-861f-b6dfc00a872f
updated: 1486069636
title: "DTU Fotonik's 100Gbit/s wireless transmission world record experiment"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/J8bgRFhfzeY/default.jpg
    - https://i3.ytimg.com/vi/J8bgRFhfzeY/1.jpg
    - https://i3.ytimg.com/vi/J8bgRFhfzeY/2.jpg
    - https://i3.ytimg.com/vi/J8bgRFhfzeY/3.jpg
---
