---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thoughtfully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484486581
title: thoughtfully
categories:
    - Dictionary
---
thoughtfully
     adv 1: in a thoughtful manner; "he stared thoughtfully out the
            window" [ant: {thoughtlessly}]
     2: showing thoughtfulness; "he had thoughtfully brought with
        him some food" [ant: {thoughtlessly}]
