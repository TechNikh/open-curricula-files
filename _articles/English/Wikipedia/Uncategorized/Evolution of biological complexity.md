---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Higher_animals
offline_file: ""
offline_thumbnail: ""
uuid: 8562903f-1c2f-4ea0-9e93-3e8f7dab0bcc
updated: 1484308363
title: Evolution of biological complexity
tags:
    - Selection for simplicity and complexity
    - Types of trends in complexity
    - History
categories:
    - Uncategorized
---
The evolution of biological complexity is one important outcome of the process of evolution. Evolution has produced some remarkably complex organisms - although the actual level of complexity is very hard to define or measure accurately in biology, with properties such as gene content, the number of cell types or morphology all being used to assess an organism's complexity.[1][2]
