---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Laplace_operators_in_differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: a2811639-d184-47a4-a876-cd7f23ee2b63
updated: 1484309226
title: Laplace operators in differential geometry
tags:
    - Connection Laplacian
    - Hodge Laplacian
    - Bochner Laplacian
    - Lichnerowicz Laplacian
    - Conformal Laplacian
categories:
    - Differential geometry
---
In differential geometry there are a number of second-order, linear, elliptic differential operators bearing the name Laplacian. This article provides an overview of some of them.
