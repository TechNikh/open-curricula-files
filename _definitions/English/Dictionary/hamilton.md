---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hamilton
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411401
title: hamilton
categories:
    - Dictionary
---
Hamilton
     n 1: Irish mathematician (1806-1865) [syn: {William Rowan
          Hamilton}, {Sir William Rowan Hamilton}]
     2: English beauty who was the mistress of Admiral Nelson
        (1765-1815) [syn: {Lady Emma Hamilton}, {Amy Lyon}]
     3: United States toxicologist known for her work on industrial
        poisons (1869-1970) [syn: {Alice Hamilton}]
     4: United States statesman and leader of the Federalists; as
        the first Secretary of the Treasury he establish a federal
        bank; was mortally wounded in a duel with Aaron Burr
        (1755-1804) [syn: {Alexander Hamilton}]
     5: a port city in southeastern Ontario at the western end of
        Lake Ontario
     ...
