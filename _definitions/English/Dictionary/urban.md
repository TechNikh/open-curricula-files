---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urban
offline_file: ""
offline_thumbnail: ""
uuid: 7aeeb01e-fee2-4442-9828-c5da124b8143
updated: 1484310441
title: urban
categories:
    - Dictionary
---
urban
     adj 1: relating to or concerned with a city or densely populated
            area; "urban sociology"; "urban development"
     2: located in or characteristic of a city or city life; "urban
        property owners"; "urban affairs"; "urban manners" [ant: {rural}]
