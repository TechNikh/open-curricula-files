---
version: 1
type: video
provider: YouTube
id: By1sRYcp4nA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2021%20-%20Temp.%20vs.%20Heat.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20fcfd23-0651-41bb-a61a-ca33244de522
updated: 1486069665
title: 'Eureka 21 - Temp. vs. Heat.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/By1sRYcp4nA/default.jpg
    - https://i3.ytimg.com/vi/By1sRYcp4nA/1.jpg
    - https://i3.ytimg.com/vi/By1sRYcp4nA/2.jpg
    - https://i3.ytimg.com/vi/By1sRYcp4nA/3.jpg
---
