---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Aanderaa%E2%80%93Karp%E2%80%93Rosenberg_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 6ff99ced-a10a-4f93-8319-c5d0de5d38b2
updated: 1484308556
title: Aanderaa–Karp–Rosenberg conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Scorpion_graph.svg.png
tags:
    - Example
    - Definitions
    - Query complexity
    - Deterministic query complexity
    - Randomized query complexity
    - Quantum query complexity
    - Notes
categories:
    - Combinatorics
---
In theoretical computer science, the Aanderaa–Karp–Rosenberg conjecture (also known as the Aanderaa–Rosenberg conjecture or the evasiveness conjecture) is a group of related conjectures about the number of questions of the form "Is there an edge between vertex u and vertex v?" that have to be answered to determine whether or not an undirected graph has a particular property such as planarity or bipartiteness. They are named after Stål Aanderaa, Richard M. Karp, and Arnold L. Rosenberg. According to the conjecture, for a wide class of properties, no algorithm can guarantee that it will be able to skip any questions: any algorithm for determining whether the graph has the property, no ...
