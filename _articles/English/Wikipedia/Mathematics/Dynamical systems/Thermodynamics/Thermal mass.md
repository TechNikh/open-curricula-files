---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_mass
offline_file: ""
offline_thumbnail: ""
uuid: 828c2b09-e312-46c1-977c-e9dd452fd9cd
updated: 1484308866
title: Thermal mass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Heavy_vs_light_weight_school_classroom.JPG
tags:
    - Background
    - Thermal mass in buildings
    - Properties required for good thermal mass
    - Use of thermal mass in different climates
    - Temperate and cold temperate climates
    - Solar-exposed thermal mass
    - Thermal mass for limiting summertime overheating
    - Hot, arid climates (e.g. desert)
    - Hot humid climates (e.g. sub-tropical and tropical)
    - Materials commonly used for thermal mass
    - Seasonal energy storage
categories:
    - Thermodynamics
---
In building design, thermal mass is a property of the mass of a building which enables it to store heat, providing "inertia" against temperature fluctuations. It is sometimes known as the thermal flywheel effect.[1] For example, when outside temperatures are fluctuating throughout the day, a large thermal mass within the insulated portion of a house can serve to "flatten out" the daily temperature fluctuations, since the thermal mass will absorb thermal energy when the surroundings are higher in temperature than the mass, and give thermal energy back when the surroundings are cooler, without reaching thermal equilibrium. This is distinct from a material's insulative value, which reduces a ...
