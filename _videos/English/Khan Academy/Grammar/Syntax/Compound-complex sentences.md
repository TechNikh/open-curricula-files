---
version: 1
type: video
provider: YouTube
id: Hnmm57B0JgQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Compound-complex%20sentences.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1d07e8df-859d-44ed-8902-cd89be4b0157
updated: 1486069599
title: Compound-complex sentences
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hnmm57B0JgQ/default.jpg
    - https://i3.ytimg.com/vi/Hnmm57B0JgQ/1.jpg
    - https://i3.ytimg.com/vi/Hnmm57B0JgQ/2.jpg
    - https://i3.ytimg.com/vi/Hnmm57B0JgQ/3.jpg
---
Compound-complex sentences are compound sentences with dependent or subordinate clauses added to them. Paige and Rosie explain how to spot and use them.

Practice this yourself on Khan Academy right now
www.khanacademy.org/syntax/e/complex-and-compound-complex-sentences/ 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/subject-verb-agreement-syntax-khan-academy 

Missed the previous lesson? watch here:
https://www.khanacademy.org/humanities/grammar/syntax/v/complex-sentences-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
