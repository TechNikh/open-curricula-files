---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nucleus
offline_file: ""
offline_thumbnail: ""
uuid: ec42bfbe-874c-42d4-9934-6db944062360
updated: 1484310297
title: nucleus
categories:
    - Dictionary
---
nucleus
     n 1: a part of the cell containing DNA and RNA and responsible
          for growth and reproduction [syn: {cell nucleus}, {karyon}]
     2: the positively charged dense center of an atom
     3: a small group of indispensable persons or things; "five
        periodicals make up the core of their publishing program"
        [syn: {core}, {core group}]
     4: (astronomy) the center of the head of a comet; consists of
        small solid particles of ice and frozen gas that vaporizes
        on approaching the sun to form the coma and tail
     5: any histologically identifiable mass of neural cell bodies
        in the brain or spinal cord
     [also: {nuclei} (pl)]
