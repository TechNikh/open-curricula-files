---
version: 1
type: video
provider: YouTube
id: gGSu9nb93Es
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f8543c63-bb4d-4f37-a627-0f7946e1571c
updated: 1486069658
title: Levitation at Home
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/gGSu9nb93Es/default.jpg
    - https://i3.ytimg.com/vi/gGSu9nb93Es/1.jpg
    - https://i3.ytimg.com/vi/gGSu9nb93Es/2.jpg
    - https://i3.ytimg.com/vi/gGSu9nb93Es/3.jpg
---
