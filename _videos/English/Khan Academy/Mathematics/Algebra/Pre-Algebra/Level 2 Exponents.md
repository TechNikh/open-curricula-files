---
version: 1
type: video
provider: YouTube
id: 1Nt-t9YJM8k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Level%202%20Exponents.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 588833a9-c39b-4da9-9186-d90a9ba3307f
updated: 1486069567
title: Level 2 Exponents
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/1Nt-t9YJM8k/default.jpg
    - https://i3.ytimg.com/vi/1Nt-t9YJM8k/1.jpg
    - https://i3.ytimg.com/vi/1Nt-t9YJM8k/2.jpg
    - https://i3.ytimg.com/vi/1Nt-t9YJM8k/3.jpg
---
negative exponents
