---
version: 1
type: video
provider: YouTube
id: rMSPVgPWTpM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3ac551e7-2ed7-4405-ad99-926197fc693b
updated: 1486069658
title: 'Anti Gravity Water - Physics magic trick'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/rMSPVgPWTpM/default.jpg
    - https://i3.ytimg.com/vi/rMSPVgPWTpM/1.jpg
    - https://i3.ytimg.com/vi/rMSPVgPWTpM/2.jpg
    - https://i3.ytimg.com/vi/rMSPVgPWTpM/3.jpg
---
