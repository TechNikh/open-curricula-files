---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Rook_and_pawn_versus_rook_endgame
offline_file: ""
offline_thumbnail: ""
uuid: bb1e411f-07b7-49af-aac1-451b7b785f9a
updated: 1484309022
title: Rook and pawn versus rook endgame
tags:
    - Importance
    - Terminology
    - Pawn on the sixth or seventh rank
    - Winning methods
    - Black king is cut off along a rank
    - "Black king is cut off from the pawn's file"
    - Rule of five
    - Lucena position
    - Defending rook prevents the bridge
    - Alternate method for bishop pawns and central pawns
    - Other methods
    - Defensive methods
    - Philidor position
    - Back-rank defense
    - >
        King in front of pawn, but cannot reach the Philidor
        position
    - "Short side" defense
    - Short side defense, less advanced pawn
    - Long side blunder
    - Last-rank defense
    - Frontal defense
    - Rook pawn
    - King in front of pawn
    - Rook in front of pawn
    - Vančura position
    - Most common rook endgame
    - Examples from master games
    - Larsen vs. Browne, 1982
    - Pein vs. Ward, 1997
    - Ward vs. Arkell, 1994
    - Kasparov vs. Kramnik, 2000
    - Anand vs. Shirov, 2004
    - Ward vs. Emms, 1997
    - Subtle differences
    - Zugzwang
    - Rook and two pawns versus rook
categories:
    - Chess theory
---
The rook and pawn versus rook endgame is of fundamental importance to chess endgames (Keres 1973:100), (de la Villa 2008:123–25), (Emms 2008:16), (Burgess 2009:94), (Nunn 2009:106) and has been widely studied (Nunn 1999:6), (Minev 2004:58). Precise play is usually required in these positions. With optimal play, some complicated wins require sixty moves to either checkmate, win the defending rook, or successfully promote the pawn (Speelman, Tisdall & Wade 1993:7). In some cases, thirty-five moves are required to advance the pawn once (Thompson 1986).
