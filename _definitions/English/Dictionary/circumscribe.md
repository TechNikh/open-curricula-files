---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circumscribe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484357821
title: circumscribe
categories:
    - Dictionary
---
circumscribe
     v 1: draw a line around; "He drew a circle around the points"
     2: restrict or confine, "I limit you to two visits to the pub a
        day" [syn: {limit}, {confine}]
     3: to draw a geometric figure around another figure so that the
        two are in contact but do not intersect
