---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Icosian_game
offline_file: ""
offline_thumbnail: ""
uuid: 3f5dc23a-57c6-4bf3-9a5a-a83916ba22af
updated: 1484308627
title: Icosian game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hamiltonian_path.svg.png
categories:
    - Graph theory
---
The icosian game is a mathematical game invented in 1857 by William Rowan Hamilton. The game's object is finding a Hamiltonian cycle along the edges of a dodecahedron such that every vertex is visited a single time, and the ending point is the same as the starting point. The puzzle was distributed commercially as a pegboard with holes at the nodes of the dodecahedral graph and was subsequently marketed in Europe in many forms.
