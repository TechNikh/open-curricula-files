---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protruding
offline_file: ""
offline_thumbnail: ""
uuid: 02240fbc-e657-4abb-b919-f7a2bb682bc1
updated: 1484310316
title: protruding
categories:
    - Dictionary
---
protruding
     adj : extending out above or beyond a surface or boundary; "the
           jutting limb of a tree"; "massive projected
           buttresses"; "his protruding ribs"; "a pile of boards
           sticking over the end of his truck" [syn: {jutting}, {projected},
            {projecting}, {sticking(p)}, {sticking out(p)}]
