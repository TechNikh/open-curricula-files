---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consecutively
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484332681
title: consecutively
categories:
    - Dictionary
---
consecutively
     adv : in a consecutive manner; "he was consecutively ill, then
           well, then ill again"
