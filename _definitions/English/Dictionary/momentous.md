---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/momentous
offline_file: ""
offline_thumbnail: ""
uuid: 3a4fb64e-78c0-46de-b4a3-45e15f95eee1
updated: 1484310172
title: momentous
categories:
    - Dictionary
---
momentous
     adj : of very great significance; "deciding to drop the atom bomb
           was a very big decision"; "a momentous event" [syn: {big}]
