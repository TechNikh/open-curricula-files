---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diatomic
offline_file: ""
offline_thumbnail: ""
uuid: 27ecd8c5-926e-43c6-9fec-54ef48500fd2
updated: 1484310397
title: diatomic
categories:
    - Dictionary
---
diatomic
     adj : of or relating to a molecule made up of two atoms; "a
           diatomic molelcule"
