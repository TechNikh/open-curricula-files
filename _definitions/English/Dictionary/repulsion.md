---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repulsion
offline_file: ""
offline_thumbnail: ""
uuid: 016bbfa8-a7f4-4ddc-b186-eac6097dea48
updated: 1484310204
title: repulsion
categories:
    - Dictionary
---
repulsion
     n 1: the force by which bodies repel one another [syn: {repulsive
          force}] [ant: {attraction}]
     2: intense aversion [syn: {repugnance}, {revulsion}, {horror}]
     3: the act of repulsing or repelling an attack; a successful
        defensive stand [syn: {standoff}]
