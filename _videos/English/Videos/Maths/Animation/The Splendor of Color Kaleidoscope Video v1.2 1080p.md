---
version: 1
type: video
provider: YouTube
id: 2a9jH8yT454
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 66c5bbdd-ace3-44a6-8abd-e20a610c24cc
updated: 1486069649
title: The Splendor of Color Kaleidoscope Video v1.2 1080p
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/2a9jH8yT454/default.jpg
    - https://i3.ytimg.com/vi/2a9jH8yT454/1.jpg
    - https://i3.ytimg.com/vi/2a9jH8yT454/2.jpg
    - https://i3.ytimg.com/vi/2a9jH8yT454/3.jpg
---
