---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intrusion
offline_file: ""
offline_thumbnail: ""
uuid: 49fe5921-1add-4fbb-93dc-28f6451abbeb
updated: 1484310515
title: intrusion
categories:
    - Dictionary
---
intrusion
     n 1: any entry into an area not previously occupied; "an invasion
          of tourists"; "an invasion of locusts" [syn: {invasion},
           {encroachment}]
     2: entrance by force or without permission or welcome
     3: the forcing of molten rock into fissures or between strata
        of an earlier rock formation
     4: rock produced by an intrusive process
     5: entry to another's property without right or permission
        [syn: {trespass}, {encroachment}, {violation}, {usurpation}]
