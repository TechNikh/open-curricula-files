---
version: 1
type: video
provider: YouTube
id: auKDJF7K3hk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bdba10b5-7dac-4cd3-b713-1d471d1e4684
updated: 1486069630
title: 'Use of First Hindi Vowel Swar  अ'
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/auKDJF7K3hk/default.jpg
    - https://i3.ytimg.com/vi/auKDJF7K3hk/1.jpg
    - https://i3.ytimg.com/vi/auKDJF7K3hk/2.jpg
    - https://i3.ytimg.com/vi/auKDJF7K3hk/3.jpg
---
