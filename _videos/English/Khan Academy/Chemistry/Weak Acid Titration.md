---
version: 1
type: video
provider: YouTube
id: gbpc_JBG1F0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Weak%20Acid%20Titration.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 59cbe0ef-e09c-45ff-9149-470adaa3d1b0
updated: 1486069589
title: Weak Acid Titration
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/gbpc_JBG1F0/default.jpg
    - https://i3.ytimg.com/vi/gbpc_JBG1F0/1.jpg
    - https://i3.ytimg.com/vi/gbpc_JBG1F0/2.jpg
    - https://i3.ytimg.com/vi/gbpc_JBG1F0/3.jpg
---
Equivalence point when titrating a weak acid
More free lessons at: http://www.khanacademy.org/video?v=gbpc_JBG1F0
