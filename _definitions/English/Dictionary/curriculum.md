---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curriculum
offline_file: ""
offline_thumbnail: ""
uuid: 223bb247-8e41-46ef-91dd-e5d4e189f2af
updated: 1484310575
title: curriculum
categories:
    - Dictionary
---
curriculum
     n : an integrated course of academic studies; "he was admitted
         to a new program at the university" [syn: {course of
         study}, {program}, {programme}, {syllabus}]
     [also: {curricula} (pl)]
