---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/improper
offline_file: ""
offline_thumbnail: ""
uuid: 0f37cdbe-458d-4d99-a443-de5b9ee78b4f
updated: 1484310480
title: improper
categories:
    - Dictionary
---
improper
     adj 1: not suitable or right or appropriate; "slightly improper to
            dine alone with a married man"; "improper medication"
            [ant: {proper}]
     2: not conforming to legality, moral law, or social convention;
        "an unconventional marriage"; "improper banking practices"
        [syn: {unconventional}, {unlawful}]
     3: not appropriate for a purpose or occasion; "unsuitable
        attire for the office"; "said all the wrong things" [syn:
        {unsuitable}, {wrong}]
