---
version: 1
type: video
provider: YouTube
id: BleyJL6dW8w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d593f415-ca40-4b87-9f3a-f7066e63504c
updated: 1486069643
title: Moon Dance
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/BleyJL6dW8w/default.jpg
    - https://i3.ytimg.com/vi/BleyJL6dW8w/1.jpg
    - https://i3.ytimg.com/vi/BleyJL6dW8w/2.jpg
    - https://i3.ytimg.com/vi/BleyJL6dW8w/3.jpg
---
