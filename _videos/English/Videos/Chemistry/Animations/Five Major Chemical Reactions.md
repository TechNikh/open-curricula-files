---
version: 1
type: video
provider: YouTube
id: tE4668aarck
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Five%20Major%20Chemical%20Reactions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 39e686b4-6c78-48c1-9a5e-710073634599
updated: 1486069664
title: Five Major Chemical Reactions
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/tE4668aarck/default.jpg
    - https://i3.ytimg.com/vi/tE4668aarck/1.jpg
    - https://i3.ytimg.com/vi/tE4668aarck/2.jpg
    - https://i3.ytimg.com/vi/tE4668aarck/3.jpg
---
