---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cross_section_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: d1ee31be-4914-411e-a13e-578f7aede6f5
updated: 1484308927
title: Cross section (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Cylindric_section.svg.png
tags:
    - Conic sections
    - Other mathematical examples
    - Examples in science
    - Area and volume
    - Cross section of four-dimensional space
categories:
    - Elementary geometry
---
In geometry and science, a cross section is the intersection of a body in three-dimensional space with a plane, or the analog in higher-dimensional space. Cutting an object into slices creates many parallel cross sections. A cross section of three-dimensional space that is parallel to two of the axes is a contour line; for example, if a plane cuts through mountains of a raised-relief map parallel to the ground, the result is a contour line in two-dimensional space showing points of equal elevation.
