---
version: 1
type: video
provider: YouTube
id: 9k97m8oWnaY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Introduction%20to%20the%20surface%20integral.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e5919dbf-09b5-4be2-8779-946c53e918af
updated: 1486069692
title: Introduction to the surface integral
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/9k97m8oWnaY/default.jpg
    - https://i3.ytimg.com/vi/9k97m8oWnaY/1.jpg
    - https://i3.ytimg.com/vi/9k97m8oWnaY/2.jpg
    - https://i3.ytimg.com/vi/9k97m8oWnaY/3.jpg
---
