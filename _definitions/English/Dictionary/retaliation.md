---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retaliation
offline_file: ""
offline_thumbnail: ""
uuid: 2571590d-0721-4fb7-a757-d373a5144bd7
updated: 1484310183
title: retaliation
categories:
    - Dictionary
---
retaliation
     n : action taken in return for an injury or offense [syn: {revenge}]
