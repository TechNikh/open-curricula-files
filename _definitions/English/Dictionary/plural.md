---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plural
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463722
title: plural
categories:
    - Dictionary
---
plural
     adj : grammatical number category referring to two or more items
           or units [ant: {singular}]
     n : the form of a word that is used to denote more than one
         [syn: {plural form}] [ant: {singular}]
