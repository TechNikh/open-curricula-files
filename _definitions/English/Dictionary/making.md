---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/making
offline_file: ""
offline_thumbnail: ""
uuid: f6bb39b4-6c5f-4dba-811b-0a026ec13fdb
updated: 1484310335
title: making
categories:
    - Dictionary
---
making
     n : the act that results in something coming to be; "the
         devising of plans"; "the fashioning of pots and pans";
         "the making of measurements"; "it was already in the
         making" [syn: {devising}, {fashioning}]
