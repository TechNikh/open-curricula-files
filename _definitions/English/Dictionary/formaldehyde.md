---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formaldehyde
offline_file: ""
offline_thumbnail: ""
uuid: 7baf418d-5902-4b58-bcc4-ee943cb17da5
updated: 1484310419
title: formaldehyde
categories:
    - Dictionary
---
formaldehyde
     n : a colorless poisonous gas; made by the oxidation of methanol
         [syn: {methanal}]
