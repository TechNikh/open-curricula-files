---
version: 1
type: video
provider: YouTube
id: IGCVTSQw7WU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7c638c49-587d-4eda-84e3-c47d4d731186
updated: 1486069684
title: 'A Brief History of the Universe- Crash Course Astronomy #44'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/IGCVTSQw7WU/default.jpg
    - https://i3.ytimg.com/vi/IGCVTSQw7WU/1.jpg
    - https://i3.ytimg.com/vi/IGCVTSQw7WU/2.jpg
    - https://i3.ytimg.com/vi/IGCVTSQw7WU/3.jpg
---
