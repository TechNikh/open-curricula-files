---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modern
offline_file: ""
offline_thumbnail: ""
uuid: c80bce8a-c463-4d74-824a-e34ea8ea1602
updated: 1484310277
title: modern
categories:
    - Dictionary
---
modern
     adj 1: belonging to the modern era; since the Middle Ages; "modern
            art"; "modern furniture"; "modern history"; "totem
            poles are modern rather than prehistoric" [ant: {nonmodern}]
     2: relating to a recently developed fashion or style; "their
        offices are in a modern skyscraper"; "tables in
        modernistic designs"; [syn: {mod}, {modernistic}]
     3: characteristic of present-day art and music and literature
        and architecture
     4: ahead of the times; "the advanced teaching methods"; "had
        advanced views on the subject"; "a forward-looking
        corporation"; "is British industry innovative enough?"
        [syn: ...
