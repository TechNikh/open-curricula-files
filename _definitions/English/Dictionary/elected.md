---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elected
offline_file: ""
offline_thumbnail: ""
uuid: a6a68d39-db53-4f38-ad56-d8190966fe46
updated: 1484310567
title: elected
categories:
    - Dictionary
---
elected
     adj : subject to popular election; "elective official" [syn: {elective}]
           [ant: {appointive}]
