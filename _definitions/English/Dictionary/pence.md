---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484511061
title: pence
categories:
    - Dictionary
---
penny
     n 1: a fractional monetary unit of Ireland and the United
          Kingdom; equal to one hundredth of a pound
     2: a coin worth one-hundredth of the value of the basic unit
        [syn: {cent}, {centime}]
     [also: {pence} (pl)]
