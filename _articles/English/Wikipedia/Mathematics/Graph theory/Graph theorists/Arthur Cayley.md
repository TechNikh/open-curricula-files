---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arthur_Cayley
offline_file: ""
offline_thumbnail: ""
uuid: 6be6cead-7940-41fb-a8c6-7ab40053d773
updated: 1484309459
title: Arthur Cayley
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_14.jpg
tags:
    - Early years
    - Education
    - As a lawyer
    - As a professor
    - BAE
    - The Collected Papers
    - Legacy
    - Bibliography
    - Sources
categories:
    - Graph theorists
---
Arthur Cayley F.R.S. (/ˈkeɪli/; 16 August 1821 – 26 January 1895) was a British mathematician. He helped found the modern British school of pure mathematics.
