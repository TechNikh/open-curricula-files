---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surreal_number
offline_file: ""
offline_thumbnail: ""
uuid: 8d63dab6-6e89-4dd1-a671-243902061b26
updated: 1484308533
title: Surreal number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Surreal_number_tree.svg.png
tags:
    - Overview
    - Construction
    - Forms
    - Numeric forms
    - Equivalence classes of numeric forms
    - Order
    - Induction
    - Arithmetic
    - Negation
    - Addition
    - Multiplication
    - Division
    - Consistency
    - Arithmetic closure
    - Infinity
    - Contents of Sω
    - Transfinite induction
    - Powers of ω
    - Exponential function
    - Other exponentials
    - Basic induction
    - Results
    - Examples
    - Exponentiation
    - Surcomplex numbers
    - Games
    - Application to combinatorial game theory
    - Alternative realizations
    - Sign expansion
    - Definitions
    - Addition and multiplication
    - Correspondence with Conway
    - Axiomatic approach
    - Hahn series
    - Relation to hyperreals
    - Notes
categories:
    - Combinatorial game theory
---
In mathematics, the surreal number system is an arithmetic continuum containing the real numbers as well as infinite and infinitesimal numbers, respectively larger or smaller in absolute value than any positive real number. The surreals share many properties with the reals, including a total order ≤ and the usual arithmetic operations (addition, subtraction, multiplication, and division); as such, they form an ordered field. (Strictly speaking, the surreals are not a set, but a proper class.[a]) If formulated in Von Neumann–Bernays–Gödel set theory, the surreal numbers are the largest possible ordered field; all other ordered fields, such as the rationals, the reals, the rational ...
