---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deviation
offline_file: ""
offline_thumbnail: ""
uuid: 06b7ae36-e4dc-476a-8103-83b7763bdef0
updated: 1484310212
title: deviation
categories:
    - Dictionary
---
deviation
     n 1: a variation that deviates from the standard or norm; "the
          deviation from the mean" [syn: {divergence}, {departure},
           {difference}]
     2: the difference between an observed value and the expected
        value of a variable or function
     3: the error of a compass due to local magnetic disturbances
     4: deviate behavior [syn: {deviance}]
     5: a turning aside (of your course or attention or concern); "a
        diversion from the main highway"; "a digression into
        irrelevant details"; "a deflection from his goal" [syn: {diversion},
         {digression}, {deflection}, {deflexion}, {divagation}]
