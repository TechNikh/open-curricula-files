---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chittoor
offline_file: ""
offline_thumbnail: ""
uuid: e64b511d-428b-4afe-be8b-37bec151e055
updated: 1484308444
title: Chittoor, Andhra Pradesh
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-APSRTC%2527S_JnNURM_2_AL_SLF_MetroExpress.jpg'
tags:
    - History
    - Pre-history
    - Political history
    - geography
    - Climate
    - Demographics
    - Governance
    - Economy
    - Landmarks
    - Education
    - Transport
    - culture
    - Politics
    - Notable people
categories:
    - Uncategorized
---
Chittoor is a city and district headquarters in Chittoor district of the Indian state of Andhra Pradesh. It is also the mandal and divisional headquarters of Chittoor mandal and Chittoor revenue division, respectively.[2] The city has a population of 153,766 and that of the agglomeration is 175,640.[3]
