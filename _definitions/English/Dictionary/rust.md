---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rust
offline_file: ""
offline_thumbnail: ""
uuid: e5eac7b1-6221-412e-8c91-5004aac43393
updated: 1484310379
title: rust
categories:
    - Dictionary
---
rust
     adj : of the color of rust [syn: {rusty}]
     n 1: a red or brown oxide coating on iron or steel caused by the
          action of oxygen and moisture
     2: a reddish-brown discoloration of leaves and stems caused by
        a rust fungus
     3: the formation of reddish-brown ferric oxides on iron by
        low-temperature oxidation in the presence of water [syn: {rusting}]
     4: any of various fungi causing rust disease in plants [syn: {rust
        fungus}]
     v 1: become destroyed by water, air, or an etching chemical such
          as an acid; "The metal corroded"; "The pipes rusted"
          [syn: {corrode}]
     2: cause to deteriorate due to the action of water, ...
