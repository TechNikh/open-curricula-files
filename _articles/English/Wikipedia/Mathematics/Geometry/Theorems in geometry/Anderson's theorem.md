---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Anderson%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 327017c9-cdec-4f08-8692-a56d673de806
updated: 1484309107
title: "Anderson's theorem"
categories:
    - Theorems in geometry
---
In mathematics, Anderson's theorem is a result in real analysis and geometry which says that the integral of an integrable, symmetric, unimodal, non-negative function f over an n-dimensional convex body K does not decrease if K is translated inwards towards the origin. This is a natural statement, since the graph of f can be thought of as a hill with a single peak over the origin; however, for n ≥ 2, the proof is not entirely obvious, as there may be points x of the body K where the value f(x) is larger than at the corresponding translate of x.
