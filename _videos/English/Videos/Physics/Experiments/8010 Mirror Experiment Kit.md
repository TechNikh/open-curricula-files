---
version: 1
type: video
provider: YouTube
id: n9hsCKCOA2Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a7495d3c-f253-404a-a868-1f0fa17a9607
updated: 1486069636
title: 8010 Mirror Experiment Kit
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/n9hsCKCOA2Q/default.jpg
    - https://i3.ytimg.com/vi/n9hsCKCOA2Q/1.jpg
    - https://i3.ytimg.com/vi/n9hsCKCOA2Q/2.jpg
    - https://i3.ytimg.com/vi/n9hsCKCOA2Q/3.jpg
---
