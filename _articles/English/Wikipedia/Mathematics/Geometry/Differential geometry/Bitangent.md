---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bitangent
offline_file: ""
offline_thumbnail: ""
uuid: 5948ba92-dec3-4c29-8bb4-238634fe03a7
updated: 1484309204
title: Bitangent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Trott_bitangents.png
tags:
    - Bitangents of algebraic curves
    - Bitangents of polygons
    - Related concepts
categories:
    - Differential geometry
---
In mathematics, a bitangent to a curve C is a line L that touches C in two distinct points P and Q and that has the same direction as C at these points. That is, L is a tangent line at P and at Q.
