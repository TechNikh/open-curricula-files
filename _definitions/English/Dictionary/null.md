---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/null
offline_file: ""
offline_thumbnail: ""
uuid: e1d1cf3a-b672-409a-a873-e622dbe5fc56
updated: 1484310138
title: 'null'
categories:
    - Dictionary
---
null
     adj : lacking any legal or binding force; "null and void" [syn: {void}]
     n : a quantity of no importance; "it looked like nothing I had
         ever seen before"; "reduced to nil all the work we had
         done"; "we racked up a pathetic goose egg"; "it was all
         for naught"; "I didn't hear zilch about it" [syn: {nothing},
          {nil}, {nix}, {nada}, {aught}, {cipher}, {cypher}, {goose
         egg}, {naught}, {zero}, {zilch}, {zip}]
