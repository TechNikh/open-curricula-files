---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_elephant
offline_file: ""
offline_thumbnail: ""
uuid: 2725daf1-e3cc-4c8c-a21a-b9232ce8b8a1
updated: 1484309147
title: General elephant
categories:
    - Algebraic geometry
---
In algebraic geometry, general elephant is an idiosyncratic name for a general element of the anticanonical system of a variety, introduced by Reid (1987). For 3-folds the general elephant problem (or conjecture) asks whether general elephants have at most du Val singularities; this has been proved in several cases.
