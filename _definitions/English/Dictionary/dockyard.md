---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dockyard
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484564221
title: dockyard
categories:
    - Dictionary
---
dockyard
     n : an establishment on the waterfront where vessels are built
         or fitted out or repaired
