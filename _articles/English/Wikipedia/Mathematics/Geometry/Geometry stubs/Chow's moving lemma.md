---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chow%27s_moving_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 8c6707cb-db05-4d93-a21b-c2210245ee6c
updated: 1484309357
title: "Chow's moving lemma"
categories:
    - Geometry stubs
---
In algebraic geometry, Chow's moving lemma, proved by Wei-Liang Chow (1956), states: given algebraic cycles Y, Z on a nonsingular quasi-projective variety X, there is another algebraic cycle Z' on X such that Z' is rationally equivalent to Z and Y and Z' intersect properly. The lemma is one of key ingredients in developing the intersection theory, as it is used to show the uniqueness of the theory.
