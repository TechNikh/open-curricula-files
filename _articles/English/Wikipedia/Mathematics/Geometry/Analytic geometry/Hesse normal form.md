---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hesse_normal_form
offline_file: ""
offline_thumbnail: ""
uuid: ea0ed165-3566-4dc4-9a5f-0191230f98b4
updated: 1484309165
title: Hesse normal form
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Hesse.svg_0.png
categories:
    - Analytic geometry
---
The Hesse normal form named after Otto Hesse, is an equation used in analytic geometry, and describes a line in 
  
    
      
        
          
            R
          
          
            2
          
        
      
    
    {\displaystyle \mathbb {R} ^{2}}
  
 or a plane in Euclidean space 
  
    
      
        
          
            R
          
          
            3
          
        
      
    
    {\displaystyle \mathbb {R} ^{3}}
  
 or a hyperplane in higher dimensions.[1] It is primarily used for calculating distances, and is written in vector notation as
