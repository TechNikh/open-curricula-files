---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oral_cavity
offline_file: ""
offline_thumbnail: ""
uuid: 095f3ddb-9ac7-4b61-abcc-6ae6b2d41a58
updated: 1484308375
title: Mouth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Australien-Krokodil.JPG
tags:
    - Development
    - Anatomy
    - Invertebrates
    - Vertebrates
    - Other functions of the mouth
categories:
    - Uncategorized
---
In biological anatomy, commonly referred to as the mouth, under formal names such as the oral cavity, buccal cavity, or in Latin cavum oris,[1] is the opening through which many animals take in food and issue vocal sounds. It is also the cavity lying at the upper end of the alimentary canal, bounded on the outside by the lips and inside by the pharynx and containing in higher vertebrates the tongue and teeth.[2] This cavity is also known as the buccal cavity, from the Latin bucca ("cheek").[3]
