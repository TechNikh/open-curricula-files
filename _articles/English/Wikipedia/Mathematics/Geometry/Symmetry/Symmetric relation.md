---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_relation
offline_file: ""
offline_thumbnail: ""
uuid: 595b53ed-b3b3-407a-a58c-9e4bc79cc45e
updated: 1484309319
title: Symmetric relation
tags:
    - Examples
    - In mathematics
    - Outside mathematics
    - Relationship to asymmetric and antisymmetric relations
    - Additional aspects
categories:
    - Symmetry
---
In mathematics and other areas, a binary relation R over a set X is symmetric if it holds for all a and b in X that a is related to b if and only if b is related to a.
