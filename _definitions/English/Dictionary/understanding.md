---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/understanding
offline_file: ""
offline_thumbnail: ""
uuid: e9008abc-ca5d-4ed4-b142-c73298f45522
updated: 1484310325
title: understanding
categories:
    - Dictionary
---
understanding
     adj : characterized by understanding based on comprehension and
           discernment and empathy; "an understanding friend"
     n 1: the cognitive condition of someone who understands; "he has
          virtually no understanding of social cause and effect"
          [syn: {apprehension}, {discernment}, {savvy}]
     2: the statement (oral or written) of an exchange of promises;
        "they had an agreement that they would not interfere in
        each other's business"; "there was an understanding
        between management and the workers" [syn: {agreement}]
     3: an inclination to support or be loyal to or to agree with an
        opinion; "his sympathies were ...
