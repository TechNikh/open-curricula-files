---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geology
offline_file: ""
offline_thumbnail: ""
uuid: bce6f5ba-c6f7-4022-8a5e-98edde65e5f0
updated: 1484310286
title: geology
categories:
    - Dictionary
---
geology
     n : a science that deals with the history of the earth as
         recorded in rocks
