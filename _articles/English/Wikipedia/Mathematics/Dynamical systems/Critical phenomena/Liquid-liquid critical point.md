---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Liquid-liquid_critical_point
offline_file: ""
offline_thumbnail: ""
uuid: abcc7648-8d93-4cda-9e7e-9a7b2cd1ea1c
updated: 1484308746
title: Liquid-liquid critical point
categories:
    - Critical phenomena
---
A liquid-liquid critical point (or LLCP) is the endpoint of a liquid-liquid phase transition line (LLPT); it is a critical point where two types of local structures coexist at the exact ratio of unity. This hypothesis was first developed by H. Eugene Stanley [1] to obtain a quantitative understanding of the huge number of anomalies present in water.[2]
