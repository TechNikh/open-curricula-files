---
version: 1
type: video
provider: YouTube
id: 69V-60sga3M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Ideal%20gas%20equation%20example%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 28885baa-da19-4160-9515-4b0db1ab4880
updated: 1486069589
title: Ideal gas equation example 3
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/69V-60sga3M/default.jpg
    - https://i3.ytimg.com/vi/69V-60sga3M/1.jpg
    - https://i3.ytimg.com/vi/69V-60sga3M/2.jpg
    - https://i3.ytimg.com/vi/69V-60sga3M/3.jpg
---
Figuring out the mass of Oxygen we have at a given P, V, and T, and why we need to check the units of R. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/ideal-gas-example-4?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/ideal-gas-equation-example-2?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
