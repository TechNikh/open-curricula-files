---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jacks
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484388001
title: jacks
categories:
    - Dictionary
---
jacks
     n 1: plaything consisting of small 6-pointed metal pieces that
          are used (along with a ball) to play the game of jacks
          [syn: {jackstones}]
     2: a game in which jackstones are thrown and picked up in
        various groups between bounces of a small rubber ball
        [syn: {jackstones}, {knucklebones}]
