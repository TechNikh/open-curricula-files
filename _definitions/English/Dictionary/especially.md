---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/especially
offline_file: ""
offline_thumbnail: ""
uuid: 6eb60e88-b6ab-46b4-b714-186490718987
updated: 1484310262
title: especially
categories:
    - Dictionary
---
especially
     adv 1: to a distinctly greater extent or degree than is common; "he
            was particularly fussy about spelling"; "a
            particularly gruesome attack"; "under peculiarly
            tragic circumstances"; "an especially (or specially)
            cautious approach to the danger" [syn: {particularly},
             {peculiarly}, {specially}]
     2: in a special manner; "a specially arranged dinner" [syn: {specially}]
