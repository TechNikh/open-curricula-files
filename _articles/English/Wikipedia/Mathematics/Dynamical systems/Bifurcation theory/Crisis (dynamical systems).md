---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crisis_(dynamical_systems)
offline_file: ""
offline_thumbnail: ""
uuid: 7d1eb142-6d1f-4d8a-accf-534ef16d9e98
updated: 1484308692
title: Crisis (dynamical systems)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bifurcations_and_crises_of_Ikeda_attractor.png
categories:
    - Bifurcation theory
---
In applied mathematics and Astrodynamics, in the theory of dynamical systems, a crisis is the sudden appearance or disappearance of a strange attractor as the parameters of a dynamical system are varied.[1][2] This global bifurcation occurs when a chaotic attractor comes into contact with an unstable periodic orbit or its stable manifold.[3] As the orbit approaches the unstable orbit it will diverge away from the previous attractor, leading to a qualitatively different behaviour. Crises can produce intermittent behaviour.
