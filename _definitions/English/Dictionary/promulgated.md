---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promulgated
offline_file: ""
offline_thumbnail: ""
uuid: dc08d58d-4d1b-4056-a1a3-d9e1f9443c07
updated: 1484310573
title: promulgated
categories:
    - Dictionary
---
promulgated
     adj : formally made public; "published accounts" [syn: {published}]
