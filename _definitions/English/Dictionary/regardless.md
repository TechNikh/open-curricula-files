---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regardless
offline_file: ""
offline_thumbnail: ""
uuid: 11aa39dc-01ba-43e1-b0af-eeabbd66d3e3
updated: 1484310279
title: regardless
categories:
    - Dictionary
---
regardless
     adj : (usually followed by `of') without due thought or
           consideration; "careless of the consequences"; "the
           proverbial grasshopper--thoughtless of tomorrow";
           "crushing the blooms with regardless tread" [syn: {careless(p)},
            {thoughtless(p)}]
     adv : in spite of everything; without regard to drawbacks; "he
           carried on regardless of the difficulties" [syn: {irrespective},
            {disregardless}, {no matter}, {disregarding}]
