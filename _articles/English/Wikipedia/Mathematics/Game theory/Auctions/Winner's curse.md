---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Winner%27s_curse'
offline_file: ""
offline_thumbnail: ""
uuid: c3e406e1-f834-41fc-96ca-80186ef74224
updated: 1484309017
title: "Winner's curse"
tags:
    - Explanation
    - Examples
    - Related uses
categories:
    - Auctions
---
The winner's curse is a phenomenon that may occur in common value auctions with incomplete information. In short, the winner's curse says that in such an auction, the winner will tend to overpay. The winner may overpay or be "cursed" in one of two ways: 1) the winning bid exceeds the value of the auctioned asset such that the winner is worse off in absolute terms; or 2) the value of the asset is less than the bidder anticipated, so the bidder may still have a net gain but will be worse off than anticipated.[1] However, an actual overpayment will generally occur only if the winner fails to account for the winner's curse when bidding (an outcome that, according to the revenue equivalence ...
