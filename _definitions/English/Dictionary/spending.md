---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spending
offline_file: ""
offline_thumbnail: ""
uuid: 58967b5c-3f44-44aa-a367-fb59078e5cc8
updated: 1484310448
title: spending
categories:
    - Dictionary
---
spending
     n : the act of spending or disbursing money [syn: {disbursement},
          {disbursal}, {outlay}]
