---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slipper
offline_file: ""
offline_thumbnail: ""
uuid: e7cd2b10-da7d-4e08-aaaf-ffc525888f30
updated: 1484310220
title: slipper
categories:
    - Dictionary
---
slipper
     n 1: low footwear that can be slipped on and off easily; usually
          worn indoors [syn: {carpet slipper}]
     2: a person who slips or slides because of loss of traction
        [syn: {skidder}, {slider}]
