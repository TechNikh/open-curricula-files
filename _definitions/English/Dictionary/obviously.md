---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obviously
offline_file: ""
offline_thumbnail: ""
uuid: 3acf6345-bc76-498c-aeb4-f73e66b755f6
updated: 1484310277
title: obviously
categories:
    - Dictionary
---
obviously
     adv : unmistakably (`plain' is often used informally for
           `plainly'); "the answer is obviously wrong"; "she was
           in bed and evidently in great pain"; "he was manifestly
           too important to leave off the guest list"; "it is all
           patently nonsense"; "she has apparently been living
           here for some time"; "I thought he owned the property,
           but apparently not"; "You are plainly wrong"; "he is
           plain stubborn" [syn: {evidently}, {manifestly}, {patently},
            {apparently}, {plainly}, {plain}]
