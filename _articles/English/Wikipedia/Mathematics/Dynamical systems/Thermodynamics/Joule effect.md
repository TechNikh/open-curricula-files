---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Joule_effect
uuid: 1355bbb7-1806-4e14-b6fc-aa15c464510b
updated: 1480455144
title: Joule effect
tags:
    - "Joule's first law"
    - Magnetostriction
    - Joule expansion
    - Gough–Joule effect
categories:
    - Thermodynamics
---
Joule effect and Joule's law are any of several different physical effects discovered or characterized by English physicist James Prescott Joule. These physical effects are not the same, but all are frequently or occasionally referred to in literature as the "Joule effect" or "Joule law" These physical effects include:
