---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ford
offline_file: ""
offline_thumbnail: ""
uuid: 0e65a884-cbe8-4dd9-9527-f5ce5f4f88bc
updated: 1484310525
title: ford
categories:
    - Dictionary
---
Ford
     n 1: United States film maker (1896-1973) [syn: {John Ford}]
     2: grandson of Henry Ford (1917-1987) [syn: {Henry Ford II}]
     3: son of Henry Ford (1893-1943) [syn: {Edsel Bryant Ford}]
     4: English writer and editor (1873-1939) [syn: {Ford Madox Ford},
         {Ford Hermann Hueffer}]
     5: 38th President of the United States; appointed Vice
        President and succeeded Nixon when Nixon resigned (1913-)
        [syn: {Gerald Ford}, {Gerald R. Ford}, {Gerald Rudolph
        Ford}, {President Ford}]
     6: United States manufacturer of automobiles who pioneered mass
        production (1863-1947) [syn: {Henry Ford}]
     7: a shallow area in a stream that can be ...
