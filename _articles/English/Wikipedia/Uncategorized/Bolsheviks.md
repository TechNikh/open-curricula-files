---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bolsheviks
offline_file: ""
offline_thumbnail: ""
uuid: aa32a44d-ced9-4b58-8463-daa7fb1b3e91
updated: 1484308479
title: Bolsheviks
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Presidium_of_the_9th_Congress_of_the_Russian_Communist_Party_%2528Bolsheviks%2529_0.jpg'
tags:
    - History of the split
    - Origins of the name
    - Composition of the party
    - Beginning of the 1905 Revolution (1903–1905)
    - The Mensheviks ("The minority") (1906–1907)
    - Split between Lenin and Bogdanov (1908–10)
    - Final attempt at party unity (1910)
    - Forming a separate party (1912)
    - Derogatory usage of "Bolshevik"
    - Related terms
categories:
    - Uncategorized
---
The Bolsheviks, originally also Bolshevists[1][a] or Bolsheviki[3] (Russian: большевики, большевик (singular); IPA: [bəlʲʂɨˈvʲik]; derived from большинство bol'shinstvo, "majority", literally meaning "one of the majority") were a faction of the Marxist Russian Social Democratic Labour Party (RSDLP) which split apart from the Menshevik faction[b] at the Second Party Congress in 1903.[4] The RSDLP was a revolutionary socialist political party formed in 1898 in Minsk to unite the various revolutionary organisations of the Russian Empire into one party.
