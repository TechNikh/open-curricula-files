---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Finite_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 54a32a1f-ea75-45fd-ab30-87794b72aaeb
updated: 1484308572
title: Finite geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Order_2_affine_plane.svg.png
tags:
    - Finite affine and projective planes
    - Order of planes
    - History
    - Finite spaces of 3 or more dimensions
    - Axiomatic definition
    - Algebraic construction
    - >
        Classification of finite projective spaces by geometric
        dimension
    - The smallest projective three-space
    - "Kirkman's Schoolgirl Problem"
    - Notes
categories:
    - Combinatorics
---
A finite geometry is any geometric system that has only a finite number of points. The familiar Euclidean geometry is not finite, because a Euclidean line contains infinitely many points. A geometry based on the graphics displayed on a computer screen, where the pixels are considered to be the points, would be a finite geometry. While there are many systems that could be called finite geometries, attention is mostly paid to the finite projective and affine spaces because of their regularity and simplicity. Other significant types of finite geometry are finite Möbius or inversive planes and Laguerre planes, which are examples of a general type called Benz planes, and their ...
