---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greenhouse
offline_file: ""
offline_thumbnail: ""
uuid: f70deb22-c1f4-489d-93ce-0b14ee42736e
updated: 1484310246
title: greenhouse
categories:
    - Dictionary
---
greenhouse
     adj : of or relating to or caused by the greenhouse effect;
           "greehouse gases"
     n : a building with glass walls and roof; for the cultivation
         and exhibition of plants under controlled conditions
         [syn: {nursery}, {glasshouse}]
