---
version: 1
type: video
provider: YouTube
id: y_omJiYB2gk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e8353d31-9460-48ad-ac1b-b95dd7a92a33
updated: 1486069662
title: Chemical energy
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/y_omJiYB2gk/default.jpg
    - https://i3.ytimg.com/vi/y_omJiYB2gk/1.jpg
    - https://i3.ytimg.com/vi/y_omJiYB2gk/2.jpg
    - https://i3.ytimg.com/vi/y_omJiYB2gk/3.jpg
---
