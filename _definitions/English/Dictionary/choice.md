---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/choice
offline_file: ""
offline_thumbnail: ""
uuid: d5ff39d5-fe13-44be-b8b7-2dcd84a2a2ac
updated: 1484310221
title: choice
categories:
    - Dictionary
---
choice
     adj 1: of superior grade; "choice wines"; "prime beef"; "prize
            carnations"; "quality paper"; "select peaches" [syn: {prime(a)},
             {prize}, {quality}, {select}]
     2: appealing to refined taste; "choice wine"
     n 1: the person or thing chosen or selected; "he was my pick for
          mayor" [syn: {pick}, {selection}]
     2: the act of choosing or selecting; "your choice of colors was
        unfortunate"; "you can take your pick" [syn: {selection},
        {option}, {pick}]
     3: one of a number of things from which only one can be chosen;
        "what option did I have?"; "there no other alternative";
        "my only choice is to refuse" [syn: ...
