---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ptolemy%27s_table_of_chords'
offline_file: ""
offline_thumbnail: ""
uuid: 9f85d7f4-75a1-4afe-a4cf-0bc0cce9e4f2
updated: 1484309347
title: "Ptolemy's table of chords"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Chords.svg.png
tags:
    - The chord function and the table
    - How Ptolemy computed chords
    - >
        The numeral system and the appearance of the untranslated
        table
categories:
    - Trigonometry
---
The table of chords, created by the astronomer, geometer, and geographer Ptolemy in Egypt during the 2nd century AD, is a trigonometric table in Book I, chapter 11 of Ptolemy's Almagest,[1] a treatise on mathematical astronomy. It is essentially equivalent to a table of values of the sine function. It was the earliest trigonometric table extensive enough for many practical purposes, including those of astronomy (an earlier table of chords by Hipparchus gave chords only for arcs that were multiples of 7½° = π/24 radians).[2] Centuries passed before more extensive trigonometric tables were created. One such table is the Canon Sinuum created at the end of the 16th century.
