---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mid-day
offline_file: ""
offline_thumbnail: ""
uuid: f9119bef-6ae5-4f31-989a-efca3e3f12db
updated: 1484308463
title: Noon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Big_Brum_from_Hill_Street_-_geograph.org.uk_-_499120.jpg
tags:
    - Etymology
    - Solar noon
    - Nomenclature
    - Notes
categories:
    - Uncategorized
---
Noon (also midday or noon time) is usually defined as 12 o'clock in the daytime, that is, 12 in the afternoon or 12 p.m. The term midday is also used colloquially to refer to an arbitrary period of time in the middle of the day. Solar noon is when the sun crosses the meridian and is at its highest elevation in the sky, at 12 o'clock apparent solar time. The local or clock time of solar noon depends on the longitude and date.[1] The opposite of noon is midnight.
