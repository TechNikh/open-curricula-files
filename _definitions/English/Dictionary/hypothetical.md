---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypothetical
offline_file: ""
offline_thumbnail: ""
uuid: f387e375-7cf6-4d69-a5df-0d3ab91b1182
updated: 1484310366
title: hypothetical
categories:
    - Dictionary
---
hypothetical
     adj : based on hypothesis; "a hypothetical situation"; "the site
           of a hypothetical colony" [syn: {hypothetic}]
