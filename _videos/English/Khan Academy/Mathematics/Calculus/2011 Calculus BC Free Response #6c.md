---
version: 1
type: video
provider: YouTube
id: l1hJABkcuYI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%236c.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 24fa0b18-e727-44e0-baab-fdc3b146ad27
updated: 1486069690
title: '2011 Calculus BC Free Response #6c'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/l1hJABkcuYI/default.jpg
    - https://i3.ytimg.com/vi/l1hJABkcuYI/1.jpg
    - https://i3.ytimg.com/vi/l1hJABkcuYI/2.jpg
    - https://i3.ytimg.com/vi/l1hJABkcuYI/3.jpg
---
