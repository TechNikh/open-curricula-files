---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symbolic_dynamics
offline_file: ""
offline_thumbnail: ""
uuid: a398e719-8844-42d9-bb12-97a0eef7bedb
updated: 1484308830
title: Symbolic dynamics
tags:
    - History
    - Examples
    - Applications
categories:
    - Symbolic dynamics
---
In mathematics, symbolic dynamics is the practice of modeling a topological or smooth dynamical system by a discrete space consisting of infinite sequences of abstract symbols, each of which corresponds to a state of the system, with the dynamics (evolution) given by the shift operator. Formally, a Markov partition is used to provide a finite cover for the smooth system; each set of the cover is associated with a single symbol, and the sequences of symbols result as a trajectory of the system moves from one covering set to another.
