---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emotional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619541
title: emotional
categories:
    - Dictionary
---
emotional
     adj 1: determined or actuated by emotion rather than reason; "it
            was an emotional judgment" [ant: {cerebral}]
     2: of more than usual emotion; "his behavior was highly
        emotional" [ant: {unemotional}]
     3: of or pertaining to emotion; "emotional health"; "an
        emotional crisis"
     4: extravagantly demonstrative; "insincere and effusive
        demonstrations of sentimental friendship"; "a large
        gushing female"; "write unrestrained and gushy poetry"
        [syn: {effusive}, {gushing(a)}, {gushy}]
     5: of persons; excessively affected by emotion; "he would
        become emotional over nothing at all"; "she was worked up
        ...
