---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Energetics
offline_file: ""
offline_thumbnail: ""
uuid: a14b54a0-2305-4cc6-8b8c-f78e6266998d
updated: 1484308838
title: Energetics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Genomics_GTL_Program_Payoffs.jpg
tags:
    - Aims
    - History
    - Theories of energetics
categories:
    - Thermodynamics
---
Energetics (also called energy economics) is the study of energy under transformation. Because energy flows at all scales, from the quantum level to the biosphere and cosmos, energetics is a very broad discipline, encompassing for example thermodynamics, chemistry, biological energetics, biochemistry and ecological energetics. Where each branch of energetics begins and ends is a topic of constant debate. For example, Lehninger (1973, p. 21) contended that when the science of thermodynamics deals with energy exchanges of all types, it can be called energetics.
