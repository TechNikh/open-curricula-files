---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stooping
offline_file: ""
offline_thumbnail: ""
uuid: 127db40c-d1bb-4c9a-8a59-8255e05699a9
updated: 1484310150
title: stooping
categories:
    - Dictionary
---
stooping
     adj : having the back and shoulders rounded; not erect; "a little
           oldish misshapen stooping woman" [syn: {hunched}, {round-backed},
            {round-shouldered}, {stooped}, {crooked}]
