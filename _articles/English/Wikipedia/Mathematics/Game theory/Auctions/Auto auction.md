---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auto_auction
offline_file: ""
offline_thumbnail: ""
uuid: 1fc06992-a1bb-4c50-ba2b-5880b6a3335c
updated: 1484309010
title: Auto auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parking_lot_at_HAA_Kobe.jpg
tags:
    - Country specific
    - Japan
    - United States
    - United Kingdom
    - Car dealer auctions
    - Pricing
    - Condition
    - Inspection
    - Inventory finance
    - Notes
categories:
    - Auctions
---
Auto auctions are a method of selling new, and most often, used vehicles based on auction system. Auto auctions can be found in most nations, but are often unused by most people, since in most nations such as the United States, auto auctions are exclusive to used car dealers. In a few countries, such as Japan, auto auctions are well known and used, sometimes indirectly, by most residents.
