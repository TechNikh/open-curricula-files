---
title: Salina in a Garden - making salt from sea water
youtube_id: -sCbBFod6EA
tags:
    - Salina
    - salt
    - making
    - Fielding
    - Ecosal
    - Atlantis
    - ALAS
categories:
    - Water harvesting
---
