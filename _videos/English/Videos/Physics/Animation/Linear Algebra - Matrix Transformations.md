---
version: 1
type: video
provider: YouTube
id: IrggOvOSZr4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3e91e18b-b35a-4376-aec6-d226591b62da
updated: 1486069656
title: 'Linear Algebra - Matrix Transformations'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/IrggOvOSZr4/default.jpg
    - https://i3.ytimg.com/vi/IrggOvOSZr4/1.jpg
    - https://i3.ytimg.com/vi/IrggOvOSZr4/2.jpg
    - https://i3.ytimg.com/vi/IrggOvOSZr4/3.jpg
---
