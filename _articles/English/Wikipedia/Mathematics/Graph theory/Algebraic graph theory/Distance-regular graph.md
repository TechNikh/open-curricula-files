---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance-regular_graph
offline_file: ""
offline_thumbnail: ""
uuid: 17fccb70-90af-4def-ad56-f22b37543611
updated: 1484309371
title: Distance-regular graph
tags:
    - Intersection numbers
    - Distance adjacency matrices
    - Examples
    - Cubic distance-regular graphs
    - Notes
categories:
    - Algebraic graph theory
---
In mathematics, a distance-regular graph is a regular graph such that for any two vertices v and w, the number of vertices at distance j from v and at distance k from w depends only upon j, k, and i = d(v, w).
