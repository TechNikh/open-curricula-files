---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Igusa_quartic
offline_file: ""
offline_thumbnail: ""
uuid: 88660018-d23d-4498-8565-5de24c6eafaa
updated: 1484309365
title: Igusa quartic
categories:
    - Geometry stubs
---
In algebraic geometry, the Igusa quartic (also called the Castelnuovo–Richmond quartic CR4 or the Castelnuovo–Richmond–Igusa quartic) is a quartic hypersurface in 4-dimensional projective space, studied by Igusa (1962). It is closely related to the moduli space of genus 2 curves with level 2 structure. It is the dual of the Segre cubic.
