---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/xylem
offline_file: ""
offline_thumbnail: ""
uuid: 5d6127f3-8d41-4fdf-9c08-8bea272c583e
updated: 1484310158
title: xylem
categories:
    - Dictionary
---
xylem
     n : the woody part of plants: the supporting and
         water-conducting tissue, consisting primarily of
         tracheids and vessels
