---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Arithmetic_hyperbolic_3-manifold
offline_file: ""
offline_thumbnail: ""
uuid: f428394f-c033-43bd-80f5-d09eea903c65
updated: 1484308502
title: Arithmetic hyperbolic 3-manifold
tags:
    - Definition and examples
    - Quaternion algebras
    - Arithmetic Kleinian groups
    - Examples
    - Trace field of arithmetic manifolds
    - >
        Geometry and spectrum of arithmetic hyperbolic
        three-manifolds
    - Volume formula
    - Finiteness results
    - Remarkable arithmetic hyperbolic three-manifolds
    - Spectrum and Ramanujan conjectures
    - Arithmetic manifolds in three-dimensional topology
    - Notes
categories:
    - Number theory
---
In mathematics, more precisely in group theory and hyperbolic geometry, Arithmetic Kleinian groups are a special class of Kleinian groups constructed using orders in quaternion algebras. They are particular instances of arithmetic groups. An arithmetic hyperbolic three-manifold is the quotient of hyperbolic space 
  
    
      
        
          
            H
          
          
            3
          
        
      
    
    {\displaystyle \mathbb {H} ^{3}}
  
 by an arithmetic Kleinian group. These manifolds include some particularly beautiful or remarkable examples.
