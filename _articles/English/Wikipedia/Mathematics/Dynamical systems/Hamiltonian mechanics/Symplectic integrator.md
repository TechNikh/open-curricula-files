---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symplectic_integrator
offline_file: ""
offline_thumbnail: ""
uuid: 1e3ab2eb-da0d-49d5-a2b2-3dd247c2a532
updated: 1484308801
title: Symplectic integrator
tags:
    - Introduction
    - Splitting methods for separable Hamiltonians
    - Splitting methods for general nonseparable Hamiltonians
    - Examples
    - A first-order example
    - A second-order example
    - A third-order example
    - A fourth-order example
categories:
    - Hamiltonian mechanics
---
In mathematics, a symplectic integrator (SI) is a numerical integration scheme for Hamiltonian systems. Symplectic integrators form the subclass of geometric integrators which, by definition, are canonical transformations. They are widely used in nonlinear dynamics, molecular dynamics, discrete element methods, accelerator physics, plasma physics, quantum physics, and celestial mechanics.
