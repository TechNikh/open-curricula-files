---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unable
offline_file: ""
offline_thumbnail: ""
uuid: 0361b38e-334b-4db8-8069-ef89f919c293
updated: 1484310220
title: unable
categories:
    - Dictionary
---
unable
     adj 1: (usually followed by `to') not having the necessary means or
            skill or know-how; "unable to get to town without a
            car"; "unable to obtain funds" [syn: {not able}] [ant:
             {able}]
     2: (usually followed by `to') lacking necessary physical or
        mental ability; "dyslexics are unable to learn to read
        adequately"; "the sun was unable to melt enough snow"
        [syn: {unable(p)}]
     3: lacking in power or forcefulness; "an ineffectual ruler";
        "like an unable phoenix in hot ashes" [syn: {ineffective},
         {ineffectual}]
