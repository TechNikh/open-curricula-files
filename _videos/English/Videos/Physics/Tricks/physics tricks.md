---
version: 1
type: video
provider: YouTube
id: EJiXt3KCpIM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a7c00ba-792a-4442-ab4c-762908b27f5c
updated: 1486069658
title: physics tricks
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/EJiXt3KCpIM/default.jpg
    - https://i3.ytimg.com/vi/EJiXt3KCpIM/1.jpg
    - https://i3.ytimg.com/vi/EJiXt3KCpIM/2.jpg
    - https://i3.ytimg.com/vi/EJiXt3KCpIM/3.jpg
---
