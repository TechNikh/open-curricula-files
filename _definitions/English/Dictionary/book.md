---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/book
offline_file: ""
offline_thumbnail: ""
uuid: 92cfa38f-5df8-4218-8b02-353fe8e78149
updated: 1484310327
title: book
categories:
    - Dictionary
---
book
     n 1: a written work or composition that has been published
          (printed on pages bound together); "I am reading a good
          book on economics"
     2: physical objects consisting of a number of pages bound
        together; "he used a large book as a doorstop" [syn: {volume}]
     3: a record in which commercial accounts are recorded; "they
        got a subpoena to examine our books" [syn: {ledger}, {leger},
         {account book}, {book of account}]
     4: a number of sheets (ticket or stamps etc.) bound together on
        one edge; "he bought a book of stamps"
     5: a compilation of the known facts regarding something or
        someone; "Al Smith used to say, ...
