---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/talent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423041
title: talent
categories:
    - Dictionary
---
talent
     n 1: natural qualities or talents [syn: {endowment}, {gift}, {natural
          endowment}]
     2: a person who possesses unusual innate ability in some field
        or activity
