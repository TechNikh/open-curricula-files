---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disagreement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570401
title: disagreement
categories:
    - Dictionary
---
disagreement
     n 1: a conflict of people's opinions or actions or characters
          [syn: {dissension}, {dissonance}] [ant: {agreement}]
     2: a difference between conflicting facts or claims or
        opinions; "a growing divergence of opinion" [syn: {discrepancy},
         {divergence}, {variance}]
     3: the speech act of disagreeing or arguing or disputing [ant:
        {agreement}]
