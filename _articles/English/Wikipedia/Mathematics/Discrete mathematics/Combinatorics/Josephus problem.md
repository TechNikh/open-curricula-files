---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Josephus_problem
offline_file: ""
offline_thumbnail: ""
uuid: 7d321a38-4e83-4e61-afaa-be728e989013
updated: 1484308585
title: Josephus problem
tags:
    - History
    - Variants and generalizations
    - Solution
    - k=2
    - The general case
    - Notes
categories:
    - Combinatorics
---
People are standing in a circle waiting to be executed. Counting begins at a specified point in the circle and proceeds around the circle in a specified direction. After a specified number of people are skipped, the next person is executed. The procedure is repeated with the remaining people, starting with the next person, going in the same direction and skipping the same number of people, until only one person remains, and is freed.
