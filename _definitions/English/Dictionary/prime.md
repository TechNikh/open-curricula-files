---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prime
offline_file: ""
offline_thumbnail: ""
uuid: 062c275d-759f-42d6-9ff6-3ec1c44afceb
updated: 1484310411
title: prime
categories:
    - Dictionary
---
prime
     adj 1: first in rank or degree; "an architect of premier rank";
            "the prime minister" [syn: {premier(a)}, {prime(a)}]
     2: used of the first or originating agent; "prime mover" [syn:
        {prime(a)}]
     3: of superior grade; "choice wines"; "prime beef"; "prize
        carnations"; "quality paper"; "select peaches" [syn: {choice},
         {prime(a)}, {prize}, {quality}, {select}]
     4: of or relating to or being an integer that cannot be
        factored into other integers; "prime number"
     5: at the best stage; "our manhood's prime vigor"- Robert
        Browning
     n 1: a number that has no factor but itself and 1 [syn: {prime
          quantity}]
   ...
