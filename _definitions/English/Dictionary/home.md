---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/home
offline_file: ""
offline_thumbnail: ""
uuid: 7f6d1379-e024-454d-a1bb-8b8cadcfa5d2
updated: 1484310236
title: home
categories:
    - Dictionary
---
home
     adj 1: used of your own ground; "a home game" [syn: {home(a)}]
            [ant: {away}]
     2: relating to or being where one lives or where one's roots
        are; "my home town"
     3: inside the country; "the British Home Office has broader
        responsibilities than the United States Department of the
        Interior"; "the nation's internal politics" [syn: {home(a)},
         {interior(a)}, {internal}, {national}]
     n 1: where you live at a particular time; "deliver the package to
          my home"; "he doesn't have a home to go to"; "your place
          or mine?" [syn: {place}]
     2: housing that someone is living in; "he built a modest
        dwelling near ...
