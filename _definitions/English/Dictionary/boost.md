---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boost
offline_file: ""
offline_thumbnail: ""
uuid: 7ce3e86d-b89f-49a3-9636-efe265d6393d
updated: 1484310561
title: boost
categories:
    - Dictionary
---
boost
     n 1: the act of giving hope or support to someone [syn: {encouragement}]
     2: an increase in cost; "they asked for a 10% rise in rates"
        [syn: {rise}, {hike}, {cost increase}]
     3: the act of giving an upward push; "he gave her a boost over
        the fence"
     v 1: increase; "The landlord hiked up the rents" [syn: {hike}, {hike
          up}]
     2: give a boost to; be beneficial to; "The tax cut will boost
        the economy"
     3: contribute to the progress or growth of; "I am promoting the
        use of computers in the classroom" [syn: {promote}, {advance},
         {further}, {encourage}]
     4: increase or raise; "boost the voltage in an electrical
   ...
