---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hereditary_property
offline_file: ""
offline_thumbnail: ""
uuid: 9b439f5f-ba4c-4fbf-89df-c08a1c432470
updated: 1484308616
title: Hereditary property
tags:
    - In topology
    - In graph theory
    - Monotone property
    - In model theory
    - In matroid theory
    - In set theory
categories:
    - Graph theory
---
In mathematics, a hereditary property is a property of an object, that inherits to all its subobjects, where the term subobject depends on the context. These properties are particularly considered in topology and graph theory, but also in set theory.
