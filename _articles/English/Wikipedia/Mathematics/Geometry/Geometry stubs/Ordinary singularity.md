---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordinary_singularity
offline_file: ""
offline_thumbnail: ""
uuid: dab08e5a-6563-4e0f-85e4-273e3accd3f5
updated: 1484309369
title: Ordinary singularity
categories:
    - Geometry stubs
---
In mathematics, an ordinary singularity of an algebraic curve is a singular point of multiplicity r where the r tangents at the point are distinct (Walker 1950, p. 54). In higher dimensions the literature on algebraic geometry contains many inequivalent definitions of ordinary singular points.
