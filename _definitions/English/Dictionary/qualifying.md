---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/qualifying
offline_file: ""
offline_thumbnail: ""
uuid: ebdea2da-24a5-47a5-b33a-f7d78cf028a0
updated: 1484310573
title: qualifying
categories:
    - Dictionary
---
qualifying
     adj : referring to or qualifying another sentence element;
           "relative pronoun"; "relative clause"
     n 1: the grammatical relation that exists when a word qualifies
          the meaning of the phrase [syn: {modification}, {limiting}]
     2: success in satisfying a test or requirement; "his future
        depended on his passing that test"; "he got a pass in
        introductory chemistry" [syn: {passing}, {pass}] [ant: {failing}]
