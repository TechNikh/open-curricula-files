---
version: 1
type: video
provider: YouTube
id: f4_QB5D4OtI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fdc557f2-6c0a-4af1-b741-f48a324371ea
updated: 1486069622
title: 'How to Trick Your Math Teacher 6 - Math Tricks Revealed'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/f4_QB5D4OtI/default.jpg
    - https://i3.ytimg.com/vi/f4_QB5D4OtI/1.jpg
    - https://i3.ytimg.com/vi/f4_QB5D4OtI/2.jpg
    - https://i3.ytimg.com/vi/f4_QB5D4OtI/3.jpg
---
