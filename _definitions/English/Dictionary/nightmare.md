---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nightmare
offline_file: ""
offline_thumbnail: ""
uuid: d536c708-9c9a-47c7-9afd-dae2e52a5b40
updated: 1484310567
title: nightmare
categories:
    - Dictionary
---
nightmare
     n 1: a situation resembling a terrifying dream [syn: {incubus}]
     2: a terrifying or deeply upsetting dream
