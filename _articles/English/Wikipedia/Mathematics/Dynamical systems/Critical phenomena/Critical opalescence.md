---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_opalescence
offline_file: ""
offline_thumbnail: ""
uuid: 6a0a6916-2d65-4eb8-b6e1-71d049acf4f2
updated: 1484308746
title: Critical opalescence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-CriticalPointMeasurementEthane.jpg
categories:
    - Critical phenomena
---
Critical opalescence is a phenomenon which arises in the region of a continuous, or second-order, phase transition. Originally reported by Charles Cagniard de la Tour in 1823 in mixtures of alcohol and water, its importance was recognised by Thomas Andrews in 1869 following his experiments on the liquid-gas transition in carbon dioxide, many other examples have been discovered since. The phenomenon is most commonly demonstrated in binary fluid mixtures, such as methanol and cyclohexane. As the critical point is approached, the sizes of the gas and liquid region begin to fluctuate over increasingly large length scales. As the density fluctuations become of a size comparable to the wavelength ...
