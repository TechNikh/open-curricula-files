---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amie_Wilkinson
offline_file: ""
offline_thumbnail: ""
uuid: c0ec5cb4-9d51-4a13-a208-e6d1cca0f10e
updated: 1484308702
title: Amie Wilkinson
categories:
    - Dynamical systems theorists
---
Amie Wilkinson (born 1968) is an American mathematician working in ergodic theory and smooth dynamical systems. She received a bachelor of arts degree from Harvard University in 1989 and a PhD from the University of California, Berkeley in 1995 under the direction of Charles C. Pugh.[1] She is currently a professor of mathematics at the University of Chicago. Wilkinson's work focuses on the geometric and statistical properties of diffeomorphisms and flows with a particular emphasis on stable ergodicity and partial hyperbolicity. In a series of papers with Christian Bonatti and Sylvain Crovisier, Wilkinson studied centralizers of diffeomorphisms[2][3] settling the C1 case of the twelfth ...
