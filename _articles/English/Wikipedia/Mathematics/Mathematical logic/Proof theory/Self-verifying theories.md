---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-verifying_theories
offline_file: ""
offline_thumbnail: ""
uuid: 47d5d85d-8af0-40e8-9eb5-4ab6d0818830
updated: 1484309499
title: Self-verifying theories
categories:
    - Proof theory
---
Self-verifying theories are consistent first-order systems of arithmetic much weaker than Peano arithmetic that are capable of proving their own consistency. Dan Willard was the first to investigate their properties, and he has described a family of such systems. According to Gödel's incompleteness theorem, these systems cannot contain the theory of Peano arithmetic, and in fact, not even the weak fragment of Robinson arithmetic; nonetheless, they can contain strong theorems.
