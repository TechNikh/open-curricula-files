---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467561
title: cite
categories:
    - Dictionary
---
cite
     v 1: make reference to; "His name was mentioned in connection
          with the invention" [syn: {mention}, {advert}, {bring up},
           {name}, {refer}]
     2: commend; "he was cited for his outstanding achievements"
        [syn: {mention}]
     3: refer to; "he referenced his colleagues' work" [syn: {reference}]
     4: repeat a passage from; "He quoted the Bible to her" [syn: {quote}]
     5: refer to for illustration or proof; "He said he could quote
        several instances of this behavior" [syn: {quote}]
     6: advance evidence for [syn: {adduce}, {abduce}]
     7: call in an official matter, such as to attend court [syn: {summon},
         {summons}]
