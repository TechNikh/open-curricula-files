---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jay_Hambidge
offline_file: ""
offline_thumbnail: ""
uuid: f77f5057-c589-4150-8495-4bd832fa77c3
updated: 1484309311
title: Jay Hambidge
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-At_the_Tomb_of_Omar_Khayyam_-_by_Jay_Hambidge.jpg
categories:
    - Symmetry
---
Jay Hambidge (1867–1924) was a Canadian born American artist. He was a pupil at the Art Students' League in New York and of William Chase, and a thorough student of classical art. He conceived the idea that the study of arithmetic with the aid of geometrical designs was the foundation of the proportion and symmetry in Greek architecture, sculpture and ceramics.[citation needed] Careful examination and measurements of classical buildings in Greece, among them the Parthenon, the temple of Apollo at Bassæ, of Zeus at Olympia and Athenæ at Ægina, prompted him to formulate the theory of "dynamic symmetry"[citation needed] as demonstrated in his works Dynamic Symmetry: The Greek Vase (1920) ...
