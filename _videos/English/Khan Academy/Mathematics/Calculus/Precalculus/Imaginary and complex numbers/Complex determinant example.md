---
version: 1
type: video
provider: YouTube
id: E7OkUomRq1Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Imaginary%20and%20complex%20numbers/Complex%20determinant%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2f6cc915-0e52-40a9-a0f4-fa1885665989
updated: 1486069694
title: Complex determinant example
categories:
    - Imaginary and complex numbers
thumbnail_urls:
    - https://i3.ytimg.com/vi/E7OkUomRq1Q/default.jpg
    - https://i3.ytimg.com/vi/E7OkUomRq1Q/1.jpg
    - https://i3.ytimg.com/vi/E7OkUomRq1Q/2.jpg
    - https://i3.ytimg.com/vi/E7OkUomRq1Q/3.jpg
---
