---
version: 1
type: video
provider: YouTube
id: iWaX97p6y9U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cf5cbbf0-2fc4-4891-978b-0c6913b383d7
updated: 1486069678
title: 'The Sex Lives of Nonvascular Plants- Alternation of Generations - Crash Course Biology #36'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/iWaX97p6y9U/default.jpg
    - https://i3.ytimg.com/vi/iWaX97p6y9U/1.jpg
    - https://i3.ytimg.com/vi/iWaX97p6y9U/2.jpg
    - https://i3.ytimg.com/vi/iWaX97p6y9U/3.jpg
---
