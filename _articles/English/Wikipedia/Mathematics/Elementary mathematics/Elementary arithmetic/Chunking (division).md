---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chunking_(division)
offline_file: ""
offline_thumbnail: ""
uuid: f80e4758-88a1-4c14-82c7-233e5a67493e
updated: 1484308912
title: Chunking (division)
categories:
    - Elementary arithmetic
---
In mathematics education at primary school level, chunking (sometimes also called the partial quotients method) is an elementary approach for solving simple division questions, by repeated subtraction. It is also known as the hangman method with the addition of a line separating the divisor, dividend, and partial quotients.[1] It has a counterpart in the grid method for multiplication.
