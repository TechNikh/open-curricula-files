---
version: 1
type: video
provider: YouTube
id: xmgk8_l3lig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Rates-of-change%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fffaab40-1246-45d3-8a96-e3307fa69a4e
updated: 1486069694
title: Rates-of-change (part 2)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/xmgk8_l3lig/default.jpg
    - https://i3.ytimg.com/vi/xmgk8_l3lig/1.jpg
    - https://i3.ytimg.com/vi/xmgk8_l3lig/2.jpg
    - https://i3.ytimg.com/vi/xmgk8_l3lig/3.jpg
---
