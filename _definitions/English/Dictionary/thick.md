---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thick
offline_file: ""
offline_thumbnail: ""
uuid: 7c7a8d27-a757-46da-97e9-e91cea0bb126
updated: 1484310258
title: thick
categories:
    - Dictionary
---
thick
     adj 1: not thin; of a specific thickness or of relatively great
            extent from one surface to the opposite usually in the
            smallest of the three solid dimensions; "an inch
            thick"; "a thick board"; "a thick sandwich"; "spread a
            thick layer of butter"; "thick coating of dust";
            "thick warm blankets" [ant: {thin}]
     2: closely crowded together; "a compact shopping center"; "a
        dense population"; "thick crowds" [syn: {compact}, {dense}]
     3: relatively dense in consistency; "thick cream"; "thick
        soup"; "thick smoke"; "thick fog" [ant: {thin}]
     4: spoken as if with a thick tongue; "the thick speech of a
   ...
