---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemical_graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: fb7d5bd1-a6fe-4ac6-a230-6a1d1c400332
updated: 1484309429
title: Chemical graph theory
categories:
    - Mathematical chemistry
---
Chemical graph theory is the topology branch of mathematical chemistry which applies graph theory to mathematical modelling of chemical phenomena.[1] The pioneers of the chemical graph theory are Alexandru Balaban, Ante Graovac, Ivan Gutman, Haruo Hosoya, Milan Randić and Nenad Trinajstić[2] (also Harry Wiener and others). In 1988, it was reported that several hundred researchers worked in this area producing about 500 articles annually. A number of monographs have been written in the area, including the two-volume comprehensive text by Trinajstic, Chemical Graph Theory, that summarized the field up to mid-1980s.[3]
