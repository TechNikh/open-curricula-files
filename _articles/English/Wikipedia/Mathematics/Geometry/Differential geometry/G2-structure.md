---
version: 1
type: article
id: https://en.wikipedia.org/wiki/G2-structure
offline_file: ""
offline_thumbnail: ""
uuid: 1d673fce-5b37-4cfc-abf5-9de946287cd3
updated: 1484309219
title: G2-structure
tags:
    - Equivalent conditions
    - History
    - Remarks
categories:
    - Differential geometry
---
In differential geometry, a 
  
    
      
        
          G
          
            2
          
        
      
    
    {\displaystyle G_{2}}
  
-structure is an important type of G-structure that can be defined on a smooth manifold. If M is a smooth manifold of dimension seven, then a G2-structure is a reduction of structure group of the frame bundle of M to the compact, exceptional Lie group G2.
