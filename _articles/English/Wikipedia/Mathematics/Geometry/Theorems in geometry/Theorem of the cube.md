---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theorem_of_the_cube
offline_file: ""
offline_thumbnail: ""
uuid: 4ac52eea-5f75-40ec-9489-f10f12ceea0a
updated: 1484309126
title: Theorem of the cube
categories:
    - Theorems in geometry
---
In mathematics, the theorem of the cube is a condition for a line bundle over a product of three complete varieties to be trivial. It was a principle discovered, in the context of linear equivalence, by the Italian school of algebraic geometry. The final version of the theorem of the cube was first published by Lang (1959), who credited it to André Weil. A discussion of the history has been given by Kleiman (2005). A treatment by means of sheaf cohomology, and description in terms of the Picard functor, was given by Mumford (2008).
