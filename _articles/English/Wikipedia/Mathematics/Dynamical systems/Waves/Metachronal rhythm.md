---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metachronal_rhythm
offline_file: ""
offline_thumbnail: ""
uuid: 83c74660-3e0d-4a1d-805a-f43ad376e99c
updated: 1484308889
title: Metachronal rhythm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Metachronal.svg.png
categories:
    - Waves
---
A metachronal rhythm or metachronal wave refers to wavy movements produced by the sequential action (as opposed to synchronized) of structures such as cilia, segments of worms or legs. These movements produce the appearance of a travelling wave.
