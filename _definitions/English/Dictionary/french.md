---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/french
offline_file: ""
offline_thumbnail: ""
uuid: 371bae23-1c80-489f-8d7e-2850a7aa900a
updated: 1484310221
title: french
categories:
    - Dictionary
---
French
     adj : of or pertaining to France or the people of France; "French
           cooking"; "a gallic shrug" [syn: {Gallic}]
     n 1: the Romance language spoken in France and in countries
          colonized by France
     2: the people of France [syn: {French people}]
     3: United States sculptor who created the seated marble figure
        of Abraham Lincoln in the Lincoln Memorial in Washington
        D.C. (1850-1931) [syn: {Daniel Chester French}]
