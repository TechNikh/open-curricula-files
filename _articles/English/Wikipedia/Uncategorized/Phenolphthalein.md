---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phenolphthalein
offline_file: ""
offline_thumbnail: ""
uuid: 0e1655ba-647c-44fe-90ff-53fd48452445
updated: 1484308312
title: Phenolphthalein
tags:
    - Uses
    - Indicator
    - Medical uses
    - Synthesis
categories:
    - Uncategorized
---
Phenolphthalein is a chemical compound with the formula C20H14O4 and is often written as "HIn" or "phph" in shorthand notation. Phenolphthalein is often used as an indicator in acid–base titrations. For this application, it turns colorless in acidic solutions and pink in basic solutions.
