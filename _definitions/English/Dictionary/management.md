---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/management
offline_file: ""
offline_thumbnail: ""
uuid: 2c3390b1-3316-448f-86de-fb69ef241d08
updated: 1484310260
title: management
categories:
    - Dictionary
---
management
     n 1: the act of managing something; "he was given overall
          management of the program"; "is the direction of the
          economy a function of government?" [syn: {direction}]
     2: those in charge of running a business
