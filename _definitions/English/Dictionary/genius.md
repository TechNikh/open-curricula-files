---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genius
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608501
title: genius
categories:
    - Dictionary
---
genius
     n 1: someone who has exceptional intellectual ability and
          originality; "Mozart was a child genius"; "he's smart
          but he's no Einstein" [syn: {mastermind}, {brain}, {Einstein}]
     2: unusual mental ability [syn: {brilliance}]
     3: someone who is dazzlingly skilled in any field [syn: {ace},
        {adept}, {champion}, {sensation}, {maven}, {mavin}, {virtuoso},
         {hotshot}, {star}, {superstar}, {whiz}, {whizz}, {wizard},
         {wiz}]
     4: exceptional creative ability [syn: {wizardry}]
     5: a natural talent; "he has a flair for mathematics"; "he has
        a genius for interior decorating" [syn: {flair}]
     [also: {genii} (pl)]
