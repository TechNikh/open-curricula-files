---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nernst_effect
offline_file: ""
offline_thumbnail: ""
uuid: 95a2b935-b8c8-47c4-87dc-299b3fc87b58
updated: 1484308760
title: Nernst effect
tags:
    - Physical picture
    - Sample types
    - Journal articles
categories:
    - Electrodynamics
---
In physics and chemistry, the Nernst Effect (also termed first Nernst–Ettingshausen effect, after Walther Nernst and Albert von Ettingshausen) is a thermoelectric (or thermomagnetic) phenomenon observed when a sample allowing electrical conduction is subjected to a magnetic field and a temperature gradient normal (perpendicular) to each other. An electric field will be induced normal to both.
