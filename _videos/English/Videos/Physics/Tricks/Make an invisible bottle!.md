---
version: 1
type: video
provider: YouTube
id: anJuZdL_vsw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 11eb9f62-5d53-4e41-ab27-75881dceba2f
updated: 1486069660
title: Make an invisible bottle!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/anJuZdL_vsw/default.jpg
    - https://i3.ytimg.com/vi/anJuZdL_vsw/1.jpg
    - https://i3.ytimg.com/vi/anJuZdL_vsw/2.jpg
    - https://i3.ytimg.com/vi/anJuZdL_vsw/3.jpg
---
