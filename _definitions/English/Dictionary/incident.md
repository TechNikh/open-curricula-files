---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incident
offline_file: ""
offline_thumbnail: ""
uuid: 0b62ff1d-e3f9-4344-aa00-fa539a2a80e1
updated: 1484310240
title: incident
categories:
    - Dictionary
---
incident
     adj 1: falling or striking on something
     2: (sometimes followed by `to') minor or casual or subordinate
        in significance or nature or occurring as a chance
        concomitant or consequence; "incidental expenses"; "the
        road will bring other incidental advantages"; "extra
        duties incidental to the job"; "labor problems incidental
        to a rapid expansion"; "confusion incidental to a quick
        change" [syn: {incidental}] [ant: {basic}]
     n 1: a single distinct event
     2: a public disturbance; "the police investigated an incident
        at the bus station"
