---
version: 1
type: video
provider: YouTube
id: iA7Thhnzc64
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Force%20of%20friction%20keeping%20velocity%20constant.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2423bdb3-ecd0-4480-938f-4a0a3c889099
updated: 1486069595
title: Force of friction keeping velocity constant
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/iA7Thhnzc64/default.jpg
    - https://i3.ytimg.com/vi/iA7Thhnzc64/1.jpg
    - https://i3.ytimg.com/vi/iA7Thhnzc64/2.jpg
    - https://i3.ytimg.com/vi/iA7Thhnzc64/3.jpg
---
Calculating the coefficient of kinetic friction (correction made in next video). Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/inclined-planes-friction/v/intuition-on-static-and-kinetic-friction-comparisons?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/inclined-planes-friction/v/correction-of-force-of-friction-keeping-the-block-stationary?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
