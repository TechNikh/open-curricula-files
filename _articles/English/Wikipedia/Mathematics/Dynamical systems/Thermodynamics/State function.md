---
version: 1
type: article
id: https://en.wikipedia.org/wiki/State_function
offline_file: ""
offline_thumbnail: ""
uuid: a1aa300f-9710-41ea-8394-b2c6c8200bac
updated: 1484308868
title: State function
tags:
    - History
    - Overview
    - List of state functions
    - Notes
categories:
    - Thermodynamics
---
In thermodynamics, a state function or function of state is a function defined for a system relating several state variables or state quantities that depends only on the current equilibrium state of the system.[1] State functions do not depend on the path by which the system arrived at its present state. A state function describes the equilibrium state of a system.
