---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Signaling_game
offline_file: ""
offline_thumbnail: ""
uuid: caa7f39a-4d78-4879-8850-1edb577660ff
updated: 1484309038
title: Signaling game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Signaling_Game.svg.png
tags:
    - Perfect Bayesian equilibrium
    - Examples
    - Reputation game
    - Education game
    - Beer-Quiche game
    - Applications of signaling games
    - Philosophy
    - Economics
    - Biology
    - Costly versus cost-free signaling
categories:
    - Game theory game classes
---
It is a game with two players, called the sender (S) and the receiver (R):
