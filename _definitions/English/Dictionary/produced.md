---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/produced
offline_file: ""
offline_thumbnail: ""
uuid: 9dd87592-4a44-4b2e-b3f8-af69fa99a0d7
updated: 1484310327
title: produced
categories:
    - Dictionary
---
produced
     adj : that is caused by; "if...such a change is produced
           by...insulin comas or electroshocks"; "the emotional
           states produced by this drug"
