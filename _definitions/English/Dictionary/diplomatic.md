---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diplomatic
offline_file: ""
offline_thumbnail: ""
uuid: cebf230c-5a98-439a-b6b6-1ba1363e4f23
updated: 1484310551
title: diplomatic
categories:
    - Dictionary
---
diplomatic
     adj 1: relating to or characteristic of diplomacy; "diplomatic
            immunity"
     2: skilled in dealing with sensitive matters or people [syn: {diplomatical}]
        [ant: {undiplomatic}]
     3: able to take a broad view of negotiations between states
        [syn: {wise}]
