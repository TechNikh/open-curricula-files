---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prismane
offline_file: ""
offline_thumbnail: ""
uuid: 05758c51-3172-4624-8889-04e495a21524
updated: 1484309282
title: Prismane
tags:
    - History
    - Properties
    - Synthesis
categories:
    - Molecular geometry
---
Prismane is a polycyclic hydrocarbon with the formula C6H6. It is an isomer of benzene, specifically a valence isomer. Prismane is far less stable than benzene. The carbon (and hydrogen) atoms of the prismane molecule are arranged in the shape of a six-atom triangular prism—this compound is the parent and simplest member of the prismanes class of molecules. Albert Ladenburg proposed this structure for the compound now known as benzene.[2] The compound was not synthesized until 1973.[3]
