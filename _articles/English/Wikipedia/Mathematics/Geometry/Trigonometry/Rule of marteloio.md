---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rule_of_marteloio
offline_file: ""
offline_thumbnail: ""
uuid: e165a854-92f6-4f54-963d-1cd377540fb5
updated: 1484309345
title: Rule of marteloio
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Tondo_e_quadro_%2528Bianco%252C_1436%2529.jpg'
tags:
    - Etymology
    - Purpose
    - The traverse problem
    - Rules
    - |
        Ramon Llull's "miliaria"
    - |
        Andrea Bianco's "toleta"
    - Rule of three
    - Circle and square
    - Other applications
    - Triangulation
    - Finding locations
    - Relation to later rules
    - Relation to the "regiment of the leagues"
    - Relation to "traverse sailing"
    - Manuscript sources
    - Notes
    - External resources
categories:
    - Trigonometry
---
The rule of marteloio is a medieval technique of navigational computation that uses compass direction, distance and a simple trigonometric table known as the toleta de marteloio. The rule told mariners how to plot the traverse between two different navigation courses by means of resolving triangles with the help of the Toleta and basic arithmetic.
