---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Shannon%27s_source_coding_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 083bc4b0-8abb-4cb2-9769-3c0c0de73948
updated: 1484308553
title: "Shannon's source coding theorem"
tags:
    - Statements
    - Source coding theorem
    - Source coding theorem for symbol codes
    - 'Proof: Source coding theorem'
    - 'Proof: Source coding theorem for symbol codes'
    - Extension to non-stationary independent sources
    - >
        Fixed Rate lossless source coding for discrete time
        non-stationary independent sources
categories:
    - Coding theory
---
In information theory, Shannon's source coding theorem (or noiseless coding theorem) establishes the limits to possible data compression, and the operational meaning of the Shannon entropy.
