---
version: 1
type: video
provider: YouTube
id: axB6uhEx628
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Earth%20geological%20and%20climatic%20history/Pangaea.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 000b485f-4f53-49e1-9ce0-edc83aee46f3
updated: 1486069685
title: Pangaea
categories:
    - Earth geological and climatic history
thumbnail_urls:
    - https://i3.ytimg.com/vi/axB6uhEx628/default.jpg
    - https://i3.ytimg.com/vi/axB6uhEx628/1.jpg
    - https://i3.ytimg.com/vi/axB6uhEx628/2.jpg
    - https://i3.ytimg.com/vi/axB6uhEx628/3.jpg
---
