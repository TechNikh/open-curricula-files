---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unripe
offline_file: ""
offline_thumbnail: ""
uuid: 0c178e4f-45a7-4394-8cab-ef38f2525fe0
updated: 1484310307
title: unripe
categories:
    - Dictionary
---
unripe
     adj 1: not fully developed or mature; not ripe; "unripe fruit";
            "fried green tomatoes"; "green wood" [syn: {green}, {unripened},
             {immature}] [ant: {ripe}]
     2: not fully prepared
