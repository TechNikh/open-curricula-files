---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_(audience)
offline_file: ""
offline_thumbnail: ""
uuid: e04f6a6e-55f7-40b4-84b7-57b33843058f
updated: 1484308903
title: Wave (audience)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Confed-Cup_2005_-_Laolawelle.JPG
tags:
    - Origins and variations
    - 1970s–1980s
    - University of Washington
    - University of Michigan
    - Monterrey, Mexico
    - Global broadcasts
    - Current appearances
    - Metrics
    - Size records
    - In animals
categories:
    - Waves
---
The wave (known as the Mexican wave in the anglosphere outside North America) is an example of metachronal rhythm achieved in a packed stadium when successive groups of spectators briefly stand, yell, and raise their arms. Immediately upon stretching to full height, the spectator returns to the usual seated position.
