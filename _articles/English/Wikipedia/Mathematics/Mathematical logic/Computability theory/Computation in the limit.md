---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computation_in_the_limit
offline_file: ""
offline_thumbnail: ""
uuid: 010e6f3e-6dc3-49e5-9eef-c3a9c54b1694
updated: 1484309485
title: Computation in the limit
tags:
    - Formal definition
    - Limit lemma
    - Proof
    - Limit computable real numbers
    - Examples
categories:
    - Computability theory
---
In computability theory, a function is called limit computable if it is the limit of a uniformly computable sequence of functions. The terms computable in the limit and limit recursive are also used. One can think of limit computable functions as those admitting an eventually correct computable guessing procedure at their true value. A set is limit computable just when its characteristic function is limit computable.
