---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fermi%E2%80%93Pasta%E2%80%93Ulam_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 011b997f-3f2a-4a1c-aadf-9a7a05fed11f
updated: 1484308773
title: Fermi–Pasta–Ulam problem
tags:
    - The FPU experiment
    - The FPU lattice system
    - Connection to the KdV equation
    - Notes
categories:
    - Ergodic theory
---
In physics, the Fermi–Pasta–Ulam problem or FPU problem was the apparent paradox in chaos theory that many complicated enough physical systems exhibited almost exactly periodic behavior – called Fermi–Pasta–Ulam recurrence – instead of ergodic behavior. One of the resolutions of the paradox includes the insight that many non-linear equations are exactly integrable. Another may be that ergodic behavior may depend on the initial energy of the system.
