---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/measured
offline_file: ""
offline_thumbnail: ""
uuid: 615978cc-55bf-4d4c-bc58-170646bdf579
updated: 1484310313
title: measured
categories:
    - Dictionary
---
measured
     adj 1: determined by measurement; "the measured distance was less
            than a mile" [ant: {unmeasured}]
     2: the rhythmic arrangement of syllables [syn: {metrical}, {metric}]
     3: carefully thought out in advance; "a calculated insult";
        "with measured irony" [syn: {calculated}, {deliberate}]
     4: with care and dignity; "walking at the same measured pace";
        "with all deliberate speed" [syn: {careful}, {deliberate}]
