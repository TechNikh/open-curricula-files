---
version: 1
type: video
provider: YouTube
id: GkB4vW16QHI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Gradients%20and%20Partial%20Derivatives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 98104c7b-e495-4739-9781-d6eeba3ad06e
updated: 1486069656
title: Gradients and Partial Derivatives
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/GkB4vW16QHI/default.jpg
    - https://i3.ytimg.com/vi/GkB4vW16QHI/1.jpg
    - https://i3.ytimg.com/vi/GkB4vW16QHI/2.jpg
    - https://i3.ytimg.com/vi/GkB4vW16QHI/3.jpg
---
