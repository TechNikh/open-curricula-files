---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Congestion_game
offline_file: ""
offline_thumbnail: ""
uuid: 85ff1bf7-2f25-4633-8ea0-6bc4bc800d62
updated: 1484309035
title: Congestion game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Congestion-diagram.svg.png
tags:
    - Motivation
    - Definition
    - Example
    - Existence of Nash equilibria
    - Continuous congestion games
    - Existence of equilibria in the continuous case
    - Quality of solutions and Price of anarchy
categories:
    - Game theory game classes
---
Congestion games are a class of games in game theory first proposed by Rosenthal in 1973. In a Congestion game we define players and resources, where the payoff of each player depends on the resources it chooses and the number of players choosing the same resource. Congestion games are a special case of potential games. Rosenthal proved that any congestion game is a potential game and Monderer and Shapley (1996) proved the converse: for any potential game, there is a congestion game with the same potential function.
