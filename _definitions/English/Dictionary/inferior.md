---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inferior
offline_file: ""
offline_thumbnail: ""
uuid: 66e8f7fc-6e73-4764-8d18-ac41d3f0f1c6
updated: 1484310533
title: inferior
categories:
    - Dictionary
---
inferior
     adj 1: of or characteristic of low rank or importance [ant: {superior}]
     2: of low or inferior quality [ant: {superior}]
     3: inferior in rank or status; "the junior faculty"; "a lowly
        corporal"; "petty officialdom"; "a subordinate
        functionary" [syn: {junior-grade}, {lower}, {lower-ranking},
         {lowly}, {petty(a)}, {secondary}, {subaltern}, {subordinate}]
     4: written or printed below and to one side of another
        character [syn: {subscript}] [ant: {adscript}, {superscript}]
     5: having an orbit between the sun and the Earth's orbit;
        "Mercury and Venus are inferior planets" [ant: {superior}]
     6: lower than a given reference ...
