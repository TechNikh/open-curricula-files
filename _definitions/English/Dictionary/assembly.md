---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assembly
offline_file: ""
offline_thumbnail: ""
uuid: 85c5a3d9-ffa5-4c56-9b46-a04706375626
updated: 1484310583
title: assembly
categories:
    - Dictionary
---
assembly
     n 1: a group of machine parts that fit together to form a
          self-contained unit
     2: the act of constructing something (as a piece of machinery)
        [syn: {fabrication}] [ant: {dismantling}]
     3: a public facility to meet for open discussion [syn: {forum},
         {meeting place}]
     4: a group of persons gathered together for a common purpose
     5: the social act of assembling; "they demanded the right of
        assembly" [syn: {assemblage}, {gathering}] [ant: {dismantling}]
