---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interest
offline_file: ""
offline_thumbnail: ""
uuid: a9d7320d-9447-424d-8d63-7ddcfc9a8fd1
updated: 1484310339
title: interest
categories:
    - Dictionary
---
interest
     n 1: a sense of concern with and curiosity about someone or
          something; "an interest in music" [syn: {involvement}]
     2: the power of attracting or holding one's interest (because
        it is unusual or exciting etc.); "they said nothing of
        great interest"; "primary colors can add interest to a
        room" [syn: {interestingness}] [ant: {uninterestingness}]
     3: a reason for wanting something done; "for your sake"; "died
        for the sake of his country"; "in the interest of safety";
        "in the common interest" [syn: {sake}]
     4: a fixed charge for borrowing money; usually a percentage of
        the amount borrowed; "how much interest do ...
