---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Doubling_space
offline_file: ""
offline_thumbnail: ""
uuid: b8340511-0db0-4dbc-bc9a-cb7d5819de9b
updated: 1484309265
title: Doubling space
tags:
    - "Assouad's embedding theorem"
    - Doubling Measures
    - Definition
    - Examples
    - Applications
categories:
    - Metric geometry
---
In mathematics, a metric space X with metric d is said to be doubling if there is some constant M > 0 such that for any x in X and r > 0, it is possible to cover the ball B(x, r) = {y|d(x, y) < r} with the union of at most M many balls of radius r/2.[1] The base-2 logarithm of M is often referred to as the doubling dimension of X. Euclidean spaces ℝd equipped with the usual Euclidean metric are examples of doubling spaces where the doubling constant M depends on the dimension d.
