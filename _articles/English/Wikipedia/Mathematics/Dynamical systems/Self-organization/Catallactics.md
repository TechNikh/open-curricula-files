---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catallactics
offline_file: ""
offline_thumbnail: ""
uuid: 28a09f74-9950-499a-8734-5ff4aed1b630
updated: 1484308816
title: Catallactics
categories:
    - Self-organization
---
Catallactics is a theory of the way the free market system reaches exchange ratios and prices. It aims to analyse all actions based on monetary calculation and trace the formation of prices back to the point where an agent makes his or her choices. It explains prices as they are, rather than as they "should" be. The laws of catallactics are not value judgments, but aim to be exact, objective and of universal validity. It was first used extensively by the Austrian School economist Ludwig von Mises.[citation needed]
