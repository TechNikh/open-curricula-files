---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dimorphism
offline_file: ""
offline_thumbnail: ""
uuid: 41f439e4-7e92-402b-ba56-c14cc26637c5
updated: 1484310163
title: dimorphism
categories:
    - Dictionary
---
dimorphism
     n 1: (chemistry) the property of certain substances that enables
          them to exist in two distinct crystalline forms
     2: (biology) the existence of two forms of individual within
        the same animal species (independent of sex differences)
