---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repair
offline_file: ""
offline_thumbnail: ""
uuid: 9b41de99-5ea3-47dd-9292-1d88bb9e559d
updated: 1484310455
title: repair
categories:
    - Dictionary
---
repair
     n 1: the act of putting something in working order again [syn: {fix},
           {fixing}, {fixture}, {mend}, {mending}, {reparation}]
     2: a formal way of referring to the condition of something;
        "the building was in good repair"
     3: a frequently visited place [syn: {haunt}, {hangout}, {resort},
         {stamping ground}]
     v 1: restore by replacing a part or putting together what is torn
          or broken; "She repaired her TV set"; "Repair my shoes
          please" [syn: {mend}, {fix}, {bushel}, {doctor}, {furbish
          up}, {restore}, {touch on}] [ant: {break}]
     2: make amends for; pay compensation for; "One can never fully
        repair the ...
