---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatigue
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484475061
title: fatigue
categories:
    - Dictionary
---
fatigue
     n 1: temporary loss of strength and energy resulting from hard
          physical or mental work; "he was hospitalized for
          extreme fatigue"; "growing fatigue was apparent from the
          decline in the execution of their athletic skills";
          "weariness overcame her after twelve hours and she fell
          asleep" [syn: {weariness}, {tiredness}]
     2: used of materials (especially metals) in a weakened state
        caused by long stress; "metal fatigue"
     3: (always used with a modifier) boredom resulting from
        overexposure to something; "he was suffering from museum
        fatigue"; "after watching TV with her husband she had a
        bad ...
