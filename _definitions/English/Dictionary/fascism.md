---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fascism
offline_file: ""
offline_thumbnail: ""
uuid: 51de1321-f00d-4aa9-ad02-e7726ebce8b7
updated: 1484310553
title: fascism
categories:
    - Dictionary
---
fascism
     n : a political theory advocating an authoritarian hierarchical
         government (as opposed to democracy or liberalism)
