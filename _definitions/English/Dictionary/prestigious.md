---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prestigious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484552581
title: prestigious
categories:
    - Dictionary
---
prestigious
     adj 1: having an illustrious reputation; respected; "our esteemed
            leader"; "a prestigious author" [syn: {esteemed}, {honored}]
     2: exerting influence by reason of high status or prestige; "a
        prestigious professor at a prestigious university"
