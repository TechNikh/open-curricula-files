---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Solving_quadratic_equations_with_continued_fractions
offline_file: ""
offline_thumbnail: ""
uuid: cfe08b9a-04df-4f91-843b-7cbff56aa588
updated: 1484308915
title: Solving quadratic equations with continued fractions
tags:
    - A simple example
    - An algebraic explanation
    - The general quadratic equation
    - A general theorem
    - Complex coefficients
categories:
    - Elementary algebra
---
where a ≠ 0.
