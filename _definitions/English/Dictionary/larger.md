---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/larger
offline_file: ""
offline_thumbnail: ""
uuid: 409a8985-2980-4da1-8d56-d917b97fe2e0
updated: 1484310271
title: larger
categories:
    - Dictionary
---
larger
     adj : large or big relative to something else [syn: {bigger}]
