---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Thermodynamic_databases_for_pure_substances
offline_file: ""
offline_thumbnail: ""
uuid: bfa5adb0-49fd-4d1f-964c-b57087ca0460
updated: 1484308872
title: Thermodynamic databases for pure substances
tags:
    - Thermodynamic data
    - Enthalpy, heat content and heat capacity
    - Enthalpy change of phase transitions
    - Enthalpy change for a chemical reaction
    - Entropy and Gibbs energy
    - Additional functions
    - Thermodynamic databases
    - Thermodynamic datafiles
categories:
    - Thermodynamics
---
Thermodynamic databases contain information about thermodynamic properties for substances, the most important being enthalpy, entropy, and Gibbs free energy. Numerical values of these thermodynamic properties are collected as tables or are calculated from thermodynamic datafiles. Data is expressed as temperature-dependent values for one mole of substance at the standard pressure of 101.325 kPa (1 atm), or 100 kPa (1 bar). Unfortunately, both of these definitions for the standard condition for pressure are in use.
