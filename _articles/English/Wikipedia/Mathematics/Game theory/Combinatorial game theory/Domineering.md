---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Domineering
offline_file: ""
offline_thumbnail: ""
uuid: 0ef3c46d-6bc7-45ac-a1b9-85bbbd669e53
updated: 1484308542
title: Domineering
tags:
    - Basic examples
    - Single box
    - Horizontal rows
    - Vertical rows
    - More complex grids
    - High-level play
    - Winning strategy
    - Cram
categories:
    - Combinatorial game theory
---
Domineering (also called Stop-Gate or Crosscram) is a mathematical game played on a sheet of graph paper, with any set of designs traced out. For example, it can be played on a 6×6 square, a checkerboard, an entirely irregular polygon, or any combination thereof. Two players have a collection of dominoes which they place on the grid in turn, covering up squares. One player, Left, plays tiles vertically, while the other, Right, plays horizontally. As in most games in combinatorial game theory, the first player who cannot move loses.
