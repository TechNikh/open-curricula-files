---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mollweide%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 5c7c7deb-ad20-4fb3-8bb4-9caf74c49065
updated: 1484309345
title: "Mollweide's formula"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/198px-Triangle_with_notations_2.svg.png
categories:
    - Trigonometry
---
In trigonometry, Mollweide's formula, sometimes referred to in older texts as Mollweide's equations,[1] named after Karl Mollweide, is a set of two relationships between sides and angles in a triangle.[2] It can be used to check solutions of triangles.[3]
