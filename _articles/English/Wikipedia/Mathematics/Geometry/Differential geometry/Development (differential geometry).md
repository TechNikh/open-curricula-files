---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Development_(differential_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: fb518a06-480a-4837-b57c-db76865e8f2b
updated: 1484309216
title: Development (differential geometry)
tags:
    - Properties
    - Flat connections
    - Undevelopable surfaces
categories:
    - Differential geometry
---
In classical differential geometry, development refers to the simple idea of rolling one smooth surface over another in Euclidean space. For example, the tangent plane to a surface (such as the sphere or the cylinder) at a point can be rolled around the surface to obtain the tangent plane at other points.
