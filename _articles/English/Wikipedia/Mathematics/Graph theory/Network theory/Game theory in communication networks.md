---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Game_theory_in_communication_networks
offline_file: ""
offline_thumbnail: ""
uuid: 2da00d9e-515c-4b45-99b5-2fc726289df2
updated: 1484309435
title: Game theory in communication networks
tags:
    - >
        Applications of non-cooperative game theory in wireless
        networks research
    - Medium access games for 802.11 WLAN
    - Power control games in CDMA systems
    - >
        Applications of cooperative game theory (coalitions) in
        wireless networks research
    - Coalitional game theory in wireless networks
categories:
    - Network theory
---
Game theory has recently become a useful tool for modeling and studying interactions between cognitive radios envisioned to operate in future communications systems. Such terminals will have the capability to adapt to the context they operate in, through possibly power and rate control as well as channel selection. Software agents embedded in these terminals will potentially be selfish, meaning they will only try to maximize the throughput/connectivity of the terminal they function for, as opposed to maximizing the welfare (total capacity) of the system they operate in. Thus, the potential interactions among them can be modeled through non-cooperative games. The researchers in this field ...
