---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/view
offline_file: ""
offline_thumbnail: ""
uuid: 8575f116-a7c1-4a8e-bbf8-23f9c81f6bf1
updated: 1484310315
title: view
categories:
    - Dictionary
---
view
     n 1: a way of regarding situations or topics etc.; "consider what
          follows from the positivist view" [syn: {position}, {perspective}]
     2: the visual percept of a region; "the most desirable feature
        of the park are the beautiful views" [syn: {aspect}, {prospect},
         {scene}, {vista}, {panorama}]
     3: the act of looking or seeing or observing; "he tried to get
        a better view of it"; "his survey of the battlefield was
        limited" [syn: {survey}, {sight}]
     4: the range of the eye; "they were soon out of view" [syn: {eyeshot}]
     5: a personal belief or judgment that is not founded on proof
        or certainty; "my opinion differs from ...
