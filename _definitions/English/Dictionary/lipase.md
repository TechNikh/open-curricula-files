---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lipase
offline_file: ""
offline_thumbnail: ""
uuid: 8e9ac7ea-84aa-4f2b-9cbc-6a5ecdd92a3c
updated: 1484310316
title: lipase
categories:
    - Dictionary
---
lipase
     n : an enzyme secreted in the digestive tract that catalyzes the
         breakdown of fats into individual fatty acids that can be
         absorbed into the bloodstream
