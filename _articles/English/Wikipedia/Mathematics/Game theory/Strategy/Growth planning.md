---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Growth_planning
offline_file: ""
offline_thumbnail: ""
uuid: de221443-460d-4092-bb03-30ca25705110
updated: 1484309065
title: Growth planning
tags:
    - Why Businesses Develop a Growth Plan
    - What is in a Growth Plan
    - Preparing for Growth
    - Business Plan vs Growth Plan
categories:
    - Strategy
---
Growth planning, is a strategic business activity that enables business owners to plan and track organic growth in their revenue. It allows businesses to allocate their limited resources toward a centered effort to adapt to changes in the industry driven by digital disruption and differentiate from competitors. The strategies and tactics included in a Growth Plan focuses on the key driver of revenue generation - the customer.[1]
