---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_surface
offline_file: ""
offline_thumbnail: ""
uuid: 9f303d3d-0a9e-49e1-9fc0-82e19d5b771d
updated: 1484308907
title: Wave surface
categories:
    - Waves
---
In mathematics, Fresnel's wave surface, found by Augustin-Jean Fresnel in 1821, is a quartic surface describing the propagation of light in an optically biaxial crystal. Wave surfaces are special cases of tetrahedroids which are in turn special cases of Kummer surfaces.
