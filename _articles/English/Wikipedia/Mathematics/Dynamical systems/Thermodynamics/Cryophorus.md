---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cryophorus
offline_file: ""
offline_thumbnail: ""
uuid: 7ba2b95c-2233-4f9d-84e7-e00ad7bad12f
updated: 1484308837
title: Cryophorus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Cryophorus.svg.png
categories:
    - Thermodynamics
---
A cryophorus is a glass container containing liquid water and water vapor. It is used in physics courses to demonstrate rapid freezing by evaporation. A typical cryophorus has a bulb at one end connected to a tube of the same material. When the liquid water is manipulated into the bulbed end and the other end is submerged into a freezing mixture (such as liquid nitrogen), the gas pressure drops as it is cooled. The liquid water begins to evaporate, producing more water vapor. Evaporation causes the water to cool rapidly to its freezing point and it solidifies suddenly.
