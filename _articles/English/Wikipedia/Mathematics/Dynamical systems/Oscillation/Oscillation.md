---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oscillation
offline_file: ""
offline_thumbnail: ""
uuid: 9994569f-d8f9-413a-9366-70024b42c846
updated: 1484308816
title: Oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Animated-mass-spring.gif
tags:
    - Simple harmonic oscillator
    - Damped and driven oscillations
    - Coupled oscillations
    - Continuous systems – waves
    - Mathematics
    - Examples
    - Mechanical
    - Electrical
    - Electro-mechanical
    - Optical
    - Biological
    - human
    - Economic and social
    - Climate and geophysics
    - Astrophysics
    - Quantum mechanical
    - Chemical
categories:
    - Oscillation
---
Oscillation is the repetitive variation, typically in time, of some measure about a central value (often a point of equilibrium) or between two or more different states. The term vibration is precisely used to describe mechanical oscillation. Familiar examples of oscillation include a swinging pendulum and alternating current power.
