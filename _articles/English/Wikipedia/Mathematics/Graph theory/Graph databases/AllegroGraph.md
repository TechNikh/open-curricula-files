---
version: 1
type: article
id: https://en.wikipedia.org/wiki/AllegroGraph
offline_file: ""
offline_thumbnail: ""
uuid: e8308ed5-dcbd-45fe-ad74-058be3b58537
updated: 1484309404
title: AllegroGraph
tags:
    - Implementation
    - Languages
categories:
    - Graph databases
---
AllegroGraph is a closed source triplestore which is designed to store RDF triples, a standard format for Linked Data.[2] AllegroGraph is currently in use in Open source projects,[3] commercial projects[4][5][6][7] and Department of Defense projects.[8] It is also the storage component for the TwitLogic project[9] that is bringing the Semantic Web to Twitter data.[10]
