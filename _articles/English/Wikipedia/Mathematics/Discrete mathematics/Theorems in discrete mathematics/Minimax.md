---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimax
offline_file: ""
offline_thumbnail: ""
uuid: af9a7fdf-41ff-4cae-b71a-ba846643f48b
updated: 1484308657
title: Minimax
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Plminmax.gif
tags:
    - Game theory
    - In general games
    - In zero-sum games
    - Example
    - Maximin
    - In repeated games
    - Combinatorial game theory
    - Minimax algorithm with alternate moves
    - Pseudocode
    - Example
    - Minimax for individual decisions
    - Minimax in the face of uncertainty
    - Minimax criterion in statistical decision theory
    - Non-probabilistic decision theory
    - Maximin in philosophy
    - Notes
categories:
    - Theorems in discrete mathematics
---
Minimax (sometimes MinMax or MM[1]) is a decision rule used in decision theory, game theory, statistics and philosophy for minimizing the possible loss for a worst case (maximum loss) scenario. Originally formulated for two-player zero-sum game theory, covering both the cases where players take alternate moves and those where they make simultaneous moves, it has also been extended to more complex games and to general decision-making in the presence of uncertainty.
