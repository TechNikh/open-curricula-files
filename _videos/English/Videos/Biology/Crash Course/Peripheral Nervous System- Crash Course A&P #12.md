---
version: 1
type: video
provider: YouTube
id: QY9NTVh-Awo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Peripheral%20Nervous%20System-%20Crash%20Course%20A%26P%20%2312.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2a483dd1-2964-4d46-ade3-fe4cd00821ee
updated: 1486069681
title: 'Peripheral Nervous System- Crash Course A&P #12'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/QY9NTVh-Awo/default.jpg
    - https://i3.ytimg.com/vi/QY9NTVh-Awo/1.jpg
    - https://i3.ytimg.com/vi/QY9NTVh-Awo/2.jpg
    - https://i3.ytimg.com/vi/QY9NTVh-Awo/3.jpg
---
