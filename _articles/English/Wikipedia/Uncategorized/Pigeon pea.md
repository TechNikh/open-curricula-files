---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tuvar
offline_file: ""
offline_thumbnail: ""
uuid: 8d53554a-9735-46b8-bdc2-48f996a3ce3c
updated: 1484308466
title: Pigeon pea
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cajanus_cajan_0.jpg
tags:
    - Common names
    - In the languages of India
    - In other countries
    - Origins
    - Cultivation
    - Uses
    - Genome sequence
    - Nutrition
    - Pathogens
categories:
    - Uncategorized
---
The pigeon pea (Cajanus cajan) is a perennial legume from the family Fabaceae. Since its domestication in India at least 3,500 years ago, its seeds have become a common food grain in Asia, Africa, and Latin America. It is consumed on a large scale mainly in south Asia and is a major source of protein for the population of that subcontinent.
