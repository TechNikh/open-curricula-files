---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/effervescence
offline_file: ""
offline_thumbnail: ""
uuid: d6a89aa7-87a8-4a6a-a5c3-73e3ea737460
updated: 1484310381
title: effervescence
categories:
    - Dictionary
---
effervescence
     n 1: the process of bubbling as gas escapes
     2: the property of giving off bubbles [syn: {bubbliness}, {frothiness}]
