---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GV-linear-code
offline_file: ""
offline_thumbnail: ""
uuid: b42ce7d8-4e6e-49cf-b7a9-fca338395ac3
updated: 1484308558
title: GV-linear-code
tags:
    - Gilbert–Varshamov bound theorem
    - Comments
categories:
    - Coding theory
---
In coding theory, the bound of parameters such as rate R, relative distance, block length, etc. is usually concerned. Here Gilbert–Varshamov bound theorem claims the lower bound of the rate of the general code. Gilbert–Varshamov bound is the best in terms of relative distance for codes over alphabets of size less than 49.[citation needed]
