---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_point_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 58e9dd46-eb3d-4454-82ed-fddd007498e5
updated: 1484309359
title: Control point (mathematics)
categories:
    - Geometry stubs
---
In computer-aided geometric design a control point is a member of a set of points used to determine the shape of a spline curve or, more generally, a surface or higher-dimensional object.[1]
