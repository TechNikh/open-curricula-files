---
version: 1
type: video
provider: YouTube
id: zQVNG_jsDgA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a89e5504-a7f4-4f2a-a86a-2ee993df9927
updated: 1486069613
title: Rounding Decimals
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/zQVNG_jsDgA/default.jpg
    - https://i3.ytimg.com/vi/zQVNG_jsDgA/1.jpg
    - https://i3.ytimg.com/vi/zQVNG_jsDgA/2.jpg
    - https://i3.ytimg.com/vi/zQVNG_jsDgA/3.jpg
---
