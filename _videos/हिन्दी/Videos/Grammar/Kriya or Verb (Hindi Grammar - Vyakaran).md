---
version: 1
type: video
provider: YouTube
id: CX1Q2xio7x0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 198113a9-1acb-4b16-8c1c-7f026ec4739d
updated: 1486069630
title: Kriya or Verb (Hindi Grammar / Vyakaran)
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/CX1Q2xio7x0/default.jpg
    - https://i3.ytimg.com/vi/CX1Q2xio7x0/1.jpg
    - https://i3.ytimg.com/vi/CX1Q2xio7x0/2.jpg
    - https://i3.ytimg.com/vi/CX1Q2xio7x0/3.jpg
---
