---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equilateral_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 608d271c-e498-49bc-b3a2-7c169a656920
updated: 1484309262
title: Equilateral dimension
tags:
    - Lebesgue spaces
    - Normed vector spaces
    - Riemannian manifolds
    - Notes
categories:
    - Metric geometry
---
In mathematics, the equilateral dimension of a metric space is the maximum number of points that are all at equal distances from each other.[1] Equilateral dimension has also been called "metric dimension", but the term "metric dimension" also has many other inequivalent usages.[1] The equilateral dimension of a d-dimensional Euclidean space is d + 1, and the equilateral dimension of a d-dimensional vector space with the Chebyshev distance (L∞ norm) is 2d. However, the equilateral dimension of a space with the Manhattan distance (L1 norm) is not known; Kusner's conjecture, named after Robert B. Kusner, states that it is exactly 2d.[2]
