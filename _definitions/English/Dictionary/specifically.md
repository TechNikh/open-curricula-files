---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specifically
offline_file: ""
offline_thumbnail: ""
uuid: 45ba52d3-98ee-4b80-a5a6-4f1e51af3150
updated: 1484310605
title: specifically
categories:
    - Dictionary
---
specifically
     adv : in distinction from others; "a program specifically for
           teenagers"; "he is interested specifically in poisonous
           snakes" [ant: {generally}]
