---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Split_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 758fee32-d2e0-494d-a99e-2f306c1cc112
updated: 1484309445
title: Split (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Split_decomposition.svg.png
tags:
    - Definitions
    - Examples
    - Algorithms
    - Applications
categories:
    - Graph theory objects
---
In graph theory, a split of an undirected graph is a cut whose cut-set forms a complete bipartite graph. A graph is prime if it has no splits. The splits of a graph can be collected into a tree-like structure called the split decomposition or join decomposition, which can be constructed in linear time. This decomposition has been used for fast recognition of circle graphs and distance-hereditary graphs, as well as for other problems in graph algorithms.
