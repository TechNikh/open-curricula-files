---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iterated_function
offline_file: ""
offline_thumbnail: ""
uuid: 680d3feb-2240-4e55-99a1-551520951e06
updated: 1484308794
title: Iterated function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/380px-Sine_iterations.svg.png
tags:
    - Definition
    - Abelian property and Iteration sequences
    - Fixed points
    - Limiting behaviour
    - Fractional iterates and flows, and negative iterates
    - Some formulas for fractional iteration
    - Example 1
    - Example 2
    - Example 3
    - Conjugacy
    - Markov chains
    - Examples
    - Means of study
    - In computer science
    - Definitions in terms of iterated functions
    - Functional derivative
    - "Lie's data transport equation"
categories:
    - Fixed points (mathematics)
---
In mathematics, an iterated function is a function X → X (that is, a function from some set X to itself) which is obtained by composing another function f : X → X with itself a certain number of times. The process of repeatedly applying the same function is called iteration. In this process, starting from some initial number, the result of applying a given function is fed again in the function as input, and this process is repeated.
