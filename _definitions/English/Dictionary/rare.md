---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rare
offline_file: ""
offline_thumbnail: ""
uuid: 5c14fbbb-0f16-4add-9b72-2b0434c98318
updated: 1484310281
title: rare
categories:
    - Dictionary
---
rare
     adj 1: not widely known; especially valued for its uncommonness; "a
            rare word"; "rare books"
     2: recurring only at long intervals; "a rare appearance";
        "total eclipses are rare events"
     3: not widely distributed; "rare herbs"; "rare patches of gree
        in the desert"
     4: marked by an uncommon quality; especially superlative or
        extreme of its kind; "what is so rare as a day in
        June"-J.R.Lowell; "a rare skill"; "an uncommon sense of
        humor"; "she was kind to an uncommon degree" [syn: {uncommon}]
     5: having low density; "rare gasses"; "lightheaded from the
        rarefied mountain air" [syn: {rarefied}, {rarified}]
     ...
