---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/busily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484472721
title: busily
categories:
    - Dictionary
---
busily
     adv : in a busy manner; "they were busily engaged in buying
           souvenirs"
