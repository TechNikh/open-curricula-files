---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bengt_Holmstr%C3%B6m'
offline_file: ""
offline_thumbnail: ""
uuid: 83b86fde-2062-43c9-b5a6-0f3672fd31a5
updated: 1484308517
title: Bengt Holmström
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Nobel_Laureates_0889_%252830679343193%2529.jpg'
tags:
    - Life and career
    - Publications
categories:
    - Game theorists
---
Bengt Robert Holmström (born 18 April 1949) is a Finnish economist who is currently Paul A. Samuelson Professor of Economics at the Massachusetts Institute of Technology. Together with Oliver Hart, he received the Central Bank of Sweden Nobel Memorial Prize in Economic Sciences in 2016.[2]
