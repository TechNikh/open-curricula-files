---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Snake-in-the-box
offline_file: ""
offline_thumbnail: ""
uuid: 70fde4ff-449d-46fb-9866-8ae15f7a5bc3
updated: 1484309396
title: Snake-in-the-box
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Snakeinthebox.svg.png
tags:
    - Known lengths and bounds
    - Notes
categories:
    - Computational problems in graph theory
---
The snake-in-the-box problem in graph theory and computer science deals with finding a certain kind of path along the edges of a hypercube. This path starts at one corner and travels along the edges to as many corners as it can reach. After it gets to a new corner, the previous corner and all of its neighbors must be marked as unusable. The path should never travel to a corner after it has been marked unusable.
