---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stationary
offline_file: ""
offline_thumbnail: ""
uuid: 237c3e9c-3446-4c2e-abab-44c91a20f724
updated: 1484310389
title: stationary
categories:
    - Dictionary
---
stationary
     adj 1: standing still; "the car remained stationary with the engine
            running"
     2: not capable of being moved; "stationary machinery"
