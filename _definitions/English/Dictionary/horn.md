---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horn
offline_file: ""
offline_thumbnail: ""
uuid: 6e51636a-21b2-4b09-8fb9-9387817f9806
updated: 1484310407
title: horn
categories:
    - Dictionary
---
horn
     n 1: a noisemaker (as at parties or games) that makes a loud
          noise when you blow through it
     2: one of the bony outgrowths on the heads of certain ungulates
     3: a noise made by the driver of an automobile to give warning;
     4: a high pommel of a Western saddle (usually metal covered
        with leather) [syn: {saddle horn}]
     5: a brass musical instrument with a brilliant tone; has a
        narrow tube and a flared bell and is played by means of
        valves [syn: {cornet}, {trumpet}, {trump}]
     6: any outgrowth from the head of an organism that resembles a
        horn
     7: the material (mostly keratin) that covers the horns of
        ungulates ...
