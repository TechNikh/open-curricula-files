---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformally_flat_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 87828c97-ea9d-4fd9-8730-50a48cb8915e
updated: 1484309185
title: Conformally flat manifold
categories:
    - Conformal geometry
---
More formally, let (M, g) be a pseudo-Riemannian manifold. Then (M, g) is conformally flat if for each point x in M, there exists a neighborhood U of x and a smooth function f defined on U such that (U, e2fg) is flat (i.e. the curvature of e2fg vanishes on U). The function f need not be defined on all of M.
