---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divisive
offline_file: ""
offline_thumbnail: ""
uuid: 30382bf4-b641-4434-828e-2cb960c175dd
updated: 1484310609
title: divisive
categories:
    - Dictionary
---
divisive
     adj : dissenting (especially dissenting with the majority opinion)
           [syn: {dissentious}, {factious}]
