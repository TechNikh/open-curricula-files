---
version: 1
type: video
provider: YouTube
id: ad5wPcKOMqQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d9e3515d-48e7-44c2-991a-f50b18cd19c1
updated: 1486069651
title: "Foucault's pendulum - 1"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ad5wPcKOMqQ/default.jpg
    - https://i3.ytimg.com/vi/ad5wPcKOMqQ/1.jpg
    - https://i3.ytimg.com/vi/ad5wPcKOMqQ/2.jpg
    - https://i3.ytimg.com/vi/ad5wPcKOMqQ/3.jpg
---
