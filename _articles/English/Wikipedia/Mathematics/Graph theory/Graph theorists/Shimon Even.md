---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shimon_Even
offline_file: ""
offline_thumbnail: ""
uuid: d2fcd4fd-cee9-4597-b710-c12a8d5ba8b6
updated: 1484309463
title: Shimon Even
categories:
    - Graph theorists
---
Shimon Even (Hebrew: שמעון אבן‎‎; June 15, 1935 – May 1, 2004) was an Israeli computer science researcher. His main topics of interest included algorithms, graph theory and cryptography. He was a member of the Computer Science Department at the Technion since 1974. Shimon Even was the PhD advisor of Oded Goldreich, a prominent cryptographer.
