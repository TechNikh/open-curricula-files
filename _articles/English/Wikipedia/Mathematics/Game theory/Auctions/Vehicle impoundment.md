---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vehicle_impoundment
offline_file: ""
offline_thumbnail: ""
uuid: 321ab814-0031-43ab-892e-96a546eb6330
updated: 1484309013
title: Vehicle impoundment
tags:
    - Reasons for impoundment
    - Grant of authority
    - Locating the vehicle
    - Towing the vehicle
    - Auction
    - Origin
    - Local laws
    - Australia
    - Queensland
    - Canada
    - Ontario
categories:
    - Auctions
---
Vehicle impoundment is the legal process of placing a vehicle into an impoundment lot, which is a holding place for cars until they are placed back in the control of the owner, recycled for their metal, stripped of their parts at a wrecking yard or auctioned off for the benefit of the impounding agency.
