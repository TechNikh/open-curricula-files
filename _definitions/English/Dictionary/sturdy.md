---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sturdy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484564401
title: sturdy
categories:
    - Dictionary
---
sturdy
     adj 1: having rugged physical strength; inured to fatigue or
            hardships; "hardy explorers of northern Canada";
            "proud of her tall stalwart son"; "stout seamen";
            "sturdy young athletes" [syn: {hardy}, {stalwart}, {stout}]
     2: substantially made or constructed; "sturdy steel shelves";
        "sturdy canvas"; "a tough all-weather fabric"; "some
        plastics are as tough as metal" [syn: {tough}]
     [also: {sturdiest}, {sturdier}]
