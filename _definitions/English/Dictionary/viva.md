---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viva
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484526901
title: viva
categories:
    - Dictionary
---
viva
     n : an examination conducted by word of mouth [syn: {oral}, {oral
         exam}, {oral examination}, {viva voce}]
