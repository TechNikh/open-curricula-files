---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/directly
offline_file: ""
offline_thumbnail: ""
uuid: aabe5b61-21aa-45cf-8c34-a68530142c57
updated: 1484310339
title: directly
categories:
    - Dictionary
---
directly
     adv 1: without deviation; "the path leads directly to the lake";
            "went direct to the office" [syn: {straight}, {direct}]
     2: without anyone or anything intervening; "these two factors
        are directly related"; "he was directly responsible";
        "measured the physical properties directly"
     3: without delay or hesitation; with no time intervening; "he
        answered immediately"; "found an answer straightaway"; "an
        official accused of dishonesty should be suspended
        forthwith"; "Come here now!" [syn: {immediately}, {instantly},
         {straightaway}, {straight off}, {now}, {right away}, {at
        once}, {forthwith}, {in real ...
