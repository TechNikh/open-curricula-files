---
version: 1
type: video
provider: YouTube
id: jwwUKaUupoM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5d47a4ab-f450-4d7e-b243-49a0ac2927b4
updated: 1486069636
title: Refraction Experiment by Sanjeev Sharma
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/jwwUKaUupoM/default.jpg
    - https://i3.ytimg.com/vi/jwwUKaUupoM/1.jpg
    - https://i3.ytimg.com/vi/jwwUKaUupoM/2.jpg
    - https://i3.ytimg.com/vi/jwwUKaUupoM/3.jpg
---
