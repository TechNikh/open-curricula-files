---
version: 1
type: video
provider: YouTube
id: pXvvhTV9K3w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1a954f0e-f6f3-4abb-b957-0ba79cfa2553
updated: 1486069636
title: Experiment to prove Dispersion of Light (Hindi)
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/pXvvhTV9K3w/default.jpg
    - https://i3.ytimg.com/vi/pXvvhTV9K3w/1.jpg
    - https://i3.ytimg.com/vi/pXvvhTV9K3w/2.jpg
    - https://i3.ytimg.com/vi/pXvvhTV9K3w/3.jpg
---
