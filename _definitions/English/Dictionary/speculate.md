---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speculate
offline_file: ""
offline_thumbnail: ""
uuid: a5da5095-9f8c-40d1-b6ba-4cde10787f79
updated: 1484310475
title: speculate
categories:
    - Dictionary
---
speculate
     v 1: to believe especially on uncertain or tentative grounds;
          "Scientists supposed that large dinosaurs lived in
          swamps" [syn: {theorize}, {theorise}, {conjecture}, {hypothesize},
           {hypothesise}, {hypothecate}, {suppose}]
     2: talk over conjecturally, or review in an idle or casual way
        and with an element of doubt or without sufficient reason
        to reach a conclusion; "We were speculating whether the
        President had to resign after the scandal"
     3: reflect deeply on a subject; "I mulled over the events of
        the afternoon"; "philosophers have speculated on the
        question of God for thousands of years"; "The ...
