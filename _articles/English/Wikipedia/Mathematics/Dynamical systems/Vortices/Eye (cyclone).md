---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eye_(cyclone)
offline_file: ""
offline_thumbnail: ""
uuid: 3e37e226-9233-4af9-a5d8-060f765546c4
updated: 1484308875
title: Eye (cyclone)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hurricane_Isabel_from_ISS.jpg
tags:
    - Structure
    - Formation and detection
    - Associated phenomena
    - Eyewall replacement cycles
    - Moats
    - Eyewall mesovortices
    - Stadium effect
    - Eye-like features
    - Hazards
    - Other cyclones
    - Polar lows
    - Extratropical cyclones
    - Subtropical cyclones
    - Tornadoes
    - Extraterrestrial vortices
categories:
    - Vortices
---
The eye is a region of mostly calm weather at the center of strong tropical cyclones. The eye of a storm is a roughly circular area, typically 30–65 km (20–40 miles) in diameter. It is surrounded by the eyewall, a ring of towering thunderstorms where the most severe weather occurs. The cyclone's lowest barometric pressure occurs in the eye and can be as much as 15 percent lower than the pressure outside the storm.[1]
