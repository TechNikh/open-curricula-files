---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Critical_point_(network_science)
offline_file: ""
offline_thumbnail: ""
uuid: 42b0136e-7ae1-4767-b5d7-ab5cf63ba751
updated: 1484309432
title: Critical point (network science)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Network_in_different_regimes.png
tags:
    - Subcritical regime
    - Supercritical regime
    - Example on different regimes
categories:
    - Network theory
---
In network science, a critical point is a value of average degree, which separates random networks that have a giant component from those that do not (i.e. it separates a network in a subcritical regime from one in a supercritical regime).[1] Considering a random network with an average degree 
  
    
      
        <
        k
        >
      
    
    {\displaystyle <k>}
  
 the critical point is
