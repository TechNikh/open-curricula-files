---
version: 1
type: video
provider: YouTube
id: FK1s1-OJ5BE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Plate%20tectonics-%20Geological%20features%20of%20divergent%20plate%20boundaries.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d2ad3a37-a6bb-4665-9f0d-cf6762542cac
updated: 1486069686
title: 'Plate tectonics- Geological features of divergent plate boundaries'
categories:
    - Cosmology and Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/FK1s1-OJ5BE/default.jpg
    - https://i3.ytimg.com/vi/FK1s1-OJ5BE/1.jpg
    - https://i3.ytimg.com/vi/FK1s1-OJ5BE/2.jpg
    - https://i3.ytimg.com/vi/FK1s1-OJ5BE/3.jpg
---
