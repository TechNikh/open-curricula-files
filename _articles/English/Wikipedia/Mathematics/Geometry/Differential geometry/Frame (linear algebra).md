---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frame_(linear_algebra)
offline_file: ""
offline_thumbnail: ""
uuid: afbe4a0a-7810-4255-b74d-2fb117e15b3d
updated: 1484309218
title: Frame (linear algebra)
tags:
    - Definition and motivation
    - 'Motivating example: computing a basis from a linearly dependent set'
    - Formal definition
    - History
    - Relation to bases
    - Applications
    - Special cases
    - Generalizations
    - Dual frames
    - Notes
categories:
    - Differential geometry
---
In linear algebra, a frame of an inner product space is a generalization of a basis of a vector space to sets that may be linearly dependent. In the terminology of signal processing, a frame provides a redundant, stable way of representing a signal.[1] Frames are used in error detection and correction and the design and analysis of filter banks and more generally in applied mathematics, computer science, and engineering.[2]
