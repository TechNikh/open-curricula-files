---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Werner_Fenchel
offline_file: ""
offline_thumbnail: ""
uuid: 88a4bfb6-71f5-4938-a594-6e5179bc5028
updated: 1484309078
title: Werner Fenchel
tags:
    - Biography
    - Early life and education
    - Professorship in Germany
    - Professorship in exile
    - Professorship postwar
    - Last years, death, legacy
    - Geometric contributions
    - Convex geometry
    - Optimization theory
    - Hyperbolic geometry
    - Books
categories:
    - Geometers
---
Moritz Werner Fenchel (German: [ˈfɛnçəl]; 3 May 1905 – 24 January 1988) was a mathematician known for his contributions to geometry and to optimization theory. Fenchel established the basic results of convex analysis and nonlinear optimization theory which would, in time, serve as the foundation for nonlinear programming. A German-born Jew and early refugee from Nazi suppression of intellectuals, Fenchel lived most of his life in Denmark. Fenchel's monographs and lecture notes are considered influential.
