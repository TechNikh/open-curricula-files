---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ideal
offline_file: ""
offline_thumbnail: ""
uuid: cc071857-265b-4907-a2c1-6d03210435e0
updated: 1484310384
title: ideal
categories:
    - Dictionary
---
ideal
     adj 1: conforming to an ultimate standard of perfection or
            excellence; embodying an ideal
     2: constituting or existing only in the form of an idea or
        mental image or conception; "a poem or essay may be
        typical of its period in idea or ideal content"
     3: of or relating to the philosophical doctrine of the reality
        of ideas [syn: {idealistic}]
     n 1: the idea of something that is perfect; something that one
          hopes to attain
     2: model of excellence or perfection of a kind; one having no
        equal [syn: {paragon}, {nonpareil}, {saint}, {apotheosis},
         {nonesuch}, {nonsuch}]
