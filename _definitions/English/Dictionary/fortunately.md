---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fortunately
offline_file: ""
offline_thumbnail: ""
uuid: c4ff325f-c333-44cb-8b87-278860e32432
updated: 1484310599
title: fortunately
categories:
    - Dictionary
---
fortunately
     adv : by good fortune; "fortunately the weather was good" [syn: {fortuitously},
            {luckily}, {as luck would have it}] [ant: {unfortunately},
            {unfortunately}]
