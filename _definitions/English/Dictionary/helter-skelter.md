---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helter-skelter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495281
title: helter-skelter
categories:
    - Dictionary
---
helter-skelter
     adj 1: lacking a visible order or organization [syn: {chaotic}]
     2: with undue hurry and confusion; "a helter-skelter kind of
        existence with never a pause"; "a pell-mell dash for the
        train" [syn: {pell-mell}]
     adv : haphazardly; "the books were piled up helter-skelter" [syn:
           {every which way}]
