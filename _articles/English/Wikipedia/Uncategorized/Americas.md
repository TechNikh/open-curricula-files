---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Americas
offline_file: ""
offline_thumbnail: ""
uuid: f1c645eb-7f7e-4180-ba3f-3ee00be60715
updated: 1484308342
title: Americas
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Americas_%2528orthographic_projection%2529.svg.png'
tags:
    - Etymology and naming
    - History
    - Settlement
    - Pre-Columbian era
    - European colonization
    - geography
    - Extent
    - Geology
    - Topography
    - Climate
    - hydrology
    - Ecology
    - Countries and territories
    - Demography
    - population
    - Largest urban centers
    - Ethnology
    - religion
    - Languages
    - Terminology
    - english
    - Spanish
    - Portuguese
    - French
    - Dutch
    - Multinational organizations
    - Notes
categories:
    - Uncategorized
---
The Americas, also collectively called America,[4][5][6] encompass the totality of the continents of North America and South America.[7][8][9] Together they make up most of Earth's western hemisphere[10][11][12][13][14][15][16][17] and comprise the New World.
