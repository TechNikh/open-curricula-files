---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/germanium
offline_file: ""
offline_thumbnail: ""
uuid: 031bd023-7011-481a-9480-d9ee31c6c731
updated: 1484310200
title: germanium
categories:
    - Dictionary
---
germanium
     n : a brittle gray crystalline element that is a semiconducting
         metalloid (resembling silicon) used in transistors;
         occurs in germanite and argyrodite [syn: {Ge}, {atomic
         number 32}]
