---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proportionality
offline_file: ""
offline_thumbnail: ""
uuid: 7fd576dd-c866-43cc-9de8-6d3286331989
updated: 1484310198
title: proportionality
categories:
    - Dictionary
---
proportionality
     n : the quotient obtained when the magnitude of a part is
         divided by the magnitude of the whole [syn: {proportion}]
