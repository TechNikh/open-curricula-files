---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fair
offline_file: ""
offline_thumbnail: ""
uuid: 551087a3-8477-422a-a76f-cb1ce214c0ed
updated: 1484310457
title: fair
categories:
    - Dictionary
---
fair
     adj 1: free from favoritism or self-interest or bias or deception;
            or conforming with established standards or rules; "a
            fair referee"; "fair deal"; "on a fair footing"; "a
            fair fight"; "by fair means or foul" [syn: {just}]
            [ant: {unfair}]
     2: showing lack of favoritism; "the cold neutrality of an
        impartial judge" [syn: {impartial}] [ant: {partial}]
     3: more than adequate in quality; "fair work"
     4: not excessive or extreme; "a fairish income"; "reasonable
        prices" [syn: {fairish}, {reasonable}]
     5: visually appealing; "our fair city" [syn: {sightly}]
     6: very pleasing to the eye; "my bonny lass"; ...
