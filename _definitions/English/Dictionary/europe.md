---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/europe
offline_file: ""
offline_thumbnail: ""
uuid: 2cae26b6-17a6-4ab7-8058-18979f49c40e
updated: 1484310473
title: europe
categories:
    - Dictionary
---
Europe
     n 1: the 2nd smallest continent (actually a vast peninsula of
          Eurasia); the British use `Europe' to refer to all of
          the continent except the British Isles
     2: an international organization of European countries formed
        after World War II to reduce trade barriers and increase
        cooperation among its members; "he took Britain into
        Europe" [syn: {European Union}, {EU}, {European Community},
         {EC}, {European Economic Community}, {EEC}, {Common
        Market}]
     3: the nations of the European continent collectively; "the
        Marshall Plan helped Europe recover from World War II"
