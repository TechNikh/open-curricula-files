---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Virtual_Cybernetic_Building_Testbed
offline_file: ""
offline_thumbnail: ""
uuid: dd65d94f-bbda-4901-8df8-34e1d56e87ee
updated: 1484308742
title: Virtual Cybernetic Building Testbed
categories:
    - Control engineering
---
The Virtual Cybernetic Building Testbed (VCBT) is a whole building emulator located at the National Institute of Standards and Technology in Gaithersburg, MD. It is designed with enough flexibility to be capable of reproducibly simulating normal operation and a variety of faulty and hazardous conditions that might occur in a cybernetic building. It serves as a testbed for investigating the interactions between integrated building systems and a wide range of issues important to the development of cybernetic building technology.
