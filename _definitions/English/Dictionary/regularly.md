---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regularly
offline_file: ""
offline_thumbnail: ""
uuid: b606dc06-3034-419e-b1ea-ea7619151686
updated: 1484310457
title: regularly
categories:
    - Dictionary
---
regularly
     adv 1: in a regular manner; "letters arrived regularly from his
            children" [syn: {on a regular basis}] [ant: {irregularly}]
     2: having a regular form; "regularly shaped objects" [ant: {irregularly}]
     3: in a regular way without variation; "try to breathe evenly"
        [ant: {irregularly}]
