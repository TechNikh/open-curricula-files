---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dirty
offline_file: ""
offline_thumbnail: ""
uuid: 27404f9a-11c1-4af3-a0ea-25f08f8efca8
updated: 1484310426
title: dirty
categories:
    - Dictionary
---
dirty
     adj 1: soiled or likely to soil with dirt or grime; "dirty unswept
            sidewalks"; "a child in dirty overalls"; "dirty
            slums"; "piles of dirty dishes"; "put his dirty feet
            on the clean sheet"; "wore an unclean shirt"; "mining
            is a dirty job"; "Cinderella did the dirty work while
            her sisters preened themselves" [syn: {soiled}, {unclean}]
            [ant: {clean}]
     2: (of behavior or especially language) characterized by
        obscenity or indecency; "dirty words"; "a dirty old man";
        "dirty books and movies"; "boys telling dirty jokes"; "has
        a dirty mouth" [ant: {clean}]
     3: vile; despicable; "a ...
