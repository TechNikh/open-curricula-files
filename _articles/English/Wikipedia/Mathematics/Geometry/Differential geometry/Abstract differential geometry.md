---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abstract_differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 18e8e913-a0c0-4d94-8059-7f6968a6c856
updated: 1484309202
title: Abstract differential geometry
tags:
    - Applications
    - ADG Gravity
categories:
    - Differential geometry
---
The adjective abstract has often been applied to differential geometry before, but the abstract differential geometry (ADG) of this article is a form of differential geometry without the calculus notion of smoothness, developed by Anastasios Mallios and others from 1998 onwards.[1]
