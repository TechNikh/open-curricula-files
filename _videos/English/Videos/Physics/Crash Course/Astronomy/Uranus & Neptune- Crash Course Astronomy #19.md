---
version: 1
type: video
provider: YouTube
id: 1hIwD17Crko
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Uranus%20%26%20Neptune-%20Crash%20Course%20Astronomy%20%2319.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 517f5b00-c9df-4af9-85ed-cf3e3f3d45f8
updated: 1486069684
title: 'Uranus & Neptune- Crash Course Astronomy #19'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/1hIwD17Crko/default.jpg
    - https://i3.ytimg.com/vi/1hIwD17Crko/1.jpg
    - https://i3.ytimg.com/vi/1hIwD17Crko/2.jpg
    - https://i3.ytimg.com/vi/1hIwD17Crko/3.jpg
---
