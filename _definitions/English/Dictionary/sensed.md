---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sensed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484646421
title: sensed
categories:
    - Dictionary
---
sensed
     adj : detected by instinct or inference rather than by recognized
           perceptual cues; "the felt presence of an intruder"; "a
           sensed presence in the room raised goosebumps on her
           arms"; "a perceived threat" [syn: {perceived}]
