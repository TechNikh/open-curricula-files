---
version: 1
type: video
provider: YouTube
id: 1gMMM62NC-4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Amazing%20Magnetic%20Levitation%20Device%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 020580cc-c4fe-46de-bbcf-7e9a1de98830
updated: 1486069660
title: Amazing Magnetic Levitation Device!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/1gMMM62NC-4/default.jpg
    - https://i3.ytimg.com/vi/1gMMM62NC-4/1.jpg
    - https://i3.ytimg.com/vi/1gMMM62NC-4/2.jpg
    - https://i3.ytimg.com/vi/1gMMM62NC-4/3.jpg
---
