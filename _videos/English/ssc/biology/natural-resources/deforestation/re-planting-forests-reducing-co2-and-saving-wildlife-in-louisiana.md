---
title: Re-planting Forests, Reducing CO2 and Saving Wildlife in Louisiana
youtube_id: 59cIzj7Zplc
tags:
    - Climate change
    - tree planting
    - carbon sequestration
    - mitigation
    - tractor
    - birds
    - usfws
    - us fish and wildlife service
    - 50/50/50
    - the conservation fund
    - go zero
    - jena meredith
    - lake ophelia
    - grand cote
    - national wildlife refuge
    - louisiana
categories:
    - Deforestation
---
