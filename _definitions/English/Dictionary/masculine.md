---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/masculine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557381
title: masculine
categories:
    - Dictionary
---
masculine
     adj 1: of grammatical gender [ant: {feminine}, {neuter}]
     2: associated with men and not with women [ant: {feminine}]
     3: (music or poetry) ending on an accented beat or syllable; "a
        masculine cadence"; "the masculine rhyme of `annoy,
        enjoy'"
     n : a gender that refers chiefly (but not exclusively) to males
         or to objects classified as male
