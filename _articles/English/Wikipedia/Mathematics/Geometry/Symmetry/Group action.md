---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_action
offline_file: ""
offline_thumbnail: ""
uuid: f30952f2-fc36-40f9-8f65-0aa3ab3c99c4
updated: 1484309312
title: Group action
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Group_action_on_equilateral_triangle.svg.png
tags:
    - Definition
    - Examples
    - Types of actions
    - Orbits and stabilizers
    - Invariant subsets
    - Fixed points and stabilizer subgroups
    - "Orbit-stabilizer theorem and Burnside's lemma"
    - Group actions and groupoids
    - Morphisms and isomorphisms between G-sets
    - Continuous group actions
    - Strongly continuous group action and smooth points
    - Variants and generalizations
    - Notes
categories:
    - Symmetry
---
In mathematics, an action of a group is a way of interpreting the elements of the group as "acting" on some space in a way that preserves the structure of that space. Common examples of spaces that groups act on are sets, vector spaces, and topological spaces. Actions of groups on vector spaces are called representations of the group.
