---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3D_pose_estimation
offline_file: ""
offline_thumbnail: ""
uuid: 60001c2a-5a41-47b9-885d-a155a23af281
updated: 1484309179
title: 3D pose estimation
tags:
    - 3D pose estimation from an uncalibrated 2D camera
    - 3D pose estimation from a calibrated 2D camera
    - Extracting 3D from 2D
    - Pseudocode
    - Estimating pose through comparison
    - Software
    - Bibliography
categories:
    - Geometry in computer vision
---
3D pose estimation is the problem of determining the transformation of an object in a 2D image which gives the 3D object. The need for 3D pose estimation arises from the limitations of feature based pose estimation. There exist environments where it is difficult to extract corners or edges from an image. To circumvent these issues, the object is dealt with as a whole through the use of free-form contours.[1]
