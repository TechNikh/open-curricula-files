---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beat_(acoustics)
offline_file: ""
offline_thumbnail: ""
uuid: 134c7712-4ca9-4ece-a0c0-b64a1173f6f5
updated: 1484308814
title: Beat (acoustics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/214px-Beating_Frequency.svg.png
tags:
    - Mathematics and physics of beat tones
    - Uses
    - Binaural beats
    - sample
categories:
    - Oscillation
---
In acoustics, a beat is an interference pattern between two sounds of slightly different frequencies, perceived as a periodic variation in volume whose rate is the difference of the two frequencies.
