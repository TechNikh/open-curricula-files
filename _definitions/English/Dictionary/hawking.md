---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hawking
offline_file: ""
offline_thumbnail: ""
uuid: 02ca18ef-da16-4372-aef9-923243596d97
updated: 1484310517
title: hawking
categories:
    - Dictionary
---
Hawking
     n 1: English theoretical physicist (born in 1942) [syn: {Stephen
          Hawking}, {Stephen William Hawking}]
     2: the act of selling goods for a living [syn: {vending}, {peddling},
         {vendition}]
