---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adverbial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484585221
title: adverbial
categories:
    - Dictionary
---
adverbial
     adj : of or relating to or functioning as an adverb; "adverbial
           syntax"
     n : a word or group of words function as an adverb
