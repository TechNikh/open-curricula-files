---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lama
offline_file: ""
offline_thumbnail: ""
uuid: cfa1b7ba-502b-4b95-a588-9eb778bed696
updated: 1484310176
title: lama
categories:
    - Dictionary
---
lama
     n 1: a Tibetan or Mongolian priest of Lamaism
     2: llamas [syn: {genus Lama}]
