---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genuine
offline_file: ""
offline_thumbnail: ""
uuid: 2ccd2afe-1fc8-4b60-be68-b2d0f5bd8b50
updated: 1484310609
title: genuine
categories:
    - Dictionary
---
genuine
     adj 1: not fake or counterfeit; "a genuine Picasso"; "genuine
            leather" [syn: {echt}] [ant: {counterfeit}]
     2: not pretended; sincerely felt or expressed; "genuine
        emotion"; "her interest in people was unfeigned"; "true
        grief" [syn: {true(a)}, {unfeigned}]
     3: being or reflecting the essential or genuine character of
        something; "her actual motive"; "a literal solitude like a
        desert"- G.K.Chesterton; "a genuine dilemma" [syn: {actual},
         {literal}, {real}]
