---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Corinna_Ulcigrai
offline_file: ""
offline_thumbnail: ""
uuid: aa071fd6-b641-4b3a-9c55-3a47f67c2fb6
updated: 1484308696
title: Corinna Ulcigrai
categories:
    - Dynamical systems theorists
---
Corinna Ulcigrai (born 3 January 1980, Trieste)[1] is an Italian mathematician working on dynamical systems.[2] She obtained her Ph.D. in 2007 from Princeton University with Yakov Sinai as her thesis advisor.[2][3] She was awarded the European Mathematical Society Prize in 2012,[2][4] and the Whitehead Prize in 2013.[5] She works at the University of Bristol, United Kingdom.[5]
