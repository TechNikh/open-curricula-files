---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laser_cooling
offline_file: ""
offline_thumbnail: ""
uuid: 47073628-b74f-4120-baf8-0f4b30b5d3cb
updated: 1484308849
title: Laser cooling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Doppler_laser_cooling.svg.png
tags:
    - Doppler cooling
    - Uses
    - Additional sources
categories:
    - Thermodynamics
---
Laser cooling refers to a number of techniques in which atomic and molecular samples are cooled down to near absolute zero through the interaction with one or more laser fields. All laser cooling techniques rely on the fact that when an object (usually an atom) absorbs and re-emits a photon (a particle of light) its momentum changes. The temperature of an ensemble of particles is larger for larger variance in the velocity distribution of the particles. Laser cooling techniques combine atomic spectroscopy with the aforementioned mechanical effect of light to compress the velocity distribution of an ensemble of particles, thereby cooling the particles.
