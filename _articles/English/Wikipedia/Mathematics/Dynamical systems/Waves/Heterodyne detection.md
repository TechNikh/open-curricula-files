---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heterodyne_detection
offline_file: ""
offline_thumbnail: ""
uuid: 69f86ccc-fe1a-40e2-82d6-80765974cfa8
updated: 1484308889
title: Heterodyne detection
categories:
    - Waves
---
Heterodyne detection is a method of detecting radiation by non-linear mixing with radiation of a reference frequency. It is commonly used in telecommunications and astronomy for detecting and analysing signals.
