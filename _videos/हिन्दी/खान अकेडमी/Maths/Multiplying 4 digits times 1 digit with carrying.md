---
version: 1
type: video
provider: YouTube
id: F2d-ZtzV4d0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fb757bf4-82d2-42ed-9c3a-f5536a703546
updated: 1486069614
title: Multiplying 4 digits times 1 digit with carrying
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/F2d-ZtzV4d0/default.jpg
    - https://i3.ytimg.com/vi/F2d-ZtzV4d0/1.jpg
    - https://i3.ytimg.com/vi/F2d-ZtzV4d0/2.jpg
    - https://i3.ytimg.com/vi/F2d-ZtzV4d0/3.jpg
---
