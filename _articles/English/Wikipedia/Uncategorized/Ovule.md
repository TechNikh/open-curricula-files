---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ovules
offline_file: ""
offline_thumbnail: ""
uuid: 43761f49-f681-4d4f-9e8b-4513cacacffc
updated: 1484308351
title: Ovule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ovules_in_flower.png
tags:
    - Location within the plant
    - Ovule parts and development
    - Integuments, micropyle and chalaza
    - Nucellus, megaspore and perisperm
    - Megagametophyte
    - Zygote, embryo and endosperm
    - Types of gametophytes
    - Bibliography
categories:
    - Uncategorized
---
In seed plants, the ovule ("small egg") is the structure that gives rise to and contains the female reproductive cells. It consists of three parts: The integument(s) forming its outer layer(s), the nucellus (or remnant of the megasporangium), and female gametophyte (formed from haploid megaspore) in its center. The female gametophyte—specifically termed a megagametophyte—is also called the embryo sac in angiosperms. The megagametophyte produces an egg cell (or several egg cells in some groups) for the purpose of fertilization. After fertilization, the ovule develops into a seed.
