---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/move
offline_file: ""
offline_thumbnail: ""
uuid: b6aa79b5-35d0-449c-943c-28b05220b980
updated: 1484310341
title: move
categories:
    - Dictionary
---
move
     n 1: the act of deciding to do something; "he didn't make a move
          to help"; "his first move was to hire a lawyer"
     2: the act of changing your residence or place of business;
        "they say that three moves equal one fire" [syn: {relocation}]
     3: a change of position that does not entail a change of
        location; "the reflex motion of his eyebrows revealed his
        surprise"; "movement is a sign of life"; "an impatient
        move of his hand"; "gastrointestinal motility" [syn: {motion},
         {movement}, {motility}]
     4: the act of changing location from one place to another;
        "police controlled the motion of the crowd"; "the movement
     ...
