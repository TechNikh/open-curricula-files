---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rid
offline_file: ""
offline_thumbnail: ""
uuid: 679ee839-a969-4b99-907e-ac6f3e1259d7
updated: 1484310383
title: rid
categories:
    - Dictionary
---
rid
     v : relieve from; "Rid the the house of pests" [syn: {free}, {disembarrass}]
     [also: {ridding}, {ridded}]
