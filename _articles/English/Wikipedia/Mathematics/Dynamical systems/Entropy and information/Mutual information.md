---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mutual_information
offline_file: ""
offline_thumbnail: ""
uuid: a82be047-f0c8-45da-a0da-0c765f80a27a
updated: 1484308773
title: Mutual information
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Entropy-mutual-information-relative-entropy-relation-diagram.svg_1.png
tags:
    - Definition of mutual information
    - Relation to other quantities
    - Variations of mutual information
    - Metric
    - Conditional mutual information
    - Multivariate mutual information
    - Applications
    - Directed Information
    - Normalized variants
    - Weighted variants
    - Adjusted mutual information
    - Absolute mutual information
    - Mutual information and linear correlation
    - Mutual information for discrete data
    - Applications of mutual information
    - Notes
categories:
    - Entropy and information
---
In probability theory and information theory, the mutual information (MI) of two random variables is a measure of the mutual dependence between the two variables. More specifically, it quantifies the "amount of information" (in units such as bits) obtained about one random variable, through the other random variable. The concept of mutual information is intricately linked to that of entropy of a random variable, a fundamental notion in information theory, that defines the "amount of information" held in a random variable.
