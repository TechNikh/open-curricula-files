---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radioactive
offline_file: ""
offline_thumbnail: ""
uuid: fe96f550-99b8-4f3c-87fa-211f9967812a
updated: 1484310281
title: radioactive
categories:
    - Dictionary
---
radioactive
     adj : exhibiting or caused by radioactivity; "radioactive
           isotope"; "radioactive decay"; "radioactive fallout"
           [ant: {nonradioactive}]
