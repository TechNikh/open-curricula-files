---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arrangement_of_lines
offline_file: ""
offline_thumbnail: ""
uuid: c19ac755-92be-4b7a-a8db-c06fe408673f
updated: 1484308595
title: Arrangement of lines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Complete-quads.svg_0.png
tags:
    - Definition
    - Complexity of arrangements
    - Projective arrangements and projective duality
    - Triangles in arrangements
    - Multigrids and Penrose tilings
    - Algorithms
    - Non-Euclidean line arrangements
    - Notes
categories:
    - Discrete geometry
---
In geometry an arrangement of lines is the partition of the plane formed by a collection of lines. Bounds on the complexity of arrangements have been studied in discrete geometry, and computational geometers have found algorithms for the efficient construction of arrangements.
