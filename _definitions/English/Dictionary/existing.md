---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/existing
offline_file: ""
offline_thumbnail: ""
uuid: 145ecbe0-b240-4082-a25c-e3b6dce7fd6f
updated: 1484310268
title: existing
categories:
    - Dictionary
---
existing
     adj 1: having existence or being or actuality; "an attempt to
            refine the existent machinery to make it more
            efficient"; "much of the beluga caviar existing in the
            world is found in the Soviet Union and Iran" [syn: {existent}]
            [ant: {nonexistent}]
     2: existing in something specified; "depletion of the oxygen
        existing in the bloodstream"
     3: presently existing; "the existing system"
