---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ocean
offline_file: ""
offline_thumbnail: ""
uuid: 124d12e0-f405-4574-a348-e274d93a4a65
updated: 1484310232
title: ocean
categories:
    - Dictionary
---
ocean
     n 1: a large body of water constituting a principal part of the
          hydrosphere
     2: anything apparently limitless in quantity or volume [syn: {sea}]
