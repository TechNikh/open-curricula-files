---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fridge
offline_file: ""
offline_thumbnail: ""
uuid: 6c97dea8-3749-49f3-95cd-948ade353b01
updated: 1484310234
title: fridge
categories:
    - Dictionary
---
fridge
     n : a refrigerator in which the coolant is pumped around by an
         electric motor [syn: {electric refrigerator}]
