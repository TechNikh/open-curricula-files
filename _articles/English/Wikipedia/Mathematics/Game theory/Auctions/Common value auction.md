---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Common_value_auction
offline_file: ""
offline_thumbnail: ""
uuid: dfee49a6-2576-473c-b4b4-47dd839cf0dc
updated: 1484309017
title: Common value auction
tags:
    - Interdependent value auctions
    - Examples
    - Binary signals, first-price auction
    - Independent signals, second-price auction
    - Dependent signals, second-price auction
    - Dependent signals, first-price auction
    - Relationship to Bertrand competition
categories:
    - Auctions
---
In common value auctions the value of the item for sale is identical amongst bidders, but bidders have different information about the item's value. This stands in contrast to a private value auction where each bidder's private valuation of the item is different and independent of peers' valuations.[1]
