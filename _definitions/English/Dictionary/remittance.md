---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remittance
offline_file: ""
offline_thumbnail: ""
uuid: 55c061ea-e33f-4a96-b954-909deaf8de34
updated: 1484310515
title: remittance
categories:
    - Dictionary
---
remittance
     n : a payment of money sent to a person in another place [syn: {remittal},
          {remission}, {remitment}]
