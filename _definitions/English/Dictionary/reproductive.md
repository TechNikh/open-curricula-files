---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reproductive
offline_file: ""
offline_thumbnail: ""
uuid: 4a40a093-3410-40ed-b5ef-1eb30ce6977c
updated: 1484310309
title: reproductive
categories:
    - Dictionary
---
reproductive
     adj : producing new life or offspring; "tXsXwhe reproductive
           potential of a species is its relative capacity to
           reproduce itself under optimal conditions"; "the
           reproductive or generative organs" [syn: {generative},
           {procreative}]
