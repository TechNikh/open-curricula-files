---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ELEMENTARY
offline_file: ""
offline_thumbnail: ""
uuid: ce5e6760-0c01-4ce8-9d18-51ed82c2b567
updated: 1484309485
title: ELEMENTARY
tags:
    - Definition
    - Lower elementary recursive functions
    - Basis for ELEMENTARY
    - Descriptive characterization
    - Notes
categories:
    - Computability theory
---
The name was coined by László Kalmár, in the context of recursive functions and undecidability; most problems in it are far from elementary. Some natural recursive problems lie outside ELEMENTARY, and are thus NONELEMENTARY. Most notably, there are primitive recursive problems that are not in ELEMENTARY. We know
