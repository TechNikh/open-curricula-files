---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sardar
offline_file: ""
offline_thumbnail: ""
uuid: f98e06e7-7c76-438f-80f2-7b2c58b14000
updated: 1484308466
title: Sardar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jackiesardar.JPG
tags:
    - Princes
    - Noblemen
    - Aristocrats
    - Head of State
    - Military title
    - Modern usage
    - Notes
categories:
    - Uncategorized
---
Sardar (Persian: سردار ‎‎, Persian pronunciation: ['særdʒæ:r]; "Commander" literally; "Headmaster"), also spelled as Sirdar, Sardaar or Serdar, is a title of nobility that was originally used to denote princes, noblemen, and other aristocrats. It has also been used to denote a chief or leader of a tribe or group. It is used as a Persian synonym of the Arabic title Amir.
