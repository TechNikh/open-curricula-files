---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/farmer
offline_file: ""
offline_thumbnail: ""
uuid: 30d8308c-e77b-461e-b895-4dea77c3c452
updated: 1484310277
title: farmer
categories:
    - Dictionary
---
farmer
     n 1: a person who operates a farm [syn: {husbandman}, {granger},
          {sodbuster}]
     2: United States civil rights leader who in 1942 founded the
        Congress of Racial Equality (born in 1920) [syn: {James
        Leonard Farmer}]
     3: an expert on cooking whose cookbook has undergone many
        editions (1857-1915) [syn: {Fannie Farmer}, {Fannie
        Merritt Farmer}]
