---
version: 1
type: video
provider: YouTube
id: QQhkEGp-YQQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Factoring%20Trinomials%20by%20Grouping%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4d172817-f460-4f28-b51c-9f4476f9cd23
updated: 1486069567
title: Factoring Trinomials by Grouping 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/QQhkEGp-YQQ/default.jpg
    - https://i3.ytimg.com/vi/QQhkEGp-YQQ/1.jpg
    - https://i3.ytimg.com/vi/QQhkEGp-YQQ/2.jpg
    - https://i3.ytimg.com/vi/QQhkEGp-YQQ/3.jpg
---
