---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-confidence
offline_file: ""
offline_thumbnail: ""
uuid: fc875172-6109-43de-9884-e762cee82274
updated: 1484310591
title: self-confidence
categories:
    - Dictionary
---
self-confidence
     n : freedom from doubt; belief in yourself and your abilities;
         "his assurance in his superiority did not make him
         popular"; "after that failure he lost his confidence";
         "she spoke with authority" [syn: {assurance}, {self-assurance},
          {confidence}, {authority}, {sureness}]
