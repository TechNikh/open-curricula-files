---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acrylic
offline_file: ""
offline_thumbnail: ""
uuid: ee6645f3-c2d0-47da-845a-af34a48463f0
updated: 1484310216
title: acrylic
categories:
    - Dictionary
---
acrylic
     n 1: polymerized from acrylonitrile [syn: {acrylic fiber}]
     2: a glassy thermoplastic; can be cast and molded or used in
        coatings and adhesives [syn: {acrylic resin}, {acrylate
        resin}]
     3: used especially by artists [syn: {acrylic paint}]
     4: a synthetic fabric
