---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Seven_Bridges_of_K%C3%B6nigsberg'
offline_file: ""
offline_thumbnail: ""
uuid: 5b6b9bc1-2f07-40fa-a38e-3e54fe8969fb
updated: 1484308625
title: Seven Bridges of Königsberg
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Konigsberg_bridges.png
tags:
    - "Euler's analysis"
    - Significance in the history of mathematics
    - Variations
    - Solutions
    - Present state of the bridges
categories:
    - Graph theory
---
The Seven Bridges of Königsberg is a historically notable problem in mathematics. Its negative resolution by Leonhard Euler in 1736 laid the foundations of graph theory and prefigured the idea of topology.[1]
