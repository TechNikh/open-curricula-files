---
version: 1
type: video
provider: YouTube
id: i05-okb1EJg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Finance%20%26%20Capital%20Markets/Housing/Finite%20geometric%20series%20word%20problem-%20mortgage.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0b9049f3-e80c-427f-a32a-8048d80f658d
updated: 1486069571
title: 'Finite geometric series word problem- mortgage'
categories:
    - Housing
thumbnail_urls:
    - https://i3.ytimg.com/vi/i05-okb1EJg/default.jpg
    - https://i3.ytimg.com/vi/i05-okb1EJg/1.jpg
    - https://i3.ytimg.com/vi/i05-okb1EJg/2.jpg
    - https://i3.ytimg.com/vi/i05-okb1EJg/3.jpg
---
Figuring out the formula for fixed mortgage payments using the sum of a geometric series. Created by Sal Khan.

Watch the next lesson: 
https://www.khanacademy.org/economics-finance-domain/core-finance/housing/home-buying-process/v/titles-and-deeds-in-real-estate?utm_source=YT&utm_medium=Desc&utm_campaign=financeandcapitalmarkets

Missed the previous lesson? Watch here: https://www.khanacademy.org/economics-finance-domain/core-finance/housing/mortgages-tutorial/v/balloon-payment-mortgage?utm_source=YT&utm_medium=Desc&utm_campaign=financeandcapitalmarkets

Finance and capital markets on Khan Academy: Most people buying a home need a mortgage to do so. This tutorial explains what a mortgage is and then actually does some math to figure out what your payments are (the last video is quite mathy so consider it optional).

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Finance and Capital Markets channel: https://www.youtube.com/channel/UCQ1Rt02HirUvBK2D2-ZO_2g?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
