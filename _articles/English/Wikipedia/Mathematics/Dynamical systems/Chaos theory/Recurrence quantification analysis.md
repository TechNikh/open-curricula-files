---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Recurrence_quantification_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 0a38d9c6-39b4-4c47-9bb8-3810d09df11c
updated: 1484308707
title: Recurrence quantification analysis
tags:
    - Background
    - RQA measures
    - Time-dependent RQA
    - Example
categories:
    - Chaos theory
---
Recurrence quantification analysis (RQA) is a method of nonlinear data analysis (cf. chaos theory) for the investigation of dynamical systems. It quantifies the number and duration of recurrences of a dynamical system presented by its phase space trajectory.
