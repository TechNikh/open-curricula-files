---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/implementation
offline_file: ""
offline_thumbnail: ""
uuid: 6517feaf-3ae2-4e9b-84d8-769ca5e1195f
updated: 1484310464
title: implementation
categories:
    - Dictionary
---
implementation
     n 1: the act of accomplishing some aim or executing some order;
          "the agency was created for the implementation of the
          policy" [syn: {execution}, {carrying out}]
     2: the act of implementing (providing a practical means for
        accomplishing something); carrying into effect [syn: {effectuation}]
