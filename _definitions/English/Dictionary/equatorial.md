---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equatorial
offline_file: ""
offline_thumbnail: ""
uuid: f8c72fd9-aba2-466d-8755-ed6158a17789
updated: 1484310273
title: equatorial
categories:
    - Dictionary
---
equatorial
     adj 1: of or relating to or at an equator; "equatorial diameter"
     2: of or relating to conditions at the geographical equator;
        "equatorial heat"
     3: of or existing at or near the geographic equator;
        "equatorial Africa" [ant: {polar}]
