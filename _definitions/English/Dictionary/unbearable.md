---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unbearable
offline_file: ""
offline_thumbnail: ""
uuid: e293be52-e766-48ec-905b-19f254227312
updated: 1484310561
title: unbearable
categories:
    - Dictionary
---
unbearable
     adj : impossible to bear; "unbearable pain"; "unendurable agony"
           [syn: {unendurable}]
