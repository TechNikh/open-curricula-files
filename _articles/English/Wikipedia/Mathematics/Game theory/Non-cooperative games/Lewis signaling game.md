---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lewis_signaling_game
offline_file: ""
offline_thumbnail: ""
uuid: 5d2ebd37-42a5-4c68-91ef-9f9b52167a10
updated: 1484309057
title: Lewis signaling game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Signaling_Game.svg_0.png
categories:
    - Non-cooperative games
---
In game theory, the Lewis signaling game is a type of signaling game that features perfect common interest between players. It is named for the philosopher David Lewis who was the first to discuss this game in his Ph.D. dissertation, and later book, Convention.[1]
