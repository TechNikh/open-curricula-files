---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Concurrence_(quantum_computing)
offline_file: ""
offline_thumbnail: ""
uuid: 84bf8236-9b0c-4a2b-90a3-3bd00824574c
updated: 1484308674
title: Concurrence (quantum computing)
tags:
    - Definition
    - Other formulations
    - Properties
categories:
    - Theoretical computer science
---
