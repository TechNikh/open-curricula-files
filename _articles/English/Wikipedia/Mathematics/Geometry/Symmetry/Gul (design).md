---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gul_(design)
offline_file: ""
offline_thumbnail: ""
uuid: fb4c1feb-6d89-4a0a-8a43-a2fc129395fb
updated: 1484309311
title: Gul (design)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Konya_18th_carpet_with_Memling_gul_design.jpg
tags:
    - Shape
    - Etymology
    - Usage
    - In Western culture
categories:
    - Symmetry
---
A gul (also written gol, göl and gül) is a medallion-like design element typical of traditional hand-woven carpets from Central and West Asia. In Turkmen weavings they are often repeated to form the pattern in the main field.
