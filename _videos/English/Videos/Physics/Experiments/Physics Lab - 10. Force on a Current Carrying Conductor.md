---
version: 1
type: video
provider: YouTube
id: M44nBel5dRI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 40ec5ffd-c511-48c1-9125-758d8aba4430
updated: 1486069640
title: 'Physics Lab - 10. Force on a Current Carrying Conductor'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/M44nBel5dRI/default.jpg
    - https://i3.ytimg.com/vi/M44nBel5dRI/1.jpg
    - https://i3.ytimg.com/vi/M44nBel5dRI/2.jpg
    - https://i3.ytimg.com/vi/M44nBel5dRI/3.jpg
---
