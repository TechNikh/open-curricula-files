---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_chess
offline_file: ""
offline_thumbnail: ""
uuid: 116e3189-6025-49e7-a550-b0f2d7cf6b28
updated: 1484309004
title: Computer chess
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-RS_Chess_Computer.JPG
tags:
    - Availability
    - Computers versus humans
    - Implementation issues
    - Board representations
    - Search techniques
    - Leaf evaluation
    - Using endgame databases
    - Other optimizations
    - Playing strength versus computer speed
    - Other chess software
    - Notable theorists
    - Solving chess
    - Chronology
    - Chess engines
    - Notes
categories:
    - Game artificial intelligence
---
Computer chess is a game of computer architecture encompassing hardware and software capable of playing chess autonomously without human guidance. Computer chess acts as solo entertainment (allowing players to practice and to better themselves when no sufficiently strong human opponents are available), as aids to chess analysis, for computer chess competitions, and as research to provide insights into human cognition.
