---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cooling_curve
offline_file: ""
offline_thumbnail: ""
uuid: 016d63a7-27e7-442f-981a-9a5e2cb4834b
updated: 1484308834
title: Cooling curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cooling_curve.png
categories:
    - Thermodynamics
---
A cooling curve is a line graph that represents the change of phase of matter, typically from a gas to a solid or a liquid to a solid. The independent variable (X-axis) is time and the dependent variable (Y-axis) is temperature.[1] Below is an example of a cooling curve used in castings.
