---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_coloring
offline_file: ""
offline_thumbnail: ""
uuid: b2cf88f9-13d2-4d0a-abb6-c27f1b8f20f9
updated: 1484308599
title: Graph coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-3-coloringEx.svg.png
tags:
    - History
    - Definition and terminology
    - Vertex coloring
    - Chromatic polynomial
    - Edge coloring
    - Total coloring
    - Unlabeled coloring
    - Properties
    - Bounds on the chromatic number
    - Lower bounds on the chromatic number
    - Graphs with high chromatic number
    - Bounds on the chromatic index
    - Other properties
    - Open problems
    - Algorithms
    - Polynomial time
    - Exact algorithms
    - Contraction
    - Greedy coloring
    - Parallel and distributed algorithms
    - Decentralized algorithms
    - Computational complexity
    - Applications
    - Scheduling
    - Register allocation
    - Other applications
    - Other colorings
    - Ramsey theory
    - Other colorings
    - Notes
categories:
    - Graph theory
---
In graph theory, graph coloring is a special case of graph labeling; it is an assignment of labels traditionally called "colors" to elements of a graph subject to certain constraints. In its simplest form, it is a way of coloring the vertices of a graph such that no two adjacent vertices share the same color; this is called a vertex coloring. Similarly, an edge coloring assigns a color to each edge so that no two adjacent edges share the same color, and a face coloring of a planar graph assigns a color to each face or region so that no two faces that share a boundary have the same color.
