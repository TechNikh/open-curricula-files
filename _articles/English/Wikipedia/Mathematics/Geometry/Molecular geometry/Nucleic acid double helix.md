---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nucleic_acid_double_helix
offline_file: ""
offline_thumbnail: ""
uuid: b1ed6722-7bed-41c0-9bfc-fc0b453b0a4d
updated: 1484309279
title: Nucleic acid double helix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-DNA_orbit_animated_static_thumb_0.png
tags:
    - History
    - Nucleic acid hybridization
    - Base pair geometry
    - Helix geometries
    - Grooves
    - Non-double helical forms
    - Bending
    - Persistence length, axial stiffness
    - Models for DNA bending
    - Bending preference
    - Circularization
    - Stretching
    - Supercoiling and topology
    - The linking number paradox
categories:
    - Molecular geometry
---
In molecular biology, the term double helix[1] refers to the structure formed by double-stranded molecules of nucleic acids such as DNA. The double helical structure of a nucleic acid complex arises as a consequence of its secondary structure, and is a fundamental component in determining its tertiary structure. The term entered popular culture with the publication in 1968 of The Double Helix: A Personal Account of the Discovery of the Structure of DNA, by James Watson.
