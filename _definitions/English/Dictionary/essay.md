---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/essay
offline_file: ""
offline_thumbnail: ""
uuid: aab0d544-e6e0-40b7-9b3c-37196bc05b46
updated: 1484310286
title: essay
categories:
    - Dictionary
---
essay
     n 1: an analytic or interpretive literary composition
     2: a tentative attempt
     v 1: make an effort or attempt; "He tried to shake off his
          fears"; "The infant had essayed a few wobbly steps";
          "The police attempted to stop the thief"; "He sought to
          improve himself"; "She always seeks to do good in the
          world" [syn: {try}, {seek}, {attempt}, {assay}]
     2: put to the test, as for its quality, or give experimental
        use to; "This approach has been tried with good results";
        "Test this recipe" [syn: {test}, {prove}, {try}, {try out},
         {examine}]
