---
version: 1
type: video
provider: YouTube
id: PX47ZXn04mY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6d59a209-e97d-4881-a825-4a23070d7c56
updated: 1486069639
title: Experiment to Find Refraction index in a Glass Block
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/PX47ZXn04mY/default.jpg
    - https://i3.ytimg.com/vi/PX47ZXn04mY/1.jpg
    - https://i3.ytimg.com/vi/PX47ZXn04mY/2.jpg
    - https://i3.ytimg.com/vi/PX47ZXn04mY/3.jpg
---
