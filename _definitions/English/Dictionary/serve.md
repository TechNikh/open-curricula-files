---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serve
offline_file: ""
offline_thumbnail: ""
uuid: aa5aea3d-59b4-4e54-9569-6f43eb46daaf
updated: 1484310256
title: serve
categories:
    - Dictionary
---
serve
     n : (sports) a stroke that puts the ball in play; "his powerful
         serves won the game" [syn: {service}]
     v 1: serve a purpose, role, or function; "The tree stump serves
          as a table"; "The female students served as a control
          group"; "This table would serve very well"; "His freedom
          served him well"; "The table functions as a desk" [syn:
          {function}]
     2: do duty or hold offices; serve in a specific function; "He
        served as head of the department for three years"; "She
        served in Congress for two terms"
     3: contribute or conduce to; "The scandal served to increase
        his popularity"
     4: be used by; as of ...
