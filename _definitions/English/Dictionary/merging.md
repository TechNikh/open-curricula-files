---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/merging
offline_file: ""
offline_thumbnail: ""
uuid: 972ecd31-cb5d-4c82-aee8-1f9b0bf748ea
updated: 1484310591
title: merging
categories:
    - Dictionary
---
merging
     adj 1: combining or mixing [syn: {blending}, {mingling}]
     2: flowing together [syn: {confluent}, {merging(a)}]
     n 1: the act of joining together as one; "the merging of the two
          groups occurred quickly"; "there was no meeting of
          minds" [syn: {meeting}, {coming together}]
     2: a flowing together [syn: {confluence}, {conflux}]
