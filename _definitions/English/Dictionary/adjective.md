---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjective
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465581
title: adjective
categories:
    - Dictionary
---
adjective
     adj 1: of or relating to or functioning as an adjective;
            "adjectival syntax"; "an adjective clause" [syn: {adjectival}]
     2: applying to methods of enforcement and rules of procedure;
        "adjective law" [syn: {procedural}] [ant: {substantive}]
     n 1: a word that expresses an attribute of something
     2: the word class that qualifies nouns
