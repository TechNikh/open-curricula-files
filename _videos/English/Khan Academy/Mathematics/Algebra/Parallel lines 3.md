---
version: 1
type: video
provider: YouTube
id: y5yNi08cr6I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Parallel%20lines%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0733abd6-b8fd-45f4-ac8f-b65719e1639f
updated: 1486069557
title: Parallel lines 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/y5yNi08cr6I/default.jpg
    - https://i3.ytimg.com/vi/y5yNi08cr6I/1.jpg
    - https://i3.ytimg.com/vi/y5yNi08cr6I/2.jpg
    - https://i3.ytimg.com/vi/y5yNi08cr6I/3.jpg
---
