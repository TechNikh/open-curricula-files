---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impure
offline_file: ""
offline_thumbnail: ""
uuid: 55c20a87-2743-43b5-a724-a57df8de8cca
updated: 1484310411
title: impure
categories:
    - Dictionary
---
impure
     adj 1: combined with extraneous elements [ant: {pure}]
     2: used of persons or behaviors; not morally pure; "impure
        thoughts" [ant: {pure}]
     3: ritually unclean or impure; "and the swine...is unclean to
        you"-Leviticus 11:3 [syn: {unclean}] [ant: {clean}]
