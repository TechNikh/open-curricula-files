---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Poset_game
offline_file: ""
offline_thumbnail: ""
uuid: 2b39e691-ed6e-4520-ad9e-cbe83be25384
updated: 1484308528
title: Poset game
tags:
    - Game play
    - Examples
    - Grundy value
    - Strategy stealing
    - Complexity
categories:
    - Combinatorial game theory
---
In combinatorial game theory, poset games are mathematical games of strategy, generalizing many well-known games such as Nim and Chomp.[1] In such games, two players start with a poset (a partially ordered set), and take turns choosing one point in the poset, removing it and all points that are greater. The player who is left with no point to choose, loses.
