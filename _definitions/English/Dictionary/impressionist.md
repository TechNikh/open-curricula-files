---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impressionist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484607961
title: impressionist
categories:
    - Dictionary
---
impressionist
     adj : relating to or characteristic of impressionism;
           "impressionist music" [syn: {impressionistic}]
     n : a painter who follows the theories of impressionism
