---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/universal
offline_file: ""
offline_thumbnail: ""
uuid: 32aa707e-1bcf-4d6d-8abf-8462eec8d093
updated: 1484310381
title: universal
categories:
    - Dictionary
---
universal
     adj : of worldwide scope or applicability; "an issue of
           cosmopolitan import"; "the shrewdest political and
           ecumenical comment of our time"- Christopher Morley;
           "universal experience" [syn: {cosmopolitan}, {ecumenical},
            {oecumenical}, {general}, {worldwide}]
     n : coupling that connects two rotating shafts allowing freedom
         of movement in all directions; "in motor vehicles a
         universal joint allows the driveshaft to move up and down
         as the vehicle passes over bumps" [syn: {universal joint}]
