---
version: 1
type: video
provider: YouTube
id: m6lMGoZTJnc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Apsidal%20precession%20%28perihelion%20precession%29%20and%20Milankovitch%20cycles.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9dc41421-2de8-452f-814a-8e8d8d64150e
updated: 1486069686
title: >
    Apsidal precession (perihelion precession) and Milankovitch
    cycles
categories:
    - Cosmology and Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/m6lMGoZTJnc/default.jpg
    - https://i3.ytimg.com/vi/m6lMGoZTJnc/1.jpg
    - https://i3.ytimg.com/vi/m6lMGoZTJnc/2.jpg
    - https://i3.ytimg.com/vi/m6lMGoZTJnc/3.jpg
---
