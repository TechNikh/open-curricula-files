---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/format
offline_file: ""
offline_thumbnail: ""
uuid: 355cae5b-16ba-4cb2-b008-44a6ad68183e
updated: 1484310522
title: format
categories:
    - Dictionary
---
format
     n 1: the organization of information according to preset
          specifications (usually for computer processing) [syn: {formatting},
           {data format}, {data formatting}]
     2: the general appearance of a publication
     v 1: set (printed matter) into a specific format; "Format this
          letter so it can be printed out" [syn: {arrange}]
     2: determine the arrangement of (data) for storage and display
        (in computer science)
     3: divide (a disk) into marked sectors so that it may store
        data; "Please format this disk before entering data!"
        [syn: {initialize}, {initialise}]
     [also: {formatting}, {formatted}]
