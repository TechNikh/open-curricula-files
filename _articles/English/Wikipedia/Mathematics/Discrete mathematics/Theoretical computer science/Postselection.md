---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Postselection
offline_file: ""
offline_thumbnail: ""
uuid: f5615b55-ead5-49f9-8a64-f06709f669c9
updated: 1484308675
title: Postselection
categories:
    - Theoretical computer science
---
In probability theory, to postselect is to condition a probability space upon the occurrence of a given event. In symbols, once we postselect for an event E, the probability of some other event F changes from Pr[F] to the conditional probability Pr[F|E].
