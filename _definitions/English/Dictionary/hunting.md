---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hunting
offline_file: ""
offline_thumbnail: ""
uuid: 9850ba1d-636b-49a9-87d7-d6e8fd1286be
updated: 1484310246
title: hunting
categories:
    - Dictionary
---
hunting
     n 1: the pursuit and killing or capture of wild animals regarded
          as a sport [syn: {hunt}]
     2: the activity of looking thoroughly in order to find
        something or someone [syn: {search}, {hunt}]
     3: the work of finding and killing or capturing animals for
        food or pelts [syn: {hunt}]
