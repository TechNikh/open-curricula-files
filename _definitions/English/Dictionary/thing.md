---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thing
offline_file: ""
offline_thumbnail: ""
uuid: e388ff88-d37a-4ff8-86a5-c6f6c994038e
updated: 1484310268
title: thing
categories:
    - Dictionary
---
thing
     n 1: a special situation; "this thing has got to end"; "it is a
          remarkable thing"
     2: an action; "how could you do such a thing?"
     3: an artifact; "how does this thing work?"
     4: an event; "a funny thing happened on the way to the..."
     5: a statement regarded as an object; "to say the same thing in
        other terms"; "how can you say such a thing?"
     6: any attribute or quality considered as having its own
        existence; "the thing I like about her is ..."
     7: a special abstraction; "a thing of the spirit"; "things of
        the heart"
     8: a vaguely specified concern; "several matters to attend to";
        "it is none of your affair"; ...
