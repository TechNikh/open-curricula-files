---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pentagon
offline_file: ""
offline_thumbnail: ""
uuid: 63dfa367-14a7-4787-b6f9-eddfc056a41f
updated: 1484310172
title: pentagon
categories:
    - Dictionary
---
Pentagon
     n 1: a government building with five sides that serves as the
          headquarters of the United States Department of Defense
     2: the United States military establishment
     3: a five-sided polygon
