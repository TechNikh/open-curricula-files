---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kodaira_embedding_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 23863a29-ef90-4cf2-9a04-a3e72cedd1e1
updated: 1484309365
title: Kodaira embedding theorem
categories:
    - Geometry stubs
---
In mathematics, the Kodaira embedding theorem characterises non-singular projective varieties, over the complex numbers, amongst compact Kähler manifolds. In effect it says precisely which complex manifolds are defined by homogeneous polynomials.
