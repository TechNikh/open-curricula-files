---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solemn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484611261
title: solemn
categories:
    - Dictionary
---
solemn
     adj 1: dignified and somber in manner or character and committed to
            keeping promises; "a grave God-fearing man"; "a quiet
            sedate nature"; "as sober as a judge"; "a solemn
            promise"; "the judge was solemn as he pronounced
            sentence" [syn: {grave}, {sedate}, {sober}]
     2: characterized by a firm and humorless belief in the validity
        of your opinions; "both sides were deeply in earnest, even
        passionate"; "an entirely sincere and cruel tyrant"; "a
        film with a solemn social message" [syn: {earnest}, {in
        earnest(p)}, {sincere}]
