---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tutte_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 9d68481b-4157-41ea-bbef-2ecdd3c11afa
updated: 1484309429
title: Tutte theorem
tags:
    - "Tutte's theorem"
    - Proof
    - Notes
categories:
    - Matching
---
In the mathematical discipline of graph theory the Tutte theorem, named after William Thomas Tutte, is a characterization of graphs with perfect matchings. It is a generalization of Hall's marriage theorem from bipartite to arbitrary graphs and is a special case of the Tutte–Berge formula.
