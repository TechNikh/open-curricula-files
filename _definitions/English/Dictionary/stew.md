---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stew
offline_file: ""
offline_thumbnail: ""
uuid: 1a343b47-31ee-468e-8538-723f073060bc
updated: 1484310313
title: stew
categories:
    - Dictionary
---
stew
     n 1: agitation resulting from active worry; "don't get in a
          stew"; "he's in a sweat about exams" [syn: {fret}, {sweat},
           {lather}, {swither}]
     2: food prepared by stewing especially meat or fish with
        vegetables
     v 1: be in a huff; be silent or sullen [syn: {grizzle}, {brood}]
     2: bear a grudge; harbor ill feelings [syn: {grudge}]
     3: cook slowly and for a long time in liquid; "Stew the
        vegetables in wine"
