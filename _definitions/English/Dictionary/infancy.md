---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infancy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484646841
title: infancy
categories:
    - Dictionary
---
infancy
     n 1: the early stage of growth or development [syn: {babyhood}, {early
          childhood}]
     2: the earliest state of immaturity [syn: {babyhood}]
