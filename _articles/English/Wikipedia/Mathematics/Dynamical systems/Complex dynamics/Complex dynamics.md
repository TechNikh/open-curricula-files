---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_dynamics
offline_file: ""
offline_thumbnail: ""
uuid: 3dabf64b-65a2-40b4-9736-52d0cc72ac68
updated: 1484308732
title: Complex dynamics
tags:
    - 'Techniques[1]'
    - Parts
    - Notes
categories:
    - Complex dynamics
---
Complex dynamics is the study of dynamical systems defined by iteration of functions on complex number spaces. Complex analytic dynamics is the study of the dynamics of specifically analytic functions.
