---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypergraph_grammar
offline_file: ""
offline_thumbnail: ""
uuid: 4b633e4f-1893-4acb-9e0a-20b2a6c51c8e
updated: 1484309452
title: Hypergraph grammar
categories:
    - Graph rewriting
---
In computer science, a hypergraph grammar (also a hyperedge replacement system) is a class of graph grammar with hyperedges rather than simple edges. Hypergraphs prove to be a particularly fruitful class of graph to treat for applications of replacement systems; in particular they model typical data structures very naturally.
