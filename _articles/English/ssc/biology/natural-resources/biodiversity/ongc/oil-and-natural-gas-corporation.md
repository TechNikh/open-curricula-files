---
title: Oil and Natural Gas Corporation
article_ref: https://en.wikipedia.org/wiki/Oil_and_Natural_Gas_Corporation
tags:
    - History
    - Foundation to 1961
    - 1961 to 2000
    - 2000 to present
    - Operations
    - Subsidiaries
    - Joint Ventures
    - Products and services
    - Listings and Shareholding
    - Employees
    - Awards and recognitions
categories:
    - ONGC
---
