---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jerusalem_Cube
offline_file: ""
offline_thumbnail: ""
uuid: c7c50117-10cd-44e1-9e43-d43caf336013
updated: 1484309365
title: Jerusalem Cube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Jerusalem_emblem.png
categories:
    - Geometry stubs
---
In mathematics, a Jerusalem Cube is a fractal object described by Eric Baird in 2011. It is created by recursively drilling Greek cross-shaped holes into a cube.[1] The name comes from a face of the cube resembling a Jerusalem cross pattern.
