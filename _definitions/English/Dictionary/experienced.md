---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experienced
offline_file: ""
offline_thumbnail: ""
uuid: a96bd938-b8c2-4543-8232-53d6a0381a72
updated: 1484310349
title: experienced
categories:
    - Dictionary
---
experienced
     adj : having become knowledgeable or skillful from observation or
           participation [ant: {inexperienced}]
