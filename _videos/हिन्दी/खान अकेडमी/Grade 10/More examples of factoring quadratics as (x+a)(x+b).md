---
version: 1
type: video
provider: YouTube
id: PGVzQGcJTzE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7f0e1318-bd7b-49dc-914e-ead79ae9e342
updated: 1486069619
title: More examples of factoring quadratics as (x+a)(x+b)
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/PGVzQGcJTzE/default.jpg
    - https://i3.ytimg.com/vi/PGVzQGcJTzE/1.jpg
    - https://i3.ytimg.com/vi/PGVzQGcJTzE/2.jpg
    - https://i3.ytimg.com/vi/PGVzQGcJTzE/3.jpg
---
