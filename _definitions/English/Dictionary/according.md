---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/according
offline_file: ""
offline_thumbnail: ""
uuid: 1ec6b7d7-7548-4ccb-81d9-677f1386a6c2
updated: 1484310305
title: according
categories:
    - Dictionary
---
according
     adj 1: (followed by `to') as reported or stated by; "according to
            historians"
     2: (followed by `to') in agreement with or accordant with;
        "according to instructions"
