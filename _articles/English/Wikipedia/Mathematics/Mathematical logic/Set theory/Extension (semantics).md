---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extension_(semantics)
offline_file: ""
offline_thumbnail: ""
uuid: c24c237c-a02f-499a-a8da-813957711c09
updated: 1484309504
title: Extension (semantics)
tags:
    - Mathematics
    - Computer science
    - Metaphysical implications
    - General semantics
categories:
    - Set theory
---
In any of several studies that treat the use of signs—for example, in linguistics, logic, mathematics, semantics, and semiotics—the extension of a concept, idea, or sign consists of the things to which it applies, in contrast with its comprehension or intension, which consists very roughly of the ideas, properties, or corresponding signs that are implied or suggested by the concept in question.
