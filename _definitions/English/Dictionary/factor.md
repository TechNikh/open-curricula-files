---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/factor
offline_file: ""
offline_thumbnail: ""
uuid: 7729b96b-1090-46c9-a110-f626687cc921
updated: 1484310299
title: factor
categories:
    - Dictionary
---
factor
     n 1: anything that contributes causally to a result; "a number of
          factors determined the outcome"
     2: an abstract part of something; "jealousy was a component of
        his character"; "two constituents of a musical composition
        are melody and harmony"; "the grammatical elements of a
        sentence"; "a key factor in her success"; "humor: an
        effective ingredient of a speech" [syn: {component}, {constituent},
         {element}, {ingredient}]
     3: any of the numbers (or symbols) that form a product when
        multiplied together
     4: one of two or more integers that can be exactly divided into
        another integer; "what are the 4 ...
