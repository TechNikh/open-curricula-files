---
version: 1
type: video
provider: YouTube
id: ey_b3aPsRl8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Simplifying%20rational%20expressions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20310f32-85bc-4da6-b2ad-bed3a2df2739
updated: 1486069565
title: Simplifying rational expressions 3
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/ey_b3aPsRl8/default.jpg
    - https://i3.ytimg.com/vi/ey_b3aPsRl8/1.jpg
    - https://i3.ytimg.com/vi/ey_b3aPsRl8/2.jpg
    - https://i3.ytimg.com/vi/ey_b3aPsRl8/3.jpg
---
