---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/merger
offline_file: ""
offline_thumbnail: ""
uuid: 2134b177-6d3f-4609-a8e1-57420aa7849a
updated: 1484310605
title: merger
categories:
    - Dictionary
---
merger
     n 1: the combination of two or more commercial companies [syn: {amalgamation},
           {uniting}]
     2: an occurrence that involves the production of a union [syn:
        {fusion}, {unification}]
