---
version: 1
type: article
id: https://en.wikipedia.org/wiki/(G,X)-manifold
offline_file: ""
offline_thumbnail: ""
uuid: 62558007-a5f1-4346-a0c4-ae2235435a75
updated: 1484309200
title: (G,X)-manifold
tags:
    - Definition and examples
    - Formal definition
    - Riemannian examples
    - Pseudo-Riemannian examples
    - Other examples
    - Developing map and completeness
    - Developing map
    - Monodromy
    - Complete (G,X)-structures
    - Examples
    - Riemannian (G,X)-structures
    - Other cases
    - (G,X)-structures as connections
    - Notes
categories:
    - Differential geometry
---
If X is a manifold with an action of a topological group G by analytical diffeomorphisms, the notion of a (G, X)-structure on a topological space is a way to formalise it being locally isomorphic to X with its G-invariant structure; spaces with a (G, X)-structures are always manifolds and are called (G, X)-manifolds. This notion is often used with G being a Lie group and X a homogeneous space for G. Foundational examples are hyperbolic manifolds and affine manifolds.
