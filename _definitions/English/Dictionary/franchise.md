---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/franchise
offline_file: ""
offline_thumbnail: ""
uuid: 14078abc-ea5b-41fd-b769-a7e9645fa0cb
updated: 1484310591
title: franchise
categories:
    - Dictionary
---
franchise
     n 1: an authorization to sell a company's goods or services in a
          particular place
     2: a business established or operated under an authorization to
        sell or distribute a company's goods or services in a
        particular area [syn: {dealership}]
     3: a statutory right or privilege granted to a person or group
        by a government (especially the rights of citizenship and
        the right to vote) [syn: {enfranchisement}]
     v : grant a franchise to
