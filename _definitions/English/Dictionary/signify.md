---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/signify
offline_file: ""
offline_thumbnail: ""
uuid: f3cf3aaa-6383-41b1-a55e-9e69fb07dca2
updated: 1484310384
title: signify
categories:
    - Dictionary
---
signify
     v 1: denote or connote; "`maison' means `house' in French"; "An
          example sentence would show what this word means" [syn:
          {mean}, {intend}, {stand for}]
     2: convey or express a meaning; "These words mean nothing to
        me!"; "What does his strange behavior signify?"
     3: make known with a word or signal; "He signified his wish to
        pay the bill for our meal"
     [also: {signified}]
