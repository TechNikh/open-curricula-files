---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helicopter
offline_file: ""
offline_thumbnail: ""
uuid: b79eeb8e-b5a5-4ffd-aec7-d805418dcdb4
updated: 1484310539
title: helicopter
categories:
    - Dictionary
---
helicopter
     n : an aircraft without wings that obtains its lift from the
         rotation of overhead blades [syn: {chopper}, {whirlybird},
          {eggbeater}]
