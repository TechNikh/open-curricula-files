---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unusable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484466301
title: unusable
categories:
    - Dictionary
---
unusable
     adj 1: impossible to use [syn: {unserviceable}, {unuseable}]
     2: not able to perform its normal function [syn: {inoperable}]
