---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry
offline_file: ""
offline_thumbnail: ""
uuid: c31154c2-0261-4057-88d2-b145d2ce48ee
updated: 1484309304
title: Symmetry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Asymmetric_%2528PSF%2529.svg.png'
tags:
    - In mathematics
    - In geometry
    - In logic
    - Other areas of mathematics
    - In science and nature
    - In physics
    - In biology
    - In chemistry
    - In social interactions
    - In the arts
    - In architecture
    - In pottery and metal vessels
    - In quilts
    - In carpets and rugs
    - In music
    - Musical form
    - Pitch structures
    - Equivalency
    - In other arts and crafts
    - In aesthetics
    - In literature
    - Notes
categories:
    - Symmetry
---
Symmetry (from Greek συμμετρία symmetria "agreement in dimensions, due proportion, arrangement")[1] in everyday language refers to a sense of harmonious and beautiful proportion and balance.[2][a] In mathematics, "symmetry" has a more precise definition, that an object is invariant to any of various transformations; including reflection, rotation or scaling. Although these two meanings of "symmetry" can sometimes be told apart, they are related, so they are here discussed together.
