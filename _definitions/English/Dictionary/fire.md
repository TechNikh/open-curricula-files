---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fire
offline_file: ""
offline_thumbnail: ""
uuid: d010e802-9b5f-47e8-aa10-4da479c78c05
updated: 1484310208
title: fire
categories:
    - Dictionary
---
fire
     n 1: the event of something burning (often destructive); "they
          lost everything in the fire"
     2: the process of combustion of inflammable materials producing
        heat and light and (often) smoke; "fire was one of our
        ancestors' first discoveries" [syn: {flame}, {flaming}]
     3: the act of firing weapons or artillery at an enemy; "hold
        your fire until you can see the whites of their eyes";
        "they retreated in the face of withering enemy fire" [syn:
         {firing}]
     4: a fireplace in which a fire is burning; "they sat by the
        fire and talked"
     5: intense adverse criticism; "Clinton directed his fire at the
        ...
