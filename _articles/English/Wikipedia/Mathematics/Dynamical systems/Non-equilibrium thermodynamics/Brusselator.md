---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brusselator
offline_file: ""
offline_thumbnail: ""
uuid: 09c086d0-8c0d-4be2-bfb7-154ce84ee8ff
updated: 1484308795
title: Brusselator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Bruesselator.svg.png
categories:
    - Non-equilibrium thermodynamics
---
The Brusselator is a theoretical model for a type of autocatalytic reaction. The Brusselator model was proposed by Ilya Prigogine and his collaborators at the Université Libre de Bruxelles.[1] It is a portmanteau of Brussels and oscillator.
