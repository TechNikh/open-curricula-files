---
version: 1
type: video
provider: YouTube
id: eWaA9RiLsno
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Magnetic%20Field%20of%20a%20Point%20Charge.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a459b16d-d694-4aeb-ae11-29dc3b327a95
updated: 1486069649
title: Magnetic Field of a Point Charge
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/eWaA9RiLsno/default.jpg
    - https://i3.ytimg.com/vi/eWaA9RiLsno/1.jpg
    - https://i3.ytimg.com/vi/eWaA9RiLsno/2.jpg
    - https://i3.ytimg.com/vi/eWaA9RiLsno/3.jpg
---
