---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phenotypic
offline_file: ""
offline_thumbnail: ""
uuid: 0bc7cf82-89fa-4daa-acda-8ca6e3a209f2
updated: 1484308351
title: Phenotype
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Coquina_variation3.jpg
tags:
    - Difficulties in definition
    - Phenotypic variation
    - The Extended Phenotype
    - Phenome and phenomics
categories:
    - Uncategorized
---
A phenotype (from Greek phainein, meaning "to show", and typos, meaning "type") is the composite of an organism's observable characteristics or traits, such as its morphology, development, biochemical or physiological properties, behavior, and products of behavior (such as a bird's nest). A phenotype results from the expression of an organism's genetic code, its genotype, as well as the influence of environmental factors and the interactions between the two. When two or more clearly different phenotypes exist in the same population of a species, the species is called polymorphic.
