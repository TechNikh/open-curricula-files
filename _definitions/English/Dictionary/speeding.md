---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speeding
offline_file: ""
offline_thumbnail: ""
uuid: de759e2a-7f26-47b0-a1ff-751b2321c7ee
updated: 1484310242
title: speeding
categories:
    - Dictionary
---
speeding
     adj : moving with great speed; "the speeding car"
     n : changing location rapidly [syn: {speed}, {hurrying}]
