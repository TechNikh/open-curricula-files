---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-discipline
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619661
title: self-discipline
categories:
    - Dictionary
---
self-discipline
     n 1: the trait of practicing self discipline [syn: {self-denial}]
     2: the act of denying yourself; controlling your impulses [syn:
         {self-denial}, {self-control}]
