---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnesia
offline_file: ""
offline_thumbnail: ""
uuid: 7b11dfa1-7ede-4b32-85db-339efa87c6d9
updated: 1484310384
title: magnesia
categories:
    - Dictionary
---
magnesia
     n : a white solid mineral that occurs naturally as periclase; a
         source of magnesium [syn: {periclase}, {magnesium oxide}]
