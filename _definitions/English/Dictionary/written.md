---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/written
offline_file: ""
offline_thumbnail: ""
uuid: bc9130ac-6790-48fd-928a-419ed10fa8d8
updated: 1484310290
title: written
categories:
    - Dictionary
---
write
     v 1: produce a literary work; "She composed a poem"; "He wrote
          four novels" [syn: {compose}, {pen}, {indite}]
     2: communicate or express by writing; "Please write to me every
        week"
     3: have (one's written work) issued for publication; "How many
        books did Georges Simenon write?"; "She published 25 books
        during her long career" [syn: {publish}]
     4: communicate (with) in writing; "Write her soon, please!"
        [syn: {drop a line}]
     5: communicate by letter; "He wrote that he would be coming
        soon"
     6: write music; "Beethoven composed nine symphonies" [syn: {compose}]
     7: mark or trace on a surface; "The artist wrote ...
