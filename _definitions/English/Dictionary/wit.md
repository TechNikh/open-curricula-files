---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443921
title: wit
categories:
    - Dictionary
---
wit
     n 1: a message whose ingenuity or verbal skill or incongruity has
          the power to evoke laughter [syn: {humor}, {humour}, {witticism},
           {wittiness}]
     2: mental ability; "he's got plenty of brains but no common
        sense" [syn: {brain}, {brainpower}, {learning ability}, {mental
        capacity}, {mentality}]
     3: a witty amusing person who makes jokes [syn: {wag}, {card}]
