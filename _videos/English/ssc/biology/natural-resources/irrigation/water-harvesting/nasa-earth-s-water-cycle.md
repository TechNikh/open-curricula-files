---
title: NASA | Earth's Water Cycle
youtube_id: oaDkph9yQBs
tags:
    - nasa
    - Water cycle
    - hydrology
    - precipitation
    - snow
    - Rain
    - natural hazards
    - chlorophyll
    - Plants
categories:
    - Water harvesting
---
