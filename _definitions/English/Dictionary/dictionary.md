---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dictionary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468041
title: dictionary
categories:
    - Dictionary
---
dictionary
     n : a reference book containing an alphabetical list of words
         with information about them [syn: {lexicon}]
