---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colossus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484521141
title: colossus
categories:
    - Dictionary
---
colossus
     n 1: someone or something that is abnormally large and powerful
          [syn: {giant}, {goliath}, {behemoth}, {monster}]
     2: a person of exceptional importance and reputation [syn: {behemoth},
         {giant}, {heavyweight}, {titan}]
     [also: {colossi} (pl)]
