---
version: 1
type: video
provider: YouTube
id: aQyettA79LI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 042a38eb-f5cf-4184-9413-44b4fe719315
updated: 1486069631
title: Learn about "Wh" Questions in English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/aQyettA79LI/default.jpg
    - https://i3.ytimg.com/vi/aQyettA79LI/1.jpg
    - https://i3.ytimg.com/vi/aQyettA79LI/2.jpg
    - https://i3.ytimg.com/vi/aQyettA79LI/3.jpg
---
