---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycles_and_fixed_points
offline_file: ""
offline_thumbnail: ""
uuid: cafa8710-7fef-41b0-80e0-a85a8c059fc4
updated: 1484308782
title: Cycles and fixed points
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Gray_code_%252A_bit_reversal_16.svg.png'
tags:
    - Counting permutations by number of cycles
    - Properties
    - Reasons for properties
    - Some values
    - Counting permutations by number of fixed points
    - Properties
    - Reasons for properties
    - Some values
    - Alternate calculations
    - Notes
categories:
    - Fixed points (mathematics)
---
In mathematics, the cycles of a permutation π of a finite set S correspond bijectively to the orbits of the subgroup generated by π acting on S. These orbits are subsets of S that can be written as { c1, ..., cl }, such that
