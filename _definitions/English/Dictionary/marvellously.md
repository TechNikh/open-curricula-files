---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marvellously
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554381
title: marvellously
categories:
    - Dictionary
---
marvellously
     adv : (used as an intensifier) extremely well; "her voice is
           superbly disciplined"; "the colors changed wondrously
           slowly" [syn: {wonderfully}, {wondrous}, {wondrously},
           {superbly}, {toppingly}, {terrifically}, {marvelously}]
