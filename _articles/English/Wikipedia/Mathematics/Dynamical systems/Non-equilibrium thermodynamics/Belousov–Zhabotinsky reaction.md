---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Belousov%E2%80%93Zhabotinsky_reaction'
offline_file: ""
offline_thumbnail: ""
uuid: a674e581-2c83-4dbe-8319-3d5f9ce0d2de
updated: 1484308805
title: Belousov–Zhabotinsky reaction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-The_Belousov-Zhabotinsky_Reaction.gif
tags:
    - History
    - Chemical mechanism
    - Variants
categories:
    - Non-equilibrium thermodynamics
---
A Belousov–Zhabotinsky reaction', or BZ reaction, is one of a class of reactions that serve as a classical example of non-equilibrium thermodynamics, resulting in the establishment of a nonlinear chemical oscillator. The only common element in these oscillating is the inclusion of bromine and an acid. The reactions are important to theoretical chemistry in that they show that chemical reactions do not have to be dominated by equilibrium thermodynamic behavior. These reactions are far from equilibrium and remain so for a significant length of time and evolve chaotically. In this sense, they provide an interesting chemical model of nonequilibrium biological phenomena, and the mathematical ...
