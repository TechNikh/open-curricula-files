---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sterilisation
offline_file: ""
offline_thumbnail: ""
uuid: 98519af8-fe0d-4da1-8a7c-f5760e8d3afc
updated: 1484310609
title: sterilisation
categories:
    - Dictionary
---
sterilisation
     n 1: the act of making an organism barren or infertile (unable to
          reproduce) [syn: {sterilization}]
     2: the procedure of making some object free of live bacteria or
        other microorganisms (usually by heat or chemical means)
        [syn: {sterilization}]
