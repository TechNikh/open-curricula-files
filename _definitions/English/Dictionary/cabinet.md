---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cabinet
offline_file: ""
offline_thumbnail: ""
uuid: 3b296904-33f4-4e68-a18e-35c19f2d858c
updated: 1484310589
title: cabinet
categories:
    - Dictionary
---
cabinet
     n 1: a cupboard-like repository or piece of furniture with doors
          and shelves and drawers; for storage or display
     2: persons appointed by a head of state to head executive
        departments of government and act as official advisers
     3: a storage compartment for clothes and valuables; usually it
        has a lock [syn: {locker}, {storage locker}]
     4: housing for electronic instruments, as radio or television
        [syn: {console}]
