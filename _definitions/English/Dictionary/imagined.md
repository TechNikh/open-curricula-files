---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imagined
offline_file: ""
offline_thumbnail: ""
uuid: 063905a3-d85f-4f61-8039-5f9530bfbd8a
updated: 1484310194
title: imagined
categories:
    - Dictionary
---
imagined
     adj : not based on fact; dubious; "the falsehood about some
           fanciful secret treaties"- F.D.Roosevelt; "a small
           child's imaginary friends"; "her imagined fame"; "to
           create a notional world for oneself" [syn: {fanciful},
           {imaginary}, {notional}]
