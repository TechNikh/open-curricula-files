---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threat
offline_file: ""
offline_thumbnail: ""
uuid: 67b38e47-918e-4bb8-a04c-009178e4d672
updated: 1484310477
title: threat
categories:
    - Dictionary
---
threat
     n 1: something that is a source of danger; "earthquakes are a
          constant threat in Japan" [syn: {menace}]
     2: a warning that something unpleasant is imminent; "they were
        under threat of arrest"
     3: declaration of an intention or a determination to inflict
        harm on another; "his threat to kill me was quite
        explicit"
     4: a person who inspires fear or dread; "he was the terror of
        the neighborhood" [syn: {terror}, {scourge}]
