---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lethargy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485201
title: lethargy
categories:
    - Dictionary
---
lethargy
     n 1: a state of comatose torpor (as found in sleeping sickness)
          [syn: {lassitude}, {sluggishness}]
     2: weakness characterized by a lack of vitality or energy [syn:
         {inanition}, {lassitude}]
     3: inactivity; showing an unusual lack of energy [syn: {languor},
         {sluggishness}, {phlegm}]
