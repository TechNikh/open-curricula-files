---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homological_mirror_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: caa44397-1f34-4be7-8772-c107f5e47d30
updated: 1484309223
title: Homological mirror symmetry
tags:
    - History
    - Examples
    - Hodge diamond
categories:
    - Differential geometry
---
Homological mirror symmetry is a mathematical conjecture made by Maxim Kontsevich. It seeks a systematic mathematical explanation for a phenomenon called mirror symmetry first observed by physicists studying string theory.
