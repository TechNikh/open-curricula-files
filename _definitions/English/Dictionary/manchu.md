---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manchu
offline_file: ""
offline_thumbnail: ""
uuid: 4fea5eba-f028-4f23-a1da-130b9f03bf34
updated: 1484310569
title: manchu
categories:
    - Dictionary
---
Manchu
     n 1: a member of the Manchu speaking people of Mongolian race of
          Manchuria; related to the Tungus; conquered China in the
          17th century
     2: the last imperial dynasty of China (from 1644 to 1912) which
        was overthrown by revolutionaries; during the Qing dynasty
        China was ruled by the Manchu [syn: {Qing}, {Qing dynasty},
         {Ch'ing}, {Ch'ing dynasty}, {Manchu dynasty}]
     3: the Tungusic language spoken by the Manchu people
