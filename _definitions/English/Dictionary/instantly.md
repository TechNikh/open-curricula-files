---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instantly
offline_file: ""
offline_thumbnail: ""
uuid: 253bf769-1983-4ae7-97d1-b1aa2af4c109
updated: 1484310527
title: instantly
categories:
    - Dictionary
---
instantly
     adv 1: without delay or hesitation; with no time intervening; "he
            answered immediately"; "found an answer straightaway";
            "an official accused of dishonesty should be suspended
            forthwith"; "Come here now!" [syn: {immediately}, {straightaway},
             {straight off}, {directly}, {now}, {right away}, {at
            once}, {forthwith}, {in real time}, {like a shot}]
     2: without any delay; "he was killed outright" [syn: {instantaneously},
         {outright}, {in a flash}]
