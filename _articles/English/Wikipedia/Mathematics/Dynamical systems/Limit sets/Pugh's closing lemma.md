---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pugh%27s_closing_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: a241e743-ccb8-41a0-ac46-da2f077005c2
updated: 1484308797
title: "Pugh's closing lemma"
categories:
    - Limit sets
---
In mathematics, Pugh's closing lemma is a result that links periodic orbit solutions of differential equations to chaotic behaviour. It can be formally stated as follows:
