---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfect_power
offline_file: ""
offline_thumbnail: ""
uuid: f2da3a2d-9888-449b-b389-fbb9c13a4f15
updated: 1484308652
title: Perfect power
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Perfect_power_number_Cuisenaire_rods_9.png
tags:
    - Examples and sums
    - Detecting perfect powers
    - Gaps between perfect powers
    - Calculation by recursion for positive integers
categories:
    - Number theory
---
In mathematics, a perfect power is a positive integer that can be expressed as an integer power of another positive integer. More formally, n is a perfect power if there exist natural numbers m > 1, and k > 1 such that mk = n. In this case, n may be called a perfect kth power. If k = 2 or k = 3, then n is called a perfect square or perfect cube, respectively. Sometimes 1 is also considered a perfect power (1k = 1 for any k).
