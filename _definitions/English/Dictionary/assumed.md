---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assumed
offline_file: ""
offline_thumbnail: ""
uuid: dca639b4-e1f2-498d-8cb9-bbeee7f98e2c
updated: 1484310305
title: assumed
categories:
    - Dictionary
---
assumed
     adj 1: accepted as real or true without proof; "an assumed increase
            in population"; "the assumed reason for his absence";
            "assumptive beliefs"; "his loyalty was taken for
            granted" [syn: {assumptive}, {taken for granted(p)}]
     2: taken as your right without justification; "was hearing
        evidence in an assumed capacity"; "Congress's arrogated
        powers over domains hitherto belonging to the states"
        [syn: {arrogated}]
     3: adopted in order to deceive; "an assumed name"; "an assumed
        cheerfulness"; "a fictitious address"; "fictive sympathy";
        "a pretended interest"; "a put-on childish voice"; "sham
        ...
