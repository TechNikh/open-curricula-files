---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filter
offline_file: ""
offline_thumbnail: ""
uuid: 4f86624e-2f94-43cd-8e73-d5e743980b34
updated: 1484310383
title: filter
categories:
    - Dictionary
---
filter
     n 1: device that removes something from whatever passes through
          it
     2: an electrical device that alters the frequency spectrum of
        signals passing through it
     v 1: remove by passing through a filter; "filter out the
          impurities" [syn: {filtrate}, {strain}, {separate out},
          {filter out}]
     2: pass through; "Water permeates sand easily" [syn: {percolate},
         {sink in}, {permeate}]
     3: run or flow slowly, as in drops or in an unsteady stream;
        "water trickled onto the lawn from the broken hose";
        "reports began to dribble in" [syn: {trickle}, {dribble}]
