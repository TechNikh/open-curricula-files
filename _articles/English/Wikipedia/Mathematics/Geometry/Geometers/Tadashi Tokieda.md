---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tadashi_Tokieda
offline_file: ""
offline_thumbnail: ""
uuid: 9ff9f3bf-a73e-4dfa-88a8-d89791a8dcc2
updated: 1484309110
title: Tadashi Tokieda
tags:
    - Life and career
    - Selected publications
categories:
    - Geometers
---
Tadashi Tokieda (in Japanese: 時枝 正) is a Japanese mathematician, working in mathematical physics. He is the Director of Studies in Mathematics[2] at Trinity Hall, Cambridge. He is also very active in inventing, collecting, and studying toys.[3] In comparison with most mathematicians, he had an unusual path in life: he started as a painter, and then became a classical philologist, before switching to mathematics.[4]
