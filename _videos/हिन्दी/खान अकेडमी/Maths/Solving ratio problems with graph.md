---
version: 1
type: video
provider: YouTube
id: Z0fdZOgMEqY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e167f94f-a12b-4920-837d-0fbf95062053
updated: 1486069609
title: Solving ratio problems with graph
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z0fdZOgMEqY/default.jpg
    - https://i3.ytimg.com/vi/Z0fdZOgMEqY/1.jpg
    - https://i3.ytimg.com/vi/Z0fdZOgMEqY/2.jpg
    - https://i3.ytimg.com/vi/Z0fdZOgMEqY/3.jpg
---
