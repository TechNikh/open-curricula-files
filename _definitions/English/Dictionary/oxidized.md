---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidized
offline_file: ""
offline_thumbnail: ""
uuid: 2c2ee9a3-417a-4fe2-bab7-7f0b0ec56db0
updated: 1484310319
title: oxidized
categories:
    - Dictionary
---
oxidized
     adj : combined with or having undergone a chemical reaction with
           oxygen; "the oxidized form of iodine" [syn: {oxidised}]
