---
version: 1
type: video
provider: YouTube
id: E-3yQqgu8OA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Magnetic%20Torque.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a5bb78f9-bd53-40c2-9e53-6bdfee3fbcb9
updated: 1486069652
title: Magnetic Torque
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/E-3yQqgu8OA/default.jpg
    - https://i3.ytimg.com/vi/E-3yQqgu8OA/1.jpg
    - https://i3.ytimg.com/vi/E-3yQqgu8OA/2.jpg
    - https://i3.ytimg.com/vi/E-3yQqgu8OA/3.jpg
---
