---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Five_points_determine_a_conic
offline_file: ""
offline_thumbnail: ""
uuid: ecd06973-a803-441c-9448-47a0b102f119
updated: 1484309116
title: Five points determine a conic
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parabola_construction_given_five_points.gif
tags:
    - Proofs
    - Dimension counting
    - Synthetic proof
    - Construction
    - Generalizations
    - Related results
    - Tangency
categories:
    - Theorems in geometry
---
In Euclidean, non-projective geometry, just as two (distinct) points determine a line (a degree-1 plane curve), five points determine a conic (a degree-2 plane curve). There are additional subtleties for conics that do not exist for lines, and thus the statement and its proof for conics are both more technical than for lines. In projective geometry, a line is defined via three points, all families of circles are conics passing through two points of a line at infinity, and coordinates are the basic conic objects of study, and are defined as all conics passing through the line and two points at infinity.
