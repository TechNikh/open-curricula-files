---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/definition
offline_file: ""
offline_thumbnail: ""
uuid: 458d00e4-69a3-4d0f-9c3f-32d3325d25e0
updated: 1484310234
title: definition
categories:
    - Dictionary
---
definition
     n 1: a concise explanation of the meaning of a word or phrase or
          symbol
     2: clarity of outline; "exercise had give his muscles superior
        definition"
