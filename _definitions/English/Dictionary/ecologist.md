---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ecologist
offline_file: ""
offline_thumbnail: ""
uuid: 7d7db6b5-c2f5-4180-8b46-481452dffbd6
updated: 1484310273
title: ecologist
categories:
    - Dictionary
---
ecologist
     n : a biologist who studies the relation between organisms and
         their environment
