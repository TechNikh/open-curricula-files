---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Herbert_Gr%C3%B6tzsch'
offline_file: ""
offline_thumbnail: ""
uuid: 40e55e24-ca18-4bd8-9f26-2593a9fc3541
updated: 1484309463
title: Herbert Grötzsch
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Groetzsch_Tietz.jpg
categories:
    - Graph theorists
---
Camillo Herbert Grötzsch (21 May 1902 – 15 May 1993) was a German mathematician. He was born in Döbeln and died in Halle. Grötzsch worked in graph theory. He was the discoverer and eponym of the Grötzsch graph, a triangle-free graph that requires four colors in any graph coloring, and Grötzsch's theorem, the result that every triangle-free planar graph requires at most three colors. He also introduced the concept of a quasiconformal mapping. He was one of Paul Koebe's students.
