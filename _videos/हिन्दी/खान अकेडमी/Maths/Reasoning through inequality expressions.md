---
version: 1
type: video
provider: YouTube
id: t8UtiDkActA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 725b54e2-df31-43d4-bfca-bf6898c7c82f
updated: 1486069607
title: Reasoning through inequality expressions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/t8UtiDkActA/default.jpg
    - https://i3.ytimg.com/vi/t8UtiDkActA/1.jpg
    - https://i3.ytimg.com/vi/t8UtiDkActA/2.jpg
    - https://i3.ytimg.com/vi/t8UtiDkActA/3.jpg
---
