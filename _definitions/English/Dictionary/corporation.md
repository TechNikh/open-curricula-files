---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corporation
offline_file: ""
offline_thumbnail: ""
uuid: 8103d1db-7f47-44ca-99a8-d9bc47b9334e
updated: 1484310234
title: corporation
categories:
    - Dictionary
---
corporation
     n 1: a business firm whose articles of incorporation have been
          approved in some state [syn: {corp}]
     2: slang terms for a paunch [syn: {pot}, {potbelly}, {bay
        window}, {tummy}]
