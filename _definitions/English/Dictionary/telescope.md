---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telescope
offline_file: ""
offline_thumbnail: ""
uuid: 1616d027-bf0b-451f-93f3-72c824577553
updated: 1484310206
title: telescope
categories:
    - Dictionary
---
telescope
     n : a magnifier of images of distant objects [syn: {scope}]
     v 1: crush together or collapse; "In the accident, the cars
          telescoped"; "my hiking sticks telescope and can be put
          into the backpack"
     2: make smaller or shorter; "the novel was telescoped into a
        short play"
