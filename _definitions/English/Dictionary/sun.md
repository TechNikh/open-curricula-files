---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sun
offline_file: ""
offline_thumbnail: ""
uuid: 05f1f521-736f-4dff-9b9a-32c8e52e9157
updated: 1484310277
title: sun
categories:
    - Dictionary
---
sun
     n 1: a typical star that is the source of light and heat for the
          planets in the solar system; "the sun contains 99.85% of
          the mass in the solar system"
     2: the rays of the sun; "the shingles were weathered by the sun
        and wind" [syn: {sunlight}, {sunshine}]
     3: a person considered as a source of warmth or energy or glory
        etc
     4: any star around which a planetary system evolves
     5: first day of the week; observed as a day of rest and worship
        by most Christians [syn: {Sunday}, {Lord's Day}, {Dominicus}]
     v 1: expose one's body to the sun [syn: {sunbathe}]
     2: expose to the rays of the sun or affect by exposure to the
 ...
