---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_redrawing
offline_file: ""
offline_thumbnail: ""
uuid: 87ee4771-5501-4302-863f-90ae74bc373d
updated: 1484309242
title: Parallel redrawing
categories:
    - Geometric graph theory
---
In geometric graph theory, and the theory of structural rigidity, a parallel redrawing of a graph drawing with straight edges in the Euclidean plane or higher-dimensional Euclidean space is another drawing of the same graph such that all edges of the second drawing are parallel to their corresponding edges in the first drawing. A parallel morph of a graph is a continuous family of drawings, all parallel redrawings of each other.
