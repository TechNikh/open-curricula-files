---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-dimensional_gas
offline_file: ""
offline_thumbnail: ""
uuid: 66028273-35af-4010-82eb-95be871c22da
updated: 1484308808
title: Two-dimensional gas
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Cyclotron_patent.png
tags:
    - Classical mechanics
    - Electron gas
    - Later applications to Bose gas
    - Experimental research with a molecular gas
    - Implications for future research
categories:
    - Non-equilibrium thermodynamics
---
A two-dimensional gas is a collection of objects constrained to move in a planar or other two-dimensional space in a gaseous state. The objects can be: ideal gas elements such as rigid disks undergoing elastic collisions; elementary particles, or any object in physics which obeys laws of motion. The concept of a two-dimensional gas is used either because:
