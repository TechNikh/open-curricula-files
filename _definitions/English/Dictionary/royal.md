---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/royal
offline_file: ""
offline_thumbnail: ""
uuid: f63939f2-1a69-4a8e-9d28-b3cc51fa224f
updated: 1484310194
title: royal
categories:
    - Dictionary
---
royal
     adj 1: of or relating to or indicative of or issued or performed by
            a king or queen or other monarch; "the royal party";
            "the royal crest"; "by royal decree"; "a royal visit"
     2: established or chartered or authorized by royalty; "the
        Royal Society"
     3: being of the rank of a monarch; "of royal ancestry";
        "princes of the blood royal"
     4: belonging to or befitting a supreme ruler; "golden age of
        imperial splendor"; "purple tyrant"; "regal attire";
        "treated with royal acclaim"; "the royal carriage of a
        stag's head" [syn: {imperial}, {majestic}, {purple}, {regal}]
     5: invested with royal power as ...
