---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Compactness_measure_of_a_shape
offline_file: ""
offline_thumbnail: ""
uuid: a98be8be-3a8e-4306-95a5-cbc038cf1992
updated: 1484309242
title: Compactness measure of a shape
tags:
    - Properties
    - Examples
    - Applications
categories:
    - Geometric measurement
---
The compactness measure of a shape, sometimes called the shape factor, is a numerical quantity representing the degree to which a shape is compact. The meaning of "compact" here is not related to the topological notion of compact space.
