---
version: 1
type: video
provider: YouTube
id: U6Re4xT0o4w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Divergence%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3aef6450-9819-4be1-8f82-c5daedf53295
updated: 1486069690
title: Divergence 3
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/U6Re4xT0o4w/default.jpg
    - https://i3.ytimg.com/vi/U6Re4xT0o4w/1.jpg
    - https://i3.ytimg.com/vi/U6Re4xT0o4w/2.jpg
    - https://i3.ytimg.com/vi/U6Re4xT0o4w/3.jpg
---
