---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pebble_motion_problems
offline_file: ""
offline_thumbnail: ""
uuid: 2404003d-482e-463d-9e26-8d571eaaf7cb
updated: 1484309398
title: Pebble motion problems
tags:
    - Theoretical formulation
    - Variations
    - Complexity
categories:
    - Computational problems in graph theory
---
The pebble motion problems, or pebble motion on graphs, are a set of related problems in graph theory dealing with the movement of multiple objects ("pebbles") from vertex to vertex in a graph with a constraint on the number of pebbles that can occupy a vertex at any time. Pebble motion problems occur in domains such as multi-robot motion planning (in which the pebbles are robots) and network routing (in which the pebbles are packets of data). The best-known example of a pebble motion problem is the famous 15 puzzle where a disordered group of fifteen tiles must be rearranged within a 4x4 grid by sliding one tile at a time.
