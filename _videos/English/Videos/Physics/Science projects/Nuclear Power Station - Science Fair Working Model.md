---
version: 1
type: video
provider: YouTube
id: qEX46TV6Ji0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 613205da-3d14-43db-8f58-0faa7bd3e11c
updated: 1486069634
title: 'Nuclear Power Station - Science Fair Working Model'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/qEX46TV6Ji0/default.jpg
    - https://i3.ytimg.com/vi/qEX46TV6Ji0/1.jpg
    - https://i3.ytimg.com/vi/qEX46TV6Ji0/2.jpg
    - https://i3.ytimg.com/vi/qEX46TV6Ji0/3.jpg
---
