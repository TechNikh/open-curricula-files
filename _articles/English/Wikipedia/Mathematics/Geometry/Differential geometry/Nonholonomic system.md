---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonholonomic_system
offline_file: ""
offline_thumbnail: ""
uuid: 797ce3cf-34b8-47f3-abd0-5f272f4154a4
updated: 1484309231
title: Nonholonomic system
tags:
    - History
    - Constraints
    - Examples
    - Foucault pendulum
    - Rolling sphere
    - Linear polarized light in an optical fiber
    - Robotics
categories:
    - Differential geometry
---
A nonholonomic system in physics and mathematics is a system whose state depends on the path taken in order to achieve it. Such a system is described by a set of parameters subject to differential constraints, such that when the system evolves along a path in its parameter space (the parameters varying continuously in values) but finally returns to the original set in values at the start of the path, the system itself may not have returned to its original state.
