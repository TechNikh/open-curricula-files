---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/world-class
offline_file: ""
offline_thumbnail: ""
uuid: 75eb3224-f4a8-49e1-a9da-125a55287d26
updated: 1484310537
title: world-class
categories:
    - Dictionary
---
world-class
     adj : ranking above all others; "was first in her class"; "the
           foremost figure among marine artists"; "the top
           graduate" [syn: {first}, {foremost}]
