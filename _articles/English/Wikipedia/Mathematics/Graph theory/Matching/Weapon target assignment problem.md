---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Weapon_target_assignment_problem
offline_file: ""
offline_thumbnail: ""
uuid: 72c20467-2f5b-4de7-8afe-30431b4d32fc
updated: 1484309429
title: Weapon target assignment problem
tags:
    - Formal mathematical definition
    - Algorithms and generalizations
    - Example
categories:
    - Matching
---
The weapon target assignment problem (WTA) is a class of combinatorial optimization problems present in the fields of optimization and operations research. It consists of finding an optimal assignment of a set of weapons of various types to a set of targets in order to maximize the total expected damage done to the opponent.
