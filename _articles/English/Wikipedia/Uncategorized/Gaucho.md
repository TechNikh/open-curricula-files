---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gaucho
offline_file: ""
offline_thumbnail: ""
uuid: 44ca3843-da42-4662-8935-3b518c61be4d
updated: 1484308471
title: Gaucho
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Gaucho1868b.jpg
tags:
    - Etymology
    - 'Essential attribute: the horse'
    - culture
    - Modern influences
    - In popular culture
    - Gallery
    - Bibliography
categories:
    - Uncategorized
---
Gaucho (Spanish: [ˈɡautʃo]) or gaúcho (Portuguese: [ɡaˈuʃo]) is a word with several meanings. In its historical sense a gaucho was one "who, in the 18th and 19th centuries, inhabited Argentina, Uruguay and Rio Grande do Sul.[1] Today, in Argentina and Uruguay, a gaucho is simply "A country person, experienced in traditional cattle ranching work".[2] Because historical gauchos were reputed to be brave, if unruly, the word is also applied metaphorically to mean "Noble, brave and generous",[3] but also "One who is skilful in subtle tricks, crafty".[4] In Portuguese the word gaúcho (note the accent) means "An inhabitant of the plains of Rio Grande do Sul or the pampas of Argentina ...
