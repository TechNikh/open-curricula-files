---
version: 1
type: video
provider: YouTube
id: VSG0C3v6r8c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3ab5eab3-cfd4-41b2-a4be-d4a12174e5cb
updated: 1486069635
title: 'Airwedge  Experimental set up'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/VSG0C3v6r8c/default.jpg
    - https://i3.ytimg.com/vi/VSG0C3v6r8c/1.jpg
    - https://i3.ytimg.com/vi/VSG0C3v6r8c/2.jpg
    - https://i3.ytimg.com/vi/VSG0C3v6r8c/3.jpg
---
