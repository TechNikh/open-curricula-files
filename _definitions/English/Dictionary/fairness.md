---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fairness
offline_file: ""
offline_thumbnail: ""
uuid: 4b6766bd-98cd-40ce-96b8-deb50aa6f989
updated: 1484310464
title: fairness
categories:
    - Dictionary
---
fairness
     n 1: conformity with rules or standards; "the judge recognized
          the fairness of my claim" [syn: {equity}] [ant: {unfairness},
           {unfairness}]
     2: ability to make judgments free from discrimination or
        dishonesty [syn: {fair-mindedness}, {candor}, {candour}]
        [ant: {unfairness}]
     3: the property of having a naturally light complexion [syn: {paleness},
         {blondness}]
     4: the quality of being good looking and attractive [syn: {comeliness},
         {loveliness}, {beauteousness}]
