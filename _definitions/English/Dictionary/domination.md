---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/domination
offline_file: ""
offline_thumbnail: ""
uuid: 981db42c-f774-49ae-b567-f012cb6b199f
updated: 1484310531
title: domination
categories:
    - Dictionary
---
domination
     n 1: social control by dominating
     2: power to dominate or defeat; "mastery of the seas" [syn: {mastery},
         {supremacy}]
