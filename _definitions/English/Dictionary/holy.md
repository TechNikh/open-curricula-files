---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holy
offline_file: ""
offline_thumbnail: ""
uuid: 17bd9f33-e030-4f6b-aa85-78cf0900c021
updated: 1484310178
title: holy
categories:
    - Dictionary
---
holy
     adj : belonging to or derived from or associated with a divine
           power [ant: {unholy}]
     n : a sacred place of pilgrimage [syn: {holy place}, {sanctum}]
     [also: {holiest}, {holier}]
