---
version: 1
type: video
provider: YouTube
id: sxbPwl_KRuA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Cosmic%20background%20radiation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f183f8ca-4e10-4318-9d96-30e8b4e580af
updated: 1486069688
title: Cosmic background radiation
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/sxbPwl_KRuA/default.jpg
    - https://i3.ytimg.com/vi/sxbPwl_KRuA/1.jpg
    - https://i3.ytimg.com/vi/sxbPwl_KRuA/2.jpg
    - https://i3.ytimg.com/vi/sxbPwl_KRuA/3.jpg
---
