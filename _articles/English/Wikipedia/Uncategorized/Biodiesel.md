---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biodiesel
offline_file: ""
offline_thumbnail: ""
uuid: b08e3c51-f526-4568-bef1-58fc5a6c28b7
updated: 1484308331
title: Biodiesel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Soybeanbus.jpg
tags:
    - Blends
    - Applications
    - Distribution
    - Vehicular use and manufacturer acceptance
    - Railway usage
    - Aircraft use
    - As a heating oil
    - Cleaning oil spills
    - Biodiesel in generators
    - Historical background
    - Properties
    - Fuel efficiency
    - Combustion
    - emissions
    - Material compatibility
    - Technical standards
    - Low temperature gelling
    - Contamination by water
    - Availability and prices
    - Production
    - Production levels
    - Biodiesel feedstocks
    - Quantity of feedstocks required
    - Yield
    - Efficiency and economic arguments
    - Economic impact
    - Energy security
    - Global biofuel policies
    - Canada
    - United States
    - European Union
    - Environmental effects
    - Food, land and water vs. fuel
    - Current research
    - Algal biodiesel
    - Pongamia
    - jatropha
    - Fungi
    - Biodiesel from used coffee grounds
    - Exotic sources
    - Biodiesel to hydrogen-cell power
    - Concerns
    - Engine wear
    - Fuel viscosity
    - Engine performance
    - Notes
categories:
    - Uncategorized
---
Biodiesel refers to a vegetable oil - or animal fat-based diesel fuel consisting of long-chain alkyl (methyl, ethyl, or propyl) esters. Biodiesel is typically made by chemically reacting lipids (e.g., vegetable oil, soybean oil,[1] animal fat (tallow[2][3])) with an alcohol producing fatty acid esters.
