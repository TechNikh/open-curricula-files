---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perseverance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410741
title: perseverance
categories:
    - Dictionary
---
perseverance
     n 1: persistent determination [syn: {doggedness}, {persistence},
          {persistency}, {tenacity}, {tenaciousness}, {pertinacity}]
     2: the act of persisting or persevering; continuing or
        repeating behavior; "his perseveration continued to the
        point where it was no longer appropriate" [syn: {persistence},
         {perseveration}]
