---
version: 1
type: video
provider: YouTube
id: kJSOqlcFpJw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Becoming%20a%20red%20giant.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9d86ec2c-0ef8-4ec6-aecd-a5fe24961e3a
updated: 1486069685
title: Becoming a red giant
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/kJSOqlcFpJw/default.jpg
    - https://i3.ytimg.com/vi/kJSOqlcFpJw/1.jpg
    - https://i3.ytimg.com/vi/kJSOqlcFpJw/2.jpg
    - https://i3.ytimg.com/vi/kJSOqlcFpJw/3.jpg
---
