---
version: 1
type: video
provider: YouTube
id: TcZK_A_wcWQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/David%20Bartel%20%28Whitehead%20Institute-MIT-HHMI%29%20Part%202-%20MicroRNAs-%20Regulation%20by%20Mammalian%20microRNAs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 057a125e-21ca-4548-a10b-72dd0686bc1f
updated: 1486069672
title: 'David Bartel (Whitehead Institute/MIT/HHMI) Part 2- MicroRNAs- Regulation by Mammalian microRNAs'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TcZK_A_wcWQ/default.jpg
    - https://i3.ytimg.com/vi/TcZK_A_wcWQ/1.jpg
    - https://i3.ytimg.com/vi/TcZK_A_wcWQ/2.jpg
    - https://i3.ytimg.com/vi/TcZK_A_wcWQ/3.jpg
---
