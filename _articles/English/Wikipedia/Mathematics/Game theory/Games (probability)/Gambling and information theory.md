---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Gambling_and_information_theory
offline_file: ""
offline_thumbnail: ""
uuid: 6ec334fe-0209-4f65-89cf-15eb03945343
updated: 1484309040
title: Gambling and information theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Sandebits.png
tags:
    - Kelly Betting
    - Side information
    - Doubling rate
    - Expected gains
    - Applications for self-information
    - Applications in games of chance
categories:
    - Games (probability)
---
Statistical inference might be thought of as gambling theory applied to the world around us. The myriad applications for logarithmic information measures tell us precisely how to take the best guess in the face of partial information.[1] In that sense, information theory might be considered a formal expression of the theory of gambling. It is no surprise, therefore, that information theory has applications to games of chance.[2]
