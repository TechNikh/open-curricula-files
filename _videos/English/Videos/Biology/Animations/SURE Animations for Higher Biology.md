---
version: 1
type: video
provider: YouTube
id: Lr94Hws90UE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b4e2e584-fcb9-4a66-af13-e2d65bb0df7a
updated: 1486069668
title: SURE Animations for Higher Biology
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Lr94Hws90UE/default.jpg
    - https://i3.ytimg.com/vi/Lr94Hws90UE/1.jpg
    - https://i3.ytimg.com/vi/Lr94Hws90UE/2.jpg
    - https://i3.ytimg.com/vi/Lr94Hws90UE/3.jpg
---
