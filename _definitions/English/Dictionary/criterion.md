---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/criterion
offline_file: ""
offline_thumbnail: ""
uuid: 396ec84a-df28-4d3b-a610-4a8ca22cb0e0
updated: 1484310445
title: criterion
categories:
    - Dictionary
---
criterion
     n 1: a basis for comparison; a reference point against which
          other things can be evaluated; "they set the measure for
          all subsequent work" [syn: {standard}, {measure}, {touchstone}]
     2: the ideal in terms of which something can be judged; "they
        live by the standards of their community" [syn: {standard}]
     [also: {criteria} (pl)]
