---
version: 1
type: video
provider: YouTube
id: 1dl65UNzoF0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 80351603-e6af-4a39-a324-501a714cf49a
updated: 1486069635
title: 'Electromagnetism Physics Project - Mr. Troy 6/12'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/1dl65UNzoF0/default.jpg
    - https://i3.ytimg.com/vi/1dl65UNzoF0/1.jpg
    - https://i3.ytimg.com/vi/1dl65UNzoF0/2.jpg
    - https://i3.ytimg.com/vi/1dl65UNzoF0/3.jpg
---
