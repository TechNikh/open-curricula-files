---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Codd%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 31becc8f-0efc-4c4e-ad5c-6551b9112b1f
updated: 1484308994
title: "Codd's theorem"
categories:
    - Theorems in the foundations of mathematics
---
Codd's theorem states that relational algebra and the domain-independent relational calculus queries, two well-known foundational query languages for the relational model, are precisely equivalent in expressive power. That is, a database query can be formulated in one language if and only if it can be expressed in the other.
