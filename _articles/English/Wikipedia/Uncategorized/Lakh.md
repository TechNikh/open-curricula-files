---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lakh
offline_file: ""
offline_thumbnail: ""
uuid: bd420775-c094-488e-a50b-8c6531657878
updated: 1484308447
title: Lakh
tags:
    - Usage
    - money
    - Precious metals market
    - Etymology and regional variants
    - South Asian languages
categories:
    - Uncategorized
---
A lakh (/ˈlæk/ or /ˈlɑːk/; abbreviated L; sometimes written Lac[1] or Lacs) is a unit in the Indian numbering system equal to one hundred thousand (100,000; scientific notation: 105). [2][1][3] In the Indian convention of digit grouping, it is written as 1,00,000. For example, in India 150,000 rupees becomes 1.5 lakh rupees, written as ₹1,50,000 or INR 1,50,000.
