---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restless
offline_file: ""
offline_thumbnail: ""
uuid: 0fcccebb-ce95-49c9-b132-37bf70ab2c5a
updated: 1484310587
title: restless
categories:
    - Dictionary
---
restless
     adj 1: unable to relax or be still; "a constant fretful stamping of
            hooves"; "itchy for excitement"; "a restless child"
            [syn: {fidgety}, {fretful}, {itchy}]
     2: worried and uneasy [syn: {ungratified}, {unsatisfied}]
     3: ceaselessly in motion; "the restless sea"; "the restless
        wind"
     4: marked by a lack of quiet; not conducive to rest; "spent a
        restless night"; "fell into an uneasy sleep" [syn: {uneasy}]
