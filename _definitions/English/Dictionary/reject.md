---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reject
offline_file: ""
offline_thumbnail: ""
uuid: 29600e3a-732b-44d1-b909-1877ef6746ef
updated: 1484310593
title: reject
categories:
    - Dictionary
---
reject
     n : the person or thing rejected or set aside as inferior in
         quality [syn: {cull}]
     v 1: refuse to accept or acknowledge; "I reject the idea of
          starting a war"; "The journal rejected the student's
          paper" [ant: {accept}]
     2: refuse to accept; "He refused my offer of hospitality" [syn:
         {refuse}, {pass up}, {turn down}, {decline}] [ant: {accept}]
     3: deem wrong or inappropriate; "I disapprove of her child
        rearing methods" [syn: {disapprove}] [ant: {approve}]
     4: reject with contempt; "She spurned his advances" [syn: {spurn},
         {freeze off}, {scorn}, {pooh-pooh}, {disdain}, {turn down}]
     5: resist ...
