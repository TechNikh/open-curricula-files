---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/labelled
offline_file: ""
offline_thumbnail: ""
uuid: a65127aa-f78a-4d97-82eb-e87da4849dfe
updated: 1484310198
title: labelled
categories:
    - Dictionary
---
labelled
     adj : bearing or marked with a label or tag; "properly labeled
           luggage" [syn: {labeled}, {tagged}] [ant: {unlabeled}]
