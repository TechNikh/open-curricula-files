---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sones_GraphDB
offline_file: ""
offline_thumbnail: ""
uuid: 403f3d40-4430-4314-9280-3c542e3fd166
updated: 1484309408
title: Sones GraphDB
tags:
    - Functionality
    - Interfaces
    - REST API
    - Traverser API
    - Architecture
categories:
    - Graph databases
---
Sones GraphDB was a graph database developed by the German company sones GmbH, available from 2010 to 2012. Its last version was released in May 2011. sones GmbH, which was based in Erfurt and Leipzig, was declared bankrupt on January 1, 2012.
