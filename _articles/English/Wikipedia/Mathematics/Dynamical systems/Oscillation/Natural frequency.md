---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_frequency
offline_file: ""
offline_thumbnail: ""
uuid: ccda131f-fd8f-462c-9ef9-31aab0d4db01
updated: 1484308822
title: Natural frequency
categories:
    - Oscillation
---
Free vibrations of an elastic body are called natural vibrations and occur at a frequency called the natural frequency. Natural vibrations are different from forced vibrations which happen at frequency of applied force (forced frequency). If forced frequency is equal to the natural frequency, the amplitude of vibration increases manyfold. This phenomenon is known as resonance.[2]
