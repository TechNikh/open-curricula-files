---
version: 1
type: video
provider: YouTube
id: YghRP8S2K-4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Applying%20radical%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fb18c1ac-33c0-4473-9067-2873e8353d14
updated: 1486069563
title: Applying radical equations 2
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YghRP8S2K-4/default.jpg
    - https://i3.ytimg.com/vi/YghRP8S2K-4/1.jpg
    - https://i3.ytimg.com/vi/YghRP8S2K-4/2.jpg
    - https://i3.ytimg.com/vi/YghRP8S2K-4/3.jpg
---
