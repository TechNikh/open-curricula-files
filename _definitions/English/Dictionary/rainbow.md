---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rainbow
offline_file: ""
offline_thumbnail: ""
uuid: 409a8681-b609-4995-a92d-2e92be62a23a
updated: 1484310389
title: rainbow
categories:
    - Dictionary
---
rainbow
     n 1: an arc of colored light in the sky caused by refraction of
          the sun's rays by rain
     2: an illusory hope; "chasing rainbows"
