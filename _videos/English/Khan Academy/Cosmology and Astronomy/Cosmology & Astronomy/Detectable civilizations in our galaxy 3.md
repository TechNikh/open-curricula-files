---
version: 1
type: video
provider: YouTube
id: LRmwUsxNSL0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Detectable%20civilizations%20in%20our%20galaxy%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6dea60ad-9937-42f0-9b79-e7b18f879232
updated: 1486069689
title: Detectable civilizations in our galaxy 3
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/LRmwUsxNSL0/default.jpg
    - https://i3.ytimg.com/vi/LRmwUsxNSL0/1.jpg
    - https://i3.ytimg.com/vi/LRmwUsxNSL0/2.jpg
    - https://i3.ytimg.com/vi/LRmwUsxNSL0/3.jpg
---
