---
version: 1
type: video
provider: YouTube
id: W254ewkkMck
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Integer%20sums.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7d49a4c0-4f81-4e6e-afd2-a46601bc7d87
updated: 1486069569
title: Integer sums
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/W254ewkkMck/default.jpg
    - https://i3.ytimg.com/vi/W254ewkkMck/1.jpg
    - https://i3.ytimg.com/vi/W254ewkkMck/2.jpg
    - https://i3.ytimg.com/vi/W254ewkkMck/3.jpg
---
Adding sums of consecutive integers
