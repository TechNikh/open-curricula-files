---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charter
offline_file: ""
offline_thumbnail: ""
uuid: b12fa37a-cd79-4691-a05d-53d4f3fa7bfd
updated: 1484310181
title: charter
categories:
    - Dictionary
---
charter
     n 1: a document incorporating an institution and specifying its
          rights; includes the articles of incorporation and the
          certificate of incorporation
     2: a contract to hire or lease transportation
     v 1: hold under a lease or rental agreement; of goods and
          services [syn: {rent}, {hire}, {lease}]
     2: grant a charter to
     3: engage for service under a term of contract; "We took an
        apartment on a quiet street"; "Let's rent a car"; "Shall
        we take a guide in Rome?" [syn: {lease}, {rent}, {hire}, {engage},
         {take}]
