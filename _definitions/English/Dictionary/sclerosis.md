---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sclerosis
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484419681
title: sclerosis
categories:
    - Dictionary
---
sclerosis
     n : any pathological hardening or thickening of tissue [syn: {induration}]
     [also: {scleroses} (pl)]
