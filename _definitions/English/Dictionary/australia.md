---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/australia
offline_file: ""
offline_thumbnail: ""
uuid: a0ae3968-ec15-4212-b91d-369efec7f585
updated: 1484310279
title: australia
categories:
    - Dictionary
---
Australia
     n 1: a nation occupying the whole of the Australian continent;
          aboriginal tribes are thought to have migrated from
          southeastern Asia 20,000 years ago; first Europeans were
          British convicts sent there as a penal colony [syn: {Commonwealth
          of Australia}]
     2: the smallest continent; between the South Pacific and the
        Indian Ocean
