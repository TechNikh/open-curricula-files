---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leopard
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653741
title: leopard
categories:
    - Dictionary
---
leopard
     n 1: the pelt of a leopard
     2: large feline of African and Asian forests usually having a
        tawny coat with black spots [syn: {Panthera pardus}]
