---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardworking
offline_file: ""
offline_thumbnail: ""
uuid: 6a27fe69-a74a-4938-8280-0a668312a402
updated: 1484310439
title: hardworking
categories:
    - Dictionary
---
hardworking
     adj : characterized by hard work and perseverance [syn: {industrious},
            {tireless}, {untiring}]
