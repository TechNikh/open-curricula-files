---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cross_product
offline_file: ""
offline_thumbnail: ""
uuid: ae83df9f-1c33-4c46-8ba7-9348391a1f00
updated: 1484309159
title: Cross product
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cross_product_vector.svg.png
tags:
    - Definition
    - Names
    - Computing the cross product
    - Coordinate notation
    - Matrix notation
    - Properties
    - Geometric meaning
    - Algebraic properties
    - Differentiation
    - Triple product expansion
    - Alternative formulation
    - "Lagrange's identity"
    - Infinitesimal generators of rotations
    - Alternative ways to compute the cross product
    - Conversion to matrix multiplication
    - Index notation for tensors
    - Mnemonic
    - Cross visualization
    - Applications
    - Computational geometry
    - Angular momentum and torque
    - Rigid body
    - Lorentz force
    - Other
    - Cross product as an exterior product
    - Cross product and handedness
    - Generalizations
    - Lie algebra
    - Quaternions
    - Octonions
    - Wedge product
    - Multilinear algebra
    - Skew-symmetric matrix
    - History
    - Notes
categories:
    - Analytic geometry
---
In mathematics and vector calculus, the cross product or vector product (occasionally directed area product to emphasize the geometric significance) is a binary operation on two vectors in three-dimensional space (R3) and is denoted by the symbol ×. Given two linearly independent vectors a and b, the cross product, a × b, is a vector that is perpendicular to both a and b and therefore normal to the plane containing them. It has many applications in mathematics, physics, engineering, and computer programming. It should not be confused with dot product (projection product).
