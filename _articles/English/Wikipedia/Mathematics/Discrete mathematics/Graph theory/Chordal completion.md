---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chordal_completion
offline_file: ""
offline_thumbnail: ""
uuid: e7d547ea-6bde-487d-bce7-0675b9eed767
updated: 1484308611
title: Chordal completion
tags:
    - Related graph families
    - Applications
    - Computational complexity
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, a chordal completion of a given undirected graph G is a chordal graph on the same vertex set that has G as a subgraph. A minimal chordal completion is a chordal completion such that any graph formed by removing an edge would no longer be a chordal completion. The minimum chordal completion is a chordal completion with as few edges as possible.
