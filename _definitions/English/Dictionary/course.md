---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/course
offline_file: ""
offline_thumbnail: ""
uuid: a4e847cd-36f0-48ad-98d1-d907a0be04af
updated: 1484310279
title: course
categories:
    - Dictionary
---
course
     n 1: education imparted in a series of lessons or class meetings;
          "he took a course in basket weaving"; "flirting is not
          unknown in college classes" [syn: {course of study}, {course
          of instruction}, {class}]
     2: a connected series of events or actions or developments;
        "the government took a firm course"; "historians can only
        point out those lines for which evidence is available"
        [syn: {line}]
     3: facility consisting of a circumscribed area of land or water
        laid out for a sport; "the course had only nine holes";
        "the course was less than a mile"
     4: a mode of action; "if you persist in that course ...
