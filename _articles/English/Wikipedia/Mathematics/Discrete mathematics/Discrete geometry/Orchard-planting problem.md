---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orchard-planting_problem
offline_file: ""
offline_thumbnail: ""
uuid: 799ba047-ed1d-49a5-805f-2170f9ccbae5
updated: 1484308599
title: Orchard-planting problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Orchard-planting_problem.svg.png
tags:
    - Integer sequence
    - Upper and lower bounds
    - Notes
categories:
    - Discrete geometry
---
In discrete geometry, the original orchard-planting problem asks for the maximum number of 3-point lines attainable by a configuration of points in the plane. It is also called the tree-planting problem or simply the orchard problem. There are also investigations into how many k-point lines there can be. Hallard T. Croft and Paul Erdős proved tk > c n2 / k3, where n is the number of points and tk is the number of k-point lines.[1] Their construction contains some m-point lines, where m > k. One can also ask the question if these are not allowed.
