---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Outline_of_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: 90594174-988a-401a-9c43-0a3ef8187000
updated: 1484309340
title: Outline of trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/226px-Angle_Symbol.svg.png
tags:
    - Basics
    - Content of trigonometry
    - Scholars associated with the development of trigonometry
    - History of trigonometry
    - Fields that use trigonometry
    - Trigonometry in calculus
    - Lists
categories:
    - Trigonometry
---
Trigonometry – a branch of mathematics that studies the relationships between the sides and the angles in triangles. Trigonometry defines the trigonometric functions, which describe those relationships and have applicability to cyclical phenomena, such as waves.
