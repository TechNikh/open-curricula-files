---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/birth
offline_file: ""
offline_thumbnail: ""
uuid: f3c31afa-63bb-4259-b6f4-d2622dbffc69
updated: 1484310445
title: birth
categories:
    - Dictionary
---
birth
     n 1: the time when something begins (especially life); "they
          divorced after the birth of the child"; "his election
          signaled the birth of a new age" [ant: {death}]
     2: the event of being born; "they celebrated the birth of their
        first child" [syn: {nativity}, {nascency}, {nascence}]
        [ant: {death}]
     3: the process of giving birth [syn: {parturition}, {giving
        birth}, {birthing}]
     4: the kinship relation of an offspring to the parents [syn: {parentage}]
     v : give birth (to a newborn); "My wife had twins yesterday!"
         [syn: {give birth}, {deliver}, {bear}, {have}]
