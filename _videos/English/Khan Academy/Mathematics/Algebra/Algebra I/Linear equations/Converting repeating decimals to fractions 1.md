---
version: 1
type: video
provider: YouTube
id: FPNhCVZlbJs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Converting%20repeating%20decimals%20to%20fractions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b4b9afe8-d205-45c0-93ba-85d9e2f8e8c6
updated: 1486069575
title: Converting repeating decimals to fractions 1
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/FPNhCVZlbJs/default.jpg
    - https://i3.ytimg.com/vi/FPNhCVZlbJs/1.jpg
    - https://i3.ytimg.com/vi/FPNhCVZlbJs/2.jpg
    - https://i3.ytimg.com/vi/FPNhCVZlbJs/3.jpg
---
Examples of how to convert basic repeating decimals to fractions

Practice this lesson yourself on KhanAcademy.org right now:
https://www.khanacademy.org/math/algebra/solving-linear-equations-and-inequalities/conv_rep_decimals/e/converting_repeating_decimals_to_fractions_1?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Watch the next lesson: https://www.khanacademy.org/math/algebra/solving-linear-equations-and-inequalities/conv_rep_decimals/v/coverting-repeating-decimals-to-fractions-2?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra/solving-linear-equations-and-inequalities/solving_for_variable/v/manipulating-formula-example?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Algebra I on Khan Academy: Algebra is the language through which we describe patterns. Think of it as a shorthand, of sorts. As opposed to having to do something over and over again, algebra gives you a simple way to express that repetitive process. It's also seen as a "gatekeeper" subject. Once you achieve an understanding of algebra, the higher-level math subjects become accessible to you. Without it, it's impossible to move forward. It's used by people with lots of different jobs, like carpentry, engineering, and fashion design. In these tutorials, we'll cover a lot of ground. Some of the topics include linear equations, linear inequalities, linear functions, systems of equations, factoring expressions, quadratic expressions, exponents, functions, and ratios.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra channel:
https://www.youtube.com/channel/UCYZrCV8PNENpJt36V0kd-4Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
