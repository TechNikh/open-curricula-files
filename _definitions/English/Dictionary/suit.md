---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suit
offline_file: ""
offline_thumbnail: ""
uuid: 625d61d4-9116-499c-82d5-f358ccda5b69
updated: 1484310275
title: suit
categories:
    - Dictionary
---
suit
     n 1: a comprehensive term for any proceeding in a court of law
          whereby an individual seeks a legal remedy; "the family
          brought suit against the landlord" [syn: {lawsuit}, {case},
           {cause}, {causa}]
     2: a set of garments (usually including a jacket and trousers
        or skirt) for outerwear all of the same fabric and color;
        "they buried him in his best suit" [syn: {suit of clothes}]
     3: playing card in any of four sets of 13 cards in a pack; each
        set has its own symbol and color; "a flush is five cards
        in the same suit"; "in bridge you must follow suit"; "what
        suit is trumps?"
     4: a businessman dressed in a ...
