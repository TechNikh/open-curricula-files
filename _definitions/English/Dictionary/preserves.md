---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preserves
offline_file: ""
offline_thumbnail: ""
uuid: 3f5c3616-5a5f-4d57-97f4-c01fd5af33f9
updated: 1484310238
title: preserves
categories:
    - Dictionary
---
preserves
     n : fruit preserved by cooking with sugar [syn: {conserve}, {preserve},
          {conserves}]
