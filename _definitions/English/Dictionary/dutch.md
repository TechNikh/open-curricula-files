---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dutch
offline_file: ""
offline_thumbnail: ""
uuid: 3024f066-6d36-46a2-bdc0-d49dd7b97ff3
updated: 1484310555
title: dutch
categories:
    - Dictionary
---
Dutch
     adj : of or relating to the Netherlands or its people or culture;
           "Dutch painting"; "Dutch painters"
     n 1: the people of the Netherlands; "the Dutch are famous for
          their tulips" [syn: {Dutch people}]
     2: the West Germanic language of the Netherlands
