---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honourable
offline_file: ""
offline_thumbnail: ""
uuid: 14a0a013-6392-47b2-83ba-253a101cdfc6
updated: 1484310595
title: honourable
categories:
    - Dictionary
---
honourable
     adj 1: showing or characterized by honor and integrity; "an
            honorable man"; "led an honorable life"; "honorable
            service to his country" [syn: {honorable}] [ant: {dishonorable}]
     2: used as a title of respect; "my honorable colleague"; "our
        worthy commanding officer" [syn: {honorable}]
     3: adhering to ethical and moral principles; "it seems ethical
        and right"; "followed the only honorable course of
        action"; "had the moral courage to stand alone" [syn: {ethical},
         {honorable}, {moral}]
