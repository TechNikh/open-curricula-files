---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Killer_heuristic
offline_file: ""
offline_thumbnail: ""
uuid: 06aca1d2-088c-45d3-b5ff-b2bf26c8affa
updated: 1484309002
title: Killer heuristic
categories:
    - Game artificial intelligence
---
In competitive two-player games, the killer heuristic is a technique for improving the efficiency of alpha-beta pruning, which in turn improves the efficiency of the minimax algorithm. This algorithm has an exponential search time to find the optimal next move, so general methods for speeding it up are very useful.
