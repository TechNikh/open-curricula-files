---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clifford_analysis
offline_file: ""
offline_thumbnail: ""
uuid: debc57e4-dfcd-4de7-b15c-1f1ba6dfbd98
updated: 1484309207
title: Clifford analysis
tags:
    - Euclidean space
    - The Fourier transform
    - Conformal structure
    - Cayley transform (stereographic projection)
    - Möbius transform
    - The Atiyah–Singer–Dirac operator
    - Hyperbolic Dirac type operators
    - Rarita–Schwinger/Stein–Weiss operators
categories:
    - Differential geometry
---
Clifford analysis, using Clifford algebras named after William Kingdon Clifford, is the study of Dirac operators, and Dirac type operators in analysis and geometry, together with their applications. Examples of Dirac type operators include, but are not limited to, the Hodge–Dirac operator, 
  
    
      
        d
        +
        ∗
        d
        ∗
      
    
    {\displaystyle d+*d*}
  
 on a Riemannian manifold, the Dirac operator in euclidean space and its inverse on 
  
    
      
        
          C
          
            0
          
          
            ∞
          
        
        (
        
          
            R
          
          
            n
          
 ...
