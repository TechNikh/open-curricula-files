---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Dot_product_representation_of_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: 68ba7a13-eb65-436f-a022-c110d0446db7
updated: 1484308614
title: Dot product representation of a graph
categories:
    - Graph theory
---
A dot product representation of a simple graph is a method of representing a graph using vector spaces and the dot product from linear algebra. Every graph has a dot product representation.[1][2][3]
