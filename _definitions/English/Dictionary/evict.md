---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evict
offline_file: ""
offline_thumbnail: ""
uuid: 34b89920-3d02-4376-b78b-f617b287e351
updated: 1484310477
title: evict
categories:
    - Dictionary
---
evict
     v 1: expel or eject without recourse to legal process; "The
          landlord wanted to evict the tenants so he banged on the
          pipes every morning at 3 a.m."
     2: expel from one's property or force to move out by a legal
        process; "The landlord evicted the tenants after they had
        not paid the rent for four months" [syn: {force out}]
