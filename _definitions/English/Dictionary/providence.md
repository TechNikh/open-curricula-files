---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/providence
offline_file: ""
offline_thumbnail: ""
uuid: e5532249-3087-4ace-b8b3-a50a7c8f6794
updated: 1484310561
title: providence
categories:
    - Dictionary
---
Providence
     n 1: the capital and largest city of Rhode Island; located in
          northeastern Rhode Island on Narragansett Bay; site of
          Brown University [syn: {capital of Rhode Island}]
     2: the guardianship and control exercised by a deity; "divine
        providence"
     3: a manifestation of God's foresightful care for His creatures
     4: the prudence and care exercised by someone in the management
        of resources [ant: {improvidence}]
