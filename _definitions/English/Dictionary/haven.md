---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haven
offline_file: ""
offline_thumbnail: ""
uuid: b93afd4d-1f22-47a7-a7ed-9121ab770173
updated: 1484310150
title: haven
categories:
    - Dictionary
---
haven
     n 1: a shelter serving as a place of safety or sanctuary [syn: {oasis}]
     2: a sheltered port where ships can take on or discharge cargo
        [syn: {seaport}, {harbor}, {harbour}]
