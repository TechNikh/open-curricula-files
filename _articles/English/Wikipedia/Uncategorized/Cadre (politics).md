---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cadres
offline_file: ""
offline_thumbnail: ""
uuid: 2197ecda-ba70-4ad1-9ff4-5f2811f8d004
updated: 1484308485
title: Cadre (politics)
categories:
    - Uncategorized
---
In political contexts a cadre (UK /ˈkɑːdər/ or US /ˈkædreɪ/) consists of a small group of people. The word may also refer to a single member of such a group.
