---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dzong
offline_file: ""
offline_thumbnail: ""
uuid: 7ddb3046-b476-4519-8b0b-650bad59d726
updated: 1484308432
title: Dzong architecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Bhutan_dzong_at_paro.jpg
tags:
    - characteristics
    - Regional differences
    - Bhutan
    - Tibet
    - Siting of dzongs
    - Construction
    - Modern architecture in the dzong style
categories:
    - Uncategorized
---
Dzong architecture is a distinctive type of fortress (Wylie: rdzong, IPA: [tzɦoŋ˩˨]) architecture found mainly in Bhutan and the former Tibet. The architecture is massive in style with towering exterior walls surrounding a complex of courtyards, temples, administrative offices, and monks' accommodation.
