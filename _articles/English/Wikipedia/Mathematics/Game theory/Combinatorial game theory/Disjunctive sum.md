---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disjunctive_sum
offline_file: ""
offline_thumbnail: ""
uuid: 0a4cda8b-2db6-48c8-8288-2d4d756b52c9
updated: 1484308542
title: Disjunctive sum
categories:
    - Combinatorial game theory
---
In the mathematics of combinatorial games, the sum or disjunctive sum of two games is a game in which the two games are played in parallel, with each player being allowed to move in just one of the games per turn. The sum game finishes when there are no moves left in either of the two parallel games, at which point (in normal play) the player to move loses. This operation may be extended to disjunctive sums of any number of games, again by playing the games in parallel and moving in exactly one of the games per turn. It is the fundamental operation that is used in the Sprague–Grundy theorem for impartial games and which led to the field of combinatorial game theory for partisan games.
