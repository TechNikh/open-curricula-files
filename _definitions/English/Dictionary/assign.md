---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assign
offline_file: ""
offline_thumbnail: ""
uuid: 2e553987-3d12-4e82-84af-b07304c87e70
updated: 1484310397
title: assign
categories:
    - Dictionary
---
assign
     v 1: give an assignment to (a person) to a post, or assign a task
          to (a person) [syn: {delegate}, {designate}, {depute}]
     2: give out or allot; "We were assigned new uniforms" [syn: {allot},
         {portion}]
     3: attribute or credit to; "We attributed this quotation to
        Shakespeare"; "People impute great cleverness to cats"
        [syn: {impute}, {ascribe}, {attribute}]
     4: select something or someone for a specific purpose; "The
        teacher assigned him to lead his classmates in the
        exercise" [syn: {specify}, {set apart}]
     5: attribute or give; "She put too much emphasis on her the
        last statement"; "He put all his efforts ...
