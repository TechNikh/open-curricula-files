---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/post
offline_file: ""
offline_thumbnail: ""
uuid: b03b97b4-9042-41d1-8db8-f19473aa1e67
updated: 1484310429
title: post
categories:
    - Dictionary
---
post
     n 1: the position where someone (as a guard or sentry) stands or
          is assigned to stand; "a soldier manned the entrance
          post"; "a sentry station" [syn: {station}]
     2: military installation at which a body of troops is
        stationed; "this military post provides an important
        source of income for the town nearby"; "there is an
        officer's club on the post" [syn: {military post}]
     3: a job in an organization; "he occupied a post in the
        treasury" [syn: {position}, {berth}, {office}, {spot}, {billet},
         {place}, {situation}]
     4: an upright consisting of a piece of timber or metal fixed
        firmly in an upright position; ...
