---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/false
offline_file: ""
offline_thumbnail: ""
uuid: 0ee9313c-b512-466e-9e1f-b200fce0ac5e
updated: 1484310206
title: 'false'
categories:
    - Dictionary
---
false
     adj 1: not in accordance with the fact or reality or actuality;
            "gave false testimony under oath"; "false tales of
            bravery" [ant: {true}]
     2: arising from error; "a false assumption"; "a mistaken view
        of the situation" [syn: {mistaken}]
     3: erroneous and usually accidental; "a false start"; "a false
        alarm"
     4: deliberately deceptive; "hollow (or false) promises"; "false
        pretenses" [syn: {hollow}]
     5: inappropriate to reality or facts; "delusive faith in a
        wonder drug"; "delusive expectations"; "false hopes" [syn:
         {delusive}]
     6: not genuine or real; being an imitation of the genuine
        ...
