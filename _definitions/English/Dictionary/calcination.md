---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calcination
offline_file: ""
offline_thumbnail: ""
uuid: 0a3d9c9b-450d-4af4-b113-dfeb3ca0c454
updated: 1484310409
title: calcination
categories:
    - Dictionary
---
calcination
     n : the conversion of metals into their oxides as a result of
         heating to a high temperature
