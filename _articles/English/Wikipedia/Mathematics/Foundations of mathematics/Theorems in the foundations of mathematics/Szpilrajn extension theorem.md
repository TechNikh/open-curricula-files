---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Szpilrajn_extension_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2f46109d-6d10-465c-9d1d-0cb498182ac7
updated: 1484309001
title: Szpilrajn extension theorem
tags:
    - Definitions and statement
    - Proof
    - Other extension theorems
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the Szpilrajn extension theorem, due to Edward Szpilrajn (1930) (later called Edward Marczewski), is one of many examples of the use of the axiom of choice (in the form of Zorn's lemma) to find a maximal set with certain properties.
