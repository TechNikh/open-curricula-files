---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minkowski_distance
offline_file: ""
offline_thumbnail: ""
uuid: 3f9d2ca2-ee69-4465-950e-ccfb1abb8b03
updated: 1484309269
title: Minkowski distance
categories:
    - Metric geometry
---
The Minkowski distance is a metric in a normed vector space which can be considered as a generalization of both the Euclidean distance and the Manhattan distance.
