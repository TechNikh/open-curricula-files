---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9%E2%80%93Bendixson_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f159b715-17e4-4c16-8017-8754cc1a1d71
updated: 1484308692
title: Poincaré–Bendixson theorem
tags:
    - Theorem
    - Discussion
    - Applications
categories:
    - Theorems in dynamical systems
---
In mathematics, the Poincaré–Bendixson theorem is a statement about the long-term behaviour of orbits of continuous dynamical systems on the plane, cylinder, or two-sphere.
