---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pursuit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484625541
title: pursuit
categories:
    - Dictionary
---
pursuit
     n 1: the act of pursuing in an effort to overtake or capture;
          "the culprit started to run and the cop took off in
          pursuit" [syn: {chase}, {following}]
     2: a search for an alternative that meets cognitive criteria;
        "the pursuit of love"; "life is more than the pursuance of
        fame"; "a quest for wealth" [syn: {pursuance}, {quest}]
     3: a diversion that occupies one's time and thoughts (usually
        pleasantly); "sailing is her favorite pastime"; "his main
        pastime is gambling"; "he counts reading among his
        interests"; "they criticized the boy for his limited
        pursuits" [syn: {pastime}, {interest}]
