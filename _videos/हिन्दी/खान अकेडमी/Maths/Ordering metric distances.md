---
version: 1
type: video
provider: YouTube
id: G9qDTH69TRw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e66cdfa3-214c-4002-8e36-e6a350e517f5
updated: 1486069605
title: Ordering metric distances
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/G9qDTH69TRw/default.jpg
    - https://i3.ytimg.com/vi/G9qDTH69TRw/1.jpg
    - https://i3.ytimg.com/vi/G9qDTH69TRw/2.jpg
    - https://i3.ytimg.com/vi/G9qDTH69TRw/3.jpg
---
