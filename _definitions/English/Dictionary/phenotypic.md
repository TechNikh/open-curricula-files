---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenotypic
offline_file: ""
offline_thumbnail: ""
uuid: 8a88bbc8-5b89-4ab0-92ee-85394ee7c084
updated: 1484310299
title: phenotypic
categories:
    - Dictionary
---
phenotypic
     adj : of or relating to or constituting a phenotype; "phenotypical
           profile" [syn: {phenotypical}]
