---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kenneth_Arrow
offline_file: ""
offline_thumbnail: ""
uuid: eb98c81f-078f-4c22-ba27-272769bd3730
updated: 1484308517
title: Kenneth Arrow
tags:
    - Education and early career
    - Academic career
    - Contributions
    - "Arrow's impossibility theorem"
    - General equilibrium theory
    - Fundamental theorems of welfare economics
    - Endogenous-growth theory
    - Information economics
    - Awards and honors
    - Publications
categories:
    - Game theorists
---
Kenneth Joseph "Ken" Arrow (born August 23, 1921) is an American economist, writer, and political theorist. He is the joint winner of the Nobel Memorial Prize in Economics with John Hicks in 1972. To date, he is the youngest person to have received this award, at 51.
