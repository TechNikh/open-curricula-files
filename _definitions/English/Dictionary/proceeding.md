---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proceeding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484336881
title: proceeding
categories:
    - Dictionary
---
proceeding
     n : (law) the institution of a sequence of steps by which legal
         judgments are invoked [syn: {legal proceeding}, {proceedings}]
