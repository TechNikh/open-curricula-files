---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gary_Chartrand
offline_file: ""
offline_thumbnail: ""
uuid: 9e5dcc11-5cda-4380-aa78-78e998fa2f54
updated: 1484309461
title: Gary Chartrand
tags:
    - Biography
    - Books
categories:
    - Graph theorists
---
Gary Theodore Chartrand (born 1936) is an American-born mathematician who specializes in graph theory. He is known for his textbooks on introductory graph theory and for the concept of a highly irregular graph.
