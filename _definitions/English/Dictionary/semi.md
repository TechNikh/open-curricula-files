---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/semi
offline_file: ""
offline_thumbnail: ""
uuid: a78bb732-a0dd-4438-86ad-33de97d36740
updated: 1484310210
title: semi
categories:
    - Dictionary
---
semi
     n 1: one of the two competitions in the next to the last round of
          an elimination tournament [syn: {semifinal}]
     2: a truck consisting of a tractor and trailer together [syn: {trailer
        truck}, {tractor trailer}, {trucking rig}, {rig}, {articulated
        lorry}]
     3: a trailer having wheels only in the rear; the front is
        supported by the towing vehicle [syn: {semitrailer}]
