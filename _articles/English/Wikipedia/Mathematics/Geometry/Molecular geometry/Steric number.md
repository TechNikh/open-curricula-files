---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steric_number
offline_file: ""
offline_thumbnail: ""
uuid: 2880be48-c025-4ec3-91c5-58059b855ff8
updated: 1484309282
title: Steric number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sulfur-tetrafluoride-2D-dimensions.png
categories:
    - Molecular geometry
---
The steric number of a molecule is the number of atoms bonded to the central atom of a molecule plus the number of lone pairs on the central atom. It is often used in VSEPR theory (valence shell electron-pair repulsion theory) in order to determine the particular shape, or molecular geometry, that will be formed.
