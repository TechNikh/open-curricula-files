---
version: 1
type: video
provider: YouTube
id: XIzScwydxOE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bcb902eb-4ff3-4550-b2f5-0bba7027bee6
updated: 1486069641
title: Trip inside a 3D fractal (Kleinian) GPU realtime rendering
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/XIzScwydxOE/default.jpg
    - https://i3.ytimg.com/vi/XIzScwydxOE/1.jpg
    - https://i3.ytimg.com/vi/XIzScwydxOE/2.jpg
    - https://i3.ytimg.com/vi/XIzScwydxOE/3.jpg
---
