---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/localisation
offline_file: ""
offline_thumbnail: ""
uuid: bc0499bd-cc55-4dbf-9cc8-be7a5dac1643
updated: 1484310549
title: localisation
categories:
    - Dictionary
---
localisation
     n 1: (physiology) the principle that specific functions have
          relatively circumscribed locations in some particular
          part or organ of the body [syn: {localization of
          function}, {localisation of function}, {localization
          principle}, {localisation principle}, {localization}]
     2: a determination of the location of something; "he got a good
        fix on the target" [syn: {localization}, {location}, {locating},
         {fix}]
