---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transport
offline_file: ""
offline_thumbnail: ""
uuid: 2b3831c3-4c4c-448b-8bab-6b3f4c9e09d1
updated: 1484310363
title: transport
categories:
    - Dictionary
---
transport
     n 1: something that serves as a means of transportation [syn: {conveyance}]
     2: an exchange of molecules (and their kinetic energy and
        momentum) across the boundary between adjacent layers of a
        fluid or across cell membranes
     3: the commercial enterprise of transporting goods and
        materials [syn: {transportation}, {shipping}]
     4: a state of being carried away by overwhelming emotion;
        "listening to sweet music in a perfect rapture"- Charles
        Dickens [syn: {ecstasy}, {rapture}, {exaltation}, {raptus}]
     5: a mechanism that transport magnetic tape across the
        read/write heads of a tape playback/recorder [syn: {tape
     ...
