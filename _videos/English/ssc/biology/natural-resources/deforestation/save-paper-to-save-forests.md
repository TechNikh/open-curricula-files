---
title: Save Paper to Save Forests
youtube_id: jHQGyMBtE9Y
tags:
    - save forests
    - save paper
    - forest conservation
    - habitat conservation
    - ways to save paper
    - how to save paper
    - how to save forest
    - how to save trees
    - save trees
    - save earth
categories:
    - Deforestation
---
