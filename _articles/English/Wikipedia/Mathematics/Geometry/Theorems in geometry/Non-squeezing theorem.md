---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-squeezing_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 35e1e380-ff11-4320-95b6-12e9b396ed94
updated: 1484309119
title: Non-squeezing theorem
tags:
    - Background and statement
    - The “symplectic camel”
categories:
    - Theorems in geometry
---
The non-squeezing theorem, also called Gromov's non-squeezing theorem, is one of the most important theorems in symplectic geometry.[1] It was first proven in 1985 by Mikhail Gromov.[2] The theorem states that one cannot embed a ball into a cylinder via a symplectic map unless the radius of the ball is less than or equal to the radius of the cylinder. The importance of this theorem is as follows: very little was known about the geometry behind symplectic transformations. One easy consequence of a transformation being symplectic is that it preserves volume.[3] One can easily embed a ball of any radius into a cylinder of any other radius by a volume-preserving transformation: just picture ...
