---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurt
offline_file: ""
offline_thumbnail: ""
uuid: 29452118-c336-49de-a160-a3bd42c7d314
updated: 1484310531
title: hurt
categories:
    - Dictionary
---
hurt
     adj 1: suffering from physical injury especially that suffered in
            battle; "nursing his wounded arm"; "ambulances...for
            the hurt men and women" [syn: {wounded}]
     2: used of inanimate objects or their value [syn: {weakened}]
     n 1: any physical damage to the body caused by violence or
          accident or fracture etc. [syn: {injury}, {harm}, {trauma}]
     2: psychological suffering; "the death of his wife caused him
        great distress" [syn: {distress}, {suffering}]
     3: feelings of mental or physical pain [syn: {suffering}]
     4: a damage or loss [syn: {detriment}]
     5: the act of damaging something or someone [syn: {damage}, {harm},
   ...
