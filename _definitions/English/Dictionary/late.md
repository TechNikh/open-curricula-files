---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/late
offline_file: ""
offline_thumbnail: ""
uuid: ed209fca-e041-4cbe-a2e8-aa963bc09fd6
updated: 1484310260
title: late
categories:
    - Dictionary
---
late
     adj 1: being or occurring at an advanced period of time or after a
            usual or expected time; "late evening"; "late 18th
            century"; "a late movie"; "took a late flight"; "had a
            late breakfast" [ant: {early}, {middle}]
     2: after the expected or usual time; delayed; "a belated
        birthday card"; "I'm late for the plane"; "the train is
        late"; "tardy children are sent to the principal"; "always
        tardy in making dental appointments" [syn: {belated}, {tardy}]
     3: of the immediate past or just previous to the present time;
        "a late development"; "their late quarrel"; "his recent
        trip to Africa"; "in recent ...
