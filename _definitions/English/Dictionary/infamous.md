---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infamous
offline_file: ""
offline_thumbnail: ""
uuid: d3b2daa6-6856-4489-a292-91ff61119f38
updated: 1484310176
title: infamous
categories:
    - Dictionary
---
infamous
     adj : having an exceedingly bad reputation; "a notorious
           gangster"; "the tenderloin district was notorious for
           vice" [syn: {ill-famed}, {notorious}]
