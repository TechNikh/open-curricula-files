---
version: 1
type: video
provider: YouTube
id: -LBSMy8gBGA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%202%20-%20Mass.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 18bebb79-fd46-4d65-aa25-3ce904920ebb
updated: 1486069666
title: 'Eureka 2 - Mass.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/-LBSMy8gBGA/default.jpg
    - https://i3.ytimg.com/vi/-LBSMy8gBGA/1.jpg
    - https://i3.ytimg.com/vi/-LBSMy8gBGA/2.jpg
    - https://i3.ytimg.com/vi/-LBSMy8gBGA/3.jpg
---
