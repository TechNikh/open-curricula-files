---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/statesmanship
offline_file: ""
offline_thumbnail: ""
uuid: 32161573-8b5f-4fe9-927d-1898d4211382
updated: 1484310192
title: statesmanship
categories:
    - Dictionary
---
statesmanship
     n : wisdom in the management of public affairs [syn: {statecraft},
          {diplomacy}]
