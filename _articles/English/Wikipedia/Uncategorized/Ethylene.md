---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ethene
offline_file: ""
offline_thumbnail: ""
uuid: 8fcc5e7c-ec96-4116-bdec-d276da0dc39f
updated: 1484308391
title: Ethylene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ethylene_Signal_Transduction.svg.png
tags:
    - Structure and properties
    - Uses
    - Polymerization
    - Oxidation
    - Halogenation and hydrohalogenation
    - Alkylation
    - Oxo reaction
    - Hydration
    - Dimerization to n-Butenes
    - Niche uses
    - Production
    - Industrial Process
    - Laboratory synthesis
    - Ethylene as a plant hormone
    - History of ethylene in plant biology
    - Ethylene biosynthesis in plants
    - Ethylene perception in plants
    - Environmental and biological triggers of ethylene
    - List of plant responses to ethylene
    - Commercial issues
    - Ligand
    - History
    - Nomenclature
    - Safety
categories:
    - Uncategorized
---
Ethylene (IUPAC name: ethene) is a hydrocarbon which has the formula C
2H
4 or H2C=CH2. It is a colorless flammable gas with a faint "sweet and musky" odour when pure.[4] It is the simplest alkene (a hydrocarbon with carbon-carbon double bonds), and the second simplest unsaturated hydrocarbon after acetylene (C
2H
2).
