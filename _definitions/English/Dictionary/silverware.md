---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silverware
offline_file: ""
offline_thumbnail: ""
uuid: 7f57cb87-54f3-4131-b4ff-48e6cfeb718f
updated: 1484310377
title: silverware
categories:
    - Dictionary
---
silverware
     n : tableware made of silver or silver plate or pewter or
         stainless steel
