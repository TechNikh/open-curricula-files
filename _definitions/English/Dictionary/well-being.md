---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well-being
offline_file: ""
offline_thumbnail: ""
uuid: 8e04ed37-4fc7-4ed3-b4e8-8f7160af2c55
updated: 1484310447
title: well-being
categories:
    - Dictionary
---
well-being
     n : a contented state of being happy and healthy and prosperous;
         "the town was finally on the upbeat after our recent
         troubles" [syn: {wellbeing}, {welfare}, {upbeat}, {eudaemonia},
          {eudaimonia}] [ant: {ill-being}]
