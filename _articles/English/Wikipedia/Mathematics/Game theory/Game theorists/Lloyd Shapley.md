---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lloyd_Shapley
offline_file: ""
offline_thumbnail: ""
uuid: db43db57-d044-4cd5-9356-d124b056cdd2
updated: 1484309029
title: Lloyd Shapley
tags:
    - Life and career
    - Contribution
    - Awards and honors
    - Selected publications
categories:
    - Game theorists
---
Lloyd Stowell Shapley (June 2, 1923 – March 12, 2016) was an American mathematician and Nobel Prize-winning economist. He contributed to the fields of mathematical economics and especially game theory.[1][2]
