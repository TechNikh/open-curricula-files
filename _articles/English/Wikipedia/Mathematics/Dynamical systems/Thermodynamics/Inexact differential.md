---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inexact_differential
offline_file: ""
offline_thumbnail: ""
uuid: 41d08b42-eda8-4e8a-ac80-f4e59194535e
updated: 1484308846
title: Inexact differential
tags:
    - Definition
    - First Law of Thermodynamics
    - Examples
    - Total distance
    - Heat and work
    - Integrating factors
categories:
    - Thermodynamics
---
An inexact differential or imperfect differential is a specific type of differential used in thermodynamics to express the path dependence of a particular differential. It is contrasted with the concept of the exact differential in calculus, which can be expressed as the gradient of another function and is therefore path independent. Consequently, an inexact differential cannot be expressed in terms of its antiderivative for the purpose of integral calculations; i.e. its value cannot be inferred just by looking at the initial and final states of a given system.[1] It is primarily used in calculations involving heat and work because they are path functions, not state functions.
