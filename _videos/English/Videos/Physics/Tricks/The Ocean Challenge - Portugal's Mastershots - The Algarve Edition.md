---
version: 1
type: video
provider: YouTube
id: A_qVyxZ9dUY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 542f950c-8a1b-4e00-bcaa-995852ac4cc5
updated: 1486069660
title: "The Ocean Challenge - Portugal's Mastershots - The Algarve Edition"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/A_qVyxZ9dUY/default.jpg
    - https://i3.ytimg.com/vi/A_qVyxZ9dUY/1.jpg
    - https://i3.ytimg.com/vi/A_qVyxZ9dUY/2.jpg
    - https://i3.ytimg.com/vi/A_qVyxZ9dUY/3.jpg
---
