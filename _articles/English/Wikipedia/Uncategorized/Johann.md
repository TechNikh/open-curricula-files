---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Johann
offline_file: ""
offline_thumbnail: ""
uuid: d4c3894d-d2de-454d-aff1-7a73cc4a0fe5
updated: 1484308347
title: Johann
tags:
    - people
    - Other uses
categories:
    - Uncategorized
---
Johann, typically a male given name, is the Germanized form of the originally Hebrew language name יוחנן (Yohanan) (meaning "God is merciful"). It is a form of the Germanic and Latin given name "Johannes." The English language form is John. It is uncommon as a surname.
