---
version: 1
type: video
provider: YouTube
id: XYES0_ooeLc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55fbd8ab-903d-46ca-a3d3-66e0eb6b4658
updated: 1486069634
title: MODEL OF ENERGY CONSERVATION FOR EXHIBITION
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/XYES0_ooeLc/default.jpg
    - https://i3.ytimg.com/vi/XYES0_ooeLc/1.jpg
    - https://i3.ytimg.com/vi/XYES0_ooeLc/2.jpg
    - https://i3.ytimg.com/vi/XYES0_ooeLc/3.jpg
---
