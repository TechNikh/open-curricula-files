---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cuboid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484362201
title: cuboid
categories:
    - Dictionary
---
cuboid
     adj : shaped like a cube [syn: {cubelike}, {cube-shaped}, {cubical},
            {cubiform}, {cuboidal}]
     n : a rectangular parallelepiped
