---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alienated
offline_file: ""
offline_thumbnail: ""
uuid: 80caa683-a6f2-465b-8378-6f7c07a91a78
updated: 1484310192
title: alienated
categories:
    - Dictionary
---
alienated
     adj 1: socially disoriented; "anomic loners musing over their
            fate"; "we live in an age of rootless alienated
            people" [syn: {anomic}, {disoriented}]
     2: caused to be unloved [syn: {estranged}]
