---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dividing
offline_file: ""
offline_thumbnail: ""
uuid: 1b1bebad-95d3-415f-9fb4-212a52da161c
updated: 1484310212
title: dividing
categories:
    - Dictionary
---
dividing
     adj 1: serving to divide or marking a division; "the divisional
            line between two states" [syn: {divisional}]
     2: serving simply to separate or partition; "a dividing
        partition" [syn: {dividing(a)}]
