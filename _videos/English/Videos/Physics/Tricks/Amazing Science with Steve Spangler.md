---
version: 1
type: video
provider: YouTube
id: ur8DZnjlhPw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f1aaae15-48fa-46c1-9673-26bf9972ef66
updated: 1486069660
title: Amazing Science with Steve Spangler
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/ur8DZnjlhPw/default.jpg
    - https://i3.ytimg.com/vi/ur8DZnjlhPw/1.jpg
    - https://i3.ytimg.com/vi/ur8DZnjlhPw/2.jpg
    - https://i3.ytimg.com/vi/ur8DZnjlhPw/3.jpg
---
