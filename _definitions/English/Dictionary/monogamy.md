---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monogamy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439901
title: monogamy
categories:
    - Dictionary
---
monogamy
     n : having only one spouse at a time [syn: {monogamousness}]
