---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dragan_Maru%C5%A1i%C4%8D'
offline_file: ""
offline_thumbnail: ""
uuid: 4cd0f3b3-68cc-47e3-b584-f80f19fa3e0a
updated: 1484309472
title: Dragan Marušič
tags:
    - Education and career
    - Achievements and honours
    - Research
    - Personal life
    - Selected publications
categories:
    - Graph theorists
---
Dragan Marušič (born 1953, Koper, Slovenia)[1] is a Slovene mathematician. Marušič obtained his BSc in technical mathematics from the University of Ljubljana in 1976, and his PhD from the University of Reading in 1981 under the supervision of Crispin Nash-Williams.
