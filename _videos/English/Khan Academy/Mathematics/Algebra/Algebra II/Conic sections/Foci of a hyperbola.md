---
version: 1
type: video
provider: YouTube
id: S0Fd2Tg2v7M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Conic%20sections/Foci%20of%20a%20hyperbola.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 217d91f8-c95d-4b65-ab6e-3e39612c3476
updated: 1486069569
title: Foci of a hyperbola
categories:
    - Conic sections
thumbnail_urls:
    - https://i3.ytimg.com/vi/S0Fd2Tg2v7M/default.jpg
    - https://i3.ytimg.com/vi/S0Fd2Tg2v7M/1.jpg
    - https://i3.ytimg.com/vi/S0Fd2Tg2v7M/2.jpg
    - https://i3.ytimg.com/vi/S0Fd2Tg2v7M/3.jpg
---
Introduction to the foci (focuses) of a hyperbola

Watch the next lesson: https://www.khanacademy.org/math/algebra2/conics_precalc/hyperbolas-precalc/v/proof-hyperbola-foci?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/conics_precalc/hyperbolas-precalc/v/conic-sections-hyperbolas-3?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
