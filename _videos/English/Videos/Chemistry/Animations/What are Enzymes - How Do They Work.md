---
version: 1
type: video
provider: YouTube
id: vTQybDgweiE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/What%20are%20Enzymes%20-%20How%20Do%20They%20Work.webm"
offline_file: ""
offline_thumbnail: ""
uuid: af10fe0b-839f-4219-9b27-c0f1c68af2c0
updated: 1486069664
title: 'What are Enzymes - How Do They Work?'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vTQybDgweiE/default.jpg
    - https://i3.ytimg.com/vi/vTQybDgweiE/1.jpg
    - https://i3.ytimg.com/vi/vTQybDgweiE/2.jpg
    - https://i3.ytimg.com/vi/vTQybDgweiE/3.jpg
---
