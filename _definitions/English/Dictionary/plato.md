---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plato
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484431202
title: plato
categories:
    - Dictionary
---
Plato
     n : ancient Athenian philosopher; pupil of Socrates; teacher of
         Aristotle (428-347 BC)
