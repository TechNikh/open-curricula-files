---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incitement
offline_file: ""
offline_thumbnail: ""
uuid: efb96358-1fcc-46e4-86c9-4920820b5fe2
updated: 1484310166
title: incitement
categories:
    - Dictionary
---
incitement
     n 1: an act of urging on or spurring on or rousing to action or
          instigating; "the incitement of mutiny" [syn: {incitation}]
     2: needed encouragement; "the result was a provocation of
        vigorous investigation" [syn: {provocation}]
     3: something that incites or provokes; a means of arousing or
        stirring to action [syn: {incitation}, {provocation}]
     4: the act of exhorting; an earnest attempt at persuasion [syn:
         {exhortation}]
