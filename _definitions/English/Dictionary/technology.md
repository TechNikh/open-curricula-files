---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technology
offline_file: ""
offline_thumbnail: ""
uuid: 5e500f31-64e3-4970-88fb-debc481e852a
updated: 1484310234
title: technology
categories:
    - Dictionary
---
technology
     n 1: the practical application of science to commerce or industry
          [syn: {engineering}]
     2: the discipline dealing with the art or science of applying
        scientific knowledge to practical problems; "he had
        trouble deciding which branch of engineering to study"
        [syn: {engineering}, {engineering science}, {applied
        science}]
