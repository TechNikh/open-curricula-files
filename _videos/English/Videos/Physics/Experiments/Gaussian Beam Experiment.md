---
version: 1
type: video
provider: YouTube
id: 9RRulxbQ9NQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5a59b43e-90f4-47f6-a326-18b8c93c6c49
updated: 1486069637
title: Gaussian Beam Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/9RRulxbQ9NQ/default.jpg
    - https://i3.ytimg.com/vi/9RRulxbQ9NQ/1.jpg
    - https://i3.ytimg.com/vi/9RRulxbQ9NQ/2.jpg
    - https://i3.ytimg.com/vi/9RRulxbQ9NQ/3.jpg
---
