---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicity_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 4261059f-d6ce-4872-aa75-513a0c528a95
updated: 1484309510
title: Multiplicity (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Polynomial_roots_multiplicity.svg.png
tags:
    - Multiplicity of a prime factor
    - Multiplicity of a root of a polynomial
    - Behavior of a polynomial function near a multiple root
    - Intersection multiplicity
    - In complex analysis
categories:
    - Set theory
---
In mathematics, the multiplicity of a member of a multiset is the number of times it appears in the multiset. For example, the number of times a given polynomial equation has a root at a given point.
