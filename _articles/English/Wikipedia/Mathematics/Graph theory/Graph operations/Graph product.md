---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_product
offline_file: ""
offline_thumbnail: ""
uuid: 36db3686-5e54-416f-be38-af36c82cc552
updated: 1484309448
title: Graph product
tags:
    - Mnemonic
    - Notes
categories:
    - Graph operations
---
In mathematics, a graph product is a binary operation on graphs. Specifically, it is an operation that takes two graphs G1 and G2 and produces a graph H with the following properties:
