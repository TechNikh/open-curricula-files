---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freely
offline_file: ""
offline_thumbnail: ""
uuid: cc208c59-32f7-44ea-9053-1f7dc5a3b9f8
updated: 1484310212
title: freely
categories:
    - Dictionary
---
freely
     adv : in a free manner; "the painting featured freely brushed
           strokes"
