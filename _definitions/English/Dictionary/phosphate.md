---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phosphate
offline_file: ""
offline_thumbnail: ""
uuid: 05cea348-03ef-48dd-ba30-b2ac2c224aa8
updated: 1484310297
title: phosphate
categories:
    - Dictionary
---
phosphate
     n 1: a salt of phosphoric acid [syn: {orthophosphate}, {inorganic
          phosphate}]
     2: carbonated drink with fruit syrup and a little phosphoric
        acid
