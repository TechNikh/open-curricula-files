---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stocking
offline_file: ""
offline_thumbnail: ""
uuid: 7daa9a36-723b-4dc9-a5a8-cb84e8ee1252
updated: 1484310168
title: stocking
categories:
    - Dictionary
---
stocking
     adj : wearing stockings; "walks about in his stockinged (or
           stocking) feet" [syn: {stockinged}]
     n 1: close-fitting hosiery to cover the foot and leg; come in
          matched pairs (usually used in the plural)
     2: the activity of supplying a stock of something; "he
        supervised the stocking of the stream with trout"
