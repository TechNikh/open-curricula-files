---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Landspout
offline_file: ""
offline_thumbnail: ""
uuid: 772e9587-7e3d-43cb-9f7f-17a5a4efffe0
updated: 1484308875
title: Landspout
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GID_Landspout.jpg
categories:
    - Vortices
---
A landspout is a term coined by meteorologist Howard B. Bluestein in 1985 for a kind of tornado not associated with the mesocyclone of a thunderstorm.[1] The Glossary of Meteorology defines a landspout as
