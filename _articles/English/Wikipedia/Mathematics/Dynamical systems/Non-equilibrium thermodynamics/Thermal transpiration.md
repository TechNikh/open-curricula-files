---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_transpiration
offline_file: ""
offline_thumbnail: ""
uuid: 73c47d2d-fcd9-4bd9-bf67-6334fc5a1716
updated: 1484308805
title: Thermal transpiration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Crookes_radiometer_thermal_creep.png
categories:
    - Non-equilibrium thermodynamics
---
Thermal transpiration or thermal diffusion refers to the thermal force on a gas due to a temperature difference. Thermal transpiration causes a flow of gas in the absence of any pressure difference, and is able to maintain a certain pressure difference (called thermomolecular pressure difference) in a steady state. The effect is strongest when the mean free path of the gas molecules is comparable to the dimensions of the gas container.
