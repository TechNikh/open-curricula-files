---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tartaric
offline_file: ""
offline_thumbnail: ""
uuid: a4abc011-efac-4a5f-9557-e8c63fabaef3
updated: 1484310384
title: tartaric
categories:
    - Dictionary
---
tartaric
     adj : relating to or derived from or resembling tartar; "tartaric
           acid"
