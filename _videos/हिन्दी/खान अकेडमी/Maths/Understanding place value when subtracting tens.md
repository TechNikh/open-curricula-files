---
version: 1
type: video
provider: YouTube
id: C4ylyWJuA2M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21effa5d-a6d3-417e-a27a-dd46dafc5ca4
updated: 1486069616
title: Understanding place value when subtracting tens
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/C4ylyWJuA2M/default.jpg
    - https://i3.ytimg.com/vi/C4ylyWJuA2M/1.jpg
    - https://i3.ytimg.com/vi/C4ylyWJuA2M/2.jpg
    - https://i3.ytimg.com/vi/C4ylyWJuA2M/3.jpg
---
