---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plight
offline_file: ""
offline_thumbnail: ""
uuid: c9904904-bd57-403b-9361-55608fc32e59
updated: 1484310567
title: plight
categories:
    - Dictionary
---
plight
     n 1: a situation from which extrication is difficult especially
          an unpleasant or trying one; "finds himself in a most
          awkward predicament"; "the woeful plight of homeless
          people" [syn: {predicament}, {quandary}]
     2: a solemn pledge of fidelity [syn: {troth}]
     v 1: give to in marriage [syn: {betroth}, {engage}, {affiance}]
     2: promise solemnly and formally; "I pledge that will honor my
        wife" [syn: {pledge}]
