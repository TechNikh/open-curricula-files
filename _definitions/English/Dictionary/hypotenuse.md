---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypotenuse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320921
title: hypotenuse
categories:
    - Dictionary
---
hypotenuse
     n : the side of a right triangle opposite the right angle
