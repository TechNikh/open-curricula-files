---
version: 1
type: video
provider: YouTube
id: pdqkrqHDF7g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9a69833a-f3a3-4e7c-afc8-916dc11bbb76
updated: 1486069623
title: I Can Guess Your Card
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/pdqkrqHDF7g/default.jpg
    - https://i3.ytimg.com/vi/pdqkrqHDF7g/1.jpg
    - https://i3.ytimg.com/vi/pdqkrqHDF7g/2.jpg
    - https://i3.ytimg.com/vi/pdqkrqHDF7g/3.jpg
---
