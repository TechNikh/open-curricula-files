---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fennel
offline_file: ""
offline_thumbnail: ""
uuid: 81b39ee6-bb64-4cf1-bd83-6490c5bf0f64
updated: 1484310353
title: fennel
categories:
    - Dictionary
---
fennel
     n 1: any of several aromatic herbs having edible seeds and leaves
          and stems
     2: aromatic bulbous stem base eaten cooked or raw in salads
        [syn: {Florence fennel}, {finocchio}]
     3: leaves used for seasoning [syn: {common fennel}]
