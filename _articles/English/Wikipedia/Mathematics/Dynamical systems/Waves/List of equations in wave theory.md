---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_equations_in_wave_theory
offline_file: ""
offline_thumbnail: ""
uuid: 79e27608-b0fe-4f06-97bc-9303546fa0e9
updated: 1484308893
title: List of equations in wave theory
tags:
    - Definitions
    - General fundamental quantities
    - General derived quantities
    - Modulation indices
    - Acoustics
    - Equations
    - Standing waves
    - Propagating waves
    - Sound waves
    - Gravitational waves
    - Superposition, interference, and diffraction
    - Wave propagation
    - General wave functions
    - Wave equations
    - Sinusoidal solutions to the 3d wave equation
    - Footnotes
    - Sources
categories:
    - Waves
---
