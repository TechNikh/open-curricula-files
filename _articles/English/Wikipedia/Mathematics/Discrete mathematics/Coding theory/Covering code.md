---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_code
offline_file: ""
offline_thumbnail: ""
uuid: 2f9a6539-2526-4d3b-b5ee-710a95036854
updated: 1484308545
title: Covering code
tags:
    - Definition
    - Example
    - Covering problem
    - Football pools problem
    - Applications
categories:
    - Coding theory
---
In coding theory, a covering code is a set of elements (called codewords) in a space, with the property that every element of the space is within a fixed distance of some codeword.
