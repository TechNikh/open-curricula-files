---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/felled
offline_file: ""
offline_thumbnail: ""
uuid: 22d765f2-73a2-45ac-b882-ebe5319b9135
updated: 1484310543
title: felled
categories:
    - Dictionary
---
felled
     adj : made to fall (as by striking or cutting or shooting or by
           illness or exhaustion); "the felled boxer lay stretched
           on the canvas"; "felled trees covered the hillside";
           "the downed oxen lay panting in the heat"; "a downed
           deer" [syn: {downed}] [ant: {unfelled}]
