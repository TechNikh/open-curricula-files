---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frigorific_mixture
uuid: 79f566f5-0f40-4f5e-aa9b-a21120a5a68a
updated: 1480455144
title: Frigorific mixture
tags:
    - Ice
    - Other examples
    - Uses
    - Limitations of acid base slushes
categories:
    - Thermodynamics
---
A frigorific mixture is a mixture of two or more chemicals that reaches an equilibrium temperature that is independent of the temperature of any of its component chemicals before they are mixed. The temperature is also relatively independent of the quantities of mixtures as long as significant amounts of each original chemical are present in its pure form.
