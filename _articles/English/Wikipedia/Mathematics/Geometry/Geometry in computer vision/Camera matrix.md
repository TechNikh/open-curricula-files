---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Camera_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 43869091-dcec-4010-9419-7488efa10e36
updated: 1484309176
title: Camera matrix
tags:
    - Derivation
    - Camera position
    - Normalized camera matrix and normalized image coordinates
    - The camera position
    - General camera matrix
categories:
    - Geometry in computer vision
---
In computer vision a camera matrix or (camera) projection matrix is a 
  
    
      
        3
        ×
        4
      
    
    {\displaystyle 3\times 4}
  
 matrix which describes the mapping of a pinhole camera from 3D points in the world to 2D points in an image.
