---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irritated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416981
title: irritated
categories:
    - Dictionary
---
irritated
     adj 1: feeling inflammation or other discomfort (especially in a
            part of the body)
     2: aroused to impatience or anger; "made an irritated gesture";
        "feeling nettled from the constant teasing"; "peeved about
        being left out"; "felt really pissed at her snootiness";
        "riled no end by his lies"; "roiled by the delay" [syn: {annoyed},
         {miffed}, {nettled}, {peeved}, {pissed}, {pissed off}, {riled},
         {roiled}, {steamed}, {stunng}]
