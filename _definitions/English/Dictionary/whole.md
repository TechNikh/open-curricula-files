---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whole
offline_file: ""
offline_thumbnail: ""
uuid: 78ad26c8-a6ec-4fef-b59e-08a892e43314
updated: 1484310339
title: whole
categories:
    - Dictionary
---
whole
     adj 1: including all components without exception; being one unit
            or constituting the full amount or extent or duration;
            complete; "gave his whole attention"; "a whole
            wardrobe for the tropics"; "the whole hog"; "a whole
            week"; "the baby cried the whole trip home"; "a whole
            loaf of bread" [ant: {fractional}]
     2: (of siblings) having the same parents; "whole brothers and
        sisters" [ant: {half}]
     3: exhibiting or restored to vigorous good health; "hale and
        hearty"; "whole in mind and body"; "a whole person again"
        [syn: {hale}]
     n 1: all of something including all its component elements ...
