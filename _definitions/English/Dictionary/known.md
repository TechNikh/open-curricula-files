---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/known
offline_file: ""
offline_thumbnail: ""
uuid: 09ddc4c3-182d-4f11-8496-7c343d12b82f
updated: 1484310321
title: known
categories:
    - Dictionary
---
know
     v 1: be cognizant or aware of a fact or a specific piece of
          information; possess knowledge or information about; "I
          know that the President lied to the people"; "I want to
          know who is winning the game!"; "I know it's time" [syn:
           {cognize}, {cognise}] [ant: {ignore}]
     2: know how to do or perform something; "She knows how to
        knit"; "Does your husband know how to cook?"
     3: be aware of the truth of something; have a belief or faith
        in something; regard as true beyond any doubt; "I know
        that I left the key on the table"; "Galileo knew that the
        earth moves around the sun"
     4: be familiar or acquainted ...
