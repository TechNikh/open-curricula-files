---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finish
offline_file: ""
offline_thumbnail: ""
uuid: ee43843d-d5eb-492f-9c49-99452c306520
updated: 1484310226
title: finish
categories:
    - Dictionary
---
finish
     n 1: a decorative texture or appearance of a surface (or the
          substance that gives it that appearance); "the boat had
          a metallic finish"; "he applied a coat of a clear
          finish"; "when the finish is too thin it is difficult to
          apply evenly" [syn: {coating}, {finishing}]
     2: designated event that concludes a contest (especially a
        race); "excitement grew as the finish neared"; "my horse
        was several lengths behind at the finish"; "the winner is
        the team with the most points at the finish"
     3: the act of finishing; "his best finish in a major tournament
        was third"; "the speaker's finishing was greeted with
 ...
