---
version: 1
type: video
provider: YouTube
id: RKdrI9MZXHQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c25a6645-9c90-4a9b-86b7-d23017e994f6
updated: 1486069641
title: 'God Math- The Golden Spiral & Fibonacci Sequence.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/RKdrI9MZXHQ/default.jpg
    - https://i3.ytimg.com/vi/RKdrI9MZXHQ/1.jpg
    - https://i3.ytimg.com/vi/RKdrI9MZXHQ/2.jpg
    - https://i3.ytimg.com/vi/RKdrI9MZXHQ/3.jpg
---
