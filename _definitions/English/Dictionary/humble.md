---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humble
offline_file: ""
offline_thumbnail: ""
uuid: 95b639b9-d4a6-4410-9b5b-03721cdce5d9
updated: 1484310315
title: humble
categories:
    - Dictionary
---
humble
     adj 1: low or inferior in station or quality; "a humble cottage";
            "a lowly parish priest"; "a modest man of the people";
            "small beginnings" [syn: {low}, {lowly}, {modest}, {small}]
     2: marked by meekness or modesty; not arrogant or prideful; "a
        humble apology"; "essentially humble...and self-effacing,
        he achieved the highest formal honors and distinctions"-
        B.K.Malinowski [ant: {proud}]
     3: used of unskilled work (especially domestic work) [syn: {menial},
         {lowly}]
     4: of low birth or station (`base' is archaic in this sense);
        "baseborn wretches with dirty faces"; "of humble (or
        lowly) birth" ...
