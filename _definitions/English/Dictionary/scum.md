---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scum
offline_file: ""
offline_thumbnail: ""
uuid: 7ef2c94f-e103-45be-b340-9c183cfed5e6
updated: 1484310411
title: scum
categories:
    - Dictionary
---
scum
     n 1: worthless people [syn: {trash}]
     2: a film of impurities or vegetation that can form on the
        surface of a liquid
     [also: {scumming}, {scummed}]
