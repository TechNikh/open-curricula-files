---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/redistribution
offline_file: ""
offline_thumbnail: ""
uuid: 5d4f017e-5abb-4e0d-adf7-27246adb2648
updated: 1484310407
title: redistribution
categories:
    - Dictionary
---
redistribution
     n : distributing again; "the revolution resulted in a
         redistribution of wealth"
