---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poisoning
offline_file: ""
offline_thumbnail: ""
uuid: 281573e4-cf37-4c56-988e-10deac848fb5
updated: 1484310443
title: poisoning
categories:
    - Dictionary
---
poisoning
     n 1: the physiological state produced by a poison or other toxic
          substance [syn: {toxic condition}, {intoxication}]
     2: the act of giving poison to a person or animal with the
        intent to kill
