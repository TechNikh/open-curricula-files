---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rubidium
offline_file: ""
offline_thumbnail: ""
uuid: 8a0b4463-83e7-4e9e-92d5-4da3740eae8a
updated: 1484310403
title: rubidium
categories:
    - Dictionary
---
rubidium
     n : a soft silvery metallic element of the alkali metal group;
         burns in air and reacts violently in water; occurs in
         carnallite and lepidolite and pollucite [syn: {Rb}, {atomic
         number 37}]
