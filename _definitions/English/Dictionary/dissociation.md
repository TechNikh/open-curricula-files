---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissociation
offline_file: ""
offline_thumbnail: ""
uuid: ef30fda1-3275-4c0a-814f-5e8567ba1c41
updated: 1484310383
title: dissociation
categories:
    - Dictionary
---
dissociation
     n 1: the act of removing from association
     2: a state in which some integrated part of a person's life
        becomes separated from the rest of the personality and
        functions independently [syn: {disassociation}]
     3: (chemistry) the temporary or reversible process in which a
        molecule or ion is broken down into smaller molecules or
        ions
