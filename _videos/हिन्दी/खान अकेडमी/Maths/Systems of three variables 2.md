---
version: 1
type: video
provider: YouTube
id: YKup4RwgbCQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de8813d0-9e03-492e-b6f6-e7401b52c38e
updated: 1486069613
title: Systems of three variables 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/YKup4RwgbCQ/default.jpg
    - https://i3.ytimg.com/vi/YKup4RwgbCQ/1.jpg
    - https://i3.ytimg.com/vi/YKup4RwgbCQ/2.jpg
    - https://i3.ytimg.com/vi/YKup4RwgbCQ/3.jpg
---
