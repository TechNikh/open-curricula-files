---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cock
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587501
title: cock
categories:
    - Dictionary
---
cock
     n 1: obscene terms for penis [syn: {prick}, {dick}, {shaft}, {pecker},
           {peter}, {tool}, {putz}]
     2: faucet consisting of a rotating device for regulating flow
        of a liquid [syn: {stopcock}, {turncock}]
     3: the part of a gunlock that strikes the percussion cap when
        the trigger is pulled [syn: {hammer}]
     4: adult male chicken [syn: {rooster}]
     5: adult male bird
     v 1: tilt or slant to one side; "cock one's head"
     2: set the trigger of a firearm back for firing
     3: to walk with a lofty proud gait, often in an attempt to
        impress others; "He struts around like a rooster in a hen
        house" [syn: {swagger}, {ruffle}, ...
