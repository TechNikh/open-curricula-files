---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jung%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 6c6d6612-cfb8-4a9d-b841-8be9ed98f24e
updated: 1484309117
title: "Jung's theorem"
tags:
    - Statement
    - "Jung's theorem in the plane"
    - General metric spaces
categories:
    - Theorems in geometry
---
In geometry, Jung's theorem is an inequality between the diameter of a set of points in any Euclidean space and the radius of the minimum enclosing ball of that set. It is named after Heinrich Jung, who first studied this inequality in 1901.
