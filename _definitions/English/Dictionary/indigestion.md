---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indigestion
offline_file: ""
offline_thumbnail: ""
uuid: 28029752-c933-40d6-86ac-153476da0a51
updated: 1484310383
title: indigestion
categories:
    - Dictionary
---
indigestion
     n : a disorder of digestive function characterized by discomfort
         or heartburn or nausea [syn: {dyspepsia}, {stomach upset},
          {upset stomach}]
