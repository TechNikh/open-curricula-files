---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anecdote
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484475721
title: anecdote
categories:
    - Dictionary
---
anecdote
     n : short account of an incident (especially a biographical one)
