---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banach_fixed-point_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2e8ba51e-a059-487c-829b-9014e0e592e7
updated: 1484309261
title: Banach fixed-point theorem
tags:
    - Statement
    - Proofs
    - "Banach's original proof"
    - Shorter proof
    - Applications
    - Converses
    - Generalizations
    - Notes
categories:
    - Metric geometry
---
In mathematics, the Banach fixed-point theorem (also known as the contraction mapping theorem or contraction mapping principle) is an important tool in the theory of metric spaces; it guarantees the existence and uniqueness of fixed points of certain self-maps of metric spaces, and provides a constructive method to find those fixed points. The theorem is named after Stefan Banach (1892–1945), and was first stated by him in 1922.[1]
