---
version: 1
type: video
provider: YouTube
id: LkAykOv1foc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1332cffd-1651-4b10-8edd-f8e05bd3ad5e
updated: 1486069662
title: Covalent Bonding
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/LkAykOv1foc/default.jpg
    - https://i3.ytimg.com/vi/LkAykOv1foc/1.jpg
    - https://i3.ytimg.com/vi/LkAykOv1foc/2.jpg
    - https://i3.ytimg.com/vi/LkAykOv1foc/3.jpg
---
