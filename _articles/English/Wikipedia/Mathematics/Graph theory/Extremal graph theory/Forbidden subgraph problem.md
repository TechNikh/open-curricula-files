---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forbidden_subgraph_problem
offline_file: ""
offline_thumbnail: ""
uuid: 1e376c77-e0dc-45ac-93ba-218284532450
updated: 1484309406
title: Forbidden subgraph problem
categories:
    - Extremal graph theory
---
In extremal graph theory, the forbidden subgraph problem is the following problem: given a graph G, find the maximal number of edges in an n-vertex graph which does not have a subgraph isomorphic to G. In this context, G is called a forbidden subgraph.[1]
