---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resist
offline_file: ""
offline_thumbnail: ""
uuid: d9e95039-483b-43b4-b480-98ea93c11333
updated: 1484310567
title: resist
categories:
    - Dictionary
---
resist
     v 1: elude, especially in a baffling way; "This behavior defies
          explanation" [syn: {defy}, {refuse}] [ant: {lend oneself}]
     2: stand up or offer resistance to somebody or something [syn:
        {hold out}, {withstand}, {stand firm}] [ant: {surrender}]
     3: express opposition through action or words; "dissent to the
        laws of the country" [syn: {protest}, {dissent}]
     4: withstand the force of something; "The trees resisted her";
        "stand the test of time"; "The mountain climbers had to
        fend against the ice and snow" [syn: {stand}, {fend}]
     5: resist immunologically the introduction of some foreign
        tissue or organ; "His body ...
