---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hadwiger%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1fdbea22-06d6-4b4a-a571-12b3c93c3000
updated: 1484309257
title: "Hadwiger's theorem"
tags:
    - Introduction
    - Valuations
    - Quermassintegrals
    - Statement
    - Corollary
categories:
    - Integral geometry
---
In integral geometry (otherwise called geometric probability theory), Hadwiger's theorem characterises the valuations on convex bodies in Rn. It was proved by Hugo Hadwiger.
