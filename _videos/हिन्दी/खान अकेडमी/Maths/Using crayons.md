---
version: 1
type: video
provider: YouTube
id: DdYJWvniZJo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 79c5e5c9-8c63-4d18-899c-ce1aa88c6956
updated: 1486069613
title: Using crayons
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/DdYJWvniZJo/default.jpg
    - https://i3.ytimg.com/vi/DdYJWvniZJo/1.jpg
    - https://i3.ytimg.com/vi/DdYJWvniZJo/2.jpg
    - https://i3.ytimg.com/vi/DdYJWvniZJo/3.jpg
---
