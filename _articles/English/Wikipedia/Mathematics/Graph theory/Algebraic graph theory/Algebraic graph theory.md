---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: 17613560-82db-4a5b-817a-b96548b67edd
updated: 1484309373
title: Algebraic graph theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_12.png
tags:
    - Branches of algebraic graph theory
    - Using linear algebra
    - Using group theory
    - Studying graph invariants
categories:
    - Algebraic graph theory
---
Algebraic graph theory is a branch of mathematics in which algebraic methods are applied to problems about graphs. This is in contrast to geometric, combinatoric, or algorithmic approaches. There are three main branches of algebraic graph theory, involving the use of linear algebra, the use of group theory, and the study of graph invariants.
