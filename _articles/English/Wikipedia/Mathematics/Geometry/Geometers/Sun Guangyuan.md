---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sun_Guangyuan
offline_file: ""
offline_thumbnail: ""
uuid: 6e694540-b99f-43a3-b796-c6e68d7235a4
updated: 1484309100
title: Sun Guangyuan
categories:
    - Geometers
---
Sun Guangyuan (simplified Chinese: 孙光远; traditional Chinese: 孫光遠; pinyin: Sūn Guāngyuǎn; Wade–Giles: Sun Kuangyüan, 1900–1979), also known as Sun Tang (孫鎕), was a modern Chinese mathematician. He studied projective geometry under Ernest Preston Lane at the University of Chicago[1] and later became a professor in Tsinghua University, Beijing, China. Reportedly he was the first Chinese mathematician to publish a research paper from China in any mathematical journal.
