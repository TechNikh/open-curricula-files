---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/highlands
offline_file: ""
offline_thumbnail: ""
uuid: fc533ae6-0c49-44de-b7df-f1ec28c999e8
updated: 1484310437
title: highlands
categories:
    - Dictionary
---
Highlands
     n : a mountainous region of northern Scotland famous for its
         rugged beauty; known for the style of dress (the kilt and
         tartan) and the clan system (now in disuse) [syn: {Highlands
         of Scotland}]
