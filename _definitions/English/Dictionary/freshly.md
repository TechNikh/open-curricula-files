---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freshly
offline_file: ""
offline_thumbnail: ""
uuid: 8f38e7b9-c3f4-46a0-92f8-106425afcbba
updated: 1484310375
title: freshly
categories:
    - Dictionary
---
freshly
     adv 1: very recently; "they are newly married"; "newly raised
            objections"; "a newly arranged hairdo"; "grass new
            washed by the rain"; "a freshly cleaned floor"; "we
            are fresh out of tomatoes" [syn: {recently}, {newly},
            {fresh}, {new}]
     2: in an impudent or impertinent manner; "a lean, swarthy
        fellow was peering through the window, grinning
        impudently" [syn: {impertinently}, {saucily}, {pertly}, {impudently}]
