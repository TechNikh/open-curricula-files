---
version: 1
type: article
id: https://en.wikipedia.org/wiki/History_of_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 4f601971-8626-4fb5-b279-d475ca4d9b29
updated: 1484309245
title: History of geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_2.png
tags:
    - Early geometry
    - Egyptian geometry
    - Babylonian geometry
    - Vedic India
    - Greek geometry
    - Classical Greek geometry
    - Thales and Pythagoras
    - Plato
    - Hellenistic geometry
    - Euclid
    - Archimedes
    - After Archimedes
    - Classical Indian geometry
    - Chinese geometry
    - The Nine Chapters on the Mathematical Art
    - Islamic Golden Age
    - Renaissance
    - Modern geometry
    - The 17th century
    - The 18th and 19th centuries
    - Non-Euclidean geometry
    - Introduction of mathematical rigor
    - Analysis situs, or topology
    - The 20th century
    - Timeline
    - Notes
categories:
    - History of geometry
---
Geometry (from the Ancient Greek: γεωμετρία; geo- "earth", -metron "measurement") arose as the field of knowledge dealing with spatial relationships. Geometry was one of the two fields of pre-modern mathematics, the other being the study of numbers (arithmetic).
