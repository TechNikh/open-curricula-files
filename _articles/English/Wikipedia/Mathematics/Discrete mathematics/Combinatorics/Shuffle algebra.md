---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shuffle_algebra
offline_file: ""
offline_thumbnail: ""
uuid: b0d42b18-de28-4089-8500-1bc19d385e66
updated: 1484308583
title: Shuffle algebra
tags:
    - Shuffle product
    - Infiltration product
categories:
    - Combinatorics
---
In mathematics, a shuffle algebra is a Hopf algebra with a basis corresponding to words on some set, whose product is given by the shuffle product XшY of two words X, Y: the sum of all ways of interlacing them. The interlacing is given by the riffle shuffle permutation.
