---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crippled
offline_file: ""
offline_thumbnail: ""
uuid: 51dee5e1-e27f-488d-a00c-d96e20f2ff7d
updated: 1484310172
title: crippled
categories:
    - Dictionary
---
crippled
     adj : disabled in the feet or legs; "a crippled soldier"; "a game
           leg" [syn: {halt}, {halting}, {lame}, {game}]
