---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Algebraic_geometry_of_projective_spaces
offline_file: ""
offline_thumbnail: ""
uuid: 7d314a3a-c280-4e88-abe7-0b8ad9c3b33b
updated: 1484309130
title: Algebraic geometry of projective spaces
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_8.png
tags:
    - Homogeneous polynomial ideals
    - Construction of projectivized schemes
    - Divisors and twisting sheaves
    - Classification of vector bundles
    - Important line bundles
    - Morphisms to projective schemes
    - 'An example: the Veronese embeddings'
    - Curves in projective spaces
    - General algebraic geometry
    - General projective geometry
categories:
    - Algebraic geometry
---
Projective space plays a central role in algebraic geometry. The aim of this article is to define the notion in terms of abstract algebraic geometry and to describe some basic uses of projective space.
