---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possibility
offline_file: ""
offline_thumbnail: ""
uuid: 526fa1f9-d133-4b89-b176-71444ac112df
updated: 1484310371
title: possibility
categories:
    - Dictionary
---
possibility
     n 1: a future prospect or potential; "this room has great
          possibilities"
     2: capability of existing or happening or being true; "there is
        a possibility that his sense of smell has been impaired"
        [syn: {possibleness}] [ant: {impossibility}]
     3: a tentative theory about the natural world; a concept that
        is not yet verified but that if true would explain certain
        facts or phenomena; "a scientific hypothesis that survives
        experimental testing becomes a scientific theory"; "he
        proposed a fresh theory of alkalis that later was accepted
        in chemical practices" [syn: {hypothesis}, {theory}]
     4: a possible ...
