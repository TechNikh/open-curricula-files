---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Erlangen_program
offline_file: ""
offline_thumbnail: ""
uuid: c2ca4bac-4a72-4441-a168-9645f1c59e36
updated: 1484309170
title: Erlangen program
tags:
    - The problems of nineteenth century geometry
    - Homogeneous spaces
    - Examples
    - Influence on later work
    - Abstract returns from the Erlangen program
categories:
    - Classical geometry
---
The Erlangen program is a method of characterizing geometries based on group theory and projective geometry. It was published by Felix Klein in 1872 as Vergleichende Betrachtungen über neuere geometrische Forschungen. It is named after the University Erlangen-Nürnberg, where Klein worked.
