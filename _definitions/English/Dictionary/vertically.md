---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vertically
offline_file: ""
offline_thumbnail: ""
uuid: bcebbcec-840b-43bc-90ef-2496ce169609
updated: 1484310220
title: vertically
categories:
    - Dictionary
---
vertically
     adv : in a vertical direction; "a gallery quite often is added to
           make use of space vertically as well as horizontally"
