---
version: 1
type: video
provider: YouTube
id: ZA7KZWt8Eik
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: def22866-73c5-4757-b662-701242b8cd7b
updated: 1486069605
title: Figuring out missing algebraic step
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZA7KZWt8Eik/default.jpg
    - https://i3.ytimg.com/vi/ZA7KZWt8Eik/1.jpg
    - https://i3.ytimg.com/vi/ZA7KZWt8Eik/2.jpg
    - https://i3.ytimg.com/vi/ZA7KZWt8Eik/3.jpg
---
