---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observation
offline_file: ""
offline_thumbnail: ""
uuid: 69da01cf-879c-4497-873f-5273f9523eec
updated: 1484310325
title: observation
categories:
    - Dictionary
---
observation
     n 1: the act of making and recording a measurement
     2: the act of observing; taking a patient look [syn: {observance},
         {watching}]
     3: facts learned by observing; "he reported his observations to
        the mayor"
     4: the act of noticing or paying attention; "he escaped the
        notice of the police" [syn: {notice}, {observance}]
     5: a remark expressing careful consideration [syn: {reflection},
         {reflexion}]
