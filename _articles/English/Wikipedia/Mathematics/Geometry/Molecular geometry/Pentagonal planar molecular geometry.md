---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pentagonal_planar_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 31615987-c1fb-4e2a-8ee7-c81c8de55596
updated: 1484309280
title: Pentagonal planar molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pentagonal-planar-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, the pentagonal planar molecular geometry describes the shape of compounds where five atoms, groups of atoms, or ligands are arranged around a central atom, defining the vertices of a pentagon.
