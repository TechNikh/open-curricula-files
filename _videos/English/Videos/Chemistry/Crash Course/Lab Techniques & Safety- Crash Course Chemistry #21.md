---
version: 1
type: video
provider: YouTube
id: VRWRmIEHr3A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32ad0ea4-6942-480a-b0a3-827946bf9df4
updated: 1486069680
title: 'Lab Techniques & Safety- Crash Course Chemistry #21'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/VRWRmIEHr3A/default.jpg
    - https://i3.ytimg.com/vi/VRWRmIEHr3A/1.jpg
    - https://i3.ytimg.com/vi/VRWRmIEHr3A/2.jpg
    - https://i3.ytimg.com/vi/VRWRmIEHr3A/3.jpg
---
