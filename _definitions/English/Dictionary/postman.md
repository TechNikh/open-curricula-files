---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/postman
offline_file: ""
offline_thumbnail: ""
uuid: b98d6ad5-659b-473e-bfd8-fd6ba5c1aff5
updated: 1484310457
title: postman
categories:
    - Dictionary
---
postman
     n : a man who delivers the mail [syn: {mailman}, {mail carrier},
          {letter carrier}, {carrier}]
