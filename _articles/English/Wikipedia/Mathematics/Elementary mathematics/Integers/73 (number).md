---
version: 1
type: article
id: https://en.wikipedia.org/wiki/73_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 711fe908-5109-4d64-abb9-4a85b13c9891
updated: 1484308970
title: 73 (number)
tags:
    - In mathematics
    - In science
    - In other fields
    - In sports
categories:
    - Integers
---
73 (seventy-three) is the natural number following 72 and preceding 74. In English, it is the smallest natural number with twelve letters in its spelled out name.
