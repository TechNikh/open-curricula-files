---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/covered
offline_file: ""
offline_thumbnail: ""
uuid: ebd9e738-7790-49aa-83a0-38b37d14c437
updated: 1484310212
title: covered
categories:
    - Dictionary
---
covered
     adj 1: overlaid or spread or topped with or enclosed within
            something; sometimes used as a combining form; "women
            with covered faces"; "covered wagons"; "a covered
            balcony" [ant: {uncovered}]
     2: having the head and face covered; "the bride's veiled head";
        "veiled Muslim women"
