---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Michael_Scott_Jacobson
offline_file: ""
offline_thumbnail: ""
uuid: b5318ca7-ee6b-4421-8a89-aff287a93133
updated: 1484309468
title: Michael Scott Jacobson
tags:
    - Early life
    - Current work
    - Mathematics
categories:
    - Graph theorists
---
Michael S. Jacobson is a mathematician, and Professor of Mathematical & Statistical Sciences in the Department of Mathematical & Statistical Science at the University of Colorado Denver. He served as Chair from 2003 - 2012 and is on loan serving as a rotator in EHR/DUE at the National Science Foundation.
