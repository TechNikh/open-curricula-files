---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unbalanced
offline_file: ""
offline_thumbnail: ""
uuid: 10a6a822-203a-4dd8-8937-78a827586a95
updated: 1484310371
title: unbalanced
categories:
    - Dictionary
---
unbalanced
     adj 1: being or thrown out of equilibrium [syn: {imbalanced}] [ant:
             {balanced}]
     2: affected with madness or insanity; "a man who had gone mad"
        [syn: {brainsick}, {crazy}, {demented}, {distracted}, {disturbed},
         {mad}, {sick}, {unhinged}]
     3: debits and credits are not equal
