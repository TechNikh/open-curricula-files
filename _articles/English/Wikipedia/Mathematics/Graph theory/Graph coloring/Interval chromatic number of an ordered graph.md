---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Interval_chromatic_number_of_an_ordered_graph
offline_file: ""
offline_thumbnail: ""
uuid: 9b6305f1-6c89-4dd9-ba80-113a7d18a873
updated: 1484309384
title: Interval chromatic number of an ordered graph
categories:
    - Graph coloring
---
In mathematics, the interval chromatic number X<(H) of an ordered graph H is the minimum number of intervals the (linearly ordered) vertex set of H can be partitioned into so that no two vertices belonging to the same interval are adjacent in H.[1]
