---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inflation
offline_file: ""
offline_thumbnail: ""
uuid: bbe590d0-a3a6-4f5e-b2ff-a541ab09d477
updated: 1484310561
title: inflation
categories:
    - Dictionary
---
inflation
     n 1: a general and progressive increase in prices; "in inflation
          everything gets more valuable except money" [syn: {rising
          prices}] [ant: {deflation}, {disinflation}]
     2: (cosmology) a brief exponential expansion of the universe
        (faster than the speed of light) postulated to have
        occurred shortly after the big bang
     3: lack of elegance as a consequence of being pompous and
        puffed up with vanity [syn: {ostentation}, {ostentatiousness},
         {pomposity}, {pompousness}, {pretentiousness}, {splashiness}]
     4: the act of filling something with air [ant: {deflation}]
