---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neither
offline_file: ""
offline_thumbnail: ""
uuid: 9995ea54-52f0-4166-ba11-5b4ea58943f6
updated: 1484310309
title: neither
categories:
    - Dictionary
---
neither
     adv : after a negative statement used to indicate that the next
           statement is similarly negative; "I was not happy and
           neither were they"; "just as you would not complain,
           neither should he"
