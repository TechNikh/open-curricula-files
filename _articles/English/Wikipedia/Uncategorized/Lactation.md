---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lactating
offline_file: ""
offline_thumbnail: ""
uuid: 4a86eb71-2ea9-4aa5-a430-be0c7fb8a3f1
updated: 1484094302
title: Lactation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-White_Cat_Nursing_Four_Kittens_HQ.jpg
tags:
    - Purpose
    - human
    - Hormonal influences
    - Secretory differentiation
    - Secretory activation
    - 'Autocrine control - Galactapoiesis'
    - Milk ejection reflex
    - Milk ejection reflex mechanism
    - Afterpains
    - Without pregnancy, induced lactation, relactation
categories:
    - Uncategorized
---
Lactation describes the secretion of milk from the mammary glands and the period of time that a mother lactates to feed her young. The process can occur with all post-pregnancy female mammals, although it predates mammals.[1] In humans the process of feeding milk is also called breastfeeding or nursing.
