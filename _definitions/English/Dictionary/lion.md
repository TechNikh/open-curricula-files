---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lion
offline_file: ""
offline_thumbnail: ""
uuid: 18f69947-5596-4092-a2e0-eb2aef2d1bba
updated: 1484310168
title: lion
categories:
    - Dictionary
---
lion
     n 1: large gregarious predatory feline of Africa and India having
          a tawny coat with a shaggy mane in the male [syn: {king
          of beasts}, {Panthera leo}]
     2: a celebrity who is lionized (much sought after) [syn: {social
        lion}]
     3: (astrology) a person who is born while the sun is in Leo
        [syn: {Leo}]
     4: the fifth sign of the zodiac; the sun is in this sign from
        about July 23 to August 22 [syn: {Leo}, {Leo the Lion}]
