---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_system
offline_file: ""
offline_thumbnail: ""
uuid: 72847630-2a46-4e48-aa53-e1ab418355cc
updated: 1484308735
title: Critical system
tags:
    - General description
    - Classification
    - Safety critical
    - Mission critical
    - Business critical
    - Security critical
    - Notes
categories:
    - Control engineering
---
There are four types of critical systems: safety critical, mission critical, business critical and security critical.[1]
