---
version: 1
type: video
provider: YouTube
id: oqI4skjr6lQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Cytotoxic%20T%20Cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7824f38f-4bbb-42e9-9119-0712ef53aaa6
updated: 1486069583
title: Cytotoxic T Cells
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/oqI4skjr6lQ/default.jpg
    - https://i3.ytimg.com/vi/oqI4skjr6lQ/1.jpg
    - https://i3.ytimg.com/vi/oqI4skjr6lQ/2.jpg
    - https://i3.ytimg.com/vi/oqI4skjr6lQ/3.jpg
---
How cytotoxic T cells get activated by MHC-I/antigen complexes and then proceed to kill infected cells
More free lessons at: http://www.khanacademy.org/video?v=oqI4skjr6lQ
