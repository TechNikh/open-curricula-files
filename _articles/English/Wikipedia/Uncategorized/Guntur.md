---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Guntur
offline_file: ""
offline_thumbnail: ""
uuid: 6323c668-fd71-447a-8188-be4fc02097c8
updated: 1484308440
title: Guntur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Nagalipi1.jpg
tags:
    - Etymology
    - History
    - geography
    - Climate
    - geography
    - Demographics
    - Governance
    - Civic administration
    - Healthcare
    - Economy
    - Agro exports
    - Industrialization
    - culture
    - Cuisine
    - Cityscape
    - Transport
    - Public transport
    - Roadways
    - Railways
    - air
    - Education and research
    - Sports
categories:
    - Uncategorized
---
Guntur ( pronunciation (help·info)); is a city in the Guntur district of the Indian state of Andhra Pradesh. It is a municipal corporation and the administrative headquarters for Guntur district.[5] It is also the mandal headquarters of Guntur mandal in Guntur revenue division.[6] The city is the second largest by area and third most populous in the state.[3] with a population of 743,354 and with an urban agglomeration population of 1,028,667.[3][7] The city is situated on the plains and located 40 miles (64 km) to north of the Bay of Bengal. River Krishna is the main source of water for the city through channels and tributaries.[8]
