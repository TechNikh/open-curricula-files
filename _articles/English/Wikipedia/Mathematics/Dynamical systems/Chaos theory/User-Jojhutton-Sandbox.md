---
version: 1
type: article
id: https://en.wikipedia.org/wiki/User:Jojhutton/Sandbox
offline_file: ""
offline_thumbnail: ""
uuid: 459572d4-24e4-4917-823d-5b23ce72bf70
updated: 1484308699
title: User:Jojhutton/Sandbox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lorenz_attractor_yb.svg.png
tags:
    - Films
    - Literature and print
    - Interactive media
    - Television
    - Music
    - Notes
categories:
    - Chaos theory
---
The butterfly effect is the phenomenon in chaos theory whereby a minor change in circumstances can cause a large change in outcome. The butterfly metaphor, created by Edward Norton Lorenz to emphasize the inherent unpredictability of events, was taken up by popular culture, and interpreted to mean that each event could be explained by some small cause.[1] The term is found in many areas of popular culture including the names of films and of pop groups.
