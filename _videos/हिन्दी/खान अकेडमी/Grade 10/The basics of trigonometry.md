---
version: 1
type: video
provider: YouTube
id: nZUepRPIsQg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2e98f1fb-7cab-43ec-9309-8d2acc98b9e3
updated: 1486069618
title: The basics of trigonometry
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/nZUepRPIsQg/default.jpg
    - https://i3.ytimg.com/vi/nZUepRPIsQg/1.jpg
    - https://i3.ytimg.com/vi/nZUepRPIsQg/2.jpg
    - https://i3.ytimg.com/vi/nZUepRPIsQg/3.jpg
---
