---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thud
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580121
title: thud
categories:
    - Dictionary
---
thud
     n : a heavy dull sound (as made by impact of heavy objects)
         [syn: {thump}, {thumping}, {clump}, {clunk}]
     v 1: make a dull sound; "the knocker thudded against the front
          door" [syn: {thump}]
     2: strike with a dull sound; "Bullets were thudding against the
        wall"
     3: make a noise typical of an engine lacking lubricants [syn: {crump},
         {scrunch}]
     [also: {thudding}, {thudded}]
