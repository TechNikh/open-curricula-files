---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rankine_vortex
offline_file: ""
offline_thumbnail: ""
uuid: 05c5505b-b31c-47c1-9a27-27db71626841
updated: 1484308883
title: Rankine vortex
categories:
    - Vortices
---
A swirling flow in a viscous fluid can be characterized by a forced vortex in its central core, surrounded by a free vortex. In an inviscid fluid, on the other hand, a swirling flow consists entirely of the free vortex with a singularity at its center point instead of the forced vortex core. The tangential velocity[1] of a Rankine vortex with circulation 
  
    
      
        Γ
      
    
    {\displaystyle \Gamma }
  
 and radius 
  
    
      
        R
      
    
    {\displaystyle R}
  
 is
