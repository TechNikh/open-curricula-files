---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ear
offline_file: ""
offline_thumbnail: ""
uuid: 1a1c6903-70cf-4745-9d1c-f2818c533705
updated: 1484310309
title: ear
categories:
    - Dictionary
---
ear
     n 1: the sense organ for hearing and equilibrium
     2: good hearing; "he had a keen ear"; "a good ear for pitch"
     3: the externally visible cartilaginous structure of the
        external ear [syn: {auricle}, {pinna}]
     4: attention to what is said; "he tried to get her ear"
     5: fruiting spike of a cereal plant especially corn [syn: {spike},
         {capitulum}]
