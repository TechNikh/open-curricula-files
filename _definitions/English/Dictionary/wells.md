---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wells
offline_file: ""
offline_thumbnail: ""
uuid: c860d3f3-70a3-46b1-83d6-7a4c05169d92
updated: 1484310266
title: wells
categories:
    - Dictionary
---
Wells
     n : prolific English writer best known for his science-fiction
         novels; he also wrote on contemporary social problems and
         wrote popular accounts of history and science (1866-1946)
         [syn: {H. G. Wells}, {Herbert George Wells}]
