---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/replenish
offline_file: ""
offline_thumbnail: ""
uuid: 385af0ca-1ae3-4ff9-9856-422265837e9c
updated: 1484310242
title: replenish
categories:
    - Dictionary
---
replenish
     v : fill something that had previously been emptied; "refill my
         glass, please" [syn: {refill}, {fill again}]
