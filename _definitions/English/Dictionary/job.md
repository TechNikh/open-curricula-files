---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/job
offline_file: ""
offline_thumbnail: ""
uuid: 2affe434-16af-4b2d-96e9-5ec2cfd7ed0f
updated: 1484310443
title: job
categories:
    - Dictionary
---
job
     n 1: the principal activity in your life that you do to earn
          money; "he's not in my line of business" [syn: {occupation},
           {business}, {line of work}, {line}]
     2: a specific piece of work required to be done as a duty or
        for a specific fee; "estimates of the city's loss on that
        job ranged as high as a million dollars"; "the job of
        repairing the engine took several hours"; "the endless
        task of classifying the samples"; "the farmer's morning
        chores" [syn: {task}, {chore}]
     3: the performance of a piece of work; "she did an outstanding
        job as Ophelia"; "he gave it up as a bad job"
     4: the responsibility to ...
