---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468941
title: fry
categories:
    - Dictionary
---
Fry
     n 1: English painter and art critic (1866-1934) [syn: {Roger Fry},
           {Roger Eliot Fry}]
     2: English dramatist noted for his comic verse dramas (born
        1907) [syn: {Christopher Fry}]
     3: a young person of either sex; "she writes books for
        children"; "they're just kids"; "`tiddler' is a British
        term for youngsters" [syn: {child}, {kid}, {youngster}, {minor},
         {shaver}, {nipper}, {small fry}, {tiddler}, {tike}, {tyke},
         {nestling}]
     v 1: be excessively hot; "If the children stay out on the beach
          for another hour, they'll be fried"
     2: cook on a hot surface using fat; "fry the pancakes"
     3: kill by ...
