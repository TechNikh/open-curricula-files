---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bifurcation_memory
offline_file: ""
offline_thumbnail: ""
uuid: 28e6e5c8-8bbb-4107-8dd8-41803acaf439
updated: 1484308797
title: Bifurcation memory
tags:
    - General information
    - The known definitions
    - History of studying
    - Topicality
    - Notes
categories:
    - Non-equilibrium thermodynamics
---
