---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cox%E2%80%93Zucker_machine'
offline_file: ""
offline_thumbnail: ""
uuid: 7159d1ac-b153-465d-8e77-bfa0d0ec6c3c
updated: 1484309360
title: Cox–Zucker machine
categories:
    - Geometry stubs
---
The Cox–Zucker machine is an algorithm created by David A. Cox and Steven Zucker. This algorithm determines if a given set of sections provides a basis (up to torsion) for the Mordell–Weil group of an elliptic surface E → S where S is isomorphic to the projective line.
