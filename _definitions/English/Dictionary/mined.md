---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mined
offline_file: ""
offline_thumbnail: ""
uuid: 955a8d08-3a76-4731-b977-815ef5d1a750
updated: 1484310383
title: mined
categories:
    - Dictionary
---
mined
     adj : extracted from a source of supply as of minerals from the
           earth [ant: {unmined}]
