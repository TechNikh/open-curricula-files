---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wounded
offline_file: ""
offline_thumbnail: ""
uuid: 19b44031-e89d-4afe-84ab-b694fffee578
updated: 1484310315
title: wounded
categories:
    - Dictionary
---
wounded
     adj : suffering from physical injury especially that suffered in
           battle; "nursing his wounded arm"; "ambulances...for
           the hurt men and women" [syn: {hurt}]
     n : people who are wounded; "they had to leave the wounded where
         they fell" [syn: {maimed}]
