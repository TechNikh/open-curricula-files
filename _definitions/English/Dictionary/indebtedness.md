---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indebtedness
offline_file: ""
offline_thumbnail: ""
uuid: c69ec97c-dca4-4a04-93ea-f5345b1164ed
updated: 1484310571
title: indebtedness
categories:
    - Dictionary
---
indebtedness
     n : an obligation to pay money to another party [syn: {liability},
          {financial obligation}]
