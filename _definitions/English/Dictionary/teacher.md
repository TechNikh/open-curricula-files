---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teacher
offline_file: ""
offline_thumbnail: ""
uuid: aa2aa62b-ab2c-4eaa-a409-9583ae9964b7
updated: 1484310283
title: teacher
categories:
    - Dictionary
---
teacher
     n 1: a person whose occupation is teaching [syn: {instructor}]
     2: a personified abstraction that teaches; "books were his
        teachers"; "experience is a demanding teacher"
