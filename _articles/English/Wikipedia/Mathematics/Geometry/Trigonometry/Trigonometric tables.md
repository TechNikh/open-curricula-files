---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_tables
offline_file: ""
offline_thumbnail: ""
uuid: 14390384-564d-4834-9bff-09b4bc51ca6f
updated: 1484309350
title: Trigonometric tables
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Bernegger_Manuale_137.jpg
tags:
    - On-demand computation
    - Half-angle and angle-addition formulas
    - A quick, but inaccurate, approximation
    - A better, but still imperfect, recurrence formula
categories:
    - Trigonometry
---
In mathematics, tables of trigonometric functions are useful in a number of areas. Before the existence of pocket calculators, trigonometric tables were essential for navigation, science and engineering. The calculation of mathematical tables was an important area of study, which led to the development of the first mechanical computing devices.
