---
version: 1
type: video
provider: YouTube
id: tF81t07MKrA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 87a940ca-494d-4302-a266-d7ee6bd0deae
updated: 1486069621
title: 'Surface Area  Volume  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/tF81t07MKrA/default.jpg
    - https://i3.ytimg.com/vi/tF81t07MKrA/1.jpg
    - https://i3.ytimg.com/vi/tF81t07MKrA/2.jpg
    - https://i3.ytimg.com/vi/tF81t07MKrA/3.jpg
---
