---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inhospitable
offline_file: ""
offline_thumbnail: ""
uuid: c933d089-997b-46f6-a77e-cea947ecd41f
updated: 1484310543
title: inhospitable
categories:
    - Dictionary
---
inhospitable
     adj 1: unfavorable to life or growth; "the barren inhospitable
            desert"; "inhospitable mountain areas" [ant: {hospitable}]
     2: not hospitable; "they are extremely inhospitable these
        days"; "her greeting was cold and inhospitable" [ant: {hospitable}]
