---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Prisoner%27s_dilemma'
offline_file: ""
offline_thumbnail: ""
uuid: ac0d96e6-a184-4e02-9c71-1f65bd203024
updated: 1484309057
title: "Prisoner's dilemma"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dilemne_du_prisonnier.svg.png
tags:
    - "Strategy for the prisoner's dilemma"
    - Generalized form
    - 'Special case: Donation game'
    - "The iterated prisoner's dilemma"
    - "Strategy for the iterated prisoner's dilemma"
    - "Stochastic iterated prisoner's dilemma"
    - Zero-determinant strategies
    - "Continuous iterated prisoner's dilemma"
    - Emergence of Stable Strategies
    - Real-life examples
    - In environmental studies
    - In animals
    - In psychology
    - In economics
    - In sport
    - Multiplayer dilemmas
    - In international politics
    - Related games
    - Closed-bag exchange
    - Friend or Foe?
    - Iterated snowdrift
    - Software
    - In fiction
categories:
    - Non-cooperative games
---
The prisoner's dilemma is a standard example of a game analyzed in game theory that shows why two completely "rational" individuals might not cooperate, even if it appears that it is in their best interests to do so. It was originally framed by Merrill Flood and Melvin Dresher working at RAND in 1950. Albert W. Tucker formalized the game with prison sentence rewards and named it, "prisoner's dilemma" (Poundstone, 1992), presenting it as follows:
