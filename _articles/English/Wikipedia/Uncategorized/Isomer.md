---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isomerism
offline_file: ""
offline_thumbnail: ""
uuid: 0846519a-3ddb-434e-a600-93bd60a5967e
updated: 1484308394
title: Isomer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Isomerism.svg.png
tags:
    - Structural isomers
    - 'Example: propanols and methoxyethane'
    - 'Example: propadiene and propyne'
    - Stereoisomers
    - Isomerization
    - Medicinal chemistry
    - History
    - Other types of isomerism
categories:
    - Uncategorized
---
An isomer (/ˈaɪsəmər/; from Greek ἰσομερής, isomerès; isos = "equal", méros = "part") is a molecule with the same molecular formula as another molecule, but with a different chemical structure. That is, isomers contain the same number of atoms of each element, but have different arrangements of their atoms.[1][2] Isomers do not necessarily share similar properties, unless they also have the same functional groups. There are two main forms of isomerism (/ˈaɪsəmərɪzm/ or /aɪˈsɒmərɪzm/): structural isomerism (or constitutional isomerism) and stereoisomerism (or spatial isomerism).
