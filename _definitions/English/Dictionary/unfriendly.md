---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfriendly
offline_file: ""
offline_thumbnail: ""
uuid: 4f3246b8-c766-4875-8e8d-ce64aaf896cd
updated: 1484310180
title: unfriendly
categories:
    - Dictionary
---
unfriendly
     adj 1: not easy to understand or use; "user-unfriendly" [ant: {friendly}]
     2: not disposed to friendship or friendliness; "an unfriendly
        coldness of manner"; "an unfriendly action to take" [ant:
        {friendly}]
     3: lacking warmth of feeling; "a chilly greeting"; "an
        unfriendly manner" [syn: {chilly}]
     4: not friendly; "an unfriendly act of aggression"; "an
        inimical critic" [syn: {inimical}]
     5: very unfavorable to life or growth; "a hostile climate"; "an
        uncongenial atmosphere"; "an uncongenial soil"; "the
        unfriendly environment at high altitudes" [syn: {hostile},
         {uncongenial}]
     [also: {unfriendliest}, ...
