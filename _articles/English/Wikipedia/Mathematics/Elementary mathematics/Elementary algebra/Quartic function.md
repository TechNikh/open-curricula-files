---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quartic_function
offline_file: ""
offline_thumbnail: ""
uuid: db2ef474-ce19-40fb-bff5-653c3b318e04
updated: 1484308918
title: Quartic function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/233px-Polynomialdeg4.svg.png
tags:
    - History
    - Applications
    - Inflection points and golden ratio
    - Solving a quartic equation
    - Nature of the roots
    - General formula for roots
    - Special cases of the formula
    - Simpler cases
    - Reducible quartics
    - Biquadratic equation
    - Quasi-palindromic equation
    - Solution methods
    - Converting to a depressed quartic
    - "Ferrari's solution"
    - "Descartes' solution"
    - "Euler's solution"
    - Solving by Lagrange resolvent
    - Solving with algebraic geometry
categories:
    - Elementary algebra
---
where a is nonzero, which is defined by a polynomial of degree four, called quartic polynomial.
