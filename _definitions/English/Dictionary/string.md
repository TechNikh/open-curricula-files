---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/string
offline_file: ""
offline_thumbnail: ""
uuid: 662b057c-4283-49f6-a915-df72ffc17873
updated: 1484310313
title: string
categories:
    - Dictionary
---
string
     n 1: a lightweight cord [syn: {twine}]
     2: stringed instruments that are played with a bow; "the
        strings played superlatively well" [syn: {bowed stringed
        instrument}]
     3: a tightly stretched cord of wire or gut, which makes sound
        when plucked, struck, or bowed
     4: a sequentially ordered set of things or events or ideas in
        which each successive member is related to the preceding;
        "a string of islands"; "train of mourners"; "a train of
        thought" [syn: {train}]
     5: a linear sequence of symbols (characters or words or
        phrases)
     6: a tie consisting of a cord that goes through a seam around
        an opening; ...
