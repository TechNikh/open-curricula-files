---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evaluate
offline_file: ""
offline_thumbnail: ""
uuid: a328b4f4-f585-4adc-84eb-7c70e2f0cd92
updated: 1484310218
title: evaluate
categories:
    - Dictionary
---
evaluate
     v : place a value on; judge the worth of something; "I will have
         the family jewels appraised by a professional" [syn: {measure},
          {valuate}, {assess}, {appraise}, {value}]
