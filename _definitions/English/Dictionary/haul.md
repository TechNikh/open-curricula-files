---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haul
offline_file: ""
offline_thumbnail: ""
uuid: db82441e-7c1c-4d41-aa1f-cf4677ab2b1d
updated: 1484310581
title: haul
categories:
    - Dictionary
---
haul
     n 1: the act of drawing or hauling something; "the haul up the
          hill went very slowly" [syn: {draw}, {haulage}]
     2: the quantity that was caught; "the catch was only 10 fish"
        [syn: {catch}]
     v 1: draw slowly or heavily; "haul stones"; "haul nets" [syn: {hale},
           {cart}, {drag}]
     2: transport in a vehicle; "haul stones from the quarry in a
        truck"; "haul vegetables to the market"
