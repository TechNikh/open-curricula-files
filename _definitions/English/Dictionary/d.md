---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/d
offline_file: ""
offline_thumbnail: ""
uuid: fe8b1af3-b3d4-48d9-a000-ac9e9684d802
updated: 1484310138
title: d
categories:
    - Dictionary
---
d
     adj : denoting a quantity consisting of 500 items or units [syn: {five
           hundred}, {500}]
     n 1: a fat-soluble vitamin that prevents rickets [syn: {vitamin D},
           {calciferol}, {viosterol}, {ergocalciferol}, {cholecarciferol}]
     2: the cardinal number that is the product of one hundred and
        five [syn: {five hundred}, {500}]
     3: the 4th letter of the Roman alphabet
