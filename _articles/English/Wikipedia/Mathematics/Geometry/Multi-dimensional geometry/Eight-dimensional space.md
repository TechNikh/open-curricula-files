---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eight-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: f11bb66c-9ffc-496d-95eb-7316861df56d
updated: 1484309283
title: Eight-dimensional space
tags:
    - Geometry
    - 8-polytope
    - 7-sphere
    - Kissing number problem
    - Octonions
    - Biquaternions
categories:
    - Multi-dimensional geometry
---
In mathematics, a sequence of n real numbers can be understood as a location in n-dimensional space. When n = 8, the set of all such locations is called 8-dimensional space. Often such spaces are studied as vector spaces, without any notion of distance. Eight-dimensional Euclidean space is eight-dimensional space equipped with a Euclidean metric, which is defined by the dot product.[dubious – discuss]
