---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Schnorr_group
offline_file: ""
offline_thumbnail: ""
uuid: b5d9e918-44d6-45e8-84db-0a38424bb54b
updated: 1484308652
title: Schnorr group
categories:
    - Number theory
---
A Schnorr group, proposed by Claus P. Schnorr, is a large prime-order subgroup of 
  
    
      
        
          
            Z
          
          
            p
          
          
            ×
          
        
      
    
    {\displaystyle \mathbb {Z} _{p}^{\times }}
  
, the multiplicative group of integers modulo 
  
    
      
        p
      
    
    {\displaystyle p}
  
 for some prime 
  
    
      
        p
      
    
    {\displaystyle p}
  
. To generate such a group, generate 
  
    
      
        p
      
    
    {\displaystyle p}
  
, 
  
    
      
        q
      
    
    {\displaystyle q}
  
, 
  
    
      
        r
      
    
    {\displaystyle ...
