---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bolus
offline_file: ""
offline_thumbnail: ""
uuid: 31f86e8e-a07f-4358-ba23-eb052aa05b32
updated: 1484310333
title: bolus
categories:
    - Dictionary
---
bolus
     n 1: a small round soft mass (as of chewed food)
     2: a large pill; used especially in veterinary medicine
