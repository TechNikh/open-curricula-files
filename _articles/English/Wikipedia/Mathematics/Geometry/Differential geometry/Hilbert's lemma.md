---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 68d0745c-3837-4df6-9e8f-15ddd8fc2eb0
updated: 1484309224
title: "Hilbert's lemma"
categories:
    - Differential geometry
---
Hilbert's lemma was proposed at the end of the 19th century by mathematician David Hilbert. The lemma describes a property of the principal curvatures of surfaces. It may be used to prove Liebmann's theorem that a compact surface with constant Gaussian curvature must be a sphere.[1]
