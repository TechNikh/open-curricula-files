---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skymind
offline_file: ""
offline_thumbnail: ""
uuid: 694d447a-fba8-4dc5-9747-4b8ab8337231
updated: 1484309006
title: Skymind
tags:
    - Technology
    - Applications and Use Cases
    - Skymind Intelligence Layer (SKIL)
    - Deeplearning4j
    - ND4J
    - DataVec
    - JavaCPP
categories:
    - Game artificial intelligence
---
Skymind is a machine intelligence company supporting the open source deep learning framework Deeplearning4j and the JVM-based scientific computing library ND4J. The company was founded in 2014 by Adam Gibson and Chris Nicholson, and is headquartered in San Francisco, California. It is privately funded, and raised $3 million in seed financing in 2016.[2] [3]
