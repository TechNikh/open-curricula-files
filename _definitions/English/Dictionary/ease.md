---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ease
offline_file: ""
offline_thumbnail: ""
uuid: 3ee2ca6d-99e3-4980-abe5-ae9456cd79df
updated: 1484310527
title: ease
categories:
    - Dictionary
---
ease
     n 1: freedom from difficulty or hardship or effort; "he rose
          through the ranks with apparent ease"; "they put it into
          containers for ease of transportation" [syn: {easiness},
           {simplicity}] [ant: {difficulty}]
     2: a freedom from financial difficulty that promotes a
        comfortable state; "a life of luxury and ease"; "he had
        all the material comforts of this world" [syn: {comfort}]
     3: the condition of being comfortable or relieved (especially
        after being relieved of distress); "he enjoyed his relief
        from responsibility"; "getting it off his conscience gave
        him some ease" [syn: {relief}]
     4: freedom from ...
