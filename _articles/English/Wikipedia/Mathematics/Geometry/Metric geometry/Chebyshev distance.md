---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chebyshev_distance
offline_file: ""
offline_thumbnail: ""
uuid: e80dee52-9f7e-4ec6-a90b-1bdd3cdff129
updated: 1484309261
title: Chebyshev distance
tags:
    - Definition
    - Properties
categories:
    - Metric geometry
---
In mathematics, Chebyshev distance (or Tchebychev distance), maximum metric, or L∞ metric[1] is a metric defined on a vector space where the distance between two vectors is the greatest of their differences along any coordinate dimension.[2] It is named after Pafnuty Chebyshev.
