---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homography
offline_file: ""
offline_thumbnail: ""
uuid: 02492961-f318-4f2b-947d-3096665dd10b
updated: 1484309336
title: Homography
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Projection_geometry.svg.png
tags:
    - Geometric motivation
    - Definition and expression in homogeneous coordinates
    - Homographies of a projective line
    - Projective frame and coordinates
    - Central collineations
    - Fundamental theorem of projective geometry
    - Homography groups
    - Cross-ratio
    - Over a ring
    - Periodic homographies
    - Notes
categories:
    - Transformation (function)
---
In projective geometry, a homography is an isomorphism of projective spaces, induced by an isomorphism of the vector spaces from which the projective spaces derive.[1] It is a bijection that maps lines to lines, and thus a collineation. In general, some collineations are not homographies, but the fundamental theorem of projective geometry asserts that is not so in the case of real projective spaces of dimension at least two. Synonyms include projectivity, projective transformation, and projective collineation.
