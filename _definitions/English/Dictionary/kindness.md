---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kindness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619781
title: kindness
categories:
    - Dictionary
---
kindness
     n 1: the quality of being warm-hearted and considerate and humane
          and sympathetic [ant: {unkindness}]
     2: tendency to be kind and forgiving [syn: {forgivingness}]
     3: a kind act [syn: {benignity}]
