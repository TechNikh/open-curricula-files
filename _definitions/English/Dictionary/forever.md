---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forever
offline_file: ""
offline_thumbnail: ""
uuid: 1850f021-a7e5-427f-a993-3cedeb95d96e
updated: 1484310254
title: forever
categories:
    - Dictionary
---
forever
     adv 1: for a limitless time; "no one can live forever"; "brightly
            beams our Father's mercy from his lighthouse
            evermore"- P.P.Bliss [syn: {everlastingly}, {eternally},
             {evermore}]
     2: seemingly without interruption; often and repeatedly;
        "always looking for faults"; "it is always raining"; "he
        is forever cracking jokes"; "they are forever arguing"
        [syn: {always}]
     3: for a very long or seemingly endless time; "she took forever
        to write the paper"; "we had to wait forever and a day"
        [syn: {forever and a day}]
