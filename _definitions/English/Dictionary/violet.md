---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violet
offline_file: ""
offline_thumbnail: ""
uuid: f9f0be44-a8be-42da-acb6-49f38c7a1809
updated: 1484310307
title: violet
categories:
    - Dictionary
---
violet
     adj : of a color midway between red and blue [syn: {purple}, {purplish}]
     n 1: any of numerous low-growing small-flowered violas
     2: a variable color that lies beyond blue in the spectrum [syn:
         {reddish blue}]
