---
version: 1
type: video
provider: YouTube
id: Y6JsEja15Vk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Example%202-%20Solving%20systems%20by%20elimination.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a1170daf-5ebc-400c-bb79-3442290c909f
updated: 1486069557
title: 'Example 2- Solving systems by elimination'
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y6JsEja15Vk/default.jpg
    - https://i3.ytimg.com/vi/Y6JsEja15Vk/1.jpg
    - https://i3.ytimg.com/vi/Y6JsEja15Vk/2.jpg
    - https://i3.ytimg.com/vi/Y6JsEja15Vk/3.jpg
---
