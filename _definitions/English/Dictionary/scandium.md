---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scandium
offline_file: ""
offline_thumbnail: ""
uuid: a89deac0-517f-4600-95a1-ea563c9dea96
updated: 1484310397
title: scandium
categories:
    - Dictionary
---
scandium
     n : a white trivalent metallic element; sometimes classified in
         the rare earth group; occurs in the Scandinavian mineral
         thortveitite [syn: {Sc}, {atomic number 21}]
