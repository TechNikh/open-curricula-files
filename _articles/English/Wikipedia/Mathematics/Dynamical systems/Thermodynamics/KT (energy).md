---
version: 1
type: article
id: https://en.wikipedia.org/wiki/KT_(energy)
offline_file: ""
offline_thumbnail: ""
uuid: 86898fa9-6ae9-4e09-bbb4-07c4d723585b
updated: 1484308855
title: KT (energy)
categories:
    - Thermodynamics
---
kT is the product of the Boltzmann constant, k, and the temperature, T. This product is used in physics as a scaling factor for energy values in molecular-scale systems (sometimes it is used as a unit of energy), as the rates and frequencies of many processes and phenomena depend not on their energy alone, but on the ratio of that energy and kT, that is, on E / kT (see Arrhenius equation, Boltzmann factor). For a system in equilibrium in canonical ensemble, the probability of the system being in state with energy E is proportional to e−ΔE / kT.
