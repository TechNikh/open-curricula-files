---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operative
offline_file: ""
offline_thumbnail: ""
uuid: 978c0bb4-b778-483b-9dce-bd45ca2efcf1
updated: 1484310188
title: operative
categories:
    - Dictionary
---
operative
     adj 1: being in force or having or exerting force; "operative
            regulations"; "the major tendencies operative in the
            American political system" [ant: {inoperative}]
     2: of or relating to a surgical operation; "operative surgery"
     3: relating to or requiring or amenable to treatment by surgery
        especially as opposed to medicine; "a surgical appendix";
        "a surgical procedure"; "operative dentistry" [syn: {surgical}]
        [ant: {medical}]
     4: effective; producing a desired effect; "the operative word"
        [syn: {key}]
     5: (of e.g. a machine) performing or capable of performing; "in
        running (or working) order"; "a ...
