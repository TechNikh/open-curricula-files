---
version: 1
type: video
provider: YouTube
id: s9i2aVKyxTY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Simplifying%20Radical%20Expressions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3ed28627-39e5-414e-99ee-5aecb44438a0
updated: 1486069559
title: Simplifying Radical Expressions 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/s9i2aVKyxTY/default.jpg
    - https://i3.ytimg.com/vi/s9i2aVKyxTY/1.jpg
    - https://i3.ytimg.com/vi/s9i2aVKyxTY/2.jpg
    - https://i3.ytimg.com/vi/s9i2aVKyxTY/3.jpg
---
