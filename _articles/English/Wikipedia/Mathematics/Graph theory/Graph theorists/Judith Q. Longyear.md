---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Judith_Q._Longyear
offline_file: ""
offline_thumbnail: ""
uuid: f806504b-9542-474e-9876-aad9b096f119
updated: 1484309472
title: Judith Q. Longyear
categories:
    - Graph theorists
---
Judith Querida Longyear (20 September 1938–13 December 1995[1]) was an American mathematician and professor whose research interests included graph theory and combinatorics. Longyear earned her Ph.D. from Pennsylvania State University 1972, under the supervision of Sarvadaman Chowla, with a thesis entitled Tactical Configurations.[2][3] She taught mathematics at several universities including California Institute of Technology,[4] Dartmouth College[5] and Wayne State University.[6] She worked on nested block designs[4][7] and Hadamard matrices.[8]
