---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wastewater
offline_file: ""
offline_thumbnail: ""
uuid: fedca571-2fb6-47f6-8af8-5c0407ad7a40
updated: 1484308337
title: Wastewater
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2012-05-28_Fotoflug_Cuxhaven_Wilhelmshaven_DSCF9867_%2528crop%2529.jpg'
tags:
    - Origin
    - Constituents
    - Quality indicators
    - Disposal
    - Treatment
    - Reuse
    - Reuse in gardening and agriculture
    - Health risks of polluted irrigation water
    - Etymology
    - Legislation
    - European Union
    - United States
    - Philippines
categories:
    - Uncategorized
---
Wastewater, also written as waste water, is any water that has been adversely affected in quality by anthropogenic influence. Wastewater can originate from a combination of domestic, industrial, commercial or agricultural activities, surface runoff or stormwater, and from sewer inflow or infiltration.[1]
