---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_group
offline_file: ""
offline_thumbnail: ""
uuid: ed6db921-aec0-4db4-b207-123b162f8f80
updated: 1484309185
title: Conformal group
categories:
    - Conformal geometry
---
In mathematics, the conformal group of a space is the group of transformations from the space to itself that preserve angles. More formally, it is the group of transformations that preserve the conformal geometry of the space.
