---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bidding
offline_file: ""
offline_thumbnail: ""
uuid: 809407c3-a717-495c-a701-7eddb099a4f2
updated: 1484309011
title: Bidding
tags:
    - Topics
    - Online Bidding
    - Timed Bidding
    - Bidding in Procurement Initiatives
    - Bidding off the Wall
categories:
    - Auctions
---
Bidding is an offer (often competitive) to set a price by an individual or business for a product or service or a demand that something be done.[1] Bidding is used to determine the cost or value of something.
