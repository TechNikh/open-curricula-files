---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depreciation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484333101
title: depreciation
categories:
    - Dictionary
---
depreciation
     n 1: a decrease in price or value; "depreciation of the dollar
          against the yen" [ant: {appreciation}]
     2: decrease in value of an asset due to obsolescence or use
        [syn: {wear and tear}]
     3: a communication that belittles somebody or something [syn: {disparagement},
         {derogation}]
