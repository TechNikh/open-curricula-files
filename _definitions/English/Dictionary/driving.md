---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/driving
offline_file: ""
offline_thumbnail: ""
uuid: 78d0942a-2584-48d2-9d06-f3b950ec6fbf
updated: 1484310210
title: driving
categories:
    - Dictionary
---
driving
     adj 1: having the power of driving or impelling; "a driving
            personal ambition"; "the driving force was his innate
            enthusiasm"; "an impulsive force" [syn: {impulsive}]
     2: acting with vigor; "responsibility turned the spoiled
        playboy into a driving young executive"
     n 1: hitting a golf ball off of a tee with a driver; "he sliced
          his drive out of bounds" [syn: {drive}]
     2: the act of controlling and steering the movement of a
        vehicle or animal
