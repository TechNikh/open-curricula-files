---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/positive
offline_file: ""
offline_thumbnail: ""
uuid: 4b55fd09-a4ac-463a-8fdb-01a36c2d180b
updated: 1484310264
title: positive
categories:
    - Dictionary
---
positive
     adj 1: characterized by or displaying affirmation or acceptance or
            certainty etc.; "a positive attitude"; "the reviews
            were all positive"; "a positive benefit"; "a positive
            demand" [ant: {negative}, {neutral}]
     2: having a positive electric charge; "protons are positive"
        [syn: {electropositive}] [ant: {negative}, {neutral}]
     3: involving advantage or good; "a plus (or positive) factor"
        [syn: {plus}]
     4: indicating existence or presence of a suspected condition or
        pathogen; "a positive pregnancy test" [syn: {confirming}]
        [ant: {negative}]
     5: formally laid down or imposed; "positive laws" [syn: ...
