---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exorbitant
offline_file: ""
offline_thumbnail: ""
uuid: b88845ed-41aa-4d90-8ca7-d4ac27aac840
updated: 1484310573
title: exorbitant
categories:
    - Dictionary
---
exorbitant
     adj : greatly exceeding bounds of reason or moderation;
           "exorbitant rent"; "extortionate prices"; "spends an
           outrageous amount on entertainment"; "usorious interest
           rate"; "unconscionable spending" [syn: {extortionate},
           {outrageous}, {steep}, {unconscionable}, {usurious}]
