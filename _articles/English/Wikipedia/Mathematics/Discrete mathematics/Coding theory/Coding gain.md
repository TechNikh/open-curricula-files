---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coding_gain
offline_file: ""
offline_thumbnail: ""
uuid: 5f98da67-9784-4528-8240-dcc99c7e7c8a
updated: 1484308553
title: Coding gain
tags:
    - Example
    - Power-limited regime
    - Example
    - Bandwidth-limited regime
categories:
    - Coding theory
---
In coding theory and related engineering problems, coding gain is the measure in the difference between the signal-to-noise ratio (SNR) levels between the uncoded system and coded system required to reach the same bit error rate (BER) levels when used with the error correcting code (ECC).
