---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Segre%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 999ae0c7-8381-4888-8778-24b0b560e00b
updated: 1484309123
title: "Segre's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Oval-def-fin.svg.png
tags:
    - Definition of an oval
    - "Pascal's 3-point version"
    - "Segre's theorem and its proof"
categories:
    - Theorems in geometry
---
This statement was assumed 1949 by the two Finnish mathematicians G. Järnefelt and P. Kustaanheimo and its proof was published in 1955 by B. Segre.
