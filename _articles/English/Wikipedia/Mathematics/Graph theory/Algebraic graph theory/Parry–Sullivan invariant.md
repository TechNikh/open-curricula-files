---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Parry%E2%80%93Sullivan_invariant'
offline_file: ""
offline_thumbnail: ""
uuid: 1c214198-585c-4c3c-b968-9169d4d46bd2
updated: 1484309374
title: Parry–Sullivan invariant
categories:
    - Algebraic graph theory
---
In mathematics, the Parry–Sullivan invariant (or Parry–Sullivan number) is a numerical quantity of interest in the study of incidence matrices in graph theory, and of certain one-dimensional dynamical systems. It provides a partial classification of non-trivial irreducible incidence matrices.
