---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vote
offline_file: ""
offline_thumbnail: ""
uuid: 9904a99a-0410-48fa-887f-273d8f586c5d
updated: 1484310547
title: vote
categories:
    - Dictionary
---
vote
     n 1: a choice that is made by voting; "there were only 17 votes
          in favor of the motion" [syn: {ballot}, {voting}, {balloting}]
     2: the opinion of a group as determined by voting; "they put
        the question to a vote"
     3: a legal right guaranteed by the 15th amendment to the US
        constitution; guaranteed to women by the 19th amendment;
        "American women got the vote in 1920" [syn: {right to vote},
         {suffrage}]
     4: a body of voters who have the same interests; "he failed to
        get the Black vote"
     5: the total number of votes cast; "they are hoping for a large
        vote" [syn: {voter turnout}]
     v 1: express one's ...
