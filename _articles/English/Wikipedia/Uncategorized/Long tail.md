---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Long_tail
offline_file: ""
offline_thumbnail: ""
uuid: fe5cee07-c214-434e-9518-db13a5f59131
updated: 1484308366
title: Long tail
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Long_tail.svg.png
tags:
    - History
    - Business
    - Statistical meaning
    - Chris Anderson and Clay Shirky
    - Academic research
    - Effects of online access
    - The longer tail over time
    - Goodbye Pareto principle, welcome the new distribution
    - Demand-side and supply-side drivers
    - Networks, crowds, and the long tail
    - Turnover within the long tail
    - Business models
    - Competitive impact
    - Internet companies
    - Video and multiplayer online games
    - Microfinance and microcredit
    - User-driven innovation
    - Marketing
    - Cultural and political impact
    - Cultural diversity
    - Distribution of independent content
    - Contemporary literature
    - Military applications and security
    - Criticisms
    - Notes
    - Footnotes
    - Sources
categories:
    - Uncategorized
---
In statistics and business, a long tail of some distributions of numbers is the portion of the distribution having a large number of occurrences far from the "head" or central part of the distribution. The distribution could involve popularities, random numbers of occurrences of events with various probabilities, etc.[1] The term is often used loosely, with no definition or arbitrary definition, but precise definitions are possible.
