---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_number
offline_file: ""
offline_thumbnail: ""
uuid: 82fb9dfd-638c-464b-b1fb-860adce2454d
updated: 1484309317
title: Symmetry number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Octahedral_reflection_domains.png
categories:
    - Symmetry
---
The symmetry number or symmetry order of an object is the number of different but indistinguishable (or equivalent) arrangements (or views) of the object, i.e. the order of its symmetry group. The object can be a molecule, crystal lattice, lattice, tiling, or in a general case any mathematical object in N-dimensions.[1]
