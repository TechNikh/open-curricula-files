---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_de_Rham_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 08b43d19-2849-4693-80d5-bc9e99cadc6d
updated: 1484309134
title: Algebraic de Rham cohomology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_6.png
categories:
    - Algebraic geometry
---
In mathematics, algebraic de Rham cohomology is an analog of de Rham cohomology for algebraic varieties, introduced by Grothendieck (1966). It is closely related to crystalline cohomology.
