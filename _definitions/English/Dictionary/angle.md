---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/angle
offline_file: ""
offline_thumbnail: ""
uuid: 1d8746ed-3a05-4b38-96b4-73b04b5800a2
updated: 1484310221
title: angle
categories:
    - Dictionary
---
angle
     n 1: the space between two lines or planes that intersect; the
          inclination of one line to another; measured in degrees
          or radians
     2: a biased way of looking at or presenting something [syn: {slant}]
     3: a member of a Germanic people who conquered England and
        merged with the Saxons and Jutes to become Anglo-Saxons
     v 1: move or proceed at an angle; "he angled his way into the
          room"
     2: to incline or bend from a vertical position; "She leaned
        over the banister" [syn: {lean}, {tilt}, {tip}, {slant}]
     3: seek indirectly; "fish for compliments" [syn: {fish}]
     4: fish with a hook
     5: present with a bias; "He ...
