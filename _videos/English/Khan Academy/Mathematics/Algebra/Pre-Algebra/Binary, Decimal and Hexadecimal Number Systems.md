---
version: 1
type: video
provider: YouTube
id: _97OwCkjh3c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Binary%2C%20Decimal%20and%20Hexadecimal%20Number%20Systems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 29980e2d-f9d9-48a2-a654-63cff76b7428
updated: 1486069567
title: Binary, Decimal and Hexadecimal Number Systems
tags:
    - Binary
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/_97OwCkjh3c/default.jpg
    - https://i3.ytimg.com/vi/_97OwCkjh3c/1.jpg
    - https://i3.ytimg.com/vi/_97OwCkjh3c/2.jpg
    - https://i3.ytimg.com/vi/_97OwCkjh3c/3.jpg
---
Conceptualizing different number systems
