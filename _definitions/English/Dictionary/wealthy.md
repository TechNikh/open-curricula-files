---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wealthy
offline_file: ""
offline_thumbnail: ""
uuid: 122017bc-7794-495f-885c-c52b216e540a
updated: 1484310575
title: wealthy
categories:
    - Dictionary
---
wealthy
     adj : having an abundant supply of money or possessions of value;
           "an affluent banker"; "a speculator flush with cash";
           "not merely rich but loaded"; "moneyed aristocrats";
           "wealthy corporations" [syn: {affluent}, {flush}, {loaded},
            {moneyed}]
     [also: {wealthiest}, {wealthier}]
