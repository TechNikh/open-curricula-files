---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bonnesen%27s_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: cb37d07b-6fb0-4dda-bf8f-bdde6db8ae6a
updated: 1484308924
title: "Bonnesen's inequality"
categories:
    - Elementary geometry
---
Bonnesen's inequality is an inequality relating the length, the area, the radius of the incircle and the radius of the circumcircle of a Jordan curve. It is a strengthening of the classical isoperimetric inequality.
