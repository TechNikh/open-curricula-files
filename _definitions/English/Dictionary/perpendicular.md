---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perpendicular
offline_file: ""
offline_thumbnail: ""
uuid: 121f99b7-ea75-4291-bce0-0b585bec6d56
updated: 1484310220
title: perpendicular
categories:
    - Dictionary
---
perpendicular
     adj 1: intersecting at or forming right angles; "the axes are
            perpendicular to each other" [ant: {oblique}, {parallel}]
     2: at right angles to the plane of the horizon or a base line;
        "a vertical camera angle"; "the monument consists of two
        vertical pillars supporting a horizontal slab"; "measure
        the perpendicular height" [syn: {vertical}] [ant: {inclined},
         {horizontal}]
     3: extremely steep; "the great perpendicular face of the cliff"
     n 1: a straight line at right angles to another line
     2: a Gothic style in 14th and 15th century England;
        characterized by vertical lines and a four-centered
        ...
