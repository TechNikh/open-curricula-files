---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/iv
offline_file: ""
offline_thumbnail: ""
uuid: 7b2fb6c6-9fd4-443d-bea8-12f4ce2270fb
updated: 1484310315
title: iv
categories:
    - Dictionary
---
iv
     adj : being one more than three [syn: {four}, {4}]
     n 1: the cardinal number that is the sum of three and one [syn: {four},
           {4}, {tetrad}, {quatern}, {quaternion}, {quaternary}, {quaternity},
           {quartet}, {quadruplet}, {foursome}, {Little Joe}]
     2: administration of nutrients through a vein [syn: {intravenous
        feeding}]
