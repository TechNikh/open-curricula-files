---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generating_function_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 5f716d51-ee46-4acb-a9ea-22984be76e9d
updated: 1484308794
title: Generating function (physics)
tags:
    - In Canonical Transformations
    - Example
categories:
    - Hamiltonian mechanics
---
Generating functions which arise in Hamiltonian mechanics are quite different from generating functions in mathematics. In physics, a generating function is, loosely, a function whose partial derivatives generate the differential equations that determine a system's dynamics. Common examples are the partition function of statistical mechanics, the Hamiltonian, and the function which acts as a bridge between two sets of canonical variables when performing a canonical transformation.
