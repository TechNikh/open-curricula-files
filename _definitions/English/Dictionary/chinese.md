---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chinese
offline_file: ""
offline_thumbnail: ""
uuid: e7d06067-2088-409f-9fb9-2fa9884597dc
updated: 1484310517
title: chinese
categories:
    - Dictionary
---
Chinese
     adj 1: of or pertaining to China or its peoples or cultures;
            "Chinese food"
     2: of or relating to or characteristic of the island republic
        on Taiwan or its residents or their language; "the
        Taiwanese capital is Taipeh" [syn: {Taiwanese}, {Formosan}]
     n 1: any of the Sino-Tibetan languages spoken in China; regarded
          as dialects of a single language (even though they are
          mutually unintelligible) because they share an
          ideographic writing system
     2: a native or inhabitant of Communist China or of Nationalist
        China
