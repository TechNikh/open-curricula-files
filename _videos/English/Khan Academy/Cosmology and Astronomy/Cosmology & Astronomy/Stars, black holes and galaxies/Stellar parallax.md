---
version: 1
type: video
provider: YouTube
id: omvNINaRdxg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Stellar%20parallax.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 44c2c204-89b9-4f93-ac19-a52b5e9d6580
updated: 1486069685
title: Stellar parallax
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/omvNINaRdxg/default.jpg
    - https://i3.ytimg.com/vi/omvNINaRdxg/1.jpg
    - https://i3.ytimg.com/vi/omvNINaRdxg/2.jpg
    - https://i3.ytimg.com/vi/omvNINaRdxg/3.jpg
---
