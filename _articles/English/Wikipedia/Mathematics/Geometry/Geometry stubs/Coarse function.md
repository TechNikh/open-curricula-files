---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coarse_function
offline_file: ""
offline_thumbnail: ""
uuid: 652a32e3-f130-4bb3-887f-d5f69938143a
updated: 1484309356
title: Coarse function
categories:
    - Geometry stubs
---
In mathematics, coarse functions are functions that may appear to be continuous at a distance, but in reality are not necessarily continuous.[1] Although continuous functions are usually observed on a small scale, coarse functions are usually observed on a large scale.[1]
