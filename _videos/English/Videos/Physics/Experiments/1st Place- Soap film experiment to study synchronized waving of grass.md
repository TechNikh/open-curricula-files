---
version: 1
type: video
provider: YouTube
id: dZ2SRFFl1Z4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6e3b3f23-42f6-4e20-a1f2-f38b7ee45d25
updated: 1486069640
title: '1st Place- Soap film experiment to study synchronized waving of grass'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/dZ2SRFFl1Z4/default.jpg
    - https://i3.ytimg.com/vi/dZ2SRFFl1Z4/1.jpg
    - https://i3.ytimg.com/vi/dZ2SRFFl1Z4/2.jpg
    - https://i3.ytimg.com/vi/dZ2SRFFl1Z4/3.jpg
---
