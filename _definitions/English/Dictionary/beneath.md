---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beneath
offline_file: ""
offline_thumbnail: ""
uuid: bdf799ca-a75a-4880-8daa-fbf75b55890a
updated: 1484310283
title: beneath
categories:
    - Dictionary
---
beneath
     adv : in or to a place that is lower [syn: {below}, {at a lower
           place}, {to a lower place}] [ant: {above}]
