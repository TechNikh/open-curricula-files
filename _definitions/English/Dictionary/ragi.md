---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ragi
offline_file: ""
offline_thumbnail: ""
uuid: e457b03e-9d9b-4713-95b8-9ed7b45b7954
updated: 1484310531
title: ragi
categories:
    - Dictionary
---
ragi
     n : East Indian cereal grass whose seed yield a somewhat bitter
         flour, a staple in the Orient [syn: {finger millet}, {ragee},
          {African millet}, {coracan}, {corakan}, {kurakkan}, {Eleusine
         coracana}]
