---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concentration
offline_file: ""
offline_thumbnail: ""
uuid: 89eb95a1-9465-426c-983f-dc2d4233e217
updated: 1484310381
title: concentration
categories:
    - Dictionary
---
concentration
     n 1: the strength of a solution; number of molecules of a
          substance in a given volume (expressed as moles/cubic
          meter)
     2: the spatial property of being crowded together [syn: {density},
         {denseness}, {compactness}] [ant: {distribution}]
     3: strengthening the concentration (as of a solute in a
        mixture) by removing extraneous material [ant: {dilution}]
     4: increase in density
     5: complete attention; intense mental effort [syn: {engrossment},
         {absorption}, {immersion}]
     6: bringing together military forces
     7: great and constant diligence and attention [syn: {assiduity},
         {assiduousness}]
