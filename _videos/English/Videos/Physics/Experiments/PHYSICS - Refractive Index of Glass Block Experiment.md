---
version: 1
type: video
provider: YouTube
id: mtxUGco2a4M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ba68cb2b-36a2-4144-9bd2-1d0be39f109f
updated: 1486069636
title: 'PHYSICS - Refractive Index of Glass Block Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/mtxUGco2a4M/default.jpg
    - https://i3.ytimg.com/vi/mtxUGco2a4M/1.jpg
    - https://i3.ytimg.com/vi/mtxUGco2a4M/2.jpg
    - https://i3.ytimg.com/vi/mtxUGco2a4M/3.jpg
---
