---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anisotropy
offline_file: ""
offline_thumbnail: ""
uuid: 7f38d801-0a1a-4e75-9232-f7b6ef8c59b3
updated: 1484309290
title: Anisotropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-WMAP_2010.png
tags:
    - Fields of interest
    - Computer graphics
    - chemistry
    - Real-world imagery
    - Physics
    - Geophysics and geology
    - Medical acoustics
    - Material science and engineering
    - Microfabrication
    - Neuroscience
    - Atmospheric Radiative Transfer
categories:
    - Orientation (geometry)
---
Anisotropy /ˌænaɪˈsɒtrəpi/ is the property of being directionally dependent, which implies different properties in different directions, as opposed to isotropy. It can be defined as a difference, when measured along different axes, in a material's physical or mechanical properties (absorbance, refractive index, conductivity, tensile strength, etc.) An example of anisotropy is the light coming through a polarizer. Another is wood, which is easier to split along its grain than against it.
