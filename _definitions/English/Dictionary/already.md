---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/already
offline_file: ""
offline_thumbnail: ""
uuid: ed2fba18-9f21-43c5-8539-a140b2bdc19c
updated: 1484310240
title: already
categories:
    - Dictionary
---
already
     adv : prior to a specified or implied time; "she has already
           graduated" [ant: {not yet}]
