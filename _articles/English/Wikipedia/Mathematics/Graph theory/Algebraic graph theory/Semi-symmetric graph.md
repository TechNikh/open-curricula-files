---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semi-symmetric_graph
offline_file: ""
offline_thumbnail: ""
uuid: c21f3133-be37-4a50-b32f-f9bcbd9b1e6b
updated: 1484309374
title: Semi-symmetric graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Folkman_graph.svg.png
tags:
    - Properties
    - History
    - Cubic graphs
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a semi-symmetric graph is an undirected graph that is edge-transitive and regular, but not vertex-transitive. In other words, a graph is semi-symmetric if each vertex has the same number of incident edges, and there is a symmetry taking any of the graph's edges to any other of its edges, but there is some pair of vertices such that no symmetry maps the first into the second.
