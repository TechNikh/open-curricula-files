---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_Grassmannian
offline_file: ""
offline_thumbnail: ""
uuid: 91d1607b-2962-4e1b-be13-abed8cc21c65
updated: 1484309130
title: Affine Grassmannian
categories:
    - Algebraic geometry
---
In mathematics, the affine Grassmannian of an algebraic group G over a field k is an ind-scheme—a colimit of finite-dimensional schemes—which can be thought of as a flag variety for the loop group G(k((t))) and which describes the representation theory of the Langlands dual group LG through what is known as the geometric Satake correspondence.
