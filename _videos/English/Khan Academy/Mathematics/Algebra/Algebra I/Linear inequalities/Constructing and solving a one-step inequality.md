---
version: 1
type: video
provider: YouTube
id: FZ2APP6-grU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20inequalities/Constructing%20and%20solving%20a%20one-step%20inequality.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00d111e5-053e-4ad9-a91f-fdc3bee4c20a
updated: 1486069565
title: Constructing and solving a one-step inequality
categories:
    - Linear inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/FZ2APP6-grU/default.jpg
    - https://i3.ytimg.com/vi/FZ2APP6-grU/1.jpg
    - https://i3.ytimg.com/vi/FZ2APP6-grU/2.jpg
    - https://i3.ytimg.com/vi/FZ2APP6-grU/3.jpg
---
