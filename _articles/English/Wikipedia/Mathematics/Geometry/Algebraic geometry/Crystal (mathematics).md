---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crystal_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 7cf20370-305b-41ee-ab35-1ba334111145
updated: 1484309145
title: Crystal (mathematics)
categories:
    - Algebraic geometry
---
In mathematics, crystals are cartesian sections of certain fibered categories. They were introduced by Alexander Grothendieck (1966a), who named them crystals because in some sense they are "rigid" and "grow". In particular quasicoherent crystals over the crystalline site are analogous to quasicoherent modules over a scheme.
