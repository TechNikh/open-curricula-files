---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nominal_techniques
offline_file: ""
offline_thumbnail: ""
uuid: 107061f3-7f27-4c23-9763-a3ee8f18a1e1
updated: 1484308681
title: Nominal techniques
categories:
    - Theoretical computer science
---
Nominal techniques are a range of techniques, based on nominal sets, for handling names and binding, e.g. in abstract syntax. Research into nominal sets gave rise to nominal terms, a metalanguage for embedding object languages with name binding constructs.
