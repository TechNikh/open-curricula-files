---
title: Reuse
article_ref: https://en.wikipedia.org/wiki/Reuse
tags:
    - Advantages and disadvantages
    - Examples
    - Reuse centers and virtual exchange
    - Remanufacturing
    - Package deposit programs
    - Closed-loop programs
    - Refilling programs
    - Regifting
    - Printer cartridges and toners
    - Repurposing
    - Waste Exchanges
    - Reuse of Waste Water and excreta in agriculture
    - Measuring the impact of reuse, reuse metrics
    - Internalized environmental costs
    - Comparison to recycling
    - Reuse of information
    - Reuse of older software
categories:
    - IUCN
---
