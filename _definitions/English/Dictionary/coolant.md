---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coolant
offline_file: ""
offline_thumbnail: ""
uuid: 0c22535e-ce10-4c37-8a78-f4d8b8480b62
updated: 1484310221
title: coolant
categories:
    - Dictionary
---
coolant
     n : a fluid agent (gas or liquid) that produces cooling;
         especially one used to cool a system by transfering heat
         away from one part to another; "he added more coolant to
         the car's radiator"; "the atomic reactor used a gas
         coolant"; "lathe operators use an emulsion of oil and
         water as a coolant for the cutting tool"
