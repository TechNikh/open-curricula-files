---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chilling
offline_file: ""
offline_thumbnail: ""
uuid: 3bb816fc-69d6-4e15-9338-16c6a3cd3591
updated: 1484310522
title: chilling
categories:
    - Dictionary
---
chilling
     adj : so scary as to cause chills and shudders; "the most terrible
           and shuddery...tales of murder and revenge" [syn: {scarey},
            {scary}, {shivery}, {shuddery}]
     n : the process of becoming cooler; a falling temperature [syn:
         {cooling}, {temperature reduction}]
