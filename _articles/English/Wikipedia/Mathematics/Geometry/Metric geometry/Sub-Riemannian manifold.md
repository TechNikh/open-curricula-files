---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sub-Riemannian_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 24b40889-4383-4a23-95e9-f33e2e32f581
updated: 1484309273
title: Sub-Riemannian manifold
tags:
    - Definitions
    - Examples
    - Properties
categories:
    - Metric geometry
---
In mathematics, a sub-Riemannian manifold is a certain type of generalization of a Riemannian manifold. Roughly speaking, to measure distances in a sub-Riemannian manifold, you are allowed to go only along curves tangent to so-called horizontal subspaces.
