---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unloading
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615821
title: unloading
categories:
    - Dictionary
---
unloading
     adj : used in unloading or removing e.g. cargo [ant: {loading(a)}]
     n : the labor of unloading something [ant: {loading}]
