---
version: 1
type: video
provider: YouTube
id: m4jzgqZu-4s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f41b4936-cfe7-41d8-aace-b69ec368c78f
updated: 1486069656
title: 'Electric Circuits-  Basics of the voltage and current laws.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/m4jzgqZu-4s/default.jpg
    - https://i3.ytimg.com/vi/m4jzgqZu-4s/1.jpg
    - https://i3.ytimg.com/vi/m4jzgqZu-4s/2.jpg
    - https://i3.ytimg.com/vi/m4jzgqZu-4s/3.jpg
---
