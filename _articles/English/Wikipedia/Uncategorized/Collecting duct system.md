---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Collecting_duct
offline_file: ""
offline_thumbnail: ""
uuid: be686cfd-ffe7-425f-988c-bf72704d3fa1
updated: 1484308371
title: Collecting duct system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Gray1128.png
tags:
    - Structure
    - Segments
    - Connecting tubule
    - Medullary collecting duct
    - Papillary duct
    - Cells
    - Principal cells
    - Intercalated cells
    - Function
categories:
    - Uncategorized
---
The collecting duct system of the kidney consists of a series of tubules and ducts that connect the nephrons to the ureter. It participates in electrolyte and fluid balance through reabsorption and excretion, processes regulated by the hormones aldosterone and vasopressin (antidiuretic hormone).
