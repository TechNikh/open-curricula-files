---
version: 1
type: video
provider: YouTube
id: Aig1hkq3OsU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Example-%20Solving%20for%20a%20variable.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f2a98f8f-ab60-4ed6-ab52-57a6daeb98b7
updated: 1486069557
title: 'Example- Solving for a variable'
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Aig1hkq3OsU/default.jpg
    - https://i3.ytimg.com/vi/Aig1hkq3OsU/1.jpg
    - https://i3.ytimg.com/vi/Aig1hkq3OsU/2.jpg
    - https://i3.ytimg.com/vi/Aig1hkq3OsU/3.jpg
---
