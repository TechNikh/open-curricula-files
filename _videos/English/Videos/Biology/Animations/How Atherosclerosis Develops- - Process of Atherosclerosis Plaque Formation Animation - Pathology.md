---
version: 1
type: video
provider: YouTube
id: mAFgfN2OPls
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20Atherosclerosis%20Develops-%20-%20Process%20of%20Atherosclerosis%20Plaque%20Formation%20Animation%20-%20Pathology.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eed90a7f-26a8-4a26-bbeb-17fe4947986c
updated: 1486069666
title: 'How Atherosclerosis Develops? - Process of Atherosclerosis Plaque Formation Animation - Pathology'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/mAFgfN2OPls/default.jpg
    - https://i3.ytimg.com/vi/mAFgfN2OPls/1.jpg
    - https://i3.ytimg.com/vi/mAFgfN2OPls/2.jpg
    - https://i3.ytimg.com/vi/mAFgfN2OPls/3.jpg
---
