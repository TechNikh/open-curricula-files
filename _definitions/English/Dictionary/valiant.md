---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valiant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413681
title: valiant
categories:
    - Dictionary
---
valiant
     adj : having or showing valor; "a valiant attempt to prevent the
           hijack"; "a valiant soldier" [syn: {valorous}]
