---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C2%B2_cohomology'
offline_file: ""
offline_thumbnail: ""
uuid: cc5f78d3-be32-417a-bce4-db4724655cc7
updated: 1484309226
title: L² cohomology
categories:
    - Differential geometry
---
In mathematics, L2 cohomology is a cohomology theory for smooth non-compact manifolds M with Riemannian metric. It is defined in the same way as de Rham cohomology except that one uses square-integrable differential forms. The notion of square-integrability makes sense because the metric on M gives rise to a norm on differential forms and a volume form.
