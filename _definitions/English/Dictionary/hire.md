---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hire
offline_file: ""
offline_thumbnail: ""
uuid: 59c61580-f3e9-4c59-b1ca-7f0a2ac15e2a
updated: 1484310455
title: hire
categories:
    - Dictionary
---
hire
     v 1: engage or hire for work; "They hired two new secretaries in
          the department"; "How many people has she employed?"
          [syn: {engage}, {employ}] [ant: {fire}]
     2: hold under a lease or rental agreement; of goods and
        services [syn: {rent}, {charter}, {lease}]
     3: engage for service under a term of contract; "We took an
        apartment on a quiet street"; "Let's rent a car"; "Shall
        we take a guide in Rome?" [syn: {lease}, {rent}, {charter},
         {engage}, {take}]
