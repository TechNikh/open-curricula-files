---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gambler%27s_fallacy'
offline_file: ""
offline_thumbnail: ""
uuid: 17155c6a-a9fc-43b6-a4b9-3d79e033e019
updated: 1484309040
title: "Gambler's fallacy"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Lawoflargenumbersanimation2.gif
tags:
    - Examples
    - Coin toss
    - Why the probability is 1/2 for a fair coin
    - Other examples
    - Reverse fallacy
    - Caveats
    - Childbirth
    - Monte Carlo Casino
    - Non-examples
    - Unknown probability
    - Psychology behind the fallacy
    - Origins
    - "Variations of the gambler's fallacy"
    - Relationship to hot-hand fallacy
    - Neurophysiology
    - Possible solutions
    - "Observations of the gambler's fallacy"
categories:
    - Games (probability)
---
The gambler's fallacy, also known as the Monte Carlo fallacy or the fallacy of the maturity of chances, is the mistaken belief that, if something happens more frequently than normal during some period, it will happen less frequently in the future, or that, if something happens less frequently than normal during some period, it will happen more frequently in the future (presumably as a means of balancing nature). In situations where what is being observed is truly random (i.e., independent trials of a random process), this belief, though appealing to the human mind, is false. This fallacy can arise in many practical situations although it is most strongly associated with gambling where such ...
