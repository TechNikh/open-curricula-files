---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/director
offline_file: ""
offline_thumbnail: ""
uuid: 0deadb22-bb7a-468d-b134-7cc72a5b7376
updated: 1484310429
title: director
categories:
    - Dictionary
---
director
     n 1: someone who controls resources and expenditures [syn: {manager},
           {managing director}]
     2: member of a board of directors
     3: someone who supervises the actors and directs the action in
        the production of a show [syn: {theater director}, {theatre
        director}]
     4: the person who leads a musical group [syn: {conductor}, {music
        director}]
