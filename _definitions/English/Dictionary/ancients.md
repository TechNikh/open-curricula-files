---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ancients
offline_file: ""
offline_thumbnail: ""
uuid: e9e47ec5-3e38-478d-a34a-56886efbc6c0
updated: 1484310411
title: ancients
categories:
    - Dictionary
---
ancients
     n : people who lived in times long past (especially during the
         historical period before the fall of the Roman Empire in
         western Europe)
