---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/villi
offline_file: ""
offline_thumbnail: ""
uuid: d1df1778-2a66-496c-afc9-441a7ec50e02
updated: 1484310323
title: villi
categories:
    - Dictionary
---
villus
     n : a minute hairlike projection on mucous membrane
     [also: {villi} (pl)]
