---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wright
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421961
title: wright
categories:
    - Dictionary
---
Wright
     n 1: United States writer of detective novels (1888-1939) [syn: {Willard
          Huntington Wright}, {S. S. Van Dine}]
     2: United States writer whose work is concerned with the
        oppression of African Americans (1908-1960) [syn: {Richard
        Wright}]
     3: United States aviation pioneer who (with his brother Orville
        Wright) invented the airplane (1867-1912) [syn: {Wilbur
        Wright}]
     4: United States aviation pioneer who (with his brother Wilbur
        Wright) invented the airplane (1871-1948) [syn: {Orville
        Wright}]
     5: influential United States architect (1869-1959) [syn: {Frank
        Lloyd Wright}]
     6: United States early ...
