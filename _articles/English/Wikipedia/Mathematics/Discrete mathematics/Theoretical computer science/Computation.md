---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computation
offline_file: ""
offline_thumbnail: ""
uuid: fdb62d7e-0497-4aa0-9628-523773d40aa4
updated: 1484308674
title: Computation
tags:
    - Physical phenomenon
    - Accounts of computation
    - The mapping account
    - The semantic account
    - The mechanistic account
    - Mathematical models
categories:
    - Theoretical computer science
---
The study of computation is paramount to the discipline of computer science.
