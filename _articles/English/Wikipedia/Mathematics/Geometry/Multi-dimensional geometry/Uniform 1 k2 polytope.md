---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uniform_1_k2_polytope
offline_file: ""
offline_thumbnail: ""
uuid: ca3fd23c-d15f-4ba5-9793-9393762136ed
updated: 1484309289
title: Uniform 1 k2 polytope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_17.png
tags:
    - Family members
    - Elements
categories:
    - Multi-dimensional geometry
---
In geometry, 1k2 polytope is a uniform polytope in n-dimensions (n = k+4) constructed from the En Coxeter group. The family was named by their Coxeter symbol 1k2 by its bifurcating Coxeter-Dynkin diagram, with a single ring on the end of the 1-node sequence. It can be named by an extended Schläfli symbol {3,3k,2}.
