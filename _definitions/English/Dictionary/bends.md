---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bends
offline_file: ""
offline_thumbnail: ""
uuid: 53f24111-1969-42a6-94a6-0778e27eee0a
updated: 1484310216
title: bends
categories:
    - Dictionary
---
bends
     n : pain resulting from rapid change in pressure [syn: {decompression
         sickness}, {aeroembolism}, {air embolism}, {gas embolism},
          {caisson disease}]
