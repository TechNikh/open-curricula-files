---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/edit
offline_file: ""
offline_thumbnail: ""
uuid: 95f6f9bd-35ea-4a3e-aa85-9a7f853dfc63
updated: 1484310152
title: edit
categories:
    - Dictionary
---
edit
     v 1: prepare for publication or presentation by correcting,
          revising, or adapting; "Edit a a book on lexical
          semantics"; "she edited the letters of the politician so
          as to omit the most personal passages" [syn: {redact}]
     2: supervise the publication of; "The same family has been
        editing the influential newspaper for almost 100 years"
     3: cut and assemble the components of; "edit film"; "cut
        recording tape" [syn: {cut}, {edit out}]
     4: cut or eliminate; "she edited the juiciest scenes" [syn: {blue-pencil},
         {delete}]
