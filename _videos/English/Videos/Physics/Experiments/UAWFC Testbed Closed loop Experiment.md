---
version: 1
type: video
provider: YouTube
id: t2sG7_t-aKw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dbb5b2d8-90bc-4d2a-b151-62e5bbdd47a6
updated: 1486069635
title: UAWFC Testbed Closed loop Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/t2sG7_t-aKw/default.jpg
    - https://i3.ytimg.com/vi/t2sG7_t-aKw/1.jpg
    - https://i3.ytimg.com/vi/t2sG7_t-aKw/2.jpg
    - https://i3.ytimg.com/vi/t2sG7_t-aKw/3.jpg
---
