---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gravitational
offline_file: ""
offline_thumbnail: ""
uuid: 32f83987-1d22-4cf9-9342-886d696e87fc
updated: 1484310401
title: gravitational
categories:
    - Dictionary
---
gravitational
     adj : of or relating to or caused by gravitation [syn: {gravitative}]
