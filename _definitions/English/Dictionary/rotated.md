---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rotated
offline_file: ""
offline_thumbnail: ""
uuid: 6a63950c-c28a-4f3b-b8c1-b7a5bb44516f
updated: 1484310210
title: rotated
categories:
    - Dictionary
---
rotated
     adj : turned in a circle around an axis [syn: {revolved}]
