---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_uniformization
offline_file: ""
offline_thumbnail: ""
uuid: e7b81a22-43ea-4530-8dee-cdeb1e194bf2
updated: 1484309160
title: Local uniformization
categories:
    - Algebraic geometry
---
In algebraic geometry, local uniformization is a weak form of resolution of singularities, stating roughly that a variety can be desingularized near any valuation, or in other words that the Zariski–Riemann space of the variety is in some sense nonsingular. Local uniformization was introduced by Zariski (1939, 1940), who separated out the problem of resolving the singularities of a variety into the problem of local uniformization and the problem of combining the local uniformizations into a global desingularization.
