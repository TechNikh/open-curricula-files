---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbola
offline_file: ""
offline_thumbnail: ""
uuid: 11d5f7df-28c1-45c5-958f-51dd95e71ced
updated: 1484309167
title: Hyperbola
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Hyperbola_%2528PSF%2529.svg.png'
tags:
    - Etymology and history
    - Nomenclature and features
    - Mathematical definitions
    - Conic section
    - Difference of distances to foci
    - Directrix and focus
    - Reciprocation of a circle
    - Quadratic equation
    - True anomaly
    - Geometrical constructions
    - Reflections and tangent lines
    - Hyperbolic functions and equations
    - Relation to other conic sections
    - >
        Conic section analysis of the hyperbolic appearance of
        circles
    - Derived curves
    - Coordinate systems
    - Cartesian coordinates
    - Polar coordinates
    - Parametric equations
    - Elliptic coordinates
    - Rectangular hyperbola
    - Other properties of hyperbolas
    - Applications
    - Sundials
    - Multilateration
    - Path followed by a particle
    - Korteweg-de Vries equation
    - Angle trisection
    - Efficient portfolio frontier
    - Extensions
    - Other conic sections
    - Other related topics
    - Notes
categories:
    - Analytic geometry
---
In mathematics, a hyperbola (plural hyperbolas or hyperbolae) is a type of smooth curve lying in a plane, defined by its geometric properties or by equations for which it is the solution set. A hyperbola has two pieces, called connected components or branches, that are mirror images of each other and resemble two infinite bows. The hyperbola is one of the three kinds of conic section, formed by the intersection of a plane and a double cone. (The other conic sections are the parabola and the ellipse. A circle is a special case of an ellipse). If the plane intersects both halves of the double cone but does not pass through the apex of the cones, then the conic is a hyperbola.
