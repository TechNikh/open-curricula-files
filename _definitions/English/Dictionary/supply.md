---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supply
offline_file: ""
offline_thumbnail: ""
uuid: bebdcf30-e009-4b49-8e7e-954ba499cc8e
updated: 1484310264
title: supply
categories:
    - Dictionary
---
supply
     n 1: an amount of something available for use
     2: offering goods and services for sale [ant: {demand}]
     3: the activity of supplying or providing something [syn: {provision},
         {supplying}]
     v 1: provide or furnish with; "We provided the room with an
          electrical heater" [syn: {provide}, {render}, {furnish}]
     2: circulate or distribute or equip with; "issue a new uniform
        to the children"; "supply blankets for the beds" [syn: {issue}]
        [ant: {recall}]
     3: provide what is desired or needed, especially support, food
        or sustenance; "The hostess provided lunch for all the
        guests" [syn: {provide}, {ply}, {cater}]
     ...
