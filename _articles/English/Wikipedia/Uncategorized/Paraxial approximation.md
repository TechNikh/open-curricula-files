---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paraxial
offline_file: ""
offline_thumbnail: ""
uuid: 61cf0b75-8bb0-4e03-bfb3-b19934891f50
updated: 1484308323
title: Paraxial approximation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Small_angle_compare_error.svg.png
categories:
    - Uncategorized
---
In geometric optics, the paraxial approximation is a small-angle approximation used in Gaussian optics and ray tracing of light through an optical system (such as a lens).[1] [2]
