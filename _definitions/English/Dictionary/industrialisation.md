---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrialisation
offline_file: ""
offline_thumbnail: ""
uuid: 4d77b4a5-ebc0-46f6-b607-32d8f1b225a7
updated: 1484310464
title: industrialisation
categories:
    - Dictionary
---
industrialisation
     n : the development of industry on an extensive scale [syn: {industrialization},
          {industrial enterprise}]
