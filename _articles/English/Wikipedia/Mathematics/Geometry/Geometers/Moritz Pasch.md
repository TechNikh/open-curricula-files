---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moritz_Pasch
offline_file: ""
offline_thumbnail: ""
uuid: d9cc252d-ceee-473e-9935-4a4cba5e2d34
updated: 1484309096
title: Moritz Pasch
categories:
    - Geometers
---
Moritz Pasch (8 November 1843, Breslau, Prussia (now Wrocław, Poland) – 20 September 1930, Bad Homburg, Germany) was a German mathematician specializing in the foundations of geometry. He completed his Ph.D. at the University of Breslau at only 22 years of age. He taught at the University of Giessen, where he is known to have supervised 30 doctorates.
