---
version: 1
type: video
provider: YouTube
id: ec2P7le9f7w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e506f085-a6d0-46b2-8fa8-5f350b8dd1bc
updated: 1486069665
title: 'Eureka 28 - Heat as Energy.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ec2P7le9f7w/default.jpg
    - https://i3.ytimg.com/vi/ec2P7le9f7w/1.jpg
    - https://i3.ytimg.com/vi/ec2P7le9f7w/2.jpg
    - https://i3.ytimg.com/vi/ec2P7le9f7w/3.jpg
---
