---
version: 1
type: video
provider: YouTube
id: IKsi-DQU2zo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Adding%20and%20subtracting%20rational%20expressions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e205c877-9fe4-4dcb-939a-c356a22a9720
updated: 1486069559
title: Adding and subtracting rational expressions 3
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/IKsi-DQU2zo/default.jpg
    - https://i3.ytimg.com/vi/IKsi-DQU2zo/1.jpg
    - https://i3.ytimg.com/vi/IKsi-DQU2zo/2.jpg
    - https://i3.ytimg.com/vi/IKsi-DQU2zo/3.jpg
---
