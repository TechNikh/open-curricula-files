---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depicting
offline_file: ""
offline_thumbnail: ""
uuid: cea39bbb-574d-4876-a62b-a6bea191159e
updated: 1484310517
title: depicting
categories:
    - Dictionary
---
depicting
     n : a representation by picture or portraiture [syn: {depiction},
          {portraying}, {portrayal}]
