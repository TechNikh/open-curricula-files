---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tent_map
offline_file: ""
offline_thumbnail: ""
uuid: 56702765-ae52-4399-880c-067172bd2c83
updated: 1484308715
title: Tent map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tent_map_2.png
tags:
    - Behaviour
    - Magnifying the orbit diagram
    - Asymmetric tent map
categories:
    - Chaotic maps
---
the name being due to the tent-like shape of the graph of fμ. For the values of the parameter μ within 0 and 2, fμ maps the unit interval [0, 1] into itself, thus defining a discrete-time dynamical system on it (equivalently, a recurrence relation). In particular, iterating a point x0 in [0, 1] gives rise to a sequence 
  
    
      
        
          x
          
            n
          
        
      
    
    {\displaystyle x_{n}}
  
 :
