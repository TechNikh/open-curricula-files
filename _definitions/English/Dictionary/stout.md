---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stout
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448901
title: stout
categories:
    - Dictionary
---
stout
     adj 1: dependable; "the stalwart citizens at Lexington"; "a
            stalwart supporter of the UN"; "stout hearts" [syn: {stalwart}]
     2: euphemisms for `fat'; "men are portly and women are stout"
        [syn: {portly}]
     3: having rugged physical strength; inured to fatigue or
        hardships; "hardy explorers of northern Canada"; "proud of
        her tall stalwart son"; "stout seamen"; "sturdy young
        athletes" [syn: {hardy}, {stalwart}, {sturdy}]
     n 1: a strong very dark heavy-bodied ale made from pale malt and
          roasted unmalted barley and (often) caramel malt with
          hops
     2: a garment size for a large or heavy person
