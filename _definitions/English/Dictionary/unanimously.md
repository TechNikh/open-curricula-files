---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unanimously
offline_file: ""
offline_thumbnail: ""
uuid: 025cde7c-0408-46a8-9208-4f54a585637d
updated: 1484310192
title: unanimously
categories:
    - Dictionary
---
unanimously
     adv : of one mind; without dissent; "the Senate unanimously
           approved the bill"; "we voted unanimously" [syn: {nem
           con}, {nemine contradicente}]
