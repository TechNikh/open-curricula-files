---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sun-Ni_law
offline_file: ""
offline_thumbnail: ""
uuid: d27c0d43-e3f4-4b12-96db-c8bcdaec3483
updated: 1484308675
title: Sun-Ni law
tags:
    - "Derivation of Sun-Ni's Law"
    - Examples
    - "Applications/Effects of Sun-Ni's Law"
categories:
    - Theoretical computer science
---
Sun-Ni's Law (or Sun and Ni's Law, also known as memory-bounded speedup), is a memory-bounded speedup model which states that as computing power increases the corresponding increase in problem size is constrained by the system’s memory capacity. In general, as a system grows in computational power, the problems run on the system increase in size. Analogous to Amdahl's law, which says that the problem size remains constant as system sizes grow, and Gustafson's law, which proposes that the problem size should scale but be bound by a fixed amount of time, Sun-Ni's Law states the problem size should scale but be bound by the memory capacity of the system. Sun-Ni's Law [1][2] was initially ...
