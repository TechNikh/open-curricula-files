---
version: 1
type: video
provider: YouTube
id: WkI_Vbwn14g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Translation%20-%20Central%20Dogma%20Part%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e1acbff8-fae0-4f5b-bd97-7f21679eaeaf
updated: 1486069666
title: 'Translation - Central Dogma Part 2'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/WkI_Vbwn14g/default.jpg
    - https://i3.ytimg.com/vi/WkI_Vbwn14g/1.jpg
    - https://i3.ytimg.com/vi/WkI_Vbwn14g/2.jpg
    - https://i3.ytimg.com/vi/WkI_Vbwn14g/3.jpg
---
