---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integral_windup
offline_file: ""
offline_thumbnail: ""
uuid: f8f67491-6d7e-4c82-907e-c5ec4da21cb4
updated: 1484308732
title: Integral windup
categories:
    - Control engineering
---
Integral windup, also known as integrator windup[1] or reset windup,[2] refers to the situation in a PID feedback controller where a large change in setpoint occurs (say a positive change) and the integral terms accumulates a significant error during the rise (windup), thus overshooting and continuing to increase as this accumulated error is unwound (offset by errors in the other direction). The specific problem is the excess overshooting.
