---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pauperisation
offline_file: ""
offline_thumbnail: ""
uuid: b1c7e3ec-b991-48cf-8a8c-1315107b9d63
updated: 1484310563
title: pauperisation
categories:
    - Dictionary
---
pauperisation
     n : the act of making someone poor [syn: {pauperization}, {impoverishment}]
