---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supramolecular_assembly
offline_file: ""
offline_thumbnail: ""
uuid: 3fda7e13-2ebf-48e5-b5e2-d00799eeef66
updated: 1484308823
title: Supramolecular assembly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Host_Guest_Complex_Nanocapsule_Science_Year2005_Vol309_Page2037.jpg
tags:
    - Templating reactions
    - Hydrogen bond assisted supramolecular assembly
    - Applications
categories:
    - Self-organization
---
A supramolecular assembly or "supermolecule" is a well defined complex of molecules held together by noncovalent bonds. While a supramolecular assembly can be simply composed of two molecules (e.g., a DNA double helix or an inclusion compound), it is more often used to denote larger complexes of molecules that form sphere-, rod-, or sheet-like species. Micelles, liposomes and biological membranes are examples of supramolecular assemblies.[1] The dimensions of supramolecular assemblies can range from nanometers to micrometers. Thus they allow access to nanoscale objects using a bottom-up approach in far fewer steps than a single molecule of similar dimensions.
