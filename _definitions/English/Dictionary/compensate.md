---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compensate
offline_file: ""
offline_thumbnail: ""
uuid: 9b0099f4-3d8e-458f-987f-4891f023f80f
updated: 1484310170
title: compensate
categories:
    - Dictionary
---
compensate
     v 1: adjust or make up for; "engineers will work to correct the
          effects or air resistance" [syn: {counterbalance}, {correct},
           {even out}, {even off}, {even up}]
     2: make amends for; pay compensation for; "One can never fully
        repair the suffering and losses of the Jews in the Third
        Reich"; "She was compensated for the loss of her arm in
        the accident" [syn: {recompense}, {repair}, {indemnify}]
     3: make up for shortcomings or a feeling of inferiority by
        exaggerating good qualities; "he is compensating for being
        a bad father" [syn: {cover}, {overcompensate}]
     4: make reparations or amends for; "right a ...
