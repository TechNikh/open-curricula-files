---
version: 1
type: video
provider: YouTube
id: Dn7G5f_nvgI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: af2860ef-4a29-4f94-ac06-28506ad0528e
updated: 1486069611
title: IIT JEE Complex Numbers part 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Dn7G5f_nvgI/default.jpg
    - https://i3.ytimg.com/vi/Dn7G5f_nvgI/1.jpg
    - https://i3.ytimg.com/vi/Dn7G5f_nvgI/2.jpg
    - https://i3.ytimg.com/vi/Dn7G5f_nvgI/3.jpg
---
