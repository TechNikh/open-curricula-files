---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kolhapur
offline_file: ""
offline_thumbnail: ""
uuid: 94f656d1-6544-479f-9c59-a0e7b47f8f73
updated: 1484308455
title: Kolhapur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Mahalaxmi_of_Kolhapur.jpg
tags:
    - History
    - Scriptural records
    - Medieval era
    - Kolhapur State
    - geography
    - Climate
    - hydrology
    - Governance
    - Demographics
    - Economy
    - Medical
    - Cuisine
    - Media and telecommunication
    - Sport
    - Transport
    - Education
    - Agriculture colleges
    - Engineering and Technology Colleges
    - Medical colleges
    - Nursing Colleges
    - Pharmacy colleges
    - Law colleges
    - Polytechnic Colleges
    - Notable people
categories:
    - Uncategorized
---
Kolhapur, ( Kolhapur.ogg (help·info)) is a city in the Panchganga River Basin in the western Indian state of Maharashtra.[4] It is the district headquarters of Kolhapur district. Kolhapur comes under the administration of Pune division.[5] Prior to Indian Independence, Kolhapur was a nineteen gun salute, princely state ruled by the Bhosale Chhatrapati (Bhosale royal clan) of the Maratha Empire.
