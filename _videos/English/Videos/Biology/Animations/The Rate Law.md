---
version: 1
type: video
provider: YouTube
id: WDXzVI8SmfE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0122f712-8f9d-4def-8e2d-4a3ffb883520
updated: 1486069665
title: The Rate Law
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/WDXzVI8SmfE/default.jpg
    - https://i3.ytimg.com/vi/WDXzVI8SmfE/1.jpg
    - https://i3.ytimg.com/vi/WDXzVI8SmfE/2.jpg
    - https://i3.ytimg.com/vi/WDXzVI8SmfE/3.jpg
---
