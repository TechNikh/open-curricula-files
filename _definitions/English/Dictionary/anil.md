---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anil
offline_file: ""
offline_thumbnail: ""
uuid: 5ae0cd12-31cc-4f8e-b028-0e020acb54bc
updated: 1484310168
title: anil
categories:
    - Dictionary
---
anil
     n 1: a blue dye obtained from plants or made synthetically [syn:
          {indigo}, {indigotin}]
     2: shrub of West Indies and South America that is a source of
        indigo dye [syn: {Indigofera suffruticosa}, {Indigofera
        anil}]
