---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/significantly
offline_file: ""
offline_thumbnail: ""
uuid: abe3881d-be28-4422-8f48-23c9859737c5
updated: 1484310467
title: significantly
categories:
    - Dictionary
---
significantly
     adv 1: in a statistically significant way; "the two groups differed
            significantly"
     2: in a significant manner; "our budget will be significantly
        affected by these new cuts" [ant: {insignificantly}]
     3: in an important way or to an important degree; "more
        importantly, Weber held that the manifold meaning attached
        to the event by the social scientist could alter his
        definition of the concrete event itself" [syn: {importantly}]
