---
version: 1
type: article
id: https://en.wikipedia.org/wiki/I
offline_file: ""
offline_thumbnail: ""
uuid: bc0200d2-5d0b-4174-ad55-9c94eb72838b
updated: 1484308432
title: I
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-I_cursiva.gif
tags:
    - History
    - Use in writing systems
    - english
    - Other languages
    - Other uses
    - Forms and variants
    - Computing codes
    - Other representations
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Ancestors and siblings in other alphabets
categories:
    - Uncategorized
---
