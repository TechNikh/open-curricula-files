---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flared
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579161
title: flared
categories:
    - Dictionary
---
flared
     adj : having a gradual increase in width; "flared nostrils"; "a
           skirt flaring from the waist" [syn: {flaring}]
