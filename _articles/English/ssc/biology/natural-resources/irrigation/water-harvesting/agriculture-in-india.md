---
title: Agriculture in India
article_ref: https://en.wikipedia.org/wiki/Agriculture_in_India
tags:
    - Overview
    - History
    - Agriculture and colonialism
    - Indian agriculture after independence
    - Irrigation
    - Output
    - Major crops and yields
    - Horticulture
    - Problems
    - Infrastructure
    - Productivity
    - Farmer suicides
    - Diversion of agricultural land for non agricultural purpose
    - Initiatives
    - Maps
    - Bibliography
categories:
    - Water harvesting
---
