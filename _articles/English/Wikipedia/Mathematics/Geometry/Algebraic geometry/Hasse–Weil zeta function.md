---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hasse%E2%80%93Weil_zeta_function'
offline_file: ""
offline_thumbnail: ""
uuid: 817c0827-cd75-4b9a-92e5-38e3d37a1883
updated: 1484309153
title: Hasse–Weil zeta function
tags:
    - 'Example: elliptic curve over Q'
    - Hasse–Weil conjecture
    - Bibliography
categories:
    - Algebraic geometry
---
In mathematics, the Hasse–Weil zeta function attached to an algebraic variety V defined over an algebraic number field K is one of the two most important types of L-function. Such L-functions are called 'global', in that they are defined as Euler products in terms of local zeta functions. They form one of the two major classes of global L-functions, the other being the L-functions associated to automorphic representations. Conjecturally there is just one essential type of global L-function, with two descriptions (coming from an algebraic variety, coming from an automorphic representation); this would be a vast generalisation of the Taniyama–Shimura conjecture, itself a very deep and ...
