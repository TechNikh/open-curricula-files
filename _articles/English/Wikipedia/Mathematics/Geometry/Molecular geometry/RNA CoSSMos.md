---
version: 1
type: article
id: https://en.wikipedia.org/wiki/RNA_CoSSMos
offline_file: ""
offline_thumbnail: ""
uuid: 08b4d954-4921-470e-b393-7a26397f63ab
updated: 1484309277
title: RNA CoSSMos
categories:
    - Molecular geometry
---
The RNA Characterization of Secondary Structure Motifs database (RNA CoSSMos) is a repository of three-dimensional nucleic acid PDB structures containing secondary structure motifs ( loops, hairpin loops ...).[1]
