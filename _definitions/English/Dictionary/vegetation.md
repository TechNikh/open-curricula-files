---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vegetation
offline_file: ""
offline_thumbnail: ""
uuid: 51905b55-52f1-4857-927f-9cf1af6144d2
updated: 1484310268
title: vegetation
categories:
    - Dictionary
---
vegetation
     n 1: all the plant life in a particular region [syn: {flora}]
          [ant: {fauna}]
     2: the process of growth in plants
     3: an abnormal growth or excrescence (especially a warty
        excrescence on the valves of the heart)
     4: inactivity that is passive and monotonous, comparable to the
        inactivity of plant life; "their holiday was spent in
        sleep and vegetation"
