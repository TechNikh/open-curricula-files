---
version: 1
type: video
provider: YouTube
id: g-biRwAVTV8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c81d2251-c825-44e7-ab10-662d5c4baf9f
updated: 1486069662
title: Classifying Chemical Reactions Flintstones.wmv
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/g-biRwAVTV8/default.jpg
    - https://i3.ytimg.com/vi/g-biRwAVTV8/1.jpg
    - https://i3.ytimg.com/vi/g-biRwAVTV8/2.jpg
    - https://i3.ytimg.com/vi/g-biRwAVTV8/3.jpg
---
