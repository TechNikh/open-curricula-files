---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissuade
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484486281
title: dissuade
categories:
    - Dictionary
---
dissuade
     v : turn away from by persuasion; "Negative campaigning will
         only dissuade people" [syn: {deter}] [ant: {persuade}]
