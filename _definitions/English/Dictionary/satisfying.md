---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satisfying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484322961
title: satisfying
categories:
    - Dictionary
---
satisfying
     adj 1: giving pleasure or satisfaction [syn: {appreciated}, {gratifying},
             {pleasing}]
     2: providing abundant nourishment; "a hearty meal"; "good solid
        food"; "ate a substantial breakfast" [syn: {hearty}, {solid},
         {substantial}]
     3: providing freedom from worry [syn: {comforting}, {cheering}]
