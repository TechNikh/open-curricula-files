---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swallowed
offline_file: ""
offline_thumbnail: ""
uuid: 100f43ac-9a58-4351-ae57-071831d6ceb8
updated: 1484310343
title: swallowed
categories:
    - Dictionary
---
swallowed
     adj : completely enclosed or swallowed up; "a house engulfed in
           flames"; "the fog-enveloped cliffs"; "a view swallowed
           by night" [syn: {engulfed}, {enveloped}]
