---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hawker
offline_file: ""
offline_thumbnail: ""
uuid: 138afb6d-0219-4f9a-b91d-44a1361eb0d4
updated: 1484310522
title: hawker
categories:
    - Dictionary
---
hawker
     n 1: someone who travels about selling his wares (as on the
          streets or at carnivals) [syn: {peddler}, {pedlar}, {packman},
           {pitchman}]
     2: a person who breeds and trains hawks and who follows the
        sport of falconry [syn: {falconer}]
