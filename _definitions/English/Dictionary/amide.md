---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amide
offline_file: ""
offline_thumbnail: ""
uuid: 32c2d42d-397a-4d23-99c1-335410e2180e
updated: 1484310423
title: amide
categories:
    - Dictionary
---
amide
     n : any organic compound containing the group -CONH2
