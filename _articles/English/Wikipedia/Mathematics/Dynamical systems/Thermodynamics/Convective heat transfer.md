---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convective_heat_transfer
offline_file: ""
offline_thumbnail: ""
uuid: cd27563f-1467-4f79-a00c-f260b4e8b747
updated: 1484308845
title: Convective heat transfer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convection-snapshot.png
tags:
    - Overview
    - "Newton's law of cooling"
    - Convective heat transfer
categories:
    - Thermodynamics
---
Convective heat transfer, often referred to simply as convection, is the transfer of heat from one place to another by the movement of fluids. Convection is usually the dominant form of heat transfer(convection) in liquids and gases. Although often discussed as a distinct method of heat transfer, convective heat transfer involves the combined processes of conduction (heat diffusion) and advection (heat transfer by bulk fluid flow).
