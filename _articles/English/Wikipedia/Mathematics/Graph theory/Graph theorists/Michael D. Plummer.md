---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Michael_D._Plummer
offline_file: ""
offline_thumbnail: ""
uuid: cec292c0-6ada-4225-8d0e-18dfa1542ba3
updated: 1484309473
title: Michael D. Plummer
tags:
    - Education and career
    - Contributions
    - Awards and honors
    - Selected publications
categories:
    - Graph theorists
---
Michael David Plummer is a retired mathematics professor from Vanderbilt University. His field of work is in graph theory in which he has produced over a hundred papers and publications. He has also spoken at over a hundred and fifty guest lectures around the world.
