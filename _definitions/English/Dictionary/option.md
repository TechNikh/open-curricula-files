---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/option
offline_file: ""
offline_thumbnail: ""
uuid: c63f3eb2-b8b1-4a87-8928-195fbb1cfa2c
updated: 1484310238
title: option
categories:
    - Dictionary
---
option
     n 1: the right to buy or sell property at an agreed price; the
          right is purchased and if it is not exercised by a
          stated date the money is forfeited
     2: one of a number of things from which only one can be chosen;
        "what option did I have?"; "there no other alternative";
        "my only choice is to refuse" [syn: {alternative}, {choice}]
     3: the act of choosing or selecting; "your choice of colors was
        unfortunate"; "you can take your pick" [syn: {choice}, {selection},
         {pick}]
