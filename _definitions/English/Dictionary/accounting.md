---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accounting
offline_file: ""
offline_thumbnail: ""
uuid: 317fbf2c-b39d-40d0-9394-255408662f69
updated: 1484310389
title: accounting
categories:
    - Dictionary
---
accounting
     n 1: a convincing explanation that reveals basic causes; "he was
          unable to give a clear accounting for his actions"
     2: a system that provides quantitative information about
        finances
     3: the occupation of maintaining and auditing records and
        preparing financial reports for a business [syn: {accountancy}]
     4: a bookkeeper's chronological list of related debits and
        credits of a business; forms part of a ledger of accounts
        [syn: {accounting system}, {method of accounting}]
     5: a statement of recent transactions and the resulting
        balance; "they send me an accounting every month" [syn: {account},
         {account ...
