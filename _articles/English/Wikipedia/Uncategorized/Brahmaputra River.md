---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dihang
offline_file: ""
offline_thumbnail: ""
uuid: c9c3cc15-6ff2-45a5-b881-ea16992485b2
updated: 1484308419
title: Brahmaputra River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Sunset_in_Dibrugarh_0.jpg
tags:
    - Course
    - Tibet
    - Assam and adjoining region
    - Bangladesh
    - Basin characteristics
    - hydrology
    - Discharge
    - Climate
    - Floodplain evolution
    - Significance to people
    - flooding
    - Channel morphology
    - International cooperation
    - History
    - River engineering
    - Notes
categories:
    - Uncategorized
---
The Brahmaputra (/ˌbrɑːməˈpuːtrə/ [brɔmmɔput̪rɔ nɔd̪] is one of the major rivers of Asia, a trans-boundary river which flows through China, India and Bangladesh. As such, it is known by various names in the region. In Assamese: ব্ৰহ্মপুত্ৰ নদী, Brôhmôputrô; Sanskrit: ब्रह्मपुत्र, IAST: Brahmaputra; Tibetan: ་, Wylie: yar klung gtsang po Yarlung Tsangpo; simplified Chinese: 布拉马普特拉河; traditional Chinese: 布拉馬普特拉河; pinyin: Bùlāmǎpǔtèlā Hé. It is also called Tsangpo-Brahaputra (when referring to the whole river including the stretch within Tibet),[2] and the Jamuna River in Bangladesh. The ...
