---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/five
offline_file: ""
offline_thumbnail: ""
uuid: 1633641b-a957-4557-b73f-12dd78d35606
updated: 1484310293
title: five
categories:
    - Dictionary
---
five
     adj : being one more than four [syn: {5}, {v}]
     n 1: the cardinal number that is the sum of four and one [syn: {5},
           {V}, {cinque}, {quint}, {quintet}, {fivesome}, {quintuplet},
           {pentad}, {fin}, {Phoebe}, {Little Phoebe}]
     2: a team that plays basketball [syn: {basketball team}]
