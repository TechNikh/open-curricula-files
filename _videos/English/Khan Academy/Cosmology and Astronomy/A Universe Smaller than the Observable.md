---
version: 1
type: video
provider: YouTube
id: 0w9R_foNLrg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/A%20Universe%20Smaller%20than%20the%20Observable.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 184efe4c-0d3c-4f65-ac02-5bb5230d6104
updated: 1486069685
title: A Universe Smaller than the Observable
categories:
    - Cosmology and Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/0w9R_foNLrg/default.jpg
    - https://i3.ytimg.com/vi/0w9R_foNLrg/1.jpg
    - https://i3.ytimg.com/vi/0w9R_foNLrg/2.jpg
    - https://i3.ytimg.com/vi/0w9R_foNLrg/3.jpg
---
