---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matter_wave
offline_file: ""
offline_thumbnail: ""
uuid: fd0ffe39-b33a-46fe-bb2d-5c0c9289933e
updated: 1484308893
title: Matter wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Propagation_of_a_de_broglie_wave.svg.png
tags:
    - Historical context
    - The de Broglie hypothesis
    - Experimental confirmation
    - Electrons
    - Neutral atoms
    - Molecules
    - de Broglie relations
    - Special relativity
    - Group velocity
    - Phase velocity
    - Four-vectors
    - Interpretations
    - "De Broglie's phase wave and periodic phenomenon"
categories:
    - Waves
---
All matter can exhibit wave-like behavior. For example, a beam of electrons can be diffracted just like a beam of light or a water wave. Matter waves are a central part of the theory of quantum mechanics, being an example of wave–particle duality. The concept that matter behaves like a wave is also referred to as the de Broglie hypothesis (/dəˈbrɔɪ/) due to having been proposed by Louis de Broglie in 1924.[1] Matter waves are often referred to as de Broglie waves.
