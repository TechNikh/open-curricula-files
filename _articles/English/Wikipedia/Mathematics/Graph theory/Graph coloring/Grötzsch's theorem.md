---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gr%C3%B6tzsch%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2c596294-6bc8-4ac5-93e4-0db3fc6fc490
updated: 1484309384
title: "Grötzsch's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bidiakis_cube_3COL.svg.png
tags:
    - History
    - Larger classes of graphs
    - Factoring through a homomorphism
    - Geometric representation
    - Computational complexity
    - Notes
categories:
    - Graph coloring
---
In the mathematical field of graph theory, Grötzsch's theorem is the statement that every triangle-free planar graph can be colored with only three colors. According to the four-color theorem, every graph that can be drawn in the plane without edge crossings can have its vertices colored using at most four different colors, so that the two endpoints of every edge have different colors, but according to Grötzsch's theorem only three colors are needed for planar graphs that do not contain three mutually-adjacent vertices.
