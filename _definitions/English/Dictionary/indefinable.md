---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indefinable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484646661
title: indefinable
categories:
    - Dictionary
---
indefinable
     adj 1: not capable of being precisely or readily described; not
            easily put into words; "an indefinable feeling of
            terror"; "an abstract concept that seems indefinable"
            [syn: {undefinable}]
     2: defying expression or description; "indefinable yearnings";
        "indescribable beauty"; "ineffable ecstasy";
        "inexpressible anguish"; "unspeakable happiness";
        "unutterable contempt"; "a thing of untellable splendor"
        [syn: {indescribable}, {ineffable}, {unspeakable}, {untellable},
         {unutterable}]
