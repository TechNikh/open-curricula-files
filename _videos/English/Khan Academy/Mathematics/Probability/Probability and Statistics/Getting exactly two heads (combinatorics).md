---
version: 1
type: video
provider: YouTube
id: 8TIben0bJpU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Getting%20exactly%20two%20heads%20%28combinatorics%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e4dc658-2744-4eb4-b0bb-eb6d1568e63c
updated: 1486069689
title: Getting exactly two heads (combinatorics)
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/8TIben0bJpU/default.jpg
    - https://i3.ytimg.com/vi/8TIben0bJpU/1.jpg
    - https://i3.ytimg.com/vi/8TIben0bJpU/2.jpg
    - https://i3.ytimg.com/vi/8TIben0bJpU/3.jpg
---
