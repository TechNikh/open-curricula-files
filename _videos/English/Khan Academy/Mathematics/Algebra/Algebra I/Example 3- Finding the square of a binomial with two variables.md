---
version: 1
type: video
provider: YouTube
id: 4fQeHtSdw80
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%203-%20Finding%20the%20square%20of%20a%20binomial%20with%20two%20variables.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f224f2c8-095d-40b3-be35-693776c336a8
updated: 1486069563
title: 'Example 3- Finding the square of a binomial with two variables'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/4fQeHtSdw80/default.jpg
    - https://i3.ytimg.com/vi/4fQeHtSdw80/1.jpg
    - https://i3.ytimg.com/vi/4fQeHtSdw80/2.jpg
    - https://i3.ytimg.com/vi/4fQeHtSdw80/3.jpg
---
