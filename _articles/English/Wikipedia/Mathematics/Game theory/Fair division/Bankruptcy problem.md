---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bankruptcy_problem
offline_file: ""
offline_thumbnail: ""
uuid: fde4dafa-a2d1-406f-adf3-4078f5d94b9d
updated: 1484308520
title: Bankruptcy problem
categories:
    - Fair division
---
In mathematical sociology, and especially game theory, the bankruptcy problem is a distribution or entitlement problem involving the allocation of a given amount of a perfectly divisible good among a group of agents. The focus is on the case where the amount is insufficient to satisfy all their demands.
