---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feat
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411761
title: feat
categories:
    - Dictionary
---
feat
     n : a notable achievement; "he performed a great deed"; "the
         book was her finest effort" [syn: {deed}, {effort}, {exploit}]
