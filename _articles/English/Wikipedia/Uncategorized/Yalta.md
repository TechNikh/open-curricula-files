---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yalta
offline_file: ""
offline_thumbnail: ""
uuid: f7e38ef4-8517-4e27-b9bc-0e0253e8d091
updated: 1484308485
title: Yalta
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Helen_villa.jpg
tags:
    - History
    - 12th-19th centuries
    - 20th century
    - Modern Yalta
    - Main sights
    - Climate
    - Demographics
    - Twin towns and sister cities
categories:
    - Uncategorized
---
Yalta (Crimean Tatar: Yalta; Russian: Я́лта; Ukrainian: Я́лта) is a resort city on the south coast of the Crimean Peninsula surrounded by the Black Sea. It serves as the administrative center of Yalta Municipality, one of the regions within Crimea. Population: 76,746 (2014 Census).[2]
