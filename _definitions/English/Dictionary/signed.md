---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/signed
offline_file: ""
offline_thumbnail: ""
uuid: cdbac399-868e-4120-b89d-5ca5147a4f87
updated: 1484310579
title: signed
categories:
    - Dictionary
---
signed
     adj 1: having a handwritten signature; "a signed letter" [ant: {unsigned}]
     2: used of the language of the deaf [syn: {gestural}, {sign(a)},
         {sign-language(a)}]
