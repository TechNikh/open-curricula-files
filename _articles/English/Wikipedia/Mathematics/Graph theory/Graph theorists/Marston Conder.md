---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marston_Conder
offline_file: ""
offline_thumbnail: ""
uuid: 4223e2f2-81b6-40ee-b07d-f0bfd13ce7b9
updated: 1484309463
title: Marston Conder
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Mars_Hubble.jpg
categories:
    - Graph theorists
---
Marston Donald Edward Conder (b. Sep. 1955) is a New Zealand mathematician, a Distinguished Professor of Mathematics at Auckland University,[1] and the former co-director of the New Zealand Institute of Mathematics and its Applications. His main research interests are in combinatorial group theory, graph theory, and their connections with each other.
