---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cubic
offline_file: ""
offline_thumbnail: ""
uuid: df03661e-767d-4c2c-a0ad-39a6954b76cb
updated: 1484310254
title: cubic
categories:
    - Dictionary
---
cubic
     adj : having three dimensions [syn: {three-dimensional}] [ant: {linear},
            {planar}]
