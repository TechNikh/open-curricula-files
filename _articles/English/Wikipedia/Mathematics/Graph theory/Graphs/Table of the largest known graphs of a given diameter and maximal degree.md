---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Table_of_the_largest_known_graphs_of_a_given_diameter_and_maximal_degree
offline_file: ""
offline_thumbnail: ""
uuid: a5eb197c-0de0-4269-ae59-db0985bd9638
updated: 1484309417
title: >
    Table of the largest known graphs of a given diameter and
    maximal degree
categories:
    - Graphs
---
In graph theory, the degree diameter problem is the problem of finding the largest possible graph for a given maximum degree and diameter. The Moore bound sets limits on this, but for many years mathematicians in the field have been interested in a more precise answer. The table below gives current progress on this problem (excluding the case of degree 2, where the largest graphs are cycles with an odd number of vertices).
