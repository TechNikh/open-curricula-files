---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landmark
offline_file: ""
offline_thumbnail: ""
uuid: 529b3577-a13b-4557-89ab-a4e920b25610
updated: 1484310605
title: landmark
categories:
    - Dictionary
---
landmark
     n 1: the position of a prominent or well-known object in a
          particular landscape; "the church steeple provided a
          convenient landmark"
     2: an event marking a unique or important historical change of
        course or one on which important developments depend; "the
        agreement was a watershed in the history of both nations"
        [syn: {turning point}, {watershed}]
     3: a mark showing the boundary of a piece of land
     4: an anatomical structure used as a point of origin in
        locating other anatomical structures (as in surgery) or as
        point from which measurements can be taken
