---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phonograph
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424421
title: phonograph
categories:
    - Dictionary
---
phonograph
     n : machine in which rotating records cause a stylus to vibrate
         and the vibrations are amplified acoustically or
         electronically [syn: {record player}]
