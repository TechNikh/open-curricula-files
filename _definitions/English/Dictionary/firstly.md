---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/firstly
offline_file: ""
offline_thumbnail: ""
uuid: 9bd616fe-ad0f-445c-ac58-9c9467973c5a
updated: 1484310531
title: firstly
categories:
    - Dictionary
---
firstly
     adv : before anything else; "first we must consider the garter
           snake" [syn: {first}, {foremost}, {first of all}, {first
           off}]
