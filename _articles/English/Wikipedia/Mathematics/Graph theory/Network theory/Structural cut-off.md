---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_cut-off
offline_file: ""
offline_thumbnail: ""
uuid: 8607113d-cf3f-4c84-ae46-76888444b7a7
updated: 1484309442
title: Structural cut-off
tags:
    - Definition
    - Structural cut-off for neutral networks
    - Structural disassortativity in scale-free networks
    - Impact of the structural cut-off
    - Generated networks
    - Real networks
categories:
    - Network theory
---
The structural cut-off is a concept in network science which imposes a degree cut-off in the degree distribution of a finite size network due to structural limitations (such as the simple graph property). Networks with vertices with degree higher than the structural cut-off will display structural disassortativity.
