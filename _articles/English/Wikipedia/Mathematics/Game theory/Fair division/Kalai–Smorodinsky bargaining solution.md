---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kalai%E2%80%93Smorodinsky_bargaining_solution'
offline_file: ""
offline_thumbnail: ""
uuid: 8f50d132-965f-4b66-bf2d-a8363e764765
updated: 1484308524
title: Kalai–Smorodinsky bargaining solution
tags:
    - Setting
    - Requirements from bargaining solutions
    - The KS solution
    - Examples
categories:
    - Fair division
---
The Kalai–Smorodinsky (KS) bargaining solution is a solution to the Bargaining problem. It was suggested by Ehud Kalai and Meir Smorodinsky,[1] as an alternative to Nash's bargaining solution suggested 25 years earlier. The main difference between the two solutions is that the Nash solution satisfies independence of irrelevant alternatives while the KS solution satisfies monotonicity.
