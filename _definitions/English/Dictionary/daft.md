---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daft
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447401
title: daft
categories:
    - Dictionary
---
daft
     adj : informal or slang terms for mentally irregular; "it used to
           drive my husband balmy" [syn: {balmy}, {barmy}, {bats},
            {batty}, {bonkers}, {buggy}, {cracked}, {crackers}, {dotty},
            {fruity}, {haywire}, {kooky}, {kookie}, {loco}, {loony},
            {loopy}, {nuts}, {nutty}, {round the bend}, {around
           the bend}, {wacky}, {whacky}]
