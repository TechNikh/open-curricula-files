---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Probability_of_kill
offline_file: ""
offline_thumbnail: ""
uuid: d2d46db5-7236-47b4-9617-12a447ef5114
updated: 1484309040
title: Probability of kill
categories:
    - Games (probability)
---
Computer games, simulations, models, and operations research programs often require a mechanism to determine statistically whether the engagement between a weapon and a target resulted in a kill, or the probability of kill. Statistical decisions are required when all of the variables that must be considered are not incorporated into the model, similar to the actuarial methods used by insurance companies to deal with large numbers of customers and huge numbers of variables. Likewise, military planners rely on such calculations to determine the amount of weapons necessary to destroy an enemy force.
