---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thinking
offline_file: ""
offline_thumbnail: ""
uuid: 655b7621-79e0-4d44-932e-d62937ef3b78
updated: 1484310399
title: thinking
categories:
    - Dictionary
---
thinking
     adj : endowed with the capacity to reason [syn: {intelligent}, {reasoning(a)},
            {thinking(a)}]
     n : the process of thinking (especially thinking carefully);
         "thinking always made him frown"; "she paused for
         thought" [syn: {thought}, {cerebration}, {intellection},
         {mentation}]
