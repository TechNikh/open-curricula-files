---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oily
offline_file: ""
offline_thumbnail: ""
uuid: b2f249bf-1d67-44e4-9d88-b2ab3a94e070
updated: 1484310426
title: oily
categories:
    - Dictionary
---
oily
     adj 1: containing an unusual amount of grease or oil; "greasy
            hamburgers"; "oily fried potatoes"; "oleaginous seeds"
            [syn: {greasy}, {sebaceous}, {oleaginous}]
     2: unpleasantly and excessively suave or ingratiating in manner
        or speech; "buttery praise"; "gave him a fulsome
        introduction"; "an oily sycophantic press agent";
        "oleaginous hypocrisy"; "smarmy self-importance"; "the
        unctuous Uriah Heep" [syn: {buttery}, {fulsome}, {oleaginous},
         {smarmy}, {unctuous}]
     3: coated or covered with oil; "oily puddles in the streets"
     4: smeared or soiled with grease or oil; "greasy coveralls";
        "get rid of ...
