---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Malthus
offline_file: ""
offline_thumbnail: ""
uuid: 0cddd3b8-6de6-4d80-80df-8b114f808d8b
updated: 1484308345
title: Thomas Robert Malthus
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Malthus_-_Essay_on_the_principle_of_population%252C_1826_-_5884843.tif.jpg'
tags:
    - Early life and education
    - Population growth
    - Academic
    - Malthus–Ricardo debate on political economy
    - Later life
    - family
    - An Essay on the Principle of Population
    - Editions and versions
    - Other works
    - '1800: The present high price of provisions'
    - '1814: Observations on the effects of the Corn Laws'
    - '1820: Principles of political economy'
    - Other publications
    - Reception and influence
    - References in popular culture
    - Epitaph
    - Notes
categories:
    - Uncategorized
---
The Reverend Thomas Robert Malthus FRS (13 February 1766 – 29 December 1834)[1] was an English cleric and scholar, influential in the fields of political economy and demography.[2] Malthus himself used only his middle name, Robert.[3]
