---
version: 1
type: video
provider: YouTube
id: 4F6cFLnAAFc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Fractional%20exponent%20expressions%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f6f251e0-9b50-4ef0-a9b0-0b28df3bb113
updated: 1486069559
title: Fractional exponent expressions 2
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/4F6cFLnAAFc/default.jpg
    - https://i3.ytimg.com/vi/4F6cFLnAAFc/1.jpg
    - https://i3.ytimg.com/vi/4F6cFLnAAFc/2.jpg
    - https://i3.ytimg.com/vi/4F6cFLnAAFc/3.jpg
---
