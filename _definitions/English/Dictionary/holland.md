---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holland
offline_file: ""
offline_thumbnail: ""
uuid: f9ead8c4-4ddf-47b8-8802-2854456a7c57
updated: 1484310569
title: holland
categories:
    - Dictionary
---
Holland
     n : a constitutional monarchy in western Europe on the North
         Sea; achieved independence from Spain in 1579; half the
         country lies below sea level [syn: {Netherlands}, {The
         Netherlands}, {Kingdom of The Netherlands}]
