---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/medulla
offline_file: ""
offline_thumbnail: ""
uuid: c56a4364-e4dd-46f1-8810-ade8e5405324
updated: 1484310335
title: medulla
categories:
    - Dictionary
---
medulla
     n 1: a white fatty substance that forms a medullary sheath around
          the axis cylinder of some nerve fibers [syn: {myelin}, {myeline}]
     2: lower or hindmost part of the brain; continuous with spinal
        cord; (`bulb' is an old term for medulla oblongata); "the
        medulla oblongata is the most vital part of the brain
        because it contains centers controlling breathing and
        heart functioning" [syn: {medulla oblongata}, {bulb}]
     3: the inner part of an organ or structure in plant or animal
        [ant: {cortex}]
     [also: {medullae} (pl)]
