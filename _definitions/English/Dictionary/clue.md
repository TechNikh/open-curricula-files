---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clue
offline_file: ""
offline_thumbnail: ""
uuid: e8ae1332-0194-42d9-a425-3f2893234587
updated: 1484310218
title: clue
categories:
    - Dictionary
---
clue
     n 1: a slight indication [syn: {hint}]
     2: evidence that helps to solve a problem [syn: {clew}, {cue}]
     v : roll into a ball [syn: {clew}]
