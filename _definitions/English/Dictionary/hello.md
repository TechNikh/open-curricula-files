---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hello
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484455561
title: hello
categories:
    - Dictionary
---
hello
     n : an expression of greeting; "every morning they exchanged
         polite hellos" [syn: {hullo}, {hi}, {howdy}, {how-do-you-do}]
