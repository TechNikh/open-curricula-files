---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cosy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508361
title: cosy
categories:
    - Dictionary
---
cosy
     adj : enjoying or affording comforting warmth and shelter
           especially in a small space; "a cozy nook near the
           fire"; "snug in bed"; "a snug little apartment" [syn: {cozy},
            {snug}]
     n : a padded cloth covering to keep a teapot warm [syn: {tea
         cosy}, {cosey}, {tea cosey}, {cozy}, {tea cozy}, {cozey},
          {tea cozey}, {cozie}, {tea cozie}]
     [also: {cosiest}, {cosier}]
