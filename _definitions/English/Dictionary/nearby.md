---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nearby
offline_file: ""
offline_thumbnail: ""
uuid: 8b43cfe1-aa9b-47be-bdb5-ccae0def9b86
updated: 1484310459
title: nearby
categories:
    - Dictionary
---
nearby
     adj : close at hand; "the nearby towns"; "concentrated his study
           on the nearby planet Venus"
     adv : not far away in relative terms; "she works nearby"; "the
           planets orbiting nearby are Venus and Mars"
