---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antenatal_diagnosis
offline_file: ""
offline_thumbnail: ""
uuid: 4684ab7d-6c34-4ca4-9c3c-263e7a114916
updated: 1484308361
title: Prenatal diagnosis
tags:
    - Invasiveness
    - Fetal versus maternal
    - Reasons for prenatal diagnosis
    - Qualifying risk factors
    - Methods of prenatal screening and diagnosis
    - Maternal serum screening
    - Advances in prenatal screening
    - Typical screening sequence
    - Conditions typically tested for
    - Aneuploidy
    - Noninvasive techniques for detecting aneuploidy
    - Digital PCR
    - Shotgun sequencing
    - Other techniques
    - Patient acceptance
    - Rarer conditions also detected
    - Ethical and practical issues
    - Ethical issues of prenatal testing
    - Will the result of the test affect treatment of the fetus?
    - False positives and false negatives
    - Societal pressures on prenatal testing decisions
    - Informed consent and medical malpractice
    - Notes and references
categories:
    - Uncategorized
---
Prenatal diagnosis and prenatal screening are aspects of prenatal care that focus on detecting anatomic and physiologic problems with the zygote, embryo, or fetus as early as possible, either before gestation even starts (as in preimplantation genetic diagnosis) or as early in gestation as practicable. They use medical tests to detect problems such as neural tube defects, chromosome abnormalities, and gene mutations that would lead to genetic disorders and birth defects, such as spina bifida, cleft palate, Tay–Sachs disease, sickle cell anemia, thalassemia, cystic fibrosis, muscular dystrophy, and fragile X syndrome. The screening focuses on finding problems among a large population with ...
