---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stadium
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484525881
title: stadium
categories:
    - Dictionary
---
stadium
     n : a large structure for open-air sports or entertainments
         [syn: {bowl}, {arena}, {sports stadium}]
     [also: {stadia} (pl)]
