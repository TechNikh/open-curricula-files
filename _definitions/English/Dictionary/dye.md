---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dye
offline_file: ""
offline_thumbnail: ""
uuid: 07e206ea-c443-44de-8bd0-8b52e2ae194b
updated: 1484310381
title: dye
categories:
    - Dictionary
---
dye
     n : a usually soluble substance for staining or coloring e.g.
         fabrics or hair [syn: {dyestuff}]
     v : color with dye; "Please dye these shoes"
