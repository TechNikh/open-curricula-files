---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/somehow
offline_file: ""
offline_thumbnail: ""
uuid: 62d4eab9-2d73-4163-81be-7861f6f8764c
updated: 1484310178
title: somehow
categories:
    - Dictionary
---
somehow
     adv 1: in some unspecified way or manner; or by some unspecified
            means; "they managed somehow"; "he expected somehow to
            discover a woman who would love him"; "he tried to
            make is someway acceptable" [syn: {someway}, {someways},
             {in some way}, {in some manner}]
     2: for some unspecified reason; "It doesn't seem fair somehow";
        "he had me dead to rights but somehow I got away with it";
        "for some reason they didn't seem to match" [syn: {for
        some reason}]
