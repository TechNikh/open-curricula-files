---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transfer_operator
offline_file: ""
offline_thumbnail: ""
uuid: 9e0753f0-6901-480b-b137-b4ed0da3c1a6
updated: 1484308712
title: Transfer operator
categories:
    - Chaos theory
---
In mathematics, the transfer operator encodes information about an iterated map and is frequently used to study the behavior of dynamical systems, statistical mechanics, quantum chaos and fractals. The transfer operator is sometimes called the Ruelle operator, after David Ruelle, or the Ruelle–Perron–Frobenius operator in reference to the applicability of the Frobenius–Perron theorem to the determination of the eigenvalues of the operator.
