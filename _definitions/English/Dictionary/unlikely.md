---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unlikely
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484382541
title: unlikely
categories:
    - Dictionary
---
unlikely
     adj 1: not likely to be true or to occur or to have occurred;
            "legislation on the question is highly unlikely"; "an
            improbable event" [syn: {improbable}] [ant: {probable}]
     2: has little chance of being the case or coming about; "an
        unlikely story"; "an unlikely candidate for reelection";
        "a butcher is unlikely to preach vegetarianism" [ant: {likely}]
     3: having a probability to low to inspire belief [syn: {improbable},
         {unbelievable}, {unconvincing}]
