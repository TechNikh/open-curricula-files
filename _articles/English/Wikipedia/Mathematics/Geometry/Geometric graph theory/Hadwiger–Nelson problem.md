---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hadwiger%E2%80%93Nelson_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 2c47652a-bee5-4472-a966-06503da3a993
updated: 1484309241
title: Hadwiger–Nelson problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Hadwiger-Nelson.svg.png
tags:
    - Lower and upper bounds
    - Variations of the problem
    - Notes
categories:
    - Geometric graph theory
---
In geometric graph theory, the Hadwiger–Nelson problem, named after Hugo Hadwiger and Edward Nelson, asks for the minimum number of colors required to color the plane such that no two points at distance 1 from each other have the same color. The answer is unknown, but has been narrowed down to one of the numbers 4, 5, 6 or 7. The correct value may actually depend on the choice of axioms for set theory.[1]
