---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sum_of_radicals
offline_file: ""
offline_thumbnail: ""
uuid: b5c89f5c-07f6-44a1-bbab-ec2254643b4f
updated: 1484309179
title: Sum of radicals
categories:
    - Computational geometry
---
In computational complexity theory, there is an open problem of whether some information about a sum of radicals may be computed in polynomial time depending on the input size, i.e., in the number of bits necessary to represent this sum. It is of importance for many problems in computational geometry, since the computation of the Euclidean distance between two points in the general case involves the computation of a square root, and therefore the perimeter of a polygon or the length of a polygonal chain takes the form of a sum of radicals.[1]
