---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/career
offline_file: ""
offline_thumbnail: ""
uuid: 8ab5e22a-f442-48bc-963e-09525830d5bc
updated: 1484310429
title: career
categories:
    - Dictionary
---
career
     n 1: the particular occupation for which you are trained [syn: {calling},
           {vocation}]
     2: the general progression of your working or professional
        life; "the general had had a distinguished career"; "he
        had a long career in the law" [syn: {life history}]
     v : move headlong at high speed; "The cars careered down the
         road"; "The mob careered through the streets"
