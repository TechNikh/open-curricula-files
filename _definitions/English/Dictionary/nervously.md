---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nervously
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576761
title: nervously
categories:
    - Dictionary
---
nervously
     adv 1: in an anxiously nervous manner; "we watched the stock market
            nervously"
     2: with nervous excitement; "our bodies jumped nervously away
        at the slightest touch"
