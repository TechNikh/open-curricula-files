---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Liquation
offline_file: ""
offline_thumbnail: ""
uuid: 642a2745-d8de-4843-8bee-677b61d88d41
updated: 1484308389
title: Liquation
tags:
    - Separating copper and silver
    - Process
    - Loss of Metal
    - First instances
    - Importance
categories:
    - Uncategorized
---
Liquation is a metallurgical method for separating metals from an ore or alloy. The material must be heated until one of the metals starts to melt and drain away from the other and can be collected. This method was largely used to remove lead containing silver from copper, but it can also be used to remove antimony minerals from ore, and refine tin.
