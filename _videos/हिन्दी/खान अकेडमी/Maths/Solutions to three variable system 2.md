---
version: 1
type: video
provider: YouTube
id: uypFNyH8MT8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e650d34c-ed83-42d8-9de2-b1082c353883
updated: 1486069616
title: Solutions to three variable system 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/uypFNyH8MT8/default.jpg
    - https://i3.ytimg.com/vi/uypFNyH8MT8/1.jpg
    - https://i3.ytimg.com/vi/uypFNyH8MT8/2.jpg
    - https://i3.ytimg.com/vi/uypFNyH8MT8/3.jpg
---
