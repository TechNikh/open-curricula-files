---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/go
offline_file: ""
offline_thumbnail: ""
uuid: b27f5c8f-287e-4661-bf14-f6838b04feba
updated: 1484310363
title: go
categories:
    - Dictionary
---
go
     adj : functioning correctly and ready for action; "all systems are
           go" [ant: {no-go}]
     n 1: a time for working (after which you will be relieved by
          someone else); "it's my go"; "a spell of work" [syn: {spell},
           {tour}, {turn}]
     2: street names for methylenedioxymethamphetamine [syn: {Adam},
         {ecstasy}, {XTC}, {disco biscuit}, {cristal}, {X}, {hug
        drug}]
     3: a usually brief attempt; "he took a crack at it"; "I gave it
        a whirl" [syn: {crack}, {fling}, {pass}, {whirl}, {offer}]
     4: a board game for two players who place counters on a grid;
        the object is to surround and so capture the opponent's
        ...
