---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_reservoir
offline_file: ""
offline_thumbnail: ""
uuid: ce3c5833-a416-4be2-933b-3c254a25d48b
updated: 1484308846
title: Thermal reservoir
categories:
    - Thermodynamics
---
A thermal reservoir, a short-form of thermal energy reservoir, or thermal bath is a thermodynamic system with a heat capacity that is large enough that when it is in thermal contact with another system of interest or its environment, its temperature remains effectively constant.[1] It is an effectively infinite pool of thermal energy at a given, constant temperature. The temperature of the reservoir does not change when heat is added or extracted because of the infinite heat capacity. As it can act as a source and sink of heat, it is often also referred to as a heat reservoir or heat bath.
