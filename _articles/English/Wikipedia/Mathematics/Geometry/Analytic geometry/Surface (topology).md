---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surface_(topology)
offline_file: ""
offline_thumbnail: ""
uuid: e9c16f16-68cd-471b-87b8-b77a648aaf9f
updated: 1484309168
title: Surface (topology)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Saddle_Point.png
tags:
    - In general
    - Definitions and first examples
    - Extrinsically defined surfaces and embeddings
    - Construction from polygons
    - Connected sums
    - Closed surfaces
    - Classification of closed surfaces
    - Monoid structure
    - Surfaces with boundary
    - Riemann surfaces
    - Non-compact surfaces
    - Surfaces that are not even second countable
    - Proof
    - Surfaces in geometry
    - Notes
    - Simplicial proofs of classification up to homeomorphism
    - >
        Morse theoretic proofs of classification up to
        diffeomorphism
    - Other proofs
categories:
    - Analytic geometry
---
In topology and differential geometry, a surface is a two-dimensional manifold, and, as such, may be an "abstract surface" not embedded in any Euclidean space. For example, the Klein bottle is a surface, which cannot be represented in the three-dimensional Euclidean space without introducing self-intersections (it cannot be embedded in the three dimensional Euclidean space).
