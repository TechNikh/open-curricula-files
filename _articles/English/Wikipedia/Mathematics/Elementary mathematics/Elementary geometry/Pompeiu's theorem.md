---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pompeiu%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5accb4ab-0cee-41c3-8d4c-eae06a4a7a5b
updated: 1484308935
title: "Pompeiu's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Pompeiu_theorem1.svg.png
categories:
    - Elementary geometry
---
Pompeiu's theorem is a result of plane geometry, discovered by the Romanian mathematician Dimitrie Pompeiu. The theorem is quite simple, but not classical. It states the following:
