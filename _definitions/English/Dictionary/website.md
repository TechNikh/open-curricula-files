---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/website
offline_file: ""
offline_thumbnail: ""
uuid: bafece26-7329-42ff-ba7d-5c91c4a3e8c9
updated: 1484310547
title: website
categories:
    - Dictionary
---
web site
     n : a computer connected to the internet that maintains a series
         of web pages on the World Wide Web; "the Israeli web site
         was damaged by hostile hackers" [syn: {internet site}, {site}]
