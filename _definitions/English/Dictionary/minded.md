---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minded
offline_file: ""
offline_thumbnail: ""
uuid: 560796fb-ef68-4687-a383-2d39010d2254
updated: 1484310189
title: minded
categories:
    - Dictionary
---
minded
     adj 1: (used in combination) mentally oriented toward something
            specified; "civic-minded"; "career-minded"
     2: (used in combination) being of a specified kind of
        inclination or disposition; "serious-minded";
        "fair-minded"
     3: (usually followed by `to') naturally disposed toward; "he is
        apt to ignore matters he considers unimportant"; "I am not
        minded to answer any questions" [syn: {apt(p)}, {disposed(p)},
         {given(p)}, {minded(p)}, {tending(p)}]
