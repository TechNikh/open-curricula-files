---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lowest
offline_file: ""
offline_thumbnail: ""
uuid: 35642a9a-43ba-4b2e-9abf-326fe6c97547
updated: 1484310258
title: lowest
categories:
    - Dictionary
---
lowest
     adj 1: lowest in rank or importance; "last prize"; "in last place"
            [syn: {last}, {last-place}]
     2: minimal in magnitude; "lowest wages"; "the least amount of
        fat allowed"; "the smallest amount" [syn: {least}, {smallest}]
     3: at the bottom; lowest or last; "the bottom price" [syn: {bottom}]
     adv : in the lowest position; nearest the ground; "the branch with
           the big peaches on it hung lowest"
