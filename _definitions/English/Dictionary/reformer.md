---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reformer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484599021
title: reformer
categories:
    - Dictionary
---
reformer
     n 1: a disputant who advocates reform [syn: {reformist}, {crusader},
           {meliorist}]
     2: an apparatus that reforms the molecular structure of
        hydrocarbons to produce richer fuel; "a catalytic
        reformer"
