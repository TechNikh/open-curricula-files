---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wasserstein_metric
offline_file: ""
offline_thumbnail: ""
uuid: 1dc0e271-c1e4-44bf-9871-7e095974748d
updated: 1484309277
title: Wasserstein metric
tags:
    - Definition
    - Applications
    - Properties
    - Metric structure
    - Dual representation of W1
    - Separability and completeness
categories:
    - Metric geometry
---
Intuitively, if each distribution is viewed as a unit amount of "dirt" piled on M, the metric is the minimum "cost" of turning one pile into the other, which is assumed to be the amount of dirt that needs to be moved times the distance it has to be moved. Because of this analogy, the metric is known in computer science as the earth mover's distance.
