---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_electromagnetics
offline_file: ""
offline_thumbnail: ""
uuid: 2d0132ae-d2d1-4e0b-a217-bbeb478fcad8
updated: 1484308751
title: Computational electromagnetics
tags:
    - Background
    - Overview of methods
    - Choice of methods
    - "Maxwell's equations in hyperbolic PDE form"
    - Integral equation solvers
    - The discrete dipole approximation
    - Method of moments (MoM) or boundary element method (BEM)
    - Fast multipole method (FMM)
    - Plane wave time-domain (PWTD)
    - Partial element equivalent circuit (PEEC) method
    - Differential equation solvers
    - Finite-difference time-domain (FDTD)
    - Multiresolution time-domain (MRTD)
    - Finite element method (FEM)
    - Finite integration technique (FIT)
    - Pseudospectral time domain (PSTD)
    - Pseudo-spectral spatial domain (PSSD)
    - Transmission line matrix (TLM)
    - Locally one-dimensional FDTD (LOD-FDTD)
    - Other methods
    - EigenMode Expansion (EME)
    - Physical optics (PO)
    - Uniform theory of diffraction (UTD)
    - Validation
    - Light scattering codes
    - Software
categories:
    - Electrodynamics
---
Computational electromagnetics, computational electrodynamics or electromagnetic modeling is the process of modeling the interaction of electromagnetic fields with physical objects and the environment.
