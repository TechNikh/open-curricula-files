---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marcel_Grossmann
offline_file: ""
offline_thumbnail: ""
uuid: d1734e10-909c-48b3-873b-717a73f98b67
updated: 1484309085
title: Marcel Grossmann
tags:
    - Career
    - Collaborations with Albert Einstein
    - End of Life
    - Notes
categories:
    - Geometers
---
Marcel Grossmann (Hungarian: Grossmann Marcell, April 9, 1878 – September 7, 1936) was a mathematician and a friend and classmate of Albert Einstein. Grossmann was a member of an old Swiss family from Zurich. His father managed a textile factory. He became a Professor of Mathematics at the Federal Polytechnic Institute in Zurich, today the ETH Zurich, specializing in descriptive geometry.
