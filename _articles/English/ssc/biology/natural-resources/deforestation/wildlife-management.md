---
title: Wildlife management
article_ref: https://en.wikipedia.org/wiki/Wildlife_management
tags:
    - History
    - Game laws
    - Emergence of wildlife conservation
    - Wildlife management in the US
    - Types of wildlife management
    - Opposition
    - Management of hunting seasons
    - Open season
    - Limited entry
    - Closed season
    - Type of weapon used
categories:
    - Deforestation
---
