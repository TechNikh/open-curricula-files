---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ultrametric_space
offline_file: ""
offline_thumbnail: ""
uuid: 1bfb244f-0d4f-4c28-a457-eb3caa560ae8
updated: 1484309273
title: Ultrametric space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/208px-Strong_triangle_ineq.svg.png
tags:
    - Formal definition
    - Properties
    - Examples
    - Applications
categories:
    - Metric geometry
---
In mathematics, an ultrametric space is a special kind of metric space in which the triangle inequality is replaced with 
  
    
      
        d
        (
        x
        ,
        z
        )
        ≤
        max
        
          {
          d
          (
          x
          ,
          y
          )
          ,
          d
          (
          y
          ,
          z
          )
          }
        
      
    
    {\displaystyle d(x,z)\leq \max \left\{d(x,y),d(y,z)\right\}}
  
. Sometimes the associated metric is also called a non-Archimedean metric or super-metric. Although some of the theorems for ultrametric spaces may seem strange at a first glance, they appear ...
