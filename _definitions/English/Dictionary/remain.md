---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remain
offline_file: ""
offline_thumbnail: ""
uuid: 2c27fd3c-6dd9-4758-811a-741bc426cfe5
updated: 1484310328
title: remain
categories:
    - Dictionary
---
remain
     v 1: stay the same; remain in a certain state; "The dress
          remained wet after repeated attempts to dry it"; "rest
          assured"; "stay alone"; "He remained unmoved by her
          tears"; "The bad weather continued for another week"
          [syn: {stay}, {rest}] [ant: {change}]
     2: continue in a place, position, or situation; "After
        graduation, she stayed on in Cambridge as a student
        adviser"; "Stay with me, please"; "despite student
        protests, he remained Dean for another year"; "She
        continued as deputy mayor for another year" [syn: {stay},
        {stay on}, {continue}]
     3: be left; of persons, questions, problems, ...
