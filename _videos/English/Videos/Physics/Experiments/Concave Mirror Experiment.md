---
version: 1
type: video
provider: YouTube
id: 8Z_FKwAdZHM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fa9d3275-d222-4398-8d3c-e818b147c2e7
updated: 1486069635
title: Concave Mirror Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/8Z_FKwAdZHM/default.jpg
    - https://i3.ytimg.com/vi/8Z_FKwAdZHM/1.jpg
    - https://i3.ytimg.com/vi/8Z_FKwAdZHM/2.jpg
    - https://i3.ytimg.com/vi/8Z_FKwAdZHM/3.jpg
---
