---
version: 1
type: video
provider: YouTube
id: Y0eWnOZpSpQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Plate%20Tectonics-%20Geological%20features%20of%20Convergent%20Plate%20Boundaries.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20499a29-4081-45bb-9e37-b89d0ff773f1
updated: 1486069686
title: 'Plate Tectonics-- Geological features of Convergent Plate Boundaries'
categories:
    - Cosmology and Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y0eWnOZpSpQ/default.jpg
    - https://i3.ytimg.com/vi/Y0eWnOZpSpQ/1.jpg
    - https://i3.ytimg.com/vi/Y0eWnOZpSpQ/2.jpg
    - https://i3.ytimg.com/vi/Y0eWnOZpSpQ/3.jpg
---
