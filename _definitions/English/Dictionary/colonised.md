---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colonised
offline_file: ""
offline_thumbnail: ""
uuid: 17cf21ec-e028-4a58-87e4-fee90305b4b9
updated: 1484310178
title: colonised
categories:
    - Dictionary
---
colonised
     adj : inhabited by colonists [syn: {colonized}, {settled}]
