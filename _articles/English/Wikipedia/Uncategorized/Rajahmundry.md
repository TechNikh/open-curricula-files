---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rajahmundry
offline_file: ""
offline_thumbnail: ""
uuid: d4f3d1e1-e60d-4e51-ade9-3e7eebbdb110
updated: 1484308455
title: Rajahmundry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Portrait_of_Nannayya.JPG
tags:
    - Etymology
    - History
    - Fort Gate (Kotagummam)
    - The fort of the Dutch
    - geography
    - Climate
    - Demographics
    - Governance
    - Infrastructure
    - Bridges
    - Transport
    - Roadways
    - Railways
    - Waterways
    - Airways
    - Tourism
    - Economy
    - culture
    - Education
    - Notable people
categories:
    - Uncategorized
---
Rajahmundry (officially: Rajamahendravaram) is one of the major cities in the Indian state of Andhra Pradesh. It is located on the banks of the Godavari River, in East Godavari district of the state. The city is the mandal headquarters to both Rajahmundry (rural) and Rajahmundry (urban) mandals. It is also the divisional headquarters of Rajahmundry revenue division and one of the two municipal corporations in the district, alongside Kakinada and the biggest city in both the godavari districts waiting for a greater status.[4] As of 2011[update] census, it is the sixth most populous city in the state, with a population of 341,831.[5]
