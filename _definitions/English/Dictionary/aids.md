---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aids
offline_file: ""
offline_thumbnail: ""
uuid: 738d8748-4d61-4b31-8a63-c6e8f21d7b50
updated: 1484310328
title: aids
categories:
    - Dictionary
---
AIDS
     n : a serious (often fatal) disease of the immune system
         transmitted through blood products especially by sexual
         contact or contaminated needles [syn: {acquired immune
         deficiency syndrome}]
