---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gauss_map
offline_file: ""
offline_thumbnail: ""
uuid: 5773b62f-e7f4-475b-8efa-fcfeac14ce47
updated: 1484309221
title: Gauss map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Gauss_map.svg.png
tags:
    - Generalizations
    - Total curvature
    - Cusps of the Gauss map
categories:
    - Differential geometry
---
In differential geometry, the Gauss map (named after Carl F. Gauss) maps a surface in Euclidean space R3 to the unit sphere S2. Namely, given a surface X lying in R3, the Gauss map is a continuous map N: X → S2 such that N(p) is a unit vector orthogonal to X at p, namely the normal vector to X at p.
