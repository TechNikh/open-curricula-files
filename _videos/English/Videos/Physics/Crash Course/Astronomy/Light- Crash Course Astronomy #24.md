---
version: 1
type: video
provider: YouTube
id: jjy-eqWM38g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e665d8a9-ea4c-4113-95bc-e3fe401a20ed
updated: 1486069684
title: 'Light- Crash Course Astronomy #24'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/jjy-eqWM38g/default.jpg
    - https://i3.ytimg.com/vi/jjy-eqWM38g/1.jpg
    - https://i3.ytimg.com/vi/jjy-eqWM38g/2.jpg
    - https://i3.ytimg.com/vi/jjy-eqWM38g/3.jpg
---
