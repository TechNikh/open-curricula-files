---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arthur_Besse
offline_file: ""
offline_thumbnail: ""
uuid: 81444d86-1aa5-4dcc-9e1c-8b529248673b
updated: 1484309204
title: Arthur Besse
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_11.jpg
categories:
    - Differential geometry
---
Arthur Besse is a pseudonym chosen by a group of French differential geometers, led by Marcel Berger, following the model of Nicolas Bourbaki. A number of monographs have appeared under the name.
