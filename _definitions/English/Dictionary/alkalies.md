---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkalies
offline_file: ""
offline_thumbnail: ""
uuid: 3595b1b3-03fc-463a-97be-4300e04cdb57
updated: 1484310424
title: alkalies
categories:
    - Dictionary
---
alkali
     n 1: any of various water-soluble compounds capable of turning
          litmus blue and reacting with an acid to form a salt and
          water; "bases include oxides and hydroxides of metals
          and ammonia" [syn: {base}]
     2: a mixture of soluble salts found in arid soils and some
        bodies of water; detrimental to agriculture
     [also: {alkalies} (pl)]
