---
version: 1
type: video
provider: YouTube
id: O3BzU9HRU2k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3145a1e5-9944-4b6a-b70b-58207b3ff445
updated: 1486069609
title: Subtraction word problems within 10
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/O3BzU9HRU2k/default.jpg
    - https://i3.ytimg.com/vi/O3BzU9HRU2k/1.jpg
    - https://i3.ytimg.com/vi/O3BzU9HRU2k/2.jpg
    - https://i3.ytimg.com/vi/O3BzU9HRU2k/3.jpg
---
