---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Droste_effect
offline_file: ""
offline_thumbnail: ""
uuid: 84724af9-c865-4f50-aac3-bd5249c2033f
updated: 1484309309
title: Droste effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Droste.jpg
tags:
    - Effect
categories:
    - Symmetry
---
The Droste effect—known as mise en abyme in art—is the effect of a picture appearing within itself, in a place where a similar picture would realistically be expected to appear.[1] The appearance is recursive: the smaller version contains an even smaller version of the picture, and so on. Only in theory could this go on forever; practically, it continues only as long as the resolution of the picture allows, which is relatively short, since each iteration geometrically reduces the picture's size. It is a visual example of a strange loop, a self-referential system of instancing which is the cornerstone of fractal geometry.
