---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experimentation
offline_file: ""
offline_thumbnail: ""
uuid: 6e30e2a2-92a2-4a12-8dff-a1a58e4a4cb4
updated: 1484310311
title: experimentation
categories:
    - Dictionary
---
experimentation
     n 1: the testing of an idea; "it was an experiment in living";
          "not all experimentation is done in laboratories" [syn:
          {experiment}]
     2: the act of conducting a controlled test or investigation
        [syn: {experiment}]
