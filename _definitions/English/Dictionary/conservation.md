---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conservation
offline_file: ""
offline_thumbnail: ""
uuid: 8064345d-91e1-470e-8668-a065fda5ace0
updated: 1484310256
title: conservation
categories:
    - Dictionary
---
conservation
     n 1: an occurrence of improvement by virtue of preventing loss or
          injury or other change [syn: {preservation}]
     2: the preservation and careful management of the environment
        and of natural resources
     3: (physics) the maintenance of a certain quantities unchanged
        during chemical reactions or physical transformations
