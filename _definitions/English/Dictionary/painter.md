---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/painter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484362501
title: painter
categories:
    - Dictionary
---
painter
     n 1: an artist who paints
     2: a worker who is employed to cover objects with paint
     3: a line that is attached to the bow of a boat and used for
        tying up (as when docking or towing)
     4: large American feline resembling a lion [syn: {cougar}, {puma},
         {catamount}, {mountain lion}, {panther}, {Felis concolor}]
