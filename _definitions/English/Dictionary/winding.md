---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/winding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561461
title: winding
categories:
    - Dictionary
---
winding
     adj 1: marked by repeated turns and bends; "a tortuous road up the
            mountain"; "winding roads are full of surprises"; "had
            to steer the car down a twisty track" [syn: {tortuous},
             {twisting}, {twisty}]
     2: of a path e.g.; "meandering streams"; "rambling forest
        paths"; "the river followed its wandering course"; "a
        winding country road" [syn: {meandering(a)}, {rambling}, {wandering(a)}]
     n : the act of winding or twisting; "he put the key in the old
         clock and gave it a good wind" [syn: {wind}, {twist}]
