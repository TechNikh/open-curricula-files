---
version: 1
type: video
provider: YouTube
id: YjTo-3iOwkg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8e0e1ab4-2d23-4aa5-8bb2-b4471d32522e
updated: 1486069616
title: Multiplying by multiples of 10
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/YjTo-3iOwkg/default.jpg
    - https://i3.ytimg.com/vi/YjTo-3iOwkg/1.jpg
    - https://i3.ytimg.com/vi/YjTo-3iOwkg/2.jpg
    - https://i3.ytimg.com/vi/YjTo-3iOwkg/3.jpg
---
