---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bundelkhand
offline_file: ""
offline_thumbnail: ""
uuid: 1e1d9e49-de31-47e9-a3dd-2ee83419c297
updated: 1484308422
title: Bundelkhand
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-UP_region_map.gif
tags:
    - Etymology
    - geography
    - Ecology
    - History
    - 'Medieval Period & Maratha Rule'
    - British rule, 1802–1947
    - Independent India, 1947–present
    - Proposed Bundelkhand state
    - culture
    - Folk dances
    - Radio
    - Prominent Bundelkhandis
categories:
    - Uncategorized
---
Bundelkhand is a region and also a mountain range in central India. The hilly region is now divided between the states of Uttar Pradesh and Madhya Pradesh, with the larger portion lying in M.P.
