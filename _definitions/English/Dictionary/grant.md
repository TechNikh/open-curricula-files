---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grant
offline_file: ""
offline_thumbnail: ""
uuid: bb33981e-23e2-4842-a414-c46fa5b5651d
updated: 1484310181
title: grant
categories:
    - Dictionary
---
grant
     n 1: any monetary aid
     2: the act of providing a subsidy [syn: {subsidization}, {subsidisation}]
     3: (law) a transfer of property by deed of conveyance [syn: {assignment}]
     4: Scottish painter; cousin of Lytton Strachey and member of
        the Bloomsbury Group (1885-1978) [syn: {Duncan Grant}, {Duncan
        James Corrow Grant}]
     5: United States actor (born in England) who was the elegant
        leading man in many films (1904-1986) [syn: {Cary Grant}]
     6: 18th President of the United States; commander of the Union
        armies in the American Civil War (1822-1885) [syn: {Ulysses
        Grant}, {Ulysses S. Grant}, {Ulysses Simpson Grant}, {Hiram
       ...
