---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smallscale
offline_file: ""
offline_thumbnail: ""
uuid: 2e61086d-bd0b-4286-ad24-44b6a5db92a9
updated: 1484310457
title: smallscale
categories:
    - Dictionary
---
small-scale
     adj 1: created or drawn on a small scale; "small-scale maps"; "a
            small-scale model"
     2: limited in size or scope; "a small business"; "a newspaper
        with a modest circulation"; "small-scale plans"; "a
        pocket-size country" [syn: {minor}, {modest}, {small}, {pocket-size},
         {pocket-sized}]
