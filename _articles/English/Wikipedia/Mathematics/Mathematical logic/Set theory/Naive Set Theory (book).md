---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Naive_Set_Theory_(book)
offline_file: ""
offline_thumbnail: ""
uuid: 737746b1-46f9-4a6d-b640-c950e70e6706
updated: 1484309509
title: Naive Set Theory (book)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NaiveSetTheory.jpg
tags:
    - Bibliography
categories:
    - Set theory
---
Naive Set Theory is a mathematics textbook by Paul Halmos providing an undergraduate introduction to set theory.[1] Originally published by Van Nostrand in 1960,[2] it was reprinted in the Springer-Verlag Undergraduate Texts in Mathematics series in 1974.[3]
