---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surely
offline_file: ""
offline_thumbnail: ""
uuid: 6b0a478f-baef-4533-8744-32a6d9a00252
updated: 1484310154
title: surely
categories:
    - Dictionary
---
surely
     adv : definitely or positively (`sure' is sometimes used
           informally for `surely'); "the results are surely
           encouraging"; "she certainly is a hard worker"; "it's
           going to be a good day for sure"; "they are coming, for
           certain"; "they thought he had been killed sure
           enough"; "he'll win sure as shooting"; "they sure smell
           good"; "sure he'll come" [syn: {certainly}, {sure}, {for
           sure}, {for certain}, {sure enough}, {sure as shooting}]
