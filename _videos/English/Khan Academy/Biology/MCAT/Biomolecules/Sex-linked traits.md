---
version: 1
type: video
provider: YouTube
id: -ROhfKyxgCo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/MCAT/Biomolecules/Sex-linked%20traits.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3393cbcd-bfcf-4f73-9aaa-9a7694e0e5a6
updated: 1486069581
title: Sex-linked traits
categories:
    - Biomolecules
thumbnail_urls:
    - https://i3.ytimg.com/vi/-ROhfKyxgCo/default.jpg
    - https://i3.ytimg.com/vi/-ROhfKyxgCo/1.jpg
    - https://i3.ytimg.com/vi/-ROhfKyxgCo/2.jpg
    - https://i3.ytimg.com/vi/-ROhfKyxgCo/3.jpg
---
Chromosomal basis of sex. Sex-linked traits. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/test-prep/mcat/biomolecules/chromosomal-inheritance/v/punnett-square-fun?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

Missed the previous lesson? https://www.khanacademy.org/test-prep/mcat/biomolecules/chromosomal-inheritance/v/evidence-that-dna-is-genetic-material-2?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

MCAT on Khan Academy: Go ahead and practice some passage-based questions!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s MCAT channel: https://www.youtube.com/channel/UCDkK5wqSuwDlJ3_nl3rgdiQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
