---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enclave
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632441
title: enclave
categories:
    - Dictionary
---
enclave
     n : an enclosed territory that is culturally distinct from the
         foreign territory that surrounds it
