---
version: 1
type: video
provider: YouTube
id: UhIwMAhZpCo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Lifecycle%20of%20massive%20stars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f825cf02-d9a9-4104-939a-55f3a3dac880
updated: 1486069685
title: Lifecycle of massive stars
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/UhIwMAhZpCo/default.jpg
    - https://i3.ytimg.com/vi/UhIwMAhZpCo/1.jpg
    - https://i3.ytimg.com/vi/UhIwMAhZpCo/2.jpg
    - https://i3.ytimg.com/vi/UhIwMAhZpCo/3.jpg
---
