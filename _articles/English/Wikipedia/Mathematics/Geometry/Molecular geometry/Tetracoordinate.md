---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tetracoordinate
offline_file: ""
offline_thumbnail: ""
uuid: 80281fa7-c71e-465b-bffa-f182abedbe74
updated: 1484309282
title: Tetracoordinate
categories:
    - Molecular geometry
---
Tetracoordinate in coordination chemistry generally refers to four ligands or atomic attachments to a single metal centre. Tetracoordinate species can form tetrahedral, square planar or pyramidal geometries. This is usually dictated by lone electron pairs on the metal centre.
