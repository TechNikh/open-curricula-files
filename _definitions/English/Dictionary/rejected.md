---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rejected
offline_file: ""
offline_thumbnail: ""
uuid: 7c2ceb29-ac4e-4c73-b052-6b5c953c218d
updated: 1484310355
title: rejected
categories:
    - Dictionary
---
rejected
     adj 1: cast off as valueless [syn: {castaway(a)}]
     2: rebuffed (by a lover) without warning; "jilted at the altar"
        [syn: {jilted}, {spurned}]
     3: something or someone judged unacceptable; "rejected
        merchandise"
