---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sail
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470321
title: sail
categories:
    - Dictionary
---
sail
     n 1: a large piece of fabric (as canvas) by means of which wind
          is used to propel a sailing vessel [syn: {canvas}, {canvass},
           {sheet}]
     2: an ocean trip taken for pleasure [syn: {cruise}]
     v 1: traverse or travel by ship on (a body of water); "We sailed
          the Atlantic"; "He sailed the Pacific all alone"
     2: move with sweeping, effortless, gliding motions; "The diva
        swept into the room"; "Shreds of paper sailed through the
        air"; "The searchlights swept across the sky" [syn: {sweep}]
     3: travel in a boat propelled by wind; "I love sailing,
        especially on the open sea"
     4: travel by boat on a boat propelled by ...
