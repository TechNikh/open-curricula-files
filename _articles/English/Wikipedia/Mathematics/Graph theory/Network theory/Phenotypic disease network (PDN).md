---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Phenotypic_disease_network_(PDN)
offline_file: ""
offline_thumbnail: ""
uuid: 2941ac15-294c-46df-8af8-8baf06e1a6cb
updated: 1484309440
title: Phenotypic disease network (PDN)
tags:
    - History
    - Data and Methodology
    - Source data
    - Comorbidity correlation measures
    - Disease network dynamics
    - Disease progression
    - Connectedness and mortality
    - Demographic differences
categories:
    - Network theory
---
The first phenotypic disease network was constructed by Hidalgo et al. (2009)[1] to help understand the origins of many diseases and the links between them. Hidalgo et al. (2009) defined diseases as specific sets of phenotypes that affect one or several physiological systems, and compiled data on pairwise comorbidity correlations for more than 10,000 diseases reconstructed from over 30 million medical records. Hidalgo et al. (2009) presented their data in the form of a network with diseases as the nodes and comorbidity correlations as the links. Intuitively, the phenotypic disease network (PDN) can be seen as a map of the phenotypic space whose structure can contribute to the understanding ...
