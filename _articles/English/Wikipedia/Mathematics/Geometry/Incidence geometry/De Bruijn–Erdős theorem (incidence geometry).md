---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/De_Bruijn%E2%80%93Erd%C5%91s_theorem_(incidence_geometry)'
offline_file: ""
offline_thumbnail: ""
uuid: 902159f3-f2a2-426f-a847-c0b5d06db39b
updated: 1484309251
title: De Bruijn–Erdős theorem (incidence geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nearpencil.svg.png
tags:
    - Statement of the theorem
    - Euclidean proof
    - "J. H. Conway's Proof"
categories:
    - Incidence geometry
---
In incidence geometry, the De Bruijn–Erdős theorem, originally published by Nicolaas Govert de Bruijn and Paul Erdős (1948), states a lower bound on the number of lines determined by n points in a projective plane. By duality, this is also a bound on the number of intersection points determined by a configuration of lines.
