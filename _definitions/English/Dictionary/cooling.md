---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooling
offline_file: ""
offline_thumbnail: ""
uuid: 0f6570ad-bb18-4255-a2af-baa15aa4a771
updated: 1484310226
title: cooling
categories:
    - Dictionary
---
cooling
     n 1: the process of becoming cooler; a falling temperature [syn:
          {chilling}, {temperature reduction}]
     2: a mechanism for keeping something cool; "the cooling was
        overhead fans" [syn: {cooling system}]
