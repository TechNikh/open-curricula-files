---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decisively
offline_file: ""
offline_thumbnail: ""
uuid: d671c37c-4a0a-4a76-8de2-2af07ac2e54f
updated: 1484310577
title: decisively
categories:
    - Dictionary
---
decisively
     adv 1: with firmness; "`I will come along,' she said decisively"
            [syn: {resolutely}] [ant: {indecisively}]
     2: with finality; conclusively; "the voted settled the argument
        decisively" [ant: {indecisively}]
     3: in an indisputable degree; "the Fisher act of 1918
        decisively raised their status and pay"
