---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mgcl
offline_file: ""
offline_thumbnail: ""
uuid: 7f0e58de-cf66-4824-9817-36afe2ef694a
updated: 1484308386
title: Magnesium chloride
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sea_salt-e-dp_hg.svg.png
tags:
    - Structure, preparation, and general properties
    - Applications
    - Use in dust and erosion control
    - Use in ice control
    - Nutritional supplement
    - Use in hydrogen storage
    - Niche uses
    - Culinary use
    - Gardening and horticulture
    - Toxicology
    - Locomotive boiler problem
    - 'Notes & references'
categories:
    - Uncategorized
---
Magnesium chloride is the name for the chemical compound with the formula MgCl2 and its various hydrates MgCl2(H2O)x. These salts are typical ionic halides, being highly soluble in water. The hydrated magnesium chloride can be extracted from brine or sea water. In North America, magnesium chloride is produced primarily from Great Salt Lake brine. It is extracted in a similar process from the Dead Sea in the Jordan valley. Magnesium chloride, as the natural mineral bischofite, is also extracted (via solution mining) out of ancient seabeds; for example, the Zechstein seabed in northwest Europe. Some magnesium chloride is made from solar evaporation of seawater. Anhydrous magnesium chloride is ...
