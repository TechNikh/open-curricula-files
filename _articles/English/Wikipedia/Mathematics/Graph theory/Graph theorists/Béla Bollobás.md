---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%A9la_Bollob%C3%A1s'
offline_file: ""
offline_thumbnail: ""
uuid: 2a8c23a0-5a2a-46c2-9221-03ea6a21dfb2
updated: 1484309459
title: Béla Bollobás
tags:
    - Early life and education
    - Career
    - Awards and honours
    - Sporting career
    - Personal life
    - Selected works
categories:
    - Graph theorists
---
Béla Bollobás FRS (born 3 August 1943) is a Hungarian-born British mathematician who has worked in various areas of mathematics, including functional analysis, combinatorics, graph theory, and percolation. He was highly influenced by Paul Erdős since he was 14.[5][6][7][8]
