---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/farmland
offline_file: ""
offline_thumbnail: ""
uuid: b0cb4839-4fd2-4a90-9d7a-2514f6fa2693
updated: 1484310462
title: farmland
categories:
    - Dictionary
---
farmland
     n 1: a rural area where farming is practiced [syn: {farming area}]
     2: arable land that is worked by plowing and sowing and raising
        crops [syn: {cultivated land}, {plowland}, {ploughland}, {tilled
        land}, {tillage}, {tilth}]
