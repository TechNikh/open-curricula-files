---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/year-end
offline_file: ""
offline_thumbnail: ""
uuid: ad31e69b-654c-4f75-bd23-5ceb3b3a3a07
updated: 1484310515
title: year-end
categories:
    - Dictionary
---
year-end
     adj : taking place at the close of a fiscal year; "year-end audit"
     n : the end of a calendar year; "he had to unload the
         merchandise before the year-end"
