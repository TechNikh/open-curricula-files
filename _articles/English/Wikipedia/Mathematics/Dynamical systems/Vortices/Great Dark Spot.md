---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Great_Dark_Spot
offline_file: ""
offline_thumbnail: ""
uuid: d6a9d6b5-6415-4ca6-83f5-228f445efd64
updated: 1484308881
title: Great Dark Spot
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Neptune%2527s_Great_Dark_Spot.jpg'
tags:
    - characteristics
    - Disappearance
categories:
    - Vortices
---
The Great Dark Spot (also known as GDS-89)[1] was one of a series of dark spots on Neptune similar in appearance to Jupiter's Great Red Spot. GDS-89 ((G)reat (D)ark (S)pot - 19(89)) was the first Great Dark Spot on Neptune to be observed in 1989 by NASA's Voyager 2 spaceprobe. Like Jupiter's spot, Great Dark Spots are anticyclonic storms. However, their interiors are relatively cloud-free, and unlike Jupiter's spot, which has lasted for hundreds of years, their lifetimes appear to be shorter, forming and dissipating once every few years or so. Based on observations taken with Voyager 2 and since then with the Hubble Space Telescope, Neptune appears to spend somewhat more than half its time ...
