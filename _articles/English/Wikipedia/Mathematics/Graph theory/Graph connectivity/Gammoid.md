---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gammoid
offline_file: ""
offline_thumbnail: ""
uuid: c7d58017-5f89-4fbe-906e-f9faa6503037
updated: 1484309400
title: Gammoid
tags:
    - Definition
    - Example
    - "Menger's theorem and gammoid rank"
    - Relation to transversal matroids
    - Representability
categories:
    - Graph connectivity
---
In matroid theory, a field within mathematics, a gammoid is a certain kind of matroid, describing sets of vertices that can be reached by vertex-disjoint paths in a directed graph.
