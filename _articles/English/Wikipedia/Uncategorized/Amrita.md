---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amrita
offline_file: ""
offline_thumbnail: ""
uuid: c0f00f8d-bc4d-4a62-9dfd-c57e67ec844c
updated: 1484308333
title: Amrita
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mohini_with_amrit.jpg
tags:
    - Hinduism
    - Sikhism
    - Theravada Buddhism
    - Mahayana Buddhism
    - Sources
categories:
    - Uncategorized
---
Amrit (Sanskrit, IAST: amṛta) or Amata (Pali) is a word that literally means "immortality" and is often referred to in texts as nectar. Amṛta is etymologically related to the Greek ambrosia[1] and carries the same meaning.[2] The word's earliest occurrence is in the Rigveda, where it is one of several synonyms for soma, the drink which confers immortality upon the gods.
