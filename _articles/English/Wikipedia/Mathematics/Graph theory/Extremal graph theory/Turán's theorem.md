---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tur%C3%A1n%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 03031fff-7528-4ed6-9933-4bbc432e4617
updated: 1484309409
title: "Turán's theorem"
tags:
    - Formal statement
    - Proof
    - "Mantel's theorem"
categories:
    - Extremal graph theory
---
An n-vertex graph that does not contain any (r + 1)-vertex clique may be formed by partitioning the set of vertices into r parts of equal or nearly equal size, and connecting two vertices by an edge whenever they belong to two different parts. We call the resulting graph the Turán graph T(n, r). Turán's theorem states that the Turán graph has the largest number of edges among all Kr+1-free n-vertex graphs.
