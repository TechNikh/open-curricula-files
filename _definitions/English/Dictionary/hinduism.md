---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hinduism
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484638621
title: hinduism
categories:
    - Dictionary
---
Hinduism
     n 1: the predominant religion of India; characterized by a caste
          system and belief in reincarnation [syn: {Hindooism}]
     2: a body of religious and philosophical beliefs and cultural
        practices native to India and characterized by a belief in
        reincarnation and a supreme beingof many forms and
        natures, by the view that opposing theories are aspects of
        one eternal truth, and by a desire for liberation from
        earthly evils [syn: {Hindooism}]
