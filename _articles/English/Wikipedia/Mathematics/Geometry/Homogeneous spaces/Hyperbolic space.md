---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_space
offline_file: ""
offline_thumbnail: ""
uuid: 6390e2b5-d5c6-4c41-a22e-ac5c88ee8981
updated: 1484309247
title: Hyperbolic space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hyperbolic_orthogonal_dodecahedral_honeycomb.png
tags:
    - Formal definition
    - Models of hyperbolic space
    - The hyperboloid model
    - The Klein model
    - The Poincaré ball model
    - The Poincaré half space model
    - Hyperbolic manifolds
    - Riemann surfaces
categories:
    - Homogeneous spaces
---
In mathematics, hyperbolic space is a homogeneous space that has a constant negative curvature, where in this case the curvature is the sectional curvature. It is hyperbolic geometry in more than 2 dimensions, and is distinguished from Euclidean spaces with zero curvature that define the Euclidean geometry, and elliptic geometry that have a constant positive curvature.
