---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anatomy
offline_file: ""
offline_thumbnail: ""
uuid: 82ebdf8c-9f5c-4c3a-8aea-f45e120308e5
updated: 1484310283
title: anatomy
categories:
    - Dictionary
---
anatomy
     n 1: the branch of morphology that deals with the structure of
          animals [syn: {general anatomy}]
     2: alternative names for the body of a human being; "Leonardo
        studied the human body"; "he has a strong physique"; "the
        spirit is willing but the flesh is weak" [syn: {human body},
         {physical body}, {material body}, {soma}, {build}, {figure},
         {physique}, {shape}, {bod}, {chassis}, {frame}, {form}, {flesh}]
     3: a detailed analysis; "he studied the anatomy of crimes"
