---
version: 1
type: video
provider: YouTube
id: U-k5N1WPk4g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/Domain%20of%20a%20function.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9bd0b353-29ef-4c19-bef8-1145bc5c22bc
updated: 1486069573
title: Domain of a function
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/U-k5N1WPk4g/default.jpg
    - https://i3.ytimg.com/vi/U-k5N1WPk4g/1.jpg
    - https://i3.ytimg.com/vi/U-k5N1WPk4g/2.jpg
    - https://i3.ytimg.com/vi/U-k5N1WPk4g/3.jpg
---
Figuring out the domain of a function

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/algebra2/functions_and_graphs/domain_range/e/domain_and_range_0.5?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Watch the next lesson: https://www.khanacademy.org/math/algebra2/functions_and_graphs/domain_range/v/domain-and-range-of-a-relation?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/functions_and_graphs/function_expressions/v/modeling-with-combined-functions?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
