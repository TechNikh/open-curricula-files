---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Five_color_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 1db1f7a4-f841-4d75-8fee-c950e06460fd
updated: 1484309384
title: Five color theorem
tags:
    - Outline of the proof by contradiction
    - Linear time five-coloring algorithm
categories:
    - Graph coloring
---
The five color theorem is a result from graph theory that given a plane separated into regions, such as a political map of the counties of a state, the regions may be colored using no more than five colors in such a way that no two adjacent regions receive the same color.
