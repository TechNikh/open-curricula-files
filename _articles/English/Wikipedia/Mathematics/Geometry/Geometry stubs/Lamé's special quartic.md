---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lam%C3%A9%27s_special_quartic'
offline_file: ""
offline_thumbnail: ""
uuid: 57776da1-6ace-4cbc-96c5-d30c3b6607b4
updated: 1484309363
title: "Lamé's special quartic"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Superellipse_chamfered_square.svg.png
categories:
    - Geometry stubs
---
where 
  
    
      
        r
        >
        0
      
    
    {\displaystyle r>0}
  
.[1] It looks like a rounded square with "sides" of length 
  
    
      
        2
        r
      
    
    {\displaystyle 2r}
  
 and centered on the origin. This curve is a squircle centered on the origin, and it is a special case of a super ellipse.[2]
