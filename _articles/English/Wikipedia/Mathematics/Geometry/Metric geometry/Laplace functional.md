---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laplace_functional
offline_file: ""
offline_thumbnail: ""
uuid: a2813975-7809-42b3-8de0-cb815950c3b7
updated: 1484309269
title: Laplace functional
tags:
    - Definition for point processes
    - Applications
    - Definition for probability measures
    - Applications
    - Notes
categories:
    - Metric geometry
---
In probability theory, a Laplace functional refers to one of two possible mathematical functions of functions or, more precisely, functionals that serve as mathematical tools for studying either point processes or concentration of measure properties of metric spaces. One type of Laplace functional,[1][2] also known as a characteristic functional[a] is defined in relation to a point process, which can be interpreted as random counting measures, and has applications in characterizing and deriving results on point processes.[5] Its is definition is analogous to a characteristic function for a random variable.
