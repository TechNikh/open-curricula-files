---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_rule
offline_file: ""
offline_thumbnail: ""
uuid: 9b403ee9-e68d-4697-a98f-226819581aed
updated: 1484309499
title: Structural rule
categories:
    - Proof theory
---
In proof theory, a structural rule is an inference rule that does not refer to any logical connective, but instead operates on the judgment or sequents directly. Structural rules often mimic intended meta-theoretic properties of the logic. Logics that deny one or more of the structural rules are classified as substructural logics.
