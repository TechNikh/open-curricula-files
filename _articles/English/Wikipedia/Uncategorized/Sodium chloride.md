---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nacl
offline_file: ""
offline_thumbnail: ""
uuid: 84759188-244d-4645-b907-ebc772c52bf8
updated: 1484308317
title: Sodium chloride
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/270px-WatNaCl.png
tags:
    - chemistry
    - Solid sodium chloride
    - Aqueous solutions
    - Unexpected stable stoichiometric variants
    - Occurrence
    - Production
    - Uses
    - Chemicals production
    - Chlor-alkali industry
    - Soda ash industry
    - Standard
    - Miscellaneous industrial uses
    - Water softening
    - Road salt
    - Environmental effects
    - Food industry, medicine and agriculture
    - Firefighting
    - Cleanser
    - Optical usage
    - Biological functions
categories:
    - Uncategorized
---
Sodium chloride /ˌsoʊdiəm ˈklɔːraɪd/,[2] also known as salt or halite, is an ionic compound with the chemical formula NaCl, representing a 1:1 ratio of sodium and chloride ions. Sodium chloride is the salt most responsible for the salinity of seawater and of the extracellular fluid of many multicellular organisms. In the form of edible or table salt it is commonly used as a condiment and food preservative. Large quantities of sodium chloride are used in many industrial processes, and it is a major source of sodium and chlorine compounds used as feedstocks for further chemical syntheses. A second major application of sodium chloride is de-icing of roadways in sub-freezing weather.
