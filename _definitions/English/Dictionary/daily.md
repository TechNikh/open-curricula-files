---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daily
offline_file: ""
offline_thumbnail: ""
uuid: 5f9ae0c4-6c6d-4d4a-bb28-481327437b23
updated: 1484310230
title: daily
categories:
    - Dictionary
---
daily
     adj 1: occurring or done each day; "a daily record"; "day-by-day
            labors of thousands of men and women"- H.S.Truman;
            "her day-after-day behavior"; "an every day
            occurrence" [syn: {day-to-day}, {day-after-day}, {every
            day}]
     2: measured by the day or happening every day; "a daily
        newspaper"; "daily chores"; "average daily wage"; "daily
        quota"
     n : a newspaper that is published every day
     adv 1: without missing a day; "he stops by daily" [syn: {every day},
             {each day}]
     2: gradually and progressively; "his health weakened day by
        day" [syn: {day by day}]
