---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parent
offline_file: ""
offline_thumbnail: ""
uuid: 12d768e5-463c-476c-9326-250d90f84a3d
updated: 1484310309
title: parent
categories:
    - Dictionary
---
parent
     n : a father or mother; one who begets or one who gives birth to
         or nurtures and raises a child; a relative who plays the
         role of guardian [ant: {child}]
     v : bring up; "raise a family"; "bring up children" [syn: {rear},
          {raise}, {bring up}, {nurture}]
