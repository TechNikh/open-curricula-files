---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regulating
offline_file: ""
offline_thumbnail: ""
uuid: 30df9d9e-7274-4250-aaaf-986632998918
updated: 1484310525
title: regulating
categories:
    - Dictionary
---
regulating
     n : the act of controlling or directing according to rule;
         "fiscal regulations are in the hands of politicians"
         [syn: {regulation}]
