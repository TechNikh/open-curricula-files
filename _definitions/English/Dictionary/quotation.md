---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quotation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484560861
title: quotation
categories:
    - Dictionary
---
quotation
     n 1: a short note recognizing a source of information or of a
          quoted passage; "the student's essay failed to list
          several important citations"; "the acknowledgments are
          usually printed at the front of a book"; "the article
          includes mention of similar clinical cases" [syn: {citation},
           {acknowledgment}, {credit}, {reference}, {mention}]
     2: a passage or expression that is quoted or cited [syn: {quote},
         {citation}]
     3: a statement of the current market price of a security or
        commodity
     4: the practice of quoting from books or plays etc.; "since he
        lacks originality he must rely on quotation"
