---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bus
offline_file: ""
offline_thumbnail: ""
uuid: 18575287-18e3-40f6-8265-565cc52939b0
updated: 1484310240
title: bus
categories:
    - Dictionary
---
bus
     n 1: a vehicle carrying many passengers; used for public
          transport; "he always rode the bus to work" [syn: {autobus},
           {coach}, {charabanc}, {double-decker}, {jitney}, {motorbus},
           {motorcoach}, {omnibus}]
     2: the topology of a network whose components are connected by
        a busbar [syn: {bus topology}]
     3: an electrical conductor that makes a common connection
        between several circuits; "the busbar in this computer can
        transmit data either way between any two components of the
        system" [syn: {busbar}]
     4: a car that is old and unreliable; "the fenders had fallen
        off that old bus" [syn: {jalopy}, {heap}]
   ...
