---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recorder
offline_file: ""
offline_thumbnail: ""
uuid: ef42c60f-4452-4752-ad4d-cb2fe0e6d7bd
updated: 1484310369
title: recorder
categories:
    - Dictionary
---
recorder
     n 1: equipment for making records [syn: {recording equipment}, {recording
          machine}]
     2: someone responsible for keeping records [syn: {registrar}, {record-keeper}]
     3: a barrister or solicitor who serves as part-time judge in
        towns or boroughs
     4: a woodwind with a vertical pipe and 8 finger holes and a
        whistle mouthpiece [syn: {fipple flute}, {fipple pipe}, {vertical
        flute}]
