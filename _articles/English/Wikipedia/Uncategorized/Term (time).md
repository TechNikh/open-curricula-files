---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Short-term
offline_file: ""
offline_thumbnail: ""
uuid: 5d637b38-e37c-490c-81ef-eac61f2cd7ba
updated: 1484308458
title: Term (time)
categories:
    - Uncategorized
---
A term is a period of duration, time or occurrence, in relation to an event. To differentiate an interval or duration, common phrases are used to distinguish the observance of length are near-term or short-term, medium-term or mid-term and long-term.
