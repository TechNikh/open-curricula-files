---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_cobordism
offline_file: ""
offline_thumbnail: ""
uuid: 7c39731f-4ae6-41e4-8511-0c16dccb6be4
updated: 1484309134
title: Algebraic cobordism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_4.png
categories:
    - Algebraic geometry
---
In mathematics, algebraic cobordism is an analogue of complex cobordism for smooth quasi-projective schemes over a field. It was introduced by Marc Levine and Fabien Morel (2001, 2001b).
