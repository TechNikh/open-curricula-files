---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnesite
offline_file: ""
offline_thumbnail: ""
uuid: a79d5e24-392f-4dd3-867d-6765c297af49
updated: 1484310409
title: magnesite
categories:
    - Dictionary
---
magnesite
     n : a black magnetic mineral consisting of magnesium carbonate;
         a source of magnesium
