---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Timothy_Browning
offline_file: ""
offline_thumbnail: ""
uuid: 4d9b6eef-fd0d-4a44-bd12-85cdf7ba440f
updated: 1484309073
title: Timothy Browning
categories:
    - Geometers
---
Timothy Browning is a mathematician working in number theory, examining the interface of analytic number theory and Diophantine geometry.[2] Browning is currently a Professor of number theory at the University of Bristol.[3]
