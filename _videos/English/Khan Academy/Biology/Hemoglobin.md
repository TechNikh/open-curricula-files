---
version: 1
type: video
provider: YouTube
id: LWtXthfG9_M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Hemoglobin.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8e21bd1f-73dc-4ce7-9fcf-d978c1e368d4
updated: 1486069583
title: Hemoglobin
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/LWtXthfG9_M/default.jpg
    - https://i3.ytimg.com/vi/LWtXthfG9_M/1.jpg
    - https://i3.ytimg.com/vi/LWtXthfG9_M/2.jpg
    - https://i3.ytimg.com/vi/LWtXthfG9_M/3.jpg
---
Hemoglobin and its role in the circulatory system
More free lessons at: http://www.khanacademy.org/video?v=LWtXthfG9_M
