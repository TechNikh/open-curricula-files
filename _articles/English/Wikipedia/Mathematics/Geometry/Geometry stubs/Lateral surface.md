---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lateral_surface
offline_file: ""
offline_thumbnail: ""
uuid: b58bc878-a0c8-4ed4-9bbf-eeaecd15da55
updated: 1484309367
title: Lateral surface
categories:
    - Geometry stubs
---
For a cube, the lateral surface area would be the area of four sides. Consider the edge of the cube as 
  
    
      
        a
      
    
    {\displaystyle a}
  
. The area of one square face Aface = a ⋅ a = a2. Thus the lateral surface of a cube will be the area of four faces: a ⋅ a ⋅ 4 = 4a2. The lateral surface can also be calculated by multiplying the perimeter of the base by the height of the prism.[1]
