---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/randomly
offline_file: ""
offline_thumbnail: ""
uuid: e052f006-cd1a-472c-a838-392dc3417873
updated: 1484310299
title: randomly
categories:
    - Dictionary
---
randomly
     adv : in a random manner; "the houses were randomly scattered";
           "bullets were fired into the crowd at random" [syn: {indiscriminately},
            {haphazardly}, {willy-nilly}, {arbitrarily}, {at
           random}, {every which way}]
