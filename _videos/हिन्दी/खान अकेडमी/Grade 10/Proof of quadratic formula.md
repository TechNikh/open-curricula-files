---
version: 1
type: video
provider: YouTube
id: MtQVJPNSPS4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d932fab4-ad47-4d2c-8440-3b18e567c34b
updated: 1486069621
title: Proof of quadratic formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/MtQVJPNSPS4/default.jpg
    - https://i3.ytimg.com/vi/MtQVJPNSPS4/1.jpg
    - https://i3.ytimg.com/vi/MtQVJPNSPS4/2.jpg
    - https://i3.ytimg.com/vi/MtQVJPNSPS4/3.jpg
---
