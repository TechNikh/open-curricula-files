---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bitten
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411461
title: bitten
categories:
    - Dictionary
---
bite
     n 1: a wound resulting from biting by an animal or a person
     2: a small amount of solid food; a mouthful; "all they had left
        was a bit of bread" [syn: {morsel}, {bit}]
     3: a painful wound caused by the thrust of an insect's stinger
        into skin [syn: {sting}, {insect bite}]
     4: a light informal meal [syn: {collation}, {snack}]
     5: (angling) an instance of a fish taking the bait; "after
        fishing for an hour he still had not had a bite"
     6: wit having a sharp and caustic quality; "he commented with
        typical pungency"; "the bite of satire" [syn: {pungency}]
     7: a strong odor or taste property; "the pungency of mustard";
        "the ...
