---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wiki
offline_file: ""
offline_thumbnail: ""
uuid: 0197d2e0-42b9-4743-8b5f-fecd8606837e
updated: 1484308823
title: Wiki
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ward_Cunningham_-_Commons-1.jpg
tags:
    - characteristics
    - Editing
    - Navigation
    - Linking and creating pages
    - Searching
    - History
    - Alternative definitions
    - Implementations
    - Trust and security
    - Controlling changes
    - Trustworthiness and reliability of content
    - Security
    - Potential malware vector
    - Communities
    - Applications
    - City wikis
    - WikiNodes
    - Participants
    - Growth factors
    - Conferences
    - Rules
    - Legal environment
    - Notes
categories:
    - Self-organization
---
A wiki (i/ˈwɪki/ WIK-ee) is a website that provides collaborative modification of its content and structure directly from the web browser. In a typical wiki, text is written using a simplified markup language (known as "wiki markup"), and often edited with the help of a rich-text editor.[1] A wiki is run using wiki software, otherwise known as a wiki engine. There are dozens of different wiki engines in use, both standalone and part of other software, such as bug tracking systems. Some wiki engines are open source, whereas others are proprietary. Some permit control over different functions (levels of access); for example, editing rights may permit changing, adding or removing material. ...
