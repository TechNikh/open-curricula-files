---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C3%89tale_topology'
offline_file: ""
offline_thumbnail: ""
uuid: 21b67095-8c4a-4830-a5cd-a0f9b7401fbc
updated: 1484309142
title: Étale topology
tags:
    - Definitions
    - Local rings in the étale topology
categories:
    - Algebraic geometry
---
In algebraic geometry, the étale topology is a Grothendieck topology on the category of schemes which has properties similar to the Euclidean topology, but unlike the Euclidean topology, it is also defined in positive characteristic. The étale topology was originally introduced by Grothendieck to define étale cohomology, and this is still the étale topology's most well-known use.
