---
version: 1
type: video
provider: YouTube
id: ozeYaikI11g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 35a5a358-c519-4e01-ad30-b3f2bd1b384c
updated: 1486069657
title: 'Transmission Lines - Signal Transmission and Reflection'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ozeYaikI11g/default.jpg
    - https://i3.ytimg.com/vi/ozeYaikI11g/1.jpg
    - https://i3.ytimg.com/vi/ozeYaikI11g/2.jpg
    - https://i3.ytimg.com/vi/ozeYaikI11g/3.jpg
---
