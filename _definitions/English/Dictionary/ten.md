---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ten
offline_file: ""
offline_thumbnail: ""
uuid: 70edb1d0-cab5-4c22-80ba-b828c5d2b607
updated: 1484310236
title: ten
categories:
    - Dictionary
---
ten
     adj : being one more than nine [syn: {10}, {x}]
     n : the cardinal number that is the sum of nine and one; the
         base of the decimal system [syn: {10}, {X}, {tenner}, {decade}]
