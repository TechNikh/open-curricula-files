---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deserted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484482981
title: deserted
categories:
    - Dictionary
---
deserted
     adj 1: left desolate or empty; "an abandoned child"; "their
            deserted wives and children"; "an abandoned shack";
            "deserted villages" [syn: {abandoned}]
     2: remote from civilization; "the victim was lured to a
        deserted spot"
