---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Basicity
offline_file: ""
offline_thumbnail: ""
uuid: deff8291-0fd8-4684-84cd-e6f1ddc985be
updated: 1484308312
title: Base (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Decorative_Soaps.jpg
tags:
    - Properties
    - Reactions between bases and water
    - Neutralization of acids
    - Alkalinity of non-hydroxides
    - Strong bases
    - Superbases
    - Neutral bases
    - Bases as catalysts
    - Solid bases
    - Weak bases
    - Uses of bases
    - Acidity of bases
    - Monoacidic bases
    - Diacidic bases
    - Triacidic bases
    - Etymology of the term
categories:
    - Uncategorized
---
In chemistry, bases are substances that, in aqueous solution, are slippery to the touch, taste astringent, change the color of indicators (e.g., turn red litmus paper blue), react with acids to form salts, promote certain chemical reactions (base catalysis), accept protons from any proton donor, and/or contain completely or partially displaceable OH− ions. Examples of bases are the hydroxides of the alkali metals and alkaline earth metals (NaOH, Ca(OH)2, etc.).
