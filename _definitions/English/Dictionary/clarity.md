---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clarity
offline_file: ""
offline_thumbnail: ""
uuid: b8974a80-71cd-4c2a-8c43-2b00e0f8904e
updated: 1484310210
title: clarity
categories:
    - Dictionary
---
clarity
     n 1: free from obscurity and easy to understand; the
          comprehensibility of clear expression [syn: {lucidity},
          {pellucidity}, {clearness}, {limpidity}] [ant: {unclearness},
           {obscureness}]
     2: the quality of clear water; "when she awoke the clarity was
        back in her eyes" [syn: {clearness}, {uncloudedness}]
        [ant: {opacity}]
