---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regional
offline_file: ""
offline_thumbnail: ""
uuid: ed100531-3cd2-4c89-8925-a98891ae2e97
updated: 1484310486
title: regional
categories:
    - Dictionary
---
regional
     adj 1: characteristic of a region; "regional flora"
     2: related or limited to a particular region; "a regional
        dialect"
