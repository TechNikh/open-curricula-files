---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Single_parent
offline_file: ""
offline_thumbnail: ""
uuid: 3207c886-7241-43b4-9eb8-0d620abd580a
updated: 1484308365
title: Single parent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Statue_of_mother_Yasukuni_112135011_bd2aaaa5d3_o.jpg
tags:
    - History
    - Demographics
    - Debates
    - Primary caregiver
    - Mother
    - Father
    - Mental health of single mothers
    - Types of single parenting
    - Widowed parents
    - Divorced parents
    - Divorce statistics
    - Children and divorce
    - Unintended pregnancy
    - Choice
    - Xin con
    - Single parent adoption
    - History of single parent adoptions
    - Considerations
    - Single parent adoption in the United States
    - Single parents in Australia
    - Statistics
    - Single parent adoption
    - Payments
    - Living arrangements for single parents
categories:
    - Uncategorized
---
A single parent is an uncoupled individual who shoulders most or all of the day-to-day responsibilities for raising a child or children. A mother is more often the primary caregiver in a single-parent family structure that has arisen due to death of the partner, divorce or unplanned pregnancy.
