---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Path_loss
offline_file: ""
offline_thumbnail: ""
uuid: 14eddd6b-d4ca-4eb9-9170-67008ffce442
updated: 1484308893
title: Path loss
tags:
    - Causes
    - Loss exponent
    - Radio engineer formula
    - Prediction
    - Examples
categories:
    - Waves
---
Path loss (or path attenuation) is the reduction in power density (attenuation) of an electromagnetic wave as it propagates through space. Path loss is a major component in the analysis and design of the link budget of a telecommunication system.
