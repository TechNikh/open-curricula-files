---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiferroics
offline_file: ""
offline_thumbnail: ""
uuid: 85898a51-97a0-4516-8e0c-03302b0c53d4
updated: 1484308413
title: Multiferroics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Multiferroics_history_use_of_terms_magnetoelectric_and_multiferroic.png
tags:
    - History
    - Symmetry
    - Classification
    - Mechanisms for ferroelectricity in multiferroics
    - Lone pair multiferroics
    - Improper geometric ferroelectricity
    - Charge ordering
    - Magnetically driven ferroelectricity
    - Optical properties
    - List of materials
    - Domains
    - Properties of multiferroic domains
    - Properties of multiferroic domain walls
    - Magnetoelectric effect
    - Strain driven magnetoelectric heterostructured effect
    - Flexomagnetoelectric effect
    - Synthesis
    - Dynamics
    - Applications
    - Reviews on Multiferroics
categories:
    - Uncategorized
---
in the same phase.[1][2] While ferroelectric ferroelastics (with their associated piezoelectric and electrostrictive coupling) and ferromagnetic ferroelastics (with piezomagnetic and magnetomechanical coupling) are formally multiferroics, these days the term is usually used to describe the magnetoelectric multiferroics that are simultaneously ferromagnetic and ferroelectric.[2] Sometimes the definition is expanded to include non-primary order parameters, such as antiferromagnetism or ferrimagnetism. In addition other types of primary order, such as ferroic arrangements of magneotelectric multipoles[3] of which ferrotoroidicity[4] is an example, have also been recently proposed.
