---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semiliquid
offline_file: ""
offline_thumbnail: ""
uuid: 4ed09f57-0178-4ca1-a7f2-804a0d8d30fa
updated: 1484308351
title: Quasi-solid
categories:
    - Uncategorized
---
Quasisolid or semisolid is the physical term for something that lies along the boundary between a solid and a liquid. While similar to a solid in some respects, in that semisolids can support their own weight and hold their shapes, a quasisolid also shares some properties of liquids, such as conforming in shape to something applying pressure to it and the ability to flow under pressure. The words quasisolid, semisolid, and semiliquid may be used interchangeably.
