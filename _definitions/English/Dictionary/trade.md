---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trade
offline_file: ""
offline_thumbnail: ""
uuid: ca3ea3f6-0266-42f4-ab97-e8077d9f51cf
updated: 1484310433
title: trade
categories:
    - Dictionary
---
trade
     adj : relating to or used in or intended for trade or commerce; "a
           trade fair"; "trade journals"; "trade goods" [syn: {trade(a)}]
     n 1: the commercial exchange (buying and selling on domestic or
          international markets) of goods and services; "Venice
          was an important center of trade with the East"; "they
          are accused of conspiring to constrain trade"
     2: people who perform a particular kind of skilled work; "he
        represented the craft of brewers"; "as they say in the
        trade" [syn: {craft}]
     3: an equal exchange; "we had no money so we had to live by
        barter" [syn: {barter}, {swap}, {swop}]
     4: the skilled ...
