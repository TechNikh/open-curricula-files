---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/empathize
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651161
title: empathize
categories:
    - Dictionary
---
empathize
     v : be understanding of; "You don't need to explain--I
         understand!" [syn: {sympathize}, {sympathise}, {empathise},
          {understand}]
