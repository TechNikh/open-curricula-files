---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tight_span
offline_file: ""
offline_thumbnail: ""
uuid: aa2329c9-02d6-4895-a6ea-282eb3fadcf5
updated: 1484309273
title: Tight span
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Orthogonal-convex-hull.svg.png
tags:
    - Definition
    - Example
    - Applications
    - Notes
categories:
    - Metric geometry
---
In metric geometry, the metric envelope or tight span of a metric space M is an injective metric space into which M can be embedded. In some sense it consists of all points "between" the points of M, analogous to the convex hull of a point set in a Euclidean space. The tight span is also sometimes known as the injective envelope or hyperconvex hull of M. It has also been called the injective hull, but should not be confused with the injective hull of a module in algebra, a concept with a similar description relative to the category of R-modules rather than metric spaces.
