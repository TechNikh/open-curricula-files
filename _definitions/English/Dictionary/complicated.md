---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complicated
offline_file: ""
offline_thumbnail: ""
uuid: 033fff8d-4b9c-45a6-a97b-ca86012c5285
updated: 1484310363
title: complicated
categories:
    - Dictionary
---
complicated
     adj : difficult to analyze or understand; "a complicated problem";
           "complicated Middle East politics"
