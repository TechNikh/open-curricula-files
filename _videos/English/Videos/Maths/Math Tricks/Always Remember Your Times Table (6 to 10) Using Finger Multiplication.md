---
version: 1
type: video
provider: YouTube
id: x2Nr-f02AUY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 66d1a69a-4cc9-4c79-8e4d-aa9a587cbc6c
updated: 1486069623
title: >
    Always Remember Your Times Table (6 to 10) Using Finger
    Multiplication
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/x2Nr-f02AUY/default.jpg
    - https://i3.ytimg.com/vi/x2Nr-f02AUY/1.jpg
    - https://i3.ytimg.com/vi/x2Nr-f02AUY/2.jpg
    - https://i3.ytimg.com/vi/x2Nr-f02AUY/3.jpg
---
