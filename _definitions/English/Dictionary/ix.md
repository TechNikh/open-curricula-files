---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ix
offline_file: ""
offline_thumbnail: ""
uuid: fb8e8651-d86b-4927-831f-3a427cb70e67
updated: 1484310315
title: ix
categories:
    - Dictionary
---
ix
     adj : denoting a quantity consisting of one more than eight and
           one less than ten [syn: {nine}, {9}]
     n : the cardinal number that is the sum of eight and one [syn: {nine},
          {9}, {niner}, {Nina from Carolina}, {ennead}]
