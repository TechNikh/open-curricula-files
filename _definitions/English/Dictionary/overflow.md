---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overflow
offline_file: ""
offline_thumbnail: ""
uuid: 55f0b65b-4b23-4386-9c2d-4cc2e298a5aa
updated: 1484310264
title: overflow
categories:
    - Dictionary
---
overflow
     n 1: a large flow [syn: {flood}, {outpouring}]
     2: the occurrence of surplus liquid (as water) exceeding the
        limit or capacity [syn: {runoff}, {overspill}]
     v 1: flow or run over (a limit or brim) [syn: {overrun}, {well
          over}, {run over}, {brim over}]
     2: overflow with a certain feeling; "The children bubbled over
        with joy"; "My boss was bubbling over with anger" [syn: {bubble
        over}, {spill over}]
     [also: {overflown}]
