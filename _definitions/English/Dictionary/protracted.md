---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protracted
offline_file: ""
offline_thumbnail: ""
uuid: 21c80a1e-ff61-4268-af49-ac3476fe9410
updated: 1484310557
title: protracted
categories:
    - Dictionary
---
protracted
     adj : relatively long in duration; tediously protracted; "a
           drawn-out argument"; "an extended discussion"; "a
           lengthy visit from her mother-in-law"; "a prolonged and
           bitter struggle"; "protracted negotiations" [syn: {drawn-out},
            {extended}, {lengthy}, {prolonged}]
