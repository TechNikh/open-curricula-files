---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/panic
offline_file: ""
offline_thumbnail: ""
uuid: eabac493-e9e9-41fc-b85e-15974c0e8b3c
updated: 1484310172
title: panic
categories:
    - Dictionary
---
panic
     n 1: an overwhelming feeling of fear and anxiety [syn: {terror}]
     2: sudden mass fear and anxiety over anticipated events; "panic
        in the stock market"; "a war scare"; "a bomb scare led
        them to evacuate the building" [syn: {scare}]
     v 1: be overcome by a sudden fear; "The students panicked when
          told that final exams were less than a week away"
     2: cause sudden fear in or fill with sudden panic; "The mere
        thought of an isolation cell panicked the prisoners"
     [also: {panicking}, {panicked}]
