---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/externally
offline_file: ""
offline_thumbnail: ""
uuid: ea0c2288-228f-4cf1-8fd9-9104cd6d6c16
updated: 1484310303
title: externally
categories:
    - Dictionary
---
externally
     adv 1: on or from the outside; "the candidate needs to be
            externally evaluated" [ant: {internally}]
     2: with respect to the outside; "outwardly, the figure is
        smooth" [syn: {outwardly}]
