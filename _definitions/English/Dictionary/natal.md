---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/natal
offline_file: ""
offline_thumbnail: ""
uuid: 33cb72da-c84c-4e4b-ad93-2be490c07c41
updated: 1484310471
title: natal
categories:
    - Dictionary
---
natal
     adj 1: relating to or accompanying birth; "natal injuries"; "natal
            day"; "natal influences"
     2: of or relating to the buttocks
     n 1: a region of eastern South Africa on the Indian Ocean
     2: a port city in northeastern Brazil
