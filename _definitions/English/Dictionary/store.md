---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/store
offline_file: ""
offline_thumbnail: ""
uuid: 132219c3-05aa-4610-8cd3-caf8c7ebfd66
updated: 1484310330
title: store
categories:
    - Dictionary
---
store
     n 1: a mercantile establishment for the retail sale of goods or
          services; "he bought it at a shop on Cape Cod" [syn: {shop}]
     2: a supply of something available for future use; "he brought
        back a large store of Cuban cigars" [syn: {stock}, {fund}]
     3: an electronic memory device; "a memory and the CPU form the
        central part of a computer to which peripherals are
        attached" [syn: {memory}, {computer memory}, {storage}, {computer
        storage}, {memory board}]
     4: a depository for goods; "storehouses were built close to the
        docks" [syn: {storehouse}, {depot}, {entrepot}, {storage}]
     v 1: keep or lay aside for future use; ...
