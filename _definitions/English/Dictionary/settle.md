---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/settle
offline_file: ""
offline_thumbnail: ""
uuid: 863414ba-39e8-468b-9551-de740dffeab9
updated: 1484310407
title: settle
categories:
    - Dictionary
---
settle
     n : a long wooden bench with a back [syn: {settee}]
     v 1: settle into a position, usually on a surface or ground;
          "dust settled on the roofs" [syn: {settle down}]
     2: bring to an end; settle conclusively; "The case was
        decided"; "The judge decided the case in favor of the
        plaintiff"; "The father adjudicated when the sons were
        quarreling over their inheritance" [syn: {decide}, {resolve},
         {adjudicate}]
     3: settle conclusively; come to terms; "We finally settled the
        argument" [syn: {square off}, {square up}, {determine}]
     4: take up residence and become established; "The immigrants
        settled in the Midwest" ...
