---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_rewriting
offline_file: ""
offline_thumbnail: ""
uuid: c8af61ec-b937-425a-841f-ac867befd58d
updated: 1484309454
title: Graph rewriting
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GraphRewriteExample.PNG
tags:
    - Graph rewriting approaches
    - Algebraic approach
    - Determinate graph rewriting
    - Term graph rewriting
    - Classes of graph grammar and graph rewriting system
    - Implementations and applications
    - Notes
categories:
    - Graph rewriting
---
In computer science, graph transformation, or graph rewriting, concerns the technique of creating a new graph out of an original graph algorithmically. It has numerous applications, ranging from software engineering (software construction and also software verification) to layout algorithms and picture generation.
