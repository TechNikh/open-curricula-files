---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parum
offline_file: ""
offline_thumbnail: ""
uuid: a589b864-17b2-438f-84e3-8c5fa7dc7f45
updated: 1484308405
title: Parum
categories:
    - Uncategorized
---
Parum is a genus of moths in the Sphingidae family, containing only one species, Parum colligata, which is found from Korea and Japan south throughout eastern and central China and Taiwan to Vietnam, northern Thailand and north-eastern Myanmar.[2]
