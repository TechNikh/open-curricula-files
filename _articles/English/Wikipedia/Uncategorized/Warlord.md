---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Warlordism
offline_file: ""
offline_thumbnail: ""
uuid: 7d6710ea-5e2a-4686-a219-49895fd63a55
updated: 1484308485
title: Warlord
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Atta_Muhammad_Nur_of_Afghanistan_in_August_2010.jpg
tags:
    - Historical origins and etymology
    - Conceptions of warlordism
    - Cooperative warlord politics vs. ungoverned warlordism
    - >
        Warlordism as the dominant political order of pre-state
        societies
    - Economics of warlordism
    - >
        Understanding warlordism in the context of European
        feudalism
    - Warlordism in the contemporary world
    - Cooperative warlord politics
    - The Philippines
    - Afghanistan
categories:
    - Uncategorized
---
A warlord is a leader able to exercise military, economic, and political control over a subnational territory within a sovereign state due to his ability to mobilize loyal armed forces. These armed forces, usually considered militias, are loyal to the warlord rather than to the state regime. Warlords have existed throughout much of history, albeit in a variety of different capacities within the political, economic, and social structure of states or ungoverned territories.
