---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differentiable_stack
offline_file: ""
offline_thumbnail: ""
uuid: 7744ba6f-5b76-4d8a-8ffb-67b9876c3d47
updated: 1484309212
title: Differentiable stack
tags:
    - Connection with Lie groupoids
    - Differential space
    - With Grothendieck topology
    - Gerbes
categories:
    - Differential geometry
---
In differential geometry, a differentiable stack is a stack over the category of differentiable manifolds (with the usual open covering topology) which admits an atlas. In other words, a differentiable stack is a stack that can be represented by a Lie groupoid.
