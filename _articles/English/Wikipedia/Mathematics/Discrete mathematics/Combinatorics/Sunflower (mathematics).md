---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sunflower_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 7fe67642-8088-4d20-8510-6ae61b217b26
updated: 1484308598
title: Sunflower (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sonnenblume_02_KMJ.jpg
tags:
    - Formal definition
    - Δ-lemma
    - Δ-lemma for ω2
    - Sunflower lemma and conjecture
categories:
    - Combinatorics
---
In mathematics, a sunflower or 
  
    
      
        Δ
      
    
    {\displaystyle \Delta }
  
-system is a collection of sets whose pairwise intersection is constant, and called the kernel.
