---
version: 1
type: video
provider: YouTube
id: wvJAgrUBF4w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8b47a5b3-ef81-449c-ac20-93f4b2a07030
updated: 1486069645
title: Amazing Resonance Experiment!
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/wvJAgrUBF4w/default.jpg
    - https://i3.ytimg.com/vi/wvJAgrUBF4w/1.jpg
    - https://i3.ytimg.com/vi/wvJAgrUBF4w/2.jpg
    - https://i3.ytimg.com/vi/wvJAgrUBF4w/3.jpg
---
