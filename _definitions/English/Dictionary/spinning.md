---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spinning
offline_file: ""
offline_thumbnail: ""
uuid: 7eb6f6bb-4526-4fc9-901f-529becc35847
updated: 1484310393
title: spinning
categories:
    - Dictionary
---
spin
     n 1: a swift whirling motion (usually of a missile)
     2: the act of rotating rapidly; "he gave the crank a spin"; "it
        broke off after much twisting" [syn: {twirl}, {twist}, {twisting},
         {whirl}]
     3: a short drive in a car; "he took the new car for a spin"
     4: rapid descent of an aircraft in a steep spiral [syn: {tailspin}]
     5: a distinctive interpretation (especially as used by
        politicians to sway public opinion); "the campaign put a
        favorable spin on the story"
     v 1: revolve quickly and repeatedly around one's own axis; "The
          dervishes whirl around and around without getting dizzy"
          [syn: {spin around}, {whirl}, ...
