---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scheme
offline_file: ""
offline_thumbnail: ""
uuid: 4e443eb6-23f2-4b42-9dd5-8bfe7f37b0af
updated: 1484310533
title: scheme
categories:
    - Dictionary
---
scheme
     n 1: an elaborate and systematic plan of action [syn: {strategy}]
     2: a statement that evades the question by cleverness or
        trickery [syn: {dodge}, {dodging}]
     3: a group of independent but interrelated elements comprising
        a unified whole; "a vast system of production and
        distribution and consumption keep the country going" [syn:
         {system}]
     4: an internal representation of the world; an organization of
        concepts and actions that can be revised by new
        information about the world [syn: {schema}]
     5: a schematic or preliminary plan [syn: {outline}, {schema}]
     v 1: form intrigues (for) in an underhand manner [syn: ...
