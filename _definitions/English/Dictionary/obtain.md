---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obtain
offline_file: ""
offline_thumbnail: ""
uuid: 88963ff6-9b0a-408c-86bc-2989ecfe41df
updated: 1484310264
title: obtain
categories:
    - Dictionary
---
obtain
     v 1: come into possession of; "How did you obtain the visa?"
     2: receive a specified treatment (abstract); "These aspects of
        civilization do not find expression or receive an
        interpretation"; "His movie received a good review"; "I
        got nothing but trouble for my good intentions" [syn: {receive},
         {get}, {find}, {incur}]
     3: be valid, applicable, or true; "This theory still holds"
        [syn: {prevail}, {hold}]
