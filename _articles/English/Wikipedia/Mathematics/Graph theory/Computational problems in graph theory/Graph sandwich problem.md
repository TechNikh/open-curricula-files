---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_sandwich_problem
offline_file: ""
offline_thumbnail: ""
uuid: d5da261d-4e68-47ee-bbd8-5fd31bc9e99b
updated: 1484309392
title: Graph sandwich problem
tags:
    - Problem statement
    - Computational complexity
    - Additional reading
categories:
    - Computational problems in graph theory
---
In graph theory and computer science, the graph sandwich problem is a problem of finding a graph that belongs to a particular family of graphs and is "sandwiched" between two other graphs, one of which must be a subgraph and the other of which must be a supergraph of the desired graph.
