---
version: 1
type: video
provider: YouTube
id: Ngzs77i8XV4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ed449601-c9a7-4201-a749-e99fccecf3b6
updated: 1486069616
title: Matching radical functions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ngzs77i8XV4/default.jpg
    - https://i3.ytimg.com/vi/Ngzs77i8XV4/1.jpg
    - https://i3.ytimg.com/vi/Ngzs77i8XV4/2.jpg
    - https://i3.ytimg.com/vi/Ngzs77i8XV4/3.jpg
---
