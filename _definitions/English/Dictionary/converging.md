---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/converging
offline_file: ""
offline_thumbnail: ""
uuid: 6dc8cd61-7c9a-40cd-80a2-d391f939dc37
updated: 1484310206
title: converging
categories:
    - Dictionary
---
converging
     n : the act of converging (coming closer) [syn: {convergence}, {convergency}]
