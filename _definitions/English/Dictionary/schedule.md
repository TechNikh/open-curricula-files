---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/schedule
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473261
title: schedule
categories:
    - Dictionary
---
schedule
     n 1: a temporally organized plan for matters to be attended to
          [syn: {agenda}, {docket}]
     2: an ordered list of times at which things are planned to
        occur
     v 1: plan for an activity or event; "I've scheduled a concert
          next week"
     2: make a schedule; plan the time and place for events; "I
        scheduled an exam for this afternoon"
