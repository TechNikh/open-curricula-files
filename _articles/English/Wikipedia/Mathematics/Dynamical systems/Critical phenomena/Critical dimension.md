---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_dimension
offline_file: ""
offline_thumbnail: ""
uuid: f370cec9-002c-4a29-a1c8-f9646f8dc283
updated: 1484308746
title: Critical dimension
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-CriticalHyperplane.png
tags:
    - Upper critical dimension in field theory
    - Lower critical dimension
categories:
    - Critical phenomena
---
In the renormalization group analysis of phase transitions in physics, a critical dimension is the dimensionality of space at which the character of the phase transition changes. Below the lower critical dimension there is no phase transition. Above the upper critical dimension the critical exponents of the theory become the same as that in mean field theory. An elegant criterion to obtain the critical dimension within mean field theory is due to V. Ginzburg.
