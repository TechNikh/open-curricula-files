---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polar_point_group
offline_file: ""
offline_thumbnail: ""
uuid: 41e06a73-ab63-4446-9964-5efcb1225c8c
updated: 1484309313
title: Polar point group
categories:
    - Symmetry
---
In geometry, a polar point group is a point group in which every symmetry operation leaves more than one point unmoved.[1] Therefore, a point group with more than one axis of rotation or a mirror plane perpendicular to the axis of rotation cannot be polar.
