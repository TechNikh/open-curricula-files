---
version: 1
type: video
provider: YouTube
id: 4i1MUWJoI0U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 94499a4b-6d62-4cb4-ba50-44622afe083d
updated: 1486069682
title: 'Thermodynamics- Crash Course Physics #23'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/4i1MUWJoI0U/default.jpg
    - https://i3.ytimg.com/vi/4i1MUWJoI0U/1.jpg
    - https://i3.ytimg.com/vi/4i1MUWJoI0U/2.jpg
    - https://i3.ytimg.com/vi/4i1MUWJoI0U/3.jpg
---
