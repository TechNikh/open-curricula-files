---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recommended
offline_file: ""
offline_thumbnail: ""
uuid: c6ba78d3-7423-4108-8eae-225fb9131c0d
updated: 1484310381
title: recommended
categories:
    - Dictionary
---
recommended
     adj : mentioned as worthy of acceptance; "the recommended
           medicine"; "the suggested course of study" [syn: {suggested}]
