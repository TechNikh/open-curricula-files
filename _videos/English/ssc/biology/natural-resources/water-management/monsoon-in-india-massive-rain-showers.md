---
title: monsoon in India. Massive rain showers
youtube_id: c7AlR4ue-kk
tags:
    - Rain
    - India
    - Monsoon
    - Storm
    - Lightning
    - Thunder
    - weather
    - Flood
    - Wind
    - Thunderstorm
    - Chandigarh
    - Punjab
categories:
    - Water management
---
