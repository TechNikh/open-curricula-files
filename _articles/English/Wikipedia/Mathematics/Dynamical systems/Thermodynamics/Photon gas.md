---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Photon_gas
offline_file: ""
offline_thumbnail: ""
uuid: 03843e06-3025-499f-9743-4af083510cf7
updated: 1484308857
title: Photon gas
tags:
    - Thermodynamics of a black body photon gas
    - Isothermal transformations
categories:
    - Thermodynamics
---
In physics, a photon gas is a gas-like collection of photons, which has many of the same properties of a conventional gas like hydrogen or neon - including pressure, temperature, and entropy. The most common example of a photon gas in equilibrium is black body radiation.
