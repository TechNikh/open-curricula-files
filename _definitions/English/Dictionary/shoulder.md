---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shoulder
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605801
title: shoulder
categories:
    - Dictionary
---
shoulder
     n 1: the part of the body between the neck and the upper arm
     2: a cut of beef from the shoulder of the animal
     3: a ball-and-socket joint between the head of the humerus and
        a cavity of the scapula [syn: {shoulder joint}, {articulatio
        humeri}]
     4: narrow edge of land (usually unpaved) along the side of a
        road [syn: {berm}]
     v 1: lift onto one's shoulders
     2: push with the shoulders; "He shouldered his way into the
        crowd"
     3: carry a burden, either real or metaphoric; "shoulder the
        burden"
