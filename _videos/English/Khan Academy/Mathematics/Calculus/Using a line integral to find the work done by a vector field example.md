---
version: 1
type: video
provider: YouTube
id: AFF8FXxt5os
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Using%20a%20line%20integral%20to%20find%20the%20work%20done%20by%20a%20vector%20field%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b98f1ee6-9e60-4aaf-bed9-0c0020ee43d5
updated: 1486069696
title: >
    Using a line integral to find the work done by a vector
    field example
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/AFF8FXxt5os/default.jpg
    - https://i3.ytimg.com/vi/AFF8FXxt5os/1.jpg
    - https://i3.ytimg.com/vi/AFF8FXxt5os/2.jpg
    - https://i3.ytimg.com/vi/AFF8FXxt5os/3.jpg
---
