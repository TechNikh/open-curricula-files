---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tight
offline_file: ""
offline_thumbnail: ""
uuid: bc2349b3-8745-4d64-a6ae-7e7f8f936fbc
updated: 1484310226
title: tight
categories:
    - Dictionary
---
tight
     adj 1: closely constrained or constricted or constricting; "tight
            skirts"; "he hated tight starched collars"; "fingers
            closed in a tight fist"; "a tight feeling in his
            chest" [ant: {loose}]
     2: pulled or drawn tight; "taut sails"; "a tight drumhead"; "a
        tight rope" [syn: {taut}]
     3: set so close together as to be invulnerable to penetration;
        "in tight formation"; "a tight blockade"
     4: pressed tightly together; "with lips compressed" [syn: {compressed}]
     5: used of persons or behavior; characterized by or indicative
        of lack of generosity; "a mean person"; "he left a miserly
        tip" [syn: {mean}, ...
