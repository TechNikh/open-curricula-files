---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/one-sided
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593321
title: one-sided
categories:
    - Dictionary
---
one-sided
     adj 1: not reversible or capable of having either side out [syn: {nonreversible}]
            [ant: {reversible}]
     2: involving only one part or side; "unilateral paralysis"; "a
        unilateral decision" [syn: {unilateral}] [ant: {multilateral},
         {bilateral}]
     3: out of proportion in shape [syn: {ill-proportioned}, {lopsided}]
     4: favoring one person or side over another; "a biased account
        of the trial"; "a decision that was partial to the
        defendant" [syn: {biased}, {colored}, {coloured}, {slanted}]
     5: excessively devoted to one faction [syn: {biased}]
