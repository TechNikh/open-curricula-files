---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Johnson_bound
offline_file: ""
offline_thumbnail: ""
uuid: 70cef821-8325-4b59-b909-3eda941c4cd9
updated: 1484308553
title: Johnson bound
categories:
    - Coding theory
---
In applied mathematics, the Johnson bound (named after Selmer Martin Johnson) is a limit on the size of error-correcting codes, as used in coding theory for data transmission or communications.
