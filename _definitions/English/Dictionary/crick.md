---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crick
offline_file: ""
offline_thumbnail: ""
uuid: cbfa0b52-b162-42f5-abc4-aaedf1194455
updated: 1484310295
title: crick
categories:
    - Dictionary
---
crick
     n 1: a painful muscle spasm especially in the neck or back
          (`rick' and `wrick' are British) [syn: {rick}, {wrick}]
     2: English biochemist who (with Watson in 1953) helped discover
        the helical structure of DNA (born in 1916) [syn: {Francis
        Crick}, {Francis Henry Compton Crick}]
     v : twist the head into a strained position
