---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/granted
offline_file: ""
offline_thumbnail: ""
uuid: 3b3595fd-ffc7-43d0-ba1f-2b4a744c6194
updated: 1484310431
title: granted
categories:
    - Dictionary
---
granted
     adj 1: acknowledged as a supposition; "given the engine's
            condition, it is a wonder that it started" [syn: {given}]
     2: given as a grant; "the special funds granted for his
        research project"
