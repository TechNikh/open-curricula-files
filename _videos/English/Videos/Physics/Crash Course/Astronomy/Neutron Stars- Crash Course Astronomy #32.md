---
version: 1
type: video
provider: YouTube
id: RrMvUL8HFlM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 45ffc359-f2ca-494d-b062-400ebc0d08e3
updated: 1486069684
title: 'Neutron Stars- Crash Course Astronomy #32'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/RrMvUL8HFlM/default.jpg
    - https://i3.ytimg.com/vi/RrMvUL8HFlM/1.jpg
    - https://i3.ytimg.com/vi/RrMvUL8HFlM/2.jpg
    - https://i3.ytimg.com/vi/RrMvUL8HFlM/3.jpg
---
