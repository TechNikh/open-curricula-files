---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kashmir
offline_file: ""
offline_thumbnail: ""
uuid: c36e8b38-0385-4dac-835c-039e01f08e93
updated: 1484308415
title: Kashmir
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kashmir_region_2004.jpg
tags:
    - Etymology
    - History
    - Hinduism and Buddhism in Kashmir
    - Muslim rule
    - Foreign rule
    - Dogra rule
    - 1947 and 1948
    - Current status and political divisions
    - Demographics
    - Economy
    - Transport
    - Notes
    - Bibliography
    - Historical sources
categories:
    - Uncategorized
---
Kashmir is the northernmost geographical region of South Asia. Until the mid-19th century, the term "Kashmir" denoted only the valley between the Great Himalayas and the Pir Panjal mountain range. Today, it denotes a larger area that includes the Indian-administered territory of Jammu and Kashmir (subdivided into Jammu, Kashmir, and Ladakh divisions), the Pakistani-administered territories of Azad Kashmir and Gilgit-Baltistan, and Chinese-administered territories of Aksai Chin and the Trans-Karakoram Tract.[1][2][3] Not only are Jammu, Ladakh, Azad Kashmir and Gilgit Baltistan separate from the Kashmir Valley geographically, they are also non-Kashmiri areas, with the Kashmir Valley being ...
