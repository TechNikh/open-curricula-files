---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pepsin
offline_file: ""
offline_thumbnail: ""
uuid: 3656c952-8d14-434c-bd73-6f6ff2ff7775
updated: 1484310160
title: pepsin
categories:
    - Dictionary
---
pepsin
     n : an enzyme produced in the stomach that splits proteins into
         peptones
