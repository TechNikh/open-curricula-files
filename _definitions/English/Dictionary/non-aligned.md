---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-aligned
offline_file: ""
offline_thumbnail: ""
uuid: cd570002-186d-42c4-b9a6-0cc74bede006
updated: 1484310607
title: non-aligned
categories:
    - Dictionary
---
nonaligned
     adj : not affiliated with any faction, party, or cause [ant: {aligned}]
