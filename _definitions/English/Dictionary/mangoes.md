---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mangoes
offline_file: ""
offline_thumbnail: ""
uuid: e1e6d2fc-86b1-4372-bcc8-be1da3765d41
updated: 1484310277
title: mangoes
categories:
    - Dictionary
---
mango
     n 1: large evergreen tropical tree cultivated for its large oval
          smooth-skinned fruit [syn: {mango tree}, {Mangifera
          indica}]
     2: large oval smooth-skinned tropical fruit with juicy aromatic
        pulp and a large hairy seed
     [also: {mangoes} (pl)]
