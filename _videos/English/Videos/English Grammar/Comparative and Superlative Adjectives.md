---
version: 1
type: video
provider: YouTube
id: kzA6PmC9bhM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Comparative%20and%20Superlative%20Adjectives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8c48a7d2-5443-4219-b6b4-18e039f983d5
updated: 1486069632
title: Comparative and Superlative Adjectives
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/kzA6PmC9bhM/default.jpg
    - https://i3.ytimg.com/vi/kzA6PmC9bhM/1.jpg
    - https://i3.ytimg.com/vi/kzA6PmC9bhM/2.jpg
    - https://i3.ytimg.com/vi/kzA6PmC9bhM/3.jpg
---
