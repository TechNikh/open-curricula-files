---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prism
offline_file: ""
offline_thumbnail: ""
uuid: 9f8270c0-0560-483f-9943-b8ed4598480c
updated: 1484310214
title: prism
categories:
    - Dictionary
---
prism
     n 1: a polyhedron with two congruent and parallel faces (the
          bases) and whose lateral faces are parallelograms
     2: optical device having a triangular shape and made of glass
        or quartz; used to deviate a beam or invert an image [syn:
         {optical prism}]
