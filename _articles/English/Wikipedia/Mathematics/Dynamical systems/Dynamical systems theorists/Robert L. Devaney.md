---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_L._Devaney
offline_file: ""
offline_thumbnail: ""
uuid: 635a7c34-e480-4ab7-92a6-0903918593e3
updated: 1484308684
title: Robert L. Devaney
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Robert_Devaney2.jpg
tags:
    - Education and career
    - Mathematical activities
    - Awards and honors
    - Selected publications
categories:
    - Dynamical systems theorists
---
Robert Luke Devaney (born 1948) is an American mathematician, the Feld Family Professor of Teaching Excellence at Boston University. His research involves dynamical systems and fractals.[1]
