---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Template:Geometry-stub
offline_file: ""
offline_thumbnail: ""
uuid: 249c1aa3-69c0-4818-b980-1ec8f8843120
updated: 1484309352
title: Template:Geometry-stub
tags:
    - About this template
    - Usage
    - General information
    - What is a stub?
    - How is a stub identified?
    - Further information
categories:
    - Geometry stubs
---
