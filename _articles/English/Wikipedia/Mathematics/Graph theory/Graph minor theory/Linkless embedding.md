---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linkless_embedding
offline_file: ""
offline_thumbnail: ""
uuid: e9878279-f6b4-477b-8474-9624c367679e
updated: 1484309411
title: Linkless embedding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hopf_Link.png
tags:
    - Definitions
    - Examples and counterexamples
    - Characterization and recognition
    - Related families of graphs
    - History
    - Notes
    - Additional reading
categories:
    - Graph minor theory
---
In topological graph theory, a mathematical discipline, a linkless embedding of an undirected graph is an embedding of the graph into Euclidean space in such a way that no two cycles of the graph have nonzero linking number. A flat embedding is an embedding with the property that every cycle is the boundary of a topological disk whose interior is disjoint from the graph. A linklessly embeddable graph is a graph that has a linkless or flat embedding; these graphs form a three-dimensional analogue of the planar graphs.[1] Complementarily, an intrinsically linked graph is a graph that does not have a linkless embedding.
