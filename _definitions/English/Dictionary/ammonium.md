---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ammonium
offline_file: ""
offline_thumbnail: ""
uuid: 184d017a-e08f-4868-9794-999d3cf1c93d
updated: 1484310375
title: ammonium
categories:
    - Dictionary
---
ammonium
     n : the ion NH4 derived from ammonia; behaves in many respects
         like an alkali metal ion [syn: {ammonium ion}]
