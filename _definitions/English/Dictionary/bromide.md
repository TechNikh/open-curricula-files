---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bromide
offline_file: ""
offline_thumbnail: ""
uuid: 13a37642-60ff-4386-a993-db63d7ef695e
updated: 1484310375
title: bromide
categories:
    - Dictionary
---
bromide
     n 1: any of the salts of hydrobromic acid; used as a sedative
     2: a trite or obvious remark [syn: {platitude}, {cliche}, {banality},
         {commonplace}]
     3: a sedative in the form of sodium or potassium bromide
