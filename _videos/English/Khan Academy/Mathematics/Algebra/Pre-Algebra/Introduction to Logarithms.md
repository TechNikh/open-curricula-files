---
version: 1
type: video
provider: YouTube
id: mQTWzLpCcW0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Introduction%20to%20Logarithms.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bdcf0c2b-fb20-4f6c-9de0-03fa4aee884d
updated: 1486069567
title: Introduction to Logarithms
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/mQTWzLpCcW0/default.jpg
    - https://i3.ytimg.com/vi/mQTWzLpCcW0/1.jpg
    - https://i3.ytimg.com/vi/mQTWzLpCcW0/2.jpg
    - https://i3.ytimg.com/vi/mQTWzLpCcW0/3.jpg
---
An introduction to logarithms
