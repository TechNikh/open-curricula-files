---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Physical_phenomena
offline_file: ""
offline_thumbnail: ""
uuid: 4df66117-293c-401d-a09a-dd1c7515396a
updated: 1484308375
title: Phenomenon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cloud_chamber_bionerd.jpg
tags:
    - Modern philosophical usage
    - Scientific
    - Mechanical
    - Gem
    - Group and social
    - Popular usage
categories:
    - Uncategorized
---
A phenomenon (Greek: , phainomenon, from the verb phainein, to show, shine, appear, to be manifest or manifest itself, plural phenomena)[1] is any thing which manifests itself. Phenomena are often, but not always, understood as "things that appear" or "experiences" for a sentient being, or in principle may be so.
