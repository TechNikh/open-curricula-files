---
version: 1
type: video
provider: YouTube
id: iUzfsUOl3-A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 304a445a-95ee-4c96-a752-0758391193a7
updated: 1486069694
title: 'Solid of Revolution  (part 2)'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/iUzfsUOl3-A/default.jpg
    - https://i3.ytimg.com/vi/iUzfsUOl3-A/1.jpg
    - https://i3.ytimg.com/vi/iUzfsUOl3-A/2.jpg
    - https://i3.ytimg.com/vi/iUzfsUOl3-A/3.jpg
---
