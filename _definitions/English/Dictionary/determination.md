---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/determination
offline_file: ""
offline_thumbnail: ""
uuid: 7891bc7e-8192-48c2-912d-b8b7a1338446
updated: 1484310297
title: determination
categories:
    - Dictionary
---
determination
     n 1: the act of determining the properties of something [syn: {finding}]
     2: the quality of being determined to do or achieve something;
        "his determination showed in his every movement"; "he is a
        man of purpose" [syn: {purpose}]
     3: a position or opinion or judgment reached after
        consideration; "a decision unfavorable to the opposition";
        "his conclusion took the evidence into account";
        "satisfied with the panel's determination" [syn: {decision},
         {conclusion}]
     4: the act of making up your mind about something; "the burden
        of decision was his"; "he drew his conclusions quickly"
        [syn: {decision}, ...
