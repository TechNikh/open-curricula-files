---
version: 1
type: video
provider: YouTube
id: rtwkshuIEl8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b14f6809-f633-4ad6-ab09-f0172b7d5c9b
updated: 1486069635
title: "Wave Optics- Modified Young's Double  Slit Experiment - 18/35"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/rtwkshuIEl8/default.jpg
    - https://i3.ytimg.com/vi/rtwkshuIEl8/1.jpg
    - https://i3.ytimg.com/vi/rtwkshuIEl8/2.jpg
    - https://i3.ytimg.com/vi/rtwkshuIEl8/3.jpg
---
