---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drawers
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447761
title: drawers
categories:
    - Dictionary
---
drawers
     n 1: (usually in the plural) underpants worn by men [syn: {underdrawers},
           {shorts}, {boxers}, {boxershorts}]
     2: (usually in the plural) underpants worn by women; "she was
        afraid that her bloomers might have been showing" [syn: {bloomers},
         {pants}, {knickers}]
