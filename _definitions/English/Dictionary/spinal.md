---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spinal
offline_file: ""
offline_thumbnail: ""
uuid: b35e236b-399a-42f5-b814-fcf0f87a98ad
updated: 1484310319
title: spinal
categories:
    - Dictionary
---
spinal
     adj : of or relating to the spine or spinal cord; "spinal cord";
           "spinal injury"
     n : anesthesia of the lower half of the body; caused by injury
         to the spinal cord or by injecting an anesthetic beneath
         the arachnoid membrane that surrounds the spinal cord
         [syn: {spinal anesthesia}, {spinal anaesthesia}]
