---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/probability
offline_file: ""
offline_thumbnail: ""
uuid: f8ddc435-8386-4a74-92d8-3d9500454fb1
updated: 1484310393
title: probability
categories:
    - Dictionary
---
probability
     n 1: a measure of how likely it is that some event will occur;
          "what is the probability of rain?"; "we have a good
          chance of winning" [syn: {chance}]
     2: the quality of being probable [ant: {improbability}]
