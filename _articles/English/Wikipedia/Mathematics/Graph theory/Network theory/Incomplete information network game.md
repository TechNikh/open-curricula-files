---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Incomplete_information_network_game
offline_file: ""
offline_thumbnail: ""
uuid: 3ecd493d-8dda-4b31-9b8b-0890f1c180a1
updated: 1484309435
title: Incomplete information network game
tags:
    - Game theoretic formulation
    - Example of imperfect information game played on networks
categories:
    - Network theory
---
Network games of incomplete information represent strategic network formation when agents do not know in advance their neighbors, i.e. the network structure and the value stemming from forming links with neighboring agents. In such a setting, agents have prior beliefs about the value of attaching to their neighbors; take their action based on their prior belief and update their belief based on the history of the game.[1] While games with a fully known network structure are widely applicable, there are many applications when players act without fully knowing with whom they interact or what their neighbors’ action will be.[2] For example, people choosing major in college can be formalized ...
