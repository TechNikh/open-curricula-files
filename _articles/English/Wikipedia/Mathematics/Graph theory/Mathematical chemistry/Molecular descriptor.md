---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_descriptor
offline_file: ""
offline_thumbnail: ""
uuid: e1236cd1-bfa9-4353-b0bd-20ca97a146a6
updated: 1484309431
title: Molecular descriptor
tags:
    - Invariance properties of molecular descriptors
    - Degeneracy of molecular descriptors
    - Basic requirements for optimal descriptors
    - Bibliography
categories:
    - Mathematical chemistry
---
Molecular descriptors play a fundamental role in chemistry, pharmaceutical sciences, environmental protection policy, and health researches, as well as in quality control, being the way molecules, thought of as real bodies, are transformed into numbers, allowing some mathematical treatment of the chemical information contained in the molecule. This was defined by Todeschini and Consonni as:
