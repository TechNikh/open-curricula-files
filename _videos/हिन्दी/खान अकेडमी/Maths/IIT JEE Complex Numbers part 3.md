---
version: 1
type: video
provider: YouTube
id: p4yWlBGS7jY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 591e5127-b5e4-47cd-8837-17b3aeab0cbf
updated: 1486069605
title: IIT JEE Complex Numbers part 3
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/p4yWlBGS7jY/default.jpg
    - https://i3.ytimg.com/vi/p4yWlBGS7jY/1.jpg
    - https://i3.ytimg.com/vi/p4yWlBGS7jY/2.jpg
    - https://i3.ytimg.com/vi/p4yWlBGS7jY/3.jpg
---
