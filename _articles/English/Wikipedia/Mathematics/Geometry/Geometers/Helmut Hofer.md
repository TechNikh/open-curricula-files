---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Helmut_Hofer
offline_file: ""
offline_thumbnail: ""
uuid: ca1a3930-4835-468e-a4b2-f557fb701b00
updated: 1484309083
title: Helmut Hofer
tags:
    - Selected publications
    - Notes
categories:
    - Geometers
---
Helmut Hermann W. Hofer (born February 18 or February 28, 1956) is a German-American mathematician, one of the founders of the area of symplectic topology.[1] He is a member of the National Academy of Sciences,[2] and the recipient of the 1999 Ostrowski Prize[3] and the 2013 Heinz Hopf Prize [1]. Since 2009, he is a faculty member at the Institute for Advanced Study in Princeton.[1] He currently works on symplectic geometry, dynamical systems, and partial differential equations. His contributions to the field include Hofer geometry.
