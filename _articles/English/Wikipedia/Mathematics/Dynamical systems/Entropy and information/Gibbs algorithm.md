---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gibbs_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 23c18784-0b2f-4695-840b-f641eb69cb6e
updated: 1484308773
title: Gibbs algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Josiah_Willard_Gibbs_-from_MMS-_0.jpg
categories:
    - Entropy and information
---
In statistical mechanics, the Gibbs algorithm, introduced by J. Willard Gibbs in 1902, is a criterion for choosing a probability distribution for the statistical ensemble of microstates of a thermodynamic system by minimizing the average log probability
