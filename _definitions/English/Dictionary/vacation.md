---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vacation
offline_file: ""
offline_thumbnail: ""
uuid: e32859bd-567f-485f-a61d-dfab3b64f9a1
updated: 1484310484
title: vacation
categories:
    - Dictionary
---
vacation
     n 1: leisure time away from work devoted to rest or pleasure; "we
          get two weeks of vacation every summer"; "we took a
          short holiday in Puerto Rico" [syn: {holiday}]
     2: the act of making something legally void
     v : spend or take a vacation [syn: {holiday}]
