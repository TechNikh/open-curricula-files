---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_machine_learning
offline_file: ""
offline_thumbnail: ""
uuid: 6aee6d1a-e34f-45ef-b3aa-b3ffb7e0dbe7
updated: 1484308678
title: Quantum machine learning
tags:
    - Quantum methods for Machine Learning
    - Quantum Support Vector Machines
    - Quantum Clustering and k-nearest neighbour methods
    - Quantum neural networks
    - Machine learning methods for quantum information
    - Corporate investments into quantum machine learning research
categories:
    - Theoretical computer science
---
Quantum machine learning is a newly emerging interdisciplinary research area between quantum physics and computer science that summarises efforts to combine quantum mechanics with methods of machine learning.[1][2] Quantum machine learning models or algorithms intend to use the advantages of quantum information in order to improve classical methods of machine learning, for example by developing efficient implementations of expensive classical algorithms on a quantum computer.[3][4][5] However, quantum machine learning also includes the vice versa approach, namely applying classical methods of machine learning to quantum information theory.
