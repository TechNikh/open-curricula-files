---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Minimum_total_potential_energy_principle
offline_file: ""
offline_thumbnail: ""
uuid: 99efe9cc-b77d-43f7-a347-b06297779e5c
updated: 1484308853
title: Minimum total potential energy principle
categories:
    - Thermodynamics
---
The minimum total potential energy principle is a fundamental concept used in physics, chemistry, biology, and engineering. It dictates that (at low temperatures) a structure or body shall deform or displace to a position that (locally) minimizes the total potential energy, with the lost potential energy being converted into kinetic energy (specifically heat).
