---
version: 1
type: video
provider: YouTube
id: Auv-yHdjA6Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 22df1b4a-608c-4b61-863e-6408e8f7c69c
updated: 1486069605
title: 'Conic Sections  Intro to Ellipses'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Auv-yHdjA6Q/default.jpg
    - https://i3.ytimg.com/vi/Auv-yHdjA6Q/1.jpg
    - https://i3.ytimg.com/vi/Auv-yHdjA6Q/2.jpg
    - https://i3.ytimg.com/vi/Auv-yHdjA6Q/3.jpg
---
