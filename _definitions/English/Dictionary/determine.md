---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/determine
offline_file: ""
offline_thumbnail: ""
uuid: 0e8608a8-eb67-42d1-ade7-013dcad589cb
updated: 1484310299
title: determine
categories:
    - Dictionary
---
determine
     v 1: after a calculation, investigation, experiment, survey, or
          study; "find the product of two numbers"; "The physicist
          who found the elusive particle won the Nobel Prize"
          [syn: {find}, {find out}, {ascertain}]
     2: shape or influence; give direction to; "experience often
        determines ability"; "mold public opinion" [syn: {shape},
        {mold}, {influence}, {regulate}]
     3: fix conclusively or authoritatively; "set the rules" [syn: {set}]
     4: decide upon or fix definitely; "fix the variables"; "specify
        the parameters" [syn: {specify}, {set}, {fix}, {limit}]
     5: reach, make, or come to a decision about something; ...
