---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beam_propagation_method
offline_file: ""
offline_thumbnail: ""
uuid: d732db18-dd38-4a3d-b944-7d3d7a22f43c
updated: 1484308754
title: Beam propagation method
tags:
    - Principles
    - Numerical methods
    - Applications
    - Limitations of BPM
    - Implementations
categories:
    - Electrodynamics
---
The beam propagation method (BPM) is an approximation technique for simulating the propagation of light in slowly varying optical waveguides. It is essentially the same as the so-called parabolic equation (PE) method in underwater acoustics. Both BPM and the PE were first introduced in the 1970s. When a wave propagates along a waveguide for a large distance (larger compared with the wavelength), rigorous numerical simulation is difficult. The BPM relies on approximate differential equations which are also called the one-way models. These one-way models involve only a first order derivative in the variable z (for the waveguide axis) and they can be solved as "initial" value problem. The ...
