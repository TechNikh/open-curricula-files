---
version: 1
type: video
provider: YouTube
id: xLgaoorsi9U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3793efd9-022b-48dd-b168-a486a2a35b14
updated: 1486069647
title: 'Fractals   The Hidden Dimension'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xLgaoorsi9U/default.jpg
    - https://i3.ytimg.com/vi/xLgaoorsi9U/1.jpg
    - https://i3.ytimg.com/vi/xLgaoorsi9U/2.jpg
    - https://i3.ytimg.com/vi/xLgaoorsi9U/3.jpg
---
