---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lawful
offline_file: ""
offline_thumbnail: ""
uuid: 25cd35d7-bf96-4d5e-b62f-0bf823debec9
updated: 1484310597
title: lawful
categories:
    - Dictionary
---
lawful
     adj 1: conformable to or allowed by law; "lawful methods of
            dissent" [ant: {unlawful}]
     2: according to custom or rule or natural law [syn: {orderly},
        {rule-governed}]
     3: having a legally established claim; "the legitimate heir";
        "the true and lawful king" [syn: {true(a)}, {rightful(a)}]
     4: authorized, sanctioned by, or in accordance with law; "a
        legitimate government" [syn: {legitimate}, {licit}]
