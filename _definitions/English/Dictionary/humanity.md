---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humanity
offline_file: ""
offline_thumbnail: ""
uuid: c21c27f2-a490-4200-b186-4f9ed302a8cc
updated: 1484310583
title: humanity
categories:
    - Dictionary
---
humanity
     n 1: all of the inhabitants of the earth; "all the world loves a
          lover"; "she always used `humankind' because `mankind'
          seemed to slight the women" [syn: {world}, {human race},
           {humankind}, {human beings}, {humans}, {mankind}, {man}]
     2: the quality of being humane
     3: the quality of being human; "he feared the speedy decline of
        all manhood" [syn: {humanness}, {manhood}]
