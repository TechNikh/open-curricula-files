---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Discrete_and_Computational_Geometry
offline_file: ""
offline_thumbnail: ""
uuid: b58af3a9-4624-4054-909f-7333c8032c6c
updated: 1484308590
title: Discrete and Computational Geometry
categories:
    - Discrete geometry
---
Discrete & Computational Geometry is a peer-reviewed mathematics journal published quarterly by Springer. Founded in 1986 by Jacob E. Goodman and Richard M. Pollack, the journal publishes articles on discrete geometry and computational geometry.
