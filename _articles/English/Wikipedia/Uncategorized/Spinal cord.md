---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spinal_cord
offline_file: ""
offline_thumbnail: ""
uuid: 7f60e293-ebc6-4532-83fe-33757f0e9350
updated: 1484308368
title: Spinal cord
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Nervous_system_diagram-en.svg.png
tags:
    - Structure
    - Spinal cord segments
    - Development
    - Blood supply
    - Function
    - Somatosensory organization
    - Motor organization
    - Spinocerebellar tracts
    - Clinical significance
    - Injury
    - Treatment
    - Lumbar puncture
    - Tumours
    - Additional images
categories:
    - Uncategorized
---
The spinal cord is a long, thin, tubular bundle of nervous tissue and support cells that extends from the medulla oblongata in the brainstem to the lumbar region of the vertebral column. The brain and spinal cord together make up the central nervous system (CNS). The spinal cord begins at the occipital bone and extends down to the space between the first and second lumbar vertebrae; it does not extend the entire length of the vertebral column. It is around 45 cm (18 in) in men and around 43 cm (17 in) long in women. Also, the spinal cord has a varying width, ranging from 13 mm (1⁄2 in) thick in the cervical and lumbar regions to 6.4 mm (1⁄4 in) thick in the thoracic area. The ...
