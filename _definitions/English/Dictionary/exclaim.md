---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exclaim
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514301
title: exclaim
categories:
    - Dictionary
---
exclaim
     v 1: utter aloud; often with surprise, horror, or joy; "`I won!'
          he exclaimed"; "`Help!' she cried"; "`I'm here,' the
          mother shouted when she saw her child looking lost"
          [syn: {cry}, {cry out}, {outcry}, {call out}, {shout}]
     2: state or announce; "`I am not a Communist,' " he exclaimed;
        "The King will proclaim an amnesty" [syn: {proclaim}, {promulgate}]
