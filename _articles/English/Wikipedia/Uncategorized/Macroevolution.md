---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Macroevolution
offline_file: ""
offline_thumbnail: ""
uuid: 7bdad855-2728-4bf0-8564-adfe90d1680e
updated: 1484308345
title: Macroevolution
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ambulocetus_nattans.jpg
tags:
    - Origin of the term
    - Macroevolution and the modern evolutionary synthesis
    - Types of macroevolution
    - Research topics
    - Notes
categories:
    - Uncategorized
---
Macroevolution is evolution on a scale at or above the level of species, in contrast with microevolution,[1] which refers to smaller evolutionary changes of allele frequencies within a species or population.[2] Macroevolution and microevolution describe fundamentally identical processes on different time scales.[3][4]
