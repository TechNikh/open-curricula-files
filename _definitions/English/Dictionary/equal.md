---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equal
offline_file: ""
offline_thumbnail: ""
uuid: 31dbb678-49e0-4fb1-ba42-ed8f458add16
updated: 1484310344
title: equal
categories:
    - Dictionary
---
equal
     adj 1: well matched; having the same quantity, value, or measure as
            another; "on equal terms"; "all men are equal before
            the law" [ant: {unequal}]
     2: equal in amount or value; "like amounts"; "equivalent
        amounts"; "the same amount"; "gave one six blows and the
        other a like number"; "an equal number"; "the same number"
        [syn: {like}, {equivalent}, {same}] [ant: {unlike}]
     n : a person who is of equal standing with another in a group
         [syn: {peer}, {match}, {compeer}]
     v 1: be identical or equivalent to; "One dollar equals 1,000
          rubles these days!" [syn: {be}] [ant: {differ}]
     2: be equal to in ...
