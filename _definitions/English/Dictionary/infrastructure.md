---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infrastructure
offline_file: ""
offline_thumbnail: ""
uuid: fb22f550-f783-4c50-830d-497b5ded2c1d
updated: 1484310264
title: infrastructure
categories:
    - Dictionary
---
infrastructure
     n 1: the basic structure or features of a system or organization
          [syn: {substructure}]
     2: the stock of basic facilities and capital equipment needed
        for the functioning of a country or area; "the industrial
        base of Japan" [syn: {base}]
