---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enrichment
offline_file: ""
offline_thumbnail: ""
uuid: 5dc43f45-1e81-4349-9fbe-d02e09707069
updated: 1484310409
title: enrichment
categories:
    - Dictionary
---
enrichment
     n 1: act of making fuller or more meaningful or rewarding
     2: a gift that significantly increases the recipient's wealth
