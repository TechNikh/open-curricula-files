---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inequal
offline_file: ""
offline_thumbnail: ""
uuid: df471579-5596-4aa3-9e6e-2801da60845a
updated: 1484308444
title: Inequality (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Linear_Programming_Feasible_Region.svg.png
tags:
    - Properties
    - Transitivity
    - Converse
    - Addition and subtraction
    - Multiplication and division
    - Additive inverse
    - Multiplicative inverse
    - Applying a function to both sides
    - Ordered fields
    - Chained notation
    - Sharp inequalities
    - Inequalities between means
    - Power inequalities
    - Examples
    - Well-known inequalities
    - Complex numbers and inequalities
    - Vector inequalities
    - General existence theorems
    - Notes
categories:
    - Uncategorized
---
If the values in question are elements of an ordered set, such as the integers or the real numbers, they can be compared in size.
