---
version: 1
type: video
provider: YouTube
id: 0pv0QtOi5l8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Double%20and%20triple%20integrals/Double%20integrals%206.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1d071633-1df3-4cd4-8d1a-18a0fede133d
updated: 1486069697
title: Double integrals 6
categories:
    - Double and triple integrals
thumbnail_urls:
    - https://i3.ytimg.com/vi/0pv0QtOi5l8/default.jpg
    - https://i3.ytimg.com/vi/0pv0QtOi5l8/1.jpg
    - https://i3.ytimg.com/vi/0pv0QtOi5l8/2.jpg
    - https://i3.ytimg.com/vi/0pv0QtOi5l8/3.jpg
---
