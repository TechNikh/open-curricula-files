---
version: 1
type: video
provider: YouTube
id: 99pD1-6ZpuM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Scalar%20field%20line%20integral%20independent%20of%20path%20direction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 528bf8e3-abb8-4ff0-89f8-2c85da38e514
updated: 1486069696
title: Scalar field line integral independent of path direction
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/99pD1-6ZpuM/default.jpg
    - https://i3.ytimg.com/vi/99pD1-6ZpuM/1.jpg
    - https://i3.ytimg.com/vi/99pD1-6ZpuM/2.jpg
    - https://i3.ytimg.com/vi/99pD1-6ZpuM/3.jpg
---
