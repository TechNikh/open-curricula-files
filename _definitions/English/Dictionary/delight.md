---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delight
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484435401
title: delight
categories:
    - Dictionary
---
delight
     n 1: a feeling of extreme pleasure or satisfaction; "his delight
          to see her was obvious to all" [syn: {delectation}]
     2: something or someone that provides pleasure; a source of
        happiness; "a joy to behold"; "the pleasure of his
        company"; "the new car is a delight" [syn: {joy}, {pleasure}]
     v 1: give pleasure to or be pleasing to; "These colors please the
          senses"; "a pleasing sensation" [syn: {please}] [ant: {displease}]
     2: take delight in; "he delights in his granddaughter" [syn: {enjoy},
         {revel}]
     3: hold spellbound [syn: {enchant}, {enrapture}, {transport}, {enthrall},
         {ravish}, {enthral}] [ant: ...
