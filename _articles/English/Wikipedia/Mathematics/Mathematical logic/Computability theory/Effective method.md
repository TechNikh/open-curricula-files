---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Effective_method
offline_file: ""
offline_thumbnail: ""
uuid: 4f4e45a7-9dc8-441d-b4c7-a03a5e8fdc55
updated: 1484309485
title: Effective method
tags:
    - Definition
    - Algorithms
    - Computable functions
categories:
    - Computability theory
---
In logic, mathematics and computer science, especially metalogic and computability theory, an effective method[1] or effective procedure is a procedure for solving a problem from a specific class. An effective method is sometimes also called mechanical method or procedure.[2]
