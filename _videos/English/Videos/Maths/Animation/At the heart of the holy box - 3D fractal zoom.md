---
version: 1
type: video
provider: YouTube
id: OW5RnrlTeow
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de9680eb-43b1-457d-b4ee-3ab2f41bea89
updated: 1486069647
title: 'At the heart of the holy box - 3D fractal zoom'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/OW5RnrlTeow/default.jpg
    - https://i3.ytimg.com/vi/OW5RnrlTeow/1.jpg
    - https://i3.ytimg.com/vi/OW5RnrlTeow/2.jpg
    - https://i3.ytimg.com/vi/OW5RnrlTeow/3.jpg
---
