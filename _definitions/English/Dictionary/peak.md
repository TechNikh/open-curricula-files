---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peak
offline_file: ""
offline_thumbnail: ""
uuid: 7c3a8b58-a4c8-48df-a476-3e52291c931c
updated: 1484310369
title: peak
categories:
    - Dictionary
---
peak
     adj 1: of a period of maximal use or demand or activity; "at peak
            hours the streets traffic is unbelievable" [ant: {off-peak}]
     2: approaching or constituting a maximum; "maximal
        temperature"; "maximum speed"; "working at peak
        efficiency" [syn: {highest}, {peak(a)}]
     n 1: the most extreme possible amount or value; "voltage peak"
          [syn: {extremum}]
     2: the period of greatest prosperity or productivity [syn: {flower},
         {prime}, {heyday}, {bloom}, {blossom}, {efflorescence}, {flush}]
     3: the highest level or degree attainable; "his landscapes were
        deemed the acme of beauty"; "the artist's gifts are at
        their ...
