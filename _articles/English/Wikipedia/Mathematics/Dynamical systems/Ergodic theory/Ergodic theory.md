---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodic_theory
offline_file: ""
offline_thumbnail: ""
uuid: bc6a76c3-9181-4fe3-8294-f67f2d0670c2
updated: 1484308775
title: Ergodic theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hamiltonian_flow_classical_0.gif
tags:
    - Ergodic transformations
    - Examples
    - Ergodic theorems
    - 'Probabilistic formulation: Birkhoff–Khinchin theorem'
    - Mean ergodic theorem
    - Convergence of the ergodic means in the Lp norms
    - Sojourn time
    - Ergodic flows on manifolds
    - Historical references
    - Modern references
categories:
    - Ergodic theory
---
Ergodic theory (Ancient Greek: ergon work, hodos way) is a branch of mathematics that studies dynamical systems with an invariant measure and related problems. Its initial development was motivated by problems of statistical physics.
