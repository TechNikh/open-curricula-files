---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Effective_Polish_space
offline_file: ""
offline_thumbnail: ""
uuid: 3a413ae5-d0fb-4edf-bbc9-45275c0f9f81
updated: 1484309485
title: Effective Polish space
categories:
    - Computability theory
---
In mathematical logic, an effective Polish space is a complete separable metric space that has a computable presentation. Such spaces are studied in effective descriptive set theory and in constructive analysis. In particular, standard examples of Polish spaces such as the real line, the Cantor set and the Baire space are all effective Polish spaces.
