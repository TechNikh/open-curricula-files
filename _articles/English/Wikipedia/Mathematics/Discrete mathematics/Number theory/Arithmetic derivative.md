---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_derivative
offline_file: ""
offline_thumbnail: ""
uuid: f4a7eceb-9ca3-4460-ab95-13f5485c8292
updated: 1484308542
title: Arithmetic derivative
tags:
    - Definition
    - Average order
    - Inequalities and bounds
    - Relevance to number theory
categories:
    - Number theory
---
In number theory, the Lagarias arithmetic derivative, or number derivative, is a function defined for integers, based on prime factorization, by analogy with the product rule for the derivative of a function that is used in mathematical analysis.
