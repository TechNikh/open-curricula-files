---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seldom
offline_file: ""
offline_thumbnail: ""
uuid: f31e39cb-a985-4917-9e99-cebdac0d53d4
updated: 1484310518
title: seldom
categories:
    - Dictionary
---
seldom
     adv : not often; "we rarely met" [syn: {rarely}] [ant: {frequently}]
