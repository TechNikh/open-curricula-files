---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_theory
offline_file: ""
offline_thumbnail: ""
uuid: b7c1d837-0049-4baa-ab4b-9573c749ebec
updated: 1484309020
title: Chess theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy.png
tags:
    - Opening theory
    - Middlegame theory
    - Endgame theory
categories:
    - Chess theory
---
The game of chess is commonly divided into three phases: the opening, middlegame, and endgame.[1] There is a large body of theory regarding how the game should be played in each of these phases, especially the opening and endgame. Those who write about chess theory, who are often but not necessarily also eminent players, are referred to as "theorists" or "theoreticians".
