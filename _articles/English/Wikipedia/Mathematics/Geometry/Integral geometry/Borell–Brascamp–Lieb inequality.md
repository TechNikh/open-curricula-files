---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Borell%E2%80%93Brascamp%E2%80%93Lieb_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: f06419ee-13a2-4106-95b0-6daf42f44e4c
updated: 1484309257
title: Borell–Brascamp–Lieb inequality
categories:
    - Integral geometry
---
In mathematics, the Borell–Brascamp–Lieb inequality is an integral inequality due to many different mathematicians but named after Christer Borell, Herm Jan Brascamp and Elliott Lieb.
