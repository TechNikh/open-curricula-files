---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/numbers
offline_file: ""
offline_thumbnail: ""
uuid: 7f1d1e1e-5a5d-4245-b3a3-15ff8a229eff
updated: 1484310273
title: numbers
categories:
    - Dictionary
---
Numbers
     n 1: the fourth book of the Old Testament; contains a record of
          the number of Israelites who followed Moses out of Egypt
          [syn: {Book of Numbers}]
     2: an illegal daily lottery [syn: {numbers pool}, {numbers game},
         {numbers racket}]
