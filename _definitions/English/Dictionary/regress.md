---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regress
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484530201
title: regress
categories:
    - Dictionary
---
regress
     n 1: the reasoning involved when you assume the conclusion is
          true and reason backward to the evidence [syn: {reasoning
          backward}]
     2: returning to a former state [syn: {regression}, {reversion},
         {retrogression}, {retroversion}]
     v 1: go back to a statistical means
     2: go back to a previous state; "We reverted to the old rules"
        [syn: {revert}, {return}, {retrovert}, {turn back}]
     3: get worse; fall back to a previous or worse condition [syn:
        {retrograde}, {retrogress}] [ant: {progress}]
     4: go back to bad behavior; "Those who recidivate are often
        minor criminals" [syn: {relapse}, {lapse}, {recidivate}, ...
