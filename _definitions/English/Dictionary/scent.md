---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scent
offline_file: ""
offline_thumbnail: ""
uuid: 11b11f8b-1253-4d72-b343-71c6409ee5d1
updated: 1484310349
title: scent
categories:
    - Dictionary
---
scent
     n 1: a distinctive odor that is pleasant [syn: {aroma}, {fragrance},
           {perfume}]
     2: an odor left in passing by which a person or animal can be
        traced
     3: any property detected by the olfactory system [syn: {olfactory
        property}, {smell}, {aroma}, {odor}, {odour}]
     v 1: cause to smell or be smelly [syn: {odorize}, {odourise}]
          [ant: {deodorize}, {deodorize}]
     2: catch the scent of; get wind of; "The dog nosed out the
        drugs" [syn: {nose}, {wind}]
     3: apply perfume to; "She perfumes herself every day" [syn: {perfume}]
