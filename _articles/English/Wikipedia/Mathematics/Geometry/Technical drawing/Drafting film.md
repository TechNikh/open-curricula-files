---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drafting_film
offline_file: ""
offline_thumbnail: ""
uuid: 8262ac25-8f22-41c4-add9-fcabf8c1188d
updated: 1484309325
title: Drafting film
categories:
    - Technical drawing
---
Drafting film is a sturdier and more dimensionally stable substitute for drafting paper sometimes used for technical drawings, especially architectural drawings, and for art layout drawings, replacing drafting linen for these purposes. Nowadays it is almost invariably made of transparent biaxially oriented polyethylene terephthalate, which should last several centuries under normal storage conditions, with one or two translucent matte surfaces provided by a coating. However, some older drafting films are cellulose acetate, which degrades in only a few decades due to the vinegar syndrome. Uncoated films are preferred for archival, because there is then no possibility that the coating ...
