---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Contact_copier
offline_file: ""
offline_thumbnail: ""
uuid: 998af314-a874-4fcd-9f51-c5e1dbd583c6
updated: 1484309324
title: Contact copier
tags:
    - Uses
    - Photography
    - Whiteprint
    - Silkscreen
    - Offset
    - Printed circuits
categories:
    - Technical drawing
---
A contact copier (also known as contact printer), is a device used to copy an image by illuminating a film negative with the image in direct contact with a photosensitive surface (film, paper, plate, etc.). The more common processes are negative, where clear areas in the original produce an opaque or hardened photosensitive surface, but positive processes are available. The light source is usually an actínic bulb internal or external to the device[1]
