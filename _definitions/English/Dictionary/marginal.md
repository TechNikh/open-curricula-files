---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marginal
offline_file: ""
offline_thumbnail: ""
uuid: 00d96280-3176-4997-8644-7c0ca3b35c1a
updated: 1484310455
title: marginal
categories:
    - Dictionary
---
marginal
     adj 1: at or constituting a border or edge; "the marginal strip of
            beach" [syn: {fringy}]
     2: of questionable or minimal quality; "borderline grades";
        "marginal writing ability" [syn: {borderline}]
     3: of a bare living gained by great labor; "the sharecropper's
        hardscrabble life"; "a marginal existence" [syn: {hardscrabble}]
     4: just barely adequate or within a lower limit; "a bare
        majority"; "a marginal victory" [syn: {bare(a)}]
     5: producing at a rate that barely covers production costs;
        "marginal industries"; "marginal land"
     6: of something or someone close to a lower limit or lower
        class; "marginal ...
