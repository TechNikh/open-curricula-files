---
version: 1
type: video
provider: YouTube
id: OPG-9IFnJnI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Proof%20-%20Sum%20of%20Measures%20of%20Angles%20in%20a%20Triangle%20are%20180.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2eaa34f5-8bbc-46c9-8c49-53781424b534
updated: 1486069579
title: 'Proof - Sum of Measures of Angles in a Triangle are 180'
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/OPG-9IFnJnI/default.jpg
    - https://i3.ytimg.com/vi/OPG-9IFnJnI/1.jpg
    - https://i3.ytimg.com/vi/OPG-9IFnJnI/2.jpg
    - https://i3.ytimg.com/vi/OPG-9IFnJnI/3.jpg
---
Proof that the sum of the measures of the angles in a triangle are 180
