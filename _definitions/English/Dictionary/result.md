---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/result
offline_file: ""
offline_thumbnail: ""
uuid: 0ecc41b5-5a66-4e47-83f8-ab23e56af6c6
updated: 1484310355
title: result
categories:
    - Dictionary
---
result
     n 1: a phenomenon that follows and is caused by some previous
          phenomenon; "the magnetic effect was greater when the
          rod was lengthwise"; "his decision had depressing
          consequences for business"; "he acted very wise after
          the event" [syn: {consequence}, {effect}, {outcome}, {event},
           {issue}, {upshot}]
     2: a statement that solves a problem or explains how to solve
        the problem; "they were trying to find a peaceful
        solution"; "the answers were in the back of the book"; "he
        computed the result to four decimal places" [syn: {solution},
         {answer}, {resolution}, {solvent}]
     3: something that ...
