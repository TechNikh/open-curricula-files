---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grundy_number
offline_file: ""
offline_thumbnail: ""
uuid: a16ad1f4-6d2d-49ee-838e-fad26fef012b
updated: 1484309382
title: Grundy number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Greedy_colourings.svg.png
tags:
    - Example
    - atoms
    - In sparse graphs
    - Computational complexity
    - Well-colored graphs
categories:
    - Graph coloring
---
In graph theory, the Grundy number or Grundy chromatic number of an undirected graph is the maximum number of colors that can be used by a greedy coloring strategy that considers the vertices of the graph in sequence and assigns each vertex its first available color, using a vertex ordering chosen to use as many colors as possible. Grundy numbers are named after P. M. Grundy, who studied an analogous concept for directed graphs in 1939.[1] The undirected version was introduced by Christen & Selkow (1979).[2]
