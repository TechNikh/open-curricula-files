---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epipolar_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 3b891f81-0dc2-4361-a921-f9e87ea5b85d
updated: 1484309181
title: Epipolar geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Aufnahme_mit_zwei_Kameras.svg.png
tags:
    - Epipolar geometry
    - Epipole or epipolar point
    - Epipolar line
    - Epipolar plane
    - Epipolar constraint and triangulation
    - Simplified cases
    - Epipolar geometry of pushbroom sensor
categories:
    - Geometry in computer vision
---
Epipolar geometry is the geometry of stereo vision. When two cameras view a 3D scene from two distinct positions, there are a number of geometric relations between the 3D points and their projections onto the 2D images that lead to constraints between the image points. These relations are derived based on the assumption that the cameras can be approximated by the pinhole camera model.
