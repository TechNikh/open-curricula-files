---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_arboricity
offline_file: ""
offline_thumbnail: ""
uuid: 6e4f3fce-7a8d-4499-8c4b-a0807c25500d
updated: 1484309420
title: Linear arboricity
categories:
    - Graph invariants
---
In graph theory, a branch of mathematics, the linear arboricity of an undirected graph is the smallest number of linear forests its edges can be partitioned into. Here, a linear forest is an acyclic graph with maximum degree two, i.e., a disjoint union of path graphs.
