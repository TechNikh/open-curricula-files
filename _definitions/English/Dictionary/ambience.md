---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ambience
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576161
title: ambience
categories:
    - Dictionary
---
ambience
     n 1: a particular environment or surrounding influence; "there
          was an atmosphere of excitement" [syn: {atmosphere}, {ambiance}]
     2: the atmosphere of an environment [syn: {ambiance}]
