---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shelter
offline_file: ""
offline_thumbnail: ""
uuid: 584d553d-7a08-4b76-ba92-0def1fa2bcba
updated: 1484310254
title: shelter
categories:
    - Dictionary
---
shelter
     n 1: a structure that provides privacy and protection from danger
     2: protective covering that provides protection from the
        weather
     3: the condition of being protected; "they were huddled
        together for protection"; "he enjoyed a sense of peace and
        protection in his new home" [syn: {protection}]
     4: a way of organizing business to reduce the taxes it must pay
        on current earnings [syn: {tax shelter}]
     5: temporary housing for homeless or displaced persons
     v 1: provide shelter for; "After the earthquake, the government
          could not provide shelter for the thousands of homeless
          people"
     2: invest (money) so ...
