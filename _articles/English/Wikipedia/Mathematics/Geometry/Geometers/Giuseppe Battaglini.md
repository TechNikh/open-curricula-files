---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Giuseppe_Battaglini
offline_file: ""
offline_thumbnail: ""
uuid: 1b938bbd-1c15-4387-bfa4-fe380dbd27fe
updated: 1484309071
title: Giuseppe Battaglini
categories:
    - Geometers
---
He studied mathematics at the Scuola d'Applicazione di Ponti e Strade (School of Bridges and Roads) of Naples. In 1860 he was appointed professor of Geometria superiore at the University of Naples. Alfredo Capelli and Giovanni Frattini were his Laurea students.
