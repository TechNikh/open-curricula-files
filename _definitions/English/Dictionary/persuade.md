---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/persuade
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453581
title: persuade
categories:
    - Dictionary
---
persuade
     v 1: win approval or support for; "Carry all before one"; "His
          speech did not sway the voters" [syn: {carry}, {sway}]
     2: cause somebody to adopt a certain position, belief, or
        course of action; twist somebody's arm; "You can't
        persuade me to buy this ugly vase!" [ant: {dissuade}]
