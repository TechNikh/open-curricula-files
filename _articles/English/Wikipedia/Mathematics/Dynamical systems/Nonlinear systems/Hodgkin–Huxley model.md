---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hodgkin%E2%80%93Huxley_model'
offline_file: ""
offline_thumbnail: ""
uuid: ed92f31b-6953-43c0-936f-7b795b3fa8f6
updated: 1484308822
title: Hodgkin–Huxley model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Hodgkin-Huxley.svg.png
tags:
    - Basic components
    - Ionic current characterization
    - Voltage-gated ion channels
    - Leak channels
    - Pumps and exchangers
    - Mathematical properties
    - Center manifold
    - Bifurcations
    - Improvements and alternative models
categories:
    - Nonlinear systems
---
The Hodgkin–Huxley model, or conductance-based model, is a mathematical model that describes how action potentials in neurons are initiated and propagated. It is a set of nonlinear differential equations that approximates the electrical characteristics of excitable cells such as neurons and cardiac myocytes, and hence it is a continuous time model, unlike the Rulkov map for example.
