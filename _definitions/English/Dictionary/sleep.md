---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sleep
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484433541
title: sleep
categories:
    - Dictionary
---
sleep
     n 1: a natural and periodic state of rest during which
          consciousness of the world is suspended; "he didn't get
          enough sleep last night"; "calm as a child in dreamless
          slumber" [syn: {slumber}]
     2: a torpid state resembling sleep
     3: a period of time spent sleeping; "he felt better after a
        little sleep"; "there wasn't time for a nap" [syn: {nap}]
     4: euphemisms for death (based on an analogy between lying in a
        bed and in a tomb); "she was laid to rest beside her
        husband"; "they had to put their family pet to sleep"
        [syn: {rest}, {eternal rest}, {eternal sleep}, {quietus}]
     v 1: be asleep [syn: {kip}, ...
