---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civilization
offline_file: ""
offline_thumbnail: ""
uuid: 1c101b8a-c63c-4e56-a605-dba6a6452c05
updated: 1484310216
title: civilization
categories:
    - Dictionary
---
civilization
     n 1: a society in an advanced state of social development (e.g.,
          with complex legal and political and religious
          organizations); "the people slowly progressed from
          barbarism to civilization" [syn: {civilisation}]
     2: the social process whereby societies achieve civilization
        [syn: {civilisation}]
     3: a particular society at a particular time and place; "early
        Mayan civilization" [syn: {culture}, {civilisation}]
     4: the quality of excellence in thought and manners and taste;
        "a man of intellectual refinement"; "he is remembered for
        his generosity and civilization" [syn: {refinement}, {civilisation}]
