---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soviets
offline_file: ""
offline_thumbnail: ""
uuid: 1a19da5b-a57b-4a68-a720-d7abf1b48afe
updated: 1484308479
title: Soviet Union
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Soviet_Union_-_Russian_SFSR_%25281922%2529.svg_0.png'
tags:
    - Name
    - Geography, climate and environment
    - History
    - Revolution and foundation
    - Unification of republics
    - Stalin era
    - 1930s
    - World War II
    - Cold War
    - Khrushchev era
categories:
    - Uncategorized
---
The Soviet Union (Russian: Сове́тский Сою́з, tr. Sovetskiy Soyuz; IPA: [sɐ'vʲetskʲɪj sɐˈjʉs]), officially the Union of Soviet Socialist Republics (Russian: Сою́з Сове́тских Социалисти́ческих Респу́блик, tr. Soyuz Sovetskikh Sotsialisticheskikh Respublik; IPA: [sɐˈjus sɐˈvʲɛtskʲɪx sətsɨəlʲɪsˈtʲitɕɪskʲɪx rʲɪˈspublʲɪk] ( listen)), abbreviated to USSR (Russian: СССР, tr. SSSR), was a socialist state on the Eurasian continent that existed from 1922 to 1991. A union of multiple subnational Soviet republics, its government and economy were highly centralized. The Soviet Union was a one-party federation, ...
