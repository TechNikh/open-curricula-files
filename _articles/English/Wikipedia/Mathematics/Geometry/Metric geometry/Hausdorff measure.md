---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hausdorff_measure
offline_file: ""
offline_thumbnail: ""
uuid: fba4f033-f2e3-4500-99fc-7ece9a57d741
updated: 1484309266
title: Hausdorff measure
tags:
    - Definition
    - Properties of Hausdorff measures
    - Relation with Hausdorff dimension
    - Generalizations
categories:
    - Metric geometry
---
In mathematics a Hausdorff measure is a type of outer measure, named for Felix Hausdorff, that assigns a number in [0,∞] to each set in Rn or, more generally, in any metric space. The zero-dimensional Hausdorff measure is the number of points in the set (if the set is finite) or ∞ if the set is infinite. The one-dimensional Hausdorff measure of a simple curve in Rn is equal to the length of the curve. Likewise, the two dimensional Hausdorff measure of a measurable subset of R2 is proportional to the area of the set. Thus, the concept of the Hausdorff measure generalizes counting, length, and area. It also generalizes volume. In fact, there are d-dimensional Hausdorff measures for any ...
