---
version: 1
type: article
id: https://en.wikipedia.org/wiki/De_Sitter_space
offline_file: ""
offline_thumbnail: ""
uuid: 778f8503-b103-4184-8354-d585cef65960
updated: 1484309215
title: De Sitter space
tags:
    - Definition
    - Properties
    - Static coordinates
    - Flat slicing
    - Open slicing
    - Closed slicing
    - dS slicing
categories:
    - Differential geometry
---
In mathematics and physics, a de Sitter space is the analog in Minkowski space, or spacetime, of a sphere in ordinary, Euclidean space. The n-dimensional de Sitter space, denoted dSn, is the Lorentzian manifold analog of an n-sphere (with its canonical Riemannian metric); it is maximally symmetric, has constant positive curvature, and is simply connected for n at least 3. De Sitter space and anti-de Sitter space are named after Willem de Sitter (1872–1934), professor of astronomy at Leiden University and director of the Leiden Observatory. Willem de Sitter and Albert Einstein worked in the 1920s in Leiden closely together on the spacetime structure of our universe.
