---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Griffiths_group
offline_file: ""
offline_thumbnail: ""
uuid: 28578e48-ed76-41c1-9df2-4529d29dddda
updated: 1484309153
title: Griffiths group
categories:
    - Algebraic geometry
---
In mathematics, more specifically in algebraic geometry, the Griffiths group of a projective complex manifold X measures the difference between homological equivalence and algebraic equivalence, which are two important equivalence relations of algebraic cycles.
