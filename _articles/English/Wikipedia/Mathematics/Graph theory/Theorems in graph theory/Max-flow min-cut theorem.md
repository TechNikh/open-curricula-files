---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Max-flow_min-cut_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 37a277f7-2c0e-4db2-9a9b-7d5763bd49a9
updated: 1484309456
title: Max-flow min-cut theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/288px-Max-flow_min-cut_example.svg.png
tags:
    - Definitions and statement
    - Maximum flow
    - Minimum cut
    - Statement
    - Linear program formulation
    - Example
    - Application
    - Generalized max-flow min-cut theorem
    - "Menger's theorem"
    - Project selection problem
    - Image segmentation problem
    - History
    - Proof
categories:
    - Theorems in graph theory
---
In optimization theory, the max-flow min-cut theorem states that in a flow network, the maximum amount of flow passing from the source to the sink is equal to the total weight of the edges in the minimum cut, i.e. the smallest total weight of the edges which if removed would disconnect the source from the sink.
