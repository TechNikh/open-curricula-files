---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angular_distance
offline_file: ""
offline_thumbnail: ""
uuid: 9ee9dea0-2bd6-416f-bffa-2328754ec348
updated: 1484309340
title: Angular distance
tags:
    - use
    - Measurement
    - Equation
categories:
    - Trigonometry
---
In mathematics (in particular geometry and trigonometry) and all natural sciences (including astronomy, geophysics, etc.), the angular distance (angular separation, apparent distance, or apparent separation) between two point objects, as observed from a location different from either of these objects, is the size of the angle between the two directions originating from the observer and pointing towards these two objects.
