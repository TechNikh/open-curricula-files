---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oesophageal
offline_file: ""
offline_thumbnail: ""
uuid: af857203-40f5-4976-b193-082b4db0e5a5
updated: 1484308378
title: Esophagus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Tractus_intestinalis_esophagus.svg.png
tags:
    - Structure
    - Sphincters
    - Nerve supply
    - Gastro-esophageal junction
    - Microanatomy
    - Development
    - Function
    - Swallowing
    - Reducing gastric reflux
    - Clinical significance
    - Inflammation
    - "Barrett's esophagus"
    - Cancer
    - Varices
    - Motility disorders
    - Malformations
    - Imaging
    - History
    - Other animals
    - Vertebrates
    - Invertebrates
categories:
    - Uncategorized
---
The esophagus (American English) or oesophagus (British English) (/ᵻˈsɒfəɡəs/), commonly known as the food pipe or gullet, is an organ in vertebrates through which food passes, aided by peristaltic contractions, from the pharynx to the stomach. The esophagus is a fibromuscular tube, about 25 centimetres long in adults, which travels behind the trachea and heart, passes through the diaphragm and empties into the uppermost region of the stomach. During swallowing, the epiglottis tilts backwards to prevent food from going down the larynx and lungs. The word esophagus is the Greek word for gullet.
