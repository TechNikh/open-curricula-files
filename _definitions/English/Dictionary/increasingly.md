---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/increasingly
offline_file: ""
offline_thumbnail: ""
uuid: c2c9a725-aaa1-4501-8020-766a3e255b63
updated: 1484310473
title: increasingly
categories:
    - Dictionary
---
increasingly
     adv : advancing in amount or intensity; "she became increasingly
           depressed" [syn: {progressively}, {more and more}]
