---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/long
offline_file: ""
offline_thumbnail: ""
uuid: 0fa36cd4-e5ca-4d85-9c61-69a451f0bdb8
updated: 1484310346
title: long
categories:
    - Dictionary
---
long
     adj 1: primarily temporal sense; being or indicating a relatively
            great or greater than average duration or passage of
            time or a duration as specified; "a long life"; "a
            long boring speech"; "a long time"; "a long
            friendship"; "a long game"; "long ago"; "an hour long"
            [ant: {short}]
     2: primarily spatial sense; of relatively great or greater than
        average spatial extension or extension as specified; "a
        long road"; "a long distance"; "contained many long
        words"; "ten miles long" [ant: {short}]
     3: of relatively great height; "a race of long gaunt men"-
        Sherwood Anderson; "looked out ...
