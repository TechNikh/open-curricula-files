---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frequency
offline_file: ""
offline_thumbnail: ""
uuid: 4638a988-ef5a-4e12-8e0b-1b8bd4e946a5
updated: 1484308460
title: Frequency
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-FrequencyAnimation.gif
tags:
    - Definitions
    - Units
    - Period versus frequency
    - Related types of frequency
    - In wave propagation
    - Measurement
    - By counting
    - By stroboscope
    - By frequency counter
    - Heterodyne methods
categories:
    - Uncategorized
---
Frequency is the number of occurrences of a repeating event per unit time.[1] It is also referred to as temporal frequency, which emphasizes the contrast to spatial frequency and angular frequency. The period is the duration of time of one cycle in a repeating event, so the period is the reciprocal of the frequency.[2] For example, if a newborn baby's heart beats at a frequency of 120 times a minute, its period—the time interval between beats—is half a second (that is, 60 seconds divided by 120 beats). Frequency is an important parameter used in science and engineering to specify the rate of oscillatory and vibratory phenomena, such as mechanical vibrations, audio (sound) signals, ...
