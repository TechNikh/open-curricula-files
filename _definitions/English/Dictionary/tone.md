---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tone
offline_file: ""
offline_thumbnail: ""
uuid: 18edbe1e-0860-42dc-80d4-aff62772a134
updated: 1484310607
title: tone
categories:
    - Dictionary
---
tone
     n 1: the quality of a person's voice; "he began in a
          conversational tone"; "he spoke in a nervous tone of
          voice" [syn: {tone of voice}]
     2: (linguistics) a pitch or change in pitch of the voice that
        serves to distinguish words in tonal languages; "the
        Beijing dialect uses four tones"
     3: (music) the distinctive property of a complex sound (a voice
        or noise or musical sound); "the timbre of her soprano was
        rich and lovely"; "the muffled tones of the broken bell
        summoned them to meet" [syn: {timbre}, {timber}, {quality}]
     4: the general atmosphere of a place or situation and the
        effect that it has on ...
