---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bargaining_problem
offline_file: ""
offline_thumbnail: ""
uuid: fbcc7382-524a-4881-87fc-03271a057ff5
updated: 1484308531
title: Bargaining problem
tags:
    - The bargaining game
    - Formal description
    - Feasibility set
    - Disagreement point
    - Equilibrium analysis
    - Bargaining solutions
    - Nash bargaining solution
    - Kalai–Smorodinsky bargaining solution
    - Egalitarian bargaining solution
    - Comparison table
    - Applications
    - Bargaining solutions and risk-aversion
categories:
    - Cooperative games
---
The two-person bargaining problem is a problem of understanding how two agents should cooperate when non-cooperation leads to Pareto-inefficient results. It is in essence an equilibrium selection problem; many games have multiple equilibria with varying payoffs for each player, forcing the players to negotiate on which equilibrium to target. Solutions to bargaining come in two flavors: an axiomatic approach where desired properties of a solution are satisfied and a strategic approach where the bargaining procedure is modeled in detail as a sequential game.
