---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/publicity
offline_file: ""
offline_thumbnail: ""
uuid: 49141f13-d210-4c97-95bb-ce1254b54a03
updated: 1484310595
title: publicity
categories:
    - Dictionary
---
publicity
     n : a message issued in behalf of some product or cause or idea
         or person or institution [syn: {promotion}, {promotional
         material}, {packaging}]
