---
version: 1
type: article
id: https://en.wikipedia.org/wiki/90_(number)
offline_file: ""
offline_thumbnail: ""
uuid: f3a800d0-ecf9-4160-8d91-8020fce915b4
updated: 1484308964
title: 90 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-I-90.svg.png
tags:
    - In mathematics
    - In science
    - In sports
    - In other fields
categories:
    - Integers
---
In English speech, the numbers 90 and 19 are often confused, as sounding very similar. When carefully enunciated, they differ in which syllable is stressed: 19 /naɪnˈtiːn/ vs 90 /ˈnaɪnti/. However, in dates such as 1999, and when contrasting numbers in the teens and when counting, such as 17, 18, 19, the stress shifts to the first syllable: 19 /ˈnaɪntiːn/.
