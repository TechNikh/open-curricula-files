---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Petersen_family
offline_file: ""
offline_thumbnail: ""
uuid: acd2e42c-fdfa-4851-8878-4cee8fd52bf7
updated: 1484309414
title: Petersen family
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Petersen_family.svg.png
categories:
    - Graph minor theory
---
In graph theory, the Petersen family is a set of seven undirected graphs that includes the Petersen graph and the complete graph K6. The Petersen family is named after Danish mathematician Julius Petersen, the namesake of the Petersen graph.
