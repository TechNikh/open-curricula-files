---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drain
offline_file: ""
offline_thumbnail: ""
uuid: dd84621c-3c3a-46a0-9fba-53fbc7016ea3
updated: 1484310437
title: drain
categories:
    - Dictionary
---
drain
     n 1: emptying accomplished by draining [syn: {drainage}]
     2: tube inserted into a body cavity (as during surgery) to
        remove unwanted material
     3: a pipe through which liquid is carried away [syn: {drainpipe},
         {waste pipe}]
     4: a gradual depletion of energy or resources; "a drain on
        resources"; "a drain of young talent by emmigration"
     v 1: flow off gradually; "The rain water drains into this big
          vat" [syn: {run out}]
     2: deplete of resources; "The exercise class drains me of
        energy"
     3: empty of liquid; drain the liquid from; "We drained the oil
        tank"
     4: make weak; "Life in the camp drained him" [syn: ...
