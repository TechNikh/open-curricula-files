---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violence
offline_file: ""
offline_thumbnail: ""
uuid: b052572d-8b5c-4224-8c7b-38375d04f7a2
updated: 1484310469
title: violence
categories:
    - Dictionary
---
violence
     n 1: an act of aggression (as one against a person who resists);
          "he may accomplish by craft in the long run what he
          cannot do by force and violence in the short one" [syn:
          {force}]
     2: the property of being wild or turbulent; "the storm's
        violence" [syn: {ferocity}, {fierceness}, {furiousness}, {fury},
         {vehemence}, {wildness}]
     3: a turbulent state resulting in injuries and destruction etc.
