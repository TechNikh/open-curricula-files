---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mich%C3%A8le_Audin'
offline_file: ""
offline_thumbnail: ""
uuid: 072a8518-3ad6-430e-ae73-1f26c959694f
updated: 1484309071
title: Michèle Audin
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mich%25C3%25A8le_Audie_09887.jpg'
categories:
    - Geometers
---
Michèle Audin is a French mathematician, writer, and a former professor. She has worked as a professor at the University of Geneva, the University of Paris-Sud and most recently at l'Institut de recherche mathématique avancée (IRMA) in University of Strasbourg, where she performed research notably in the area of symplectic geometry.
