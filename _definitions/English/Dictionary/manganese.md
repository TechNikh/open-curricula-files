---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manganese
offline_file: ""
offline_thumbnail: ""
uuid: 174e6d92-867a-476c-8175-d875924fdbec
updated: 1484310198
title: manganese
categories:
    - Dictionary
---
manganese
     n : a hard brittle gray polyvalent metallic element that
         resembles iron but is not magnetic; used in making steel;
         occurs in many minerals [syn: {Mn}, {atomic number 25}]
