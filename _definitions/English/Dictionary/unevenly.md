---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unevenly
offline_file: ""
offline_thumbnail: ""
uuid: 0cf6fd67-3da8-4671-bd0c-6746894b82f6
updated: 1484310529
title: unevenly
categories:
    - Dictionary
---
unevenly
     adv 1: in an uneven and irregular way [ant: {evenly}]
     2: in a ragged uneven manner; "I took the cigarette he offered,
        drawing at it raggedly" [syn: {raggedly}]
     3: in an unequal or partial manner; "profits were distributed
        unevenly"; "angry at being dealt with so unequally" [syn:
        {unequally}] [ant: {evenly}, {evenly}]
