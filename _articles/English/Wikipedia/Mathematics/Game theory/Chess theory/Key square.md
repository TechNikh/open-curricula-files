---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Key_square
offline_file: ""
offline_thumbnail: ""
uuid: fc17f4b5-01f2-4c4d-bc60-80c4912d0fab
updated: 1484309017
title: Key square
tags:
    - King and pawn versus king
    - Rook pawn
    - Other pawns
    - An exception
    - Example from game
    - Blocked pawns
    - Example with a protected passed pawn
    - Example with more pawns
    - Any key square by any route
    - Notes
categories:
    - Chess theory
---
In chess, particularly in endgames, a key square (also known as a critical square) is a square such that if a player's king can occupy it, he can force some gain such as the promotion of a pawn or the capture of an opponent's pawn. Key squares are useful mostly in endgames involving only kings and pawns. In the king and pawn versus king endgame, the key squares depend on the position of the pawn and are easy to determine. Some more complex positions have easily determined key squares while other positions have harder-to-determine key squares. Some positions have key squares for both White and Black.
