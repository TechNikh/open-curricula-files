---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lathi
offline_file: ""
offline_thumbnail: ""
uuid: 8182bd1d-5e16-4f54-ac80-b59abec40300
updated: 1484310541
title: lathi
categories:
    - Dictionary
---
lathi
     n : club consisting of a heavy stick (often bamboo) bound with
         iron; used by police in India [syn: {lathee}]
