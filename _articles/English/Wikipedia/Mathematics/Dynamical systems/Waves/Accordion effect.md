---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Accordion_effect
offline_file: ""
offline_thumbnail: ""
uuid: 990980ff-605c-45af-9cd3-2b35db38d4a2
updated: 1484308883
title: Accordion effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-ACC_Map_crop_2014_0.png
categories:
    - Waves
---
In physics, the accordion effect occurs when fluctuations in the motion of a travelling body causes disruptions in the flow of elements following it. This can happen in road traffic, foot marching, bicycle racing, and, in general, to processes in a pipeline. These are examples of nonlinear processes. The accordion effect generally decreases the throughput of the system in which it occurs.
