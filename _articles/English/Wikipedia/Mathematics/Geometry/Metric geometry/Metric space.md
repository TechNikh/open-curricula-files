---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_space
offline_file: ""
offline_thumbnail: ""
uuid: a5b38ce1-ab32-4da6-ab6d-4e7e3d4d9dd6
updated: 1484309270
title: Metric space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Diameter_of_a_Set.svg.png
tags:
    - History
    - Definition
    - Examples of metric spaces
    - Open and closed sets, topology and convergence
    - Types of metric spaces
    - Complete spaces
    - Bounded and totally bounded spaces
    - Compact spaces
    - Locally compact and proper spaces
    - Connectedness
    - Separable spaces
    - Types of maps between metric spaces
    - Continuous maps
    - Uniformly continuous maps
    - Lipschitz-continuous maps and contractions
    - Isometries
    - Quasi-isometries
    - Notions of metric space equivalence
    - Topological properties
    - >
        Distance between points and sets; Hausdorff distance and
        Gromov metric
    - Product metric spaces
    - Continuity of distance
    - Quotient metric spaces
    - Generalizations of metric spaces
    - Metric spaces as enriched categories
    - Notes
categories:
    - Metric geometry
---
In mathematics, a metric space is a set for which distances between all members of the set are defined. Those distances, taken together, are called a metric on the set. A metric on a space induces topological properties like open and closed sets, which lead to the study of more abstract topological spaces.
