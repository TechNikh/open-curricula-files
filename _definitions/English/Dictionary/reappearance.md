---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reappearance
offline_file: ""
offline_thumbnail: ""
uuid: e38d7ed1-3904-44b8-9602-d608533eb396
updated: 1484310381
title: reappearance
categories:
    - Dictionary
---
reappearance
     n 1: the event of something appearing again; "the reappearance of
          Halley's comet"
     2: the act of someone appearing again; "his reappearance as
        Hamlet has been long awaited" [syn: {return}]
