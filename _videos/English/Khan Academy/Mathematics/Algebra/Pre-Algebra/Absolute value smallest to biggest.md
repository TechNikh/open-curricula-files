---
version: 1
type: video
provider: YouTube
id: NvGTCzAfvr0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Absolute%20value%20smallest%20to%20biggest.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fe266a95-8528-4222-a13c-3ef528cccf90
updated: 1486069561
title: Absolute value smallest to biggest
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/NvGTCzAfvr0/default.jpg
    - https://i3.ytimg.com/vi/NvGTCzAfvr0/1.jpg
    - https://i3.ytimg.com/vi/NvGTCzAfvr0/2.jpg
    - https://i3.ytimg.com/vi/NvGTCzAfvr0/3.jpg
---
