---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/do
offline_file: ""
offline_thumbnail: ""
uuid: 491643d9-7144-4165-8a0c-4234d546547b
updated: 1484310355
title: do
categories:
    - Dictionary
---
do
     n 1: an uproarious party [syn: {bash}, {brawl}]
     2: the syllable naming the first (tonic) note of any major
        scale in solmization [syn: {doh}, {ut}]
     3: doctor's degree in osteopathy [syn: {Doctor of Osteopathy}]
     v 1: engage in; "make love, not war"; "make an effort"; "do
          research"; "do nothing"; "make revolution" [syn: {make}]
     2: carry out or perform an action; "John did the painting, the
        weeding, and he cleaned out the gutters"; "the skater
        executed a triple pirouette"; "she did a little dance"
        [syn: {perform}, {execute}]
     3: get (something) done; "I did my job" [syn: {perform}]
     4: proceed or get along; "How is ...
