---
version: 1
type: video
provider: YouTube
id: A_NcMQA5RQc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7ce03c6d-f072-4271-9a28-9791a5b35dd3
updated: 1486069652
title: Electric field of a charged sphere
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/A_NcMQA5RQc/default.jpg
    - https://i3.ytimg.com/vi/A_NcMQA5RQc/1.jpg
    - https://i3.ytimg.com/vi/A_NcMQA5RQc/2.jpg
    - https://i3.ytimg.com/vi/A_NcMQA5RQc/3.jpg
---
