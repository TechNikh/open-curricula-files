---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Categorical_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 8deae201-8cc1-4331-8613-cacc2d1191ea
updated: 1484309136
title: Categorical algebra
categories:
    - Algebraic geometry
---
In mathematics, categorical algebra is a subfield of algebra that approaches algebra from the categorical point of view. For example, the study of a symmetric monoidal category as an algebraic object might be considered as a part of categorical algebra. It is closely related to homotopical algebra. In the recent years, the field has been experiencing a turbulent change due to the infusion of topological ideas.
