---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encephalitis
offline_file: ""
offline_thumbnail: ""
uuid: 7498ede4-6705-48a4-a70e-80cb38d371e8
updated: 1484310160
title: encephalitis
categories:
    - Dictionary
---
encephalitis
     n : inflammation of the brain usually caused by a virus;
         symptoms include headache and neck pain and drowsiness
         and nausea and fever (`phrenitis' is no longer in
         scientific use) [syn: {cephalitis}, {phrenitis}]
     [also: {encephalitides} (pl)]
