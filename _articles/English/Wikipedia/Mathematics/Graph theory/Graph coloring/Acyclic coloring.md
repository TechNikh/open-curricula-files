---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acyclic_coloring
offline_file: ""
offline_thumbnail: ""
uuid: ac7a1488-f091-4007-bca8-e426607f4d94
updated: 1484309379
title: Acyclic coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Acyclic_coloring.svg.png
tags:
    - Upper bounds
    - Algorithms and Complexity
categories:
    - Graph coloring
---
In graph theory, an acyclic coloring is a (proper) vertex coloring in which every 2-chromatic subgraph is acyclic. The acyclic chromatic number A(G) of a graph G is the least number of colors needed in any acyclic coloring of G.
