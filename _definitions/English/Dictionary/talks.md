---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/talks
offline_file: ""
offline_thumbnail: ""
uuid: 6ca2b7a8-89f9-4045-8e30-60ed5fc8d69e
updated: 1484310585
title: talks
categories:
    - Dictionary
---
talks
     n : a discussion intended to produce an agreement; "the buyout
         negotiation lasted several days"; "they disagreed but
         kept an open dialogue"; "talks between Israelis and
         Palestinians" [syn: {negotiation}, {dialogue}]
