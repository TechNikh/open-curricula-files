---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/William_Wallace_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 1638a4e4-0508-4ce9-beba-b123ca70e2f5
updated: 1484309107
title: William Wallace (mathematician)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Grave_of_William_Wallace_%25281768-1843%2529_mathematician.jpg'
tags:
    - Biography
    - Mathematical contributions
    - Other works
    - Books
    - family
    - Notes
categories:
    - Geometers
---
Prof William Wallace FRSE MInstCE FRAS LLD (23 September 1768 – 28 April 1843) was a Scottish mathematician and astronomer who invented the eidograph.
