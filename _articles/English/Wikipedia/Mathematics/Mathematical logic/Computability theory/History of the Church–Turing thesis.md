---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/History_of_the_Church%E2%80%93Turing_thesis'
offline_file: ""
offline_thumbnail: ""
uuid: 13c87c87-4ab8-409e-8f31-9327a6e936a4
updated: 1484309487
title: History of the Church–Turing thesis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_5.png
tags:
    - "Peano's nine axioms of arithmetic"
    - Hilbert and the Entscheidungsproblem
    - "Three problems from Hilbert's 2nd and 10th problems"
    - >
        Simple arithmetic functions irreducible to primitive
        recursion
    - "Gödel's proof"
    - Gödel expansion of "effective calculation"
    - Kleene
    - Church definition of "effectively calculable"
    - Post and "effective calculability" as "natural law"
    - Turing and computability
    - >
        Turing identifies effective calculability with machine
        computation
    - 'Rosser: recursion, λ-calculus, and Turing-machine computation identity'
    - Kleene and Thesis I
    - Kleene and Church and Turing theses
    - Gödel, Turing machines, and effectively calculability
    - 'Gandy: "machine computation", discrete, deterministic, and limited to "local causation" by light speed'
    - Soare
    - Breger and problem of tacit axioms
    - Sieg and axiomatic definitions
    - Notes
categories:
    - Computability theory
---
The history of the Church–Turing thesis ("thesis") involves the history of the development of the study of the nature of functions whose values are effectively calculable; or, in more modern terms, functions whose values are algorithmically computable. It is an important topic in modern mathematical theory and computer science, particularly associated with the work of Alonzo Church and Alan Turing.
