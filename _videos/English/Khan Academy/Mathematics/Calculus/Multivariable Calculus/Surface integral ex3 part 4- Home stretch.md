---
version: 1
type: video
provider: YouTube
id: bhrmEy0wvD0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20ex3%20part%204-%20Home%20stretch.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 42da96b3-c489-4ce5-abd8-8cbc1f0da837
updated: 1486069690
title: 'Surface integral ex3 part 4- Home stretch'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/bhrmEy0wvD0/default.jpg
    - https://i3.ytimg.com/vi/bhrmEy0wvD0/1.jpg
    - https://i3.ytimg.com/vi/bhrmEy0wvD0/2.jpg
    - https://i3.ytimg.com/vi/bhrmEy0wvD0/3.jpg
---
