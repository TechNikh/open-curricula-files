---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pattern_language_(formal_languages)
offline_file: ""
offline_thumbnail: ""
uuid: 1b8b2c72-78cb-4792-be1e-d8170ba10922
updated: 1484308683
title: Pattern language (formal languages)
tags:
    - Definition
    - Properties
    - Location in the Chomsky hierarchy
    - Learning patterns
    - Notes
categories:
    - Theoretical computer science
---
In theoretical computer science, a pattern language is a formal language that can be defined as the set of all particular instances of a string of constants and variables. Pattern Languages were introduced by Dana Angluin in the context of machine learning.[1]
