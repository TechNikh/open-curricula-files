---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expert
offline_file: ""
offline_thumbnail: ""
uuid: 060349f3-70c0-4015-b0a1-c99be9aade44
updated: 1484310256
title: expert
categories:
    - Dictionary
---
expert
     adj : having or showing knowledge and skill and aptitude; "adept
           in handicrafts"; "an adept juggler"; "an expert job";
           "a good mechanic"; "a practiced marksman"; "a
           proficient engineer"; "a lesser-known but no less
           skillful composer"; "the effect was achieved by
           skillful retouching" [syn: {adept}, {good}, {practiced},
            {proficient}, {skillful}, {skilful}]
     n : a person with special knowledge or ability who performs
         skillfully
