---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_sphere_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 0ffff7fd-89c5-4f2b-a92d-c1b35b71f1c9
updated: 1484309185
title: Lie sphere geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Sophus_Lie.jpg
tags:
    - Basic concepts
    - Lie sphere geometry in the plane
    - The Lie quadric
    - Incidence of cycles
    - The problem of Apollonius
    - Lie transformations
    - Contact elements and contact lifts
    - Lie sphere geometry in space and higher dimensions
    - General theory
    - Three dimensions and the line-sphere correspondence
    - Dupin cyclides
    - Notes
categories:
    - Conformal geometry
---
Lie sphere geometry is a geometrical theory of planar or spatial geometry in which the fundamental concept is the circle or sphere. It was introduced by Sophus Lie in the nineteenth century.[1] The main idea which leads to Lie sphere geometry is that lines (or planes) should be regarded as circles (or spheres) of infinite radius and that points in the plane (or space) should be regarded as circles (or spheres) of zero radius.
