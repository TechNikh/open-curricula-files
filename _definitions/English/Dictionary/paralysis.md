---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paralysis
offline_file: ""
offline_thumbnail: ""
uuid: b44360d4-3716-4616-9da7-6311cba71563
updated: 1484310186
title: paralysis
categories:
    - Dictionary
---
paralysis
     n : loss of the ability to move a body part [syn: {palsy}]
     [also: {paralyses} (pl)]
