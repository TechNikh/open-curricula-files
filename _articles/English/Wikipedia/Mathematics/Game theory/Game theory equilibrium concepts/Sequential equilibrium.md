---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequential_equilibrium
uuid: 1d96c772-ba1e-4eba-ae56-82efdd6df98b
updated: 1480455025
title: Sequential equilibrium
categories:
    - Game theory equilibrium concepts
---
Sequential equilibrium is a refinement of Nash Equilibrium for extensive form games due to David M. Kreps and Robert Wilson. A sequential equilibrium specifies not only a strategy for each of the players but also a belief for each of the players. A belief gives, for each information set of the game belonging to the player, a probability distribution on the nodes in the information set. A profile of strategies and beliefs is called an assessment for the game. Informally speaking, an assessment is a perfect Bayesian equilibrium if its strategies are sensible given its beliefs and its beliefs are confirmed on the outcome path given by its strategies. The definition of sequential equilibrium ...
