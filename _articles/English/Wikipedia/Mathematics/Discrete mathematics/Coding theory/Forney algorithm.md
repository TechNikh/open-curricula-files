---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forney_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: fa1f150a-d398-4590-a95c-3b398cb985ec
updated: 1484308547
title: Forney algorithm
tags:
    - Procedure
    - Formal derivative
    - Derivation
    - Erasures
categories:
    - Coding theory
---
In coding theory, the Forney algorithm (or Forney's algorithm) calculates the error values at known error locations. It is used as one of the steps in decoding BCH codes and Reed–Solomon codes (a subclass of BCH codes). George David Forney, Jr. developed the algorithm.[1]
