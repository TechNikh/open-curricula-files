---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blueprint
offline_file: ""
offline_thumbnail: ""
uuid: a3b0c604-2c19-408d-9966-e453d89383dc
updated: 1484309321
title: Blueprint
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/LaBelle_Blueprint.jpg
tags:
    - The blueprint processes
    - Blueprints replaced by whiteprints
categories:
    - Technical drawing
---
A blueprint is a reproduction of a technical drawing, documenting an architecture or an engineering design, using a contact print process on light-sensitive sheets. Introduced in the 19th century, the process allowed rapid and accurate reproduction of documents used in construction and industry. The blue-print process was characterized by light colored lines on a blue background, a negative of the original. The process was unable to reproduce color or shades of grey.
