---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visible
offline_file: ""
offline_thumbnail: ""
uuid: aeed2d64-fe85-4786-87b4-09f8834efaa8
updated: 1484310299
title: visible
categories:
    - Dictionary
---
visible
     adj 1: perceptible especially by the eye; or open to easy view; "a
            visible object"; "visible stars"; "mountains visible
            in the distance"; "a visible change of expression";
            "visible files" [syn: {seeable}] [ant: {invisible}]
     2: obvious to the eye; "a visible change of expression" [syn: {obvious}]
     3: being often in the public eye; "a visible public figure"
     4: present and easily available; "the cash on hand is adequate
        for current needs"; "emergency police were on hand in case
        of trouble"; "a visible supply"; "visible resources" [syn:
         {on hand(p)}, {visible(a)}]
