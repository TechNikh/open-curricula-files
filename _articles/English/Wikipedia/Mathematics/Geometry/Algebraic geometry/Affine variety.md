---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_variety
offline_file: ""
offline_thumbnail: ""
uuid: 0c1aae4d-50bb-46bc-8022-b0f4ba4f1879
updated: 1484309130
title: Affine variety
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cubic_with_double_point.svg.png
tags:
    - Introduction
    - Structure sheaf
    - Examples
    - Rational points
    - Tangent space
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, an affine variety over an algebraically closed field k is the zero-locus in the affine n-space 
  
    
      
        
          k
          
            n
          
        
      
    
    {\displaystyle k^{n}}
  
 of some finite family of polynomials of n variables with coefficients in k that generate a prime ideal. If the condition of generating a prime ideal is removed, such a set is called an (affine) algebraic set. A Zariski open subvariety of an affine variety is called a quasi-affine variety.
