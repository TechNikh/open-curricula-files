---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impoverished
offline_file: ""
offline_thumbnail: ""
uuid: c68eff3c-6e6c-4743-8bc7-2f84bcc4b365
updated: 1484310189
title: impoverished
categories:
    - Dictionary
---
impoverished
     adj 1: poor enough to need help from others [syn: {destitute}, {indigent},
             {necessitous}, {needy}, {poverty-stricken}]
     2: destroyed financially; "the broken fortunes of the family"
        [syn: {broken}, {wiped out(p)}]
