---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chinese_auction
offline_file: ""
offline_thumbnail: ""
uuid: 39130d2d-7e3e-4935-80a4-76e601a17329
updated: 1484309010
title: Chinese auction
categories:
    - Auctions
---
A Chinese auction is a combination of a raffle and an auction that is typically featured at charity, church festival and numerous other events. Can also be known as penny social, penny sale, tricky tray or pick-a-prize according to local custom, or to avoid causing offense.
