---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Network_theory_in_risk_assessment
offline_file: ""
offline_thumbnail: ""
uuid: ca28716f-f065-4ba2-9589-a8ba87d0c2d7
updated: 1484309440
title: Network theory in risk assessment
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%2522Bow-tie%2522_diagram_of_components_in_a_directed_network_SVG.svg.png'
tags:
    - Risk assessment key components
    - Network theory key components
    - Basic terminology
    - Other Examples of Network Theory Application
    - Social network
    - Food web
    - Epidemiology
    - Notes
categories:
    - Network theory
---
A network is an abstract structure capturing only the basics of connection patterns and little else. Because it is a generalized pattern, tools developed for analyzing, modeling and understanding networks can theoretically be implemented across disciplines. As long as a system can be represented by a network, there is an extensive set of tools – mathematical, computational, and statistical – that are well-developed and if understood can be applied to the analysis of the system of interest.
