---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 08edea7f-172c-4bea-a1a2-98ece886b135
updated: 1484309257
title: Partial geometry
tags:
    - Properties
    - Special case
    - Generalisations
categories:
    - Incidence geometry
---
An incidence structure 
  
    
      
        C
        =
        (
        P
        ,
        L
        ,
        I
        )
      
    
    {\displaystyle C=(P,L,I)}
  
 consists of points 
  
    
      
        P
      
    
    {\displaystyle P}
  
, lines 
  
    
      
        L
      
    
    {\displaystyle L}
  
, and flags 
  
    
      
        I
        ⊆
        P
        ×
        L
      
    
    {\displaystyle I\subseteq P\times L}
  
 where a point 
  
    
      
        p
      
    
    {\displaystyle p}
  
 is said to be incident with a line 
  
    
      
        l
      
    
    {\displaystyle l}
  
 if 
  
    
      
        (
        p
        ,
        ...
