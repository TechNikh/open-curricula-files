---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equipment
offline_file: ""
offline_thumbnail: ""
uuid: 71da804c-6426-43a3-aaa6-e3d66f00feeb
updated: 1484310451
title: equipment
categories:
    - Dictionary
---
equipment
     n : an instrumentality needed for an undertaking or to perform a
         service
