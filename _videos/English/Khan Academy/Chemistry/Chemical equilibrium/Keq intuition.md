---
version: 1
type: video
provider: YouTube
id: ONBJo7dXJm8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Chemical%20equilibrium/Keq%20intuition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 06781204-48e2-4f77-83bb-b989c3814923
updated: 1486069585
title: Keq intuition
categories:
    - Chemical equilibrium
thumbnail_urls:
    - https://i3.ytimg.com/vi/ONBJo7dXJm8/default.jpg
    - https://i3.ytimg.com/vi/ONBJo7dXJm8/1.jpg
    - https://i3.ytimg.com/vi/ONBJo7dXJm8/2.jpg
    - https://i3.ytimg.com/vi/ONBJo7dXJm8/3.jpg
---
A probabilistic look at how molecules react to develop the intuition behind the equilibrium constant formula. Not necessary to keep progressing through chemistry. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-equilibrium/equilibrium-constant/v/keq-derivation-intuition-can-skip-bit-mathy?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-equilibrium/equilibrium-constant/v/reactions-in-equilibrium?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
