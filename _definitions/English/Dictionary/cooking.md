---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooking
offline_file: ""
offline_thumbnail: ""
uuid: 281cde04-91dd-4151-a166-be023efdf04a
updated: 1484310371
title: cooking
categories:
    - Dictionary
---
cooking
     n : the act of preparing something (as food) by the application
         of heat; "cooking can be a great art"; "people are needed
         who have experience in cookery"; "he left the preparation
         of meals to his wife" [syn: {cookery}, {preparation}]
