---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harmony
offline_file: ""
offline_thumbnail: ""
uuid: 986c00d3-c498-4683-a3a3-ae51d03f7b4c
updated: 1484310249
title: harmony
categories:
    - Dictionary
---
harmony
     n 1: compatibility in opinion and action [syn: {harmoniousness}]
     2: the structure of music with respect to the composition and
        progression of chords [syn: {musical harmony}]
     3: a harmonious state of things in general and of their
        properties (as of colors and sounds); congruity of parts
        with one another and with the whole [syn: {concord}, {concordance}]
     4: agreement of opinions [syn: {concord}, {concordance}]
     5: an agreeable sound property [ant: {dissonance}]
