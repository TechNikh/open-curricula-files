---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inverted_pendulum
offline_file: ""
offline_thumbnail: ""
uuid: 7713ed30-5801-43c6-9fcb-df0ba4a21c36
updated: 1484308728
title: Inverted pendulum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Balancer_with_wine_3.JPG
tags:
    - Overview
    - Equations of motion
    - Stationary pivot point
    - Inverted pendulum on a cart
    - Essentials of stabilization
    - "Lagrange's Equations"
    - "Newton's Second Law"
    - Pendulum with oscillatory base
    - Types of inverted pendulums
    - Examples of inverted pendulums
categories:
    - Control engineering
---
An inverted pendulum is a pendulum that has its center of mass above its pivot point. It is often implemented with the pivot point mounted on a cart that can move horizontally and may be called a cart and pole as shown in the photo.[1] Most applications limit the pendulum to 1 degree of freedom by affixing the pole to an axis of rotation. Whereas a normal pendulum is stable when hanging downwards, an inverted pendulum is inherently unstable, and must be actively balanced in order to remain upright; this can be done either by applying a torque at the pivot point, by moving the pivot point horizontally as part of a feedback system, changing the rate of rotation of a mass mounted on the ...
