---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fireplace
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454181
title: fireplace
categories:
    - Dictionary
---
fireplace
     n : an open recess in a wall at the base of a chimney where a
         fire can be built; "the fireplace was so large you could
         walk inside it"; "he laid a fire in the hearth and lit
         it"; "the hearth was black with the charcoal of many
         fires" [syn: {hearth}, {open fireplace}]
