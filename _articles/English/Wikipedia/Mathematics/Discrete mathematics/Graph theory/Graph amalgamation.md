---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_amalgamation
offline_file: ""
offline_thumbnail: ""
uuid: 9d6bc317-b2c1-4a86-8388-2cfb1985eb76
updated: 1484308619
title: Graph amalgamation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph_amalgamation_of_k5.png
tags:
    - Definition
    - Properties
    - Example
    - Hamiltonian decompositions
    - Notes
categories:
    - Graph theory
---
In graph theory, a graph amalgamation is a relationship between two graphs (one graph is an amalgamation of another). Similar relationships include subgraphs and minors. Amalgamations can provide a way to reduce a graph to a simpler graph while keeping certain structure intact. The amalgamation can then be used to study properties of the original graph in an easier to understand context. Applications include embeddings,[1] computing genus distribution,[2] and Hamiltonian decompositions.
