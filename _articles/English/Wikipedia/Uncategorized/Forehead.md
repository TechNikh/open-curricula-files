---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fore_head
offline_file: ""
offline_thumbnail: ""
uuid: 6e3b9da3-a2fa-4e49-8ca4-20ef840c0cf3
updated: 1484308377
title: Forehead
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Male_forehead-01_ies.jpg
tags:
    - Structure
    - Function
    - Expression
    - Wrinkles
    - Society and culture
categories:
    - Uncategorized
---
In human anatomy, the forehead is an area of the head bounded by three features, two of the skull and one of the scalp. The top of the forehead is marked by the hairline, the edge of the area where hair on the scalp grows. The bottom of the forehead is marked by the supraorbital ridge, the bone feature of the skull above the eyes. The two sides of the forehead are marked by the temporal ridge, a bone feature that links the supraorbital ridge to the coronal suture line and beyond.[1][2]
