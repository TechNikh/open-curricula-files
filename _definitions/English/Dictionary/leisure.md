---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leisure
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566321
title: leisure
categories:
    - Dictionary
---
leisure
     n 1: time available for ease and relaxation; "his job left him
          little leisure" [syn: {leisure time}]
     2: freedom to choose a pastime or enjoyable activity; "he
        lacked the leisure for golf"
