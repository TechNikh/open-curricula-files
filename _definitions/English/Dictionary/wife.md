---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wife
offline_file: ""
offline_thumbnail: ""
uuid: a9e3ca79-efe6-46d0-bbfc-db6d43497faf
updated: 1484310484
title: wife
categories:
    - Dictionary
---
wife
     n : a married woman; a man's partner in marriage [syn: {married
         woman}] [ant: {husband}]
     [also: {wives} (pl)]
