---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abc
offline_file: ""
offline_thumbnail: ""
uuid: 59dd0b0b-5960-4321-ad26-3c6c2dc7bdb3
updated: 1484308444
title: ABC
tags:
    - Broadcasting
    - Companies
    - Computing
    - Economics
    - Science and medicine
    - Mathematics
    - Music
    - Songs and albums
    - Groups
    - Other music
    - Organizations
    - Ornithology
    - Politics
    - religion
    - Other organizations
    - Places
    - Publications and media
    - Technology
    - Transport
    - Cars
    - Others
    - Other uses
categories:
    - Uncategorized
---
ABC or abc may refer to:
