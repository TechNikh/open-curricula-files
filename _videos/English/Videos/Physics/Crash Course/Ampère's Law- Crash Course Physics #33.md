---
version: 1
type: video
provider: YouTube
id: 5fqwJyt4Lus
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Ampe%CC%80re%27s%20Law-%20Crash%20Course%20Physics%20%2333.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 05b2cfd7-82cc-4ca9-94b8-09827c3cce9f
updated: 1486069682
title: "Ampère's Law- Crash Course Physics #33"
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/5fqwJyt4Lus/default.jpg
    - https://i3.ytimg.com/vi/5fqwJyt4Lus/1.jpg
    - https://i3.ytimg.com/vi/5fqwJyt4Lus/2.jpg
    - https://i3.ytimg.com/vi/5fqwJyt4Lus/3.jpg
---
