---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wto
offline_file: ""
offline_thumbnail: ""
uuid: 0ecc93b5-abb7-4cd2-9086-6ad6c77e670e
updated: 1484308460
title: World Trade Organization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-WhiteandKeynes.jpg
tags:
    - History
    - GATT rounds of negotiations
    - From Geneva to Tokyo
    - Uruguay Round
    - Ministerial conferences
    - Doha Round (Doha Agenda)
    - Functions
    - Principles of the trading system
    - Organizational structure
    - Decision-making
categories:
    - Uncategorized
---
The World Trade Organization (WTO) is an intergovernmental organization which regulates international trade. The WTO officially commenced on 1 January 1995 under the Marrakesh Agreement, signed by 123 nations on 15 April 1994, replacing the General Agreement on Tariffs and Trade (GATT), which commenced in 1948.[5] The WTO deals with regulation of trade between participating countries by providing a framework for negotiating trade agreements and a dispute resolution process aimed at enforcing participants' adherence to WTO agreements, which are signed by representatives of member governments[6]:fol.9–10 and ratified by their parliaments.[7] Most of the issues that the WTO focuses on derive ...
