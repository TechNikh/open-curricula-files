---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Halide
offline_file: ""
offline_thumbnail: ""
uuid: e92258ad-5eb0-4a3d-9082-f993c044827c
updated: 1484308401
title: Halide
tags:
    - Tests
    - Uses
    - Compounds
categories:
    - Uncategorized
---
A halide is a binary compound, of which one part is a halogen atom and the other part is an element or radical that is less electronegative (or more electropositive) than the halogen, to make a fluoride, chloride, bromide, iodide, astatide, or theoretically tennesside compound. The alkali metals combine directly with halogens under appropriate conditions forming halides of the general formula, MX(X = F, Cl, Br or I). Many salts are halides; the hal- syllable in halide and halite reflects this correlation. All Group 1 metals form halides that are white solids at room temperature.
