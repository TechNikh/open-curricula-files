---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: a46a3055-a18d-48f7-b5e1-3d27c5700259
updated: 1484310479
title: hierarchy
categories:
    - Dictionary
---
hierarchy
     n 1: a series of ordered groupings of people or things within a
          system; "put honesty first in her hierarchy of values"
     2: the organization of people at different ranks in an
        administrative body [syn: {power structure}, {pecking
        order}]
