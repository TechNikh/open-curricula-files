---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrest
offline_file: ""
offline_thumbnail: ""
uuid: 5c68feb3-6f80-47bd-b683-78048de33ef1
updated: 1484310462
title: arrest
categories:
    - Dictionary
---
arrest
     n 1: the act of apprehending (especially apprehending a
          criminal); "the policeman on the beat got credit for the
          collar" [syn: {apprehension}, {catch}, {collar}, {pinch},
           {taking into custody}]
     2: the state of inactivity following an interruption; "the
        negotiations were in arrest"; "held them in check";
        "during the halt he got some lunch"; "the momentary stay
        enabled him to escape the blow"; "he spent the entire stop
        in his seat" [syn: {check}, {halt}, {hitch}, {stay}, {stop},
         {stoppage}]
     v 1: take into custody; "the police nabbed the suspected
          criminals" [syn: {collar}, {nail}, ...
