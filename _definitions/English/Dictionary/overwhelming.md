---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overwhelming
offline_file: ""
offline_thumbnail: ""
uuid: 171ac3ee-d8bc-44d6-95a5-4247484e1673
updated: 1484310448
title: overwhelming
categories:
    - Dictionary
---
overwhelming
     adj 1: so strong as to be irresistible; "an overpowering need for
            solitude"; "the temptation to despair may become
            overwhelming"; "an overwhelming majority" [syn: {overpowering}]
     2: very intense; "politics is his consuming passion";
        "overwhelming joy" [syn: {consuming}]
