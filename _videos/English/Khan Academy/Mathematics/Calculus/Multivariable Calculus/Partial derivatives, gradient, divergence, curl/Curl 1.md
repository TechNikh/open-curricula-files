---
version: 1
type: video
provider: YouTube
id: Mt4dpGFVsYc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%2C%20gradient%2C%20divergence%2C%20curl/Curl%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ab6e1e5b-cec4-46ee-b86a-906767d4a87e
updated: 1486069690
title: Curl 1
categories:
    - Partial derivatives, gradient, divergence, curl
thumbnail_urls:
    - https://i3.ytimg.com/vi/Mt4dpGFVsYc/default.jpg
    - https://i3.ytimg.com/vi/Mt4dpGFVsYc/1.jpg
    - https://i3.ytimg.com/vi/Mt4dpGFVsYc/2.jpg
    - https://i3.ytimg.com/vi/Mt4dpGFVsYc/3.jpg
---
