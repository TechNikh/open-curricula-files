---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Granular_computing
offline_file: ""
offline_thumbnail: ""
uuid: 6c2c7468-e5d7-4665-b3fa-1c725f622bc6
updated: 1484308671
title: Granular computing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Catarina_26_mar_2004_1310Z.jpg
tags:
    - Types of granulation
    - Value granulation (discretization/quantization)
    - Motivations
    - Issues and methods
    - Variable granulation (clustering/aggregation/transformation)
    - Variable transformation
    - Variable aggregation
    - System granulation (aggregation)
    - Concept granulation (component analysis)
    - Equivalence class granulation
    - Component granulation
    - Different interpretations of granular computing
categories:
    - Theoretical computer science
---
Granular computing (GrC) is an emerging computing paradigm of information processing. It concerns the processing of complex information entities called information granules, which arise in the process of data abstraction and derivation of knowledge from information or data. Generally speaking, information granules are collections of entities that usually originate at the numeric level and are arranged together due to their similarity, functional or physical adjacency, indistinguishability, coherency, or the like.
