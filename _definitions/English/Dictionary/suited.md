---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suited
offline_file: ""
offline_thumbnail: ""
uuid: 9ec5f153-949d-4110-b2d8-3c2760a4b990
updated: 1484310401
title: suited
categories:
    - Dictionary
---
suited
     adj 1: meant or adapted for an occasion or use; "a tractor suitable
            (or fit) for heavy duty"; "not an appropriate (or fit)
            time for flippancy" [syn: {appropriate}, {suitable}]
     2: outfitted or supplied with clothing; "recruits suited in
        green"
