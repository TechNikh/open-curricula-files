---
version: 1
type: video
provider: YouTube
id: ZVklPwGALpI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 99427148-06e7-477b-8904-a1166fe2a286
updated: 1486069681
title: 'Blood Vessels, part 2- Crash Course A&P #28'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZVklPwGALpI/default.jpg
    - https://i3.ytimg.com/vi/ZVklPwGALpI/1.jpg
    - https://i3.ytimg.com/vi/ZVklPwGALpI/2.jpg
    - https://i3.ytimg.com/vi/ZVklPwGALpI/3.jpg
---
