---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schr%C3%B6der%E2%80%93Bernstein_theorem_for_measurable_spaces'
offline_file: ""
offline_thumbnail: ""
uuid: aaf5312c-eaf1-499b-87e8-d7178412038b
updated: 1484308994
title: Schröder–Bernstein theorem for measurable spaces
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Mutual_embedding_of_open_and_closed_real_unit_interval_svg.svg.png
tags:
    - The theorem
    - Comments
    - Proof
    - Examples
    - Example 1
    - Example 2
categories:
    - Theorems in the foundations of mathematics
---
The Cantor–Bernstein–Schroeder theorem of set theory has a counterpart for measurable spaces, sometimes called the Borel Schroeder–Bernstein theorem, since measurable spaces are also called Borel spaces. This theorem, whose proof is quite easy, is instrumental when proving that two measurable spaces are isomorphic. The general theory of standard Borel spaces contains very strong results about isomorphic measurable spaces, see Kuratowski's theorem. However, (a) the latter theorem is very difficult to prove, (b) the former theorem is satisfactory in many important cases (see Examples), and (c) the former theorem is used in the proof of the latter theorem.
