---
version: 1
type: video
provider: YouTube
id: I64X7vHSHOE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de5e9d22-d75c-4fac-9b94-250f4439c98f
updated: 1486069673
title: 'Method of the Year 2010- Optogenetics - by Nature Video'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/I64X7vHSHOE/default.jpg
    - https://i3.ytimg.com/vi/I64X7vHSHOE/1.jpg
    - https://i3.ytimg.com/vi/I64X7vHSHOE/2.jpg
    - https://i3.ytimg.com/vi/I64X7vHSHOE/3.jpg
---
