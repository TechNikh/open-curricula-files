---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/penal
offline_file: ""
offline_thumbnail: ""
uuid: ca82186d-4525-43f1-9c7a-395138df6ed8
updated: 1484310597
title: penal
categories:
    - Dictionary
---
penal
     adj 1: of or relating to punishment; "penal reform"; "penal code"
     2: serving as or designed to impose punishment; "penal
        servitude"; "a penal colony"
     3: subject to punishment by law; "a penal offense" [syn: {punishable}]
