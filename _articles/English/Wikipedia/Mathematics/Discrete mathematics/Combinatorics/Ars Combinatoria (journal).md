---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ars_Combinatoria_(journal)
offline_file: ""
offline_thumbnail: ""
uuid: 508c1565-5429-431c-8fde-ebbe6b83b5b0
updated: 1484308562
title: Ars Combinatoria (journal)
categories:
    - Combinatorics
---
Ars Combinatoria, a Canadian Journal of Combinatorics is an English language research journal in combinatorics, issued in Winnipeg, Manitoba, ISSN 0381-7032.
