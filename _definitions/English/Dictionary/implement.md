---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/implement
offline_file: ""
offline_thumbnail: ""
uuid: b3e932e7-3949-47cf-a580-c46c4df90d41
updated: 1484310464
title: implement
categories:
    - Dictionary
---
implement
     n : instrumentation (a piece of equipment or tool) used to
         effect an end
     v 1: apply in a manner consistent with its purpose or design;
          "implement a procedure"
     2: ensure observance of laws and rules; "Apply the rules to
        everyone"; [syn: {enforce}, {apply}] [ant: {exempt}]
     3: pursue to a conclusion or bring to a successful issue; "Did
        he go through with the treatment?"; "He implemented a new
        economic plan"; "She followed up his recommendations with
        a written proposal" [syn: {follow through}, {follow up}, {follow
        out}, {carry out}, {put through}, {go through}]
