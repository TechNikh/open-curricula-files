---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toss
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484384221
title: toss
categories:
    - Dictionary
---
toss
     n 1: the act of flipping a coin [syn: {flip}]
     2: (sports) the act of throwing the ball to another member of
        your team; "the pass was fumbled" [syn: {pass}, {flip}]
     3: an abrupt movement; "a toss of his head"
     v 1: throw or toss with a light motion; "flip me the beachball";
          "toss me newspaper" [syn: {flip}, {sky}, {pitch}]
     2: lightly throw to see which side comes up; "I don't know what
        to do--I may as well flip a coin!" [syn: {flip}]
     3: throw carelessly; "chuck the ball" [syn: {chuck}]
     4: move or stir about violently; "The feverish patient thrashed
        around in his bed" [syn: {convulse}, {thresh}, {thresh
        about}, ...
