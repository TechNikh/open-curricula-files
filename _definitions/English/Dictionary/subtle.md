---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subtle
offline_file: ""
offline_thumbnail: ""
uuid: 42e4c657-6270-4579-817a-0493d2c57cf4
updated: 1484310307
title: subtle
categories:
    - Dictionary
---
subtle
     adj 1: be difficult to detect or grasp by the mind; "his whole
            attitude had undergone a subtle change"; "a subtle
            difference"; "that elusive thing the soul" [syn: {elusive}]
     2: faint and difficult to analyze; "subtle aromas"
     3: able to make fine distinctions; "a subtle mind"
     4: working or spreading in a hidden and usually injurious way;
        "glaucoma is an insidious disease"; "a subtle poison"
        [syn: {insidious}, {pernicious}]
