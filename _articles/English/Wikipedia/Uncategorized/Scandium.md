---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scandium
offline_file: ""
offline_thumbnail: ""
uuid: 5e9a2f04-7e9a-49f5-8294-e94c746354d4
updated: 1484308383
title: Scandium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Scandium_sublimed_dendritic_and_1cm3_cube.jpg
tags:
    - Properties
    - Chemical characteristics of the element
    - Isotopes
    - Occurrence
    - Production
    - Compounds
    - Oxides and hydroxides
    - Halides and pseudohalides
    - Organic derivatives
    - Uncommon oxidation states
    - History
    - Applications
    - Health and safety
categories:
    - Uncategorized
---
Scandium is a chemical element with symbol Sc and atomic number 21. A silvery-white metallic d-block element, it has historically been sometimes classified as a rare earth element, together with yttrium and the lanthanides. It was discovered in 1879 by spectral analysis of the minerals euxenite and gadolinite from Scandinavia.
