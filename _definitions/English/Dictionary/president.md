---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/president
offline_file: ""
offline_thumbnail: ""
uuid: 74eeb405-b761-401b-8a84-627624b9ba26
updated: 1484310429
title: president
categories:
    - Dictionary
---
president
     n 1: an executive officer of a firm or corporation
     2: the person who holds the office of head of state of the
        United States government; "the President likes to jog
        every morning" [syn: {President of the United States}, {United
        States President}, {Chief Executive}]
     3: the chief executive of a republic
     4: the officer who presides at the meetings of an organization;
        "address your remarks to the chairperson" [syn: {chairman},
         {chairwoman}, {chair}, {chairperson}]
     5: the head administrative officer of a college or university
        [syn: {prexy}]
     6: the office of the United States head of state; "a President
       ...
