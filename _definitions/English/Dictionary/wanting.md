---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wanting
offline_file: ""
offline_thumbnail: ""
uuid: 642c1eba-f63c-4f01-99a3-9fb2cab79253
updated: 1484310174
title: wanting
categories:
    - Dictionary
---
wanting
     adj 1: not existing; "innovation has been sadly lacking";
            "character development is missing from the book" [syn:
             {lacking(p)}, {missing}, {nonexistent}, {wanting(a)}]
     2: inadequate in amount or degree; "a deficient education";
        "deficient in common sense"; "lacking in stamina"; "tested
        and found wanting" [syn: {deficient}, {lacking(p)}, {wanting(p)}]
