---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bulgarian_solitaire
offline_file: ""
offline_thumbnail: ""
uuid: fcf37972-47f1-4038-adfa-f64524d1d24f
updated: 1484309020
title: Bulgarian solitaire
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Bulgaria.svg.png
categories:
    - Combinatorial game theory
---
In the game, a pack of 
  
    
      
        N
      
    
    {\displaystyle N}
  
 cards is divided into several piles. Then for each pile, remove one card; collect the removed cards together to form a new pile (piles of zero size are ignored).
