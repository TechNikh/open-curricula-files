---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565241
title: furious
categories:
    - Dictionary
---
furious
     adj 1: marked by extreme and violent energy; "a ferocious beating";
            "fierce fighting"; "a furious battle" [syn: {ferocious},
             {fierce}, {savage}]
     2: marked by extreme anger; "the enraged bull attached";
        "furious about the accident"; "a furious scowl";
        "infuriated onlookers charged the police who were beating
        the boy"; "could not control the maddened crowd" [syn: {angered},
         {enraged}, {infuriated}, {maddened}]
     3: (of the elements) as if showing violent anger; "angry clouds
        on the horizon"; "furious winds"; "the raging sea" [syn: {angry},
         {raging}, {tempestuous}, {wild}]
