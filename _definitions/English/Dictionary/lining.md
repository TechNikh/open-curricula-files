---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lining
offline_file: ""
offline_thumbnail: ""
uuid: 0004ce4a-a97f-4aa2-acad-add3b72c0b49
updated: 1484310327
title: lining
categories:
    - Dictionary
---
lining
     n 1: a protective covering that protects an inside surface
     2: a piece of cloth that is used as the inside surface of a
        garment [syn: {liner}]
     3: providing something with a surface of a different material
        [syn: {facing}]
     4: the act of attaching an inside lining (to a garment or
        curtain etc.)
