---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Micro_perforated_plate
offline_file: ""
offline_thumbnail: ""
uuid: 01df0bca-c96d-491e-9c3c-3f132f5e55f3
updated: 1484308894
title: Micro perforated plate
tags:
    - Structure
    - Operating principle
    - Comparison with other materials
    - History
categories:
    - Waves
---
A Micro Perforated Plate (MPP) is a device used to absorb sound, reducing its intensity. It consists of a thin flat plate, made from one of several different materials, with small holes punched in it. An MPP offers an alternative to traditional sound absorbers made from porous materials.
