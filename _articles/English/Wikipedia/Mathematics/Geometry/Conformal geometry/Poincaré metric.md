---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_metric'
offline_file: ""
offline_thumbnail: ""
uuid: 2d0b70a8-4fe9-4e26-a5b5-22932633394f
updated: 1484309185
title: Poincaré metric
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-J-inv-modulus.jpeg
tags:
    - Overview of metrics on Riemann surfaces
    - Metric and volume element on the Poincaré plane
    - Conformal map of plane to disk
    - Metric and volume element on the Poincaré disk
    - The punctured disk model
    - Schwarz lemma
categories:
    - Conformal geometry
---
In mathematics, the Poincaré metric, named after Henri Poincaré, is the metric tensor describing a two-dimensional surface of constant negative curvature. It is the natural metric commonly used in a variety of calculations in hyperbolic geometry or Riemann surfaces.
