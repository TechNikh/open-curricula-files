---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inter
offline_file: ""
offline_thumbnail: ""
uuid: 4fd71f50-f116-4f3d-b3b2-9ab9ca280d78
updated: 1484310359
title: inter
categories:
    - Dictionary
---
inter
     v : place in a grave or tomb; "Stalin was buried behind the
         Kremlin wall on Red Square"; "The pharaos were entombed
         in the pyramids"; "My grandfather was laid to rest last
         Sunday" [syn: {bury}, {entomb}, {inhume}, {lay to rest}]
     [also: {interring}, {interred}]
