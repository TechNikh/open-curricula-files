---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indigenous_bundle
offline_file: ""
offline_thumbnail: ""
uuid: b2cb707a-6350-42ab-b303-00d13f84b57f
updated: 1484309363
title: Indigenous bundle
categories:
    - Geometry stubs
---
In mathematics, an indigenous bundle on a Riemann surface is a fiber bundle with a flat connection associated to some complex projective structure. Indigenous bundles were introduced by Gunning (1967). Indigenous bundles for curves over p-adic fields were introduced by Mochizuki (1996) in his study of p-adic Teichmüller theory.
