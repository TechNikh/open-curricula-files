---
version: 1
type: video
provider: YouTube
id: 9_FkOcpJg1A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2c4d9ec3-1a6f-45d8-95c8-bad858bff777
updated: 1486069636
title: Mathematical Analysis in Young’s Double Slit Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/9_FkOcpJg1A/default.jpg
    - https://i3.ytimg.com/vi/9_FkOcpJg1A/1.jpg
    - https://i3.ytimg.com/vi/9_FkOcpJg1A/2.jpg
    - https://i3.ytimg.com/vi/9_FkOcpJg1A/3.jpg
---
