---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tilled
offline_file: ""
offline_thumbnail: ""
uuid: a5a9b22f-a638-4a51-8bcc-a0540b18f3ec
updated: 1484310541
title: tilled
categories:
    - Dictionary
---
tilled
     adj : turned or stirred by plowing or harrowing or hoeing; "tilled
           land ready for seed"
