---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oct
offline_file: ""
offline_thumbnail: ""
uuid: 0c6b5625-6d3a-4e55-9d3b-4bc3d10815a4
updated: 1484310423
title: oct
categories:
    - Dictionary
---
Oct
     n : the month following September and preceding November [syn: {October}]
