---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worthy
offline_file: ""
offline_thumbnail: ""
uuid: 597ed4ab-fff0-4c89-a7df-c4500cc3fef6
updated: 1484310583
title: worthy
categories:
    - Dictionary
---
worthy
     adj 1: having worth or merit or value; being honorable or
            admirable; "a worthy fellow"; "no student deemed
            worthy, and chosen for admission, would be kept out
            for lack of funds"- Nathan Pusey; "worthy of acclaim";
            "orthy of consideration"; "a worthy cause" [ant: {unworthy}]
     2: worthy of being chosen especially as a spouse; "the parents
        found the girl suitable for their son" [syn: {desirable},
        {suitable}]
     3: meriting respect or esteem; "the worthy gentleman"
     4: having high moral qualities; "a noble spirit"; "a solid
        citizen"; "an upstanding man"; "a worthy successor" [syn:
        {noble}, ...
