---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coronary_artery
offline_file: ""
offline_thumbnail: ""
uuid: 66429006-987a-47d2-8848-b54272cceb4b
updated: 1484308366
title: Coronary circulation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/379px-Coronary_arteries.svg.png
tags:
    - Structure
    - Anastomoses
    - Variation
    - Coronary artery dominance
    - Function
    - Supply to papillary muscles
    - Changes in diastole
    - Changes in oxygen demand
    - Branches
    - Additional images
categories:
    - Uncategorized
---
Coronary circulation is the circulation of blood in the blood vessels of the heart muscle myocardium known as coronary arteries. The vessels that remove the deoxygenated blood from the heart muscle are known as cardiac veins. These include the great cardiac vein, the middle cardiac vein, the small cardiac vein and the anterior cardiac veins.
