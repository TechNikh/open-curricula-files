---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degradation
offline_file: ""
offline_thumbnail: ""
uuid: 782b9430-b718-448a-be0c-10f4ba8d865b
updated: 1484310384
title: degradation
categories:
    - Dictionary
---
degradation
     n 1: changing to a lower state (a less respected state) [syn: {debasement}]
     2: a low or downcast state; "each confession brought her into
        an attitude of abasement"- H.L.Menchken [syn: {abasement},
         {abjection}]
