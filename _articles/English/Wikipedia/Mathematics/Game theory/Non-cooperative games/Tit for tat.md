---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tit_for_tat
offline_file: ""
offline_thumbnail: ""
uuid: 60670c7a-3a94-483e-818f-35575542b558
updated: 1484309059
title: Tit for tat
tags:
    - Meaning and etymology
    - In biology
    - In social psychology
    - In business
    - In game theory
    - Background
    - Problems
    - Real world use
    - Explaining reciprocal altruism in animal communities
    - Explaining war
    - Peer-to-peer file sharing
categories:
    - Non-cooperative games
---
Tit for tat (or tit-for-tat) is an English saying dating to 1556, from "tip for tap", meaning "blow for blow," i.e., retaliation in kind—or more broadly, an equivalent to an action given in return. It has related meanings and use as a concept in biology, social psychology, business, as well as in the mathematical area of game theory. The concept in its various forms has found use in the real world in attempting to explain a form of reciprocated altruism in animal communities, and as a strategy for managing activities in technology areas.
