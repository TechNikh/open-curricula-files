---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grassmann%E2%80%93Cayley_algebra'
offline_file: ""
offline_thumbnail: ""
uuid: 89c5d4db-d2d9-4ef4-ad93-871b9230b800
updated: 1484309363
title: Grassmann–Cayley algebra
categories:
    - Geometry stubs
---
Grassmann–Cayley algebra, also known as double algebra, is a form of modeling algebra for use in projective geometry.[1] The technique is based on work by German mathematician Hermann Grassmann on exterior algebra, and subsequently by British mathematician Arthur Cayley's work on matrices and linear algebra.
