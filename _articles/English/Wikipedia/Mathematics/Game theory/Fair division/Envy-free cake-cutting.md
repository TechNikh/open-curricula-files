---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Envy-free_cake-cutting
offline_file: ""
offline_thumbnail: ""
uuid: a6307c36-21b0-4f1a-8f4b-966d34f2948f
updated: 1484308527
title: Envy-free cake-cutting
tags:
    - Short history
    - Connected pieces
    - Existence proof
    - Procedures
    - Hardness result
    - Approximations
    - Variants
    - Disconnected pieces
    - Procedures
    - Approximations and partial solutions
    - Hardness result
    - Existence proofs and variants
    - Weighted-envy-free division
    - Envy-free chore division
    - Summary tables
categories:
    - Fair division
---
An envy-free cake-cutting is a kind of fair cake-cutting. It is a division of a heterogeneous resource ("cake") that satisfies the envy-free criterion, namely, that every partner feels that their allocated share is at least as good as any other share, according to their own subjective valuation.
