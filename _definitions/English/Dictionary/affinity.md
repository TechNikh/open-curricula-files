---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affinity
offline_file: ""
offline_thumbnail: ""
uuid: 6433dea4-545b-4207-b6e9-4b1a7cb575f4
updated: 1484310401
title: affinity
categories:
    - Dictionary
---
affinity
     n 1: the force attracting atoms to each other and binding them
          together in a molecule; "basic dyes have an affinity for
          wool and silk" [syn: {chemical attraction}]
     2: (immunology) the attraction between an antigen and an
        antibody
     3: kinship by marriage or adoption; not a blood relationship
        [syn: {kinship by marriage}] [ant: {consanguinity}]
     4: (biology) state of relationship between organisms or groups
        of organisms resulting in resemblance in structure or
        structural parts; "in anatomical structure prehistoric man
        shows close affinity with modern humans" [syn: {phylogenetic
        relation}]
     5: a ...
