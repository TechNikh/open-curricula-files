---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-help
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484426281
title: self-help
categories:
    - Dictionary
---
self-help
     n : the act of helping or improving yourself without relying on
         anyone else
