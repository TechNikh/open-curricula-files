---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aspiring
offline_file: ""
offline_thumbnail: ""
uuid: 49330900-0a17-41fc-9f4e-caeac5674424
updated: 1484310486
title: aspiring
categories:
    - Dictionary
---
aspiring
     adj 1: seeking advancement or recognition [syn: {aspirant}, {aspiring(p)},
             {wishful}, {would-be(a)}]
     2: desiring or striving for recognition or advancement [syn: {aspirant},
         {aspiring(a)}]
