---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ran
offline_file: ""
offline_thumbnail: ""
uuid: 179bd1b9-470a-469c-8e9b-08fe3995f235
updated: 1484310611
title: ran
categories:
    - Dictionary
---
run
     n 1: a score in baseball made by a runner touching all four bases
          safely; "the Yankees scored 3 runs in the bottom of the
          9th"; "their first tally came in the 3rd inning" [syn: {tally}]
     2: the act of testing something; "in the experimental trials
        the amount of carbon was measured separately"; "he called
        each flip of the coin a new trial" [syn: {test}, {trial}]
     3: a race run on foot; "she broke the record for the half-mile
        run" [syn: {footrace}, {foot race}]
     4: an unbroken series of events; "had a streak of bad luck";
        "Nicklaus had a run of birdies" [syn: {streak}]
     5: (American football) a play in which a player ...
