---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Szemer%C3%A9di_regularity_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: c3f92c3c-7595-4ad7-8a35-96fe47f4cb2a
updated: 1484309456
title: Szemerédi regularity lemma
tags:
    - Statement
    - Extensions
    - Additional reading
categories:
    - Theorems in graph theory
---
In mathematics, the Szemerédi regularity lemma states that every large enough graph can be divided into subsets of about the same size so that the edges between different subsets behave almost randomly. Szemerédi (1975) introduced a weaker version of this lemma, restricted to bipartite graphs, in order to prove Szemerédi's theorem, and in (Szemerédi 1978) he proved the full lemma. Extensions of the regularity method to hypergraphs were obtained by Rödl and his collaborators[1][2][3] and Gowers.[4][5]
