---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wish
offline_file: ""
offline_thumbnail: ""
uuid: f0ee051e-3961-42cb-b3f1-8ab3b648dac6
updated: 1484310462
title: wish
categories:
    - Dictionary
---
wish
     n 1: a specific feeling of desire; "he got his wish"; "he was
          above all wishing and desire" [syn: {wishing}, {want}]
     2: an expression of some desire or inclination; "I could tell
        that it was his wish that the guests leave"; "his crying
        was an indirect request for attention" [syn: {indirect
        request}]
     3: (usually plural) a polite expression of desire for someone's
        welfare; "give him my kind regards"; "my best wishes"
        [syn: {regard}, {compliments}]
     4: the particular preference that you have; "it was his last
        wish"; "they should respect the wishes of the people"
     v 1: hope for; have a wish; "I wish I could go ...
