---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Steinitz%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 87e8f86b-312b-46ef-af1c-add35857be1f
updated: 1484309242
title: "Steinitz's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Schegel_diagram_as_shadow.png
tags:
    - Definitions and statement of the theorem
    - History and naming
    - Proofs
    - Induction
    - Lifting
    - Circle packing
    - Realizations with additional properties
    - Integer coordinates
    - Equal slopes
    - Specifying the shape of a face
    - Tangent spheres
    - Related results
categories:
    - Geometric graph theory
---
In polyhedral combinatorics, a branch of mathematics, Steinitz's theorem is a characterization of the undirected graphs formed by the edges and vertices of three-dimensional convex polyhedra: they are exactly the (simple) 3-vertex-connected planar graphs (with at least four vertices). That is, every convex polyhedron forms a 3-connected planar graph, and every 3-connected planar graph can be represented as the graph of a convex polyhedron. For this reason, the 3-connected planar graphs are also known as polyhedral graphs.[1] Branko Grünbaum has called this theorem “the most important and deepest known result on 3-polytopes.”[2]
