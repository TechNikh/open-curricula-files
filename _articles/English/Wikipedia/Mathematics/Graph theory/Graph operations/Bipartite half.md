---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bipartite_half
offline_file: ""
offline_thumbnail: ""
uuid: 667365c5-aa62-4930-81cf-ce9ed956436e
updated: 1484309448
title: Bipartite half
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Demi-4-cube.svg.png
categories:
    - Graph operations
---
In graph theory, the bipartite half or half-square of a bipartite graph G = (U,V,E) is a graph whose vertex set is one of the two sides of the bipartition (without loss of generality, U) and in which there is an edge uiuj for each two vertices ui and uj in U that are at distance two from each other in G.[1] That is, in a more compact notation, the bipartite half is G2[U] where the superscript 2 denotes the square of a graph and the square brackets denote an induced subgraph.
