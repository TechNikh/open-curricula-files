---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exact_differential
offline_file: ""
offline_thumbnail: ""
uuid: b0475e0c-f463-48fb-93a4-4a847e6fa238
updated: 1484308843
title: Exact differential
tags:
    - Overview
    - Definition
    - One dimension
    - Two and three dimensions
    - Partial differential relations
    - Reciprocity relation
    - Cyclic relation
    - >
        Some useful equations derived from exact differentials in
        two dimensions
categories:
    - Thermodynamics
---
In multivariate calculus, a differential is said to be exact or perfect, as contrasted with an inexact differential, if it is of the form dQ, for some differentiable function Q.
