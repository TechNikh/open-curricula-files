---
version: 1
type: video
provider: YouTube
id: SCoRGXssRPY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 278af908-084f-458f-bea4-8877550da430
updated: 1486069617
title: Addition with regrouping
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/SCoRGXssRPY/default.jpg
    - https://i3.ytimg.com/vi/SCoRGXssRPY/1.jpg
    - https://i3.ytimg.com/vi/SCoRGXssRPY/2.jpg
    - https://i3.ytimg.com/vi/SCoRGXssRPY/3.jpg
---
