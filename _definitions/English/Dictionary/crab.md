---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crab
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587261
title: crab
categories:
    - Dictionary
---
crab
     n 1: decapod having eyes on short stalks and a broad flattened
          carapace with a small abdomen folded under the thorax
          and pincers
     2: a quarrelsome grouch [syn: {crabby person}]
     3: (astrology) a person who is born while the sun is in Cancer
        [syn: {Cancer}]
     4: the fourth sign of the zodiac; the sun is in this sign from
        about June 21 to July 22 [syn: {Cancer}, {Cancer the Crab}]
     5: the edible flesh of any of various crabs [syn: {crabmeat}]
     6: infests the pubic region of the human body [syn: {crab louse},
         {pubic louse}, {Phthirius pubis}]
     7: a stroke of the oar that either misses the water or digs too
        ...
