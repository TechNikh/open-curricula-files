---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proportional_cake-cutting
offline_file: ""
offline_thumbnail: ""
uuid: 1664ae4b-c09b-4953-8a45-f0dcc571a552
updated: 1484308520
title: Proportional cake-cutting
tags:
    - Procedures
    - Simple procedures
    - Recursive halving
    - Selection procedures
    - Randomized versions
    - Hardness results
    - Variants
    - Different entitlements
    - Super-proportional division
    - Adjacency constraint
    - Economically efficient division
    - Truthful division
    - Proportional division of chores
categories:
    - Fair division
---
A proportional cake-cutting is a kind of fair cake-cutting. It is a division of a heterogeneous resource ("cake") that satisfies the proportionality criterion, namely, that every partner feels that his allocated share is worth at least 1/n of the total.
