---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underprivileged
offline_file: ""
offline_thumbnail: ""
uuid: eac55fe6-9179-4cc3-82a4-5dbb43a70e06
updated: 1484310170
title: underprivileged
categories:
    - Dictionary
---
underprivileged
     adj : lacking the rights and advantages of other members of
           society [ant: {privileged}]
