---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fullerene
offline_file: ""
offline_thumbnail: ""
uuid: 95940fe4-415d-4c48-8d73-82d41b3485f7
updated: 1484310416
title: fullerene
categories:
    - Dictionary
---
fullerene
     n : a form of carbon having a large molecule consisting of an
         empty cage of sixty or more carbon atoms
