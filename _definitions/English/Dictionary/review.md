---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/review
offline_file: ""
offline_thumbnail: ""
uuid: f829f4be-44b2-4cde-87bf-3ff01cf94c96
updated: 1484310236
title: review
categories:
    - Dictionary
---
review
     n 1: a new appraisal or evaluation [syn: {reappraisal}, {revaluation},
           {reassessment}]
     2: an essay or article that gives a critical evaluation (as of
        a book or play) [syn: {critique}, {critical review}, {review
        article}]
     3: a subsequent examination of a patient for the purpose of
        monitoring earlier treatment [syn: {follow-up}, {followup},
         {reexamination}]
     4: (accounting) a service (less exhaustive than an audit) that
        provides some assurance to interested parties as to the
        reliability of financial data [syn: {limited review}]
     5: a variety show with topical sketches and songs and dancing
        and ...
