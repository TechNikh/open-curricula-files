---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seal
offline_file: ""
offline_thumbnail: ""
uuid: 45ccfdd0-8117-4104-a9d3-6aa2478b10b8
updated: 1484310595
title: seal
categories:
    - Dictionary
---
seal
     n 1: fastener consisting of a resinous composition that is
          plastic when warm; used for sealing documents and
          parcels and letters [syn: {sealing wax}]
     2: a device incised to make an impression; used to secure a
        closing or to authenticate documents [syn: {stamp}]
     3: the pelt or fur (especially the underfur) of a seal; "a coat
        of seal" [syn: {sealskin}]
     4: a member of a Naval Special Warfare unit who is trained for
        unconventional warfare; "SEAL is an acronym for Sea Air
        and Land" [syn: {Navy SEAL}]
     5: a stamp affixed to a document (as to attest to its
        authenticity or to seal it); "the warrant bore the
    ...
