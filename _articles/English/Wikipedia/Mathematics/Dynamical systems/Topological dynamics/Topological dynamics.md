---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_dynamics
offline_file: ""
offline_thumbnail: ""
uuid: 8a9528c6-ed6a-4bfc-a794-1673e3091119
updated: 1484308872
title: Topological dynamics
categories:
    - Topological dynamics
---
In mathematics, topological dynamics is a branch of the theory of dynamical systems in which qualitative, asymptotic properties of dynamical systems are studied from the viewpoint of general topology.
