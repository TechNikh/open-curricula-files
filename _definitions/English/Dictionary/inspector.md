---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inspector
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484589301
title: inspector
categories:
    - Dictionary
---
inspector
     n 1: a high ranking police officer
     2: an investigator who observes carefully; "the examiner
        searched for clues" [syn: {examiner}]
