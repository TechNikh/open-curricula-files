---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alarm
offline_file: ""
offline_thumbnail: ""
uuid: 1c8a52a8-614d-46b4-b6d8-18948fefd6c6
updated: 1484310366
title: alarm
categories:
    - Dictionary
---
alarm
     n 1: fear resulting from the awareness of danger [syn: {dismay},
          {consternation}]
     2: a device that signals the occurrence of some undesirable
        event [syn: {warning device}, {alarm system}]
     3: an automatic signal (usually a sound) warning of danger
        [syn: {alert}, {warning signal}, {alarum}]
     4: a clock that wakes sleeper at preset time [syn: {alarm clock}]
     v 1: fill with apprehension or alarm; cause to be unpleasantly
          surprised; "I was horrified at the thought of being late
          for my interview"; "The news of the executions horrified
          us" [syn: {dismay}, {appal}, {appall}, {horrify}]
     2: warn or arouse to a ...
