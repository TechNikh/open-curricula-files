---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mouse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423221
title: mouse
categories:
    - Dictionary
---
mouse
     n 1: any of numerous small rodents typically resembling
          diminutive rats having pointed snouts and small ears on
          elongated bodies with slender usually hairless tails
     2: a hand-operated electronic device that controls the
        coordinates of a cursor on your computer screen as you
        move it around on a pad; on the bottom of the mouse is a
        ball that rolls on the surface of the pad; "a mouse takes
        much more room than a trackball" [syn: {computer mouse}]
     v 1: to go stealthily or furtively; "..stead of sneaking around
          spying on the neighbor's house" [syn: {sneak}, {creep},
          {steal}, {pussyfoot}]
     2: ...
