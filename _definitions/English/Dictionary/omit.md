---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/omit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484594521
title: omit
categories:
    - Dictionary
---
omit
     v 1: prevent from being included or considered or accepted; "The
          bad results were excluded from the report"; "Leave off
          the top piece" [syn: {exclude}, {except}, {leave out}, {leave
          off}, {take out}] [ant: {include}]
     2: leave undone or leave out; "How could I miss that typo?";
        "The workers on the conveyor belt miss one out of ten"
        [syn: {neglect}, {pretermit}, {drop}, {miss}, {leave out},
         {overlook}, {overleap}] [ant: {attend to}]
     [also: {omitting}, {omitted}]
