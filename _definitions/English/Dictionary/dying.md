---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dying
offline_file: ""
offline_thumbnail: ""
uuid: 28573ca3-b0e0-4188-85f5-f7d551b42eb6
updated: 1484310445
title: dying
categories:
    - Dictionary
---
die
     n 1: small cubes with 1 to 6 spots on the faces; used to generate
          random numbers [syn: {dice}]
     2: a device used for shaping metal
     3: a cutting tool that is fitted into a diestock and used for
        cutting male (external) screw threads on screws or bolts
        or pipes or rods
     v 1: pass from physical life and lose all all bodily attributes
          and functions necessary to sustain life; "She died from
          cancer"; "They children perished in the fire"; "The
          patient went peacefully" [syn: {decease}, {perish}, {go},
           {exit}, {pass away}, {expire}, {pass}] [ant: {be born}]
     2: suffer or face the pain of death; "Martyrs may ...
