---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/item
offline_file: ""
offline_thumbnail: ""
uuid: baa3d8c1-ef8b-4321-ad36-a8b6c5c5a3be
updated: 1484310522
title: item
categories:
    - Dictionary
---
item
     n 1: a distinct part that can be specified separately in a group
          of things that could be enumerated on a list; "he
          noticed an item in the New York Times"; "she had several
          items on her shopping list"; "the main point on the
          agenda was taken up first" [syn: {point}]
     2: a whole individual unit; especially when included in a list
        or collection; "they reduced the price on many items"
     3: a small part that can be considered separately from the
        whole; "it was perfect in all details" [syn: {detail}, {particular}]
     4: an isolated fact that is considered separately from the
        whole; "several of the details are ...
