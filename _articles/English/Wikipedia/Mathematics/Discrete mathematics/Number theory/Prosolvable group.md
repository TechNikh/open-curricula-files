---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prosolvable_group
offline_file: ""
offline_thumbnail: ""
uuid: ee8ae10b-e993-4b36-b9f8-ce49b7e11176
updated: 1484308654
title: Prosolvable group
categories:
    - Number theory
---
In mathematics, more precisely in algebra, a prosolvable group (less common: prosoluble group) is a group that is isomorphic to the inverse limit of an inverse system of solvable groups. Equivalently, a group is called prosolvable, if, viewed as a topological group, every open neighborhood of the identity contains a normal subgroup whose corresponding quotient group is a solvable group.
