---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hemisphere
offline_file: ""
offline_thumbnail: ""
uuid: e9a279c0-c027-411e-82f2-57818abbf71c
updated: 1484310433
title: hemisphere
categories:
    - Dictionary
---
hemisphere
     n 1: half of the terrestrial globe
     2: half of a sphere
     3: either half of the cerebrum [syn: {cerebral hemisphere}]
