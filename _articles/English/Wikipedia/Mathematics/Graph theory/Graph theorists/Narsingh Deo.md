---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Narsingh_Deo
offline_file: ""
offline_thumbnail: ""
uuid: 1eb314b8-ae0c-4cd6-9948-1a9b88aac642
updated: 1484309461
title: Narsingh Deo
categories:
    - Graph theorists
---
Narsingh Deo is a professor and Charles N. Millican Endowed Chair of the Department of Computer Science, University of Central Florida. He received his Ph.D. for his dessertation 'Topological Analysis of Active Networks and Generalization of Hamiltonian tree' from Northwestern University, IL., in 1965; S. L. Hakimi was his advisor. Deo was professor at the Indian Institute of Technology, Kanpur.
