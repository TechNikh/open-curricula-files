---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 7702065f-e979-4d30-b4a1-20b31184bffa
updated: 1484308832
title: Thermodynamics
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Carnot_engine_%2528hot_body_-_working_body_-_cold_body%2529.jpg'
tags:
    - Introduction
    - History
    - Etymology
    - Branches of description
    - Classical thermodynamics
    - Statistical mechanics
    - Chemical thermodynamics
    - Treatment of equilibrium
    - Laws of thermodynamics
    - System models
    - States and processes
    - Instrumentation
    - Conjugate variables
    - Potentials
    - Applied fields
    - Lists and timelines
    - Wikibooks
categories:
    - Thermodynamics
---
Thermodynamics is a branch of science concerned with heat and temperature and their relation to energy and work. The behavior of these quantities is governed by the four laws of thermodynamics, irrespective of the composition or specific properties of the material or system in question. The laws of thermodynamics are explained in terms of microscopic constituents by statistical mechanics. Thermodynamics applies to a wide variety of topics in science and engineering, especially physical chemistry, chemical engineering and mechanical engineering.
