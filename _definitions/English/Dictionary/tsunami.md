---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tsunami
offline_file: ""
offline_thumbnail: ""
uuid: ac26dd6f-ef43-4d9c-82f9-3ab510fc336a
updated: 1484310439
title: tsunami
categories:
    - Dictionary
---
tsunami
     n : a huge destructive wave (especially one caused by an
         earthquake) [syn: {tidal wave}]
