---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disease
offline_file: ""
offline_thumbnail: ""
uuid: 58d8fc0c-ac89-4939-acb5-adbdcf62af10
updated: 1484310293
title: disease
categories:
    - Dictionary
---
disease
     n : an impairment of health or a condition of abnormal
         functioning
