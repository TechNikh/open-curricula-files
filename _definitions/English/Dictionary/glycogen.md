---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glycogen
offline_file: ""
offline_thumbnail: ""
uuid: f0fdf7e5-7f12-4de6-bd03-461496ad59e4
updated: 1484310160
title: glycogen
categories:
    - Dictionary
---
glycogen
     n : one form in which body fuel is stored; stored primarily in
         the liver and broken down into glucose when needed by the
         body [syn: {animal starch}]
