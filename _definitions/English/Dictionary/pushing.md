---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pushing
offline_file: ""
offline_thumbnail: ""
uuid: 949d3bb3-2da1-44b2-995b-7f73a30c98b8
updated: 1484310335
title: pushing
categories:
    - Dictionary
---
pushing
     adj : marked by aggressive ambition and energy and initiative; "an
           aggressive young exective"; "a pushful insurance
           agent"; "a pushing youth intent on getting on in the
           world" [syn: {aggressive}, {enterprising}, {pushful}, {pushy}]
     n : the act of applying force in order to move something away;
         "he gave the door a hard push"; "the pushing is good
         exercise" [syn: {push}]
