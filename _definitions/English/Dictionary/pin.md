---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pin
offline_file: ""
offline_thumbnail: ""
uuid: 13a4c962-a40e-4081-a6b8-fdd1e53319e8
updated: 1484310220
title: pin
categories:
    - Dictionary
---
pin
     n 1: a piece of jewelry that is pinned onto the wearer's garment
     2: when a wrestler's shoulders are forced to the mat [syn: {fall}]
     3: small markers inserted into a surface to mark scores or
        define locations etc. [syn: {peg}]
     4: a number you choose and use to gain access to various
        accounts [syn: {personal identification number}, {PIN
        number}]
     5: informal terms of the leg; "fever left him weak on his
        sticks" [syn: {peg}, {stick}]
     6: axis consisting of a short shaft that supports something
        that turns [syn: {pivot}]
     7: cylindrical tumblers consisting of two parts that are held
        in place by springs; when they ...
