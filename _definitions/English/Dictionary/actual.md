---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/actual
offline_file: ""
offline_thumbnail: ""
uuid: b26fae73-e0df-432b-9581-bfc7eb5a293d
updated: 1484310284
title: actual
categories:
    - Dictionary
---
actual
     adj 1: presently existing in fact and not merely potential or
            possible; "the predicted temperature and the actual
            temperature were markedly different"; "actual and
            imagined conditions" [syn: {existent}] [ant: {potential}]
     2: taking place in reality; not pretended or imitated; "we saw
        the actual wedding on television"; "filmed the actual
        beating"
     3: being or reflecting the essential or genuine character of
        something; "her actual motive"; "a literal solitude like a
        desert"- G.K.Chesterton; "a genuine dilemma" [syn: {genuine},
         {literal}, {real}]
     4: of the nature of fact; having actual ...
