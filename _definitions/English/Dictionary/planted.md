---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/planted
offline_file: ""
offline_thumbnail: ""
uuid: 8ee03ac4-9b61-427b-8cf0-d2b5d911a901
updated: 1484310254
title: planted
categories:
    - Dictionary
---
planted
     adj 1: (used especially of ideas or principles) deeply rooted;
            firmly fixed or held; "deep-rooted prejudice";
            "deep-seated differences of opinion"; "implanted
            convictions"; "ingrained habits of a lifetime"; "a
            deeply planted need" [syn: {deep-rooted}, {deep-seated},
             {implanted}, {ingrained}]
     2: set in the soil for growth [ant: {unplanted}]
