---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fringe
offline_file: ""
offline_thumbnail: ""
uuid: 93dcb8b3-980f-40ed-865c-50e6af234367
updated: 1484310517
title: fringe
categories:
    - Dictionary
---
fringe
     n 1: the outside boundary or surface of something [syn: {periphery},
           {outer boundary}]
     2: a part of the city far removed from the center; "they built
        a factory on the outskirts of the city" [syn: {outskirt}]
     3: a social group holding marginal or extreme views; "members
        of the fringe believe we should be armed with guns at all
        times"
     4: edging consisting of hanging threads or tassels
     v 1: adorn with a fringe; "The weaver fringed the scarf"
     2: decorate with or as if with a surrounding fringe; "fur
        fringed the hem of the dress"
