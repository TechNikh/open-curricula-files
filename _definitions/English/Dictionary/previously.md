---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/previously
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320861
title: previously
categories:
    - Dictionary
---
previously
     adv : at an earlier time or formerly; "she had previously lived in
           Chicago"; "he was previously president of a bank";
           "better than anything previously proposed"; "a
           previously unquestioned attitude"; "antecedently
           arranged" [syn: {antecedently}]
