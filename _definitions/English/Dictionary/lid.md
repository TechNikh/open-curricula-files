---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lid
offline_file: ""
offline_thumbnail: ""
uuid: c47330c6-4a84-4ae1-b024-75861ccc0a24
updated: 1484310230
title: lid
categories:
    - Dictionary
---
lid
     n 1: either of two folds of skin that can be moved to cover or
          open the eye; "his lids would stay open no longer" [syn:
           {eyelid}, {palpebra}]
     2: a movable top or cover (hinged or separate) for closing the
        opening of a container
     3: headdress that protects the head from bad weather; has
        shaped crown and usually a brim [syn: {hat}, {chapeau}]
