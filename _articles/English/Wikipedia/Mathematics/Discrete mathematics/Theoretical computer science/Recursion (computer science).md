---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursion_(computer_science)
offline_file: ""
offline_thumbnail: ""
uuid: 5d897333-7f1e-45ef-956e-8dacee567a8e
updated: 1484308684
title: Recursion (computer science)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-RecursiveTree.JPG
tags:
    - Recursive functions and algorithms
    - Recursive data types
    - Inductively defined data
    - Coinductively defined data and corecursion
    - Types of recursion
    - Single recursion and multiple recursion
    - Indirect recursion
    - Anonymous recursion
    - Structural versus generative recursion
    - Recursive programs
    - Recursive procedures
    - Factorial
    - Greatest common divisor
    - Towers of Hanoi
    - Binary search
    - Recursive data structures (structural recursion)
    - Linked lists
    - Binary trees
    - Filesystem traversal
    - Implementation issues
    - Wrapper function
    - Short-circuiting the base case
    - Depth-first search
    - Hybrid algorithm
    - Recursion versus iteration
    - Expressive power
    - Performance issues
    - Stack space
    - Multiply recursive problems
    - Tail-recursive functions
    - Order of execution
    - Function 1
    - Function 2 with swapped lines
    - Time-efficiency of recursive algorithms
    - Shortcut rule (master theorem)
    - Recursive functions
    - Books
    - Notes and references
categories:
    - Theoretical computer science
---
Recursion in computer science is a method where the solution to a problem depends on solutions to smaller instances of the same problem (as opposed to iteration).[1] The approach can be applied to many types of problems, and recursion is one of the central ideas of computer science.[2]
