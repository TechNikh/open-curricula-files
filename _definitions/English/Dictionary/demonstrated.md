---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demonstrated
offline_file: ""
offline_thumbnail: ""
uuid: d3d56213-99c4-43b2-aad2-736f9083fe0a
updated: 1484310194
title: demonstrated
categories:
    - Dictionary
---
demonstrated
     adj : having been demonstrated or verified beyond doubt
