---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Institutional_model_theory
offline_file: ""
offline_thumbnail: ""
uuid: 672571fd-c18e-472a-8df3-31d5210f215a
updated: 1484308674
title: Institutional model theory
tags:
    - Overview
categories:
    - Theoretical computer science
---
Institutional model theory generalizes a large portion of first-order model theory to an arbitrary logical system.
