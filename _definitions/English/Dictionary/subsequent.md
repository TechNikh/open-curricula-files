---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsequent
offline_file: ""
offline_thumbnail: ""
uuid: d5a6adf3-f8a1-4e12-912a-5c986d303a62
updated: 1484310244
title: subsequent
categories:
    - Dictionary
---
subsequent
     adj : following in time or order; "subsequent developments" [ant:
           {antecedent}]
