---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/niacin
offline_file: ""
offline_thumbnail: ""
uuid: 188c18c4-abfc-4206-af1e-5a3dc9d8b799
updated: 1484310162
title: niacin
categories:
    - Dictionary
---
niacin
     n : a B vitamin essential for the normal function of the nervous
         system and the gastrointestinal tract [syn: {nicotinic
         acid}]
