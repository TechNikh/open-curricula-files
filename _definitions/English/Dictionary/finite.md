---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finite
offline_file: ""
offline_thumbnail: ""
uuid: 05a7cdae-5e5b-4554-befa-bf7974422282
updated: 1484310369
title: finite
categories:
    - Dictionary
---
finite
     adj 1: bounded or limited in magnitude or spatial or temporal
            extent [ant: {infinite}]
     2: of verbs; relating to forms of the verb that are limited in
        time by a tense and (usually) show agreement with number
        and person [ant: {infinite}]
