---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Standard_map
offline_file: ""
offline_thumbnail: ""
uuid: 9516ab65-9f14-4937-98d9-8ea25237c81d
updated: 1484308722
title: Standard map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Std-map-0.6.png
tags:
    - Physical model
    - Main properties
    - Circle map
    - Notes
categories:
    - Chaotic maps
---
The standard map (also known as the Chirikov–Taylor map or as the Chirikov standard map) is an area-preserving chaotic map from a square with side 
  
    
      
        2
        π
      
    
    {\displaystyle 2\pi }
  
 onto itself.[1] It is constructed by a Poincaré's surface of section of the kicked rotator, and is defined by:
