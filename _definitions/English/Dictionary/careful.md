---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/careful
offline_file: ""
offline_thumbnail: ""
uuid: 6cb410de-7b73-4550-946a-67313c04cdde
updated: 1484310303
title: careful
categories:
    - Dictionary
---
careful
     adj 1: exercising caution or showing care or attention; "they were
            careful when crossing the busy street"; "be careful to
            keep her shoes clean"; "did very careful research";
            "careful art restorers"; "careful of the rights of
            others"; "careful about one's behavior" [ant: {careless}]
     2: cautiously attentive; "careful of her feelings"; "heedful of
        his father's advice" [syn: {heedful}]
     3: with care and dignity; "walking at the same measured pace";
        "with all deliberate speed" [syn: {deliberate}, {measured}]
     4: full of cares or anxiety; "Thou art careful and troubled
        about many things"-Luke 10.41
  ...
