---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perron_number
offline_file: ""
offline_thumbnail: ""
uuid: 0cd48a11-1ff3-4e6b-bfc6-57499ddfcf99
updated: 1484309420
title: Perron number
categories:
    - Graph invariants
---
In mathematics, a Perron number is an algebraic integer α which is real and exceeds 1, but such that its conjugate elements, other than its complex conjugate, are all less than α in absolute value.[example needed]
