---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humiliated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421661
title: humiliated
categories:
    - Dictionary
---
humiliated
     adj 1: subdued or brought low in condition or status; "brought
            low"; "a broken man"; "his broken spirit" [syn: {broken},
             {crushed}, {humbled}, {low}]
     2: made to feel uncomfortable because of shame or wounded
        pride; "too embarrassed to say hello to his drunken father
        on the street"; "humiliated that his wife had to go out to
        work"; "felt mortified by the comparison with her sister"
        [syn: {embarrassed}, {mortified}]
