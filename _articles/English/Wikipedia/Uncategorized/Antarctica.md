---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antarctica
offline_file: ""
offline_thumbnail: ""
uuid: a7338136-1715-4d99-9f0d-2ce72638b76c
updated: 1484308333
title: Antarctica
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Antarctica_%2528orthographic_projection%2529.svg.png'
tags:
    - Etymology
    - History of exploration
    - geography
    - Geology
    - Geological history and paleontology
    - Palaeozoic era (540–250 Ma)
    - Mesozoic era (250–66 Ma)
    - Gondwana breakup (160–23 Ma)
    - Neogene Period (23–0.05 Ma)
    - Meyer Desert Formation biota
    - Present-day
    - Climate
    - population
    - biodiversity
    - Animals
    - Fungi
    - Plants
    - Other organisms
    - Conservation
    - Politics
    - Antarctic territories
    - Economy
    - Research
    - Meteorites
    - Ice mass and global sea level
    - Effects of global warming
    - Ozone depletion
    - Notes
categories:
    - Uncategorized
---
Antarctica (US English i/æntˈɑːrktɪkə/, UK English /ænˈtɑːktɪkə/ or /ænˈtɑːtɪkə/[note 1]) is Earth's southernmost continent. It contains the geographic South Pole and is situated in the Antarctic region of the Southern Hemisphere, almost entirely south of the Antarctic Circle, and is surrounded by the Southern Ocean. At 14,000,000 square kilometres (5,400,000 square miles), it is the fifth-largest continent. For comparison, Antarctica is nearly twice the size of Australia. About 98% of Antarctica is covered by ice that averages 1.9 km (1.2 mi; 6,200 ft) in thickness,[5] which extends to all but the northernmost reaches of the Antarctic Peninsula.
