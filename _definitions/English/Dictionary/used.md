---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/used
offline_file: ""
offline_thumbnail: ""
uuid: 282778ed-7794-4062-a962-baefb7394736
updated: 1484310279
title: used
categories:
    - Dictionary
---
used
     adj 1: employed in accomplishing something; "the principle of
            surprise is the most used and misused of all the
            principles of war"- H.H.Arnold & I.C.Eaker [ant: {misused}]
     2: of persons; taken advantage of; "after going out of his way
        to help his friend get the job he felt not appreciated but
        used" [syn: {exploited}, {ill-used}, {put-upon}, {victimized},
         {victimised}]
     3: previously used or owned by another; "bought a secondhand
        (or used) car" [syn: {secondhand}]
