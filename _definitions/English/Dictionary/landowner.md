---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landowner
offline_file: ""
offline_thumbnail: ""
uuid: 7c43c45d-be33-4031-b04b-81f4adb6e935
updated: 1484310467
title: landowner
categories:
    - Dictionary
---
landowner
     n : a holder or proprietor of land [syn: {landholder}, {property
         owner}]
