---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intolerant
offline_file: ""
offline_thumbnail: ""
uuid: 7a4002e4-a7c3-4829-9479-f24b4b9f9f8f
updated: 1484310597
title: intolerant
categories:
    - Dictionary
---
intolerant
     adj 1: unwilling to tolerate difference of opinion [ant: {tolerant}]
     2: narrow-minded about cherished opinions [syn: {illiberal}]
