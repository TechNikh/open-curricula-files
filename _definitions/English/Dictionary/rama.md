---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rama
offline_file: ""
offline_thumbnail: ""
uuid: d6aa2b52-d95a-4234-a0a6-7d2dc4ff8683
updated: 1484310258
title: rama
categories:
    - Dictionary
---
Rama
     n : avatar of Vishnu whose name is synonymous with God; any of
         three incarnations: Ramachandra or Parashurama or
         Balarama; "in Hindu folklore Rama is the epitome of
         chivalry and courage and obedience to sacred law"
