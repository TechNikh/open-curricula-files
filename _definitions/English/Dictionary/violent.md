---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violent
offline_file: ""
offline_thumbnail: ""
uuid: 9dd8297c-b904-41d3-9904-da93aa4dfdc6
updated: 1484310189
title: violent
categories:
    - Dictionary
---
violent
     adj 1: acting with or marked by or resulting from great force or
            energy or emotional intensity; "a violent attack"; "a
            violent person"; "violent feelings"; "a violent rage";
            "felt a violent dislike" [ant: {nonviolent}]
     2: effected by force or injury rather than natural causes; "a
        violent death"
     3: (of colors or sounds) intensely vivid or loud; "a violent
        clash of colors"; "her dress was a violent red"; "a
        violent noise"; "wild colors"; "wild shouts" [syn: {wild}]
     4: marked by extreme intensity of emotions or convictions;
        inclined to react violently; fervid; "fierce loyalty"; "in
        a tearing ...
