---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scale_of_temperature
offline_file: ""
offline_thumbnail: ""
uuid: d45e5128-44ec-4a36-9884-a2b7ffc6728e
updated: 1484308860
title: Scale of temperature
tags:
    - Formal description
    - Empirical scales
    - Ideal gas scale
    - International temperature scale of 1990
    - Celsius scale
    - Thermodynamic scale
    - Definition
    - Equality to ideal gas scale
    - Conversion table between the different temperature units
    - Notes and references
categories:
    - Thermodynamics
---
