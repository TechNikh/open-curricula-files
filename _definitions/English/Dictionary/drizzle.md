---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drizzle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437261
title: drizzle
categories:
    - Dictionary
---
drizzle
     n : very light rain; stronger than mist but less than a shower
         [syn: {mizzle}]
     v 1: rain lightly; "When it drizzles in summer, hiking can be
          pleasant" [syn: {mizzle}]
     2: moisten with fine drops; "drizzle the meat with melted
        butter" [syn: {moisten}]
