---
version: 1
type: video
provider: YouTube
id: 789aMeepbxI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Proof-%20d-dx%28sqrt%28x%29%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0526e8a5-fb01-42a3-9742-3eadf4a44e4d
updated: 1486069693
title: 'Proof- d/dx(sqrt(x))'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/789aMeepbxI/default.jpg
    - https://i3.ytimg.com/vi/789aMeepbxI/1.jpg
    - https://i3.ytimg.com/vi/789aMeepbxI/2.jpg
    - https://i3.ytimg.com/vi/789aMeepbxI/3.jpg
---
