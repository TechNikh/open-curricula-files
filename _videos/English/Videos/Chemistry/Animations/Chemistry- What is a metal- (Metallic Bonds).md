---
version: 1
type: video
provider: YouTube
id: vOuFTuvf4qk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Chemistry-%20What%20is%20a%20metal-%20%28Metallic%20Bonds%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 10b600f0-1e16-4604-90a0-f15261527ff7
updated: 1486069661
title: 'Chemistry- What is a metal? (Metallic Bonds)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vOuFTuvf4qk/default.jpg
    - https://i3.ytimg.com/vi/vOuFTuvf4qk/1.jpg
    - https://i3.ytimg.com/vi/vOuFTuvf4qk/2.jpg
    - https://i3.ytimg.com/vi/vOuFTuvf4qk/3.jpg
---
