---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forget
offline_file: ""
offline_thumbnail: ""
uuid: 75a9721c-92c7-4ef0-962a-66b09449018e
updated: 1484310541
title: forget
categories:
    - Dictionary
---
forget
     v 1: dismiss from the mind; stop remembering; "i tried to bury
          these unpleasant memories" [syn: {bury}] [ant: {remember}]
     2: be unable to remember; "I'm drawing a blank"; "You are
        blocking the name of your first wife!" [syn: {block}, {blank
        out}, {draw a blank}] [ant: {remember}]
     3: forget to do something; "Don't forget to call the chairman
        of the board to the meeting!" [ant: {mind}]
     4: leave behind unintentionally; "I forgot my umbrella in the
        restaurant"; "I left my keys inside the car and locked the
        doors" [syn: {leave}]
     [also: {forgotten}, {forgot}, {forgetting}]
