---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inherent_chirality
offline_file: ""
offline_thumbnail: ""
uuid: 7037a595-b786-426f-a0ad-275eff58e87b
updated: 1484309311
title: Inherent chirality
categories:
    - Symmetry
---
In chemistry, inherent chirality is a property of molecules and complexes whose lack of symmetry does not originate from a classic stereogenic element, but is rather the consequence of the presence of a curvature in a structure that would be devoid of symmetry axes in any bidimensional representation.[1][2]
