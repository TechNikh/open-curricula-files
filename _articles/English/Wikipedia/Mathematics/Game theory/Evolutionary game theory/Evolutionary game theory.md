---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolutionary_game_theory
offline_file: ""
offline_thumbnail: ""
uuid: 8e02e3ca-e8ff-4ae9-b9db-ffddd7e140e3
updated: 1484308520
title: Evolutionary game theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-John_Maynard_Smith.jpg
tags:
    - The problem
    - Classical game theory
    - Adapting game theory to evolutionary games
    - Models
    - Games
    - Hawk Dove
    - The War of Attrition game
    - Asymmetries that allow new strategies
    - Strategic alternatives in social behaviour
    - Who is playing the game?
    - Eusociality and kin selection
    - Prisoners dilemma
    - Routes to altruism
    - The ESS
    - Rock Paper Scissors game
    - The side-blotched lizard
    - RPS and ecology
    - RPS and human social cyclic behavior
    - Signalling, sexual selection and the handicap principle
    - Co-evolution
    - Extensions of the evolutionary game theory model
    - Further application
    - Bibliography
categories:
    - Evolutionary game theory
---
Evolutionary game theory (EGT) is the application of game theory to evolving populations of lifeforms in biology. EGT is useful in this context by defining a framework of contests, strategies, and analytics into which Darwinian competition can be modelled. EGT originated in 1973 with John Maynard Smith and George R. Price's formalisation of the way in which such contests can be analysed as "strategies" and the mathematical criteria that can be used to predict the resulting prevalence of such competing strategies.[1]
