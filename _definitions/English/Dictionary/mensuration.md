---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mensuration
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484361481
title: mensuration
categories:
    - Dictionary
---
mensuration
     n : the act or process of measuring; "the measurements were
         carefully done"; "his mental measurings proved remarkably
         accurate" [syn: {measurement}, {measuring}, {measure}]
