---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/address
offline_file: ""
offline_thumbnail: ""
uuid: 621b6b1b-cbfc-4a27-9c0a-b8ed227905d9
updated: 1484310448
title: address
categories:
    - Dictionary
---
address
     n 1: (computer science) the code that identifies where a piece of
          information is stored [syn: {computer address}]
     2: the place where a person or organization can be found or
        communicated with
     3: the act of delivering a formal spoken communication to an
        audience; "he listened to an address on minor Roman poets"
        [syn: {speech}]
     4: the manner of speaking to another individual; "he failed in
        his manner of address to the captain"
     5: a sign in front of a house or business carrying the
        conventional form by which its location is described
     6: written directions for finding some location; written on
        ...
