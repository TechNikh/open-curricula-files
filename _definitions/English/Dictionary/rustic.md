---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rustic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582881
title: rustic
categories:
    - Dictionary
---
rustic
     adj 1: characteristic of rural life; "countrified clothes"; "rustic
            awkwardness" [syn: {countrified}, {countryfied}]
     2: awkwardly simple and provincial; "bumpkinly country boys";
        "rustic farmers"; "a hick town"; "the nightlife of
        Montmartre awed the unsophisticated tourists" [syn: {bumpkinly},
         {hick}, {unsophisticated}]
     3: used of idealized country life; "a country life of arcadian
        contentment"; "a pleasant bucolic scene"; "charming in its
        pastoral setting"; "rustic tranquility" [syn: {arcadian},
        {bucolic}, {pastoral}]
     4: characteristic of the fields or country; "agrestic
        simplicity"; "rustic ...
