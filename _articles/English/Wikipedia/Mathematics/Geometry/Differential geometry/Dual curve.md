---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_curve
offline_file: ""
offline_thumbnail: ""
uuid: ea4c5cfa-b6f0-4232-8ff5-1ddea53a27bc
updated: 1484309218
title: Dual curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Dual_curve.svg.png
tags:
    - Equations
    - Degree
    - Polar reciprocal
    - Properties of dual curve
    - Generalizations
    - Higher dimensions
    - Dual polygon
    - Notes
categories:
    - Differential geometry
---
In projective geometry, a dual curve of a given plane curve C is a curve in the dual projective plane consisting of the set of lines tangent to C. There is a map from a curve to its dual, sending each point to the point dual to its tangent line. If C is algebraic then so is its dual and the degree of the dual is known as the class of the original curve. The equation of the dual of C, given in line coordinates, is known as the tangential equation of C.
