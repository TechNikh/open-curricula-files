---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/keeper
offline_file: ""
offline_thumbnail: ""
uuid: 1ccd4e10-9465-4ce0-8743-154c1ef248cf
updated: 1484310395
title: keeper
categories:
    - Dictionary
---
keeper
     n 1: someone in charge of other people; "am I my brother's
          keeper?"
     2: one having charge of buildings or grounds or animals [syn: {custodian},
         {steward}]
