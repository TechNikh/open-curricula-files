---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Toma%C5%BE_Pisanski'
offline_file: ""
offline_thumbnail: ""
uuid: 6185e042-e33f-4ddc-92cb-f3660864b7d7
updated: 1484309429
title: Tomaž Pisanski
tags:
    - Biography
    - Awards and honors
categories:
    - Mathematical chemistry
---
Tomaž (Tomo) Pisanski (born May 24, 1949 in Ljubljana, Slovenia) is a Slovenian mathematician working mainly in discrete mathematics and graph theory. In 1980 he calculated the genus of the Cartesian product of any pair of connected, bipartite, d-valent graphs using a method that was later called the White–Pisanski method.[1] In 1982 Vladimir Batagelj and Pisanski proved that the Cartesian product of a tree and a cycle is Hamiltonian if and only if no degree of the tree exceeds the length of the cycle. They also proposed a conjecture concerning cyclic Hamiltonicity of graphs. Their conjecture was proved in 2005.[2]
