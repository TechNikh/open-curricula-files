---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bladder
offline_file: ""
offline_thumbnail: ""
uuid: 3389ec3d-910d-45b8-8e42-c3cdfebd761a
updated: 1484310158
title: bladder
categories:
    - Dictionary
---
bladder
     n 1: a distensible membranous sac (usually containing liquid or
          gas) [syn: {vesica}]
     2: a bag that fills with air
