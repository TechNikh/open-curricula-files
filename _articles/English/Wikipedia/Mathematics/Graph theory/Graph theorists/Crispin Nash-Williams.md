---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crispin_Nash-Williams
offline_file: ""
offline_thumbnail: ""
uuid: 8a362d3f-c85d-484b-85cb-8429c8139e18
updated: 1484309470
title: Crispin Nash-Williams
tags:
    - Biography
    - Awards and honours
    - Contributions
categories:
    - Graph theorists
---
Crispin St. John Alvah Nash-Williams (19 December 1932 – 20 January 2001) was a British mathematician. His research interest was in the field of discrete mathematics, especially graph theory.
