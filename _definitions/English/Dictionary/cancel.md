---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cancel
offline_file: ""
offline_thumbnail: ""
uuid: 0a5f1b8a-d091-490a-8aa0-e4d25582b644
updated: 1484310234
title: cancel
categories:
    - Dictionary
---
cancel
     n : a notation cancelling a previous sharp or flat [syn: {natural}]
     v 1: postpone indefinitely or annul something that was scheduled;
          "Call off the engagement"; "cancel the dinner party"
          [syn: {call off}]
     2: make up for; "His skills offset his opponent's superior
        strength" [syn: {offset}, {set off}]
     3: declare null and void; make ineffective; "Cancel the
        election results"; "strike down a law" [syn: {strike down}]
     4: remove or make invisible; "Please delete my name from your
        list" [syn: {delete}]
     5: of cheques or tickets [syn: {invalidate}]
     [also: {cancelling}, {cancelled}]
