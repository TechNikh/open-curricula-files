---
version: 1
type: video
provider: YouTube
id: LO7-3MpWijU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Cosmological%20time%20scale%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7f36e37e-d3d1-48d5-a070-ea78f5730b4c
updated: 1486069686
title: Cosmological time scale 2
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/LO7-3MpWijU/default.jpg
    - https://i3.ytimg.com/vi/LO7-3MpWijU/1.jpg
    - https://i3.ytimg.com/vi/LO7-3MpWijU/2.jpg
    - https://i3.ytimg.com/vi/LO7-3MpWijU/3.jpg
---
