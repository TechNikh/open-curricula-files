---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aurifeuillean_factorization
offline_file: ""
offline_thumbnail: ""
uuid: 4a7c23cf-17c6-4d28-a714-ecf2c1d35b20
updated: 1484308635
title: Aurifeuillean factorization
tags:
    - Examples
    - History
categories:
    - Number theory
---
In number theory, an aurifeuillean factorization, or aurifeuillian factorization, named after Léon-François-Antoine Aurifeuille, is a special type of algebraic factorization that comes from non-trivial factorizations of cyclotomic polynomials over the integers.[1] Although cyclotomic polynomials themselves are irreducible over the integers, when restricted to particular integer values they may have an algebraic factorization, as in the examples below.
