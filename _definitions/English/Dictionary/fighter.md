---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fighter
offline_file: ""
offline_thumbnail: ""
uuid: 98c20bf9-900c-48b2-9f81-fe327dfabc19
updated: 1484310581
title: fighter
categories:
    - Dictionary
---
fighter
     n 1: someone who fights (or is fighting) [syn: {combatant}, {battler},
           {belligerent}, {scrapper}]
     2: a high-speed military or naval airplane designed to destroy
        enemy aircraft in the air [syn: {fighter aircraft}, {attack
        aircraft}]
     3: someone who fights for a cause [syn: {champion}, {hero}, {paladin}]
