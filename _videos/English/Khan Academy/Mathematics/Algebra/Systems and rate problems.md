---
version: 1
type: video
provider: YouTube
id: at4T4n4JYNc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Systems%20and%20rate%20problems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d078ce2-01a5-4c99-9d22-441ad1a785ba
updated: 1486069559
title: Systems and rate problems
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/at4T4n4JYNc/default.jpg
    - https://i3.ytimg.com/vi/at4T4n4JYNc/1.jpg
    - https://i3.ytimg.com/vi/at4T4n4JYNc/2.jpg
    - https://i3.ytimg.com/vi/at4T4n4JYNc/3.jpg
---
