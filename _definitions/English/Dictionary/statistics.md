---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/statistics
offline_file: ""
offline_thumbnail: ""
uuid: 116cb017-69fd-41f9-92f8-c787c6056af2
updated: 1484310462
title: statistics
categories:
    - Dictionary
---
statistics
     n : a branch of applied mathematics concerned with the
         collection and interpretation of quantitative data and
         the use of probability theory to estimate population
         parameters
