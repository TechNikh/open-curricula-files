---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hated
offline_file: ""
offline_thumbnail: ""
uuid: 56aae047-8399-47d9-aadf-c273e35ffb03
updated: 1484310567
title: hated
categories:
    - Dictionary
---
hated
     adj : treated with contempt [syn: {despised}, {detested}, {scorned}]
