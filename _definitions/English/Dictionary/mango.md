---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mango
offline_file: ""
offline_thumbnail: ""
uuid: 4460b66f-d997-48c0-b4cb-807f4d0b1272
updated: 1484310343
title: mango
categories:
    - Dictionary
---
mango
     n 1: large evergreen tropical tree cultivated for its large oval
          smooth-skinned fruit [syn: {mango tree}, {Mangifera
          indica}]
     2: large oval smooth-skinned tropical fruit with juicy aromatic
        pulp and a large hairy seed
     [also: {mangoes} (pl)]
