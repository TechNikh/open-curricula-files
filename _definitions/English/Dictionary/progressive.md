---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/progressive
offline_file: ""
offline_thumbnail: ""
uuid: 18011329-49f1-4646-aa35-ee8999d56a6e
updated: 1484310593
title: progressive
categories:
    - Dictionary
---
progressive
     adj 1: favoring or promoting progress; "progressive schools" [ant:
            {regressive}]
     2: favoring or promoting reform (often by government action)
        [syn: {reformist}, {reform-minded}]
     3: (of taxes) adjusted so that the rate increases as the amount
        increases [ant: {regressive}]
     4: gradually advancing in extent
     5: advancing in severity; "progressive paralysis"
     n 1: a tense of verbs used in describing action that is on-going
          [syn: {progressive tense}, {imperfect}, {imperfect tense},
           {continuous tense}]
     2: a person who favors a political philosophy of progress and
        reform and the protection of civil ...
