---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dependency
offline_file: ""
offline_thumbnail: ""
uuid: 8b9d6e9e-6d3f-4423-8062-ee34c303fd2c
updated: 1484310535
title: dependency
categories:
    - Dictionary
---
dependency
     n 1: lack of independence or self-sufficiency [syn: {dependence},
           {dependance}]
     2: being abnormally tolerant to and dependent on something that
        is psychologically or physically habit-forming (especially
        alcohol or narcotic drugs) [syn: {addiction}, {dependence},
         {habituation}]
     3: a geographical area politically controlled by a distant
        country [syn: {colony}]
