---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hormone
offline_file: ""
offline_thumbnail: ""
uuid: 559dde1c-eda2-4c33-b6dc-1d0c2c6a277d
updated: 1484310359
title: hormone
categories:
    - Dictionary
---
hormone
     n : the secretion of an endocrine gland that is transmitted by
         the blood to the tissue on which it has a specific effect
         [syn: {endocrine}, {internal secretion}]
