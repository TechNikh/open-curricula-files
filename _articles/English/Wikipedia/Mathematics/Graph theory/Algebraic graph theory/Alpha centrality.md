---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alpha_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 83bf41a2-fbb3-47f8-af40-ee95dc5d4d76
updated: 1484309373
title: Alpha centrality
tags:
    - Definition
    - Motivation
    - Applications
    - Notes and references
categories:
    - Algebraic graph theory
---
In graph theory and social network analysis, alpha centrality is a measure of centrality of nodes within a graph. It is an adaptation of eigenvector centrality with the addition that nodes are imbued with importance from external sources.
