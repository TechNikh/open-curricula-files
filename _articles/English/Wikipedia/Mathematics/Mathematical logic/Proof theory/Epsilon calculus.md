---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epsilon_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 82c5814b-34cb-4a2f-9316-7d7dd2713237
updated: 1484309492
title: Epsilon calculus
tags:
    - Epsilon operator
    - Hilbert notation
    - Bourbaki notation
    - Modern approaches
    - Epsilon substitution method
    - Notes
categories:
    - Proof theory
---
Hilbert's epsilon calculus is an extension of a formal language by the epsilon operator, where the epsilon operator substitutes for quantifiers in that language as a method leading to a proof of consistency for the extended formal language. The epsilon operator and epsilon substitution method are typically applied to a first-order predicate calculus, followed by a showing of consistency. The epsilon-extended calculus is further extended and generalized to cover those mathematical objects, classes, and categories for which there is a desire to show consistency, building on previously-shown consistency at earlier levels.[1]
