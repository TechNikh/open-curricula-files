---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Club_set
offline_file: ""
offline_thumbnail: ""
uuid: 97cd59bf-54d4-4bc0-b2d6-6c517eb2fd2c
updated: 1484309501
title: Club set
tags:
    - Formal definition
    - The closed unbounded filter
categories:
    - Set theory
---
In mathematics, particularly in mathematical logic and set theory, a club set is a subset of a limit ordinal which is closed under the order topology, and is unbounded (see below) relative to the limit ordinal. The name club is a contraction of "closed and unbounded".
