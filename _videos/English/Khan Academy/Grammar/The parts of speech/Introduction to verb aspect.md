---
version: 1
type: video
provider: YouTube
id: r7Psg-4vvsA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Introduction%20to%20verb%20aspect.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e0678dc1-e6de-4d68-a19e-cbf4f4b54244
updated: 1486069599
title: Introduction to verb aspect
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/r7Psg-4vvsA/default.jpg
    - https://i3.ytimg.com/vi/r7Psg-4vvsA/1.jpg
    - https://i3.ytimg.com/vi/r7Psg-4vvsA/2.jpg
    - https://i3.ytimg.com/vi/r7Psg-4vvsA/3.jpg
---
If verb tense allows you to control the past, the present, and the future, then aspect gives you even finer control over time. David, Khan Academy's resident grammarian, explains.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/e/simple-verb-aspect?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/simple-aspect?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/the-truly-irregular-irregular-verbs-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
