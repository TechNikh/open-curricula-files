---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tropics
offline_file: ""
offline_thumbnail: ""
uuid: 6c7a04d6-5bf2-4e62-8b9e-2b2428f5b10e
updated: 1484310258
title: tropics
categories:
    - Dictionary
---
tropics
     n : the part of the Earth's surface between the Tropic of Cancer
         and the Tropic of Capricorn; characterized by a hot
         climate [syn: {Torrid Zone}, {tropical zone}]
