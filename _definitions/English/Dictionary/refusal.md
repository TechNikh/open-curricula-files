---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refusal
offline_file: ""
offline_thumbnail: ""
uuid: d3dceb0d-dff0-427e-9eb8-6fc07448181b
updated: 1484310172
title: refusal
categories:
    - Dictionary
---
refusal
     n 1: the act of refusing
     2: a message refusing to accept something that is offered
