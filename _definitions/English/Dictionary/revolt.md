---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revolt
offline_file: ""
offline_thumbnail: ""
uuid: c934ce3e-8ce8-4ae8-85ab-cbed73e40538
updated: 1484310242
title: revolt
categories:
    - Dictionary
---
revolt
     n : organized opposition to authority; a conflict in which one
         faction tries to wrest control from another [syn: {rebellion},
          {insurrection}, {rising}, {uprising}]
     v 1: make revolution; "The people revolted when bread prices
          tripled again"
     2: fill with distaste; "This spoilt food disgusts me" [syn: {disgust},
         {gross out}, {repel}]
     3: cause aversion in; offend the moral sense of; "The
        pornographic pictures sickened us" [syn: {disgust}, {nauseate},
         {sicken}, {churn up}]
