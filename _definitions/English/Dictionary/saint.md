---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saint
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484517361
title: saint
categories:
    - Dictionary
---
saint
     n 1: a person who has died and has been declared a saint by
          canonization
     2: person of exceptional holiness [syn: {holy man}, {holy
        person}, {angel}]
     3: model of excellence or perfection of a kind; one having no
        equal [syn: {ideal}, {paragon}, {nonpareil}, {apotheosis},
         {nonesuch}, {nonsuch}]
     v 1: hold sacred [syn: {enshrine}]
     2: in the Catholic church; declare (a dead person) to be a
        saint; "After he was shown to have performed a miracle,
        the priest was canonized" [syn: {canonize}, {canonise}]
