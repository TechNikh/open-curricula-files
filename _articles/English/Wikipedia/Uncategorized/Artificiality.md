---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Man-made
offline_file: ""
offline_thumbnail: ""
uuid: 64407269-c810-4148-b4e5-0beae75dbf1c
updated: 1484308340
title: Artificiality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Homebrew_reaction_diffusion_example_512iter.jpg
tags:
    - Connotations
    - Distinguishing natural objects from artificial objects
categories:
    - Uncategorized
---
Artificiality (also called factitiousness, or the state of being artificial or man-made) is the state of being the product of intentional human manufacture, rather than occurring naturally through processes not involving or requiring human activity.
