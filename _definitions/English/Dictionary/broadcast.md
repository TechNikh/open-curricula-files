---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broadcast
offline_file: ""
offline_thumbnail: ""
uuid: 6c666da8-3dd9-4cd1-b8f6-06c7802f916c
updated: 1484310391
title: broadcast
categories:
    - Dictionary
---
broadcast
     n 1: message that is transmitted by radio or television
     2: a radio or television show; "did you see his program last
        night?" [syn: {program}, {programme}]
     v 1: broadcast over the airwaves, as in radio or television; "We
          cannot air this X-rated song" [syn: {air}, {send}, {beam},
           {transmit}]
     2: sow over a wide area, especially by hand; "broadcast seeds"
     3: cause to become widely known; "spread information";
        "circulate a rumor"; "broadcast the news" [syn: {circulate},
         {circularize}, {circularise}, {distribute}, {disseminate},
         {propagate}, {spread}, {diffuse}, {disperse}, {pass
        around}]
