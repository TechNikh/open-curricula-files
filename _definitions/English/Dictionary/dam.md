---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dam
offline_file: ""
offline_thumbnail: ""
uuid: bdfe8645-1b3c-4110-b394-4eb8d90d89de
updated: 1484310260
title: dam
categories:
    - Dictionary
---
dam
     n 1: a barrier constructed to contain the flow of water or to
          keep out the sea [syn: {dike}, {dyke}, {levee}]
     2: a metric unit of length equal to ten meters [syn: {decameter},
         {dekameter}, {decametre}, {dekametre}, {dkm}]
     3: female parent of an animal especially domestic livestock
     v : obstruct with, or as if with, a dam; "dam the gorges of the
         Yangtse River" [syn: {dam up}]
     [also: {damming}, {dammed}]
