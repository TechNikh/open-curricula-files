---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metre
offline_file: ""
offline_thumbnail: ""
uuid: 7099497e-ae97-4c86-bd44-1375924a5612
updated: 1484310405
title: metre
categories:
    - Dictionary
---
metre
     n 1: the basic unit of length adopted under the Systeme
          International d'Unites (approximately 1.094 yards) [syn:
           {meter}, {m}]
     2: (prosody) the accent in a metrical foot of verse [syn: {meter},
         {measure}, {beat}, {cadence}]
     3: rhythm as given by division into parts of equal time [syn: {meter},
         {time}]
