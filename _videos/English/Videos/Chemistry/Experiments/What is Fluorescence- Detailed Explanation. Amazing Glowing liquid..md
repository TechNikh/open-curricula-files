---
version: 1
type: video
provider: YouTube
id: 62pwykA_huU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/What%20is%20Fluorescence-%20Detailed%20Explanation.%20Amazing%20Glowing%20liquid..webm"
offline_file: ""
offline_thumbnail: ""
uuid: c5f6a587-1296-4d71-acde-86f1797adbce
updated: 1486069661
title: >
    What is Fluorescence? Detailed Explanation. Amazing Glowing
    liquid.
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/62pwykA_huU/default.jpg
    - https://i3.ytimg.com/vi/62pwykA_huU/1.jpg
    - https://i3.ytimg.com/vi/62pwykA_huU/2.jpg
    - https://i3.ytimg.com/vi/62pwykA_huU/3.jpg
---
