---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dinostratus
offline_file: ""
offline_thumbnail: ""
uuid: 87797527-c489-4d3e-b413-44e1dca6b5f6
updated: 1484309080
title: Dinostratus
tags:
    - Life and work
    - Citations and footnotes
categories:
    - Geometers
---
Dinostratus (Greek: Δεινόστρατος, c. 390 BCE – c. 320 BCE) was a Greek mathematician and geometer, and the brother of Menaechmus. He is known for using the quadratrix to solve the problem of squaring the circle.
