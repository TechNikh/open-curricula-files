---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genetic
offline_file: ""
offline_thumbnail: ""
uuid: d6774829-30b1-4f84-82f3-90814caa9560
updated: 1484310293
title: genetic
categories:
    - Dictionary
---
genetic
     adj 1: tending to occur among members of a family usually by
            heredity; "an inherited disease"; "familial traits";
            "genetically transmitted features" [syn: {familial}, {hereditary},
             {inherited}, {transmitted}, {transmissible}]
     2: of or relating to or produced by or being a gene; "genic
        combinations"; "genetic code" [syn: {genic}]
     3: of or relating to the science of genetics; "genetic
        research" [syn: {genetical}]
