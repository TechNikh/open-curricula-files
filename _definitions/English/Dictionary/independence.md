---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/independence
offline_file: ""
offline_thumbnail: ""
uuid: 27a43d3e-22f0-41d1-ab62-85879a1984ef
updated: 1484310447
title: independence
categories:
    - Dictionary
---
independence
     n 1: freedom from control or influence of another or others [syn:
           {independency}]
     2: the successful ending of the American Revolution; "they
        maintained close relations with England even after
        independence"
     3: a city in western Missouri; the beginning of the Santa Fe
        Trail
