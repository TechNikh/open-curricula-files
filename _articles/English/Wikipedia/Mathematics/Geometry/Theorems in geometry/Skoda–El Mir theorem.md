---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Skoda%E2%80%93El_Mir_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 34ef847e-bdca-4222-b79d-7ad839095a29
updated: 1484309126
title: Skoda–El Mir theorem
categories:
    - Theorems in geometry
---
Theorem (Skoda,[1] El Mir,[2] Sibony [3]). Let X be a complex manifold, and E a closed complete pluripolar set in X. Consider a closed positive current 
  
    
      
        Θ
      
    
    {\displaystyle \Theta }
  
 on 
  
    
      
        X
        ∖
        E
      
    
    {\displaystyle X\backslash E}
  
 which is locally integrable around E. Then the trivial extension of 
  
    
      
        Θ
      
    
    {\displaystyle \Theta }
  
 to X is closed on X.
