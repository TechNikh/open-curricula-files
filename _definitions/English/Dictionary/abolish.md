---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abolish
offline_file: ""
offline_thumbnail: ""
uuid: bd2b7f53-4cf4-412a-828c-537e1db85358
updated: 1484310240
title: abolish
categories:
    - Dictionary
---
abolish
     v : do away with; "Slavery was abolished in the mid-19th century
         in America and in Russia" [syn: {get rid of}] [ant: {establish}]
