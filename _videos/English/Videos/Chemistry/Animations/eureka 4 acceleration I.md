---
version: 1
type: video
provider: YouTube
id: jDy-GpByrnU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/eureka%204%20acceleration%20I.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a26630f5-2e60-4279-b18c-ff4c2abc276e
updated: 1486069665
title: eureka 4 acceleration I
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/jDy-GpByrnU/default.jpg
    - https://i3.ytimg.com/vi/jDy-GpByrnU/1.jpg
    - https://i3.ytimg.com/vi/jDy-GpByrnU/2.jpg
    - https://i3.ytimg.com/vi/jDy-GpByrnU/3.jpg
---
