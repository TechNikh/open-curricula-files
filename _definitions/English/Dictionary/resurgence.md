---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resurgence
offline_file: ""
offline_thumbnail: ""
uuid: b5683b3f-6898-488b-8381-c7ae95ed0309
updated: 1484310579
title: resurgence
categories:
    - Dictionary
---
resurgence
     n : bringing again into activity and prominence; "the revival of
         trade"; "a revival of a neglected play by Moliere"; "the
         Gothic revival in architecture" [syn: {revival}, {revitalization},
          {revitalisation}, {revivification}]
