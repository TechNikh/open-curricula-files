---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tucked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408041
title: tucked
categories:
    - Dictionary
---
tucked
     adj : having tucked or being tucked; "tightly tucked blankets"; "a
           fancy tucked shirt" [ant: {untucked}]
