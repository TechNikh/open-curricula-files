---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Australia
offline_file: ""
offline_thumbnail: ""
uuid: c5df6fae-59c4-43a8-b592-f278f03ab21c
updated: 1484308342
title: Australia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bradshaw_rock_paintings.jpg
tags:
    - Name
    - History
    - Prehistory
    - European arrival
    - Colonial expansion
    - Nationhood
    - government
    - States and territories
    - Foreign relations and military
    - Geography and Climate
    - Environment
    - Environmental issues
    - Economy
    - Demographics
    - language
    - religion
    - Education
    - Health
    - culture
    - Arts
    - Media
    - Cuisine
    - Sport and recreation
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Australia (i/əˈstreɪliə/, /ɒ-/, /-ljə/),[10][11] officially the Commonwealth of Australia,[12] is a country comprising the mainland of the Australian continent, the island of Tasmania and numerous smaller islands. It is the world's sixth-largest country by total area. The neighbouring countries are Papua New Guinea, Indonesia and East Timor to the north; the Solomon Islands and Vanuatu to the north-east; and New Zealand to the south-east. Australia's capital is Canberra, and its largest urban area is Sydney.
