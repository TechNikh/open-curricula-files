---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fixed-point_space
offline_file: ""
offline_thumbnail: ""
uuid: c746f721-a437-45d2-b461-f49f2b443ee7
updated: 1484308782
title: Fixed-point space
categories:
    - Fixed points (mathematics)
---
In mathematics, a Hausdorff space X is called a fixed-point space if every continuous function 
  
    
      
        f
        :
        X
        →
        X
      
    
    {\displaystyle f:X\rightarrow X}
  
 has a fixed point.
