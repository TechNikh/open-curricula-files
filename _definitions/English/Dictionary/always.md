---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/always
offline_file: ""
offline_thumbnail: ""
uuid: 9b903f7e-fff6-4545-9e2e-2cbdd66765e5
updated: 1484310303
title: always
categories:
    - Dictionary
---
always
     adv 1: at all times; all the time and on every occasion; "I will
            always be there to help you"; "always arrives on
            time"; "there is always some pollution in the air";
            "ever hoping to strike it rich"; "ever busy" [syn: {ever},
             {e'er}] [ant: {never}]
     2: seemingly without interruption; often and repeatedly;
        "always looking for faults"; "it is always raining"; "he
        is forever cracking jokes"; "they are forever arguing"
        [syn: {forever}]
     3: at any time or in any event; "you can always resign if you
        don't like it"; "you could always take a day off"
     4: forever; throughout all time; "we will ...
