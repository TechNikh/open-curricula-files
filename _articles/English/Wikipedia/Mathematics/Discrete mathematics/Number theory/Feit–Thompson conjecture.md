---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Feit%E2%80%93Thompson_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 8b4e39fb-2d35-429d-9c08-36803c4a2a2d
updated: 1484308639
title: Feit–Thompson conjecture
categories:
    - Number theory
---
In mathematics, the Feit–Thompson conjecture is a conjecture in number theory, suggested by Walter Feit and John G. Thompson (1962). The conjecture states that there are no distinct prime numbers p and q such that
