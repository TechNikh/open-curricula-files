---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monster_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 007e71c4-9f72-4a50-bc16-19db5d99178a
updated: 1484308773
title: Monster (physics)
categories:
    - Entropy
---
A monster, in quantum physics, is an arrangement of matter that has maximum disorder. The high-entropy state of monsters has been theorized as being responsible for the high entropy of black holes; while the likelihood of any given star entering a "monster" state while collapsing is small, quantum mechanics takes into account all possible outcomes so the monster's entropy has to be taken into account when calculating black hole entropy.[1]
