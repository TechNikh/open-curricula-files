---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Buckyballs
offline_file: ""
offline_thumbnail: ""
uuid: feedb02a-70d7-4ffc-9479-629a0206068e
updated: 1484308392
title: Buckminsterfullerene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Fussball_1.jpg
tags:
    - Etymology
    - History
    - discovery
    - Synthesis
    - Further developments
    - Properties
    - Molecule
    - Solution
    - Solid
    - Band structure and superconductivity
    - Chemical reactions and properties
    - Hydrogenation
    - Halogenation
    - Addition of oxygen atoms
    - Cycloadditions
    - Free radical reactions
    - Cyclopropanation (Bingel reaction)
    - Redox reactions – C60 anions and cations
    - Metal complexes
    - Endohedral fullerenes
    - Applications
    - Bibliography
categories:
    - Uncategorized
---
Buckminsterfullerene (or bucky-ball) is a spherical fullerene molecule with the formula C60. It has a cage-like fused-ring structure (truncated icosahedron) which resembles a football (soccer ball), made of twenty hexagons and twelve pentagons, with a carbon atom at each vertex of each polygon and a bond along each polygon edge.
