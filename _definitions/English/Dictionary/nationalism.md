---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nationalism
offline_file: ""
offline_thumbnail: ""
uuid: f2d23a4e-293f-4b44-8bee-9bd46ed5ec68
updated: 1484310529
title: nationalism
categories:
    - Dictionary
---
nationalism
     n 1: love of country and willingness to sacrifice for it [syn: {patriotism}]
     2: the doctrine that your national culture and interests are
        superior to any other [ant: {multiculturalism}, {internationalism}]
     3: the aspiration for national independence felt by people
        under foreign domination
     4: the doctrine that nations should act independently (rather
        than collectively) to attain their goals [ant: {internationalism}]
