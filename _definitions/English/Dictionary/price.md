---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/price
offline_file: ""
offline_thumbnail: ""
uuid: f32c7973-8e85-4b2a-9e3a-81cb4de30f41
updated: 1484310445
title: price
categories:
    - Dictionary
---
price
     n 1: the amount of money needed to purchase something; "the price
          of gasoline"; "he got his new car on excellent terms";
          "how much is the damage?" [syn: {terms}, {damage}]
     2: the property of having material worth (often indicated by
        the amount of money something would bring if sold); "the
        fluctuating monetary value of gold and silver"; "he puts a
        high price on his services"; "he couldn't calculate the
        cost of the collection" [syn: {monetary value}, {cost}]
     3: value measured by what must be given or done or undergone to
        obtain something; "the cost in human life was enormous";
        "the price of success is ...
