---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_group
offline_file: ""
offline_thumbnail: ""
uuid: 9c7e1e84-56c0-4d06-88bc-8dc3e6d03c79
updated: 1484309319
title: Symmetry group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Tetrahedral_group_2.svg.png
tags:
    - Introduction
    - One dimension
    - Two dimensions
    - Three dimensions
    - Symmetry groups in general
categories:
    - Symmetry
---
In abstract algebra, the symmetry group of an object (image, signal, etc.) is the group of all transformations under which the object is invariant with composition as the group operation. For a space with a metric, it is a subgroup of the isometry group of the space concerned. If not stated otherwise, this article considers symmetry groups in Euclidean geometry, but the concept may also be studied in more general contexts as expanded below.
