---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/referring
offline_file: ""
offline_thumbnail: ""
uuid: 34f82685-214c-4db0-9c66-ba8c190f1067
updated: 1484310597
title: referring
categories:
    - Dictionary
---
refer
     v 1: make reference to; "His name was mentioned in connection
          with the invention" [syn: {mention}, {advert}, {bring up},
           {cite}, {name}]
     2: have to do with or be relevant to; "There were lots of
        questions referring to her talk"; "My remark pertained to
        your earlier comments" [syn: {pertain}, {relate}, {concern},
         {come to}, {bear on}, {touch}, {touch on}]
     3: think of, regard, or classify under a subsuming principle or
        with a general group or in relation to another; "This
        plant can be referred to a known species"
     4: send or direct for treatment, information, or a decision;
        "refer a patient to a ...
