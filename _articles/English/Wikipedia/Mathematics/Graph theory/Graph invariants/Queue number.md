---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Queue_number
offline_file: ""
offline_thumbnail: ""
uuid: 5fd57e14-eaec-47be-8efc-b524c7e5729e
updated: 1484309422
title: Queue number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-DeBruijn-3-2.svg.png
tags:
    - Definition
    - Graph classes with bounded queue number
    - Related invariants
    - Computational complexity
    - Application in graph drawing
    - Notes
categories:
    - Graph invariants
---
In mathematics, the queue number of a graph is a graph invariant defined analogously to stack number (book thickness) using first-in first-out (queue) orderings in place of last-in first-out (stack) orderings.
