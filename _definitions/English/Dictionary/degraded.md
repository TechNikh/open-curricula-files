---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degraded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484599921
title: degraded
categories:
    - Dictionary
---
degraded
     adj 1: unrestrained by convention or morality; "Congreve draws a
            debauched aristocratic society"; "deplorably
            dissipated and degraded"; "riotous living"; "fast
            women" [syn: {debauched}, {degenerate}, {dissipated},
            {dissolute}, {libertine}, {profligate}, {riotous}, {fast}]
     2: lowered in value; "the dollar is low"; "a debased currency"
        [syn: {debased}, {devalued}]
