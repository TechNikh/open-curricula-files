---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kepler_triangle
offline_file: ""
offline_thumbnail: ""
uuid: 3b7c0a1d-adb7-4faf-bd2d-3333490249bb
updated: 1484308935
title: Kepler triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kepler_triangle.svg.png
tags:
    - Derivation
    - Relation to arithmetic, geometric, and harmonic mean
    - Constructing a Kepler triangle
    - A mathematical coincidence
categories:
    - Elementary geometry
---
A Kepler triangle is a right triangle with edge lengths in geometric progression. The ratio of the edges of a Kepler triangle is linked to the golden ratio
