---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enthusiastic
offline_file: ""
offline_thumbnail: ""
uuid: b5a20cc4-dc16-419f-aad1-924a17aea2a8
updated: 1484310178
title: enthusiastic
categories:
    - Dictionary
---
enthusiastic
     adj : having or showing great excitement and interest;
           "enthusiastic crowds filled the streets"; "an
           enthusiastic response"; "was enthusiastic about taking
           ballet lessons" [ant: {unenthusiastic}]
