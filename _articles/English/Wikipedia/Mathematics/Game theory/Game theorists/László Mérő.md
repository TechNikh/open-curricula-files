---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A1szl%C3%B3_M%C3%A9r%C5%91'
offline_file: ""
offline_thumbnail: ""
uuid: 664f48e1-0d14-4100-bce9-70a9cd157830
updated: 1484308504
title: László Mérő
tags:
    - Volumes published in English
    - Other language editions
categories:
    - Game theorists
---
László Mérő (December 11, 1949, Budapest -) is a Hungarian research psychologist and popular science author. He is a lecturer at the Experimental Psychology Department of Eötvös Loránd University and at the business school Kürt Academy.[1] He is also a founder and leader of a software company producing computer games. One of his projects is a computer game he is developing with Ernő Rubik, the inventor of the Rubik's Cube. He is also the leader of the Hungarian team at the World Puzzle Championship. His son is Csaba Mérő, 8 time Hungarian go champion.
