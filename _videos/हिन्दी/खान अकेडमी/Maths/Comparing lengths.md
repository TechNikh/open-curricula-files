---
version: 1
type: video
provider: YouTube
id: XUMiN25zxdY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4a6dcc47-3be0-4932-b026-52827bd70e2e
updated: 1486069609
title: Comparing lengths
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/XUMiN25zxdY/default.jpg
    - https://i3.ytimg.com/vi/XUMiN25zxdY/1.jpg
    - https://i3.ytimg.com/vi/XUMiN25zxdY/2.jpg
    - https://i3.ytimg.com/vi/XUMiN25zxdY/3.jpg
---
