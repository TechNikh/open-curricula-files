---
version: 1
type: video
provider: YouTube
id: jUUJSOM1ihU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Biology/Macromolecules/Molecular%20structure%20of%20RNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 03f12d67-8f82-4d36-be69-71b4b643d478
updated: 1486069672
title: Molecular structure of RNA
categories:
    - Macromolecules
thumbnail_urls:
    - https://i3.ytimg.com/vi/jUUJSOM1ihU/default.jpg
    - https://i3.ytimg.com/vi/jUUJSOM1ihU/1.jpg
    - https://i3.ytimg.com/vi/jUUJSOM1ihU/2.jpg
    - https://i3.ytimg.com/vi/jUUJSOM1ihU/3.jpg
---
