---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wavelength
offline_file: ""
offline_thumbnail: ""
uuid: 60484599-0325-4cfd-af66-4ac759417263
updated: 1484308909
title: Wavelength
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sine_wavelength.svg.png
tags:
    - Sinusoidal waves
    - Standing waves
    - Mathematical representation
    - General media
    - Nonuniform media
    - Crystals
    - More general waveforms
    - Wave packets
    - Interference and diffraction
    - Double-slit interference
    - Single-slit diffraction
    - Diffraction-limited resolution
    - Subwavelength
    - Angular wavelength
categories:
    - Waves
---
In physics, the wavelength of a sinusoidal wave is the spatial period of the wave—the distance over which the wave's shape repeats,[1] and thus the inverse of the spatial frequency. It is usually determined by considering the distance between consecutive corresponding points of the same phase, such as crests, troughs, or zero crossings and is a characteristic of both traveling waves and standing waves, as well as other spatial wave patterns.[2][3] Wavelength is commonly designated by the Greek letter lambda (λ). The concept can also be applied to periodic waves of non-sinusoidal shape.[1][4] The term wavelength is also sometimes applied to modulated waves, and to the sinusoidal envelopes ...
