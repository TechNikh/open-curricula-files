---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integral_curve
offline_file: ""
offline_thumbnail: ""
uuid: 1204a07a-9098-4760-ab99-39ff44069f3d
updated: 1484309224
title: Integral curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Slope_Field.png
tags:
    - Definition
    - Generalization to differentiable manifolds
    - Definition
    - Relationship to ordinary differential equations
    - Remarks on the time derivative
categories:
    - Differential geometry
---
In mathematics, an integral curve is a parametric curve that represents a specific solution to an ordinary differential equation or system of equations. If the differential equation is represented as a vector field or slope field, then the corresponding integral curves are tangent to the field at each point.
