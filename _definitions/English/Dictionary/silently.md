---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silently
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436961
title: silently
categories:
    - Dictionary
---
silently
     adv : without speaking; "he sat mutely next to her" [syn: {mutely},
            {wordlessly}, {taciturnly}]
