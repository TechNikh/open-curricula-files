---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warmed
offline_file: ""
offline_thumbnail: ""
uuid: 782cf0ca-4ffb-434e-bbae-58e39e62958c
updated: 1484310429
title: warmed
categories:
    - Dictionary
---
warmed
     adj : having been warmed up; "a cup of warmed milk"
