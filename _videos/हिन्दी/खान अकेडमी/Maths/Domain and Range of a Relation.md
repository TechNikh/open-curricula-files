---
version: 1
type: video
provider: YouTube
id: -wGrZX0oCpY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b3bb2508-99d9-4177-9a35-d22d0cb5a819
updated: 1486069616
title: Domain and Range of a Relation
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/-wGrZX0oCpY/default.jpg
    - https://i3.ytimg.com/vi/-wGrZX0oCpY/1.jpg
    - https://i3.ytimg.com/vi/-wGrZX0oCpY/2.jpg
    - https://i3.ytimg.com/vi/-wGrZX0oCpY/3.jpg
---
