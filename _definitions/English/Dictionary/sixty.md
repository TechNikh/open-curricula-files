---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sixty
offline_file: ""
offline_thumbnail: ""
uuid: d6a33cf3-e7bc-4d61-8654-d3b58a7c8943
updated: 1484310395
title: sixty
categories:
    - Dictionary
---
sixty
     adj : being ten more than fifty [syn: {60}, {lx}, {threescore}]
     n : the cardinal number that is the product of ten and six [syn:
          {60}, {LX}]
