---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confidence
offline_file: ""
offline_thumbnail: ""
uuid: 26041458-795e-487e-8e6a-a70a3224daf0
updated: 1484310518
title: confidence
categories:
    - Dictionary
---
confidence
     n 1: freedom from doubt; belief in yourself and your abilities;
          "his assurance in his superiority did not make him
          popular"; "after that failure he lost his confidence";
          "she spoke with authority" [syn: {assurance}, {self-assurance},
           {self-confidence}, {authority}, {sureness}]
     2: a feeling of trust (in someone or something); "I have
        confidence in our team"; "confidence is always borrowed,
        never owned" [ant: {diffidence}]
     3: a state of confident hopefulness that events will be
        favorable; "public confidence in the economy"
     4: a trustful relationship; "he took me into his confidence";
        "he ...
