---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hall
offline_file: ""
offline_thumbnail: ""
uuid: fd60bebe-dd19-414f-ae3a-8952a43f163f
updated: 1484310148
title: hall
categories:
    - Dictionary
---
hall
     n 1: an interior passage or corridor onto which rooms open; "the
          elevators were at the end of the hall" [syn: {hallway}]
     2: a large entrance or reception room or area [syn: {anteroom},
         {antechamber}, {entrance hall}, {foyer}, {lobby}, {vestibule}]
     3: a large room for gatherings or entertainment; "lecture
        hall"; "pool hall"
     4: a college or university building containing living quarters
        for students [syn: {dormitory}, {dorm}, {residence hall},
        {student residence}]
     5: the large room of a manor or castle [syn: {manor hall}]
     6: English writer whose novel about a lesbian relationship was
        banned in Britain for ...
