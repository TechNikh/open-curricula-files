---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/associated
offline_file: ""
offline_thumbnail: ""
uuid: e0b6d92d-8c8f-40b1-8be5-4e8d27e8e488
updated: 1484310273
title: associated
categories:
    - Dictionary
---
associated
     adj 1: related to or accompanying; "Christmas and associated
            festivities" [syn: {connected}]
     2: joined in some kind of relationship (as a colleague or ally
        or companion etc.); "a cabal of associated lawyers"
