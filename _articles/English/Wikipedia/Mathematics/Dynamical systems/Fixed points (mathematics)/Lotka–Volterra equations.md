---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lotka%E2%80%93Volterra_equations'
offline_file: ""
offline_thumbnail: ""
uuid: 3ecd2252-34fb-4d85-bc37-16c9e62c5cba
updated: 1484308786
title: Lotka–Volterra equations
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/340px-LotkaVolterra_en.svg.png
tags:
    - History
    - In economics
    - Physical meaning of the equations
    - Prey
    - Predators
    - Solutions to the equations
    - A simple example
    - Phase-space plot of a further example
    - Dynamics of the system
    - Population equilibrium
    - Stability of the fixed points
    - First fixed point (extinction)
    - Second fixed point (oscillations)
    - Notes
categories:
    - Fixed points (mathematics)
---
The Lotka–Volterra equations, also known as the predator–prey equations, are a pair of first-order, non-linear, differential equations frequently used to describe the dynamics of biological systems in which two species interact, one as a predator and the other as prey. The populations change through time according to the pair of equations:
