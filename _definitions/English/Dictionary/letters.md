---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/letters
offline_file: ""
offline_thumbnail: ""
uuid: 21cb1d6c-3997-4874-83f0-027e80c17a9a
updated: 1484310301
title: letters
categories:
    - Dictionary
---
letters
     n 1: the literary culture; "this book shows American letters at
          its best"
     2: scholarly attainment; "he is a man of letters"
