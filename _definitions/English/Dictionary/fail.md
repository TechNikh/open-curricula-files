---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fail
offline_file: ""
offline_thumbnail: ""
uuid: 3bdfa5aa-e7b3-4d06-a72e-7dab8038288f
updated: 1484310361
title: fail
categories:
    - Dictionary
---
fail
     v 1: fail to do something; leave something undone; "She failed to
          notice that her child was no longer in his crib"; "The
          secretary failed to call the customer and the company
          lost the account" [syn: {neglect}]
     2: be unsuccessful; "Where do today's public schools fail?";
        "The attempt to rescue the hostages failed miserably"
        [syn: {go wrong}, {miscarry}] [ant: {succeed}]
     3: disappoint, prove undependable to; abandon, forsake; "His
        sense of smell failed him this time"; "His strength
        finally failed him"; "His children failed him in the
        crisis" [syn: {betray}]
     4: stop operating or functioning; "The ...
