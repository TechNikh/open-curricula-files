---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Telangana
offline_file: ""
offline_thumbnail: ""
uuid: 0eb04405-ff08-4e2b-b634-580f9f7dcfc9
updated: 1484308335
title: Telangana
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hyderabad_state_from_the_Imperial_Gazetteer_of_India%252C_1909.jpg'
tags:
    - Etymology
    - History
    - Early history
    - Kakatiya Dynasty
    - "Qutb Shahi and Asaf Jahi's"
    - Post-independence
    - Telangana Rebellion
    - States Reorganisation Commission
    - Telangana movement
    - Formation of Telangana state in 2014
    - geography
    - Climate
    - Ecology
    - National Parks and Sanctuaries
    - Administrative divisions
    - Government and politics
    - Demographics
    - Economy
    - Agriculture
    - Industries
    - Tourism
    - Awards
    - Infrastructure
    - Power
    - Transport
    - Roads
    - Railways
    - Airports
    - culture
    - Monuments
    - Religious destinations
    - Waterfalls
    - Education
    - Sports
categories:
    - Uncategorized
---
Telangana (i/tɛlənˈɡɑːnə/) is one of the 29 states in India, located in southern India. Telangana has an area of 112,077 square kilometres (43,273 sq mi), and a population of 35,193,978 (2011 census).[3] making it the twelfth largest state in India, and the twelfth most populated state in India. Its major cities include Hyderabad, Warangal, Nizamabad, Khammam and Karimnagar. Telangana is bordered by the states of Maharashtra to the north and north west, Chhattisgarh to the north, Karnataka to the west and Andhra Pradesh to the east and south.[4]
