---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generic_flatness
offline_file: ""
offline_thumbnail: ""
uuid: 13b04bd7-e8de-44df-85e4-34c7c155eabf
updated: 1484309149
title: Generic flatness
categories:
    - Algebraic geometry
---
In algebraic geometry and commutative algebra, the theorems of generic flatness and generic freeness state that under certain hypotheses, a sheaf of modules on a scheme is flat or free. They are due to Alexander Grothendieck.
