---
version: 1
type: video
provider: YouTube
id: 2-nCSLMGwhI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8e74b109-e576-48d1-a903-d729165857f0
updated: 1486069672
title: Nucleic acid structure 2
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/2-nCSLMGwhI/default.jpg
    - https://i3.ytimg.com/vi/2-nCSLMGwhI/1.jpg
    - https://i3.ytimg.com/vi/2-nCSLMGwhI/2.jpg
    - https://i3.ytimg.com/vi/2-nCSLMGwhI/3.jpg
---
