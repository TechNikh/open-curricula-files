---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unintelligible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440681
title: unintelligible
categories:
    - Dictionary
---
unintelligible
     adj 1: poorly articulated or enunciated, or drowned by noise;
            "unintelligible speech" [ant: {intelligible}]
     2: not clearly understood or expressed [syn: {opaque}]
