---
title: Irrigation
article_ref: https://en.wikipedia.org/wiki/Irrigation
tags:
    - History
    - China
    - Korea
    - North America
    - Present extent
    - Types of irrigation
    - Surface irrigation
    - Localized irrigation
    - Subsurface textile irrigation
    - Drip irrigation
    - Irrigation using sprinkler systems
    - Irrigation using Center pivot
    - Irrigation by Lateral move (side roll, wheel line, wheelmove)[31][32]
    - Sub-irrigation
    - Irrigation Automatically, non-electric using buckets and ropes
    - Irrigation using water condensed from humid air
    - In-ground irrigation
    - Water sources
    - Efficiency
    - Technical challenges
    - Journals
categories:
    - Water harvesting
---
