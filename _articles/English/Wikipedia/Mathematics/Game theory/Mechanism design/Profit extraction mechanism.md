---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Profit_extraction_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: eb5020e7-4e79-4458-935e-01d6450a64e4
updated: 1484309046
title: Profit extraction mechanism
tags:
    - Profit extraction in a digital goods auction
    - Estimating the maximum revenue
    - Profit extraction in a double auction
    - History
categories:
    - Mechanism design
---
In mechanism design and auction theory, a profit extraction mechanism (also called profit extractor or revenue extractor) is a truthful mechanism whose goal is to win a pre-specified amount of profit, if it is possible. [1]:347
