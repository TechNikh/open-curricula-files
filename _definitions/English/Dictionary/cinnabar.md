---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cinnabar
offline_file: ""
offline_thumbnail: ""
uuid: 95130566-f9c7-430c-abd3-bd129a2798cd
updated: 1484310407
title: cinnabar
categories:
    - Dictionary
---
cinnabar
     adj : of a vivid red to reddish-orange color [syn: {vermilion}, {vermillion},
            {Chinese-red}]
     n 1: a heavy reddish mineral consisting of mercuric sulfide; the
          chief source of mercury
     2: large red-and-black European moth; larvae feed on leaves of
        ragwort; introduced into United States to control ragwort
        [syn: {cinnabar moth}, {Callimorpha jacobeae}]
