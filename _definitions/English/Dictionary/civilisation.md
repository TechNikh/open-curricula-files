---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civilisation
offline_file: ""
offline_thumbnail: ""
uuid: 7f442d58-db4c-4b46-8147-91a935d5225a
updated: 1484310555
title: civilisation
categories:
    - Dictionary
---
civilisation
     n 1: the social process whereby societies achieve civilization
          [syn: {civilization}]
     2: a particular society at a particular time and place; "early
        Mayan civilization" [syn: {culture}, {civilization}]
     3: a society in an advanced state of social development (e.g.,
        with complex legal and political and religious
        organizations); "the people slowly progressed from
        barbarism to civilization" [syn: {civilization}]
     4: the quality of excellence in thought and manners and taste;
        "a man of intellectual refinement"; "he is remembered for
        his generosity and civilization" [syn: {refinement}, {civilization}]
