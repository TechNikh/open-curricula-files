---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/penetration
offline_file: ""
offline_thumbnail: ""
uuid: c2b96a0e-e9b3-477d-b8f4-cd83682b7357
updated: 1484310399
title: penetration
categories:
    - Dictionary
---
penetration
     n 1: an attack that penetrates into enemy territory [syn: {incursion}]
     2: clear or deep perception of a situation [syn: {insight}]
     3: the act of entering into or through something; "the
        penetration of upper management by women"
     4: the ability to make way into or through something; "the
        greater penetration of the new projectiles will result in
        greater injuries"
     5: the depth to which something penetrates (especially the
        depth reached by a projectile that hits a target)
     6: the act (by a man) of inserting his penis into the vagina of
        a woman
