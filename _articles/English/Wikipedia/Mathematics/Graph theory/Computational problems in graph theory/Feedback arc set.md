---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Feedback_arc_set
offline_file: ""
offline_thumbnail: ""
uuid: 624d1e90-1052-4d17-80a9-39d1394b3cbd
updated: 1484309391
title: Feedback arc set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Directed_acyclic_graph_2.svg.png
tags:
    - Example
    - Minimum feedback arc set
    - Theoretical results
    - Approximations
categories:
    - Computational problems in graph theory
---
In graph theory, a directed graph may contain directed cycles, a one-way loop of edges. In some applications, such cycles are undesirable, and we wish to eliminate them and obtain a directed acyclic graph (DAG). One way to do this is simply to drop edges from the graph to break the cycles. A feedback arc set (FAS) or feedback edge set is a set of edges which, when removed from the graph, leave a DAG. Put another way, it's a set containing at least one edge of every cycle in the graph.
