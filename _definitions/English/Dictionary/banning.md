---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banning
offline_file: ""
offline_thumbnail: ""
uuid: 3164b404-1420-451a-afae-9f3a75100db3
updated: 1484310539
title: banning
categories:
    - Dictionary
---
banning
     n : an official prohibition or edict against something [syn: {ban},
          {forbiddance}, {forbidding}]
