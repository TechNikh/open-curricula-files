---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lorraine
offline_file: ""
offline_thumbnail: ""
uuid: 61a524f9-56b4-4ee4-b504-551657c8c747
updated: 1484310553
title: lorraine
categories:
    - Dictionary
---
Lorraine
     n : an eastern French region rich in iron-ore deposits [syn: {Lothringen}]
