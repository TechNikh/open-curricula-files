---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hobby%E2%80%93Rice_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d2689c6a-0e53-4547-8c63-af22cfd7c275
updated: 1484308524
title: Hobby–Rice theorem
categories:
    - Fair division
---
In mathematics, and in particular the necklace splitting problem, the Hobby–Rice theorem is a result that is useful in establishing the existence of certain solutions. It was proved in 1965 by Charles R. Hobby and John R. Rice;[1] a simplified proof was given in 1976 by A. Pinkus.[2]
