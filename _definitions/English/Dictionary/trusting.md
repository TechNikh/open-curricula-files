---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trusting
offline_file: ""
offline_thumbnail: ""
uuid: fde9b9c9-37a4-446a-8107-db6f41efc0ea
updated: 1484310593
title: trusting
categories:
    - Dictionary
---
trusting
     adj 1: inclined to believe or confide readily; full of trust;
            "great brown eye, true and trustful"- Nordhoff & Hall
            [syn: {trustful}] [ant: {distrustful}]
     2: tending to trust; "she had an open and trusting nature"
