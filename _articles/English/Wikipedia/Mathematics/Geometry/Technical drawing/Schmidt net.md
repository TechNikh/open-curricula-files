---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Schmidt_net
offline_file: ""
offline_thumbnail: ""
uuid: 30804700-bce0-4dae-8d44-55468b8278a7
updated: 1484309332
title: Schmidt net
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Schmidtnet.svg.png
tags:
    - Construction
    - use
    - Sources
categories:
    - Technical drawing
---
The Schmidt net is a manual drafting method for the Lambert azimuthal equal-area projection using graph paper. It results in one lateral hemisphere of the Earth with the grid of parallels and meridians. The method is common in the geophysical sciences.
