---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/almost
offline_file: ""
offline_thumbnail: ""
uuid: 5095dcb2-fec1-421b-aa0b-5a58cb9a0cd2
updated: 1484310260
title: almost
categories:
    - Dictionary
---
almost
     adv : (of actions or states) slightly short of or not quite
           accomplished; `near' is sometimes used informally for
           `nearly' and `most' is sometimes used informally for
           `almost'; "the job is (just) about done"; "the baby was
           almost asleep when the alarm sounded"; "we're almost
           finished"; "the car all but ran her down"; "he nearly
           fainted"; "talked for nigh onto 2 hours"; "the
           recording is well-nigh perfect"; "virtually all the
           parties signed the contract"; "I was near exhausted by
           the run"; "most everyone agrees" [syn: {about}, {just
           about}, {most}, {all but}, {nearly}, ...
