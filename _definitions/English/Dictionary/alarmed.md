---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alarmed
offline_file: ""
offline_thumbnail: ""
uuid: d7b65af5-6b42-4b0b-a25b-b3e77c7e870b
updated: 1484310473
title: alarmed
categories:
    - Dictionary
---
alarmed
     adj : experiencing a sudden sense of danger
