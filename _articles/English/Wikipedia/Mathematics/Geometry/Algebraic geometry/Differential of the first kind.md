---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_of_the_first_kind
offline_file: ""
offline_thumbnail: ""
uuid: 9b0f709d-accd-40a4-a154-968999c58a84
updated: 1484309142
title: Differential of the first kind
categories:
    - Algebraic geometry
---
In mathematics, differential of the first kind is a traditional term used in the theories of Riemann surfaces (more generally, complex manifolds) and algebraic curves (more generally, algebraic varieties), for everywhere-regular differential 1-forms. Given a complex manifold M, a differential of the first kind ω is therefore the same thing as a 1-form that is everywhere holomorphic; on an algebraic variety V that is non-singular it would be a global section of the coherent sheaf Ω1 of Kähler differentials. In either case the definition has its origins in the theory of abelian integrals.
