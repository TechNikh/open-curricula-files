---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oxidase
offline_file: ""
offline_thumbnail: ""
uuid: 7009da4f-238c-4ca9-95d3-f97240d83888
updated: 1484308313
title: Oxidase
tags:
    - Examples
    - Oxidase test
categories:
    - Uncategorized
---
An oxidase is an enzyme that catalyzes an oxidation-reduction reaction, especially one involving dioxygen (O2) as the electron acceptor. In reactions involving donation of a hydrogen atom, oxygen is reduced to water (H2O) or hydrogen peroxide (H2O2). Some oxidation reactions, such as those involving monoamine oxidase or xanthine oxidase, typically do not involve free molecular oxygen.[1][2]
