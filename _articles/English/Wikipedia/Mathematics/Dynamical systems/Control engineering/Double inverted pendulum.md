---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_inverted_pendulum
offline_file: ""
offline_thumbnail: ""
uuid: 1278d116-b14e-4860-820b-c9e96754912a
updated: 1484308728
title: Double inverted pendulum
categories:
    - Control engineering
---
A double inverted pendulum is combination of the inverted pendulum and the double pendulum. The double inverted pendulum is unstable, meaning that it will fall down unless it is controlled in some way. The two main methods of controlling a double inverted pendulum are moving the base, as with the inverted pendulum, or by applying a torque at the pivot point between the two pendulums.[1]
