---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_computing
offline_file: ""
offline_thumbnail: ""
uuid: 0585e8d9-730f-40db-aee6-1ac027c6b1f1
updated: 1484308674
title: Natural computing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Selfassemble_Sierpinski.jpg
tags:
    - Nature-inspired models of computation
    - Cellular automata
    - Neural computation
    - Evolutionary computation
    - Swarm intelligence
    - Artificial immune systems
    - Membrane computing
    - Amorphous computing
    - Synthesizing nature by means of computing
    - Artificial life
    - Nature-inspired novel hardware
    - Molecular computing
    - Quantum computing
    - Nature as information processing
    - Systems biology
    - Synthetic biology
    - Cellular computing
categories:
    - Theoretical computer science
---
Natural computing,[1][2] also called natural computation, is a terminology introduced to encompass three classes of methods: 1) those that take inspiration from nature for the development of novel problem-solving techniques; 2) those that are based on the use of computers to synthesize natural phenomena; and 3) those that employ natural materials (e.g., molecules) to compute. The main fields of research that compose these three branches are artificial neural networks, evolutionary algorithms, swarm intelligence, artificial immune systems, fractal geometry, artificial life, DNA computing, and quantum computing, among others.
