---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chamber
offline_file: ""
offline_thumbnail: ""
uuid: 5b8b278d-afd8-42c6-98e6-e83193bd693b
updated: 1484310411
title: chamber
categories:
    - Dictionary
---
chamber
     n 1: a natural or artificial enclosed space
     2: an enclosed volume (as the aqueous chamber of the eyeball or
        the chambers of the heart)
     3: a room where a judge transacts business
     4: a deliberative or legislative or administrative or judicial
        assembly; "the upper chamber is the senate"
     5: a room used primarily for sleeping [syn: {bedroom}, {sleeping
        room}, {bedchamber}]
     v : place in a chamber
