---
version: 1
type: video
provider: YouTube
id: PYrgjMubh-c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7f526b4f-7281-4398-b7b4-6ed3e3b77529
updated: 1486069625
title: 'Fast Math Tricks - How to multiply 2 digit numbers up to 100 - the fast way!'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/PYrgjMubh-c/default.jpg
    - https://i3.ytimg.com/vi/PYrgjMubh-c/1.jpg
    - https://i3.ytimg.com/vi/PYrgjMubh-c/2.jpg
    - https://i3.ytimg.com/vi/PYrgjMubh-c/3.jpg
---
