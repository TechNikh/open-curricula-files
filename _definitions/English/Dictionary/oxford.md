---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxford
offline_file: ""
offline_thumbnail: ""
uuid: e2007790-0842-4231-96ac-28b5eb847783
updated: 1484310144
title: oxford
categories:
    - Dictionary
---
Oxford
     n 1: a city in southern England northwest of London; site of
          Oxford University
     2: a university town in northern Mississippi; home of William
        Faulkner
     3: a university in England [syn: {Oxford University}]
     4: a low shoe laced over the instep
