---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broadleaf
offline_file: ""
offline_thumbnail: ""
uuid: 21c3f7c3-75c8-41fc-8f08-5fa6e609403e
updated: 1484310435
title: broadleaf
categories:
    - Dictionary
---
broadleaf
     adj : having relatively broad rather than needle-like or
           scale-like leaves [syn: {broad-leafed}, {broad-leaved}]
     [also: {broadleaves} (pl)]
