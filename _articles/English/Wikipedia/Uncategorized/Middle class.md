---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Middle-income
offline_file: ""
offline_thumbnail: ""
uuid: b8f40994-6ced-4265-b79e-7316584caf64
updated: 1484308428
title: Middle class
tags:
    - History and evolution of the term
    - Marxism
    - Social reproduction
    - Professional-managerial class
    - Recent global growth
    - Russia
    - China
    - India
    - Africa
    - Latin America
categories:
    - Uncategorized
---
The middle class is a class of people in the middle of a social hierarchy. In Weberian socio-economic terms, the middle class is the broad group of people in contemporary society who fall socio-economically between the working class and upper class. The common measures of what constitutes middle class vary significantly among cultures. A sizable and healthy middle-class can be viewed as a characteristic of a healthy society.
