---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/programmer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409001
title: programmer
categories:
    - Dictionary
---
programmer
     n : a person who designs and writes and tests computer programs
         [syn: {computer programmer}, {coder}, {software engineer}]
