---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/estimate
offline_file: ""
offline_thumbnail: ""
uuid: d9aac522-405c-4a07-b8a2-96b27525a755
updated: 1484310244
title: estimate
categories:
    - Dictionary
---
estimate
     n 1: an approximate calculation of quantity or degree or worth;
          "an estimate of what it would cost"; "a rough idea how
          long it would take" [syn: {estimation}, {approximation},
           {idea}]
     2: a judgment of the qualities of something or somebody; "many
        factors are involved in any estimate of human life"; "in
        my estimation the boy is innocent" [syn: {estimation}]
     3: a document appraising the value of something (as for
        insurance or taxation) [syn: {appraisal}, {estimation}]
     4: a statement indicating the likely cost of some job; "he got
        an estimate from the car repair shop"
     5: the respect with which a ...
