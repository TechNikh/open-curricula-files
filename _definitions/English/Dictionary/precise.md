---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precise
offline_file: ""
offline_thumbnail: ""
uuid: 4d093202-d615-42e7-ba51-20eb59f3c111
updated: 1484310268
title: precise
categories:
    - Dictionary
---
precise
     adj 1: sharply exact or accurate or delimited; "a precise mind";
            "specified a precise amount"; "arrived at the precise
            moment" [ant: {imprecise}]
     2: (of ideas, images, representations, expressions)
        characterized by perfect conformity to fact or truth ;
        strictly correct; "a precise image"; "a precise
        measurement" [syn: {accurate}, {exact}]
