---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grothendieck%E2%80%93Teichm%C3%BCller_group'
offline_file: ""
offline_thumbnail: ""
uuid: 153a6fb9-58b0-41d4-bce5-535f5cd23170
updated: 1484308645
title: Grothendieck–Teichmüller group
categories:
    - Number theory
---
In mathematics, the Grothendieck–Teichmüller group GT is a group closely related to (and possibly equal to) the absolute Galois group of the rational numbers. It was introduced by Vladimir Drinfeld (1990) and named after Alexander Grothendieck and Oswald Teichmüller, based on Grothendieck's suggestion in his Esquisse d'un Programme to study the absolute Galois group of the rationals by relating it to its action on the Teichmüller tower of Teichmüller groupoids Tg,n, the fundamental groupoids of moduli stacks of genus g curves with n points removed. There are several minor variations of the group: a discrete version, a pro-l version, a k-pro-unipotent version, and a profinite version; ...
