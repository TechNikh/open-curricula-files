---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asserting
offline_file: ""
offline_thumbnail: ""
uuid: 03791a8f-63a6-441e-8269-738b2aeff67e
updated: 1484310183
title: asserting
categories:
    - Dictionary
---
asserting
     adj : relating to the use of or having the nature of a declaration
           [syn: {declarative}, {declaratory}, {asserting(a)}]
           [ant: {interrogative}, {interrogative}]
