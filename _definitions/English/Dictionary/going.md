---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/going
offline_file: ""
offline_thumbnail: ""
uuid: e33137c9-a044-43fc-a3d1-22e78109e256
updated: 1484310361
title: going
categories:
    - Dictionary
---
going
     adj : in full operation; "a going concern" [syn: {going(a)}]
     n 1: act of departing [syn: {departure}, {going away}, {leaving}]
     2: euphemistic expressions for death; "thousands mourned his
        passing" [syn: {passing}, {loss}, {departure}, {exit}, {expiration},
         {release}]
     3: advancing toward a goal; "persuading him was easy going";
        "the proposal faces tough sledding" [syn: {sledding}]
