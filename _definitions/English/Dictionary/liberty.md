---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberty
offline_file: ""
offline_thumbnail: ""
uuid: 6a6d9c02-d4ba-412e-aca4-6df59d21f29d
updated: 1484310547
title: liberty
categories:
    - Dictionary
---
liberty
     n 1: immunity from arbitrary exercise of authority: political
          independence [syn: {autonomy}]
     2: freedom of choice; "liberty of opinion"; "liberty of
        worship"; "liberty--perfect liberty--to think or feel or
        do just as one pleases"; "at liberty to choose whatever
        occupation one wishes"
     3: personal freedom from servitude or confinement or oppression
     4: leave granted to a sailor or naval officer [syn: {shore
        leave}]
     5: an act of undue intimacy [syn: {familiarity}, {impropriety},
         {indecorum}]
