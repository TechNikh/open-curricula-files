---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comfortable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454601
title: comfortable
categories:
    - Dictionary
---
comfortable
     adj 1: providing or experiencing physical well-being or relief
            (`comfy' is informal); "comfortable clothes";
            "comfortable suburban houses"; "made himself
            comfortable in an armchair"; "the antihistamine made
            her feel more comfortable"; "are you comfortable?";
            "feeling comfy now?" [syn: {comfy}] [ant: {uncomfortable}]
     2: free from stress or conducive to mental ease; having or
        affording peace of mind; "was settled in a comfortable
        job, one for which he was well prepared"; "the comfortable
        thought that nothing could go wrong"; "was comfortable in
        his religious beliefs"; "she's a ...
