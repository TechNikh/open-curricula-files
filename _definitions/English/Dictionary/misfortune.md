---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misfortune
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484581861
title: misfortune
categories:
    - Dictionary
---
misfortune
     n 1: unnecessary and unforeseen trouble resulting from an
          unfortunate event [syn: {bad luck}]
     2: an unfortunate state resulting from unfavorable outcomes
        [syn: {bad luck}, {tough luck}, {ill luck}] [ant: {good
        fortune}, {good fortune}]
