---
version: 1
type: video
provider: YouTube
id: LfDYGanMi6Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Protein%20Transport%20%28Mitochondria%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f7a06403-075f-4339-8d6e-ab157099eaa8
updated: 1486069666
title: Protein Transport (Mitochondria)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/LfDYGanMi6Q/default.jpg
    - https://i3.ytimg.com/vi/LfDYGanMi6Q/1.jpg
    - https://i3.ytimg.com/vi/LfDYGanMi6Q/2.jpg
    - https://i3.ytimg.com/vi/LfDYGanMi6Q/3.jpg
---
