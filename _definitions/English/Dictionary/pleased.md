---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pleased
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480761
title: pleased
categories:
    - Dictionary
---
pleased
     adj 1: experiencing or manifesting pleasure [ant: {displeased}]
     2: feeling pleasurable satisfaction over something by which you
        measures your self-worth; "proud of their child" [syn: {proud
        of(p)}]
     3: experiencing pleasure or joy; "happy you are here"; "pleased
        with the good news" [syn: {happy}]
