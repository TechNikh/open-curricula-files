---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/totally
offline_file: ""
offline_thumbnail: ""
uuid: d980b46a-a623-42dc-8ecd-07a10112900a
updated: 1484310224
title: totally
categories:
    - Dictionary
---
totally
     adv : to a complete degree or to the full or entire extent
           (`whole' is often used informally for `wholly'); "he
           was wholly convinced"; "entirely satisfied with the
           meal"; "it was completely different from what we
           expected"; "was completely at fault"; "a totally new
           situation"; "the directions were all wrong"; "it was
           not altogether her fault"; "an altogether new
           approach"; "a whole new idea" [syn: {wholly}, {entirely},
            {completely}, {all}, {altogether}, {whole}] [ant: {partially}]
