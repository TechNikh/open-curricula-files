---
version: 1
type: video
provider: YouTube
id: 9cbF9A6eQNA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4393e5f8-29c3-4a5d-9275-89075ca718f1
updated: 1486069682
title: 'Statics- Crash Course Physics #13'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/9cbF9A6eQNA/default.jpg
    - https://i3.ytimg.com/vi/9cbF9A6eQNA/1.jpg
    - https://i3.ytimg.com/vi/9cbF9A6eQNA/2.jpg
    - https://i3.ytimg.com/vi/9cbF9A6eQNA/3.jpg
---
