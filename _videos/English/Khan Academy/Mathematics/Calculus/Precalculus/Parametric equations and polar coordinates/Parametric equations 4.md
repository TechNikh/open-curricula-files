---
version: 1
type: video
provider: YouTube
id: IReD6c_njOY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Parametric%20equations%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 27126164-2163-44b3-8f46-b7e3fc409ead
updated: 1486069696
title: Parametric equations 4
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/IReD6c_njOY/default.jpg
    - https://i3.ytimg.com/vi/IReD6c_njOY/1.jpg
    - https://i3.ytimg.com/vi/IReD6c_njOY/2.jpg
    - https://i3.ytimg.com/vi/IReD6c_njOY/3.jpg
---
