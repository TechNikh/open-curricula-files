---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amusing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476501
title: amusing
categories:
    - Dictionary
---
amusing
     adj 1: providing enjoyment; pleasantly entertaining; "an amusing
            speaker"; "a diverting story"; "a fun thing to do"
            [syn: {amusive}, {diverting}, {fun(a)}]
     2: arousing or provoking laughter; "an amusing film with a
        steady stream of pranks and pratfalls"; "an amusing
        fellow"; "a comic hat"; "a comical look of surprise";
        "funny stories that made everybody laugh"; "a very funny
        writer"; "it would have been laughable if it hadn't hurt
        so much"; "a mirthful experience"; "risible courtroom
        antics" [syn: {comic}, {comical}, {funny}, {laughable}, {mirthful},
         {risible}]
