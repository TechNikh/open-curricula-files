---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/likelihood
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484382661
title: likelihood
categories:
    - Dictionary
---
likelihood
     n : the probability of a specified outcome [syn: {likeliness}, {odds}]
         [ant: {unlikelihood}, {unlikelihood}]
