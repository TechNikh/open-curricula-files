---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Milnor%E2%80%93Thurston_kneading_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 10b7326d-ca38-4659-b87d-86174acb4ee8
updated: 1484308875
title: Milnor–Thurston kneading theory
categories:
    - Topological dynamics
---
The Milnor–Thurston kneading theory is a mathematical theory which analyzes the iterates of piecewise monotone mappings of an interval into itself. The emphasis is on understanding the properties of the mapping that are invariant under topological conjugacy.
