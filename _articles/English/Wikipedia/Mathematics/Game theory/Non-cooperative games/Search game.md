---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Search_game
offline_file: ""
offline_thumbnail: ""
uuid: eb65ff29-7076-4225-bca6-efd22c4453a0
updated: 1484309057
title: Search game
categories:
    - Non-cooperative games
---
A search game is a two-person zero-sum game which takes place in a set called the search space. The searcher can choose any continuous trajectory subject to a maximal velocity constraint. It is always assumed that neither the searcher nor the hider has any knowledge about the movement of the other player until their distance apart is less than or equal to the discovery radius and at this very moment capture occurs. As mathematical models, search games can be applied to areas such as hide-and-seek games that children play or representations of some tactical military situations. The area of search games was introduced in the last chapter of Rufus Isaacs' classic book "Differential Games"[1] ...
