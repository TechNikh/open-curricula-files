---
version: 1
type: video
provider: YouTube
id: W2tiu8_ReGM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Salutations%20and%20valedictions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d52bff5a-a119-4513-9ece-8b431e968432
updated: 1486069601
title: Salutations and valedictions
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/W2tiu8_ReGM/default.jpg
    - https://i3.ytimg.com/vi/W2tiu8_ReGM/1.jpg
    - https://i3.ytimg.com/vi/W2tiu8_ReGM/2.jpg
    - https://i3.ytimg.com/vi/W2tiu8_ReGM/3.jpg
---
David and Paige, KA’s resident grammarians, talk about how to address and sign off on letters using commas! 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/commas-in-space-and-time-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/meet-the-comma-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
