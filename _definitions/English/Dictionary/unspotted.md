---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unspotted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500681
title: unspotted
categories:
    - Dictionary
---
unspotted
     adj : without soil or spot or stain [syn: {unsoiled}, {unstained}]
