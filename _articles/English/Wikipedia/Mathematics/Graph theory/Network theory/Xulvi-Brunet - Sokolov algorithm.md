---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Xulvi-Brunet_-_Sokolov_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 9d790caa-a4d5-4625-9a3e-548a0ca25a87
updated: 1484309442
title: 'Xulvi-Brunet - Sokolov algorithm'
categories:
    - Network theory
---
Xulvi-Brunet and Sokolov’s algorithm generates networks with chosen degree correlations. This method is based on link rewiring, in which the desired degree is governed by parameter ρ. By varying this single parameter it is possible to generate networks from random (when ρ = 0) to perfectly assortative or disassortative (when ρ = 1). This algorithm allows to keep network’s degree distribution unchanged when changing the value of ρ.[1]
