---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Andrew_Marshall_(foreign_policy_strategist)
offline_file: ""
offline_thumbnail: ""
uuid: f96ec10d-f967-4ebb-a780-63b9f69d9fa3
updated: 1484308509
title: Andrew Marshall (foreign policy strategist)
tags:
    - Biography
categories:
    - Game theorists
---
Andrew W. Marshall (born September 13, 1921)[1] was the director of the United States Department of Defense's Office of Net Assessment from 1973 to 2015. Appointed to the position by President Richard Nixon, Marshall remained in office during all successive administrations that followed until his retirement on January 2, 2015.[2][3][4] He was succeeded in the role by James H. Baker.[5]
