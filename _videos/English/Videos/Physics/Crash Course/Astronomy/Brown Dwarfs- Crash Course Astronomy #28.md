---
version: 1
type: video
provider: YouTube
id: 4zKVx29_A1w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Brown%20Dwarfs-%20Crash%20Course%20Astronomy%20%2328.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d46315b5-23ec-487a-a96b-d5e30a9df654
updated: 1486069684
title: 'Brown Dwarfs- Crash Course Astronomy #28'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/4zKVx29_A1w/default.jpg
    - https://i3.ytimg.com/vi/4zKVx29_A1w/1.jpg
    - https://i3.ytimg.com/vi/4zKVx29_A1w/2.jpg
    - https://i3.ytimg.com/vi/4zKVx29_A1w/3.jpg
---
