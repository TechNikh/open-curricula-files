---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hall%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 23c47b01-1251-4ab0-9350-1d9903ce50d1
updated: 1484308645
title: "Hall's conjecture"
categories:
    - Number theory
---
In mathematics, Hall's conjecture is an open question, as of 2015[update], on the differences between perfect squares and perfect cubes. It asserts that a perfect square y2 and a perfect cube x3 that are not equal must lie a substantial distance apart. This question arose from consideration of the Mordell equation in the theory of integer points on elliptic curves.
