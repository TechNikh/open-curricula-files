---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Holditch%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 740d1646-ab5a-48bb-ba2c-b418a00152d6
updated: 1484309114
title: "Holditch's theorem"
tags:
    - Observations
    - Extensions
    - Sources
categories:
    - Theorems in geometry
---
In plane geometry, Holditch's theorem states that if a chord of fixed length is allowed to rotate inside a convex closed curve, then the locus of a point on the chord a distance p from one end and a distance q from the other is a closed curve whose area is less than that of the original curve by 
  
    
      
        π
        p
        q
      
    
    {\displaystyle \pi pq}
  
. The theorem was published in 1858 by Rev. Hamnet Holditch.[1][2] While not mentioned by Holditch, the proof of the theorem requires an assumption that the chord be short enough that the traced locus is a simple closed curve.[3]
