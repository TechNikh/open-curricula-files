---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/help
offline_file: ""
offline_thumbnail: ""
uuid: e2e573a3-7479-46d6-b861-1102b0a7d087
updated: 1484310349
title: help
categories:
    - Dictionary
---
help
     n 1: the activity of contributing to the fulfillment of a need or
          furtherance of an effort or purpose; "he gave me an
          assist with the housework"; "could not walk without
          assistance"; "rescue party went to their aid"; "offered
          his help in unloading" [syn: {aid}, {assist}, {assistance}]
     2: a resource; "visual aids in teaching"; "economic assistance
        to depressed areas" [syn: {aid}, {assistance}]
     3: a means of serving; "of no avail"; "there's no help for it"
        [syn: {avail}, {service}]
     4: a person who contributes to the fulfillment of a need or
        furtherance of an effort or purpose; "my invaluable
        ...
