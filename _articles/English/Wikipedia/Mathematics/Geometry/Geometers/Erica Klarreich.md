---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Erica_Klarreich
offline_file: ""
offline_thumbnail: ""
uuid: 2d2b3951-1256-4291-a18e-38c10ef1c8ad
updated: 1484309088
title: Erica Klarreich
categories:
    - Geometers
---
As a mathematician, Klarreich is known for her theorem in geometric topology that states that the boundary of the curve complex is homeomorphic to the space of ending laminations.[1][2][3]
