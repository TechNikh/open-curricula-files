---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moist
offline_file: ""
offline_thumbnail: ""
uuid: d46edff9-f703-4b79-8447-194a860ac49d
updated: 1484310375
title: moist
categories:
    - Dictionary
---
moist
     adj : slightly wet; "clothes damp with perspiration"; "a moist
           breeze"; "eyes moist with tears" [syn: {damp}, {dampish}]
