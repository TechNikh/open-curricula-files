---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algorithm
offline_file: ""
offline_thumbnail: ""
uuid: bf26e7d1-c353-4190-ac32-1e9574a03772
updated: 1484308671
title: Algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Euclid_flowchart.svg.png
tags:
    - Informal definition
    - Formalization
    - Expressing algorithms
    - Implementation
    - Computer algorithms
    - Examples
    - Algorithm example
    - Euclid’s algorithm
    - "Computer language for Euclid's algorithm"
    - "An inelegant program for Euclid's algorithm"
    - "An elegant program for Euclid's algorithm"
    - Testing the Euclid algorithms
    - Measuring and improving the Euclid algorithms
    - Algorithmic analysis
    - Formal versus empirical
    - Execution efficiency
    - Classification
    - By implementation
    - By design paradigm
    - Optimization problems
    - By field of study
    - By complexity
    - Continuous algorithms
    - Legal issues
    - Etymology
    - 'History: Development of the notion of "algorithm"'
    - Ancient Near East
    - Discrete and distinguishable symbols
    - 'Manipulation of symbols as "place holders" for numbers: algebra'
    - Mechanical contrivances with discrete states
    - >
        Mathematics during the 19th century up to the mid-20th
        century
    - Emil Post (1936) and Alan Turing (1936–37, 1939)
    - J. B. Rosser (1939) and S. C. Kleene (1943)
    - History after 1950
    - Notes
    - Secondary references
categories:
    - Theoretical computer science
---
In mathematics and computer science, an algorithm (i/ˈælɡərɪðəm/ AL-gə-ri-dhəm) is a self-contained step-by-step set of operations to be performed. Algorithms perform calculation, data processing, and/or automated reasoning tasks.
