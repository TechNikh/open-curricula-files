---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dependent
offline_file: ""
offline_thumbnail: ""
uuid: 05e62343-9e6b-4a99-9e93-66475f1d6bc3
updated: 1484310361
title: dependent
categories:
    - Dictionary
---
dependent
     adj 1: not independent; "dependent children" [ant: {independent}]
     2: contingent on something else [syn: {dependant}, {qualified}]
     3: of a clause; unable to stand alone syntactically as a
        complete sentence; "a subordinate (or dependent) clause
        functions as a noun or adjective or adverb within a
        sentence" [syn: {subordinate}] [ant: {independent}]
     4: being under the power or sovereignty of another or others;
        "subject peoples"; "a dependent prince" [syn: {subject}]
     5: addicted to a drug [syn: {dependant}, {drug-addicted}, {hooked},
         {strung-out}]
     n : a person who relies on another person for support
         ...
