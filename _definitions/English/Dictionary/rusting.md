---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rusting
offline_file: ""
offline_thumbnail: ""
uuid: 02a1ac12-aae9-4ef1-bed6-063b72db050a
updated: 1484310375
title: rusting
categories:
    - Dictionary
---
rusting
     n : the formation of reddish-brown ferric oxides on iron by
         low-temperature oxidation in the presence of water [syn:
         {rust}]
