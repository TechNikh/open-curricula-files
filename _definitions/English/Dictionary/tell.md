---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tell
offline_file: ""
offline_thumbnail: ""
uuid: cadc8cfa-89d7-42e6-8bd3-edef717a8586
updated: 1484310337
title: tell
categories:
    - Dictionary
---
Tell
     n : a Swiss patriot who lived in the early 14th century and who
         was renowned for his skill as an archer; according to
         legend an Austrian governor compelled him to shoot an
         apple from his son's head with his crossbow (which he did
         successfully without mishap) [syn: {William Tell}]
     v 1: express in words; "He said that he wanted to marry her";
          "tell me what is bothering you"; "state your opinion";
          "state your name" [syn: {state}, {say}]
     2: let something be known; "Tell them that you will be late"
     3: narrate or give a detailed account of; "Tell what happened";
        "The father told a story to his child" [syn: ...
