---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alphabet
offline_file: ""
offline_thumbnail: ""
uuid: 3884d207-cd96-4bdd-a88f-fe2320fdc662
updated: 1484310158
title: alphabet
categories:
    - Dictionary
---
alphabet
     n 1: a character set that includes letters and is used to write a
          language
     2: the elementary stages of any subject (usually plural); "he
        mastered only the rudiments of geometry" [syn: {rudiment},
         {first rudiment}, {first principle}, {ABC}, {ABC's}, {ABCs}]
