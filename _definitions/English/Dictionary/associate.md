---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/associate
offline_file: ""
offline_thumbnail: ""
uuid: 335705f2-a18b-42c4-a5d9-c6f6da998bc2
updated: 1484310381
title: associate
categories:
    - Dictionary
---
associate
     adj : having partial rights and privileges or subordinate status;
           "an associate member"; "an associate professor" [syn: {associate(a)}]
     n 1: a person who joins with others in some activity; "he had to
          consult his associate before continuing"
     2: a person who is frequently in the company of another;
        "drinking companions"; "comrades in arms" [syn: {companion},
         {comrade}, {fellow}, {familiar}]
     3: any event that usually accompanies or is closely connected
        with another; "first was the lightning and then its
        thunderous associate"
     4: a degree granted by a two-year college on successful
        completion of the ...
