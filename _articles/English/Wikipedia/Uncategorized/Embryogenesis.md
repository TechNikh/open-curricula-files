---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Embryonic_development
offline_file: ""
offline_thumbnail: ""
uuid: fb4688cd-fb59-4c61-abe5-00819c34d1c5
updated: 1484308371
title: Embryogenesis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gray9.png
tags:
    - Fertilization and the zygote
    - Cleavage and morula
    - Formation of the blastula
    - Formation of the germ layers
    - Formation of the gastrula
    - 'Formation of the early nervous system - neural groove, tube and notochord'
    - Formation of the early septum
    - Early formation of the heart and other primitive structures
    - Somitogenesis
    - Organogenesis
categories:
    - Uncategorized
---
Embryogenesis is the process by which the embryo forms and develops. In mammals, the term refers chiefly to early stages of prenatal development, whereas the terms fetus and fetal development describe later stages.
