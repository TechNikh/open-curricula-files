---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Through_and_through
offline_file: ""
offline_thumbnail: ""
uuid: 834f8851-d348-44e0-84ff-6574359958fb
updated: 1484309321
title: Through and through
categories:
    - Symmetry
---
Through and through describes a situation where an object, real or imaginary, passes completely through another object, also real or imaginary. The phrase has several common uses:
