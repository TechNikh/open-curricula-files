---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/procure
offline_file: ""
offline_thumbnail: ""
uuid: ce688dd3-b59a-44f6-af59-459f22a5d6eb
updated: 1484310459
title: procure
categories:
    - Dictionary
---
procure
     v 1: get by special effort; "He procured extra cigarettes even
          though they were rationed" [syn: {secure}]
     2: arrange for sexual partners for others [syn: {pander}, {pimp}]
