---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/survival
offline_file: ""
offline_thumbnail: ""
uuid: 46a2d859-e835-48c2-91d4-c7d7bb8b60f3
updated: 1484310293
title: survival
categories:
    - Dictionary
---
survival
     n 1: a state of surviving; remaining alive [syn: {endurance}]
     2: a natural process resulting in the evolution of organisms
        best adapted to the environment [syn: {survival of the
        fittest}, {natural selection}, {selection}]
     3: something that survives
