---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indefinitely
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331541
title: indefinitely
categories:
    - Dictionary
---
indefinitely
     adv : to an indefinite extent; for an indefinite time; "this could
           go on indefinitely"
