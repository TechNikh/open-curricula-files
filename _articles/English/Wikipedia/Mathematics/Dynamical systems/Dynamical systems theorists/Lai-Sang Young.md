---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lai-Sang_Young
offline_file: ""
offline_thumbnail: ""
uuid: 1ad9e5b6-e137-4e32-8675-9a6fde1f8c89
updated: 1484308699
title: Lai-Sang Young
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Lai_Sang_Young.jpg
tags:
    - Education and career
    - Awards and honors
    - Selected publications
categories:
    - Dynamical systems theorists
---
Lai-Sang Lily Young (born 1952) is a mathematician from Hong Kong, who holds the Henry & Lucy Moses Professorship of Science and is a professor of mathematics and neural science at the Courant Institute of Mathematical Sciences of New York University. Her research interests include dynamical systems, ergodic theory, chaos theory, probability theory, statistical mechanics, and neuroscience.[1] She is particularly known for introducing the method of Markov returns in 1998, which she used to prove exponential correlation delay in Sinai billiards and other hyperbolic dynamical systems.[2]
