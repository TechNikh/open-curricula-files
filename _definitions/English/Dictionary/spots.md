---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spots
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484381521
title: spots
categories:
    - Dictionary
---
spots
     n : spots before the eyes caused by opaque cell fragments in the
         vitreous humor and lens [syn: {musca volitans}, {muscae
         volitantes}, {floater}]
