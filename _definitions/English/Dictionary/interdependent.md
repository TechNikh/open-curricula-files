---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interdependent
offline_file: ""
offline_thumbnail: ""
uuid: 652abc16-1868-406a-8021-b63cd839175b
updated: 1484310365
title: interdependent
categories:
    - Dictionary
---
interdependent
     adj : mutually dependent [syn: {mutualist}, {mutually beneficial}]
