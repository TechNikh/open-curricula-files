---
version: 1
type: article
id: https://en.wikipedia.org/wiki/12_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 6bba95b9-8619-4ef7-97f0-824c07efe0c8
updated: 1484308948
title: 12 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Flag_of_Europe.svg.png
tags:
    - Name
    - In mathematics
    - List of basic calculations
    - In numeral systems
    - In science
    - Astronomy
    - In religion and mythology
    - In time
    - In sports
    - In technology
    - In the arts
    - Film
    - Television
    - Theatre
    - Literature
    - Music
    - Art theory
    - Games
    - In other fields
    - Notes
    - Citations
    - Bibliography
categories:
    - Integers
---
12 (twelve i/ˈtwɛlv/) is the natural number following 11 and preceding 13. The product of the first three factorials, twelve is a superior highly composite number, divisible by 2, 3, 4, and 6. It is central to many systems of counting, including the Western calendar and units of time, and frequently appears in the Abrahamic religions.
