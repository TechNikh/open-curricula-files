---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amplification
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484427061
title: amplification
categories:
    - Dictionary
---
amplification
     n 1: addition of extra material or illustration or clarifying
          detail; "a few remarks added in amplification and
          defense"; "an elaboration of the idea followed" [syn: {elaboration}]
     2: the amount of increase in signal power or voltage or current
        expressed as the ratio of output to input [syn: {gain}]
     3: (electronics) the act of increasing voltage or power or
        current
