---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crucial
offline_file: ""
offline_thumbnail: ""
uuid: 68daf55c-6c4f-47c7-8043-40832190b070
updated: 1484310244
title: crucial
categories:
    - Dictionary
---
crucial
     adj 1: of extreme importance; vital to the resolution of a crisis;
            "a crucial moment in his career"; "a crucial
            election"; "a crucial issue for women" [syn: {important}]
            [ant: {noncrucial}]
     2: having crucial relevance; "crucial to the case"; "relevant
        testimony" [syn: {relevant}]
     3: of the greatest importance; "the all-important subject of
        disarmament"; "crucial information"; "in chess cool nerves
        are of the essence" [syn: {all-important(a)}, {all
        important(p)}, {essential}, {of the essence(p)}]
     4: having the power or quality of deciding; "the crucial
        experiment"; "cast the deciding ...
