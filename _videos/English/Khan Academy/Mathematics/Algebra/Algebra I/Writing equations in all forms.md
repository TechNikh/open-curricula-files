---
version: 1
type: video
provider: YouTube
id: -6Fu2T_RSGM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Writing%20equations%20in%20all%20forms.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 534323e7-0e2c-48fe-bfc9-c6ede95489dc
updated: 1486069561
title: Writing equations in all forms
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/-6Fu2T_RSGM/default.jpg
    - https://i3.ytimg.com/vi/-6Fu2T_RSGM/1.jpg
    - https://i3.ytimg.com/vi/-6Fu2T_RSGM/2.jpg
    - https://i3.ytimg.com/vi/-6Fu2T_RSGM/3.jpg
---
