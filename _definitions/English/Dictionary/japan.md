---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/japan
offline_file: ""
offline_thumbnail: ""
uuid: 309974b1-57be-42e6-a68a-30afb67db3ba
updated: 1484310238
title: japan
categories:
    - Dictionary
---
Japan
     n 1: a string of more than 3,000 islands east of Asia extending
          1,300 miles between the Sea of Japan and the western
          Pacific Ocean [syn: {Japanese Islands}, {Japanese
          Archipelago}]
     2: a constitutional monarchy occupying the Japanese
        Archipelago; a world leader in electronics and automobile
        manufacture and ship building [syn: {Nippon}, {Nihon}]
     3: lacquerware decorated and varnished in the Japanese manner
        with a glossy durable black lacquer
     4: lacquer with a durable glossy black finish, originally from
        the orient
     v : coat with a lacquer, as done in Japan
