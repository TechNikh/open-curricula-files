---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shiva
offline_file: ""
offline_thumbnail: ""
uuid: 66f358c1-eca7-4a62-ad11-3a84093b1ca1
updated: 1484310183
title: shiva
categories:
    - Dictionary
---
shiva
     n 1: (Judaism) a period of seven days of mourning after the death
          of close relative; "the family is sitting shiva" [syn: {shivah},
           {shibah}]
     2: the Destroyer; one of the three major divinities in the
        later Hindu pantheon [syn: {Siva}]
