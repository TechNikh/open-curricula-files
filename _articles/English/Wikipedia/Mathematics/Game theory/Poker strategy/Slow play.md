---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slow_play
offline_file: ""
offline_thumbnail: ""
uuid: 9a1ba5a3-4c34-4d61-b523-cc794e5c38bd
updated: 1484309062
title: Slow play
tags:
    - Relationship between slow playing and bluffing
    - Check raising as a slow play
    - Fishing for the overcall
    - Notes
categories:
    - Poker strategy
---
Slow playing (also called sandbagging or trapping) is deceptive play in poker that is roughly the opposite of bluffing: betting weakly or passively with a strong holding rather than betting aggressively with a weak one. The flat call is one such play. The objective of the passive slow play is to lure opponents into a pot who might fold to a raise, or to cause them to bet more strongly than they would if the player had played aggressively (bet or raised). Slow playing sacrifices protection against hands that may improve and risks losing the pot-building value of a bet if the opponent also checks.
