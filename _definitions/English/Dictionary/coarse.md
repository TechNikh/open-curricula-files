---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coarse
offline_file: ""
offline_thumbnail: ""
uuid: 584a053d-3516-4402-bd83-b6845dc71947
updated: 1484310531
title: coarse
categories:
    - Dictionary
---
coarse
     adj 1: of texture; large-grained or rough to the touch; "coarse
            meal"; "coarse sand"; "a coarse weave" [ant: {fine}]
     2: lacking refinement or cultivation or taste; "he had coarse
        manners but a first-rate mind"; "behavior that branded him
        as common"; "an untutored and uncouth human being"; "an
        uncouth soldier--a real tough guy"; "appealing to the
        vulgar taste for violence"; "the vulgar display of the
        newly rich" [syn: {common}, {rough-cut}, {uncouth}, {vulgar}]
     3: of low or inferior quality or value; "of what coarse metal
        ye are molded"- Shakespeare; "produced...the common cloths
        used by the poorer ...
