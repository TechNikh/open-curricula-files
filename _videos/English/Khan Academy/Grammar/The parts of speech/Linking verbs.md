---
version: 1
type: video
provider: YouTube
id: 4dPbciiZSbo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Linking%20verbs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9fd42786-5171-4f71-b5a6-ba4abbff13a3
updated: 1486069601
title: Linking verbs
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/4dPbciiZSbo/default.jpg
    - https://i3.ytimg.com/vi/4dPbciiZSbo/1.jpg
    - https://i3.ytimg.com/vi/4dPbciiZSbo/2.jpg
    - https://i3.ytimg.com/vi/4dPbciiZSbo/3.jpg
---
In a previous video, we established that one thing verbs can do is show actions, like "the bear runs." Verbs can also link ideas to one another, like "the bear looks cheerful."

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/helping-verbs-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/the-future-tense-verbs-the-parts-of-speech?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
