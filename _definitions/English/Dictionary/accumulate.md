---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accumulate
offline_file: ""
offline_thumbnail: ""
uuid: f61d5355-60d3-4f38-9128-b88b121cb412
updated: 1484310286
title: accumulate
categories:
    - Dictionary
---
accumulate
     v 1: get or gather together; "I am accumulating evidence for the
          man's unfaithfulness to his wife"; "She is amassing a
          lot of data for her thesis"; "She rolled up a small
          fortune" [syn: {roll up}, {collect}, {pile up}, {amass},
           {compile}, {hoard}]
     2: collect or gather; "Journals are accumulating in my office";
        "The work keeps piling up" [syn: {cumulate}, {conglomerate},
         {pile up}, {gather}, {amass}]
