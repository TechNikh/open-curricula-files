---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strength_of_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: e2e8db00-7108-46d8-843f-54de26d32b2c
updated: 1484309402
title: Strength of a graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Force-wiki.jpg
tags:
    - Definitions
    - Complexity
    - Properties
categories:
    - Graph connectivity
---
In the branch of mathematics called graph theory, the strength of an undirected graph corresponds to the minimum ratio edges removed/components created in a decomposition of the graph in question. It is a method to compute partitions of the set of vertices and detect zones of high concentration of edges.
