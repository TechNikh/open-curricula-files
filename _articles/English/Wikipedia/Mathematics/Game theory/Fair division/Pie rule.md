---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pie_rule
offline_file: ""
offline_thumbnail: ""
uuid: 77065d54-6def-4993-b448-8439c8817339
updated: 1484308524
title: Pie rule
categories:
    - Fair division
---
The pie rule, sometimes referred to as the swap rule, is a rule used to balance abstract strategy games where a first-move advantage has been demonstrated. Its use was first reported in 1909 for a game in the Mancala family.[1] Among modern games, Hex uses this rule. Twixt in tournament play uses a swap rule.[2]
