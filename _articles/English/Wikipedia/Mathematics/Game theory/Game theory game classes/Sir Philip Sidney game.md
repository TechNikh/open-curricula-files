---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sir_Philip_Sidney_game
offline_file: ""
offline_thumbnail: ""
uuid: acb574cf-0d4f-4c6d-a3ee-3b89b7879703
updated: 1484309040
title: Sir Philip Sidney game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sir_Philip_Sidney_from_NPG.jpg
tags:
    - The phenomenon
    - The game
    - Criticisms
categories:
    - Game theory game classes
---
In biology and game theory, the Sir Philip Sidney game is used as a model for the evolution and maintenance of informative communication between relatives. Developed by John Maynard Smith as a model for chick begging behavior, it has been studied extensively including the development of many modified versions.[1]
