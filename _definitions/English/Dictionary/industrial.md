---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrial
offline_file: ""
offline_thumbnail: ""
uuid: e36a1c00-5e0a-4711-b393-dc2050def098
updated: 1484310384
title: industrial
categories:
    - Dictionary
---
industrial
     adj 1: of or relating to or resulting from industry; "industrial
            output"
     2: having highly developed industries; "the industrial
        revolution"; "an industrial nation" [ant: {nonindustrial}]
     3: employed in industry; "industrial workers"; "the industrial
        term in use among professional thieves"
     4: employed in industry; "the industrial classes"; "industrial
        work"
     5: suitable to stand up to hard wear; "industrial carpeting"
