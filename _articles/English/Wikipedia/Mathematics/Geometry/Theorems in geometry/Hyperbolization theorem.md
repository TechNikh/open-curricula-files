---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolization_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2dd4fd05-2fde-48ae-a5ca-12ecf847f7e6
updated: 1484309114
title: Hyperbolization theorem
tags:
    - Statement
    - Manifolds with boundary
    - Proofs
    - Manifolds that fiber over the circle
    - Manifolds that do not fiber over the circle
categories:
    - Theorems in geometry
---
In geometry, Thurston's geometrization theorem or hyperbolization theorem implies that closed atoroidal Haken manifolds are hyperbolic, and in particular satisfy the Thurston conjecture.
