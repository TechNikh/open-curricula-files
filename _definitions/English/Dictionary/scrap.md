---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scrap
offline_file: ""
offline_thumbnail: ""
uuid: 16767fd6-25d5-4a7b-999a-0ab658fcbde4
updated: 1484310240
title: scrap
categories:
    - Dictionary
---
scrap
     adj : disposed of as useless; "waste paper" [syn: {cast-off(a)}, {discarded},
            {junked}, {scrap(a)}, {waste}]
     n 1: a small fragment of something broken off from the whole; "a
          bit of rock caught him in the eye" [syn: {bit}, {chip},
          {flake}, {fleck}]
     2: worthless material that is to be disposed of [syn: {rubbish},
         {trash}]
     3: a small piece of something that is left over after the rest
        has been used; "she jotted it on a scrap of paper"; "there
        was not a scrap left"
     4: the act of fighting; any contest or struggle; "a fight broke
        out at the hockey game"; "there was fighting in the
        streets"; ...
