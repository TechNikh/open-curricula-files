---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Open_coloring_axiom
offline_file: ""
offline_thumbnail: ""
uuid: 85f10128-2472-4e0b-964a-13c7a2e50ed5
updated: 1484309509
title: Open coloring axiom
categories:
    - Set theory
---
In mathematical set theory, the open coloring axiom (abbreviated OCA) is an axiom about coloring edges of a graph whose vertices are a subset of the real numbers: two different versions were introduced by Abraham, Rubin & Shelah (1985) and by Todorčević (1989). The open coloring axiom follows from the proper forcing axiom.
