---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enlarged_liver
offline_file: ""
offline_thumbnail: ""
uuid: c7fcde97-4ddb-4a3f-8c75-358bdeb5c2ca
updated: 1484308371
title: Hepatomegaly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Se000.jpg
tags:
    - Symptoms/signs
    - Causes
    - Infective
    - Neoplastic
    - Biliary
    - Metabolic
    - Drugs (and alcohol)
    - Congenital
    - Others
    - Mechanism
    - Diagnosis
    - Treatment
categories:
    - Uncategorized
---
Hepatomegaly is the condition of having an enlarged liver.[1] It is a non-specific medical sign having many causes, which can broadly be broken down into infection, hepatic tumours, or metabolic disorder. Often, hepatomegaly will present as an abdominal mass. Depending on the cause, it may sometimes present along with jaundice.[2]
