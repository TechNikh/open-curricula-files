---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guarded
offline_file: ""
offline_thumbnail: ""
uuid: 62c95cfa-cc79-4804-8970-f307e3f6cfa3
updated: 1484310533
title: guarded
categories:
    - Dictionary
---
guarded
     adj 1: very reluctant to give out information [syn: {noncommittal},
             {unrevealing}]
     2: prudent; "guarded optimism" [syn: {restrained}]
