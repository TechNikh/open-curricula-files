---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Post-tropical_cyclone
offline_file: ""
offline_thumbnail: ""
uuid: 2fa4e58f-b300-489e-838e-de5132d40218
updated: 1484308875
title: Post-tropical cyclone
tags:
    - Formation
    - Impacts
    - Origin
    - Synonym
categories:
    - Vortices
---
Not all systems fall into the above two classes. According to the guideline, a system without frontal characteristics but with maximum winds above 34 knots may not be designated as a remnant low. It should be merely described as post-tropical.[4] A few examples falling into this gray area are listed below.
