---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haunt
offline_file: ""
offline_thumbnail: ""
uuid: 1ff7165c-f030-47ed-a548-b3c3dd9e6d2a
updated: 1484310290
title: haunt
categories:
    - Dictionary
---
haunt
     n : a frequently visited place [syn: {hangout}, {resort}, {repair},
          {stamping ground}]
     v 1: follow stealthily or recur constantly and spontaneously to;
          "her ex-boyfriend stalked her"; "the ghost of her mother
          haunted her" [syn: {stalk}]
     2: haunt like a ghost; pursue; "Fear of illness haunts her"
        [syn: {obsess}, {ghost}]
     3: be a regular or frequent visitor to a certain place; "She
        haunts the ballet" [syn: {frequent}]
