---
version: 1
type: video
provider: YouTube
id: ATmfCYvO6VE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7cbf975f-6439-44ec-8aeb-7ba8bd49218a
updated: 1486069619
title: Example 2 Using the quadratic formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/ATmfCYvO6VE/default.jpg
    - https://i3.ytimg.com/vi/ATmfCYvO6VE/1.jpg
    - https://i3.ytimg.com/vi/ATmfCYvO6VE/2.jpg
    - https://i3.ytimg.com/vi/ATmfCYvO6VE/3.jpg
---
