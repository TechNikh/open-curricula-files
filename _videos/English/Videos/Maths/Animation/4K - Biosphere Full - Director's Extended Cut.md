---
version: 1
type: video
provider: YouTube
id: I54vzcQmnKY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6144ee04-342c-4c3d-8d67-3799ce49f3d0
updated: 1486069645
title: "4K | Biosphere Full - Director's Extended Cut"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/I54vzcQmnKY/default.jpg
    - https://i3.ytimg.com/vi/I54vzcQmnKY/1.jpg
    - https://i3.ytimg.com/vi/I54vzcQmnKY/2.jpg
    - https://i3.ytimg.com/vi/I54vzcQmnKY/3.jpg
---
