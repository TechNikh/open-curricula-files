---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scale_invariance
offline_file: ""
offline_thumbnail: ""
uuid: 7384820b-bed5-4783-93c7-e63e51d5f574
updated: 1484308757
title: Scale invariance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Wiener_process_animated.gif
tags:
    - Scale-invariant curves and self-similarity
    - Projective geometry
    - Fractals
    - Scale invariance in stochastic processes
    - Scale invariant Tweedie distributions
    - Cosmology
    - Scale invariance in classical field theory
    - Scale invariance of field configurations
    - Classical electromagnetism
    - Massless scalar field theory
    - φ4 theory
    - Scale invariance in quantum field theory
    - Quantum electrodynamics
    - Massless scalar field theory
    - Conformal field theory
    - Scale and conformal anomalies
    - Phase transitions
    - The Ising model
    - CFT description
    - Schramm–Loewner evolution
    - Universality
    - Other examples of scale invariance
    - Newtonian fluid mechanics with no applied forces
    - Computer vision
categories:
    - Critical phenomena
---
In physics, mathematics, statistics, and economics, scale invariance is a feature of objects or laws that do not change if scales of length, energy, or other variables, are multiplied by a common factor. The technical term for this transformation is a dilatation (also known as dilation), and the dilatations can also form part of a larger conformal symmetry.
