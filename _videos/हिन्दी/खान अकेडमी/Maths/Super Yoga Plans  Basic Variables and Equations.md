---
version: 1
type: video
provider: YouTube
id: Y_B_GE9h9FI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0a7f7d5c-463f-4a46-85b7-a8b5dc1d31c3
updated: 1486069616
title: 'Super Yoga Plans  Basic Variables and Equations'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y_B_GE9h9FI/default.jpg
    - https://i3.ytimg.com/vi/Y_B_GE9h9FI/1.jpg
    - https://i3.ytimg.com/vi/Y_B_GE9h9FI/2.jpg
    - https://i3.ytimg.com/vi/Y_B_GE9h9FI/3.jpg
---
