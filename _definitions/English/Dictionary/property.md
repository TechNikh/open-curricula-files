---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/property
offline_file: ""
offline_thumbnail: ""
uuid: 78d81cac-b91f-4180-a94b-d92774d63d30
updated: 1484310277
title: property
categories:
    - Dictionary
---
property
     n 1: any area set aside for a particular purpose; "who owns this
          place?"; "the president was concerned about the property
          across from the White House" [syn: {place}]
     2: something owned; any tangible or intangible possession that
        is owned by someone; "that hat is my property"; "he is a
        man of property"; [syn: {belongings}, {holding}, {material
        possession}]
     3: a basic or essential attribute shared by all members of a
        class; "a study of the physical properties of atomic
        particles"
     4: a construct whereby objects or individuals can be
        distinguished; "self-confidence is not an endearing
        ...
