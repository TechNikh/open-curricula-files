---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Self-organized_criticality_control
offline_file: ""
offline_thumbnail: ""
uuid: 75dd8617-a328-427f-be9c-00b6a3c64b60
updated: 1484308713
title: Self-organized criticality control
tags:
    - Self-organized criticality control schemes
    - Applications
categories:
    - Chaos theory
---
In applied physics, the concept of controlling self-organized criticality refers to the control of processes by which a self-organized system dissipates energy. The objective of the control is to reduce the probability of occurrence of and size of energy dissipation bursts, often called avalanches, of self-organized systems. Dissipation of energy in a self-organized critical system into a lower energy state can be costly for society, since it depends on avalanches of all sizes usually following a kind of power law distribution and large avalanches can be damaging and disruptive.[1][2][3]
