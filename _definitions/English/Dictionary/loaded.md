---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loaded
offline_file: ""
offline_thumbnail: ""
uuid: 9c49d599-1cfd-4568-ac11-b4f243d510ea
updated: 1484310518
title: loaded
categories:
    - Dictionary
---
loaded
     adj 1: filled with a great quantity; "a tray loaded with dishes";
            "table laden with food"; "`ladened' is not current
            usage" [syn: {laden}, {ladened}]
     2: (of weapons) charged with ammunition; "a loaded gun" [ant: {unloaded}]
     3: (of statements or questions) charged with associative
        significance and often meant to mislead or influence; "a
        loaded question"
     4: having an abundant supply of money or possessions of value;
        "an affluent banker"; "a speculator flush with cash"; "not
        merely rich but loaded"; "moneyed aristocrats"; "wealthy
        corporations" [syn: {affluent}, {flush}, {moneyed}, {wealthy}]
     5: ...
