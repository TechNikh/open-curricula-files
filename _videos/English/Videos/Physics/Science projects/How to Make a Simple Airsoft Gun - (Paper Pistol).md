---
version: 1
type: video
provider: YouTube
id: 1z_51PUVrlY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ccb42b9d-5da3-4af7-bd7a-c605506a7247
updated: 1486069632
title: 'How to Make a Simple Airsoft Gun - (Paper Pistol)'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/1z_51PUVrlY/default.jpg
    - https://i3.ytimg.com/vi/1z_51PUVrlY/1.jpg
    - https://i3.ytimg.com/vi/1z_51PUVrlY/2.jpg
    - https://i3.ytimg.com/vi/1z_51PUVrlY/3.jpg
---
