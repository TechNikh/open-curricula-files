---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Repeating_decimal
offline_file: ""
offline_thumbnail: ""
uuid: 40cfa82c-1c3a-47b8-90be-63589751defd
updated: 1484308924
title: Repeating decimal
tags:
    - Background
    - Notation
    - Decimal expansion and recurrence sequence
    - >
        Every rational number is either a terminating or repeating
        decimal
    - Every repeating or terminating decimal is a rational number
    - Table of values
    - Fractions with prime denominators
    - Cyclic numbers
    - Other reciprocals of primes
    - Totient rule
    - Reciprocals of composite integers coprime to 10
    - Reciprocals of integers not co-prime to 10
    - Converting repeating decimals to fractions
    - A shortcut
    - Repeating decimals as infinite series
    - Multiplication and cyclic permutation
    - Other properties of repetend lengths
    - Extension to other bases
    - Applications to cryptography
categories:
    - Elementary arithmetic
---
A repeating or recurring decimal is decimal representation of a number whose decimal digits are periodic (repeating its values at regular intervals) and the infinitely-repeated portion is not zero. It can be shown that a number is rational if and only if its decimal representation is repeating or terminating (i.e. all except finitely many digits are zero). For example, the decimal representation of ⅓ becomes periodic just after the decimal point, repeating the single digit "3" forever, i.e. 0.333…. A more complicated example is 3227/555, whose decimal becomes periodic after the second digit following the decimal point and then repeats the sequence "144" forever, i.e. 5.8144144144…. At ...
