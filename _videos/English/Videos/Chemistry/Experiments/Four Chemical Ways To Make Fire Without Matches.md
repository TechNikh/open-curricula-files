---
version: 1
type: video
provider: YouTube
id: hDct3tV12Ns
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6d537406-838f-456e-a52d-c89824beb681
updated: 1486069660
title: Four Chemical Ways To Make Fire Without Matches
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/hDct3tV12Ns/default.jpg
    - https://i3.ytimg.com/vi/hDct3tV12Ns/1.jpg
    - https://i3.ytimg.com/vi/hDct3tV12Ns/2.jpg
    - https://i3.ytimg.com/vi/hDct3tV12Ns/3.jpg
---
