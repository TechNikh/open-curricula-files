---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rota%E2%80%93Baxter_algebra'
offline_file: ""
offline_thumbnail: ""
uuid: 33136f45-ea49-468a-beb9-6bb706dd9c2e
updated: 1484308605
title: Rota–Baxter algebra
tags:
    - Definition and first properties
    - Examples
    - Spitzer identity
    - Bohnenblust–Spitzer identity
    - Notes
categories:
    - Combinatorics
---
In mathematics, a Rota–Baxter algebra is an algebra, usually over a field k, together with a particular k-linear map R which satisfies the weight-θ Rota–Baxter identity. It appeared first in the work of the American mathematician Glen E. Baxter[1] in the realm of probability theory. Baxter's work was further explored from different angles by Gian-Carlo Rota,[2][3][4] Pierre Cartier,[5] and Frederic V. Atkinson,[6] among others. Baxter’s derivation of this identity that later bore his name emanated from some of the fundamental results of the famous probabilist Frank Spitzer in random walk theory.[7][8]
