---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/builder
offline_file: ""
offline_thumbnail: ""
uuid: 41222524-1e7b-4c75-b6b4-1db49fd82797
updated: 1484310156
title: builder
categories:
    - Dictionary
---
builder
     n 1: a substance added to soaps or detergents to increase their
          cleansing action [syn: {detergent builder}]
     2: someone who contracts for and supervises construction (as of
        a building) [syn: {constructor}]
