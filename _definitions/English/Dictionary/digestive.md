---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digestive
offline_file: ""
offline_thumbnail: ""
uuid: 9750a3b9-a53e-4425-bc83-23fc5429d2c4
updated: 1484310359
title: digestive
categories:
    - Dictionary
---
digestive
     adj : relating to or having the power to cause or promote
           digestion; "digestive juices"; "a digestive enzyme";
           "digestive ferment"
