---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graphically
offline_file: ""
offline_thumbnail: ""
uuid: e14831eb-3022-4d94-b0a5-4dfe0fa19f0c
updated: 1484310271
title: graphically
categories:
    - Dictionary
---
graphically
     adv 1: in a diagrammatic manner; "the landscape unit drawn
            diagrammatically illustrates the gentle rolling
            relief, with a peat-filled basin" [syn: {diagrammatically}]
     2: with respect to graphic aspects; "graphically interesting
        designs"
     3: in a graphic way; "he described the event graphically"
