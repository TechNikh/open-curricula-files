---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indium
offline_file: ""
offline_thumbnail: ""
uuid: 08650aa9-2277-4626-b055-c18a71edbe16
updated: 1484310399
title: indium
categories:
    - Dictionary
---
indium
     n : a rare soft silvery metallic element; occurs in small
         quantities in sphalerite [syn: {In}, {atomic number 49}]
