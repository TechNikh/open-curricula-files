---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/despair
offline_file: ""
offline_thumbnail: ""
uuid: 1f4a5701-cdf4-46fa-9867-a6159898da50
updated: 1484310565
title: despair
categories:
    - Dictionary
---
despair
     n 1: a state in which everything seems wrong and will turn out
          badly; "they were rescued from despair at the last
          minute" [syn: {desperation}]
     2: the feeling that everything is wrong and nothing will turn
        out well [ant: {hope}]
     v : abandon hope; give up hope; lose heart; "Don't despair--help
         is on the way!" [ant: {hope}]
