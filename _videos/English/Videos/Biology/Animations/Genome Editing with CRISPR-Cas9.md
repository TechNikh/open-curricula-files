---
version: 1
type: video
provider: YouTube
id: 2pp17E4E-O8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Genome%20Editing%20with%20CRISPR-Cas9.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1d840068-3ccc-43f8-b936-f0a23b5f973f
updated: 1486069670
title: Genome Editing with CRISPR-Cas9
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/2pp17E4E-O8/default.jpg
    - https://i3.ytimg.com/vi/2pp17E4E-O8/1.jpg
    - https://i3.ytimg.com/vi/2pp17E4E-O8/2.jpg
    - https://i3.ytimg.com/vi/2pp17E4E-O8/3.jpg
---
