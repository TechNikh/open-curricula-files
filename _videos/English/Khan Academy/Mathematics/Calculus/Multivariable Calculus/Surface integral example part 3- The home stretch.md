---
version: 1
type: video
provider: YouTube
id: uzOXGgAgmPs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20example%20part%203-%20The%20home%20stretch.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f7809b7c-5e3c-43d6-aa37-5f02197df967
updated: 1486069689
title: 'Surface integral example part 3- The home stretch'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/uzOXGgAgmPs/default.jpg
    - https://i3.ytimg.com/vi/uzOXGgAgmPs/1.jpg
    - https://i3.ytimg.com/vi/uzOXGgAgmPs/2.jpg
    - https://i3.ytimg.com/vi/uzOXGgAgmPs/3.jpg
---
