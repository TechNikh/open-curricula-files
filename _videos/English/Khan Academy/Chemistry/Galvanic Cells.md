---
version: 1
type: video
provider: YouTube
id: N4L3dDIixSU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Galvanic%20Cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92288fa8-4dd9-4e14-8e8f-cca1c0d0c25f
updated: 1486069583
title: Galvanic Cells
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/N4L3dDIixSU/default.jpg
    - https://i3.ytimg.com/vi/N4L3dDIixSU/1.jpg
    - https://i3.ytimg.com/vi/N4L3dDIixSU/2.jpg
    - https://i3.ytimg.com/vi/N4L3dDIixSU/3.jpg
---
Redox reactions to drive Galvanic Cells.
