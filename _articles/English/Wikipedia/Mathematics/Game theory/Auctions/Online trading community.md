---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Online_trading_community
offline_file: ""
offline_thumbnail: ""
uuid: 1a0c168b-0191-476f-b2e5-6f172e79506d
updated: 1484309013
title: Online trading community
tags:
    - History
    - Formal trading communities
    - Trading communities
    - General rules of conduct
    - Trading circle
    - Trading Portal
    - Examples
categories:
    - Auctions
---
An online trading community provides participants with a structured method for trading, bartering, or selling goods and services. These communities often have forums and chatrooms designed to facilitate communication between the members. An online trading community can be likened electronic equivalent of a bazaar, flea market, or garage sale.
