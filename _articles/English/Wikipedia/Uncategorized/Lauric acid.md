---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laurate
offline_file: ""
offline_thumbnail: ""
uuid: e40fd1d2-be70-4265-a569-7e549ba15eae
updated: 1484308409
title: Lauric acid
tags:
    - Occurrence
    - In various plants
    - Properties
    - Niche uses
    - Potential medicinal properties
categories:
    - Uncategorized
---
Lauric acid or systematically, dodecanoic acid, is a saturated fatty acid with a 12-carbon atom chain, thus falling into the medium chain fatty acids, is a white, powdery solid with a faint odor of baby oil or soap. The salts and esters of lauric acid are known as laurates.
