---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accountant
offline_file: ""
offline_thumbnail: ""
uuid: 70b58e5e-7bd8-42cd-9c01-575b639d53a0
updated: 1484310521
title: accountant
categories:
    - Dictionary
---
accountant
     n : someone who maintains and audits business accounts [syn: {comptroller},
          {controller}]
