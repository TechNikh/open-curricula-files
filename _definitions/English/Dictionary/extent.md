---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extent
offline_file: ""
offline_thumbnail: ""
uuid: d25f0d3f-8409-4be8-9cdf-6bed1ab55acb
updated: 1484310254
title: extent
categories:
    - Dictionary
---
extent
     n 1: the point or degree to which something extends; "the extent
          of the damage"; "the full extent of the law"; "to a
          certain extent she was right"
     2: the distance or area or volume over which something extends;
        "the vast extent of the desert"; "an orchard of
        considerable extent"
