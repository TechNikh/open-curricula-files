---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Modular_invariance
offline_file: ""
offline_thumbnail: ""
uuid: 1d761bd8-db7b-4e90-8897-099f279f6e89
updated: 1484309312
title: Modular invariance
categories:
    - Symmetry
---
In theoretical physics, modular invariance is the invariance under the group such as SL(2,Z) of large diffeomorphisms of the torus. The name comes from the classical name modular group of this group, as in modular form theory.
