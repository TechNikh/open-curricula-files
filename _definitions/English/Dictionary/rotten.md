---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rotten
offline_file: ""
offline_thumbnail: ""
uuid: 256dbd9f-b3ab-40aa-85ec-281aacf2e89d
updated: 1484310156
title: rotten
categories:
    - Dictionary
---
rotten
     adj 1: very bad; "a lousy play"; "it's a stinking world" [syn: {icky},
             {crappy}, {lousy}, {shitty}, {stinking}, {stinky}]
     2: having rotted or disintegrated; usually implies foulness;
        "dead and rotten in his grave" [ant: {unrotten}]
     3: damaged by decay; hence unsound and useless; "rotten floor
        boards"; "rotted beams"; "a decayed foundation" [syn: {decayed},
         {rotted}]
