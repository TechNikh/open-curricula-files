---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graphical
offline_file: ""
offline_thumbnail: ""
uuid: 0e48362b-471e-4064-89de-16c0f0bc218d
updated: 1484310138
title: graphical
categories:
    - Dictionary
---
graphical
     adj 1: relating to or presented by a graph; "a graphic presentation
            of the data" [syn: {graphic}]
     2: written or drawn or engraved; "graphic symbols" [syn: {graphic},
         {in writing(p)}]
