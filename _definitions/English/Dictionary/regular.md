---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regular
offline_file: ""
offline_thumbnail: ""
uuid: bf642b3d-28a6-4a49-baac-9f0f41444dd9
updated: 1484310366
title: regular
categories:
    - Dictionary
---
regular
     adj 1: in accordance with fixed order or procedure or principle;
            "his regular calls on his customers"; "regular meals";
            "regular duties" [ant: {irregular}]
     2: often used as intensifiers; "a regular morass of details";
        "a regular nincompoop"; "he's a veritable swine" [syn: {regular(a)},
         {veritable(a)}]
     3: conforming to a standard or pattern; "following the regular
        procedure of the legislature"; "a regular electrical
        outlet"
     4: (of solids) having clear dimensions that can be measured;
        volume can be determined with a suitable geometric formula
        [ant: {irregular}]
     5: regularly scheduled for ...
