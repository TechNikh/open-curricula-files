---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fondly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653141
title: fondly
categories:
    - Dictionary
---
fondly
     adv : with fondness; with love; "she spoke to her children fondly"
           [syn: {lovingly}]
