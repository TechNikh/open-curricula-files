---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mile_stone
offline_file: ""
offline_thumbnail: ""
uuid: db2d0cd3-5f04-4dab-9e0c-46660501221c
updated: 1484308365
title: Milestone
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-National_Road_marker%252C_Columbus%252C_OH_01.jpg'
tags:
    - History
    - Roman Empire
    - Byzantine Empire
    - Post-Medieval Europe
    - India
    - Modern highways
    - Australia
    - Canada
    - India
    - United Kingdom
    - United States
    - Zimbabwe
    - Railway mileposts
    - Boundaries
    - Gallery
categories:
    - Uncategorized
---
A milestone is one of a series of numbered markers placed along a road or boundary at intervals of one mile or occasionally, parts of a mile. They are typically located at the side of the road or in a median. They are alternatively known as mile markers, mileposts or mile posts (sometimes abbreviated MPs). Mileage is the distance along the road from a fixed commencement point.
