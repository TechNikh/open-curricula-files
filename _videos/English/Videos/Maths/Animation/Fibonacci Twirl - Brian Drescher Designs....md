---
version: 1
type: video
provider: YouTube
id: glIL2kmGUhk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a393ff2d-9537-44b7-bf39-34be30281702
updated: 1486069643
title: 'Fibonacci Twirl - Brian Drescher Designs...'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/glIL2kmGUhk/default.jpg
    - https://i3.ytimg.com/vi/glIL2kmGUhk/1.jpg
    - https://i3.ytimg.com/vi/glIL2kmGUhk/2.jpg
    - https://i3.ytimg.com/vi/glIL2kmGUhk/3.jpg
---
