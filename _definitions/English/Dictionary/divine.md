---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501101
title: divine
categories:
    - Dictionary
---
divine
     adj 1: emanating from God; "divine judgment"; "divine guidance";
            "everything is black1 or white...satanic or
            godlyt"-Saturday Rev. [syn: {godly}]
     2: resulting from divine providence; "providential care"; "a
        providential visitation" [syn: {providential}]
     3: being or having the nature of a god; "the custom of killing
        the divine king upon any serious failure of
        his...powers"-J.G.Frazier; "the divine will"; "the divine
        capacity for love"; "'Tis wise to learn; 'tis God-like to
        create"-J.G.Saxe [syn: {godlike}]
     4: devoted to or in the service or worship of a deity; "divine
        worship"; "divine ...
