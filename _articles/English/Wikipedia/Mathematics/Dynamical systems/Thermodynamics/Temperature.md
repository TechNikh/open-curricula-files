---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Temperature
offline_file: ""
offline_thumbnail: ""
uuid: d30ee6da-86e9-4b37-ad11-8b1059d8b1f8
updated: 1484308866
title: Temperature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Body_Temp_Variation.png
tags:
    - Effects of temperature
    - Temperature scales
    - Thermodynamic approach to temperature
    - Kinds of temperature scale
    - Empirically based scales
    - Theoretically based scales
    - Absolute thermodynamic scale
    - Definition of the Kelvin scale
    - Temperature as an intensive variable
    - >
        Temperature local when local thermodynamic equilibrium
        prevails
    - Kinetic theory approach to temperature
    - Basic theory
    - Temperature for bodies in thermodynamic equilibrium
    - >
        Temperature for bodies in a steady state but not in
        thermodynamic equilibrium
    - Temperature for bodies not in a steady state
    - Thermodynamic equilibrium axiomatics
    - Heat capacity
    - Temperature measurement
    - Units
    - Conversion
    - Plasma physics
    - Theoretical foundation
    - Kinetic theory of gases
    - Zeroth law of thermodynamics
    - Second law of thermodynamics
    - Definition from statistical mechanics
    - Generalized temperature from single-particle statistics
    - Negative temperature
    - Examples of temperature
    - Notes and references
    - Bibliography of cited references
categories:
    - Thermodynamics
---
A temperature is an objective comparative measurement of hot or cold. It is measured by a thermometer. Several scales and units exist for measuring temperature, the most common being Celsius (denoted °C; formerly called centigrade), Fahrenheit (denoted °F), and, especially in science, Kelvin (denoted K).
