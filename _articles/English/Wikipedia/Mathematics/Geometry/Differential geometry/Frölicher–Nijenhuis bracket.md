---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fr%C3%B6licher%E2%80%93Nijenhuis_bracket'
offline_file: ""
offline_thumbnail: ""
uuid: ae0e7530-a0a9-444f-94e9-01f18ab8e47b
updated: 1484309219
title: Frölicher–Nijenhuis bracket
tags:
    - Definition
    - Derivations of the ring of forms
    - Applications
categories:
    - Differential geometry
---
In mathematics, the Frölicher–Nijenhuis bracket is an extension of the Lie bracket of vector fields to vector-valued differential forms on a differentiable manifold.
