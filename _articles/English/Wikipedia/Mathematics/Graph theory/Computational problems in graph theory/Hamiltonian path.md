---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_path
offline_file: ""
offline_thumbnail: ""
uuid: 1b9002b9-aa0b-40c0-a524-ff661ab08c06
updated: 1484309389
title: Hamiltonian path
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hamiltonian_path.svg_0.png
tags:
    - Definitions
    - Examples
    - Properties
    - Bondy–Chvátal theorem
    - Existence of Hamiltonian cycles in planar graphs
    - Notes
categories:
    - Computational problems in graph theory
---
In the mathematical field of graph theory, a Hamiltonian path (or traceable path) is a path in an undirected or directed graph that visits each vertex exactly once. A Hamiltonian cycle (or Hamiltonian circuit) is a Hamiltonian path that is a cycle. Determining whether such paths and cycles exist in graphs is the Hamiltonian path problem, which is NP-complete.
