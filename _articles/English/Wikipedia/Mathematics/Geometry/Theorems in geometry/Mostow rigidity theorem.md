---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mostow_rigidity_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 84214339-9a39-478d-aa43-6d17c855d842
updated: 1484309123
title: Mostow rigidity theorem
tags:
    - The theorem
    - Geometric form
    - Algebraic form
    - In greater generality
    - Applications
categories:
    - Theorems in geometry
---
In mathematics, Mostow's rigidity theorem, or strong rigidity theorem, or Mostow–Prasad rigidity theorem, essentially states that the geometry of a complete, finite-volume hyperbolic manifold of dimension greater than two is determined by the fundamental group and hence unique. The theorem was proven for closed manifolds by Mostow (1968) and extended to finite volume manifolds by Marden (1974) in 3 dimensions, and by Prasad (1973) in all dimensions at least 3. Gromov (1981) gave an alternate proof using the Gromov norm.
