---
version: 1
type: video
provider: YouTube
id: vVKrOpyt4hA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b682422f-2355-4ae4-bbe3-298865872ccb
updated: 1486069645
title: '3D Fractal- "Origin of Intelligence"'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vVKrOpyt4hA/default.jpg
    - https://i3.ytimg.com/vi/vVKrOpyt4hA/1.jpg
    - https://i3.ytimg.com/vi/vVKrOpyt4hA/2.jpg
    - https://i3.ytimg.com/vi/vVKrOpyt4hA/3.jpg
---
