---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dance
offline_file: ""
offline_thumbnail: ""
uuid: a54ced5d-555a-4ee5-8a17-7f068194454f
updated: 1484310479
title: dance
categories:
    - Dictionary
---
dance
     n 1: an artistic form of nonverbal communication
     2: a party of people assembled for dancing
     3: taking a series of rhythmical steps (and movements) in time
        to music [syn: {dancing}, {terpsichore}, {saltation}]
     4: a party for social dancing
     v 1: move in a graceful and rhythmical way; "The young girl
          danced into the room"
     2: move in a pattern; usually to musical accompaniment; do or
        perform a dance; "My husband and I like to dance at home
        to the radio" [syn: {trip the light fantastic}, {trip the
        light fantastic toe}]
     3: skip, leap, or move up and down or sideways; "Dancing
        flames"; "The children danced ...
