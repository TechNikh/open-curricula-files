---
version: 1
type: video
provider: YouTube
id: 6CpNOu4l1dM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3fe87b56-167e-4dfa-9ba0-b0984d5846dc
updated: 1486069652
title: Radiation Belt as a Magnetic Bottle
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/6CpNOu4l1dM/default.jpg
    - https://i3.ytimg.com/vi/6CpNOu4l1dM/1.jpg
    - https://i3.ytimg.com/vi/6CpNOu4l1dM/2.jpg
    - https://i3.ytimg.com/vi/6CpNOu4l1dM/3.jpg
---
