---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kidnapping
offline_file: ""
offline_thumbnail: ""
uuid: 60409097-d26b-4c43-8c9c-bfb22840d9ff
updated: 1484310188
title: kidnapping
categories:
    - Dictionary
---
kidnap
     v : take away to an undisclosed location against their will and
         usually in order to extract a ransom; "The
         industrialist's son was kidnapped" [syn: {nobble}, {abduct},
          {snatch}]
     [also: {kidnapping}, {kidnapped}]
