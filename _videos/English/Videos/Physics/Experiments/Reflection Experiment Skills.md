---
version: 1
type: video
provider: YouTube
id: JARJjgVBZQ4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b6ca46d9-0cae-4e81-8570-bf3147f8b2cf
updated: 1486069636
title: Reflection Experiment Skills
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/JARJjgVBZQ4/default.jpg
    - https://i3.ytimg.com/vi/JARJjgVBZQ4/1.jpg
    - https://i3.ytimg.com/vi/JARJjgVBZQ4/2.jpg
    - https://i3.ytimg.com/vi/JARJjgVBZQ4/3.jpg
---
