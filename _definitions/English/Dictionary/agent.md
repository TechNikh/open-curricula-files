---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agent
offline_file: ""
offline_thumbnail: ""
uuid: 45ba7e5c-2127-4d28-a8c0-2d399a7e1772
updated: 1484310313
title: agent
categories:
    - Dictionary
---
agent
     n 1: an active and efficient cause; capable of producing a
          certain effect; "their research uncovered new disease
          agents"
     2: a substance that exerts some force or effect
     3: a representative who acts on behalf of other persons or
        organizations
     4: a businessman who buys or sells for another in exchange for
        a commission [syn: {factor}, {broker}]
     5: any agent or representative of a federal agency or bureau
        [syn: {federal agent}]
     6: the semantic role of the animate entity that instigates or
        causes the hapening denoted by the verb in the clause
        [syn: {agentive role}]
