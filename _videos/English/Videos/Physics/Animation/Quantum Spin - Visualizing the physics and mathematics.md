---
version: 1
type: video
provider: YouTube
id: 3k5IWlVdMbo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Quantum%20Spin%20-%20Visualizing%20the%20physics%20and%20mathematics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 822cc5b2-8884-4929-81c9-f6b416a21798
updated: 1486069654
title: 'Quantum Spin - Visualizing the physics and mathematics'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3k5IWlVdMbo/default.jpg
    - https://i3.ytimg.com/vi/3k5IWlVdMbo/1.jpg
    - https://i3.ytimg.com/vi/3k5IWlVdMbo/2.jpg
    - https://i3.ytimg.com/vi/3k5IWlVdMbo/3.jpg
---
