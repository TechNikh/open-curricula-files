---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hip
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406901
title: hip
categories:
    - Dictionary
---
hip
     adj : informed about the latest trends [syn: {hep}, {hip to(p)}]
     n 1: either side of the body below the waist and above the thigh
     2: the structure of the vertebrate skeleton supporting the
        lower limbs in humans and the hind limbs or corresponding
        parts in other vertebrates [syn: {pelvis}, {pelvic girdle},
         {pelvic arch}]
     3: the ball-and-socket joint between the head of the femur and
        the acetabulum [syn: {hip joint}, {coxa}, {articulatio
        coxae}]
     4: the fruit of a rose plant [syn: {rose hip}, {rosehip}]
     [also: {hippest}, {hipper}]
