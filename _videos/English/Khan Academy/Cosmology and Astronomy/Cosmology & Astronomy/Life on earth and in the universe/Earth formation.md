---
version: 1
type: video
provider: YouTube
id: VbNXh0GaLYo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Earth%20formation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c697b5f7-0cf1-45fe-a286-5b9170119d62
updated: 1486069689
title: Earth formation
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/VbNXh0GaLYo/default.jpg
    - https://i3.ytimg.com/vi/VbNXh0GaLYo/1.jpg
    - https://i3.ytimg.com/vi/VbNXh0GaLYo/2.jpg
    - https://i3.ytimg.com/vi/VbNXh0GaLYo/3.jpg
---
