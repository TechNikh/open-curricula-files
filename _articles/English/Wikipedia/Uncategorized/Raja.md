---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raja
offline_file: ""
offline_thumbnail: ""
uuid: 512fe558-4534-4954-9342-8fb2434ecf38
updated: 1484308470
title: Raja
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jai_Singh_and_Shivaji.jpg
tags:
    - Etymology
    - Related titles and variations
    - Compound titles
    - Raja-ruled Indian states
    - Usage outside India
    - Rajadharma
    - Famous personalities
    - Notes
categories:
    - Uncategorized
---
Raja (/ˈrɑːdʒɑː/; also spelled Rajah, from Sanskrit राजन् rājan-), is a title for a Monarch or princely ruler in South and Southeast Asia. The female form Rani (sometimes spelled ranee) applies equally to the wife of a Raja (or of an equivalent style such as Rana), usually as queen consort and occasionally as regent.
