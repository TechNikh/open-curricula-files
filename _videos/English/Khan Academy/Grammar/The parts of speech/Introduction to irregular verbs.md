---
version: 1
type: video
provider: YouTube
id: ZKr--3HpP_A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Introduction%20to%20irregular%20verbs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c2f49617-d004-4aaf-97e3-c81fb24180d8
updated: 1486069601
title: Introduction to irregular verbs
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZKr--3HpP_A/default.jpg
    - https://i3.ytimg.com/vi/ZKr--3HpP_A/1.jpg
    - https://i3.ytimg.com/vi/ZKr--3HpP_A/2.jpg
    - https://i3.ytimg.com/vi/ZKr--3HpP_A/3.jpg
---
Words like "walk" and "look" are regular verbs – they behave in a predictable way. But there's a whole class of words in English, called irregular verbs, that are harder to predict. David, KA's Grammar Fellow, explains.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/e/intro-to-irregular-verbs?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/the-funky-ed-irregular-verb-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/helping-verbs-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
