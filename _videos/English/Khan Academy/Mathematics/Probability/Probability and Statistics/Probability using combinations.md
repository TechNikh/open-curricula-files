---
version: 1
type: video
provider: YouTube
id: Xqfcy1rqMbI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Probability%20using%20combinations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 83e5d9ae-5c36-4e6b-ac99-1f63e1d147e3
updated: 1486069689
title: Probability using combinations
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xqfcy1rqMbI/default.jpg
    - https://i3.ytimg.com/vi/Xqfcy1rqMbI/1.jpg
    - https://i3.ytimg.com/vi/Xqfcy1rqMbI/2.jpg
    - https://i3.ytimg.com/vi/Xqfcy1rqMbI/3.jpg
---
