---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%27s_fifteenth_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 76bb87de-c347-466a-b344-00a9d75464ec
updated: 1484309153
title: "Hilbert's fifteenth problem"
tags:
    - Introduction
    - Problem statement
    - Schubert calculus
categories:
    - Algebraic geometry
---
Hilbert's fifteenth problem is one of the 23 Hilbert problems set out in a celebrated list compiled in 1900 by David Hilbert. The problem is to put Schubert's enumerative calculus on a rigorous foundation.
