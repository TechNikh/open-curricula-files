---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roasted
offline_file: ""
offline_thumbnail: ""
uuid: 99d3a4df-f9b8-45b4-8484-caa1f0be9dd7
updated: 1484310411
title: roasted
categories:
    - Dictionary
---
roasted
     adj : (of meat) cooked by dry heat in an oven [syn: {roast}]
