---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/estimated
offline_file: ""
offline_thumbnail: ""
uuid: 0ef82388-c06f-4c57-a542-31c9d7141f3e
updated: 1484310254
title: estimated
categories:
    - Dictionary
---
estimated
     adj : calculated approximately; "an estimated mass of 25 g"
