---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/situated
offline_file: ""
offline_thumbnail: ""
uuid: 7c549275-85b9-4459-bba4-f4918caff1a1
updated: 1484310389
title: situated
categories:
    - Dictionary
---
situated
     adj : situated in a particular spot or position; "valuable
           centrally located urban land"; "strategically placed
           artillery"; "a house set on a hilltop"; "nicely
           situated on a quiet riverbank" [syn: {located}, {placed},
            {set}]
