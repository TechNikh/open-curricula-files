---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fold_equity
offline_file: ""
offline_thumbnail: ""
uuid: 7051a3bb-0f8a-43e7-a6f3-94b978c600fc
updated: 1484309059
title: Fold equity
categories:
    - Poker strategy
---
Fold equity is a concept in poker strategy that is especially important when a player becomes short-stacked in a no limit (or possibly pot limit) tournament. It is the equity a player can expect to gain due to the opponent folding to his or her bets. It equates to:
