---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infinitesimal_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 4014c71b-524d-482a-b88b-b62b8937cec2
updated: 1484309156
title: Infinitesimal cohomology
categories:
    - Algebraic geometry
---
In mathematics, infinitesimal cohomology is a cohomology theory for algebraic varieties introduced by Grothendieck (1966). In characteristic 0 it is essentially the same as crystalline cohomology. In nonzero characteristic p Ogus (1975) showed that it is closely related to etale cohomology with mod p coefficients, a theory known to have undesirable properties.
