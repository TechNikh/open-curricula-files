---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Deterministic_scale-free_network
offline_file: ""
offline_thumbnail: ""
uuid: 9f31e3ed-7863-44c7-88b2-a56c74e184e8
updated: 1484309510
title: Deterministic scale-free network
tags:
    - concept
    - Properties
    - Examples
    - Barabási-Ravasz-Vicsek model
    - Lu-Su-Guo model
    - Zhu-Yin-Zhao-Chai model
categories:
    - Network theory
---
A scale-free network is type of networks that is of particular interest of network science. It is characterized by its degree distribution following a power law. While the most widely known generative models for scale-free networks are stochastic, such as the Barabási–Albert model or the Fitness model can reproduce many properties of real-life networks by assuming preferential attachment and incremental growth, the understanding of deterministic scale-free networks leads to valuable, analytical results.
