---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Index_set_(recursion_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 6c3d0d10-f820-4492-a5fc-c0066c4ae4e9
updated: 1484309487
title: Index set (recursion theory)
tags:
    - Definition
    - "Index sets and Rice's theorem"
    - Notes
categories:
    - Computability theory
---
In the field of recursion theory, index sets describe classes of partial recursive functions, specifically they give all indices of functions in that class according to a fixed enumeration of partial recursive functions (a Gödel numbering).
