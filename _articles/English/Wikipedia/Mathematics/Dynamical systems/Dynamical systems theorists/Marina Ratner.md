---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marina_Ratner
offline_file: ""
offline_thumbnail: ""
uuid: 8e3f5be4-592e-4dd8-ab29-9edba728e605
updated: 1484308688
title: Marina Ratner
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Marina_Ratner.jpeg
categories:
    - Dynamical systems theorists
---
Marina Evseevna Ratner (Russian: Мари́на Евсе́евна Ра́тнер; born October 30, 1938, Moscow, Russian SFSR) is a professor of mathematics at the University of California, Berkeley who works in ergodic theory.[1] Around 1990 she proved a group of major theorems concerning unipotent flows on homogeneous spaces, known as Ratner's theorems.[2] Ratner was elected to the American Academy of Arts and Sciences in 1992,[3] awarded the Ostrowski Prize in 1993 and elected to the National Academy of Sciences the same year. In 1994 she was awarded the John J. Carty Award from the National Academy of Sciences.[4]
