---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Killing%E2%80%93Hopf_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: db07325b-1959-4a70-a439-1956cdb959da
updated: 1484309365
title: Killing–Hopf theorem
categories:
    - Geometry stubs
---
In geometry, the Killing–Hopf theorem states that complete connected Riemannian manifolds of constant curvature are isometric to a quotient of a sphere, Euclidean space, or hyperbolic space by a group acting freely and properly discontinuously. These manifolds are called space forms. The Killing–Hopf theorem was proved by Killing (1891) and Hopf (1926).
