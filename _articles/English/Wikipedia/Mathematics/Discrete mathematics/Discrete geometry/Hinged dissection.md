---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hinged_dissection
offline_file: ""
offline_thumbnail: ""
uuid: 8c4d5b4d-4602-415d-a270-9c6ae5d9482f
updated: 1484308580
title: Hinged dissection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hinged_dissection_3-4-6-3_loop.gif
tags:
    - History
    - Other hinges
    - Bibliography
categories:
    - Discrete geometry
---
A hinged dissection, also known as a swing-hinged dissection or Dudeney dissection,[1] is a kind of geometric dissection in which all of the pieces are connected into a chain by "hinged" points, such that the rearrangement from one figure to another can be carried out by swinging the chain continuously, without severing any of the connections.[2] Typically, it is assumed that the pieces are allowed to overlap in the folding and unfolding process;[3] this is sometimes called the "wobbly-hinged" model of hinged dissection.[4]
