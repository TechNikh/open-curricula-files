---
version: 1
type: video
provider: YouTube
id: d0cQtKSpwJo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 605a8efd-d962-45f2-8402-801fd25c6724
updated: 1486069621
title: "Golden ratio and Rembrandt's self portrait"
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/d0cQtKSpwJo/default.jpg
    - https://i3.ytimg.com/vi/d0cQtKSpwJo/1.jpg
    - https://i3.ytimg.com/vi/d0cQtKSpwJo/2.jpg
    - https://i3.ytimg.com/vi/d0cQtKSpwJo/3.jpg
---
