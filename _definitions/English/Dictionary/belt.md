---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belt
offline_file: ""
offline_thumbnail: ""
uuid: 0106d308-5113-4db3-8486-686d448998c2
updated: 1484310234
title: belt
categories:
    - Dictionary
---
belt
     n 1: endless loop of flexible material between two rotating
          shafts or pulleys
     2: a band to tie or buckle around the body (usually at the
        waist)
     3: an elongated region where a specific condition is found; "a
        belt of high pressure"
     4: a vigorous blow; "the sudden knock floored him"; "he took a
        bash right in his face"; "he got a bang on the head" [syn:
         {knock}, {bash}, {bang}, {smash}]
     5: a path or strip (as cut by one course of mowing) [syn: {swath}]
     6: the act of hitting vigorously; "he gave the table a whack"
        [syn: {knock}, {rap}, {whack}, {whang}]
     v 1: sing loudly and forcefully [syn: {belt out}]
    ...
