---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Banks%E2%80%93Zaks_fixed_point'
offline_file: ""
offline_thumbnail: ""
uuid: 8746795f-766e-44d5-b7d0-7294bedb3e89
updated: 1484308784
title: Banks–Zaks fixed point
categories:
    - Fixed points (mathematics)
---
In quantum chromodynamics (and also N = 1 superquantum chromodynamics) with massless flavors, if the number of flavors, Nf, is sufficiently small (i.e. small enough to guarantee asymptotic freedom, depending on the number of colors), the theory can flow to an interacting conformal fixed point of the renormalization group. If the value of the coupling at that point is less than one (i.e. one can perform perturbation theory in weak coupling), then the fixed point is called a Banks–Zaks fixed point. The existence of the fixed point was first reported by William E. Caswell in 1974, and later used by Banks and Zaks in their analysis of the phase structure of vector-like gauge theories with ...
