---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disagree
offline_file: ""
offline_thumbnail: ""
uuid: 770f61ed-2262-4b6f-ba41-a9b13000e956
updated: 1484310480
title: disagree
categories:
    - Dictionary
---
disagree
     v 1: be of different opinions; "I beg to differ!"; "She disagrees
          with her husband on many questions" [syn: {differ}, {dissent},
           {take issue}] [ant: {agree}]
     2: be different from one another [syn: {disaccord}, {discord}]
        [ant: {match}]
