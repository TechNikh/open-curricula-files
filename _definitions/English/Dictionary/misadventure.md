---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misadventure
offline_file: ""
offline_thumbnail: ""
uuid: 84bd6f46-7a40-43e4-b576-e04cdca72182
updated: 1484310188
title: misadventure
categories:
    - Dictionary
---
misadventure
     n : an instance of misfortune [syn: {mishap}, {mischance}]
