---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Koszul_duality
offline_file: ""
offline_thumbnail: ""
uuid: 6d450499-a561-4bf1-832d-13c829a0deb4
updated: 1484309236
title: Koszul duality
tags:
    - Koszul duality for modules over Koszul algebras
    - Koszul dual of a Koszul algebra
    - Koszul duality
    - Variants
    - Koszul duality for operads
    - Notes
categories:
    - Duality theories
---
In mathematics, Koszul duality, named after the French mathematician Jean-Louis Koszul, is any of various kinds of dualities found in representation theory of Lie algebras, abstract algebras (semisimple algebra)[1] as well as topology (e.g., equivariant cohomology[2]). The prototype example, due to Bernstein, Gelfand and Gelfand,[3] is the rough duality between the derived category of a symmetric algebra and that of an exterior algebra. The importance of the notion rests on the suspicion that Koszul duality seems quite ubiquitous in nature.[citation needed]
