---
version: 1
type: video
provider: YouTube
id: wiXhK7aEuGY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Using%20quotation%20marks%20in%20titles.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fd7f296a-7609-47a9-b128-ce10ff8a3c04
updated: 1486069599
title: Using quotation marks in titles
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/wiXhK7aEuGY/default.jpg
    - https://i3.ytimg.com/vi/wiXhK7aEuGY/1.jpg
    - https://i3.ytimg.com/vi/wiXhK7aEuGY/2.jpg
    - https://i3.ytimg.com/vi/wiXhK7aEuGY/3.jpg
---
Learn how to use quotation marks to title things like poems, songs, and episodes of TV shows.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/miscellaneous-punctuation/e/italics--underlines--and-quotes/

Watch the next lesson: https://www.khanacademy.org/miscellaneous-punctuation/v/parentheses

Missed the previous lesson? Watch here: https://www.khanacademy.org/miscellaneous-punctuation/v/italics-and-underlining

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
