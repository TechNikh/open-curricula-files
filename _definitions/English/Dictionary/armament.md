---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/armament
offline_file: ""
offline_thumbnail: ""
uuid: d4c531ad-598a-4cca-a48c-bcf46f7cf540
updated: 1484310555
title: armament
categories:
    - Dictionary
---
armament
     n 1: weaponry used by military or naval force
     2: the act of equiping with weapons in preparation for war
        [syn: {arming}, {equipping}] [ant: {disarming}, {disarming}]
