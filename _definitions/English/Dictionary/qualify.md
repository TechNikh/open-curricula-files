---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/qualify
offline_file: ""
offline_thumbnail: ""
uuid: 306832a6-44c3-4f1e-b8c0-095427d3b1e0
updated: 1484310144
title: qualify
categories:
    - Dictionary
---
qualify
     v 1: prove capable or fit; meet requirements [syn: {measure up}]
     2: pronounce fit or able; "She was qualified to run the
        marathon"; "They nurses were qualified to administer the
        injections" [ant: {disqualify}]
     3: make more specific; "qualify these remarks" [syn: {restrict}]
     4: make fit or prepared; "Your education qualifies you for this
        job" [syn: {dispose}] [ant: {disqualify}]
     5: specify as a condition or requirement in a contract or
        agreement; make an express demand or provision in an
        agreement; "The will stipulates that she can live in the
        house for the rest of her life"; "The contract stipulates
        the ...
