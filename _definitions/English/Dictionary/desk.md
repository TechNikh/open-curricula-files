---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desk
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448541
title: desk
categories:
    - Dictionary
---
desk
     n : a piece of furniture with a writing surface and usually
         drawers or other compartments
