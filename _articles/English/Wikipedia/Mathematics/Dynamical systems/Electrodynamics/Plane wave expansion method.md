---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plane_wave_expansion_method
offline_file: ""
offline_thumbnail: ""
uuid: 3c563da7-bab5-4f31-aca2-3f0345f408f4
updated: 1484308762
title: Plane wave expansion method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Photonic_Crystal_1D_DBR_aircore_epsr12point25_DbyA0point8.png
tags:
    - Principles
    - Example for 1D case
    - Example code
    - Advantages
    - Disadvantages
categories:
    - Electrodynamics
---
[dubious – discuss] Plane wave expansion method (PWE) refers to a computational technique in electromagnetics to solve the Maxwell's equations by formulating an eigenvalue problem out of the equation. This method is popular among the photonic crystal community as a method of solving for the band structure (dispersion relation) of specific photonic crystal geometries. PWE is traceable to the analytical formulations, and is useful in calculating modal solutions of Maxwell's equations over an inhomogeneous or periodic geometry. It is specifically tuned to solve problems in a time-harmonic forms, with non-dispersive media.
