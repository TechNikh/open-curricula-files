---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Folk_theorem_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: fafde4ea-a3ce-46d0-864a-4160c01efb0a
updated: 1484309032
title: Folk theorem (game theory)
tags:
    - Preliminaries
    - Infinitely-repeated games without discounting
    - Subgame perfection
    - Overtaking
    - Infinitely-repeated games with discounting
    - Subgame perfection
    - Finitely-repeated games without discount
    - Applications
    - Summary of folk theorems
    - Notes
categories:
    - Game theory equilibrium concepts
---
In game theory, folk theorems are a class of theorems about possible Nash equilibrium payoff profiles in repeated games (Friedman 1971).[1] The original Folk Theorem concerned the payoffs of all the Nash equilibria of an infinitely repeated game. This result was called the Folk Theorem because it was widely known among game theorists in the 1950s, even though no one had published it. Friedman's (1971) Theorem concerns the payoffs of certain subgame-perfect Nash equilibria of an infinitely repeated game, and so strengthens the original Folk Theorem by using a stronger equilibrium concept subgame-perfect Nash equilibria rather than Nash equilibrium.[2]
