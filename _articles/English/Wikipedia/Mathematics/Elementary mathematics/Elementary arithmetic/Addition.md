---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Addition
offline_file: ""
offline_thumbnail: ""
uuid: d09531ed-ed6a-4435-bd48-b1ff351ee148
updated: 1484308921
title: Addition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Addition01.svg.png
tags:
    - Notation and terminology
    - Interpretations
    - Combining sets
    - Extending a length
    - Properties
    - Commutativity
    - Associativity
    - Identity element
    - Successor
    - Units
    - Performing addition
    - Innate ability
    - Learning addition as children
    - Addition table
    - Decimal system
    - Carry
    - Addition of decimal fractions
    - Scientific notation
    - Addition in other bases
    - Computers
    - Addition of numbers
    - Natural numbers
    - Integers
    - Rational numbers (fractions)
    - Real numbers
    - Complex numbers
    - Generalizations
    - Addition in abstract algebra
    - Vector addition
    - Matrix addition
    - Modular arithmetic
    - General addition
    - Addition in set theory and category theory
    - Related operations
    - Arithmetic
    - Ordering
    - Other ways to add
    - Notes
categories:
    - Elementary arithmetic
---
Addition (often signified by the plus symbol "+") is one of the four basic operations of arithmetic, with the others being subtraction, multiplication and division. The addition of two whole numbers is the total amount of those quantities combined. For example, in the picture on the right, there is a combination of three apples and two apples together, making a total of five apples. This observation is equivalent to the mathematical expression "3 + 2 = 5" i.e., "3 add 2 is equal to 5".
