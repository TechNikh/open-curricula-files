---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beginning
offline_file: ""
offline_thumbnail: ""
uuid: 73fc07be-12b9-42b2-917f-8fb5f513f1ba
updated: 1484310313
title: beginning
categories:
    - Dictionary
---
beginning
     adj : serving to begin; "the beginning canto of the poem"; "the
           first verse" [syn: {beginning(a)}, {first}]
     n 1: the event consisting of the start of something; "the
          beginning of the war" [ant: {ending}]
     2: the time at which something is supposed to begin; "they got
        an early start"; "she knew from the get-go that he was the
        man for her" [syn: {commencement}, {first}, {outset}, {get-go},
         {start}, {kickoff}, {starting time}, {showtime}, {offset}]
        [ant: {middle}, {end}]
     3: the first part or section of something; "`It was a dark and
        stormy night' is a hackneyed beginning for a story" [ant:
        ...
