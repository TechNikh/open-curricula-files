---
version: 1
type: video
provider: YouTube
id: TgDk06Qayxw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/SSS%20to%20show%20a%20radius%20is%20perpendicular%20to%20a%20chord%20that%20it%20bisects.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 07d10659-0b6c-4d4f-9d40-1fec54325483
updated: 1486069579
title: >
    SSS to show a radius is perpendicular to a chord that it
    bisects
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/TgDk06Qayxw/default.jpg
    - https://i3.ytimg.com/vi/TgDk06Qayxw/1.jpg
    - https://i3.ytimg.com/vi/TgDk06Qayxw/2.jpg
    - https://i3.ytimg.com/vi/TgDk06Qayxw/3.jpg
---
More on the difference between a theorem and axiom. Proving a cool result using SSS

Watch the next lesson: https://www.khanacademy.org/math/geometry/congruent-triangles/cong_triangle/v/other-triangle-congruence-postulates?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/congruent-triangles/cong_triangle/v/congruent-triangles-and-sss?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
