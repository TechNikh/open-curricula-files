---
version: 1
type: video
provider: YouTube
id: sSyPAAyL8nQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Green%27s%20theorem%20example%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a88ed581-065a-4e54-954f-963839445f52
updated: 1486069689
title: "Green's theorem example 2"
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sSyPAAyL8nQ/default.jpg
    - https://i3.ytimg.com/vi/sSyPAAyL8nQ/1.jpg
    - https://i3.ytimg.com/vi/sSyPAAyL8nQ/2.jpg
    - https://i3.ytimg.com/vi/sSyPAAyL8nQ/3.jpg
---
