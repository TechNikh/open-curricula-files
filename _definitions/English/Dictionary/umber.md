---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/umber
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484355301
title: umber
categories:
    - Dictionary
---
umber
     adj : of the color of any of various natural brown earth pigments
     n 1: an earth pigment
     2: a medium to dark brown color [syn: {chocolate}, {coffee}, {deep
        brown}, {burnt umber}]
