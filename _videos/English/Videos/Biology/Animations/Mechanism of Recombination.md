---
version: 1
type: video
provider: YouTube
id: 8rXizmLjegI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Mechanism%20of%20Recombination.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4d105cda-e7a6-47dc-b65b-34bedb941487
updated: 1486069670
title: Mechanism of Recombination
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8rXizmLjegI/default.jpg
    - https://i3.ytimg.com/vi/8rXizmLjegI/1.jpg
    - https://i3.ytimg.com/vi/8rXizmLjegI/2.jpg
    - https://i3.ytimg.com/vi/8rXizmLjegI/3.jpg
---
