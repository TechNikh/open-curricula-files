---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plan_(drawing)
offline_file: ""
offline_thumbnail: ""
uuid: 6fe837fe-f2c7-44fc-a7af-eeb95a896250
updated: 1484309329
title: Plan (drawing)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Lat%25C3%25A9co%25C3%25A8re_28.svg.png'
tags:
    - Overview
    - Plan features
    - Format
    - Scale
    - Views and projections
    - Planning approach
categories:
    - Technical drawing
---
Plans are a set of drawings or two-dimensional diagrams used to describe a place or object, or to communicate building or fabrication instructions. Usually plans are drawn or printed on paper, but they can take the form of a digital file.
