---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Book_embedding
offline_file: ""
offline_thumbnail: ""
uuid: 7f5d8f3c-8b38-4f70-8715-8b3fcff4a3da
updated: 1484309479
title: Book embedding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-3page_K5.svg.png
tags:
    - History
    - Definitions
    - Specific graphs
    - Properties
    - Planarity and outerplanarity
    - Behavior under subdivisions
    - Relation to other graph invariants
    - Computational complexity
    - Applications
    - Fault-tolerant multiprocessing
    - Stack sorting
    - Traffic control
    - Graph drawing
    - RNA folding
    - Computational complexity theory
    - Other areas of mathematics
categories:
    - Topological graph theory
---
In graph theory, a book embedding is a generalization of planar embedding of a graph to embeddings into a book, a collection of half-planes all having the same line as their boundary. Usually, the vertices of the graph are required to lie on this boundary line, called the spine, and the edges are required to stay within a single half-plane. The book thickness of a graph is the smallest possible number of half-planes for any book embedding of the graph. Book thickness is also called pagenumber, stacknumber or fixed outerthickness. Book embeddings have also been used to define several other graph invariants including the pagewidth and book crossing number.
