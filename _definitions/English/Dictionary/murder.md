---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/murder
offline_file: ""
offline_thumbnail: ""
uuid: 642e7127-c8e7-4f63-8c82-016664c16c43
updated: 1484310551
title: murder
categories:
    - Dictionary
---
murder
     n : unlawful premeditated killing of a human being by a human
         being [syn: {slaying}, {execution}]
     v 1: kill intentionally and with premeditation; "The mafia boss
          ordered his enemies murdered" [syn: {slay}, {hit}, {dispatch},
           {bump off}, {polish off}, {remove}]
     2: alter so as to make unrecognizable; "The tourists murdered
        the French language" [syn: {mangle}, {mutilate}]
