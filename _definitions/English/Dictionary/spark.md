---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spark
offline_file: ""
offline_thumbnail: ""
uuid: bd529dfc-f59c-499d-b967-440226f7a2d4
updated: 1484310204
title: spark
categories:
    - Dictionary
---
spark
     n 1: a momentary flash of light [syn: {flicker}, {glint}]
     2: brightness and animation of countenance; "he had a sparkle
        in his eye" [syn: {sparkle}, {light}]
     3: electrical conduction through a gas in an applied electric
        field [syn: {discharge}, {arc}, {electric arc}, {electric
        discharge}]
     4: a small but noticeable trace of some quality that might
        become stronger; "a spark of interest"; "a spark of
        decency"
     5: Scottish writer of satirical novels (born in 1918) [syn: {Muriel
        Spark}, {Dame Muriel Spark}, {Muriel Sarah Spark}]
     6: a small fragment of a burning substance thrown out by
        burning material or ...
