---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/look
offline_file: ""
offline_thumbnail: ""
uuid: 5d0531d6-5cbc-4586-b9f6-51d078885c99
updated: 1484310337
title: look
categories:
    - Dictionary
---
look
     n 1: the expression on a person's face; "a sad expression"; "a
          look of triumph"; "an angry face" [syn: {expression}, {aspect},
           {facial expression}, {face}]
     2: the act of directing the eyes toward something and
        perceiving it visually; "he went out to have a look"; "his
        look was fixed on her eyes"; "he gave it a good looking
        at"; "his camera does his looking for him" [syn: {looking},
         {looking at}]
     3: physical appearance; "I don't like the looks of this place"
     4: the general atmosphere of a place or situation and the
        effect that it has on people; "the feel of the city
        excited him"; "a clergyman ...
