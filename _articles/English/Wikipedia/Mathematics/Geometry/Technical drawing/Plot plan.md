---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plot_plan
offline_file: ""
offline_thumbnail: ""
uuid: 531ff4ae-61f4-4964-bc17-c3827dd2aa6d
updated: 1484309331
title: Plot plan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Venedig-in-Wien-Lageplan.jpg
categories:
    - Technical drawing
---
A plot plan is an architecture, engineering, and/or landscape architecture plan drawing—diagram which shows the buildings, utility runs, and equipment layout, the position of roads, and other constructions of an existing or proposed project site at a defined scale. Plot plans are also known more commonly as site plans. The plot plan is a 'top-down' orientation.
