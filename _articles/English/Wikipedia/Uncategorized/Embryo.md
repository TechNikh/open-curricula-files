---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Embryos
offline_file: ""
offline_thumbnail: ""
uuid: 4a26d460-1659-486d-9d98-4b4bc5400143
updated: 1484308344
title: Embryo
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Embryo_7_weeks_after_conception.jpg
tags:
    - Etymology
    - Development
    - Plant embryos
    - Research and technology
    - Fossilized embryos
    - 'Miscarriage & abortion'
    - Notes
categories:
    - Uncategorized
---
An embryo is an early stage of development of a multicellular diploid eukaryotic organism. In general, in organisms that reproduce sexually, an embryo develops from a zygote, the single cell resulting from the fertilization of the female egg cell by the male sperm cell. The zygote possesses half the DNA of each of its two parents. In plants, animals, and some protists, the zygote will begin to divide by mitosis to produce a multicellular organism. The result of this process is an embryo.
