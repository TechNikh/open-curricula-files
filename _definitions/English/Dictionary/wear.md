---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wear
offline_file: ""
offline_thumbnail: ""
uuid: 2c330df5-b26d-4a42-a9c4-e2e991307d88
updated: 1484310413
title: wear
categories:
    - Dictionary
---
wear
     n 1: impairment resulting from long use; "the tires showed uneven
          wear"
     2: a covering designed to be worn on a person's body [syn: {clothing},
         {article of clothing}, {vesture}]
     3: the act of having on your person as a covering or adornment;
        "she bought it for everyday wear" [syn: {wearing}]
     v 1: be dressed in; "She was wearing yellow that day" [syn: {have
          on}]
     2: have on one's person; "He wore a red ribbon"; "bear a scar"
        [syn: {bear}]
     3: have in one's aspect; wear an expression of one's attitude
        or personality; "He always wears a smile"
     4: deteriorate through use or stress; "The constant friction
  ...
