---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Distorted_octahedral_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 2f2c5b99-0621-4018-b8ea-f58fee0a6553
updated: 1484309275
title: Distorted octahedral molecular geometry
categories:
    - Molecular geometry
---
In chemistry, the distorted octahedral molecular geometry describes the shape of compounds where six atoms or groups of atoms or ligands are arranged around a central atom (with an electron pair capping the octahedron), defining the vertices of a gyroelongated triangular pyramid. This shape has C3v symmetry.
