---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/period
offline_file: ""
offline_thumbnail: ""
uuid: 383ef672-2f66-43a1-a9ae-e1b14fea8bfa
updated: 1484310328
title: period
categories:
    - Dictionary
---
period
     n 1: an amount of time; "a time period of 30 years"; "hastened
          the period of time of his recovery"; "Picasso's blue
          period" [syn: {time period}, {period of time}]
     2: one of three periods of play in hockey games
     3: a stage in the history of a culture having a definable place
        in space and time; "a novel from the Victorian period"
        [syn: {historic period}, {historical period}]
     4: the interval taken to complete one cycle of a regularly
        repeating phenomenon
     5: the monthly discharge of blood from the uterus of
        nonpregnant women from puberty to menopause; "the women
        were sickly and subject to excessive ...
