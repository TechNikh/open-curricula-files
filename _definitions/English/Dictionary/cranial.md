---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cranial
offline_file: ""
offline_thumbnail: ""
uuid: 8d30637f-fc9c-485c-8ba4-67e6b1362b59
updated: 1484310355
title: cranial
categories:
    - Dictionary
---
cranial
     adj : of or relating to the cranium which encloses the brain;
           "cranial pressure"
