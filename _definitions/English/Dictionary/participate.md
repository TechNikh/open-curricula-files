---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/participate
offline_file: ""
offline_thumbnail: ""
uuid: 55b403ff-d8ef-48b8-9400-5308140b180e
updated: 1484310403
title: participate
categories:
    - Dictionary
---
participate
     v 1: share in something [syn: {take part}]
     2: become a participant; be involved in; "enter a race"; "enter
        an agreement"; "enter a drug treatment program"; "enter
        negotiations" [syn: {enter}] [ant: {drop out}]
