---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mercenary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439721
title: mercenary
categories:
    - Dictionary
---
mercenary
     adj 1: marked by materialism [syn: {materialistic}, {worldly-minded}]
     2: used of soldiers hired by a foreign army [syn: {mercenary(a)},
         {freelance(a)}]
     3: profit oriented; "a commercial book"; "preached a mercantile
        and militant patriotism"- John Buchan; "a mercenary
        enterprise"; "a moneymaking business" [syn: {mercantile},
        {moneymaking(a)}]
     n : a person hired to fight for another country than their own
         [syn: {soldier of fortune}]
