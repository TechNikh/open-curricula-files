---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unwanted
offline_file: ""
offline_thumbnail: ""
uuid: 99db0720-7188-4145-b2bf-bb5e74f756f4
updated: 1484310330
title: unwanted
categories:
    - Dictionary
---
unwanted
     adj 1: not wanted; not needed; "tried to give away unwanted
            kittens" [ant: {wanted}]
     2: without being invited; "an unwanted intrusion"; "uninvited
        guests" [syn: {unasked}, {uninvited}]
     3: interfering; "unwanted shadows distort the picture";
        "removed the unwanted vegetation"
