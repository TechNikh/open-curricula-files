---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heavenly
offline_file: ""
offline_thumbnail: ""
uuid: 60302907-bca2-4334-8a36-02c3aacc8874
updated: 1484310475
title: heavenly
categories:
    - Dictionary
---
heavenly
     adj 1: relating to or inhabiting a divine heaven; "celestial
            beings"; "heavenly hosts" [syn: {celestial}]
     2: of or relating to the sky; "celestial map"; "a heavenly
        body" [syn: {celestial}]
     3: of or belonging to heaven or god [ant: {earthly}]
