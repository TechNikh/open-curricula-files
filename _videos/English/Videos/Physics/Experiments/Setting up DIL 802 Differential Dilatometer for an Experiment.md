---
version: 1
type: video
provider: YouTube
id: N_jQYgojUDM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd2c0870-84bf-4a0c-8fe5-e141ff875f04
updated: 1486069637
title: >
    Setting up DIL 802 Differential Dilatometer for an
    Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/N_jQYgojUDM/default.jpg
    - https://i3.ytimg.com/vi/N_jQYgojUDM/1.jpg
    - https://i3.ytimg.com/vi/N_jQYgojUDM/2.jpg
    - https://i3.ytimg.com/vi/N_jQYgojUDM/3.jpg
---
