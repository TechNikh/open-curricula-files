---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tirunelveli
offline_file: ""
offline_thumbnail: ""
uuid: 9414bee8-f6d5-4fe9-a428-ef9eeab5da3c
updated: 1484308428
title: Tirunelveli
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Agasthiyamalai_range_and_Tirunelveli_rainshadow.jpg
tags:
    - Etymology
    - History
    - Geography and Climate
    - Demographics
    - Economy
    - Administration and politics
    - Transport
    - culture
    - Education
    - Utilities
    - Notes
categories:
    - Uncategorized
---
Tirunelveli  pronunciation (help·info)), also known as Nellai and historically (during British rule) as Tinnevelly, is a city in the South Indian state of Tamil Nadu. It is the administrative headquarters of the Tirunelveli District. It is the sixth-largest municipal corporation in the state (after Chennai, Madurai, Coimbatore, Tiruchirappalli, Salem). Tirunelveli is located 700 km (430 mi) southwest of the state capital, Chennai and 58 km (36 mi) away from Thoothukudi.
