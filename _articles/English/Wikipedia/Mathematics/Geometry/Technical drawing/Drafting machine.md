---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drafting_machine
offline_file: ""
offline_thumbnail: ""
uuid: 369113e2-5cc8-406d-b5a0-d0387fbc3a65
updated: 1484309325
title: Drafting machine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Zeichenmaschine.jpg
categories:
    - Technical drawing
---
A drafting machine is a tool used in technical drawing, consisting of a pair of scales mounted to form a right angle on an articulated protractor head that allows an angular rotation.[1][2]
