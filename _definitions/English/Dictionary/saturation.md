---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saturation
offline_file: ""
offline_thumbnail: ""
uuid: 1eb433f0-53b3-46ac-9f59-a1cae79c5600
updated: 1484310421
title: saturation
categories:
    - Dictionary
---
saturation
     n 1: the process of totally saturating something with a
          substance; "the impregnation of wood with preservative";
          "the saturation of cotton with ether" [syn: {impregnation}]
     2: the act of soaking thoroughly with a liquid
     3: a condition in which a quantity no longer responds to some
        external influence
     4: chromatic purity: freedom from dilution with white and hence
        vividness of hue [syn: {chroma}, {intensity}, {vividness}]
