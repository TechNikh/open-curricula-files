---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urine
offline_file: ""
offline_thumbnail: ""
uuid: 62d3660b-147b-4281-b4ce-ec27766f5e44
updated: 1484310158
title: urine
categories:
    - Dictionary
---
urine
     n : liquid excretory product; "there was blood in his urine";
         "the child had to make water" [syn: {piss}, {pee}, {piddle},
          {weewee}, {water}]
