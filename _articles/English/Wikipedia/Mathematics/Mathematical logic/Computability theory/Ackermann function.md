---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ackermann_function
offline_file: ""
offline_thumbnail: ""
uuid: 8456f549-48d3-4fd7-92c8-565115713785
updated: 1484309483
title: Ackermann function
tags:
    - History
    - Definition and properties
    - Table of values
    - Expansion
    - Inverse
    - Use as benchmark
categories:
    - Computability theory
---
In computability theory, the Ackermann function, named after Wilhelm Ackermann, is one of the simplest[1] and earliest-discovered examples of a total computable function that is not primitive recursive. All primitive recursive functions are total and computable, but the Ackermann function illustrates that not all total computable functions are primitive recursive.
