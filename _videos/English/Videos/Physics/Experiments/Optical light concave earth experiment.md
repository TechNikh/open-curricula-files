---
version: 1
type: video
provider: YouTube
id: _Xgi2iTB14Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 530f505f-a32d-49e2-bd70-9a6d43771343
updated: 1486069640
title: Optical light concave earth experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/_Xgi2iTB14Q/default.jpg
    - https://i3.ytimg.com/vi/_Xgi2iTB14Q/1.jpg
    - https://i3.ytimg.com/vi/_Xgi2iTB14Q/2.jpg
    - https://i3.ytimg.com/vi/_Xgi2iTB14Q/3.jpg
---
