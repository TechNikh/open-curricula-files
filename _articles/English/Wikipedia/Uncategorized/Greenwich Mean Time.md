---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gmt
offline_file: ""
offline_thumbnail: ""
uuid: cafe16f9-c6a9-4805-9f9a-26ee533aa98b
updated: 1484308415
title: Greenwich Mean Time
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Time_zones_of_Europe_%2528Crimea_disputed%2529.svg.png'
tags:
    - History
    - Ambiguity in the definition of GMT
    - GMT in legislation
    - United Kingdom
    - Other countries
    - Time zone
    - Discrepancies between legal GMT and geographical GMT
    - Notes
categories:
    - Uncategorized
---
Greenwich Mean Time (GMT) is the mean solar time at the Royal Observatory in Greenwich, London. GMT was formerly used as the international civil time standard, now superseded in that function by Coordinated Universal Time (UTC). Today GMT is considered equivalent to UTC for UK civil purposes (but this is not formalised) and for navigation is considered equivalent to UT1 (the modern form of mean solar time at 0° longitude); these two meanings can differ by up to 0.9 s. Consequently, the term GMT should not be used for precise purposes.[1]
