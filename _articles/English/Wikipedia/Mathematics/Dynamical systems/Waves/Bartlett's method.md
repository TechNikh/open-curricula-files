---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bartlett%27s_method'
offline_file: ""
offline_thumbnail: ""
uuid: 674e18b3-2a3d-4924-9352-6918661075b4
updated: 1484308881
title: "Bartlett's method"
tags:
    - Definition and procedure
    - Related methods
categories:
    - Waves
---
In time series analysis, Bartlett's method (also known as the method of averaged periodograms[1]), is used for estimating power spectra. It provides a way to reduce the variance of the periodogram in exchange for a reduction of resolution, compared to standard periodograms.[2][3] A final estimate of the spectrum at a given frequency is obtained by averaging the estimates from the periodograms (at the same frequency) derived from a non-overlapping portions of the original series.
