---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula'
uuid: 549a1e19-29b6-4842-a320-ea7376bc7ef1
updated: 1480454887
title: "Bhaskara I's sine approximation formula"
tags:
    - The approximation formula
    - Equivalent forms of the formula
    - Accuracy of the formula
    - Derivation of the formula
    - 'Derivation based on elementary geometry [2][3]'
    - Derivation starting with a general rational expression
    - 'An elementary argument[4]'
    - Further references
categories:
    - History of geometry
---
In mathematics, Bhaskara I's sine approximation formula is a rational expression in one variable for the computation of the approximate values of the trigonometric sines discovered by Bhaskara I (c. 600 – c. 680), a seventh-century Indian mathematician.[1] This formula is given in his treatise titled Mahabhaskariya. It is not known how Bhaskara I arrived at his approximation formula. However, several historians of mathematics have put forward different hypotheses as to the method Bhaskara might have used to arrive at his formula. The formula is elegant, simple and enables one to compute reasonably accurate values of trigonometric sines without using any geometry whatsoever.[2]
