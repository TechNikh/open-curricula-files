---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telecommunication
offline_file: ""
offline_thumbnail: ""
uuid: b86abb63-c961-4d9a-bd31-9d20ecc558aa
updated: 1484310527
title: telecommunication
categories:
    - Dictionary
---
telecommunication
     n 1: (often plural) systems used in transmitting messages over a
          distance electronically [syn: {telecom}]
     2: (often plural) the branch of electrical engineering
        concerned with the technology of electronic communication
        at a distance
