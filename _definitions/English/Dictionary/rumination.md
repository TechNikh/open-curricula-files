---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rumination
offline_file: ""
offline_thumbnail: ""
uuid: dbbdedbc-5d5b-42f0-b162-26da9c6ee99e
updated: 1484310363
title: rumination
categories:
    - Dictionary
---
rumination
     n 1: a calm lengthy intent consideration [syn: {contemplation}, {reflection},
           {reflexion}, {musing}, {thoughtfulness}]
     2: (of ruminants) chewing (the cud); "ruminants have remarkable
        powers of rumination"
     3: regurgitation of small amounts of food; seen in some infants
        after feeding
