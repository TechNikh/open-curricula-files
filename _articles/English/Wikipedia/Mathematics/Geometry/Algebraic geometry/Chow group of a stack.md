---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chow_group_of_a_stack
offline_file: ""
offline_thumbnail: ""
uuid: b7248f7d-876c-490a-bb45-42b9aae7c421
updated: 1484309136
title: Chow group of a stack
categories:
    - Algebraic geometry
---
In algebraic geometry, the Chow group of a stack is a generalization of the Chow group of a variety or scheme to stacks. For quotient stacks, it is closely related to the equivariant Chow group.
