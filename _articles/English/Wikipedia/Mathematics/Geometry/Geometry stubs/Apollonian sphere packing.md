---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apollonian_sphere_packing
offline_file: ""
offline_thumbnail: ""
uuid: b18d887d-d490-4d8f-92ab-ced6d7accfe5
updated: 1484309352
title: Apollonian sphere packing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonian_spheres.jpg
categories:
    - Geometry stubs
---
Apollonian sphere packing is the three-dimensional equivalent of the Apollonian gasket. The principle of construction is very similar: with any four spheres that are cotangent to each other, it is then possible to construct two more spheres that are cotangent to four of them.
