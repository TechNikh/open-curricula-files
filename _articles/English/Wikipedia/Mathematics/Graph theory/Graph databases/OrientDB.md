---
version: 1
type: article
id: https://en.wikipedia.org/wiki/OrientDB
offline_file: ""
offline_thumbnail: ""
uuid: ffed77d0-8936-4285-8e64-dc0f771762db
updated: 1484309405
title: OrientDB
tags:
    - Engine
    - 'Editions & licenses'
    - features
    - Applications
    - History
categories:
    - Graph databases
---
OrientDB is an open source NoSQL database management system written in Java. It is a multi-model database, supporting graph, document, key/value, and object models,[2] but the relationships are managed as in graph databases with direct connections between records. It supports schema-less, schema-full and schema-mixed modes. It has a strong security profiling system based on users and roles and supports querying with Gremlin along with SQL extended for graph traversal. OrientDB uses several indexing mechanisms based on B-tree and Extendible hashing , the last one is known as "hash index", there are plans to implement LSM-tree and Fractal tree index based indexes. Each record has Surrogate ...
