---
version: 1
type: video
provider: YouTube
id: bamcYQDzVTw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Example%203-%20Multiplying%20a%20binomial%20by%20a%20polynomial%20word%20problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7a6efb79-f172-4de2-b734-ab0443948b65
updated: 1486069557
title: 'Example 3- Multiplying a binomial by a polynomial word problem'
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/bamcYQDzVTw/default.jpg
    - https://i3.ytimg.com/vi/bamcYQDzVTw/1.jpg
    - https://i3.ytimg.com/vi/bamcYQDzVTw/2.jpg
    - https://i3.ytimg.com/vi/bamcYQDzVTw/3.jpg
---
