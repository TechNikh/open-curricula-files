---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Normal
offline_file: ""
offline_thumbnail: ""
uuid: 2461be85-366f-439e-95ab-0b6e59358282
updated: 1484308323
title: Normal
tags:
    - geography
    - people
    - Psychology, education and social sciences
    - Chemistry and Technology
    - Mathematics
    - Algebra and number theory
    - Analysis
    - Geometry
    - Logic and Foundations
    - Mathematical Physics
    - Probability and Statistics
    - Topology
    - Film
    - Music
    - Transport
categories:
    - Uncategorized
---
There are many unrelated notions of "normality" in mathematics. Non-mathematical subjects very frequently refer to Normal (geometry) and Normal Distribution; see also the disambiguation pages for Normal form and Normalization.
