---
version: 1
type: video
provider: YouTube
id: F0AShQK46xA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7bd69022-96c7-4d0d-a4e2-e97681be3057
updated: 1486069630
title: "Learn the usage of the 'Bindu' (.) in Hindi Language"
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/F0AShQK46xA/default.jpg
    - https://i3.ytimg.com/vi/F0AShQK46xA/1.jpg
    - https://i3.ytimg.com/vi/F0AShQK46xA/2.jpg
    - https://i3.ytimg.com/vi/F0AShQK46xA/3.jpg
---
