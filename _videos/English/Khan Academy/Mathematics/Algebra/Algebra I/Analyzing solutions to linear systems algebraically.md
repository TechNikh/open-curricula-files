---
version: 1
type: video
provider: YouTube
id: HJV_HY0Sh0s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Analyzing%20solutions%20to%20linear%20systems%20algebraically.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c7fd0ebb-332b-44c4-ad1e-b4bc20f4315b
updated: 1486069559
title: Analyzing solutions to linear systems algebraically
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/HJV_HY0Sh0s/default.jpg
    - https://i3.ytimg.com/vi/HJV_HY0Sh0s/1.jpg
    - https://i3.ytimg.com/vi/HJV_HY0Sh0s/2.jpg
    - https://i3.ytimg.com/vi/HJV_HY0Sh0s/3.jpg
---
