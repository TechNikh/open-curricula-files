---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/another
offline_file: ""
offline_thumbnail: ""
uuid: 88963cc8-9147-443c-a8c8-e1cd2bde06eb
updated: 1484310355
title: another
categories:
    - Dictionary
---
another
     adj 1: distinctly separate from the first; "that's another (or
            different) issue altogether" [syn: {another(a)}, {different}]
     2: one more or an added; "another chance"; "another cup of
        coffee"; "an additional piece of pie" [syn: {another(a)},
        {additional}]
     3: any of various alternatives; some other; "put it off to
        another (or some other) day" [syn: {another(a)}, {some
        other}]
