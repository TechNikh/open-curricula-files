---
version: 1
type: video
provider: YouTube
id: m4DUZhnNo4s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Fungi-%20Death%20Becomes%20Them%20-%20CrashCourse%20Biology%20%2339.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2b6588ca-be47-4de0-8514-f5e022ce02fc
updated: 1486069678
title: 'Fungi- Death Becomes Them - CrashCourse Biology #39'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/m4DUZhnNo4s/default.jpg
    - https://i3.ytimg.com/vi/m4DUZhnNo4s/1.jpg
    - https://i3.ytimg.com/vi/m4DUZhnNo4s/2.jpg
    - https://i3.ytimg.com/vi/m4DUZhnNo4s/3.jpg
---
