---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/writing
offline_file: ""
offline_thumbnail: ""
uuid: 849150c2-394d-426e-9765-d62fb9851e06
updated: 1484310246
title: writing
categories:
    - Dictionary
---
writing
     n 1: the act of creating written works; "writing was a form of
          therapy for him"; "it was a matter of disputed
          authorship" [syn: {authorship}, {composition}, {penning}]
     2: the work of a writer; anything expressed in letters of the
        alphabet (especially when considered from the point of
        view of style and effect); "the writing in her novels is
        excellent"; "that editorial was a fine piece of writing"
        [syn: {written material}, {piece of writing}]
     3: (usually plural) the collected work of an author; "the idea
        occurs with increasing frequency in Hemingway's writings"
     4: letters or symbols written or imprinted on ...
