---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/patronage
offline_file: ""
offline_thumbnail: ""
uuid: bcd2248b-c791-444e-9879-2bd2436330de
updated: 1484310477
title: patronage
categories:
    - Dictionary
---
patronage
     n 1: the act of providing approval and support; "his vigorous
          backing of the conservatives got him in trouble with
          progressives" [syn: {backing}, {backup}, {championship}]
     2: customers collectively; "they have an upper class clientele"
        [syn: {clientele}, {business}]
     3: a communication that indicates lack of respect by
        patronizing the recipient [syn: {condescension}, {disdain}]
     4: (politics) granting favors or giving contracts or making
        appointments to office in return for political support
     5: the business given to a commercial establishment by its
        customers; "even before noon there was a considerable
     ...
