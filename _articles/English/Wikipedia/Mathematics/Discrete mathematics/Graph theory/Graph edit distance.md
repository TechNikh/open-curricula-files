---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_edit_distance
offline_file: ""
offline_thumbnail: ""
uuid: b7c3a6c5-f5a4-42b3-b205-0c32060aa431
updated: 1484308616
title: Graph edit distance
tags:
    - Formal definitions and properties
    - Applications
    - Algorithms and Complexity
categories:
    - Graph theory
---
In mathematics and computer science, graph edit distance (GED) is a measure of similarity (or dissimilarity) between two graphs. The concept of graph edit distance was first formalized mathematically by Alberto Sanfliu and King-Sun Fu in 1983.[1] A major application of graph edit distance is in inexact graph matching, such as error-tolerant pattern recognition in machine learning.[2]
