---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Agra
offline_file: ""
offline_thumbnail: ""
uuid: 3482a759-4367-4570-83d9-d367425c28f8
updated: 1484308412
title: Agra
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-One_of_the_drawings_of_Mughal_monuments_at_Agra_and_Fatehpur_Sikri.jpg
tags:
    - Climate
    - population
    - Demographics
    - religion
    - History
    - Origins
    - Mughal era
    - Later periods
    - Legacies
    - Transport
    - air
    - Taj International Airport
    - Rail
    - Railway stations
    - Agra-Delhi Semi-High Speed Train
    - Road
    - Local transport
    - Metro
    - Places of interest
    - Tāj Mahal
    - Agra Fort
    - Fatehpur Sikri
    - "I'timād-Ud-Daulah"
    - "Akbar's Tomb, Sikandra"
    - Mankameshwar Temple
    - "Indrabhan Girls' Inter College"
    - Gurudwara Guru ka Taal
    - Jamā Masjid
    - Chīnī kā Rauza
    - Rām Bāgh
    - "Mariam's Tomb"
    - Mehtāb Bāgh
    - Keetham Lake
    - Mughal Heritage Walk
    - The Cathedral of the Immaculate Conception
    - Paliwal Park (Hewitt Park)
    - Economy
    - Property
    - Future projects
    - Development
    - Education
    - Universities and colleges
    - Media
    - Radio stations
    - Fairs and festivals
    - Taj Mahotsav
    - Ram Barat
    - Taj Literature Festival
    - Kailash Fair
    - Gangaur Fair
    - Taj Municipal Museum in Paliwal Park Agra
    - Notable individuals
categories:
    - Uncategorized
---
Agra (i/ˈɑːɡrə/; Āgrā) is a city on the banks of the river Yamuna in the northern state of Uttar Pradesh, India.[3] It is 378 kilometres (235 mi) west of the state capital, Lucknow, 206 kilometres (128 mi) south of the national capital New Delhi and 125 kilometres (78 mi) north of Gwalior. With a population of 1,686,993 (2013 est.), it is one of the most populous cities in Uttar Pradesh and the 19th most populous in India.[4] Agra can also refer to the administrative district that has its headquarters in Agra city. It is a major tourist destination because of its many splendid Mughal-era buildings, most notably the Tāj Mahal, Agra Fort and Fatehpūr Sikrī, all three of which are ...
