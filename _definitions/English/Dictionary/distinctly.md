---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinctly
offline_file: ""
offline_thumbnail: ""
uuid: 847fd551-43f1-496d-974d-091f911d520c
updated: 1484310284
title: distinctly
categories:
    - Dictionary
---
distinctly
     adv 1: clear to the mind; with distinct mental discernment; "it's
            distinctly possible"; "I could clearly see myself in
            his situation" [syn: {clearly}]
     2: in a distinct and distinguishable manner; "the subtleties of
        this distinctly British occasion"
     3: to a distinct degree; "urbanization in Spain is distinctly
        correlated with a fall in reproductive rate"
