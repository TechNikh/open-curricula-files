---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inertia
offline_file: ""
offline_thumbnail: ""
uuid: 5d58795f-00b2-4e51-8e0f-54fa141874d4
updated: 1484310366
title: inertia
categories:
    - Dictionary
---
inertia
     n 1: a disposition to remain inactive or inert; "he had to
          overcome his inertia and get back to work" [syn: {inactiveness},
           {inactivity}] [ant: {activeness}]
     2: (physics) the tendency of a body to maintain is state of
        rest or uniform motion unless acted upon by an external
        force
