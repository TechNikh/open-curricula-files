---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rickshaw
offline_file: ""
offline_thumbnail: ""
uuid: 7dca4cef-c837-4298-ae9d-74d601ec02dd
updated: 1484310484
title: rickshaw
categories:
    - Dictionary
---
rickshaw
     n : a small two-wheeled cart for one passenger; pulled by one
         person [syn: {jinrikisha}, {ricksha}]
