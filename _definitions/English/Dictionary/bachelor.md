---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bachelor
offline_file: ""
offline_thumbnail: ""
uuid: 61a743c5-5104-49ae-b112-b6117856e4b5
updated: 1484310431
title: bachelor
categories:
    - Dictionary
---
bachelor
     n 1: a man who has never been married [syn: {unmarried man}]
     2: a knight of the lowest order; could display only a pennon
        [syn: {knight bachelor}, {bachelor-at-arms}]
     v : lead a bachelor's existence [syn: {bach}]
