---
version: 1
type: video
provider: YouTube
id: g1WU35KxLrA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Big%20Bang%20in%20an%20infinite%20Universe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1d109212-bc06-4405-b82a-7bf38504b12f
updated: 1486069656
title: Big Bang in an infinite Universe
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/g1WU35KxLrA/default.jpg
    - https://i3.ytimg.com/vi/g1WU35KxLrA/1.jpg
    - https://i3.ytimg.com/vi/g1WU35KxLrA/2.jpg
    - https://i3.ytimg.com/vi/g1WU35KxLrA/3.jpg
---
