---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calcium
offline_file: ""
offline_thumbnail: ""
uuid: c63326ca-4b9f-40d9-9e68-a5557dc78f37
updated: 1484310198
title: calcium
categories:
    - Dictionary
---
calcium
     n : a white metallic element that burns with a brilliant light;
         the fifth most abundant element in the earth's crust; an
         important component of most plants and animals [syn: {Ca},
          {atomic number 20}]
