---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fortune%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 09b77df2-04db-417c-a485-43f3ecc18f54
updated: 1484309173
title: "Fortune's algorithm"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Fortunes-algorithm-slowed.gif
tags:
    - Algorithm description
    - Pseudocode
    - Weighted sites and disks
categories:
    - Computational geometry
---
Fortune's algorithm is a sweep line algorithm for generating a Voronoi diagram from a set of points in a plane using O(n log n) time and O(n) space.[1][2] It was originally published by Steven Fortune in 1986 in his paper "A sweepline algorithm for Voronoi diagrams."[3]
