---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/role
offline_file: ""
offline_thumbnail: ""
uuid: 378cd8f7-90c3-4d5d-9f6d-a0a0f0e122a6
updated: 1484310349
title: role
categories:
    - Dictionary
---
role
     n 1: the actions and activities assigned to or required or
          expected of a person or group; "the function of a
          teacher"; "the government must do its part"; "play its
          role" [syn: {function}, {office}, {part}]
     2: an actor's portrayal of someone in a play; "she played the
        part of Desdemona" [syn: {character}, {theatrical role}, {part},
         {persona}]
     3: what something is used for; "the function of an auger is to
        bore holes"; "ballet is beautiful but what use is it?"
        [syn: {function}, {purpose}, {use}]
     4: normal or customary activity of a person in a particular
        social setting; "what is your role on the ...
