---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Institute_of_Refrigeration
offline_file: ""
offline_thumbnail: ""
uuid: 8c604e81-a14f-458a-99e4-71ce0686ccf0
updated: 1484308846
title: International Institute of Refrigeration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-COOL-SAVE_Best_Practice_Guide.jpg
tags:
    - History
    - Organization
    - Statutory bodies
    - General Conference
    - Executive Committee
    - Management Committee
    - Science and Technology Council
    - Commissions
    - 'Section A: Cryogenics and liquefied gases'
    - 'Section B: Thermodynamics, equipment and systems'
    - 'Section C: Biology and food technology'
    - 'Section D: Storage and transport'
    - 'Section E: Air conditioning, heat pumps, energy recovery'
    - Recent European Projects
    - ELICiT
    - COOL-SAVE
    - FRISBEE
    - Activities and services
    - Fridoc database
    - Publications
    - Newsletter
    - International Journal of Refrigeration
    - Conferences and congresses
    - Working groups
    - IIR network
    - Member countries
    - Benefactor and corporate members
    - Private members
categories:
    - Thermodynamics
---
The International Institute of Refrigeration (IIR) (also known, in French, as the Institut International du Froid (IIF)), is an independent intergovernmental science and technology based organization which promotes knowledge of refrigeration and associated technologies and applications on a global scale that improve quality of life in a cost effective and environmentally sustainable manner including:
