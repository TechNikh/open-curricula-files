---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cad
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484373061
title: cad
categories:
    - Dictionary
---
cad
     n 1: someone who is morally reprehensible; "you dirty dog" [syn:
          {bounder}, {blackguard}, {dog}, {hound}, {heel}]
     2: software used in art and architecture and engineering and
        manufacturing to assist in precision drawing [syn: {computer-aided
        design}]
