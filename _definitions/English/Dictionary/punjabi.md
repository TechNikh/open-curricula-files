---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/punjabi
offline_file: ""
offline_thumbnail: ""
uuid: 068c93d6-1b7a-4238-ba0c-788c7b74cc49
updated: 1484310585
title: punjabi
categories:
    - Dictionary
---
Punjabi
     n 1: a member of the majority people of Punjab in northwestern
          India [syn: {Panjabi}]
     2: the Indic language spoken by most people in Punjab in
        northwestern India [syn: {Panjabi}]
