---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Implementation_intention
offline_file: ""
offline_thumbnail: ""
uuid: 6b66ba90-249a-404d-b69a-9e8c714fe795
updated: 1484309069
title: Implementation intention
tags:
    - History
    - concept
    - Empirical support
    - Physical health goals
    - Emotion regulation
    - Mechanism
    - Implementation Intentions and Goal Shielding
    - Limitations
categories:
    - Strategy
---
An implementation intention (II) is a self-regulatory strategy in the form of an "if-then plan" that can lead to better goal attainment, as well as help in habit and behavior modification. It is subordinate to goal intentions as it specifies the when, where and how portions of goal-directed behavior. The concept of implementation intentions was introduced in 1999 by psychologist Peter Gollwitzer.[1] Studies conducted in 1997 and earlier showed that the use of implementation intentions can result in a higher probability of successful goal attainment, by predetermining a specific and desired goal-directed behavior in response to a particular future event or cue.[2]
