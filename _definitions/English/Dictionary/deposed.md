---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deposed
offline_file: ""
offline_thumbnail: ""
uuid: d1be89b0-2882-401c-9d2f-ac707ad115bc
updated: 1484310573
title: deposed
categories:
    - Dictionary
---
deposed
     adj : removed from office or power; "the deposed boatswain became
           Hudson's sworn enemy"
