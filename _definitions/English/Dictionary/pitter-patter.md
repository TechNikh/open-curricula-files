---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitter-patter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495761
title: pitter-patter
categories:
    - Dictionary
---
pitter-patter
     n : a series of rapid tapping sounds; "she missed the
         pitter-patter of little feet around the house"
     adv 1: as of footsteps; "he came running pit-a-pat down the hall"
            [syn: {pit-a-pat}, {pitty-patty}, {pitty-pat}]
     2: describing a rhythmic beating; "his heart went pit-a-pat"
        [syn: {pit-a-pat}, {pitty-patty}, {pitty-pat}]
     v 1: rain gently; "It has only sprinkled, but the roads are
          slick" [syn: {sprinkle}, {spit}, {spatter}, {patter}]
     2: make light, rapid and repeated sounds; "gently pattering
        rain" [syn: {patter}]
