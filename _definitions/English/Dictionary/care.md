---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/care
offline_file: ""
offline_thumbnail: ""
uuid: d9822c00-0a28-43c1-b3f5-504c37788560
updated: 1484310254
title: care
categories:
    - Dictionary
---
care
     n 1: the work of caring for or attending to someone or something;
          "no medical care was required"; "the old car needed
          constant attention" [syn: {attention}, {aid}, {tending}]
     2: judiciousness in avoiding harm or danger; "he exercised
        caution in opening the door"; "he handled the vase with
        care" [syn: {caution}, {precaution}, {forethought}]
     3: an anxious feeling; "care had aged him"; "they hushed it up
        out of fear of public reaction" [syn: {concern}, {fear}]
     4: a cause for feeling concern; "his major care was the illness
        of his wife"
     5: attention and management implying responsibility for safety;
        "he is ...
