---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extravagance
offline_file: ""
offline_thumbnail: ""
uuid: f9f28bbd-e283-4dbb-a7ec-6d31a69c5e62
updated: 1484310148
title: extravagance
categories:
    - Dictionary
---
extravagance
     n 1: the quality of exceeding the appropriate limits of decorum
          or probability or truth; "we were surprised by the
          extravagance of his description" [syn: {extravagancy}]
     2: the trait of spending extravagantly [syn: {prodigality}, {profligacy}]
     3: excessive spending [syn: {prodigality}, {lavishness}, {highlife},
         {high life}]
