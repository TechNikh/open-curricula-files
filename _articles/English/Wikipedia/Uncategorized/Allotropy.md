---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Allotropy
offline_file: ""
offline_thumbnail: ""
uuid: af3b128c-269f-4046-9ca9-707a68505611
updated: 1484308391
title: Allotropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Diamond_and_graphite_0.jpg
tags:
    - History
    - "Differences in properties of an element's allotropes"
    - List of allotropes
    - Non-metals
    - Metalloids
    - Metals
    - Lanthanides and actinides
    - Notes
categories:
    - Uncategorized
---
Allotropy or allotropism (from Greek ἄλλος (allos), meaning "other", and τρόπος (tropos), meaning "manner, form") is the property of some chemical elements to exist in two or more different forms, in the same physical state, known as allotropes of these elements. Allotropes are different structural modifications of an element;[1] the atoms of the element are bonded together in a different manner. For example, the allotropes of carbon include diamond (where the carbon atoms are bonded together in a tetrahedral lattice arrangement), graphite (where the carbon atoms are bonded together in sheets of a hexagonal lattice), graphene (single sheets of graphite), and fullerenes (where ...
