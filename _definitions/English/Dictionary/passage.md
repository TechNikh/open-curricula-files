---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passage
offline_file: ""
offline_thumbnail: ""
uuid: 79a1d8b0-951a-4172-8aa3-6466c97c503c
updated: 1484310330
title: passage
categories:
    - Dictionary
---
passage
     n 1: the act of passing from one state or place to the next [syn:
           {transition}]
     2: a section of text; particularly a section of medium length
     3: a way through or along which someone or something may pass
     4: the passing of a law by a legislative body [syn: {enactment}]
     5: a journey usually by ship; "the outward passage took 10
        days" [syn: {transit}]
     6: a short section of a musical composition [syn: {musical
        passage}]
     7: a path or channel or duct through or along which something
        may pass; "the nasal passages" [syn: {passageway}]
     8: a bodily process of passing from one place or stage to
        another; "the ...
