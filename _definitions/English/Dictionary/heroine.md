---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heroine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484548381
title: heroine
categories:
    - Dictionary
---
heroine
     n 1: the main good female character in a work of fiction
     2: a woman possessing heroic qualities or a woman who has
        performed heroic deeds
