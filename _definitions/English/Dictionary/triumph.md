---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triumph
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410321
title: triumph
categories:
    - Dictionary
---
triumph
     n 1: a successful ending of a struggle or contest; "the general
          always gets credit for his army's victory"; "the
          agreement was a triumph for common sense" [syn: {victory}]
          [ant: {defeat}]
     2: the exultation of victory
     v 1: prove superior; "The champion prevailed, though it was a
          hard fight" [syn: {prevail}]
     2: be ecstatic with joy [syn: {wallow}, {rejoice}]
     3: dwell on with satisfaction [syn: {gloat}, {crow}]
     4: to express great joy; "Who cannot exult in Spring?" [syn: {exult},
         {rejoice}, {jubilate}]
