---
version: 1
type: video
provider: YouTube
id: vVXbgbMp0oY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Frequency%20Stability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3e29e33b-c057-4b23-9f86-083cccb26151
updated: 1486069688
title: Frequency Stability
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/vVXbgbMp0oY/default.jpg
    - https://i3.ytimg.com/vi/vVXbgbMp0oY/1.jpg
    - https://i3.ytimg.com/vi/vVXbgbMp0oY/2.jpg
    - https://i3.ytimg.com/vi/vVXbgbMp0oY/3.jpg
---
