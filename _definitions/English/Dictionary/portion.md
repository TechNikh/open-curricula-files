---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/portion
offline_file: ""
offline_thumbnail: ""
uuid: 9060aca3-f374-42de-91b7-8478d55ef505
updated: 1484310343
title: portion
categories:
    - Dictionary
---
portion
     n 1: something determined in relation to something that includes
          it; "he wanted to feel a part of something bigger than
          himself"; "I read a portion of the manuscript"; "the
          smaller component is hard to reach" [syn: {part}, {component
          part}, {component}]
     2: something less than the whole of a human artifact; "the rear
        part of the house"; "glue the two parts together" [syn: {part}]
     3: the result of parcelling out or sharing; "death gets more
        than its share of attention from theologicans" [syn: {parcel},
         {share}]
     4: assets belonging to or due to or contributed by an
        individual person or group; ...
