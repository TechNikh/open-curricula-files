---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kroto
offline_file: ""
offline_thumbnail: ""
uuid: 6039c19a-0fe5-43d8-b862-600fd0fa207a
updated: 1484310414
title: kroto
categories:
    - Dictionary
---
Kroto
     n : British chemist who with Robert Curl and Richard Smalley
         discovered fullerenes and opened a new branch of
         chemistry (bron in 1939) [syn: {Harold Kroto}, {Harold W.
         Kroto}, {Sir Harold Walter Kroto}]
