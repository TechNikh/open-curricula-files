---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/De_Bruijn%E2%80%93Erd%C5%91s_theorem_(graph_theory)'
offline_file: ""
offline_thumbnail: ""
uuid: 769db501-ca50-40be-9447-6f4766cb67e6
updated: 1484309379
title: De Bruijn–Erdős theorem (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hadwiger-Nelson.svg.png
tags:
    - Proofs
    - Dependence on choice
    - Applications
    - Generalizations
    - Notes
categories:
    - Graph coloring
---
In graph theory, the De Bruijn–Erdős theorem, proved by Nicolaas Govert de Bruijn and Paul Erdős (1951), states that, for every infinite graph G and finite integer k, G can be colored by k colors (with no two adjacent vertices having the same color) if and only if all of its finite subgraphs can be colored by k colors. That is, every k-critical graph (a graph that requires k colors but for which all subgraphs require fewer colors) must have a finite number of vertices.
