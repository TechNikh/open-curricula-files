---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Glide_plane
offline_file: ""
offline_thumbnail: ""
uuid: f26a6ad1-f6e4-4605-81f0-ffa74477fe81
updated: 1484309311
title: Glide plane
categories:
    - Symmetry
---
In geometry, and crystallography, a glide plane (or transflection) is a symmetry operation describing how a reflection in a plane, followed by a translation parallel with that plane, may leave the crystal unchanged.
