---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/layout
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484503141
title: layout
categories:
    - Dictionary
---
lay out
     v 1: lay out in a line [syn: {range}, {array}, {set out}]
     2: get ready for a particular purpose or event; "set up an
        experiment"; "set the table"; "lay out the tools for the
        surgery" [syn: {set up}, {set}]
     3: bring forward and present to the mind; "We presented the
        arguments to him"; "We cannot represent this knowledge to
        our formal reason" [syn: {present}, {represent}]
     4: provide a detailed plan or design; "She laid out her plans
        for the new house"
