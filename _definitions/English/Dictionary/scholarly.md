---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scholarly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516641
title: scholarly
categories:
    - Dictionary
---
scholarly
     adj : characteristic of scholars or scholarship; "scholarly
           pursuits"; "a scholarly treatise"; "a scholarly
           attitude" [ant: {unscholarly}]
