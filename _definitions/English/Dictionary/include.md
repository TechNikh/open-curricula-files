---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/include
offline_file: ""
offline_thumbnail: ""
uuid: f0e38525-ffa3-42b4-b87a-e9498fab8c60
updated: 1484310254
title: include
categories:
    - Dictionary
---
include
     v 1: have as a part, be made up out of; "The list includes the
          names of many famous writers" [ant: {exclude}]
     2: consider as part of something; "I include you in the list of
        culprits" [ant: {exclude}]
     3: add as part of something else; put in as part of a set,
        group, or category; "We must include this chemical element
        in the group"
     4: allow participation in or the right to be part of; permit to
        exercise the rights, functions, and responsibilities of;
        "admit someone to the profession"; "She was admitted to
        the New Jersey Bar" [syn: {admit}, {let in}] [ant: {exclude}]
