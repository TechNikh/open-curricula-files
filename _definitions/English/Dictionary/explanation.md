---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/explanation
offline_file: ""
offline_thumbnail: ""
uuid: 30d4e086-184c-4ca7-bf85-a973a20ccaf6
updated: 1484310221
title: explanation
categories:
    - Dictionary
---
explanation
     n 1: a statement that makes something comprehensible by
          describing the relevant structure or operation or
          circumstances etc.; "the explanation was very simple";
          "I expected a brief account" [syn: {account}]
     2: thought that makes something comprehensible
     3: the act of explaining; making something plain or
        intelligible; "I heard his explanation of the accident"
