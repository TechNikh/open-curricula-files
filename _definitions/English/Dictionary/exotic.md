---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exotic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484600581
title: exotic
categories:
    - Dictionary
---
exotic
     adj 1: being or from or characteristic of another place or part of
            the world; "alien customs"; "exotic plants in a
            greenhouse"; "exotic cuisine" [syn: {alien}]
     2: strikingly strange or unusual; "an exotic hair style";
        "protons, neutrons, electrons and all their exotic
        variants"; "the exotic landscape of a dead planet"
