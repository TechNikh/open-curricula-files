---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imaginative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414161
title: imaginative
categories:
    - Dictionary
---
imaginative
     adj : (used of persons or artifacts) marked by independence and
           creativity in thought or action; "an imaginative use of
           material"; "the invention of the knitting frame by
           another ingenious English clergyman"- Lewis Mumford;
           "an ingenious device"; "had an inventive turn of mind";
           "inventive ceramics" [syn: {ingenious}, {inventive}]
