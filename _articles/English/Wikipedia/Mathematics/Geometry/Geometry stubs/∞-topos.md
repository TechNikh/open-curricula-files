---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%E2%88%9E-topos'
offline_file: ""
offline_thumbnail: ""
uuid: 854d7ecf-7f4f-4223-80f5-e38f112f55f5
updated: 1484309352
title: ∞-topos
categories:
    - Geometry stubs
---
In mathematics, an ∞-topos is, roughly, an ∞-category such that its objects are sheaves with some choice of Grothendieck topology; in other words, it gives an intrinsic notion of sheaves without reference to an external space. The prototypical example of an ∞-topos is the ∞-category of sheaves of, say, abelian groups on some topological space. But the notion is more flexible; for example, the ∞-category of étale sheaves on some affine scheme is not the ∞-category of sheaves on any topological space but it is still an ∞-topos.
