---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_geometers
offline_file: ""
offline_thumbnail: ""
uuid: 972a18f1-dfa6-46b5-b140-39049d1c5a18
updated: 1484309073
title: List of geometers
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-P._Oxy._I_29.jpg
tags:
    - 800 BC to 1 BC
    - 1–1400 AD
    - 1401–1800 AD
    - 1801–1900 AD
    - 1901–present
    - Geometers in art
categories:
    - Geometers
---
Some important geometers and their main fields of work, chronologically listed are:
