---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disrupt
offline_file: ""
offline_thumbnail: ""
uuid: 0ad7ac86-be0e-4c98-abf1-956a4f3468f5
updated: 1484310441
title: disrupt
categories:
    - Dictionary
---
disrupt
     v 1: make a break in; "We interrupt the program for the following
          messages" [syn: {interrupt}, {break up}, {cut off}]
     2: throw into disorder; "This event disrupted the orderly
        process"
     3: interfere in someone else's activity; "Please don't
        interrupt me while I'm on the phone" [syn: {interrupt}]
