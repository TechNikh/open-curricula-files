---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nickel
offline_file: ""
offline_thumbnail: ""
uuid: 585dbdc9-aaca-4d18-ab61-62040664e13d
updated: 1484310198
title: nickel
categories:
    - Dictionary
---
nickel
     n 1: a hard malleable ductile silvery metallic element that is
          resistant to corrosion; used in alloys; occurs in
          pentlandite and smaltite and garnierite and millerite
          [syn: {Ni}, {atomic number 28}]
     2: a United States coin worth one twentieth of a dollar
     3: five dollars worth of a drug; "a nickel bag of drugs"; "a
        nickel deck of heroin" [syn: {nickel note}]
     v : plate with nickel; "nickel the plate"
     [also: {nickelling}, {nickelled}]
