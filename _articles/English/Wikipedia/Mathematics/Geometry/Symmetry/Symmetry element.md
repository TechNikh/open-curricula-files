---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_element
offline_file: ""
offline_thumbnail: ""
uuid: 8f347b4a-673d-4618-948b-04ea1f9f6a95
updated: 1484309317
title: Symmetry element
tags:
    - Identity
    - Mirror planes
    - Cyclic symmetry
    - Inversion
    - Gallery
categories:
    - Symmetry
---
A symmetry element is a point of reference about which symmetry operations can take place. In particular, symmetry elements can be identities, mirror planes, axes of rotation (both proper and improper), and centers of inversion.[1] A symmetry element corresponds to a symmetry operation that generates the same representation of an object.
