---
version: 1
type: video
provider: YouTube
id: TSv-Rq5C3K8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20and%20RNA%20transcription%20video%20-%20real%20time%20DNA%20encoding%20pr.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4b91148a-a504-42a8-bd90-a90435876632
updated: 1486069673
title: 'DNA and RNA transcription video - real time DNA encoding pr'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TSv-Rq5C3K8/default.jpg
    - https://i3.ytimg.com/vi/TSv-Rq5C3K8/1.jpg
    - https://i3.ytimg.com/vi/TSv-Rq5C3K8/2.jpg
    - https://i3.ytimg.com/vi/TSv-Rq5C3K8/3.jpg
---
