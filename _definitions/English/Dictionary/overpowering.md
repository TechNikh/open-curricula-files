---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overpowering
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463241
title: overpowering
categories:
    - Dictionary
---
overpowering
     adj : so strong as to be irresistible; "an overpowering need for
           solitude"; "the temptation to despair may become
           overwhelming"; "an overwhelming majority" [syn: {overwhelming}]
