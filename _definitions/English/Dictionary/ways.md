---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ways
offline_file: ""
offline_thumbnail: ""
uuid: 2c7182e9-c637-4881-87ae-94f1ef9dd834
updated: 1484310305
title: ways
categories:
    - Dictionary
---
ways
     n 1: the property of distance in general; "it's a long way to
          Moscow"; "he went a long ways" [syn: {way}]
     2: structure consisting of a sloping way down to the water from
        the place where ships are built or repaired [syn: {shipway},
         {slipway}]
     adv : n is between 2 and infinity; "They split the loot four ways"
           [syn: {n-ways}]
