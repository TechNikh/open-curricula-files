---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antiresonance
offline_file: ""
offline_thumbnail: ""
uuid: 0838c61c-95b0-4947-a3fe-b6d358739ee5
updated: 1484308816
title: Antiresonance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Antires_spectra.svg.png
tags:
    - Antiresonance in coupled oscillators
    - Interpretation as destructive interference
    - Complex coupled systems
    - Applications
    - Other uses
categories:
    - Oscillation
---
In the physics of coupled oscillators, antiresonance, by analogy with resonance, is a pronounced minimum in the amplitude of one oscillator at a particular frequency, accompanied by a large shift in its oscillation phase. Such frequencies are known as the system's antiresonant frequencies, and at these frequencies the oscillation amplitude can drop to almost zero. Antiresonances are caused by destructive interference, for example between an external driving force and interaction with another oscillator.
