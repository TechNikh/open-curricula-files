---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gallai%E2%80%93Hasse%E2%80%93Roy%E2%80%93Vitaver_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 93f34a0b-f6ce-4e15-bb37-2682fcaca9ee
updated: 1484309234
title: Gallai–Hasse–Roy–Vitaver theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gallai-Hasse-Roy-Vitaver_theorem.svg.png
tags:
    - Examples
    - Proof
    - Category-theoretic interpretation
    - History
categories:
    - Duality theories
---
In graph theory, the Gallai–Hasse–Roy–Vitaver theorem is a form of duality between the colorings of the vertices of a given undirected graph and the orientations of its edges. It states that the minimum number of colors needed to properly color any graph G equals one plus the length of a longest path in an orientation of G chosen to minimize this path's length.[1] The orientations for which the longest path has minimum length always include at least one acyclic orientation.[2]
