---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glass
offline_file: ""
offline_thumbnail: ""
uuid: 390fa345-0328-48a2-9448-ef2be0ba5974
updated: 1484310333
title: glass
categories:
    - Dictionary
---
glass
     n 1: a brittle transparent solid with irregular atomic structure
     2: a glass container for holding liquids while drinking [syn: {drinking
        glass}]
     3: the quantity a glass will hold [syn: {glassful}]
     4: a small refracting telescope [syn: {field glass}, {spyglass}]
     5: amphetamine used in the form of a crystalline hydrochloride;
        used as a stimulant to the nervous system and as an
        appetite suppressant [syn: {methamphetamine}, {methamphetamine
        hydrochloride}, {Methedrine}, {meth}, {deoxyephedrine}, {chalk},
         {chicken feed}, {crank}, {ice}, {shabu}, {trash}]
     6: a mirror; usually a ladies' dressing mirror [syn: {looking
     ...
