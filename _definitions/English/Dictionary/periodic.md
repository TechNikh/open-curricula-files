---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/periodic
offline_file: ""
offline_thumbnail: ""
uuid: ea4441d6-e27b-48b2-b08a-4760838871ed
updated: 1484310389
title: periodic
categories:
    - Dictionary
---
periodic
     adj 1: happening or recurring at regular intervals [syn: {periodical}]
            [ant: {aperiodic}]
     2: recurring at regular intervals
     3: recurring or reappearing from time to time; "periodic
        feelings of anxiety"
