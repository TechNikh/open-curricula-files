---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Keralites
offline_file: ""
offline_thumbnail: ""
uuid: e5293d85-1438-49fc-bbc3-cf258629b5d1
updated: 1484308456
title: Malayali
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-British_Residency_in_Asramam%252C_Kollam.jpg'
tags:
    - Etymology
    - Geographic distribution and population
    - History and culture
    - Language and literature
    - Tharavadu
    - Architecture
    - Nalukettu
    - Performing arts and music
    - Vallam Kali
    - Festivals
    - Cuisine
    - Martial arts
    - Keralites
categories:
    - Uncategorized
---
The Malayali or Malayalee (Malayalam: മലയാളി) are an ethno-linguistic group native to the South Indian state of Kerala and the Union Territory of Lakshadweep and have the Malayalam language as their mother tongue. The Malayali community in India has a history of immigrants to the region from various parts of the world, as well as a unique sub-culture owing to the tropical environment of the state.[13][14]
