---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anode
offline_file: ""
offline_thumbnail: ""
uuid: c3b983c9-7a09-4ce7-84eb-d1e70a5b2ddb
updated: 1484310202
title: anode
categories:
    - Dictionary
---
anode
     n 1: a positively charged electrode by which electrons leave an
          electrical device [ant: {cathode}]
     2: the negatively charged terminal of a voltaic cell or storage
        battery that supplies current [ant: {cathode}]
