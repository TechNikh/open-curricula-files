---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gender
offline_file: ""
offline_thumbnail: ""
uuid: 42154fca-2a66-4382-89ff-afb98a1548d3
updated: 1484310448
title: gender
categories:
    - Dictionary
---
gender
     n 1: a grammatical category in inflected languages governing the
          agreement between nouns and pronouns and adjectives; in
          some languages it is quite arbitrary but in
          Indo-European languages it is usually based on sex or
          animateness [syn: {grammatical gender}]
     2: the properties that distinguish organisms on the basis of
        their reproductive roles; "she didn't want to know the sex
        of the foetus" [syn: {sex}, {sexuality}]
