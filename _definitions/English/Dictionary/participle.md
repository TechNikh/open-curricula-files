---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/participle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484497261
title: participle
categories:
    - Dictionary
---
participle
     n : a non-finite form of the verb; in English it is used
         adjectivally and to form compound tenses [syn: {participial}]
