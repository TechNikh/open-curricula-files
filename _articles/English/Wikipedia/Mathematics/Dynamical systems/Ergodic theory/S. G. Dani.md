---
version: 1
type: article
id: https://en.wikipedia.org/wiki/S._G._Dani
offline_file: ""
offline_thumbnail: ""
uuid: aca23be1-93ee-4988-b9f0-4041d6894b6d
updated: 1484308780
title: S. G. Dani
tags:
    - Education
    - Administration
    - Awards and recognition
categories:
    - Ergodic theory
---
Shrikrishna Gopalrao Dani is a Professor of mathematics at the Indian Institute of Technology, Bombay, Mumbai who works in the broad area of ergodic theory.[1]
