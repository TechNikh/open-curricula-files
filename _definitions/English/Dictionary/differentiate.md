---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/differentiate
offline_file: ""
offline_thumbnail: ""
uuid: 83b434a9-fdff-4c19-b16e-32206a337db3
updated: 1484310234
title: differentiate
categories:
    - Dictionary
---
differentiate
     v 1: mark as different; "We distinguish several kinds of maple"
          [syn: {distinguish}, {separate}, {secern}, {secernate},
          {severalize}, {severalise}, {tell}, {tell apart}]
     2: be a distinctive feature, attribute, or trait; sometimes in
        a very positive sense; "His modesty distinguishes him form
        his peers" [syn: {distinguish}, {mark}]
     3: calculate a derivative; take the derivative [ant: {integrate}]
     4: become different during development; "cells differentiate"
        [ant: {dedifferentiate}]
     5: evolve so as to lead to a new species or develop in a way
        most suited to the environment [syn: {speciate}, ...
