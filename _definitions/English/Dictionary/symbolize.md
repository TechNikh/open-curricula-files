---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symbolize
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484612161
title: symbolize
categories:
    - Dictionary
---
symbolize
     v 1: express indirectly by an image, form, or model; be a symbol;
          "What does the Statue of Liberty symbolize?" [syn: {typify},
           {symbolise}, {stand for}, {represent}]
     2: represent or identify by using a symbol; use symbols; "The
        poet symbolizes love in this poem"; "These painters
        believed that artists should symbolize" [syn: {symbolise}]
