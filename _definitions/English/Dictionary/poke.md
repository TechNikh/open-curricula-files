---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poke
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460961
title: poke
categories:
    - Dictionary
---
poke
     n 1: tall coarse perennial American herb having small white
          flowers followed by blackish-red berries on long
          drooping racemes; young fleshy stems are edible; berries
          and root are poisonous [syn: {pigeon berry}, {garget}, {scoke},
           {Phytolacca americana}]
     2: a bag made of paper or plastic for holding customer's
        purchases [syn: {sack}, {paper bag}, {carrier bag}]
     3: a sharp hand gesture (resembling a blow); "he warned me with
        a jab with his finger"; "he made a thrusting motion with
        his fist" [syn: {jab}, {jabbing}, {poking}, {thrust}, {thrusting}]
     4: (boxing) a blow with the fist; "I gave him a clout on ...
