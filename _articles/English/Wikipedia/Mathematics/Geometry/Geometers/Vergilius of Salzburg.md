---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vergilius_of_Salzburg
offline_file: ""
offline_thumbnail: ""
uuid: ab713707-26cb-4587-aa3d-fa32082ba86b
updated: 1484309105
title: Vergilius of Salzburg
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-St.Adolari_-_Empore_6_Virgil.jpg
tags:
    - Biography
    - Veneration
    - Dedications
    - 'Art & Culture'
    - Sources
categories:
    - Geometers
---
Vergilius of Salzburg (also Virgilius, Feirgil or Fergal) (born c. 700 in Ireland; died 27 November 784 in Salzburg) was an Irish churchman and early astronomer; he served as abbot of Aghaboe, bishop of Ossory and later, bishop of Salzburg. He was called "the Apostle of Carinthia"[1] and "the geometer".
