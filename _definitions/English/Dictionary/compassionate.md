---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compassionate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415961
title: compassionate
categories:
    - Dictionary
---
compassionate
     adj 1: showing merciful compassion; "sparing the child's mother was
            a compassionate act"
     2: showing or having compassion; "heard the soft and
        compassionate voices of women" [ant: {uncompassionate}]
     3: showing recognition of unusually distressful circumstances;
        "compassionate leave"; "considered for a compassionate
        discharge because of domestic difficulties"
     v : share the suffering of [syn: {feel for}, {pity}, {condole
         with}, {sympathize with}]
