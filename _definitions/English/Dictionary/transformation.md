---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transformation
offline_file: ""
offline_thumbnail: ""
uuid: 5c7c887b-08bd-4b9c-b867-e3ca8880ac84
updated: 1484310224
title: transformation
categories:
    - Dictionary
---
transformation
     n 1: a qualitative change [syn: {transmutation}, {shift}]
     2: (mathematics) a function that changes the position or
        direction of the axes of a coordinate system
     3: a rule describing the conversion of one syntactic structure
        into another related syntactic structure
     4: (genetics) modification of a cell or bacterium by the uptake
        and incorporation of exogenous DNA
     5: the act of changing in form or shape or appearance; "a
        photograph is a translation of a scene onto a
        two-dimensional surface" [syn: {translation}]
