---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Even_circuit_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 93aa5556-4d05-46dc-ba5f-9660aec463df
updated: 1484309406
title: Even circuit theorem
tags:
    - History
    - Lower bounds
    - Constant factors
categories:
    - Extremal graph theory
---
In extremal graph theory, the even circuit theorem is a result of Paul Erdős according to which an n-vertex graph that does not have a simple cycle of length 2k can only have O(n1 + 1/k) edges. For instance, 4-cycle-free graphs have O(n3/2) edges, 6-cycle-free graphs have O(n4/3) edges, etc.
