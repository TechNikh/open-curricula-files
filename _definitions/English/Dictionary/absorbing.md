---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absorbing
offline_file: ""
offline_thumbnail: ""
uuid: ba45d272-6b9d-483c-939a-36c4dfb65f33
updated: 1484310321
title: absorbing
categories:
    - Dictionary
---
absorbing
     adj : capable of arousing and holding the attention; "a
           fascinating story" [syn: {engrossing}, {fascinating}, {gripping},
            {riveting}]
