---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capillary
offline_file: ""
offline_thumbnail: ""
uuid: c7a3d27f-075e-4445-9f0d-1fbd96657009
updated: 1484310315
title: capillary
categories:
    - Dictionary
---
capillary
     adj 1: of or relating to hair
     2: long and slender with a very small internal diameter; "a
        capillary tube" [syn: {hairlike}]
     n 1: a tube of small internal diameter; holds liquid by capillary
          action [syn: {capillary tube}, {capillary tubing}]
     2: any of the minute blood vessels connecting arterioles with
        venules [syn: {capillary vessel}]
