---
version: 1
type: article
id: https://en.wikipedia.org/wiki/New_old_stock
offline_file: ""
offline_thumbnail: ""
uuid: a2293099-8239-44c1-8d33-6ab956fa9020
updated: 1484309011
title: New old stock
categories:
    - Auctions
---
New old stock (NOS), also backronymed as "new on the shelf" or "new off the shelf",[citation needed] refers to obsolete equipment, or original parts (components) for obsolete equipment, that have never been sold at retail.[1]
