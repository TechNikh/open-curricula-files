---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 6b511e87-7245-4430-9bef-6b5b24293ab7
updated: 1484309216
title: Discrete differential geometry
categories:
    - Differential geometry
---
Discrete differential geometry is the study of discrete counterparts of notions in differential geometry. Instead of smooth curves and surfaces, there are polygons, meshes, and simplicial complexes. It is used in the study of computer graphics and topological combinatorics.
