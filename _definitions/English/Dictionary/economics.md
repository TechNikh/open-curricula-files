---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/economics
offline_file: ""
offline_thumbnail: ""
uuid: 10d1d3c5-a588-4338-84a4-f7a26ca76cb4
updated: 1484310236
title: economics
categories:
    - Dictionary
---
economics
     n : the branch of social science that deals with the production
         and distribution and consumption of goods and services
         and their management [syn: {economic science}, {political
         economy}]
