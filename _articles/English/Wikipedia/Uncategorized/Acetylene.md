---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ethyne
offline_file: ""
offline_thumbnail: ""
uuid: fe950815-0812-4f62-af7b-5c8a99d92db7
updated: 1484308391
title: Acetylene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Laskarbit.jpg
tags:
    - discovery
    - Preparation
    - Bonding
    - Physical properties
    - Changes of state
    - Other
    - Reactions
    - Metal acetylides
    - Reppe chemistry
    - Applications
    - Welding
    - Portable lighting
    - Plastics and acrylic acid derivatives
    - Niche applications
    - Natural occurrence
    - Safety and handling
categories:
    - Uncategorized
---
Acetylene (systematic name: ethyne) is the chemical compound with the formula C2H2. It is a hydrocarbon and the simplest alkyne.[5] This colorless gas is widely used as a fuel and a chemical building block. It is unstable in its pure form and thus is usually handled as a solution.[6] Pure acetylene is odorless, but commercial grades usually have a marked odor due to impurities.[7]
