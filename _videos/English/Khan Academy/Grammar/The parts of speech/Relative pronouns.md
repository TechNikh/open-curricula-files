---
version: 1
type: video
provider: YouTube
id: ZHzKQkX3IxI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Relative%20pronouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1be3d660-0f29-49dd-9be5-2e58b2e9ce52
updated: 1486069599
title: Relative pronouns
tags:
    - grammar
    - relative pronouns
    - the man who sold the world
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZHzKQkX3IxI/default.jpg
    - https://i3.ytimg.com/vi/ZHzKQkX3IxI/1.jpg
    - https://i3.ytimg.com/vi/ZHzKQkX3IxI/2.jpg
    - https://i3.ytimg.com/vi/ZHzKQkX3IxI/3.jpg
---
We use the relative pronouns to connect clauses together, like "the man *who sold the world* is coming over for dinner." David, KA's Grammar Fellow, explains.

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/that-versus-which-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/reflexive-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
