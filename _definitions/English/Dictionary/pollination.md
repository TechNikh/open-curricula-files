---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pollination
offline_file: ""
offline_thumbnail: ""
uuid: 0e245405-28cc-4dd2-aefd-65c9c854fc4d
updated: 1484310299
title: pollination
categories:
    - Dictionary
---
pollination
     n : transfer of pollen from the anther to the stigma of a plant
         [syn: {pollenation}]
