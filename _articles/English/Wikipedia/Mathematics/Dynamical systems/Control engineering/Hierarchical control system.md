---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hierarchical_control_system
offline_file: ""
offline_thumbnail: ""
uuid: 92ada138-2f16-40d0-8070-2be04ce47707
updated: 1484308735
title: Hierarchical control system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Functional_levels_of_a_Distributed_Control_System.svg_0.png
tags:
    - Overview
    - Applications
    - Manufacturing, robotics and vehicles
    - Artificial intelligence
categories:
    - Control engineering
---
A hierarchical control system is a form of control system in which a set of devices and governing software is arranged in a hierarchical tree. When the links in the tree are implemented by a computer network, then that hierarchical control system is also a form of networked control system.
