---
version: 1
type: video
provider: YouTube
id: uzkc-qNVoOk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Independent%20and%20dependent%20events/Probability%20explained.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9cd63346-ca85-429b-9b0e-2d2885cd9da6
updated: 1486069688
title: Probability explained
categories:
    - Independent and dependent events
thumbnail_urls:
    - https://i3.ytimg.com/vi/uzkc-qNVoOk/default.jpg
    - https://i3.ytimg.com/vi/uzkc-qNVoOk/1.jpg
    - https://i3.ytimg.com/vi/uzkc-qNVoOk/2.jpg
    - https://i3.ytimg.com/vi/uzkc-qNVoOk/3.jpg
---
