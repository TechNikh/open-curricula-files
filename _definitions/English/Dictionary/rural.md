---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rural
offline_file: ""
offline_thumbnail: ""
uuid: de4800da-d178-467c-a0cb-ee27a8ff3669
updated: 1484310439
title: rural
categories:
    - Dictionary
---
rural
     adj 1: living in or characteristic of farming or country life;
            "rural people"; "large rural households"; "unpaved
            rural roads"; "an economy that is basically rural"
            [ant: {urban}]
     2: relating to rural areas; "rural electrification"; "rural
        free delivery (RFD)"
