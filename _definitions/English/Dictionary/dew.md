---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dew
offline_file: ""
offline_thumbnail: ""
uuid: f75b8c5c-f270-4853-a45a-51ebbdb9ae7c
updated: 1484310226
title: dew
categories:
    - Dictionary
---
dew
     n : water that has condensed on a cool surface overnight from
         water vapor in the air; "in the morning the grass was wet
         with dew"
