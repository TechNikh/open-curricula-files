---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonmetricity_tensor
offline_file: ""
offline_thumbnail: ""
uuid: b2aafee2-9733-4b22-800e-470e980c5367
updated: 1484309369
title: Nonmetricity tensor
categories:
    - Geometry stubs
---
In mathematics, the nonmetricity tensor in differential geometry is the covariant derivative of the metric tensor.[1] It is therefore a tensor field of order three. It vanishes for the case of Riemannian geometry.
