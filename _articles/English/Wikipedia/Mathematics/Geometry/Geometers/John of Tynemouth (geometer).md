---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_of_Tynemouth_(geometer)
offline_file: ""
offline_thumbnail: ""
uuid: 1c84e6fb-b7cc-4913-925a-0cef4b90312f
updated: 1484309085
title: John of Tynemouth (geometer)
categories:
    - Geometers
---
Little is known of his background, but he authored the Liber de curvis superficiebus Archimenidis, a tract about Archimedes' measurements of spheres. This is an important work in the history of medieval geometry, as it helped transmit Archimedes' ideas to other medieval scholars. The work itself follows closely Archimedes' own reasoning, but with enough differences to lead modern historians to believe the John's work was dependent on a Greek text from late antiquity.[1]
