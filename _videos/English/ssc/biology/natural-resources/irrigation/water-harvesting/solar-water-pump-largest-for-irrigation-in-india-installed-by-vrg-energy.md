---
title: solar water pump, largest for irrigation in india installed by vrg energy
youtube_id: XqGypPjqm8w
tags:
    - Solar
    - Water
    - pump
    - pumping
    - system
    - Agriculture
    - for
    - pv
    - powered
    - centrifugal
    - submersible
    - open
    - well
    - tube
    - bore
    - controller
    - inverter
    - drive
categories:
    - Water harvesting
---
