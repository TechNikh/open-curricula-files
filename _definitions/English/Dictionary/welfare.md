---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/welfare
offline_file: ""
offline_thumbnail: ""
uuid: 10119c1d-fc56-4405-b7e1-389a89113c39
updated: 1484310467
title: welfare
categories:
    - Dictionary
---
welfare
     n 1: governmental provision of economic assistance to persons in
          need [syn: {social welfare}]
     2: something that aids or promotes well-being; "for the common
        good" [syn: {benefit}]
     3: a contented state of being happy and healthy and prosperous;
        "the town was finally on the upbeat after our recent
        troubles" [syn: {wellbeing}, {well-being}, {upbeat}, {eudaemonia},
         {eudaimonia}] [ant: {ill-being}]
