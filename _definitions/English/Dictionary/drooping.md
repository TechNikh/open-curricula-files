---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drooping
offline_file: ""
offline_thumbnail: ""
uuid: 077ecf88-8c12-4b47-abaf-8a64c6616115
updated: 1484310150
title: drooping
categories:
    - Dictionary
---
drooping
     adj 1: weak from exhaustion [syn: {flagging}]
     2: hanging down (as from exhaustion or weakness) [syn: {droopy},
         {sagging}]
     3: having branches or flower heads that bend downward; "nodding
        daffodils"; "the pendulous branches of a weeping willow";
        "lilacs with drooping panicles of fragrant flowers" [syn:
        {cernuous}, {nodding}, {pendulous}]
