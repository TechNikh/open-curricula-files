---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero-sum_game
offline_file: ""
offline_thumbnail: ""
uuid: d9da8a89-168e-400a-a81f-ec0a9e11aa08
updated: 1484309040
title: Zero-sum game
tags:
    - Definition
    - Solution
    - Example
    - Solving
    - Universal Solution
    - Non-zero-sum
    - Economics
    - Psychology
    - Complexity
    - Extensions
    - Misunderstandings
    - Zero-sum mentality
categories:
    - Game theory game classes
---
In game theory and economic theory, a zero-sum game is a mathematical representation of a situation in which each participant's gain (or loss) of utility is exactly balanced by the losses (or gains) of the utility of the other participant(s). If the total gains of the participants are added up and the total losses are subtracted, they will sum to zero. Thus cutting a cake, where taking a larger piece reduces the amount of cake available for others, is a zero-sum game if all participants value each unit of cake equally (see marginal utility).
