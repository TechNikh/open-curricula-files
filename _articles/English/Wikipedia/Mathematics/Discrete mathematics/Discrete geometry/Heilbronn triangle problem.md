---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heilbronn_triangle_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3509bf2e-b4eb-4a13-8a60-52fbd3acaa06
updated: 1484308603
title: Heilbronn triangle problem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Heilbronn_square_n%253D6.svg.png'
tags:
    - Definition
    - "Heilbronn's conjecture and lower bound constructions"
    - Upper bounds
    - Specific shapes and numbers
    - Variations
categories:
    - Discrete geometry
---
In discrete geometry and discrepancy theory, the Heilbronn triangle problem is a problem of placing points within a region in the plane, in order to avoid triangles of small area. It is named after Hans Heilbronn, who conjectured prior to 1950 that this smallest triangle area is necessarily at most inversely proportional to the square of the number of points. Heilbronn's conjecture was proven false, but the asymptotic growth rate of the minimum triangle area remains unknown.
