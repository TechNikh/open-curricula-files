---
version: 1
type: video
provider: YouTube
id: J1HAY8E3gms
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%203-%20Modeling%20with%20polynomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e748189-287d-4b1f-9965-9a3011d72405
updated: 1486069565
title: 'Example 3- Modeling with polynomials'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/J1HAY8E3gms/default.jpg
    - https://i3.ytimg.com/vi/J1HAY8E3gms/1.jpg
    - https://i3.ytimg.com/vi/J1HAY8E3gms/2.jpg
    - https://i3.ytimg.com/vi/J1HAY8E3gms/3.jpg
---
