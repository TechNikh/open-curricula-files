---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flue
offline_file: ""
offline_thumbnail: ""
uuid: 5331847e-0744-4113-9556-10b381180464
updated: 1484310413
title: flue
categories:
    - Dictionary
---
flue
     n 1: flat blade-like projection on the arm of an anchor [syn: {fluke}]
     2: organ pipe whose tone is produced by air passing across the
        sharp edge of a fissure or lip [syn: {flue pipe}, {labial
        pipe}]
     3: a conduit to carry off smoke
