---
version: 1
type: video
provider: YouTube
id: Q_B5GpsbSQw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Drawing%20the%204th%2C%205th%2C%206th%2C%20and%207th%20dimension.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 214806c0-76d2-4138-825b-ffb2ee39225f
updated: 1486069656
title: Drawing the 4th, 5th, 6th, and 7th dimension
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Q_B5GpsbSQw/default.jpg
    - https://i3.ytimg.com/vi/Q_B5GpsbSQw/1.jpg
    - https://i3.ytimg.com/vi/Q_B5GpsbSQw/2.jpg
    - https://i3.ytimg.com/vi/Q_B5GpsbSQw/3.jpg
---
