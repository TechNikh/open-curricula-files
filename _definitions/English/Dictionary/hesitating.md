---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hesitating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484583481
title: hesitating
categories:
    - Dictionary
---
hesitating
     adj : lacking decisiveness of character; unable to act or decide
           quickly or firmly; "stood irresolute waiting for some
           inspiration" [syn: {hesitant}, {irresolute}]
