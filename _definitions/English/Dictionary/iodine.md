---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/iodine
offline_file: ""
offline_thumbnail: ""
uuid: e7b4b7ae-e626-4222-83ad-aeda9195c00d
updated: 1484310333
title: iodine
categories:
    - Dictionary
---
iodine
     n 1: a nonmetallic element belonging to the halogens; used
          especially in medicine and photography and in dyes;
          occurs naturally only in combination in small quantities
          (as in sea water or rocks) [syn: {iodin}, {I}, {atomic
          number 53}]
     2: a tincture consisting of a solution of iodine in ethyl
        alcohol; applied topically to wounds as an antiseptic
        [syn: {tincture of iodine}]
