---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mammary
offline_file: ""
offline_thumbnail: ""
uuid: 9b6088ac-d6a1-462a-b99c-f59ce2a5b2cc
updated: 1484310279
title: mammary
categories:
    - Dictionary
---
mammary
     adj : of or relating to the milk-giving gland of the female
