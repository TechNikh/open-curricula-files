---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/focus
offline_file: ""
offline_thumbnail: ""
uuid: 3f525542-b863-4e8f-acdb-a534874797c8
updated: 1484310221
title: focus
categories:
    - Dictionary
---
focus
     n 1: the concentration of attention or energy on something; "the
          focus of activity shifted to molecular biology"; "he had
          no direction in his life" [syn: {focusing}, {focussing},
           {direction}, {centering}]
     2: maximum clarity or distinctness of an image rendered by an
        optical system; "in focus"; "out of focus"
     3: maximum clarity or distinctness of an idea; "the controversy
        brought clearly into focus an important difference of
        opinion"
     4: a central point or locus of an infection in an organism;
        "the focus of infection" [syn: {focal point}, {nidus}]
     5: special emphasis attached to something; "the ...
