---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Division_by_two
offline_file: ""
offline_thumbnail: ""
uuid: a884397d-8c7e-4b30-9e04-f16da8e362a7
updated: 1484308918
title: Division by two
tags:
    - Binary
    - Binary floating point
    - Decimal
categories:
    - Elementary arithmetic
---
In mathematics, division by two or halving has also been called mediation or dimidiation.[1] The treatment of this as a different operation from multiplication and division by other numbers goes back to the ancient Egyptians, whose multiplication algorithm used division by two as one of its fundamental steps.[2] Some mathematicians as late as the sixteenth century continued to view halving as a separate operation,[3][4] and it often continues to be treated separately in modern computer programming.[5] Performing this operation is simple in decimal arithmetic, in the binary numeral system used in computer programming, and in other even-numbered bases.
