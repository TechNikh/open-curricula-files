---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transcritical_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: 26f4c032-a178-40e5-bed7-1e9ce26ba4b1
updated: 1484308699
title: Transcritical bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Transcritical_bifurcation.gif
categories:
    - Bifurcation theory
---
In bifurcation theory, a field within mathematics, a transcritical bifurcation is a particular kind of local bifurcation, meaning that it is characterized by an equilibrium having an eigenvalue whose real part passes through zero.
