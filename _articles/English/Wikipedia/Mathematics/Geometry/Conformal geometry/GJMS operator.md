---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GJMS_operator
offline_file: ""
offline_thumbnail: ""
uuid: 1b42676b-c3bb-4bf5-8f8d-b322a3ef9287
updated: 1484309187
title: GJMS operator
categories:
    - Conformal geometry
---
In the mathematical field of differential geometry, the GJMS operators are a family of differential operators, that are defined on a Riemannian manifold. In an appropriate sense, they depend only on the conformal structure of the manifold. The GJMS operators generalize the Paneitz operator and the conformal Laplacian. The initials GJMS are for its discoverers Graham, Jenne, Mason & Sparling (1992).
