---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_edge-coloring
offline_file: ""
offline_thumbnail: ""
uuid: 9b2e12f9-aa0d-41ef-83cb-c75ba134cf75
updated: 1484309384
title: List edge-coloring
categories:
    - Graph coloring
---
In mathematics, list edge-coloring is a type of graph coloring that combines list coloring and edge coloring. An instance of a list edge-coloring problem consists of a graph together with a list of allowed colors for each edge. A list edge-coloring is a choice of a color for each edge, from its list of allowed colors; a coloring is proper if no two adjacent edges receive the same color.
