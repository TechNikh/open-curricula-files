---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rooms
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484318701
title: rooms
categories:
    - Dictionary
---
rooms
     n : apartment consisting of a series of connected rooms used as
         a living unit (as in a hotel) [syn: {suite}]
