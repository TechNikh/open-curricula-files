---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_encoding
offline_file: ""
offline_thumbnail: ""
uuid: 5e79e21e-f58d-42fb-a93d-73c57de12bc6
updated: 1484308770
title: Entropy encoding
categories:
    - Entropy and information
---
One of the main types of entropy coding creates and assigns a unique prefix-free code to each unique symbol that occurs in the input. These entropy encoders then compress data by replacing each fixed-length input symbol with the corresponding variable-length prefix-free output codeword. The length of each codeword is approximately proportional to the negative logarithm of the probability. Therefore, the most common symbols use the shortest codes.
