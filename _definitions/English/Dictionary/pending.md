---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pending
offline_file: ""
offline_thumbnail: ""
uuid: 872540bd-8b2b-4d64-86e3-580c2ae6348f
updated: 1484310467
title: pending
categories:
    - Dictionary
---
pending
     adj : awaiting conclusion or confirmation; "business still
           pending"
