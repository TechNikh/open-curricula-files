---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/xy
offline_file: ""
offline_thumbnail: ""
uuid: 60e61fb2-7574-4450-9667-246b9dd5c124
updated: 1484310297
title: xy
categories:
    - Dictionary
---
XY
     n : (genetics) normal complement of sex hormones in a male
