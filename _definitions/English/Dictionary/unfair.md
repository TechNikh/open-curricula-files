---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfair
offline_file: ""
offline_thumbnail: ""
uuid: 01aedfc5-6e74-4bc1-ad9e-91cc0e50fd70
updated: 1484310469
title: unfair
categories:
    - Dictionary
---
unfair
     adj 1: showing favoritism [syn: {partial}] [ant: {impartial}]
     2: not fair; marked by injustice or partiality or deception;
        "used unfair methods"; "it was an unfair trial"; "took an
        unfair advantage" [syn: {unjust}] [ant: {fair}]
