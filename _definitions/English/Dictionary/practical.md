---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/practical
offline_file: ""
offline_thumbnail: ""
uuid: d6641c3c-5dcf-4f34-b190-b28225ba08a2
updated: 1484310214
title: practical
categories:
    - Dictionary
---
practical
     adj 1: concerned with actual use or practice; "he is a very
            practical person"; "the idea had no practical
            application"; "a practical knowledge of Japanese";
            "woodworking is a practical art" [ant: {impractical}]
     2: guided by practical experience and observation rather than
        theory; "a hardheaded appraisal of our position"; "a
        hard-nosed labor leader"; "completely practical in his
        approach to business"; "not ideology but pragmatic
        politics" [syn: {hardheaded}, {hard-nosed}, {pragmatic}]
     3: being actually such in almost every respect; "a practical
        failure"; "the once elegant temple lay in ...
