---
version: 1
type: video
provider: YouTube
id: DlqyyyvTI3k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b57d609f-fdf8-4890-a06d-b93f44387ad7
updated: 1486069681
title: 'Urinary System, part 2- Crash Course A&P #39'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/DlqyyyvTI3k/default.jpg
    - https://i3.ytimg.com/vi/DlqyyyvTI3k/1.jpg
    - https://i3.ytimg.com/vi/DlqyyyvTI3k/2.jpg
    - https://i3.ytimg.com/vi/DlqyyyvTI3k/3.jpg
---
