---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/G%C3%B6del_numbering_for_sequences'
offline_file: ""
offline_thumbnail: ""
uuid: 5bf70af5-ff22-493a-b882-c7a29a8369a5
updated: 1484309487
title: Gödel numbering for sequences
tags:
    - Gödel numbering
    - Accessing members
    - "Gödel's β-function lemma"
    - Using a pairing function
    - Remainder for natural numbers
    - Using the Chinese remainder theorem
    - Implementation of the β function
    - Hand-tuned assumptions
    - >
        Proof that (coprimality) assumption for Chinese remainder
        theorem is met
    - First steps
    - Resorting to the first hand-tuned assumption
    - >
        Using an (object) theorem of the propositional calculus as a
        lemma
    - Reaching the contradiction
    - End of reductio ad absurdum
    - The system of simultaneous congruences
    - Resorting to the second hand-tuned assumption
    - QED
    - Existence and uniqueness
    - Uniqueness of encoding, achieved by minimalization
    - >
        Totality, because minimalization is restricted to special
        functions
    - >
        The Gödel numbering function g can be chosen to be total
        recursive
    - Access of length
    - Notes
categories:
    - Computability theory
---
In mathematics, a Gödel numbering for sequences provides us an effective way to represent each finite sequence of natural numbers as a single natural number. Of course, the embedding is surely possible set theoretically, but the emphasis is on the effectiveness of the functions manipulating such representations of sequences: the operations on sequences (accessing individual members, concatenation) can be "implemented" using total recursive functions, and in fact by primitive recursive functions.
