---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/writer
offline_file: ""
offline_thumbnail: ""
uuid: fae7b881-3003-420b-bfb2-12ceed820c18
updated: 1484310518
title: writer
categories:
    - Dictionary
---
writer
     n 1: writes (books or stories or articles or the like)
          professionally (for pay) [syn: {author}]
     2: a person who is able to write and has written something
