---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexandrov_space
offline_file: ""
offline_thumbnail: ""
uuid: f4859dd1-6232-47c5-bf2a-7455118b320f
updated: 1484309200
title: Alexandrov space
categories:
    - Differential geometry
---
In geometry, Alexandrov spaces with curvature ≥ k form a generalization of Riemannian manifolds with sectional curvature ≥ k, where k is some real number. By definition, these spaces are locally compact complete length spaces where the lower curvature bound is defined via comparison of geodesic triangles in the space to geodesic triangles in standard constant-curvature Riemannian surfaces.[1]
