---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_controllability
offline_file: ""
offline_thumbnail: ""
uuid: c64f419d-8243-4062-b551-9e829112b1e6
updated: 1484309438
title: Network controllability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-YYL1.png
tags:
    - Background
    - Structural Controllability
    - Maximum Matching
    - >
        Control of composite quantum systems and algebraic graph
        theory
categories:
    - Network theory
---
Network Controllability is concerned about the structural controllability of a network. Controllability describes our ability to guide a dynamical system from any initial state to any desired final state in finite time, with a suitable choice of inputs. This definition agrees well with our intuitive notion of control. The controllability of general directed and weighted complex networks has recently been the subject of intense study by a number of groups, worldwide.
