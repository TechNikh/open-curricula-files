---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epitome
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441581
title: epitome
categories:
    - Dictionary
---
epitome
     n 1: a standard or typical example; "he is the prototype of good
          breeding"; "he provided America with an image of the
          good father" [syn: {prototype}, {paradigm}, {image}]
     2: a brief abstract (as of an article or book)
