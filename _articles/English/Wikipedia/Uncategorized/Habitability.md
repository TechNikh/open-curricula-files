---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Living_conditions
offline_file: ""
offline_thumbnail: ""
uuid: 10a7da7f-55d4-4262-86a2-c1a6ff7ad428
updated: 1484308355
title: Habitability
tags:
    - New York law
    - Consequences
categories:
    - Uncategorized
---
Habitability is the conformance of a residence or abode to the implied warranty of habitability. A residence that complies is said to be "habitable". It is an implied warranty or contract, meaning it does not have to be an express contract, covenant, or provision of a contract. It is a common law right of a tenant or legal doctrine.[1]
