---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inevitable
offline_file: ""
offline_thumbnail: ""
uuid: 179f56d5-c4a4-477d-be77-f5b633475f78
updated: 1484310464
title: inevitable
categories:
    - Dictionary
---
inevitable
     adj 1: incapable of being avoided or prevented; "the inevitable
            result" [ant: {evitable}]
     2: invariably occurring or appearing; "the inevitable changes
        of the seasons"
     n : an unavoidable event; "don't argue with the inevitable"
