---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radiant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484575441
title: radiant
categories:
    - Dictionary
---
radiant
     adj : radiating or as if radiating light; "the beaming sun"; "the
           effulgent daffodils"; "a radiant sunrise"; "a refulgent
           sunset" [syn: {beaming}, {beamy}, {effulgent}, {refulgent}]
