---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/user
offline_file: ""
offline_thumbnail: ""
uuid: ff82083a-eb31-41df-b181-fec5c4e66cd1
updated: 1484310200
title: user
categories:
    - Dictionary
---
user
     n 1: a person who makes use of a thing; someone who uses or
          employs something
     2: a person who uses something or someone selfishly or
        unethically [syn: {exploiter}]
     3: a person who takes drugs [syn: {drug user}, {substance
        abuser}]
