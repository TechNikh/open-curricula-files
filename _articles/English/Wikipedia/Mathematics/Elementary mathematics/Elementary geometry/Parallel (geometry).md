---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 28763b27-35de-4312-aa0e-25c99eba4cb6
updated: 1484308935
title: Parallel (geometry)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parallel_%2528PSF%2529.png'
tags:
    - Symbol
    - Euclidean parallelism
    - Two lines in a plane
    - Conditions for parallelism
    - History
    - Construction
    - Distance between two parallel lines
    - Two lines in three-dimensional space
    - A line and a plane
    - Two planes
    - Extension to non-Euclidean geometry
    - Hyperbolic geometry
    - Spherical or elliptic geometry
    - Reflexive variant
    - Notes
categories:
    - Elementary geometry
---
In geometry, parallel lines are lines in a plane which do not meet; that is, two lines in a plane that do not intersect or touch each other at any point are said to be parallel. By extension, a line and a plane, or two planes, in three-dimensional Euclidean space that do not share a point are said to be parallel. However, two lines in three-dimensional space which do not meet must be in a common plane to be considered parallel; otherwise they are called skew lines. Parallel planes are planes in the same three-dimensional space that never meet.
