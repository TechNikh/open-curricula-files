---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anti-clockwise
offline_file: ""
offline_thumbnail: ""
uuid: 07852a80-52a0-4b74-b48a-ba973c0fec42
updated: 1484308306
title: Clockwise
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clockwise_arrow.svg.png
tags:
    - Terminology
    - Usage
    - Shop-work
    - Mathematics
    - Games and activities
    - In humans
categories:
    - Uncategorized
---
Rotation can occur in two possible directions. A clockwise (typically abbreviated as CW) motion is one that proceeds in the same direction as a clock's hands: from the top to the right, then down and then to the left, and back up to the top. The opposite sense of rotation or revolution is (in Commonwealth English) anticlockwise (ACW), or (in North American English) counterclockwise (CCW). In a mathematical sense, a circle defined parametrically in a positive Cartesian plane by the equations x = cos t and y = sin t is traced anticlockwise as t increases in value.
