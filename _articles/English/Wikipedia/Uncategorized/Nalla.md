---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nalla
offline_file: ""
offline_thumbnail: ""
uuid: 618e8840-1b1a-490d-a8e4-9d216dfb6cc7
updated: 1484308333
title: Nalla
tags:
    - Cast
    - Soundtrack
categories:
    - Uncategorized
---
Nalla is a 2004 Kannada romance-drama film directed by V. Nagendra Prasad featuring Sudeep and Sangeetha in the lead roles. The film features background score and soundtrack composed by Venkat Narayan and lyrics by V. Nagendra Prasad. The film released on 12 November 2004. This movie was dubbed in hindi as Aur Ek Diljala[2]
