---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Maximum_entropy_spectral_estimation
offline_file: ""
offline_thumbnail: ""
uuid: a5bdc5b8-4c95-4210-8fd9-a7600a52d4f8
updated: 1484308767
title: Maximum entropy spectral estimation
tags:
    - Method description
    - Spectral estimation
categories:
    - Entropy
---
Maximum entropy spectral estimation is a method of spectral density estimation. The goal is to improve the spectral quality based on the principle of maximum entropy. The method is based on choosing the spectrum which corresponds to the most random or the most unpredictable time series whose autocorrelation function agrees with the known values. This assumption, which corresponds to the concept of maximum entropy as used in both statistical mechanics and information theory, is maximally non-committal with regard to the unknown values of the autocorrelation function of the time series. It is simply the application of maximum entropy modeling to any type of spectrum and is used in all fields ...
