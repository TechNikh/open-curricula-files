---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/between
offline_file: ""
offline_thumbnail: ""
uuid: 200e0174-ea5c-42eb-a293-438316c01f90
updated: 1484310353
title: between
categories:
    - Dictionary
---
between
     adv 1: in the interval; "dancing all the dances with little rest
            between" [syn: {betwixt}]
     2: in between; "two houses with a tree between" [syn: {'tween}]
