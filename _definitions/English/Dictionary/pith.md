---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pith
offline_file: ""
offline_thumbnail: ""
uuid: 7296d7e1-637f-47d2-8151-6ffa67f0a7c8
updated: 1484310158
title: pith
categories:
    - Dictionary
---
pith
     n 1: soft spongelike central cylinder of the stems of most
          flowering plants
     2: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {substance}, {core},
         {center}, {essence}, {gist}, {heart}, {heart and soul}, {inwardness},
         {marrow}, {meat}, {nub}, {sum}, {nitty-gritty}]
