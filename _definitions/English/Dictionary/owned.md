---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/owned
offline_file: ""
offline_thumbnail: ""
uuid: fdf70221-0bcd-4401-8a9c-853f18a93d79
updated: 1484310262
title: owned
categories:
    - Dictionary
---
owned
     adj : having an owner; often used in combination; "state-owned
           railways" [ant: {unowned}]
