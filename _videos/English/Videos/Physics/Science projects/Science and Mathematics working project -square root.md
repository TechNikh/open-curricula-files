---
version: 1
type: video
provider: YouTube
id: U__ha6dChcI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca2e78b0-f61b-4abc-8865-a336fd94a06a
updated: 1486069635
title: Science and Mathematics working project -square root
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/U__ha6dChcI/default.jpg
    - https://i3.ytimg.com/vi/U__ha6dChcI/1.jpg
    - https://i3.ytimg.com/vi/U__ha6dChcI/2.jpg
    - https://i3.ytimg.com/vi/U__ha6dChcI/3.jpg
---
