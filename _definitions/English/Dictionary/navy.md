---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/navy
offline_file: ""
offline_thumbnail: ""
uuid: 5e5e7f19-b8b6-4398-b7f0-0eaf0b78f07d
updated: 1484310587
title: navy
categories:
    - Dictionary
---
navy
     n 1: an organization of military naval forces [syn: {naval forces}]
     2: a dark shade of blue [syn: {dark blue}, {navy blue}]
