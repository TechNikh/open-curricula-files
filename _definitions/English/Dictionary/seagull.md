---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seagull
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484624221
title: seagull
categories:
    - Dictionary
---
sea gull
     n : mostly white aquatic bird having long pointed wings and
         short legs [syn: {gull}, {seagull}]
