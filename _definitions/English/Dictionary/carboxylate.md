---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carboxylate
offline_file: ""
offline_thumbnail: ""
uuid: 6fe63594-065f-4de4-8ebf-7cf79b2b72d2
updated: 1484310423
title: carboxylate
categories:
    - Dictionary
---
carboxylate
     v : treat (a chemical compound) with carboxyl or carboxylic acid
