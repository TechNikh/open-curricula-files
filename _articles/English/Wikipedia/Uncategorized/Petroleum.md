---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Petrolium
offline_file: ""
offline_thumbnail: ""
uuid: 61bfcefd-5145-40df-a608-439e51ca1325
updated: 1484308331
title: Petroleum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Oil_Reserves.png
tags:
    - Etymology
    - History
    - Early history
    - Modern history
    - Composition
    - chemistry
    - Empirical equations for thermal properties
    - Heat of combustion
    - Thermal conductivity
    - Specific heat
    - Latent heat of vaporization
    - Formation
    - Reservoirs
    - Crude oil reservoirs
    - Unconventional oil reservoirs
    - Classification
    - Petroleum industry
    - Shipping
    - price
    - Uses
    - Fuels
    - Other derivatives
    - Agriculture
    - Petroleum by country
    - Consumption statistics
    - Consumption
    - Production
    - Export
    - Import
    - Oil Imports to the USA by country 2010
    - Non-producing consumers
    - Environmental effects
    - Ocean acidification
    - Global warming
    - Extraction
    - Oil spills
    - Tarballs
    - Whales
    - Alternatives to petroleum
    - Alternatives to petroleum-based vehicle fuels
    - Alternatives to using oil in industry
    - Alternatives to burning petroleum for electricity
    - Future of petroleum production
    - Peak oil
    - Unconventional production
    - Notes
categories:
    - Uncategorized
---
Petroleum (from Latin: petra: "rock" + oleum: "oil".[1][2][3]) is a naturally occurring, yellow-to-black liquid found in geological formations beneath the Earth's surface, which is commonly refined into various types of fuels. Components of petroleum are separated using a technique called fractional distillation.
