---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fertility
offline_file: ""
offline_thumbnail: ""
uuid: dedc21e6-aa7f-4c6c-8093-1d4ff19e5171
updated: 1484310473
title: fertility
categories:
    - Dictionary
---
fertility
     n 1: the ratio of live births in an area to the population of
          that area; expressed per 1000 population per year [syn:
          {birthrate}, {birth rate}, {fertility rate}, {natality}]
     2: the state of being fertile; capable of producing offspring
        [syn: {fecundity}] [ant: {sterility}]
     3: the property of producing abundantly and sustaining growth;
        "he praised the richness of the soil" [syn: {richness}, {prolificacy}]
