---
version: 1
type: video
provider: YouTube
id: cwSB_GhCrF0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2029%20-%20Radiation%20Waves.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e066f264-3503-402d-9655-a4e936d7dcca
updated: 1486069664
title: 'Eureka 29 - Radiation Waves.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/cwSB_GhCrF0/default.jpg
    - https://i3.ytimg.com/vi/cwSB_GhCrF0/1.jpg
    - https://i3.ytimg.com/vi/cwSB_GhCrF0/2.jpg
    - https://i3.ytimg.com/vi/cwSB_GhCrF0/3.jpg
---
