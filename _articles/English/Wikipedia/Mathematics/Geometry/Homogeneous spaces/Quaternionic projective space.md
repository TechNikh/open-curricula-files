---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quaternionic_projective_space
offline_file: ""
offline_thumbnail: ""
uuid: ae785502-ea49-4a25-a989-89ccbe83a447
updated: 1484309249
title: Quaternionic projective space
tags:
    - In coordinates
    - Projective line
    - Infinite-dimensional quaternionic projective space
    - Quaternionic projective plane
categories:
    - Homogeneous spaces
---
In mathematics, quaternionic projective space is an extension of the ideas of real projective space and complex projective space, to the case where coordinates lie in the ring of quaternions H. Quaternionic projective space of dimension n is usually denoted by
