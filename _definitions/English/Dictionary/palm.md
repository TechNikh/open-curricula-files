---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/palm
offline_file: ""
offline_thumbnail: ""
uuid: 81161941-62b7-4ed8-a933-876fd9a58c49
updated: 1484310228
title: palm
categories:
    - Dictionary
---
palm
     n 1: the inner surface of the hand from the wrist to the base of
          the fingers [syn: {thenar}]
     2: a linear unit based on the length or width of the human hand
     3: any plant of the family Palmae having an unbranched trunk
        crowned by large pinnate or palmate leaves [syn: {palm
        tree}]
     4: an award for winning a championship or commemorating some
        other event [syn: {decoration}, {laurel wreath}, {medal},
        {medallion}, {ribbon}]
     v : touch, lift, or hold with the hands; "Don't handle the
         merchandise" [syn: {handle}]
