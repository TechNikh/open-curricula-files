---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proteinaceous
offline_file: ""
offline_thumbnail: ""
uuid: cd7abccc-1471-47c1-945f-f0e4a95c34d3
updated: 1484308337
title: Protein
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Myoglobin.png
tags:
    - Biochemistry
    - Abundance in cells
    - Synthesis
    - Biosynthesis
    - Chemical synthesis
    - Structure
    - Structure determination
    - Cellular functions
    - Enzymes
    - Cell signaling and ligand binding
    - Structural proteins
    - Methods of study
    - Protein purification
    - Cellular localization
    - Proteomics
    - Bioinformatics
    - Structure prediction and simulation
    - Protein disorder and unstructure prediction
    - Nutrition
    - History and etymology
    - Textbooks
    - Databases and projects
    - Tutorials and educational websites
categories:
    - Uncategorized
---
Proteins (/ˈproʊˌtiːnz/ or /ˈproʊti.ᵻnz/) are large biomolecules, or macromolecules, consisting of one or more long chains of amino acid residues. Proteins perform a vast array of functions within organisms, including catalysing metabolic reactions, DNA replication, responding to stimuli, and transporting molecules from one location to another. Proteins differ from one another primarily in their sequence of amino acids, which is dictated by the nucleotide sequence of their genes, and which usually results in protein folding into a specific three-dimensional structure that determines its activity.
