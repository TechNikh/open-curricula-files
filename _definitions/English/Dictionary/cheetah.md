---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheetah
offline_file: ""
offline_thumbnail: ""
uuid: b7ea62dc-0962-4931-87ba-144dae78c474
updated: 1484310286
title: cheetah
categories:
    - Dictionary
---
cheetah
     n : long-legged spotted cat of Africa and southwestern Asia
         having nonretractile claws; the swiftest mammal; can be
         trained to run down game [syn: {chetah}, {Acinonyx
         jubatus}]
