---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headache
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456341
title: headache
categories:
    - Dictionary
---
head ache
     n : pain in the head caused by dilation of cerebral arteries or
         muscle contractions or a reaction to drugs [syn: {headache},
          {cephalalgia}]
