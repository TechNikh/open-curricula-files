---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pigeonhole_principle
offline_file: ""
offline_thumbnail: ""
uuid: 8ddaf749-bdec-48d8-8807-b013f11ffe99
updated: 1484308588
title: Pigeonhole principle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TooManyPigeons.jpg
tags:
    - Etymology
    - Examples
    - Sock-picking
    - Hand-shaking
    - Hair-counting
    - The birthday problem
    - Softball team
    - Subset sum
    - Uses and applications
    - Alternate formulations
    - Strong form
    - Generalizations of the pigeonhole principle
    - Infinite sets
    - Quantum mechanics
    - Notes
categories:
    - Combinatorics
---
In mathematics, the pigeonhole principle states that if n items are put into m containers, with n > m, then at least one container must contain more than one item.[1] This theorem is exemplified in real life by truisms like "there must be at least two left gloves or two right gloves in a group of three gloves". It is an example of a counting argument, and despite seeming intuitive it can be used to demonstrate possibly unexpected results; for example, that two people in London have the same number of hairs on their heads.
