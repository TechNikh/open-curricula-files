---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cerebrum
offline_file: ""
offline_thumbnail: ""
uuid: a895357d-ad48-4467-acde-33de7b73dd58
updated: 1484310315
title: cerebrum
categories:
    - Dictionary
---
cerebrum
     n : anterior portion of the brain consisting of two hemispheres;
         dominant part of the brain in humans
     [also: {cerebra} (pl)]
