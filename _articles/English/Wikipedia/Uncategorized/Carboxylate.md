---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carboxylate
offline_file: ""
offline_thumbnail: ""
uuid: a49180a7-4762-40bf-a22b-0fffbc931d19
updated: 1484308403
title: Carboxylate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/255px-Carboxylate-resonance-hybrid.png
categories:
    - Uncategorized
---
A carboxylate is a salt or ester of a carboxylic acid. Carboxylate salts have the general formula M(RCOO)n, where M is a metal and n is 1, 2,...; carboxylate esters have the general formula RCOOR′. R and R′ are organic groups; R′ ≠ H.
