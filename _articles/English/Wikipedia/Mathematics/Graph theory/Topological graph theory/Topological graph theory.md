---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: 36be6635-6d80-4a45-b8ec-07f879bc2fe6
updated: 1484309479
title: Topological graph theory
tags:
    - Graphs as topological spaces
    - Example studies
    - Notes
categories:
    - Topological graph theory
---
In mathematics, topological graph theory is a branch of graph theory. It studies the embedding of graphs in surfaces, spatial embeddings of graphs, and graphs as topological spaces.[1] It also studies immersions of graphs.
