---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Free_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 3d1bb39f-7cf3-4ce9-a496-910d9a484b29
updated: 1484308837
title: Free expansion
categories:
    - Thermodynamics
---
Real gases experience a temperature change during free expansion. For an ideal gas, the temperature doesn't change, and the conditions before and after adiabatic free expansion satisfy
