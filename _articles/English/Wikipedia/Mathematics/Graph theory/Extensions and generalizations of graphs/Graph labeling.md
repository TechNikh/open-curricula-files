---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_labeling
offline_file: ""
offline_thumbnail: ""
uuid: cac028f3-d587-4911-a6c1-84c45550a0af
updated: 1484309406
title: Graph labeling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Graceful_labeling.svg.png
tags:
    - History
    - Special cases
    - Graceful labeling
    - Edge-graceful labeling
    - Harmonious labeling
    - Graph coloring
    - Lucky labeling
categories:
    - Extensions and generalizations of graphs
---
In the mathematical discipline of graph theory, a graph labeling is the assignment of labels, traditionally represented by integers, to the edges or vertices, or both, of a graph.[1]
