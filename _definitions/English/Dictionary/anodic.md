---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anodic
offline_file: ""
offline_thumbnail: ""
uuid: efc0da62-b206-4102-9402-75e03aaff553
updated: 1484310411
title: anodic
categories:
    - Dictionary
---
anodic
     adj : of or at or relating to an anode [syn: {anodal}] [ant: {cathodic}]
