---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provisional
offline_file: ""
offline_thumbnail: ""
uuid: cefacfde-a6ed-4fcb-9249-9de5e1ca26c2
updated: 1484310557
title: provisional
categories:
    - Dictionary
---
provisional
     adj : under terms not final or fully worked out or agreed upon;
           "probationary employees"; "a provisional government";
           "just a tentative schedule" [syn: {probationary}, {provisionary},
            {tentative}]
