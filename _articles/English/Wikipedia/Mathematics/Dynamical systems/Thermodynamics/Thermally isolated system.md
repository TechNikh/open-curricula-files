---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermally_isolated_system
offline_file: ""
offline_thumbnail: ""
uuid: db5bbc9c-aab5-4f0b-b542-80e431e2bdd6
updated: 1484308870
title: Thermally isolated system
categories:
    - Thermodynamics
---
In thermodynamics, a thermally isolated system can exchange no mass or heat energy with its environment, but may exchange work energy with its environment. The internal energy of a thermally isolated system may therefore change due to the exchange of work energy. The entropy of a thermally isolated system will increase in time if it is not at equilibrium, but as long as it is at equilibrium, its entropy will be at a maximum and constant value and will not change, no matter how much work energy the system exchanges with its environment. To maintain this constant entropy, any exchange of work energy with the environment must therefore be quasistatic in nature, in order to assure that the ...
