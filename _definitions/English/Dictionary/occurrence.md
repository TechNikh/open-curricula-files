---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occurrence
offline_file: ""
offline_thumbnail: ""
uuid: abda3f13-2244-4bd1-b856-3562995b792d
updated: 1484310246
title: occurrence
categories:
    - Dictionary
---
occurrence
     n 1: an event that happens [syn: {happening}, {natural event}]
     2: an instance of something occurring; "a disease of frequent
        occurrence"; "the occurrence (or presence) of life on
        other planets"
