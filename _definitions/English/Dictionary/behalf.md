---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/behalf
offline_file: ""
offline_thumbnail: ""
uuid: 5a6ed536-1273-41b6-98da-6cd05ea5c4fc
updated: 1484310551
title: behalf
categories:
    - Dictionary
---
behalf
     n 1: as the agent of or on someone's part (usually expressed as
          "on behalf of" rather than "in behalf of"); "the
          guardian signed the contract on behalf of the minor
          child"; "this letter is written on behalf of my client";
     2: for someone's benefit (usually expressed as `in behalf'
        rather than `on behalf' and usually with a possessive);
        "in your behalf"; "campaigning in his own behalf"; "spoke
        a good word in his friend's behalf"
