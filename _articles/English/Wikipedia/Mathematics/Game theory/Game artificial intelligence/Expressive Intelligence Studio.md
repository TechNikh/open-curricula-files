---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Expressive_Intelligence_Studio
offline_file: ""
offline_thumbnail: ""
uuid: 36ab9adf-c041-4ea1-af61-9365342541a6
updated: 1484309002
title: Expressive Intelligence Studio
tags:
    - History
    - Research
categories:
    - Game artificial intelligence
---
The Expressive Intelligence Studio is a research group at the University of California, Santa Cruz, established to conduct research in the field of game design technology.[1] The studio is currently being run by Michael Mateas, Noah Wardrip-Fruin, Arnav Jhala, and Jim Whitehead, who work closely with the students in their research.[2]
