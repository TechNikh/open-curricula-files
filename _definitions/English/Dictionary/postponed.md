---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/postponed
offline_file: ""
offline_thumbnail: ""
uuid: 010184f7-a8d0-4e65-8bb8-29459ed9849e
updated: 1484310168
title: postponed
categories:
    - Dictionary
---
postponed
     adj : put off to later; "requested a deferred payment"; "our
           postponed trip" [syn: {deferred}]
