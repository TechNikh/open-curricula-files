---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submerge
offline_file: ""
offline_thumbnail: ""
uuid: bbe1a4d2-31a9-417c-977a-daa39679ed45
updated: 1484310441
title: submerge
categories:
    - Dictionary
---
submerge
     v 1: sink below the surface; go under or as if under water [syn:
          {submerse}]
     2: cover completely or make imperceptible; "I was drowned in
        work"; "The noise drowned out her speech" [syn: {drown}, {overwhelm}]
     3: put under water; "submerge your head completely" [syn: {submerse}]
     4: fill or cover completely, usually with water [syn: {inundate},
         {deluge}]
