---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saturation_vapor_curve
offline_file: ""
offline_thumbnail: ""
uuid: 1d1c5405-4a28-4f2e-82de-5a00ad368e46
updated: 1484308860
title: Saturation vapor curve
categories:
    - Thermodynamics
---
The saturation vapor curve is the curve separating the two-phase state and the superheated vapor state in the T-s diagram. The saturated liquid curve is the curve separating the subcooled liquid state and the two-phase state in the T-s diagram.[1]
