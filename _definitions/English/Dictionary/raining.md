---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raining
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484384761
title: raining
categories:
    - Dictionary
---
raining
     adj : falling in drops or as if falling like rain; "watched the
           raining apple blossoms"
