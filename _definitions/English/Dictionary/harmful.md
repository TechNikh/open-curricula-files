---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harmful
offline_file: ""
offline_thumbnail: ""
uuid: 82d2ab09-f101-49c6-8af1-b8a7952b84e7
updated: 1484310363
title: harmful
categories:
    - Dictionary
---
harmful
     adj 1: injurious to physical or mental health; "noxious chemical
            wastes"; "noxious ideas" [syn: {noxious}] [ant: {innocuous}]
     2: able or likely to do harm
     3: causing or capable of causing harm; "too much sun is harmful
        to the skin"; "harmful effects of smoking" [ant: {harmless}]
     4: constituting a disadvantage [syn: {disadvantageous}] [ant: {advantageous}]
     5: contrary to your interests or welfare; "adverse
        circumstances"; "made a place for themselves under the
        most untoward conditions" [syn: {adverse}, {inauspicious},
         {untoward}]
     6: tending to cause great harm [syn: {evil}, {injurious}]
