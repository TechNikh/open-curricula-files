---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soul
offline_file: ""
offline_thumbnail: ""
uuid: 44d774c7-5a39-4d64-bdf4-523c5f7b5eec
updated: 1484310448
title: soul
categories:
    - Dictionary
---
soul
     n 1: the immaterial part of a person; the actuating cause of an
          individual life [syn: {psyche}]
     2: a human being; "there was too much for one person to do"
        [syn: {person}, {individual}, {someone}, {somebody}, {mortal},
         {human}]
     3: deep feeling or emotion [syn: {soulfulness}]
     4: the human embodiment of something; "the soul of honor"
     5: a secular form of gospel that was a major Black musical
        genre in the 1960s and 1970s; "soul was politically
        significant during the Civil Rights movement"
