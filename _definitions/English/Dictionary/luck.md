---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/luck
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484382601
title: luck
categories:
    - Dictionary
---
luck
     n 1: your overall circumstances or condition in life (including
          everything that happens to you); "whatever my fortune
          may be"; "deserved a better fate"; "has a happy lot";
          "the luck of the Irish"; "a victim of circumstances";
          "success that was her portion" [syn: {fortune}, {destiny},
           {fate}, {lot}, {circumstances}, {portion}]
     2: an unknown and unpredictable phenomenon that causes an event
        to result one way rather than another; "bad luck caused
        his downfall"; "we ran into each other by pure chance"
        [syn: {fortune}, {chance}, {hazard}]
     3: an unknown and unpredictable phenomenon that leads to a
      ...
