---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bread
offline_file: ""
offline_thumbnail: ""
uuid: d3148b62-ebd3-40b6-a191-752a9d83ca4c
updated: 1484310311
title: bread
categories:
    - Dictionary
---
bread
     n 1: food made from dough of flour or meal and usually raised
          with yeast or baking powder and then baked [syn: {breadstuff},
           {staff of life}]
     2: informal terms for money [syn: {boodle}, {cabbage}, {clams},
         {dinero}, {dough}, {gelt}, {kale}, {lettuce}, {lolly}, {lucre},
         {loot}, {moolah}, {pelf}, {scratch}, {shekels}, {simoleons},
         {sugar}, {wampum}]
     v : cover with bread crumbs; "bread the pork chops before frying
         them"
