---
version: 1
type: article
id: https://en.wikipedia.org/wiki/1_(number)
offline_file: ""
offline_thumbnail: ""
uuid: fd8d4df0-caaa-44ea-9122-d183f2e096fc
updated: 1484308942
title: 1 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clock_24_J.jpg
tags:
    - Etymology
    - As a number
    - As a digit
    - Mathematics
    - Table of basic calculations
    - In technology
    - In science
    - In astronomy
    - In philosophy
    - In literature
    - In comics
    - In sports
    - In other fields
categories:
    - Integers
---
1 (one, also called unit, unity, and (multiplicative) identity), is a number, a numeral, and the name of the glyph representing that number. It represents a single entity, the unit of counting or measurement. For example, a line segment of unit length is a line segment of length 1.
