---
version: 1
type: video
provider: YouTube
id: vbZZR2D3bUQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9612bb25-553a-4f7b-9c9a-bc4cd3365339
updated: 1486069632
title: 'Solar Water Purification - Science Investigatory Project'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/vbZZR2D3bUQ/default.jpg
    - https://i3.ytimg.com/vi/vbZZR2D3bUQ/1.jpg
    - https://i3.ytimg.com/vi/vbZZR2D3bUQ/2.jpg
    - https://i3.ytimg.com/vi/vbZZR2D3bUQ/3.jpg
---
