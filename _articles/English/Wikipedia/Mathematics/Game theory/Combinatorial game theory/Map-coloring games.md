---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Map-coloring_games
offline_file: ""
offline_thumbnail: ""
uuid: 5563a635-3729-45a8-97a2-ce209c263c74
updated: 1484308538
title: Map-coloring games
tags:
    - Move constraints
    - Winning conditions
    - Monochrome and variants
    - Col and Snort
    - Other games
categories:
    - Combinatorial game theory
---
Several map-coloring games are studied in combinatorial game theory. The general idea is that we are given a map with regions drawn in but with not all the regions colored. Two players, Left and Right, take turns coloring in one uncolored region per turn, subject to various constraints. The move constraints and the winning condition are features of the particular game.
