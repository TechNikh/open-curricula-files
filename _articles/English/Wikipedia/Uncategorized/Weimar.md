---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weimar
offline_file: ""
offline_thumbnail: ""
uuid: 77a3c5d9-2153-48e0-90f9-8e17652ca5a5
updated: 1484094290
title: Weimar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kasseturm_in_Weimar.jpg
tags:
    - History
    - Prehistoric times
    - Middle Ages
    - Early Modern Period
    - Golden or Classical Age (1758–1832)
    - Silver Ages and The New Weimar (1832–1918)
    - Weimar Republic
    - Nazi Germany and World War II
    - Since 1945
    - Geography and demographics
categories:
    - Uncategorized
---
Weimar (German pronunciation: [ˈvaɪmaɐ]; Latin: Vimaria) is a city in the federal state of Thuringia, Germany. It is located between Erfurt in the west and Jena in the east, approximately 80 kilometres (50 miles) southwest of Leipzig, 170 kilometres (106 miles) north of Nuremberg and 170 kilometres (106 miles) west of Dresden. Together with the neighbour-cities Erfurt and Jena it forms the central metropolitan area of Thuringia with approximately 500,000 inhabitants, whereas the city itself counts a population of 65,000. Weimar is well known because of its large cultural heritage and its importance in German history.
