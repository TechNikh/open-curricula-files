---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agony
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580421
title: agony
categories:
    - Dictionary
---
agony
     n 1: intense feelings of suffering; acute mental or physical
          pain; "an agony of doubt"; "the torments of the damned"
          [syn: {torment}, {torture}]
     2: a state of acute pain [syn: {suffering}, {excruciation}]
