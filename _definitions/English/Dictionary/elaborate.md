---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elaborate
offline_file: ""
offline_thumbnail: ""
uuid: df7378dd-a2ee-4c60-a43b-1356a10fa253
updated: 1484310601
title: elaborate
categories:
    - Dictionary
---
elaborate
     adj 1: marked by complexity and richness of detail; "an elaborate
            lace pattern" [syn: {luxuriant}]
     2: developed or executed with care and in minute detail; "a
        detailed plan"; "the elaborate register of the inhabitants
        prevented tax evasion"- John Buchan; "the carefully
        elaborated theme" [syn: {detailed}, {elaborated}]
     v 1: add details, as to an account or idea; clarify the meaning
          of and discourse in a learned way, usually in writing;
          "She elaborated on the main ideas in her dissertation"
          [syn: {lucubrate}, {expatiate}, {exposit}, {enlarge}, {flesh
          out}, {expand}, {expound}, {dilate}] [ant: ...
