---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrears
offline_file: ""
offline_thumbnail: ""
uuid: 690a61db-ca8b-4403-bc67-879ece050777
updated: 1484310577
title: arrears
categories:
    - Dictionary
---
arrears
     n 1: the state of being behind in payments; "an account in
          arrears"
     2: an unpaid overdue debt
