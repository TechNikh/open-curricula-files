---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cussed
offline_file: ""
offline_thumbnail: ""
uuid: 97bd6f5e-7153-4f8f-b8d8-b97c4537b4d8
updated: 1484310138
title: cussed
categories:
    - Dictionary
---
cussed
     adj : stubbornly persistent in wrongdoing [syn: {obdurate}, {obstinate},
            {unrepentant}]
