---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/babe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484597281
title: babe
categories:
    - Dictionary
---
babe
     n : a very young child (birth to 1 year) who has not yet begun
         to walk or talk; "isn't she too young to have a baby?"
         [syn: {baby}, {infant}]
