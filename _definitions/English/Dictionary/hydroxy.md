---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydroxy
offline_file: ""
offline_thumbnail: ""
uuid: 53937cc5-09a5-4e9c-9600-5df39e9ed9e0
updated: 1484310423
title: hydroxy
categories:
    - Dictionary
---
hydroxy
     adj : being or containing a hydroxyl group
