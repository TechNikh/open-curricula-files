---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radiative_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 6e30cba6-1fa2-41ba-8b2c-2e24bb35860b
updated: 1484308853
title: Radiative equilibrium
tags:
    - Definitions
    - "Prevost's definitions"
    - Pointwise radiative equilibrium
    - Approximate pointwise radiative equilibrium
    - Radiative exchange equilibrium
    - Approximate radiative exchange equilibrium
    - >
        Definition for an entire passive celestial system such as a
        planet that does not supply its own energy
    - Global radiative equilibrium
    - >
        Definition for an entire active celestial system such as a
        star that supplies its own energy
    - A contrary definition
    - Mechanisms of radiative equilibrium
categories:
    - Thermodynamics
---
Radiative equilibrium is one of the several requirements for thermodynamic equilibrium, but it can occur in the absence of thermodynamic equilibrium. There are various types of radiative equilibrium, which is itself a kind of dynamic equilibrium.
