---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swap
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499901
title: swap
categories:
    - Dictionary
---
swap
     n : an equal exchange; "we had no money so we had to live by
         barter" [syn: {barter}, {swop}, {trade}]
     v 1: exchange or give (something) in exchange for [syn: {trade},
          {swop}, {switch}]
     2: move (a piece of a program) into memory, in computer science
     [also: {swops}, {swopping}, {swopped}, {swapping}, {swapped}]
