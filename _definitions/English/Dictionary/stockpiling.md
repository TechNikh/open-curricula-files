---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stockpiling
offline_file: ""
offline_thumbnail: ""
uuid: 519230f0-c530-4996-a434-afc1e2d532ef
updated: 1484310174
title: stockpiling
categories:
    - Dictionary
---
stockpiling
     n : accumulating and storing a reserve supply; "the stockpiling
         of war materials"
