---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bateman_transform
offline_file: ""
offline_thumbnail: ""
uuid: 47ae274d-b409-411d-8996-40c54dd6186a
updated: 1484309257
title: Bateman transform
categories:
    - Integral geometry
---
In the mathematical study of partial differential equations, the Bateman transform is a method for solving the Laplace equation in four dimensions and wave equation in three by using a line integral of a holomorphic function in three complex variables. It is named after the English mathematician Harry Bateman, who first published the result in (Bateman 1904).
