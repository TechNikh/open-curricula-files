---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enigmatic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484431081
title: enigmatic
categories:
    - Dictionary
---
enigmatic
     adj 1: not clear to the understanding; "I didn't grasp the meaning
            of that enigmatic comment until much later";
            "prophetic texts so enigmatic that their meaning has
            been disputed for centuries" [syn: {enigmatical}, {puzzling}]
     2: resembling an oracle in obscurity of thought; "the oracular
        sayings of Victorian poets"; "so enigmatic that priests
        might have to clarify it"; "an enigmatic smile" [syn: {oracular}]
