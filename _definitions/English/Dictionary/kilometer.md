---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kilometer
offline_file: ""
offline_thumbnail: ""
uuid: 3a52fc26-a3d5-43b3-8e6b-8d80e68439cf
updated: 1484310163
title: kilometer
categories:
    - Dictionary
---
kilometer
     n : a metric unit of length equal to 1000 meters (or 0.621371
         miles) [syn: {kilometre}, {km}, {klick}]
