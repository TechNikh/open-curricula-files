---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collecting
offline_file: ""
offline_thumbnail: ""
uuid: 439b6886-c391-4093-84e9-f962479f139b
updated: 1484310311
title: collecting
categories:
    - Dictionary
---
collecting
     n : the act of gathering something together [syn: {collection},
         {assembling}, {aggregation}]
