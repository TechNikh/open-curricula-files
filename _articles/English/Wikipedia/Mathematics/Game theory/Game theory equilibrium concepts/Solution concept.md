---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solution_concept
offline_file: ""
offline_thumbnail: ""
uuid: eeea5625-a569-49eb-bf48-f30ff439f8aa
updated: 1484309032
title: Solution concept
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Equilibrium_refinements.svg.png
tags:
    - Formal definition
    - Rationalizability and iterated dominance
    - Nash equilibrium
    - Backward induction
    - Subgame perfect Nash equilibrium
    - Perfect Bayesian equilibrium
    - Forward induction
categories:
    - Game theory equilibrium concepts
---
In game theory, a solution concept is a formal rule for predicting how a game will be played. These predictions are called "solutions", and describe which strategies will be adopted by players and, therefore, the result of the game. The most commonly used solution concepts are equilibrium concepts, most famously Nash equilibrium.
