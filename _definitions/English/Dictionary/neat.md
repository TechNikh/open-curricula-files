---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neat
offline_file: ""
offline_thumbnail: ""
uuid: 877a29a3-8f4f-4e37-ab5e-b530f006cfe4
updated: 1484310319
title: neat
categories:
    - Dictionary
---
neat
     adj 1: clean or orderly; "her neat dress"; "a neat room"
     2: showing care in execution; "neat homework"; "neat
        handwriting"
     3: free from what is tawdry or unbecoming; "a neat style"; "a
        neat set of rules"; "she hated to have her neat plans
        upset" [syn: {refined}, {tasteful}]
     4: free from clumsiness; precisely or deftly executed; "he
        landed a clean left on his opponent's cheek"; "a clean
        throw"; "the neat exactness of the surgeon's knife" [syn:
        {clean}]
     5: very good; "he did a bully job"; "a neat sports car"; "had a
        great time at the party"; "you look simply smashing" [syn:
         {bang-up}, {bully}, ...
