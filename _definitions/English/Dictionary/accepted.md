---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accepted
offline_file: ""
offline_thumbnail: ""
uuid: 912e0685-f7ba-4329-9f0e-bd625e0d8a3e
updated: 1484310395
title: accepted
categories:
    - Dictionary
---
accepted
     adj 1: generally approved or compelling recognition; "several
            accepted techniques for treating the condition"; "his
            recognized superiority in this kind of work" [syn: {recognized},
             {recognised}]
     2: generally agreed upon; not subject to dispute; "the accepted
        interpretation of the poem"; "an accepted theory" [syn: {undisputed}]
     3: generally accepted or used; "accepted methods of harmony and
        melody"; "three accepted types of pump"
     4: judged to be in conformity with approved usage; "acceptable
        English usage" [syn: {acceptable}]
     5: widely or permanently accepted; "an accepted precedent"
     6: widely ...
