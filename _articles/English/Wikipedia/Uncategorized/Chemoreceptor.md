---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemoreceptors
offline_file: ""
offline_thumbnail: ""
uuid: 10795d8b-1a37-4ae2-be46-28edef0bd1d9
updated: 1484308351
title: Chemoreceptor
tags:
    - Classes
    - Sensory Organs
    - In Physiology
    - Control of Breathing
    - Heart rate
categories:
    - Uncategorized
---
A chemoreceptor, also known as chemosensor, is a sensory receptor that transduces a chemical signal into an action potential. In more general terms, a chemosensor detects certain chemical stimuli in the environment.
