---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Limiting_point_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: a85fdec6-7645-471c-8bf0-aa4d4601f355
updated: 1484309262
title: Limiting point (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Apollonian_circles.svg.png
categories:
    - Inversive geometry
---
In geometry, the limiting points of two disjoint circles A and B in the Euclidean plane are points p that may be defined by any of the following equivalent properties:
