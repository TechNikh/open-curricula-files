---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entertain
offline_file: ""
offline_thumbnail: ""
uuid: 55debe2b-6675-4579-bc2c-f15244efeb58
updated: 1484310148
title: entertain
categories:
    - Dictionary
---
entertain
     v 1: provide entertainment for
     2: take into consideration, have in view; "He entertained the
        notion of moving to South America" [syn: {think of}, {toy
        with}, {flirt with}, {think about}]
     3: maintain (a theory, thoughts, or feelings); "bear a grudge";
        "entertain interesting notions"; "harbor a resentment"
        [syn: {harbor}, {harbour}, {hold}, {nurse}]
