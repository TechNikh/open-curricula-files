---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epsilon-equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 1b43dcb2-4ac5-4deb-a131-d4fe9a44560a
updated: 1484309032
title: Epsilon-equilibrium
tags:
    - Definition
    - The standard definition
    - Well-supported approximate equilibrium
    - Results
    - Example
categories:
    - Game theory equilibrium concepts
---
In game theory, an epsilon-equilibrium, or near-Nash equilibrium, is a strategy profile that approximately satisfies the condition of Nash equilibrium. In a Nash equilibrium, no player has an incentive to change his behavior. In an approximate Nash equilibrium, this requirement is weakened to allow the possibility that a player may have a small incentive to do something different. This may still be considered an adequate solution concept, assuming for example status quo bias. This solution concept may be preferred to Nash equilibrium due to being easier to compute, or alternatively due to the possibility that in games of more than 2 players, the probabilities involved in an exact Nash ...
