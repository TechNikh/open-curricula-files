---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Node_deletion
offline_file: ""
offline_thumbnail: ""
uuid: dcf30ac4-990e-4487-8c24-2046e81bfac4
updated: 1484309435
title: Node deletion
tags:
    - Random deletion
    - Erdős-Rényi model
    - Barabási-Albert model
    - Random removal from a growing network
    - Targeted deletion
    - Erdős-Rényi model
    - Barabási-Albert model
categories:
    - Network theory
---
Node deletion is the procedure of removing a node from a network, where the node is either chosen randomly or directly. Node deletion is used to test the robustness and the attack tolerance of networks. Understanding how a network changes in response to node deletion is critical in many empirical networks. Application varies across many fields, including the breakdown of the World Wide Web via router removal, elimination of epidemics or fighting against criminal organizations.
