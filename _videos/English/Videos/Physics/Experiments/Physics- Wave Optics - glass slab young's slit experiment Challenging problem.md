---
version: 1
type: video
provider: YouTube
id: RGu1l2-9mVM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d2a57a26-cff1-460e-a66d-a4efb47bc95a
updated: 1486069635
title: "Physics- Wave Optics - glass slab young's slit experiment Challenging problem"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/RGu1l2-9mVM/default.jpg
    - https://i3.ytimg.com/vi/RGu1l2-9mVM/1.jpg
    - https://i3.ytimg.com/vi/RGu1l2-9mVM/2.jpg
    - https://i3.ytimg.com/vi/RGu1l2-9mVM/3.jpg
---
