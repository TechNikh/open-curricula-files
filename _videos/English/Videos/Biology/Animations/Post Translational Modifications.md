---
version: 1
type: video
provider: YouTube
id: PMsxwRCliBE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Post%20Translational%20Modifications.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9759f0ac-6f03-4ab0-ba50-446414f0479a
updated: 1486069670
title: Post Translational Modifications
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/PMsxwRCliBE/default.jpg
    - https://i3.ytimg.com/vi/PMsxwRCliBE/1.jpg
    - https://i3.ytimg.com/vi/PMsxwRCliBE/2.jpg
    - https://i3.ytimg.com/vi/PMsxwRCliBE/3.jpg
---
