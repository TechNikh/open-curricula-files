---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Combinatorics_and_dynamical_systems
offline_file: ""
offline_thumbnail: ""
uuid: 56ded83f-35ee-4306-9eb5-328fbfdee8e8
updated: 1484308551
title: Combinatorics and dynamical systems
categories:
    - Combinatorics
---
The mathematical disciplines of combinatorics and dynamical systems interact in a number of ways. The ergodic theory of dynamical systems has recently been used to prove combinatorial theorems about number theory which has given rise to the field of arithmetic combinatorics. Also dynamical systems theory is heavily involved in the relatively recent field of combinatorics on words. Also combinatorial aspects of dynamical systems are studied. Dynamical systems can be defined on combinatorial objects; see for example graph dynamical system.
