---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Square_trisection
offline_file: ""
offline_thumbnail: ""
uuid: 58641c14-8b34-4243-915d-49cfd55776e9
updated: 1484309249
title: Square trisection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trisection_Blanvillain.png
tags:
    - Problem History
    - Search of optimality
    - Bibliography
categories:
    - History of geometry
---
