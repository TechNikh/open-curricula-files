---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radius
offline_file: ""
offline_thumbnail: ""
uuid: 8668bfb5-782d-434c-9c87-6fb2c1e7b0ae
updated: 1484310220
title: radius
categories:
    - Dictionary
---
radius
     n 1: the length of a line segment between the center and
          circumference of a circle or sphere [syn: {r}]
     2: a straight line from the center to the perimeter of a circle
        (or from the center to the surface of a sphere)
     3: a circular region whose area is indicated by the length of
        its radius; "they located it within a radius of 2 miles"
     4: the outer and slightly shorter of the two bones of the human
        forearm
     5: support consisting of a radial member of a wheel joining the
        hub to the rim [syn: {spoke}]
     [also: {radii} (pl)]
