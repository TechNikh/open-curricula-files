---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Morton%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2ed9edfe-9a42-4c28-8375-a5248868f392
updated: 1484309059
title: "Morton's theorem"
tags:
    - An example
    - Analysis
    - Notes
categories:
    - Poker strategy
---
Morton's theorem is a poker principle articulated by Andy Morton in a Usenet poker newsgroup. It states that in multi-way pots, a player's expectation may be maximized by an opponent making a correct decision.
