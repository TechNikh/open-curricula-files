---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenomenon
offline_file: ""
offline_thumbnail: ""
uuid: 359c8740-df98-4aa6-ac04-2e3089d9f55e
updated: 1484310228
title: phenomenon
categories:
    - Dictionary
---
phenomenon
     n 1: any state or process known through the senses rather than by
          intuition or reasoning
     2: a remarkable development
     [also: {phenomena} (pl)]
