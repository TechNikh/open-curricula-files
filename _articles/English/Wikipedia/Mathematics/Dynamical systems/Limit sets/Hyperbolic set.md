---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_set
offline_file: ""
offline_thumbnail: ""
uuid: 6d9d2fa3-8eec-4f8e-a733-33b0755229dc
updated: 1484308797
title: Hyperbolic set
categories:
    - Limit sets
---
In dynamical systems theory, a subset Λ of a smooth manifold M is said to have a hyperbolic structure with respect to a smooth map f if its tangent bundle may be split into two invariant subbundles, one of which is contracting and the other is expanding under f, with respect to some Riemannian metric on M. An analogous definition applies to the case of flows.
