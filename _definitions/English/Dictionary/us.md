---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/us
offline_file: ""
offline_thumbnail: ""
uuid: e74437f3-2485-413d-9cec-aa66ac70f6d4
updated: 1484310361
title: us
categories:
    - Dictionary
---
U.S.
     n 1: the executive and legislative and judicial branches of the
          federal government of the United States [syn: {United
          States government}, {United States}, {U.S. government},
          {US Government}]
     2: North American republic containing 50 states - 48
        conterminous states in North America plus Alaska in
        northwest North America and the Hawaiian Islands in the
        Pacific Ocean; achieved independence in 1776 [syn: {United
        States}, {United States of America}, {America}, {US}, {USA},
         {U.S.A.}]
