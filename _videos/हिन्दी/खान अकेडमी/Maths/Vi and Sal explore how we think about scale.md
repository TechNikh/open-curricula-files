---
version: 1
type: video
provider: YouTube
id: ah-pTun6Zjk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a12fec8f-9b02-4b02-8133-aef1d2b23965
updated: 1486069617
title: Vi and Sal explore how we think about scale
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ah-pTun6Zjk/default.jpg
    - https://i3.ytimg.com/vi/ah-pTun6Zjk/1.jpg
    - https://i3.ytimg.com/vi/ah-pTun6Zjk/2.jpg
    - https://i3.ytimg.com/vi/ah-pTun6Zjk/3.jpg
---
