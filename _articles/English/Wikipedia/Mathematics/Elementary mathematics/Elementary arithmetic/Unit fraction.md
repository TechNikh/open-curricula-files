---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unit_fraction
offline_file: ""
offline_thumbnail: ""
uuid: b5668144-7eec-4b98-95a0-cb51515d59dc
updated: 1484308925
title: Unit fraction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_11.png
tags:
    - Elementary arithmetic
    - Modular arithmetic
    - Finite sums of unit fractions
    - Series of unit fractions
    - Matrices of unit fractions
    - Adjacent fractions
    - Unit fractions in probability and statistics
    - Unit fractions in physics
categories:
    - Elementary arithmetic
---
A unit fraction is a rational number written as a fraction where the numerator is one and the denominator is a positive integer. A unit fraction is therefore the reciprocal of a positive integer, 1/n. Examples are 1/1, 1/2, 1/3, 1/4 ,1/5, etc.
