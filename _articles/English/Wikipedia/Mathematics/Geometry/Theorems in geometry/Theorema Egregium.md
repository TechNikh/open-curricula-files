---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theorema_Egregium
offline_file: ""
offline_thumbnail: ""
uuid: f22bdf37-6bb0-4515-ae38-1b1b6a47155c
updated: 1484309127
title: Theorema Egregium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mercator-proj.png
tags:
    - Elementary applications
    - Notes
categories:
    - Theorems in geometry
---
Gauss's Theorema Egregium (Latin for "Remarkable Theorem") is a foundational result in differential geometry proved by Carl Friedrich Gauss that concerns the curvature of surfaces. The theorem says that the Gaussian curvature of a surface does not change if one bends the surface without stretching it. In other words, Gaussian curvature can be determined entirely by measuring angles, distances and their rates on the surface itself, without further reference to the particular way in which the surface is embedded in the ambient 3-dimensional Euclidean space. Thus the Gaussian curvature is an intrinsic invariant of a surface.
