---
version: 1
type: video
provider: YouTube
id: WaS2zOWQM00
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 044c802b-fe9c-43fc-b0a2-cd777da445f2
updated: 1486069634
title: 'How to Make a Motorcycle - (Recycle - Toy)'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/WaS2zOWQM00/default.jpg
    - https://i3.ytimg.com/vi/WaS2zOWQM00/1.jpg
    - https://i3.ytimg.com/vi/WaS2zOWQM00/2.jpg
    - https://i3.ytimg.com/vi/WaS2zOWQM00/3.jpg
---
