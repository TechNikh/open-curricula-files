---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orderly
offline_file: ""
offline_thumbnail: ""
uuid: 78c88dfd-2f18-481a-bd8e-527189213c89
updated: 1484310365
title: orderly
categories:
    - Dictionary
---
orderly
     adj 1: devoid of violence or disruption; "an orderly crowd
            confronted the president" [ant: {disorderly}]
     2: according to custom or rule or natural law [syn: {lawful}, {rule-governed}]
     3: not haphazard; "a series of orderly actions at regular
        hours" [syn: {systematic}]
     4: marked by or adhering to method or system; "a clean orderly
        man"; "an orderly mind"; "an orderly desk"
     5: marked by system or regularity or discipline; "a quiet
        ordered house"; "an orderly universe"; "a well regulated
        life" [syn: {ordered}, {regulated}]
     6: marked by an orderly, logical, and aesthetically consistent
        relation of parts; ...
