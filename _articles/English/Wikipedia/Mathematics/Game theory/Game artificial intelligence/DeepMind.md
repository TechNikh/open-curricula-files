---
version: 1
type: article
id: https://en.wikipedia.org/wiki/DeepMind
offline_file: ""
offline_thumbnail: ""
uuid: 42c36900-58d0-4cfa-bf68-29baa9caf002
updated: 1484309004
title: DeepMind
tags:
    - History
    - Machine learning
    - Deep reinforcement learning
    - AlphaGo
    - Healthcare
    - Controversies
categories:
    - Game artificial intelligence
---
DeepMind Technologies Limited is a British artificial intelligence company founded in September 2010. It was acquired by Google in 2014. The company has created a neural network that learns how to play video games in a fashion similar to that of humans,[4] as well as a Neural Turing Machine, or a neural network that may be able to access an external memory like a conventional Turing machine, resulting in a computer that mimics the short-term memory of the human brain.[5]
