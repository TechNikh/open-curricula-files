---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Periodic_graph_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: c04f7824-02cb-4fd2-9762-7823ae4b2157
updated: 1484309420
title: Periodic graph (graph theory)
categories:
    - Graph invariants
---
In graph theory, a branch of mathematics, a periodic graph with respect to an operator F on graphs is one for which there exists an integer n > 0 such that Fn(G) is isomorphic to G.[1] For example, every graph is periodic with respect to the complementation operator, whereas only complete graphs are periodic with respect to the operator that assigns to each graph the complete graph on the same vertices. Periodicity is one of many properties of graph operators, the central topic in graph dynamics.[2]
