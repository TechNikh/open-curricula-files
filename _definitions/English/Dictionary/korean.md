---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/korean
offline_file: ""
offline_thumbnail: ""
uuid: 0476c175-a56d-4dd4-b6ff-be80ec2b6dc8
updated: 1484310486
title: korean
categories:
    - Dictionary
---
Korean
     adj : of or relating to or characteristic of Korea or its people
           or language; "Korean handicrafts"
     n 1: a native or inhabitant of Korea who speaks the Korean
          language
     2: the Altaic language spoken by the Korean people
