---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anna_Wienhard
offline_file: ""
offline_thumbnail: ""
uuid: 911bc6c8-60cb-40a9-ba55-fb25a1160d10
updated: 1484309107
title: Anna Wienhard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Anna_Wienhard_MFO.jpg
categories:
    - Geometers
---
Anna Katharina Wienhard (born 1977)[1] is a German mathematician whose research concerns differential geometry, and especially the use of higher Teichmüller spaces to study the deformation theory of symmetric geometric structures.[1] She is a professor at Heidelberg University.[2]
