---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaken
offline_file: ""
offline_thumbnail: ""
uuid: d0b81592-3a0d-4504-9443-9b699b6d5dd2
updated: 1484310152
title: shaken
categories:
    - Dictionary
---
shake
     n 1: building material used as siding or roofing [syn: {shingle}]
     2: frothy drink of milk and flavoring and sometimes fruit or
        ice cream [syn: {milkshake}, {milk shake}]
     3: a note that alternates rapidly with another note a semitone
        above it [syn: {trill}]
     4: grasping and shaking a person's hand (as to acknowledge an
        introduction or to agree on a contract) [syn: {handshake},
         {handshaking}, {handclasp}]
     5: reflex shaking caused by cold or fear or excitement [syn: {tremble},
         {shiver}]
     6: causing to move repeatedly from side to side [syn: {wag}, {waggle}]
     v 1: move or cause to move back and forth; "The chemist ...
