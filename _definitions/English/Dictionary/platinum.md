---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/platinum
offline_file: ""
offline_thumbnail: ""
uuid: a36fbd14-318d-4fe0-951e-5decaaee0c33
updated: 1484310389
title: platinum
categories:
    - Dictionary
---
platinum
     n : a heavy precious metallic element; gray-white and resistant
         to corroding; occurs in some nickel and copper ores and
         is also found native in some deposits [syn: {Pt}, {atomic
         number 78}]
