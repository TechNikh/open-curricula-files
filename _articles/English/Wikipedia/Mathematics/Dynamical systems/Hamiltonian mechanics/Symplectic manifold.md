---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symplectic_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 4cf8572b-f926-45cd-b8d5-c7da572bd1bb
updated: 1484308794
title: Symplectic manifold
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TIKZ_PICT_FBN.png
tags:
    - Motivation
    - Definition
    - Linear symplectic manifold
    - Lagrangian and other submanifolds
    - Special Lagrangian submanifolds
    - Lagrangian fibration
    - Lagrangian mapping
    - Special cases and generalizations
    - Notes
categories:
    - Hamiltonian mechanics
---
In mathematics, a symplectic manifold is a smooth manifold, M, equipped with a closed nondegenerate differential 2-form, ω, called the symplectic form. The study of symplectic manifolds is called symplectic geometry or symplectic topology. Symplectic manifolds arise naturally in abstract formulations of classical mechanics and analytical mechanics as the cotangent bundles of manifolds. For example, in the Hamiltonian formulation of classical mechanics, which provides one of the major motivations for the field, the set of all possible configurations of a system is modeled as a manifold, and this manifold's cotangent bundle describes the phase space of the system.
