---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sedimentary
offline_file: ""
offline_thumbnail: ""
uuid: a4acd186-e2d9-46db-8ca4-6aa6e4cedbc1
updated: 1484310435
title: sedimentary
categories:
    - Dictionary
---
sedimentary
     adj : resembling or containing or formed by the accumulation of
           sediment; "sedimentary deposits"
