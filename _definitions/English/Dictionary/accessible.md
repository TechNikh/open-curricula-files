---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accessible
offline_file: ""
offline_thumbnail: ""
uuid: e9cf6cd0-0170-4e93-beaa-65c6a6f8d854
updated: 1484310210
title: accessible
categories:
    - Dictionary
---
accessible
     adj 1: capable of being reached; "a town accessible by rail" [ant:
            {inaccessible}]
     2: capable of being read with comprehension; "readily
        accessible to the nonprofessional reader"; "the tales seem
        more approachable than his more difficult novels" [syn: {approachable}]
     3: easily obtained; "most students now have computers
        accessible"; "accessible money"
     4: easy to get along with or talk to; friendly; "an accessible
        and genial man"
