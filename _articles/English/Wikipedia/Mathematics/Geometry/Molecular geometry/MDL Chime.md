---
version: 1
type: article
id: https://en.wikipedia.org/wiki/MDL_Chime
offline_file: ""
offline_thumbnail: ""
uuid: d09d3e10-4c0e-4870-8d2e-d27a61833d86
updated: 1484309279
title: MDL Chime
categories:
    - Molecular geometry
---
MDL Chime is a free plugin used by web browsers to display the three-dimensional structures of molecules. It is part of the ISIS product line acquired by Symyx Technologies from scientific publisher Elsevier in October 2007. It is based on the RasMol code.
