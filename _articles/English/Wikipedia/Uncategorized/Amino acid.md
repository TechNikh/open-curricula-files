---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amino_acids
offline_file: ""
offline_thumbnail: ""
uuid: 24260552-aed0-4757-958e-936d87da0e7f
updated: 1484308371
title: Amino acid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-AminoAcidball.svg.png
tags:
    - History
    - General structure
    - Isomerism
    - Side chains
    - Zwitterions
    - Isoelectric point
    - Occurrence and functions in biochemistry
    - Proteinogenic amino acids
    - Non-proteinogenic amino acids
    - D-amino acid natural abundance
    - Non-standard amino acids
    - In human nutrition
    - Non-protein functions
    - Uses in industry
    - Expanded genetic code
    - Nullomers
    - Chemical building blocks
    - Biodegradable plastics
    - Reactions
    - Chemical synthesis
    - Peptide bond formation
    - Biosynthesis
    - Catabolism
    - Physicochemical properties of amino acids
    - Table of standard amino acid abbreviations and properties
    - References and notes
categories:
    - Uncategorized
---
Amino acids are biologically important organic compounds containing amine (-NH2) and carboxyl (-COOH) functional groups, along with a side-chain (R group) specific to each amino acid.[1][2][3] The key elements of an amino acid are carbon, hydrogen, oxygen, and nitrogen, though other elements are found in the side-chains of certain amino acids. About 500 amino acids are known (though only 20 appear in the genetic code) and can be classified in many ways.[4] They can be classified according to the core structural functional groups' locations as alpha- (α-), beta- (β-), gamma- (γ-) or delta- (δ-) amino acids; other categories relate to polarity, pH level, and side-chain group type ...
