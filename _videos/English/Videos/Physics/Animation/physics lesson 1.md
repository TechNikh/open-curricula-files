---
version: 1
type: video
provider: YouTube
id: 43pdxmXEaDw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/physics%20lesson%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ddb52521-b2f4-47ec-be25-348978a0d593
updated: 1486069654
title: physics lesson 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/43pdxmXEaDw/default.jpg
    - https://i3.ytimg.com/vi/43pdxmXEaDw/1.jpg
    - https://i3.ytimg.com/vi/43pdxmXEaDw/2.jpg
    - https://i3.ytimg.com/vi/43pdxmXEaDw/3.jpg
---
