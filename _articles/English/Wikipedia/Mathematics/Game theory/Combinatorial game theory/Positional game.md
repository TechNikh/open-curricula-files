---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Positional_game
offline_file: ""
offline_thumbnail: ""
uuid: e2afbe89-40d1-45d8-bd5a-729a634e2e4f
updated: 1484308533
title: Positional game
categories:
    - Combinatorial game theory
---
In the mathematical study of combinatorial games, positional games are games described by a finite set of positions in which a move consists of claiming a previously-unclaimed position. Well-known games that fall into this class include Tic-tac-toe, Hex, and the Shannon switching game.[1][2]
