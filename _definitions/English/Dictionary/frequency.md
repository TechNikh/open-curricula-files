---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frequency
offline_file: ""
offline_thumbnail: ""
uuid: 1e4f1a37-c474-4aa2-9511-b315df7d5c70
updated: 1484310295
title: frequency
categories:
    - Dictionary
---
frequency
     n 1: the number of occurrences within a given time period
          (usually 1 second); "the frequency of modulation was 40
          cycles per second" [syn: {frequence}, {oftenness}]
     2: the ratio of the number of observations in a statistical
        category to the total number of observations [syn: {relative
        frequency}]
     3: the number of observations in a given statistical category
        [syn: {absolute frequency}]
