---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kempf%E2%80%93Ness_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 79d1183a-907b-48fd-98ac-33927ddec13e
updated: 1484309363
title: Kempf–Ness theorem
categories:
    - Geometry stubs
---
In algebraic geometry, the Kempf–Ness theorem, introduced by Kempf and Ness (1979), gives a criterion for the stability of a vector in a representation of a complex reductive group. If the complex vector space is given a norm that is invariant under a maximal compact subgroup of the reductive group, then the Kempf–Ness theorem states that a vector is stable if and only if the norm attains a minimum value on the orbit of the vector.
