---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rudolf_Luneburg
offline_file: ""
offline_thumbnail: ""
uuid: 9da62df5-f823-4b49-a82c-6d63332b9ecd
updated: 1484309088
title: Rudolf Luneburg
categories:
    - Geometers
---
Rudolf Karl Lüneburg (30 March 1903, Volkersheim (Bockenem) - 19 August 1949, Great Falls, Montana), after his emigration at first Lueneburg, later Luneburg, falsified Luneberg) was a professor of mathematics and optics at the Dartmouth College Eye Institute. He was born in Germany, received his doctorate at Göttingen, and emigrated to the United States in 1935.
