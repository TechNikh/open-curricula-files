---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yellow
offline_file: ""
offline_thumbnail: ""
uuid: 25736413-b9cd-451f-9741-731ac3c8b277
updated: 1484310305
title: yellow
categories:
    - Dictionary
---
yellow
     adj 1: similar to the color of an egg yolk [syn: {yellowish}, {xanthous}]
     2: easily frightened [syn: {chicken}, {chickenhearted}, {lily-livered},
         {white-livered}, {yellow-bellied}]
     3: changed to a yellowish color by age; "yellowed parchment"
        [syn: {yellowed}]
     4: typical of tabloids; "sensational journalistic reportage of
        the scandal"; "yellow journalism" [syn: {scandalmongering},
         {sensationalistic}, {yellow(a)}]
     5: cowardly or treacherous; "the little yellow stain of
        treason"-M.W.Straight; "too yellow to stand and fight"
     6: affected by jaundice which causes yellowing of skin etc
        [syn: {jaundiced}, ...
