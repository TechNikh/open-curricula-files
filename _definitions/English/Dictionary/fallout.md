---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fallout
offline_file: ""
offline_thumbnail: ""
uuid: 0498177a-16e6-44b4-89c3-077454df260b
updated: 1484310188
title: fallout
categories:
    - Dictionary
---
fall out
     v 1: have a breach in relations; "We fell out over a trivial
          question"
     2: come as a logical consequence; follow logically; "It follows
        that your assertion is false"; "the theorem falls out
        nicely" [syn: {follow}]
     3: come forth or out; "You stick the coins in, but they come
        out again"; "His hair and teeth fell out" [syn: {come out},
         {pop out}]
     4: leave (a barracks, for example) in order to take a place in
        a military formation, or leave a military formation; "the
        soldiers fell out"
     5: come to pass; "What is happening?"; "The meeting took place
        off without an incidence"; "Nothing occurred that ...
