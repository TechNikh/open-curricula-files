---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shout
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484533561
title: shout
categories:
    - Dictionary
---
shout
     n : a loud utterance; often in protest or opposition; "the
         speaker was interrupted by loud cries from the rear of
         the audience" [syn: {cry}, {outcry}, {call}, {yell}, {vociferation}]
     v 1: utter in a loud voice; talk in a loud voice (usually
          denoting characteristic manner of speaking); "My
          grandmother is hard of hearing--you'll have to shout"
          [ant: {whisper}]
     2: utter a sudden loud cry; "she cried with pain when the
        doctor inserted the needle"; "I yelled to her from the
        window but she couldn't hear me" [syn: {shout out}, {cry},
         {call}, {yell}, {scream}, {holler}, {hollo}, {squall}]
     3: utter ...
