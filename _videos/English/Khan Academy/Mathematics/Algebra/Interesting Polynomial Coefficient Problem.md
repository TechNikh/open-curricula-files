---
version: 1
type: video
provider: YouTube
id: hypi8QPsFEk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Interesting%20Polynomial%20Coefficient%20Problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 93d8ef9f-12e2-49c8-a3ae-1298d1bea678
updated: 1486069573
title: Interesting Polynomial Coefficient Problem
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/hypi8QPsFEk/default.jpg
    - https://i3.ytimg.com/vi/hypi8QPsFEk/1.jpg
    - https://i3.ytimg.com/vi/hypi8QPsFEk/2.jpg
    - https://i3.ytimg.com/vi/hypi8QPsFEk/3.jpg
---
Finding the coefficients of a third degree polynomial given 2 roots and the y-intercept
More free lessons at: http://www.khanacademy.org/video?v=hypi8QPsFEk
