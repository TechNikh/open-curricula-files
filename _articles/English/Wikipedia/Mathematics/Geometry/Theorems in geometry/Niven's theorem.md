---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Niven%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f0dee131-c166-4ca3-ac49-4b00709b122d
updated: 1484309117
title: "Niven's theorem"
categories:
    - Theorems in geometry
---
In mathematics, Niven's theorem, named after Ivan Niven, states that the only rational values of θ in the interval 0 ≤ θ ≤ 90 for which the sine of θ degrees is also a rational number are:[1]
