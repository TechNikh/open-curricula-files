---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sir
offline_file: ""
offline_thumbnail: ""
uuid: 8982d373-080f-4e24-9444-14cb79cd333d
updated: 1484310290
title: sir
categories:
    - Dictionary
---
sir
     n 1: term of address for a man
     2: a title used before the name of knight or baronet
