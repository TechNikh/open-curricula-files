---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scattered
offline_file: ""
offline_thumbnail: ""
uuid: 73f9c9c0-52a0-44fd-933a-abf1d5292378
updated: 1484310455
title: scattered
categories:
    - Dictionary
---
scattered
     adj 1: occurring or distributed over widely spaced and irregular
            intervals in time or space; "scattered showers";
            "scattered villages"
     2: not close together in time; "isolated instances of
        rebellion"; "scattered fire"; "a stray bullet grazed his
        thigh" [syn: {isolated}, {stray}]
     3: being distributed here and there without order; "scattered
        leaves littered the sidewalk"; "don't forget to pick up
        the clothes lying strewn all over the floor" [syn: {strewn}]
     4: lacking orderly continuity; "a confused set of
        instructions"; "a confused dream about the end of the
        world"; "disconnected fragments of ...
