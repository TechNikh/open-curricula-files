---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cruel
offline_file: ""
offline_thumbnail: ""
uuid: 8312992e-94d4-4516-a32f-01a2edd47fae
updated: 1484310535
title: cruel
categories:
    - Dictionary
---
cruel
     adj 1: lacking or showing kindness or compassion or mercy [syn: {unkind}]
     2: (of persons or their actions) able or disposed to inflict
        pain or suffering; "a barbarous crime"; "brutal beatings";
        "cruel tortures"; "Stalin's roughshod treatment of the
        kulaks"; "a savage slap"; "vicious kicks" [syn: {barbarous},
         {brutal}, {fell}, {roughshod}, {savage}, {vicious}]
     3: (of weapons or instruments) causing suffering and pain;
        "brutal instruments of torture"; "cruel weapons of war"
        [syn: {brutal}]
     4: used of circumstances (especially weather) that cause
        suffering; "brutal weather"; "northern winters can be
        ...
