---
version: 1
type: video
provider: YouTube
id: y7QLay8wrW8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20inequalities/Solving%20a%20two-step%20inequality.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 288e8f5e-c4cc-4e36-853f-d8bdf447caf2
updated: 1486069559
title: Solving a two-step inequality
categories:
    - Linear inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/y7QLay8wrW8/default.jpg
    - https://i3.ytimg.com/vi/y7QLay8wrW8/1.jpg
    - https://i3.ytimg.com/vi/y7QLay8wrW8/2.jpg
    - https://i3.ytimg.com/vi/y7QLay8wrW8/3.jpg
---
