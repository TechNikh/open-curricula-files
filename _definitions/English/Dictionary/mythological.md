---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mythological
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483581
title: mythological
categories:
    - Dictionary
---
mythological
     adj 1: based on or told of in traditional stories; lacking factual
            basis or historical validity; "mythical centaurs";
            "the fabulous unicorn" [syn: {fabulous}, {mythic}, {mythical},
             {mythologic}]
     2: of or relating to mythology; dealt with in myths;
        "mythological stories"
