---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bounded
offline_file: ""
offline_thumbnail: ""
uuid: 055f64bc-d648-44e3-913c-5301c837d26b
updated: 1484310208
title: bounded
categories:
    - Dictionary
---
bounded
     adj : having the limits or boundaries established; "a delimited
           frontier through the disputed region" [syn: {delimited}]
