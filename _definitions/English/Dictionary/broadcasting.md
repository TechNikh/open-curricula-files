---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broadcasting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484649721
title: broadcasting
categories:
    - Dictionary
---
broadcasting
     n 1: a medium that disseminates via telecommunications [syn: {broadcast
          medium}]
     2: taking part in a radio or tv program
