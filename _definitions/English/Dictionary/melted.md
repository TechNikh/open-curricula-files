---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melted
offline_file: ""
offline_thumbnail: ""
uuid: af7977e8-0674-4370-9cf5-8cddc81e4486
updated: 1484310198
title: melted
categories:
    - Dictionary
---
melted
     adj : changed from a solid to a liquid state; "rivers filled to
           overflowing by melted snow" [syn: {liquid}, {liquified}]
           [ant: {unmelted}]
