---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Second_law_of_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 9dbcc3d9-cd04-4f76-9b23-91b41571144d
updated: 1484308802
title: Second law of thermodynamics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Deriving_Kelvin_Statement_from_Clausius_Statement.svg.png
tags:
    - Introduction
    - Various statements of the law
    - "Carnot's principle"
    - Clausius statement
    - Kelvin statement
    - Equivalence of the Clausius and the Kelvin statements
    - "Planck's proposition"
    - "Relation between Kelvin's statement and Planck's proposition"
    - "Planck's statement"
    - Principle of Carathéodory
    - "Planck's Principle"
    - >
        Statement for a system that has a known expression of its
        internal energy as a function of its extensive state
        variables
    - Corollaries
    - Perpetual motion of the second kind
    - Carnot theorem
    - Clausius Inequality
    - Thermodynamic temperature
    - Entropy
    - Energy, available useful work
    - History
    - Account given by Clausius
    - Statistical mechanics
    - Derivation from statistical mechanics
    - Derivation of the entropy change for reversible processes
    - Derivation for systems described by the canonical ensemble
    - Living organisms
    - Gravitational systems
    - Non-equilibrium states
    - Arrow of time
    - Irreversibility
    - "Loschmidt's paradox"
    - Poincaré recurrence theorem
    - "Maxwell's demon"
    - Quotations
    - Bibliography of citations
categories:
    - Non-equilibrium thermodynamics
---
The second law of thermodynamics states that the total entropy of an isolated system always increases over time, or remains constant in ideal cases where the system is in a steady state or undergoing a reversible process. The increase in entropy accounts for the irreversibility of natural processes, and the asymmetry between future and past.
