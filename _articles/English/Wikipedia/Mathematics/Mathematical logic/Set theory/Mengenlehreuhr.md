---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mengenlehreuhr
offline_file: ""
offline_thumbnail: ""
uuid: 1aa2eb98-266c-40b4-a474-a2df2f278129
updated: 1484309506
title: Mengenlehreuhr
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Mengenlehreuhr.jpg
tags:
    - Telling the time
    - Kryptos
categories:
    - Set theory
---
The Mengenlehreuhr (German for "Set Theory Clock") or Berlin-Uhr ("Berlin Clock") is the first public clock in the world that tells the time by means of illuminated, coloured fields, for which it entered the Guinness Book of Records upon its installation on 17 June 1975. Commissioned by the Senate of Berlin and designed by Dieter Binninger, the original full-sized Mengenlehreuhr was originally located at the Kurfürstendamm on the corner with Uhlandstraße. After the Senate decommissioned it in 1995, the clock was relocated to a site in Budapester Straße in front of Europa-Center, where it stands today.
