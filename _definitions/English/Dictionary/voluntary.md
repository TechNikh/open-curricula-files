---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/voluntary
offline_file: ""
offline_thumbnail: ""
uuid: 68745113-b336-491a-a47e-653d33dbbec1
updated: 1484310319
title: voluntary
categories:
    - Dictionary
---
voluntary
     adj 1: of your own free will or design; not forced or compelled;
            "man is a voluntary agent"; "participation was
            voluntary"; "voluntary manslaughter"; "voluntary
            generosity in times of disaster"; "voluntary social
            workers"; "a voluntary confession" [ant: {involuntary}]
     2: controlled by individual volition; "voluntary motions";
        "voluntary muscles" [ant: {involuntary}]
     n 1: (military) a person who freely enlists for service [syn: {volunteer},
           {military volunteer}] [ant: {draftee}]
     2: composition (often improvised) for a solo instrument
        (especially solo organ) and not a regular part of a
    ...
