---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sexually_reproducing
offline_file: ""
offline_thumbnail: ""
uuid: a0337203-578c-40ca-978a-f376eb1f143b
updated: 1484308375
title: Sexual reproduction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Sexual_cycle.svg.png
tags:
    - Evolution
    - Sexual selection
    - Sex ratio
    - Animals
    - Insects
    - birds
    - Mammals
    - Fish
    - Reptiles
    - Amphibians
    - Mollusks
    - Plants
    - Flowering plants
    - Ferns
    - Bryophytes
    - Fungi
    - Bacteria and archaea
    - Notes
categories:
    - Uncategorized
---
Sexual reproduction is a form of reproduction where two morphologically distinct types of specialized reproductive cells called gametes fuse together, involving a female's large ovum (or egg) and a male's smaller sperm. Each gamete contains half the number of chromosomes of normal cells. They are created by a specialized type of cell division, which only occurs in eukaryotic cells, known as meiosis. The two gametes fuse during fertilization to produce DNA replication and the creation of a single-celled zygote which includes genetic material from both gametes. In a process called genetic recombination, genetic material (DNA) joins up so that homologous chromosome sequences are aligned with ...
