---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/girl
offline_file: ""
offline_thumbnail: ""
uuid: 6ed86425-2a45-43d4-ad3a-85d1db69b564
updated: 1484310297
title: girl
categories:
    - Dictionary
---
girl
     n 1: a young woman; "a young lady of 18" [syn: {miss}, {missy}, {young
          lady}, {young woman}, {fille}]
     2: a youthful female person; "the baby was a girl"; "the girls
        were just learning to ride a tricycle" [syn: {female child},
         {little girl}] [ant: {male child}, {male child}]
     3: a female human offspring; "her daughter cared for her in her
        old age" [syn: {daughter}] [ant: {son}, {son}]
     4: a girl or young woman with whom a man is romantically
        involved; "his girlfriend kicked him out" [syn: {girlfriend},
         {lady friend}]
     5: a friendly informal reference to a grown woman; "Mrs. Smith
        was just one of the girls"
