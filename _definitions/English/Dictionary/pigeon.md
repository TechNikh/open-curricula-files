---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pigeon
offline_file: ""
offline_thumbnail: ""
uuid: 8748119f-aa6e-4591-9c1b-e125512788a4
updated: 1484310256
title: pigeon
categories:
    - Dictionary
---
pigeon
     n : wild and domesticated birds having a heavy body and short
         legs
