---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_Laplace_operator
offline_file: ""
offline_thumbnail: ""
uuid: 36dee3a2-a4ce-4a81-b21c-ce235dbc741c
updated: 1484308627
title: Discrete Laplace operator
tags:
    - Definitions
    - Graph Laplacians
    - Mesh Laplacians
    - Finite differences
    - Finite-element method
    - Image Processing
    - Implementation in image processing
    - Spectrum
    - Theorems
    - Discrete Schrödinger operator
    - "Discrete Green's function"
    - ADE classification
categories:
    - Graph theory
---
In mathematics, the discrete Laplace operator is an analog of the continuous Laplace operator, defined so that it has meaning on a graph or a discrete grid. For the case of a finite-dimensional graph (having a finite number of edges and vertices), the discrete Laplace operator is more commonly called the Laplacian matrix.
