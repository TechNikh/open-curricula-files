---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outburst
offline_file: ""
offline_thumbnail: ""
uuid: e9d6b50f-9eff-4220-b58a-5b7ea98ae2bb
updated: 1484310152
title: outburst
categories:
    - Dictionary
---
outburst
     n 1: an unrestrained expression of emotion [syn: {effusion}, {gush},
           {blowup}, {ebullition}]
     2: a sudden violent happening; "an outburst of heavy rain"; "a
        burst of lightning" [syn: {burst}, {flare-up}]
     3: a sudden violent disturbance [syn: {tumultuous disturbance}]
