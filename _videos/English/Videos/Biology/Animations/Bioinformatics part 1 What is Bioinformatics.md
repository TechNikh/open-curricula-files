---
version: 1
type: video
provider: YouTube
id: w-uk-_TOgR0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Bioinformatics%20part%201%20What%20is%20Bioinformatics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9cfed931-1b07-4cce-b8ae-a882866ce7dd
updated: 1486069669
title: Bioinformatics part 1 What is Bioinformatics
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/w-uk-_TOgR0/default.jpg
    - https://i3.ytimg.com/vi/w-uk-_TOgR0/1.jpg
    - https://i3.ytimg.com/vi/w-uk-_TOgR0/2.jpg
    - https://i3.ytimg.com/vi/w-uk-_TOgR0/3.jpg
---
