---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inside
offline_file: ""
offline_thumbnail: ""
uuid: 89a0f126-789a-4998-b945-1cd5202cabd0
updated: 1484310230
title: inside
categories:
    - Dictionary
---
inside
     adj 1: relating to or being within or near the inner side or limit;
            "he reached into his inside jacket pocket"; "inside
            out"; "an inside pitch is between home plate and the
            batter" [ant: {outside}]
     2: being or applying to the inside of a building; "an inside
        wall" [syn: {inside(a)}]
     3: located, suited for, or taking place within a building;
        "indoor activities for a rainy day"; "an indoor pool"
        [syn: {indoor(a)}] [ant: {outdoor(a)}]
     4: confined to an exclusive group; "privy to inner knowledge";
        "inside information"; "privileged information" [syn: {inner},
         {privileged}]
     5: away from ...
