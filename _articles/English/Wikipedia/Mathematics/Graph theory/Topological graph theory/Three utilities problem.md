---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Three_utilities_problem
offline_file: ""
offline_thumbnail: ""
uuid: c68ce020-1c4b-4e51-9e45-221ef22e6214
updated: 1484309481
title: Three utilities problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-K33_one_crossing.svg.png
tags:
    - History
    - Solution
    - Generalizations
    - Other graph-theoretic properties
categories:
    - Topological graph theory
---
The classical mathematical puzzle known as the three utilities problem; the three cottages problem or sometimes water, gas and electricity can be stated as follows:
