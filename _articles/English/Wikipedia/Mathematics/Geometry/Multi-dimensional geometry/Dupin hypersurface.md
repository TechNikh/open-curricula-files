---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dupin_hypersurface
offline_file: ""
offline_thumbnail: ""
uuid: 7244fa68-9c9f-4081-b2a2-51bce887b8f1
updated: 1484309283
title: Dupin hypersurface
categories:
    - Multi-dimensional geometry
---
A hypersurface is called a Dupin hypersurface if the multiplicity of each principal curvature is constant on hypersurface and each principal curvature is constant along its associated principal directions.[2] All proper Dupin submanifolds arise as focal submanifolds of proper Dupin hypersurfaces.[3]
