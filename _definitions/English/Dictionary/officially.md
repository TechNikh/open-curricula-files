---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/officially
offline_file: ""
offline_thumbnail: ""
uuid: fd939e07-8d0c-4491-b0d8-580b70e4b59e
updated: 1484310168
title: officially
categories:
    - Dictionary
---
officially
     adv 1: in an official role; "officially, he is in charge";
            "officially responsible" [ant: {unofficially}]
     2: with official authorization; "the club will be formally
        recognized" [syn: {formally}]
