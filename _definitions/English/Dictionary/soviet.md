---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soviet
offline_file: ""
offline_thumbnail: ""
uuid: b640cab8-455f-4c47-9a6f-73d37a33b3b3
updated: 1484310555
title: soviet
categories:
    - Dictionary
---
Soviet
     adj : of or relating to or characteristic of the former Soviet
           Union or its people; "Soviet leaders"
     n : an elected governmental council in a Communist country
         (especially one that is a member of the Union of Soviet
         Socialist Republics)
