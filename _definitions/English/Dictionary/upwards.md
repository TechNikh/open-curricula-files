---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upwards
offline_file: ""
offline_thumbnail: ""
uuid: 26c62a79-ed14-49db-9805-1a55e4b8ef82
updated: 1484310218
title: upwards
categories:
    - Dictionary
---
upwards
     adv 1: spatially or metaphorically from a lower to a higher
            position; "look up!"; "the music surged up"; "the
            fragments flew upwards"; "prices soared upwards";
            "upwardly mobile" [syn: {up}, {upward}, {upwardly}]
            [ant: {down}, {down}, {down}, {down}]
     2: to a later time; "they moved the meeting date up"; "from
        childhood upward" [syn: {up}, {upward}]
