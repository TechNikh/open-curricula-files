---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C5%A0varc%E2%80%93Milnor_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 3dced52b-1c4d-4f78-912b-5bb9ee91a635
updated: 1484309272
title: Švarc–Milnor lemma
tags:
    - Precise statement
    - Notes
    - Explanation of the terms
    - Examples of applications of the Švarc–Milnor lemma
categories:
    - Metric geometry
---
In the mathematical subject of geometric group theory, the Švarc–Milnor lemma (sometimes also called Milnor–Švarc lemma, with both variants also sometimes spelling Švarc as Schwarz) is a statement which says that a group 
  
    
      
        G
      
    
    {\displaystyle G}
  
, equipped with a "nice" discrete isometric action on a metric space 
  
    
      
        X
      
    
    {\displaystyle X}
  
, is quasi-isometric to 
  
    
      
        X
      
    
    {\displaystyle X}
  
.
