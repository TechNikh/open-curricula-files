---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guide
offline_file: ""
offline_thumbnail: ""
uuid: c6335659-c224-43d7-a7b0-d837a609d5e1
updated: 1484310421
title: guide
categories:
    - Dictionary
---
guide
     n 1: someone employed to conduct others [syn: {usher}]
     2: someone who shows the way by leading or advising
     3: something that offers basic information or instruction [syn:
         {guidebook}]
     4: a model or standard for making comparisons [syn: {template},
         {templet}]
     5: someone who can find paths through unexplored territory
        [syn: {scout}, {pathfinder}]
     v 1: direct the course; determine the direction of travelling
          [syn: {steer}, {maneuver}, {manoeuver}, {manoeuvre}, {direct},
           {point}, {head}, {channelize}, {channelise}]
     2: take somebody somewhere; "We lead him to our chief"; "can
        you take me to the main ...
