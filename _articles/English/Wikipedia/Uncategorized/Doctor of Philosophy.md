---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phd
offline_file: ""
offline_thumbnail: ""
uuid: 5c5b60e3-9929-42d4-a769-9edf694fa8bd
updated: 1484308413
title: Doctor of Philosophy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dphil_gown.jpg
tags:
    - Terminology
    - History
    - Medieval and early modern Europe
    - Educational reforms in Germany
    - United Kingdom
    - United States
    - Requirements
    - PhD confirmation
    - Value and criticism
    - National variations
    - Degrees around the globe
    - Argentina
    - Admission
    - Funding
    - Requirements for completion
    - Australia
    - Admission
    - Scholarships
    - Fees
    - Requirements for completion
    - Canada
    - Admission
    - Funding
    - Requirements for completion
    - Colombia
    - Admission
    - Funding
    - Requirements for completion
    - Finland
    - France
    - History
    - Admission
    - Funding
    - India
    - Admission
    - Germany
    - Admission
    - Structure
    - Duration
    - USSR, Russian Federation and former Soviet Republics
    - Italy
    - History
    - Admission
    - Nepal
    - Poland
    - Ukraine
    - Scandinavia
    - Spain
    - United Kingdom
    - Admission
    - Funding
    - Completion
    - Other doctorates
    - United States
    - Models of supervision
    - International PhD equivalent degrees
    - Notes and references
    - Bibliography
categories:
    - Uncategorized
---
A Doctor of Philosophy (PhD or DPhil; Latin Philosophiae Doctor or Doctor Philosophiae) is a type of doctorate degree awarded by universities in many countries. Ph.D.s are awarded for a wide range of programs in the sciences (e.g., biology, chemistry, physics, mathematics, etc.), engineering, and humanities (e.g., history, literature, musicology, etc.), among others. The Ph.D. is a terminal degree in many fields. The completion of a Ph.D. is often a requirement for employment as a university professor, researcher, or scientist in many fields. A clear distinction is made between an "earned doctorate", which is awarded for completion of a course of study and a thesis or dissertation, and an ...
