---
version: 1
type: video
provider: YouTube
id: O-48znAg7VE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Solving%20the%20puzzle%20of%20the%20periodic%20table%20-%20Eric%20Rosado.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5bf250da-43a6-4633-9eec-1acda999a674
updated: 1486069664
title: 'Solving the puzzle of the periodic table - Eric Rosado'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/O-48znAg7VE/default.jpg
    - https://i3.ytimg.com/vi/O-48znAg7VE/1.jpg
    - https://i3.ytimg.com/vi/O-48znAg7VE/2.jpg
    - https://i3.ytimg.com/vi/O-48znAg7VE/3.jpg
---
