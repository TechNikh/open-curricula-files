---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negative_frequency
offline_file: ""
offline_thumbnail: ""
uuid: e33ff550-9ab2-4034-a20e-5a0aae8f372d
updated: 1484308894
title: Negative frequency
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Negative_frequency.svg.png
tags:
    - Sinusoids
    - Applications
    - Sampling of positive and negative frequencies and aliasing
    - Notes
categories:
    - Waves
---
The concept of negative and positive frequency can be as simple as a wheel rotating one way or the other way: a signed value of frequency can indicate both the rate and direction of rotation. The rate is expressed in units such as revolutions (aka cycles) per second (hertz) or radian/second (where 1 cycle corresponds to 2π radians).
