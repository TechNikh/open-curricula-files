---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centrosymmetry
offline_file: ""
offline_thumbnail: ""
uuid: 9fd7ec0c-f542-4333-8784-d495f246acf4
updated: 1484309305
title: Centrosymmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Centrosymmetry-_Benzene.png
categories:
    - Symmetry
---
In crystallography, a point group which contains an inversion center as one of its symmetry elements is centrosymmetric.[1] In such a point group, for every point (x, y, z) in the unit cell there is an indistinguishable point (-x, -y, -z). Such point groups are also said to have inversion .[2] Point reflection is a similar term used in geometry. Crystals with an inversion center cannot display certain properties, such as the piezoelectric effect.
