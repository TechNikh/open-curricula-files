---
version: 1
type: video
provider: YouTube
id: DZKFZ9nCOhk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dac15031-3741-4bcf-b950-a296f53d39c4
updated: 1486069653
title: What is Theory of Relativity Part 3
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DZKFZ9nCOhk/default.jpg
    - https://i3.ytimg.com/vi/DZKFZ9nCOhk/1.jpg
    - https://i3.ytimg.com/vi/DZKFZ9nCOhk/2.jpg
    - https://i3.ytimg.com/vi/DZKFZ9nCOhk/3.jpg
---
