---
version: 1
type: video
provider: YouTube
id: -7aJgc68Y5g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5de022e1-3ae0-48d0-b19d-c529a9995ac7
updated: 1486069614
title: Determining a translation for a shape
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/-7aJgc68Y5g/default.jpg
    - https://i3.ytimg.com/vi/-7aJgc68Y5g/1.jpg
    - https://i3.ytimg.com/vi/-7aJgc68Y5g/2.jpg
    - https://i3.ytimg.com/vi/-7aJgc68Y5g/3.jpg
---
