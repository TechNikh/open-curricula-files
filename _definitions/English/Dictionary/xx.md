---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/xx
offline_file: ""
offline_thumbnail: ""
uuid: 7b2e5c22-3689-4972-9196-5994831d3eb2
updated: 1484310295
title: xx
categories:
    - Dictionary
---
xx
     adj : denoting a quantity consisting of 20 items or units [syn: {twenty},
            {20}]
     n 1: the cardinal number that is the sum of nineteen and one
          [syn: {twenty}, {20}]
     2: (genetics) normal complement of sex chromosomes in a female
