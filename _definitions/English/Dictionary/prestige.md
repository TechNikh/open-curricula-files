---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prestige
offline_file: ""
offline_thumbnail: ""
uuid: f651e895-5e89-4898-9c38-1d075d2c04da
updated: 1484310569
title: prestige
categories:
    - Dictionary
---
prestige
     n : a high standing achieved through success or influence or
         wealth etc.; "he wanted to achieve power and prestige"
         [syn: {prestigiousness}]
