---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evening
offline_file: ""
offline_thumbnail: ""
uuid: 14b4734b-203c-4eee-ba5d-13b27cdf6b12
updated: 1484310455
title: evening
categories:
    - Dictionary
---
evening
     n 1: the latter part of the day (the period of decreasing
          daylight from late afternoon until nightfall); "he
          enjoyed the evening light across the lake" [syn: {eve},
          {eventide}]
     2: a later concluding time period; "it was the evening of the
        Roman Empire"
     3: the early part of night (from dinner until bedtime) spent in
        a special way; "an evening at the opera"
