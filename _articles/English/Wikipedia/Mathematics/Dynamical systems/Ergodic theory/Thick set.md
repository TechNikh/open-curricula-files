---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thick_set
offline_file: ""
offline_thumbnail: ""
uuid: fef1e5b1-b896-4eb0-a4be-f22540401474
updated: 1484308775
title: Thick set
categories:
    - Ergodic theory
---
In mathematics, a thick set is a set of integers that contains arbitrarily long intervals. That is, given a thick set 
  
    
      
        T
      
    
    {\displaystyle T}
  
, for every 
  
    
      
        p
        ∈
        
          N
        
      
    
    {\displaystyle p\in \mathbb {N} }
  
, there is some 
  
    
      
        n
        ∈
        
          N
        
      
    
    {\displaystyle n\in \mathbb {N} }
  
 such that 
  
    
      
        {
        n
        ,
        n
        +
        1
        ,
        n
        +
        2
        ,
        .
        .
        .
        ,
        n
        +
        p
        }
        ⊂
        T
      
   ...
