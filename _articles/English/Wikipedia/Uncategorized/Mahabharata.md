---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mahabharata
offline_file: ""
offline_thumbnail: ""
uuid: 90c29e75-3118-459c-8843-5007550044ab
updated: 1484308415
title: Mahabharata
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Krishna_and_Arjun_on_the_chariot%252C_Mahabharata%252C_18th-19th_century%252C_India.jpg'
tags:
    - Textual history and structure
    - Accretion and redaction
    - Historical references
    - The 18 parvas or books
    - Historical context
    - Synopsis
    - The older generations
    - The Pandava and Kaurava princes
    - Lakshagraha (the house of lac)
    - Marriage to Draupadi
    - Indraprastha
    - The dice game
    - Exile and return
    - The battle at Kurukshetra
    - The end of the Pandavas
    - The reunion
    - Themes
    - Just war
    - Versions, translations, and derivative works
    - Critical Edition
    - Regional versions
    - Translations
    - Derivative literature
    - In film and television
    - Jain version
    - Kuru family tree
    - Cultural influence
    - Editions
    - Sources
categories:
    - Uncategorized
---
The Mahabharata or Mahābhārata (US /məhɑːˈbɑːrətə/;[1] UK /ˌmɑːhəˈbɑːrətə/;[2] Sanskrit: महाभारतम्, Mahābhāratam, pronounced [məɦaːˈbʱaːrət̪əm]) is one of the two major Sanskrit epics of ancient India, the other being the Ramayana.[3]
