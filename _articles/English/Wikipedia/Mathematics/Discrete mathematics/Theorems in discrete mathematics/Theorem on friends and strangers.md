---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Theorem_on_friends_and_strangers
offline_file: ""
offline_thumbnail: ""
uuid: 772521d8-8363-4c0b-9260-e833036dd4a9
updated: 1484308667
title: Theorem on friends and strangers
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Friends_strangers_graph.gif
tags:
    - Statement
    - Conversion to a graph-theoretic setting
    - Proof
    - "Ramsey's paper"
    - Boundaries to the theorem
categories:
    - Theorems in discrete mathematics
---
