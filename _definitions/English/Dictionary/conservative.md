---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conservative
offline_file: ""
offline_thumbnail: ""
uuid: 40f7472c-e875-4f4c-b079-0ae27760a4e5
updated: 1484310571
title: conservative
categories:
    - Dictionary
---
conservative
     adj 1: resistant to change [ant: {liberal}]
     2: opposed to liberal reforms
     3: avoiding excess; "a conservative estimate" [syn: {cautious}]
     4: unimaginatively conventional; "a colorful character in the
        buttoned-down, dull-gray world of business"- Newsweek
        [syn: {button-down}, {buttoned-down}]
     5: conforming to the standards and conventions of the middle
        class; "a bourgeois mentality" [syn: {bourgeois}, {materialistic}]
     n : a person who has conservative ideas or opinions [syn: {conservativist}]
         [ant: {liberal}]
