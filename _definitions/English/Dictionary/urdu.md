---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urdu
offline_file: ""
offline_thumbnail: ""
uuid: 48103b89-e696-4b91-a7c9-2a01b741df59
updated: 1484310585
title: urdu
categories:
    - Dictionary
---
Urdu
     adj : relating to the Urdu language; "Urdu poetry"
     n : the official literary language of Pakistan, closely related
         to Hindi; widely used in India (mostly by Moslems);
         written in Arabic script
