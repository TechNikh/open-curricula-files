---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fuller
offline_file: ""
offline_thumbnail: ""
uuid: 3020ddf5-2dd3-486c-8b73-6e40764796c8
updated: 1484310414
title: fuller
categories:
    - Dictionary
---
Fuller
     n 1: United States architect who invented the geodesic dome
          (1895-1983) [syn: {Buckminster Fuller}, {R. Buckminster
          Fuller}, {Richard Buckminster Fuller}]
     2: a workman who fulls (cleans and thickens) freshly woven
        cloth for a living
