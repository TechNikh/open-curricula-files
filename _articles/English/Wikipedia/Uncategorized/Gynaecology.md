---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gyno
offline_file: ""
offline_thumbnail: ""
uuid: 5a31a154-d82f-4a8e-ba6d-66c52781a254
updated: 1484308349
title: Gynaecology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Dilating_vaginal_speculum_inflating_vagina_and_light_illuminating.jpg
tags:
    - Etymology
    - History
    - Examination
    - Diseases
    - Therapies
    - Specialist training
    - Gender of physicians
categories:
    - Uncategorized
---
Gynaecology or gynecology (see spelling differences) is the medical practice dealing with the health of the female reproductive systems (vagina, uterus, and ovaries) and the breasts. Literally, outside medicine, the term means "the science of women". Its counterpart is andrology, which deals with medical issues specific to the male reproductive system.
