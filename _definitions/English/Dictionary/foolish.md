---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foolish
offline_file: ""
offline_thumbnail: ""
uuid: d8b3e9ae-ac41-44f3-b61e-72169498bbd6
updated: 1484310148
title: foolish
categories:
    - Dictionary
---
foolish
     adj 1: devoid of good sense or judgment; "foolish remarks"; "a
            foolish decision" [ant: {wise}]
     2: having or revealing stupidity; "ridiculous anserine
        behavior"; "a dopey answer"; "a dopey kid"; "some fool
        idea about rewriting authors' books" [syn: {anserine}, {dopy},
         {dopey}, {goosey}, {goosy}, {gooselike}]
