---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formal_methods
offline_file: ""
offline_thumbnail: ""
uuid: 21d1bea8-acc2-4ee3-80d3-2d3ab3d69daa
updated: 1484308675
title: Formal methods
tags:
    - Taxonomy
    - Lightweight formal methods
    - Uses
    - Specification
    - Development
    - Verification
    - Human-directed proof
    - Automated proof
    - Applications
    - Formal methods and notations
categories:
    - Theoretical computer science
---
In computer science, specifically software engineering and hardware engineering, formal methods are a particular kind of mathematically based techniques for the specification, development and verification of software and hardware systems.[1] The use of formal methods for software and hardware design is motivated by the expectation that, as in other engineering disciplines, performing appropriate mathematical analysis can contribute to the reliability and robustness of a design.[2]
