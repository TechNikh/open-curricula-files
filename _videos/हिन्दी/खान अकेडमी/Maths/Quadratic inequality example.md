---
version: 1
type: video
provider: YouTube
id: pVRiO7YyloU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f9a9cba7-5291-4828-9d84-d3b9a820b1bd
updated: 1486069613
title: Quadratic inequality example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/pVRiO7YyloU/default.jpg
    - https://i3.ytimg.com/vi/pVRiO7YyloU/1.jpg
    - https://i3.ytimg.com/vi/pVRiO7YyloU/2.jpg
    - https://i3.ytimg.com/vi/pVRiO7YyloU/3.jpg
---
