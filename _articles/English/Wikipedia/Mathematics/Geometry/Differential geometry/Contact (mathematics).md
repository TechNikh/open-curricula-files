---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Contact_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 4fef1a9c-3e15-4f86-9e18-0186a738dc38
updated: 1484309207
title: Contact (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-TangentCircle.png
tags:
    - Contact between curves
    - Contact between a curve and a circle
    - Bi-tangents in econometrics
categories:
    - Differential geometry
---
In mathematics, two functions have a contact of order k if, at a point P, they have the same value and k equal derivatives. This is an equivalence relation, whose equivalence classes are generally called jets. The point of osculation is also called the double cusp.
