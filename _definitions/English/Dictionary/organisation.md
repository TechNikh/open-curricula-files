---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organisation
offline_file: ""
offline_thumbnail: ""
uuid: d2ac23c2-8c95-4cdf-8ddf-a2f7be672a19
updated: 1484310254
title: organisation
categories:
    - Dictionary
---
organisation
     n 1: an organized structure for arranging or classifying; "he
          changed the arrangement of the topics"; "the facts were
          familiar but it was in the organization of them that he
          was original"; "he tried to understand their system of
          classification" [syn: {arrangement}, {organization}, {system}]
     2: the persons (or committees or departments etc.) who make up
        a body for the purpose of administering something; "he
        claims that the present administration is corrupt"; "the
        governance of an association is responsible to its
        members"; "he quickly became recognized as a member of the
        establishment" ...
