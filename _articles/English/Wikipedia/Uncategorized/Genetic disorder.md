---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Genetic_disorder
offline_file: ""
offline_thumbnail: ""
uuid: a4ca02f5-80a2-4e1d-9d38-bf95c6a7669f
updated: 1484308371
title: Genetic disorder
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Drill.jpg
tags:
    - Single-gene
    - Autosomal dominant
    - Autosomal recessive
    - X-linked dominant
    - X-linked recessive
    - Y-linked
    - Mitochondrial
    - Many genes
    - Diagnosis
    - Prognosis
    - Treatment
categories:
    - Uncategorized
---
A genetic disorder is a genetic problem caused by one or more abnormalities in the genome, especially a condition that is present from birth (congenital). Most genetic disorders are quite rare and affect one person in every several thousands or millions.
