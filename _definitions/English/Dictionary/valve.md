---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valve
offline_file: ""
offline_thumbnail: ""
uuid: 6dd7b7d5-b316-4418-9d31-ac1c1f4c86f7
updated: 1484310327
title: valve
categories:
    - Dictionary
---
valve
     n 1: a structure in a hollow organ (like the heart) with a flap
          to insure one-way flow of fluid through it
     2: device in a brass wind instrument for varying the length of
        the air column to alter the pitch of a tone
     3: control consisting of a mechanical device for controlling
        the flow of a fluid
