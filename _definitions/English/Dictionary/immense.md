---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immense
offline_file: ""
offline_thumbnail: ""
uuid: 5831079e-bd05-4108-b21a-cac6ced432d9
updated: 1484310431
title: immense
categories:
    - Dictionary
---
immense
     adj : unusually great in size or amount or degree or especially
           extent or scope; "huge government spending"; "huge
           country estates"; "huge popular demand for higher
           education"; "a huge wave"; "the Los Angeles aqueduct
           winds like an immense snake along the base of the
           mountains"; "immense numbers of birds"; "at vast (or
           immense) expense"; "the vast reaches of outer space";
           "the vast accumulation of knowledge...which we call
           civilization"- W.R.Inge [syn: {huge}, {vast}, {Brobdingnagian}]
