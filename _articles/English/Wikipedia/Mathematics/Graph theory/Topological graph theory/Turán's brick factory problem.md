---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tur%C3%A1n%27s_brick_factory_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 92d67ac3-00dc-40e1-93e2-13fc739f8366
updated: 1484309481
title: "Turán's brick factory problem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Zarankiewicz_K4%252C7.svg.png'
tags:
    - Upper bound
    - Lower bounds
    - Rectilinear crossing numbers
categories:
    - Topological graph theory
---
In the mathematics of graph drawing, Turán's brick factory problem asks for the minimum number of crossings in a drawing of the complete bipartite graph Km,n.[1] More precisely, the graph should be drawn in the plane with each vertex as a point, each edge as a curve connecting its two endpoints, and no vertex placed on an edge that it is not incident to. A crossing is counted whenever two edges that are disjoint in the graph have a nonempty intersection in the plane.
