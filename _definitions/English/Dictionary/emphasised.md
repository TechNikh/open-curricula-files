---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphasised
offline_file: ""
offline_thumbnail: ""
uuid: 76a2e138-aead-4262-ae4e-5c6b22861446
updated: 1484310597
title: emphasised
categories:
    - Dictionary
---
emphasised
     adj : spoken with emphasis; "an emphatic word" [syn: {emphatic}, {emphasized}]
