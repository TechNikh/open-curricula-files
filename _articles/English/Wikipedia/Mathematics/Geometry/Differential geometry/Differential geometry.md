---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 686a0edc-3e13-4886-a068-a279e0e4f62f
updated: 1484309202
title: Differential geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/235px-Hyperbolic_triangle.svg.png
tags:
    - History of Development
    - Branches of differential geometry
    - Riemannian geometry
    - Pseudo-Riemannian geometry
    - Finsler geometry
    - Symplectic geometry
    - Contact geometry
    - Complex and Kähler geometry
    - CR geometry
    - Differential topology
    - Lie groups
    - Bundles and connections
    - Intrinsic versus extrinsic
    - Applications
categories:
    - Differential geometry
---
Differential geometry is a mathematical discipline that uses the techniques of differential calculus, integral calculus, linear algebra and multilinear algebra to study problems in geometry. The theory of plane and space curves and surfaces in the three-dimensional Euclidean space formed the basis for development of differential geometry during the 18th century and the 19th century.
