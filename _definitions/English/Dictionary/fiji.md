---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fiji
offline_file: ""
offline_thumbnail: ""
uuid: c1e611d8-f529-4db3-bf6b-517869e21b66
updated: 1484310525
title: fiji
categories:
    - Dictionary
---
Fiji
     n : an independent state within the British Commonwealth located
         on the Fiji Islands [syn: {Republic of Fiji}]
