---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pant
offline_file: ""
offline_thumbnail: ""
uuid: 92cb1d29-649c-4b85-97df-6b2e3de6004a
updated: 1484310226
title: pant
categories:
    - Dictionary
---
pant
     n 1: the noise made by a short puff of steam (as from an engine)
     2: a short labored intake of breath with the mouth open; "she
        gave a gasp and fainted" [syn: {gasp}]
     v 1: breathe noisily, as when one is exhausted; "The runners
          reached the finish line, panting heavily" [syn: {puff},
          {gasp}, {heave}]
     2: utter while panting, as if out of breath
