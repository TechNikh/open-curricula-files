---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wrap
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406121
title: wrap
categories:
    - Dictionary
---
wrap
     n 1: cloak that is folded or wrapped around a person [syn: {wrapper}]
     2: a sandwich in which the filling is rolled up in a soft
        tortilla
     3: the covering (usually paper or cellophane) in which
        something is wrapped [syn: {wrapping}, {wrapper}]
     v 1: arrange or fold as a cover or protection; "wrap the baby
          before taking her out"; "Wrap the present" [syn: {wrap
          up}] [ant: {unwrap}]
     2: wrap or coil around; "roll your hair around your finger";
        "Twine the thread around the spool" [syn: {wind}, {roll},
        {twine}] [ant: {unwind}]
     3: enclose or enfold completely with or as if with a covering;
        "Fog enveloped ...
