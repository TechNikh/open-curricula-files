---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vicinal_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: bda9788e-1690-4dfe-af81-6cf5ad5dc62a
updated: 1484309285
title: Vicinal (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Geminal_example.png
categories:
    - Molecular geometry
---
In chemistry vicinal (from Latin vicinus = neighbor), abbreviated vic, describes any two functional groups bonded to two adjacent carbon atoms (i.e., in a 1,2-relationship). For example, the molecule 2,3-dibromobutane carries two vicinal bromine atoms and 1,3-dibromobutane does not.
