---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Networks_in_labor_economics
offline_file: ""
offline_thumbnail: ""
uuid: fc53f16a-c95a-4aae-9324-c0ca1d0f7995
updated: 1484309440
title: Networks in labor economics
tags:
    - The model of Calvo-Armegnol and Jackson
    - The model
    - Implications
    - Referral based job search
categories:
    - Network theory
---
The importance of social ties in job searching is known, and empirically proved for quite a while, workers often find jobs through their friends and relatives. However, the exploration of the role of social networks in labor market outcomes has just recently started. New evidence shows that social networks not only increase the productivity of job searching but partly explain wage differences, and help decreasing the information asymmetry between the employer and employee.
