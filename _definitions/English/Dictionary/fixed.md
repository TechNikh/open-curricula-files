---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fixed
offline_file: ""
offline_thumbnail: ""
uuid: e8b69302-fa13-47f3-8bdb-60b5890f8320
updated: 1484310258
title: fixed
categories:
    - Dictionary
---
fixed
     adj 1: (of a number) having a fixed and unchanging value
     2: fixed and unmoving; "with eyes set in a fixed glassy stare";
        "his bearded face already has a set hollow look"- Connor
        Cruise O'Brien; "a face rigid with pain" [syn: {set}, {rigid}]
     3: securely placed or fastened or set; "a fixed piece of wood";
        "a fixed resistor" [ant: {unfixed}]
     4: intent and directed steadily; "had her gaze fastened on the
        stranger"; "a fixed expresson" [syn: {fastened}]
     5: incapable of being changed or moved or undone; e.g. "frozen
        prices"; "living on fixed incomes" [syn: {frozen}]
     6: mended or put in working order; "a reconditioned ...
