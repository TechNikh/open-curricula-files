---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Teath
offline_file: ""
offline_thumbnail: ""
uuid: f8ca50ec-a525-4dde-8e7f-4bd1090f82ef
updated: 1484308351
title: Saint Tetha
tags:
    - Name and identity
    - life
    - Legacy
categories:
    - Uncategorized
---
Saint Tetha (Cornish: Tedha; Welsh: Tedda), also known as Saint Teath (/tɛθ/),[1][2] Saint Tecla,[3][4] and by a variety of other names,[5] was a 5th-century virgin and saint in Wales and Cornwall. She is associated with the parish church of St Teath in Cornwall. Baring-Gould gives her feast day as 27 October,[3] but this has been called a mistaken conflation with Saint Ia.[4] In 1878, it was held on the movable feast of Whit Tuesday.[5] Other sources place it on 1 May,[6] 6 September,[7][8] and (mistakenly) 15 January.[7] It is no longer observed by either the Anglican[9] or Catholic church in Wales.[10]
