---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blown
offline_file: ""
offline_thumbnail: ""
uuid: b38b73b0-480e-46c0-a3dd-85826f688cf6
updated: 1484310228
title: blown
categories:
    - Dictionary
---
blow
     n 1: a powerful stroke with the fist or a weapon; "a blow on the
          head"
     2: an impact (as from a collision); "the bump threw him off the
        bicycle" [syn: {bump}]
     3: an unfortunate happening that hinders of impedes; something
        that is thwarting or frustrating [syn: {reverse}, {reversal},
         {setback}, {black eye}]
     4: an unpleasant or disappointing surprise; "it came as a shock
        to learn that he was injured" [syn: {shock}]
     5: a strong current of air; "the tree was bent almost double by
        the gust" [syn: {gust}, {blast}]
     6: street names for cocaine [syn: {coke}, {nose candy}, {snow},
         {C}]
     7: forceful ...
