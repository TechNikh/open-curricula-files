---
version: 1
type: video
provider: YouTube
id: EftSTybaU1E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2df95c80-ba93-455a-823a-5e1d15a27cf9
updated: 1486069619
title: Secant (sec), cosecant (csc) and cotangent (cot) example
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/EftSTybaU1E/default.jpg
    - https://i3.ytimg.com/vi/EftSTybaU1E/1.jpg
    - https://i3.ytimg.com/vi/EftSTybaU1E/2.jpg
    - https://i3.ytimg.com/vi/EftSTybaU1E/3.jpg
---
