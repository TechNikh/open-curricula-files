---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperplane_section
offline_file: ""
offline_thumbnail: ""
uuid: b6ddbb81-4af5-476d-a363-f9a945ae42ba
updated: 1484309159
title: Hyperplane section
categories:
    - Algebraic geometry
---
In mathematics, a hyperplane section of a subset X of projective space Pn is the intersection of X with some hyperplane H. In other words, we look at the subset XH of those elements x of X that satisfy the single linear condition L = 0 defining H as a linear subspace. Here L or H can range over the dual projective space of non-zero linear forms in the homogeneous coordinates, up to scalar multiplication.
