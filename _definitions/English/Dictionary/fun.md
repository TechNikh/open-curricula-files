---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fun
offline_file: ""
offline_thumbnail: ""
uuid: 82764297-3d51-4e7d-91b4-00bf661a24fb
updated: 1484310220
title: fun
categories:
    - Dictionary
---
fun
     adj : providing enjoyment; pleasantly entertaining; "an amusing
           speaker"; "a diverting story"; "a fun thing to do"
           [syn: {amusing}, {amusive}, {diverting}, {fun(a)}]
     n 1: activities that are enjoyable or amusing; "I do it for the
          fun of it"; "he is fun to have around" [syn: {merriment},
           {playfulness}]
     2: verbal wit (often at another's expense but not to be taken
        seriously); "he became a figure of fun" [syn: {play}, {sport}]
     3: violent and excited activity; "she asked for money and then
        the fun began"; "they began to fight like fun"
     4: a disposition to find (or make) causes for amusement; "her
        ...
