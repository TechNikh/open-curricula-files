---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reportedly
offline_file: ""
offline_thumbnail: ""
uuid: 5efb71ac-e626-4658-8c71-e6290a59d82a
updated: 1484310591
title: reportedly
categories:
    - Dictionary
---
reportedly
     adv : according to reports or other information; "she was
           reportedly his mistress for many years"
