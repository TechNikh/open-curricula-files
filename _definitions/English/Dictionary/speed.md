---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speed
offline_file: ""
offline_thumbnail: ""
uuid: 9c344f56-c785-4637-8b41-5e5d4da9a452
updated: 1484310232
title: speed
categories:
    - Dictionary
---
speed
     n 1: distance travelled per unit time [syn: {velocity}]
     2: a rate (usually rapid) at which something happens; "the
        project advanced with gratifying speed" [syn: {swiftness},
         {fastness}]
     3: changing location rapidly [syn: {speeding}, {hurrying}]
     4: the ratio of the focal length to the diameter of a (camera)
        lens system [syn: {focal ratio}, {f number}, {stop number}]
     5: a central nervous system stimulant that increases energy and
        decreases appetite; used to treat narcolepsy and some
        forms of depression [syn: {amphetamine}, {pep pill}, {upper}]
     v 1: step on it; "He rushed down the hall to receive his guests";
         ...
