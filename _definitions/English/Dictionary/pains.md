---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pains
offline_file: ""
offline_thumbnail: ""
uuid: c8fc5eaf-d392-48df-a9af-69a5d088bf4a
updated: 1484310148
title: pains
categories:
    - Dictionary
---
pains
     n : an effortful attempt to attain a goal [syn: {striving}, {nisus},
          {strain}]
