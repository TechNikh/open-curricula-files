---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/southernmost
offline_file: ""
offline_thumbnail: ""
uuid: e307944f-8fbd-4412-95cb-c8945ccec88c
updated: 1484310437
title: southernmost
categories:
    - Dictionary
---
southernmost
     adj : situated farthest south; "Key West is the southernmost city
           in the United States" [syn: {southmost}]
