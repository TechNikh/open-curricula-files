---
version: 1
type: video
provider: YouTube
id: umvNQj-zmq4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Are%20southern%20hemisphere%20seasons%20more%20severe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0442fca1-ff01-455c-818f-93ca8904cd78
updated: 1486069685
title: Are southern hemisphere seasons more severe?
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/umvNQj-zmq4/default.jpg
    - https://i3.ytimg.com/vi/umvNQj-zmq4/1.jpg
    - https://i3.ytimg.com/vi/umvNQj-zmq4/2.jpg
    - https://i3.ytimg.com/vi/umvNQj-zmq4/3.jpg
---
