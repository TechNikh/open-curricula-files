---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curiosity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484568301
title: curiosity
categories:
    - Dictionary
---
curiosity
     n 1: a state in which you want to learn more about something
          [syn: {wonder}]
     2: something unusual -- perhaps worthy of collecting [syn: {curio},
         {oddity}, {oddment}, {peculiarity}, {rarity}]
