---
version: 1
type: video
provider: YouTube
id: NSEzg6TBheY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 792e3f9e-c345-44c4-aad3-1a724f155176
updated: 1486069669
title: 'How Do We Breathe Animation - How Do The Lungs Work Video - Respiratory System- Process of Breathing'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/NSEzg6TBheY/default.jpg
    - https://i3.ytimg.com/vi/NSEzg6TBheY/1.jpg
    - https://i3.ytimg.com/vi/NSEzg6TBheY/2.jpg
    - https://i3.ytimg.com/vi/NSEzg6TBheY/3.jpg
---
