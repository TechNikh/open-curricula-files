---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimulate
offline_file: ""
offline_thumbnail: ""
uuid: 0df801cb-4d97-48b3-93d4-7786ec03e361
updated: 1484310333
title: stimulate
categories:
    - Dictionary
---
stimulate
     v 1: act as a stimulant; "The book stimulated her imagination";
          "This play stimulates" [syn: {excite}] [ant: {stifle}]
     2: cause to do; cause to act in a specified manner; "The ads
        induced me to buy a VCR"; "My children finally got me to
        buy a computer"; "My wife made me buy a new sofa" [syn: {induce},
         {cause}, {have}, {get}, {make}]
     3: stir the feelings, emotions, or peace of; "These stories
        shook the community"; "the civil war shook the country"
        [syn: {shake}, {shake up}, {excite}, {stir}]
     4: cause to be alert and energetic; "Coffee and tea stimulate
        me"; "This herbal infusion doesn't stimulate" [syn: ...
