---
version: 1
type: video
provider: YouTube
id: iulx0z1lz8M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Example%201-%20Using%20the%20quadratic%20formula.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d05aa476-6972-4582-93b0-78012bf43c0c
updated: 1486069563
title: 'Example 1- Using the quadratic formula'
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/iulx0z1lz8M/default.jpg
    - https://i3.ytimg.com/vi/iulx0z1lz8M/1.jpg
    - https://i3.ytimg.com/vi/iulx0z1lz8M/2.jpg
    - https://i3.ytimg.com/vi/iulx0z1lz8M/3.jpg
---
