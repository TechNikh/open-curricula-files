---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Covariant_Hamiltonian_field_theory
offline_file: ""
offline_thumbnail: ""
uuid: 02e4f91f-3fd7-4164-9ba6-639388979b8c
updated: 1484309215
title: Covariant Hamiltonian field theory
categories:
    - Differential geometry
---
In mathematical physics, Hamiltonian field theory usually means the symplectic Hamiltonian formalism when applied to classical field theory, that takes the form of the instantaneous Hamiltonian formalism on an infinite-dimensional phase space, and where canonical coordinates are field functions at some instant of time.[1] This Hamiltonian formalism is applied to quantization of fields, e.g., in quantum gauge theory.
