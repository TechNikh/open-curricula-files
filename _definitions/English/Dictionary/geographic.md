---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geographic
offline_file: ""
offline_thumbnail: ""
uuid: c6fd07d2-ed46-41d5-a104-65fd55c6d5d9
updated: 1484310194
title: geographic
categories:
    - Dictionary
---
geographic
     adj 1: of or relating to the science of geography [syn: {geographical}]
     2: determined by geography; "the north and south geographic
        poles" [syn: {geographical}] [ant: {magnetic}]
