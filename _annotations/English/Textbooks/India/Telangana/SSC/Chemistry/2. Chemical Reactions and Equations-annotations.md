---
version: 1
type: annotation
id: 'http://eschool2go.org/sites/default/files/chapter_2%20Chemical%20Reactions%20and%20Equations.pdf'
uuid: 0caea1b1-a62c-411a-94e6-09881f0b8b50
updated: "1483306201"
annotations:
    - 
        quote: dil
        id: https://en.wikipedia.org/wiki/Dil
        uuid: c621e82d-36af-43b9-ba9a-ad3033e3e7e0
        text: Dil
        comment: 'Dil (translation: Heart) is a 1990 Indian Hindi romantic drama film starring Madhuri Dixit, Aamir Khan, Anupam Kher and Saeed Jaffrey. It was directed by Indra Kumar with music composed by Anand-Milind. Dixit received the Filmfare Award for Best Actress for her performance. The film was remade in Telugu in 1993 under the title Tholi Muddhu, starring Divya Bharti and Prashanth; it was also remade in Bengali (Bangladesh) in 1997 under the title Amar Ghor Amar Beheshto (my home my heaven). The film was also remade in Kannada as Shivaranjini.'
    - 
        quote: cao
        id: https://en.wikipedia.org/wiki/Cao
        uuid: df5c805f-dccc-4f05-8b9c-0798af422952
        text: Cao
        comment: ""
    - 
        quote: baso
        id: https://en.wikipedia.org/wiki/Baso
        uuid: 5959bcb9-4096-4f5e-943a-386cce06fa95
        text: Mazu Daoyi
        comment: |
            Mazu Daoyi (709–788) (Chinese: 馬祖道一; pinyin: Mǎzŭ Dàoyī; Wade–Giles: Ma-tsu Tao-yi, Japanese: Baso Dōitsu) was an influential abbot of Chan Buddhism during the Tang dynasty. The earliest recorded use of the term "Chan school" is from his Extensive Records.[1] Master Ma's teaching style of "strange words and extraordinary actions"[2] became paradigmatic Zen lore.
    - 
        quote: nacl
        id: https://en.wikipedia.org/wiki/Nacl
        uuid: 84759188-244d-4645-b907-ebc772c52bf8
        text: Sodium chloride
        comment: 'Sodium chloride /ˌsoʊdiəm ˈklɔːraɪd/,[2] also known as salt or halite, is an ionic compound with the chemical formula NaCl, representing a 1:1 ratio of sodium and chloride ions. Sodium chloride is the salt most responsible for the salinity of seawater and of the extracellular fluid of many multicellular organisms. In the form of edible or table salt it is commonly used as a condiment and food preservative. Large quantities of sodium chloride are used in many industrial processes, and it is a major source of sodium and chlorine compounds used as feedstocks for further chemical syntheses. A second major application of sodium chloride is de-icing of roadways in sub-freezing weather.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/270px-WatNaCl.png
            uuid: d9db713d-fbd2-4dc2-8f9f-9230870701cf
            comment: Phase diagram of water-NaCl mixture.
    - 
        quote: rhs
        id: https://en.wikipedia.org/wiki/Rhs
        uuid: 7c124f94-b117-433f-8a93-8154f5551b1a
        text: RHS
        comment: 'RHS may stand for:'
    - 
        quote: inco
        id: https://en.wikipedia.org/wiki/Inco
        uuid: 1181d89b-ab45-4283-a40d-5fe45a7154dd
        text: Vale Limited
        comment: |
            Vale Canada Limited (formerly Vale Inco, CVRD Inco and Inco Limited; for corporate branding purposes simply known as "Vale" and pronounced /'vɑːleɪ/ in English)[1] is a wholly owned subsidiary of the Brazilian mining company Vale. Vale's nickel mining and metals division is headquartered in Toronto, Ontario, Canada. It produces nickel, copper, cobalt, platinum, rhodium, ruthenium, iridium, gold, and silver. Prior to being purchased by CVRD (now Vale) in 2006, Inco was the world's second largest producer of nickel, and the third largest mining company outside South Africa and Russia of platinum group metals. It was also a charter member of the 30-stock Dow Jones Industrial Average formed ...
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Logo_Inco.png
            uuid: e7d61362-507d-4d71-a61d-17f7c51b60cb
            comment: "The logo of Inco Limited and CVRD Inco, prior to CVRD's rebranding as Vale on November 29, 2007"
    - 
        quote: inh
        id: https://en.wikipedia.org/wiki/Inh
        uuid: dc556549-259c-42c9-97cb-97efbf5eb19a
        text: INH
        comment: 'INH or inh may refer to:'
    - 
        quote: ino
        id: https://en.wikipedia.org/wiki/Ino
        uuid: 0846c04e-60ec-4a4a-9a87-ef1478d21134
        text: Ino
        comment: 'Ino or INO may refer to:'
    - 
        quote: whole number
        id: https://en.wikipedia.org/wiki/Whole_number
        uuid: 96120299-56f7-4edb-b8a5-c79c768d5fdc
        text: Natural number
        comment: >
            In mathematics, the natural numbers are those used for
            counting (as in "there are six coins on the table") and
            ordering (as in "this is the third largest city in the
            country"). In common language, words used for counting are
            "cardinal numbers" and words used for ordering are "ordinal
            numbers".
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Three_apples%25281%2529.svg.png'
            uuid: 878e9d5c-73e8-40b7-b2a5-33a588e18504
            comment: >
                Natural numbers can be used for counting (one apple, two
                apples, three apples, …)
    - 
        quote: trioxide
        id: https://en.wikipedia.org/wiki/Trioxide
        uuid: 1be6f47c-9272-4367-a337-1f6f233ff7ce
        text: Trioxide
        comment: 'A trioxide is a compound with three oxygen atoms. For metals with the M2O3 formula there are several common structures. Al2O3, Cr2O3, Fe2O3, and V2O3 adopt the corundum structure. Many rare earth oxides adopt the "A-type rare earth structure" which is hexagonal. Several others plus indium oxide adopt the "C-type rare earth structure", also called "bixbyite", which is cubic and related to the fluorite structure.[1]'
    - 
        quote: agno
        id: https://en.wikipedia.org/wiki/Agno
        uuid: dbc39505-e097-4160-b95e-ed4afae2e03c
        text: Agno
        comment: 'Agno may refer to:'
    - 
        quote: nano
        id: https://en.wikipedia.org/wiki/Nano
        uuid: e03ab37c-9719-4b0c-a43f-ccfcbbc9c938
        text: Nano
        comment: ""
    - 
        quote: avagadro
        id: https://en.wikipedia.org/wiki/Avagadro
        uuid: 40b78ef5-d6f9-4b8a-91d0-d99740aaec2e
        text: Amedeo Avogadro
        comment: "Lorenzo Romano Amedeo Carlo Avogadro di Quaregna e di Cerreto,[1] Count of Quaregna and Cerreto (9 August 1776, Turin, Piedmont-Sardinia – 9 July 1856), was an Italian scientist, most noted for his contribution to molecular theory now known as Avogadro's law, which states that equal volumes of gases under the same conditions of temperature and pressure will contain equal numbers of molecules. In tribute to him, the number of elementary entities (atoms, molecules, ions or other particles) in 1 mole of a substance, 7023602214179000000♠6.02214179(30)×1023, is known as the Avogadro constant, one of the seven SI base units and represented by NA."
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Avogadro_-_M%25C3%25A9moire_sur_les_chaleurs_sp%25C3%25A9cifiques_des_corps_solides_et_liquides%252C_1833_-_6060053_TOAS005003_00003.tif.jpg'
            uuid: 0edb48c9-6a66-46ac-be7d-77a3dfe46061
            comment: Mémoire
    - 
        quote: mol
        id: https://en.wikipedia.org/wiki/Mol
        uuid: 17995b30-6e3b-46e1-8cf6-7a31ac92c110
        text: Mol
        comment: 'Mol or MOL may refer to:'
    - 
        quote: stp
        id: https://en.wikipedia.org/wiki/Stp
        uuid: d5518a00-eaa1-4756-91c1-4f30e94105db
        text: STP
        comment: 'STP may refer to:'
    - 
        quote: naoh
        id: https://en.wikipedia.org/wiki/Naoh
        uuid: 29754205-98cc-49d4-af80-1a67a3cf802a
        text: Sodium hydroxide
        comment: 'Sodium hydroxide (NaOH), also known as lye and caustic soda,[1][2] is an inorganic compound. It is a white solid and highly caustic metallic base and alkali of sodium which is available in pellets, flakes, granules, and as prepared solutions at different concentrations.[10] Sodium hydroxide forms an approximately 50% (by mass) saturated solution with water.[11]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lye.jpg
            uuid: cbecb404-46ef-4a50-ba24-f285675cd39e
            comment: Canister of sodium hydroxide.
    - 
        quote: caco
        id: https://en.wikipedia.org/wiki/Caco
        uuid: 897b708e-648a-4139-a8a8-c22623726e6b
        text: Caco
        comment: 'Caco or CACO may refer to:'
    - 
        quote: bunsen
        id: https://en.wikipedia.org/wiki/Bunsen
        uuid: f66aa91e-69cd-43fd-bbe7-73d0eaa225e7
        text: Bunsen
        comment: 'Bunsen may refer to:'
    - 
        quote: cucl
        id: https://en.wikipedia.org/wiki/Cucl
        uuid: 82522a11-e455-4176-8544-af8dc60776d5
        text: Copper(I) chloride
        comment: 'Copper(I) chloride, commonly called cuprous chloride, is the lower chloride of copper, with the formula CuCl. The substance is a white solid sparingly soluble in water, but very soluble in concentrated hydrochloric acid. Impure samples appear green due to the presence of copper(II) chloride.[3]'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-IR_Spectrum_of_Copper%2528I%2529_chloride.png'
            uuid: f76f3187-f611-482c-a56b-d03fc4b7eaca
            comment: IR spectrum of Copper(I) chloride
    - 
        quote: kno
        id: https://en.wikipedia.org/wiki/Kno
        uuid: be9aac49-e011-4ad9-9bd3-9396fa9e0338
        text: Kno
        comment: 'Kno, Inc. is a software company that works with publishers to offer digital textbooks and other educational materials. [1] In November 2013, after raising nearly $100 million in venture capital, the company was acquired by Intel.[2]'
    - 
        quote: cuo
        id: https://en.wikipedia.org/wiki/Cuo
        uuid: 5fc97e27-eef1-4211-b833-6234b737e540
        text: CUO
        comment: 'Refer also:'
    - 
        quote: oxidation-reduction
        id: https://en.wikipedia.org/wiki/Oxidation-reduction
        uuid: 3313adf5-71f3-4fa1-b6b1-0e595f140c1c
        text: Redox
        comment: 'Redox (short for reduction–oxidation reaction) is a chemical reaction in which the oxidation states of atoms are changed. Any such reaction involves both a reduction process and a complementary oxidation process, two key concepts involved with electron transfer processes.[1] Redox reactions include all chemical reactions in which atoms have their oxidation state changed; in general, redox reactions involve the transfer of electrons between chemical species. The chemical species from which the electron is stripped is said to have been oxidized, while the chemical species to which the electron is added is said to have been reduced. It can be explained in simple terms:'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/350px-NaF.gif
            uuid: 21603e4f-bac9-47f5-bdf4-ae77b1515f71
            comment: >
                Sodium and fluorine bonding ionically to form sodium
                fluoride. Sodium loses its outer electron to give it a
                stable electron configuration, and this electron enters the
                fluorine atom exothermically. The oppositely charged ions
                are then attracted to ...
    - 
        quote: potatoes
        id: https://en.wikipedia.org/wiki/Potatoes
        uuid: c3bf7bad-a721-421b-b1bd-8c1c7a2a1c11
        text: Potato
        comment: |
            The potato is a starchy, tuberous crop from the perennial nightshade Solanum tuberosum. The word "potato" may refer either to the plant itself or to the edible tuber.[2] In the Andes, where the species is indigenous, there are some other closely related cultivated potato species. Potatoes were introduced outside the Andes region approximately four centuries ago,[3] and have since become an integral part of much of the world's food supply. It is the world's fourth-largest food crop, following maize, wheat, and rice.[4] The green leaves and green skins of tubers exposed to the light are toxic.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Potato_flowers.jpg
            uuid: 6e3e4b1a-f78e-4876-90ec-ecf7a1834afb
            comment: Flowers of a potato plant
    - 
        quote: polyphenol
        id: https://en.wikipedia.org/wiki/Polyphenol
        uuid: 1c029463-2639-48d0-8c4d-63ddf5428992
        text: Polyphenol
        comment: 'Polyphenols[1][2] (noun, pronunciation of the singular /pɒliˈfiːnəl/[3] or /pɒliˈfɛnəl/, also known as polyhydroxyphenols) are a structural class of mainly natural, but also synthetic or semisynthetic, organic chemicals characterized by the presence of large multiples of phenol structural units. The number and characteristics of these phenol structures underlie the unique physical, chemical, and biological (metabolic, toxic, therapeutic, etc.) properties of particular members of the class. Examples include tannic acid (image at right) and ellagitannin (image below). The historically important chemical class of tannins is a subset of the polyphenols.[1][4]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Tannic_acid.svg.png
            uuid: 06975f6d-8af9-49db-baeb-b573dd61a3b6
            comment: >
                Plant-derived polyphenol, tannic acid, formed by
                esterification of ten equivalents of the
                phenylpropanoid-derived gallic acid to a monosaccharide
                (glucose) core from primary metabolism.
    - 
        quote: oxidase
        id: https://en.wikipedia.org/wiki/Oxidase
        uuid: 7009da4f-238c-4ca9-95d3-f97240d83888
        text: Oxidase
        comment: 'An oxidase is an enzyme that catalyzes an oxidation-reduction reaction, especially one involving dioxygen (O2) as the electron acceptor. In reactions involving donation of a hydrogen atom, oxygen is reduced to water (H2O) or hydrogen peroxide (H2O2). Some oxidation reactions, such as those involving monoamine oxidase or xanthine oxidase, typically do not involve free molecular oxygen.[1][2]'
    - 
        quote: tyrosinase
        id: https://en.wikipedia.org/wiki/Tyrosinase
        uuid: f5df26ff-44b9-4491-90a1-22724c560ea6
        text: Tyrosinase
        comment: 'Tyrosinase is an oxidase that is the rate-limiting enzyme for controlling the production of melanin. The enzyme is mainly involved in two distinct reactions of melanin synthesis; firstly, the hydroxylation of a monophenol and secondly, the conversion of an o-diphenol to the corresponding o-quinone. o-Quinone undergoes several reactions to eventually form melanin.[4] Tyrosinase is a copper-containing enzyme present in plant and animal tissues that catalyzes the production of melanin and other pigments from tyrosine by oxidation, as in the blackening of a peeled or sliced potato exposed to air.[5] It is found inside melanosomes which are synthesised in the skin melanocytes. In humans, the ...'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/250px-PBB_GE_TYR_206630_at_tn.png
            uuid: 903dde88-6f02-4122-b3b7-8bd1febfae07
            comment: ""
    - 
        quote: oxidative
        id: https://en.wikipedia.org/wiki/Oxidative
        uuid: efdf5099-a8bb-44bb-a24e-9f51711e186e
        text: Redox
        comment: 'Redox (short for reduction–oxidation reaction) is a chemical reaction in which the oxidation states of atoms are changed. Any such reaction involves both a reduction process and a complementary oxidation process, two key concepts involved with electron transfer processes.[1] Redox reactions include all chemical reactions in which atoms have their oxidation state changed; in general, redox reactions involve the transfer of electrons between chemical species. The chemical species from which the electron is stripped is said to have been oxidized, while the chemical species to which the electron is added is said to have been reduced. It can be explained in simple terms:'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/350px-NaF_0.gif
            uuid: 460abee3-e18c-4d14-bc1c-42a0d6b5d3e3
            comment: >
                Sodium and fluorine bonding ionically to form sodium
                fluoride. Sodium loses its outer electron to give it a
                stable electron configuration, and this electron enters the
                fluorine atom exothermically. The oppositely charged ions
                are then attracted to ...
    - 
        quote: nonmetal
        id: https://en.wikipedia.org/wiki/Nonmetal
        uuid: c66c3154-ce03-45f4-b0e1-333820557bc8
        text: Nonmetal
        comment: >
            In chemistry, a nonmetal (or non-metal) is a chemical
            element that mostly lacks metallic attributes. Physically,
            nonmetals tend to be highly volatile (easily vaporized),
            have low elasticity, and are good insulators of heat and
            electricity; chemically, they tend to have high ionization
            energy and electronegativity values, and gain or share
            electrons when they react with other elements or compounds.
            Seventeen elements are generally classified as nonmetals;
            most are gases (hydrogen, helium, nitrogen, oxygen,
            fluorine, neon, chlorine, argon, krypton, xenon and radon);
            one is a liquid (bromine), and a few are solids (carbon,
            phosphorus, sulfur, selenium, and iodine).
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Periodic_table_%2528polyatomic%2529.svg.png'
            uuid: f1a4619a-1989-48f3-9522-074ef432facc
            comment: 'Nonmetals in the periodic table:   Polyatomic nonmetal   Diatomic nonmetal   Noble gas Apart from hydrogen, nonmetals are located in the p-block. Helium, although an s-block element, is normally placed above neon (in the p-block) on account of ...'
    - 
        quote: rancidity
        id: https://en.wikipedia.org/wiki/Rancidity
        uuid: 8014d5ce-ea8d-4113-95ec-810a3970bd6c
        text: Rancidification
        comment: 'Rancidification, the product of which can be described as rancidity, is the process which causes a substance to become rancid, that is, having a rank, unpleasant smell or taste. Specifically, it is the hydrolysis and/or autoxidation of fats into short-chain aldehydes and ketones which are objectionable in taste and odor.[1] When these processes occur in food, undesirable odors and flavors can result. In some cases, however, the flavors can be desirable (as in aged cheeses).[2] In processed meats, these flavors are collectively known as warmed-over flavor. Rancidification can also detract from the nutritional value of food, and some vitamins are highly sensitive to degradation.[3] Akin to ...'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lipid_peroxidation.svg.png
            uuid: 5c54e9fd-a01a-441e-bb4d-d182bba2cdd5
            comment: >
                The free radical pathway for the first phase of the
                oxidative rancidification of fats.
    - 
        quote: mno
        id: https://en.wikipedia.org/wiki/Mno
        uuid: ca71c7ae-dbf8-45c2-b98b-4666ce43bf03
        text: mno
        comment: >
            mno is a file extension created by some web designing
            software like UltraDev or Dreamweaver. It often goes after
            the existing extension of the webpage (.html, .asp, .php),
            in effect creating a double extension, for instance,
            filename.html.mno.
    - 
        quote: cro
        id: https://en.wikipedia.org/wiki/Cro
        uuid: d39f3bc8-9fc5-4da3-bec6-d0aa8c7005d5
        text: Cro
        comment: "Cro is an American animated television series produced by the Children's Television Workshop (now known as Sesame Workshop) and Film Roman. It debuted on September 18, 1993 as part of the Saturday morning line-up for fall 1993 on ABC. Cro lasted 1½ seasons and ran in reruns through summer 1995. The show had an educational theme (this was before federal educational/informational mandates took effect in 1996), introducing basic concepts of physics, mechanical engineering, and technology. The premise of using woolly mammoths as a teaching tool for the principles of technology was inspired by David Macaulay's The Way Things Work; Macaulay is credited as writer on the show.[1] The last new ..."
    - 
        quote: double-displacement
        id: https://en.wikipedia.org/wiki/Double-displacement
        uuid: 731daab4-529c-462c-a9b6-2215c1e07b49
        text: Salt metathesis reaction
        comment: 'A salt metathesis reaction (from the Greek μετάθεσις, "transposition"), sometimes called a double replacement reaction or double displacement reaction, is a chemical process involving the exchange of bonds between two reacting chemical species, which results in the creation of products with similar or identical bonding affiliations.[1] This reaction is represented by the general scheme:'
    - 
        quote: displacement
        id: https://en.wikipedia.org/wiki/Displacement
        uuid: 0b3f62c1-d986-4b48-893a-54425e06c4bb
        text: Displacement
        comment: 'Displacement may refer to:'
    - 
        quote: placement
        id: https://en.wikipedia.org/wiki/Placement
        uuid: 6dc2ffc3-22d2-43d1-85d4-621ddf6d6f77
        text: Placement
        comment: 'Placement may refer to:'
    - 
        quote: cement
        id: https://en.wikipedia.org/wiki/Cement
        uuid: 866a0e1f-0ca1-4b6a-be3f-90ac4d13f1c2
        text: Cement
        comment: >
            A cement is a binder, a substance used in construction that
            sets and hardens and can bind other materials together. The
            most important types of cement are used as a component in
            the production of mortar in masonry, and of concrete, which
            is a combination of cement and an aggregate to form a strong
            building material.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-USMC-110806-M-IX060-148.jpg
            uuid: 7d1e8725-f4d3-4fd2-975e-22291c0a38fe
            comment: >
                Cement is often supplied as a powder, which is mixed with
                other materials and water. Many types of cement powder can
                cause allergic reactions upon skin contact and are
                irritating to skin, eyes, and lungs, so handlers should wear
                a dust mask, ...
    - 
        quote: ment
        id: https://en.wikipedia.org/wiki/Ment
        uuid: 3e324820-1d38-4c8a-8bc1-988690415319
        text: Adverb
        comment: >
            An adverb is a word that modifies a verb, adjective, another
            adverb, determiner, noun phrase, clause, or sentence.
            Adverbs typically express manner, place, time, frequency,
            degree, level of certainty, etc., answering questions such
            as how?, in what way?, when?, where?, and to what extent?.
            This function is called the adverbial function, and may be
            realised by single words (adverbs) or by multi-word
            expressions (adverbial phrases and adverbial clauses).
    - 
        quote: ent
        id: https://en.wikipedia.org/wiki/Ent
        uuid: 84d49570-33bb-4c41-bd1f-2a490dc94833
        text: Ent
        comment: "Ents are a race of beings in J. R. R. Tolkien's fantasy world Middle-earth who closely resemble trees. They are similar to the talking trees in folklore around the world. Their name is derived from the Anglo-Saxon word for giant."
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/250px-TREEBEARD.jpg
            uuid: 33b6e274-e5d0-4c81-9472-21ad8c0be1f9
            comment: "Treebeard and Hobbits." Illustration by Tom Loback.
    - 
        quote: nt
        id: https://en.wikipedia.org/wiki/Nt
        uuid: 356f96e6-9287-462c-b52a-46fd5fbbc28a
        text: NT
        comment: 'NT or nt may refer to:'
---
