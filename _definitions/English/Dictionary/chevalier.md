---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chevalier
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484611621
title: chevalier
categories:
    - Dictionary
---
Chevalier
     n 1: French actor and cabaret singer (1888-1972) [syn: {Maurice
          Chevalier}]
     2: a gallant or courtly gentleman [syn: {cavalier}]
