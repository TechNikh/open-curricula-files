---
version: 1
type: video
provider: YouTube
id: 92U67CUy9Gc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Rational%20expressions/Direct%20and%20inverse%20variation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a9c20014-e3fb-4464-9d69-2ce95fdfa4e3
updated: 1486069571
title: Direct and inverse variation
categories:
    - Rational expressions
thumbnail_urls:
    - https://i3.ytimg.com/vi/92U67CUy9Gc/default.jpg
    - https://i3.ytimg.com/vi/92U67CUy9Gc/1.jpg
    - https://i3.ytimg.com/vi/92U67CUy9Gc/2.jpg
    - https://i3.ytimg.com/vi/92U67CUy9Gc/3.jpg
---
Understanding direct and inverse variation

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/algebra2/rational-expressions/direct_inverse_variation/e/direct_and_inverse_variation?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Watch the next lesson: https://www.khanacademy.org/math/algebra2/rational-expressions/direct_inverse_variation/v/recognizing-direct-and-inverse-variation?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/rational-expressions/rational-function-graphing/v/finding-asymptotes-example?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
