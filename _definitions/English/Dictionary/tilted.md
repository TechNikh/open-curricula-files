---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tilted
offline_file: ""
offline_thumbnail: ""
uuid: 779d8455-6c8e-4b46-8514-b80155c2e4a8
updated: 1484310437
title: tilted
categories:
    - Dictionary
---
tilted
     adj : departing or being caused to depart from the true vertical
           or horizontal; "the leaning tower of Pisa"; "the
           headstones were tilted" [syn: {atilt}, {canted}, {leaning},
            {tipped}]
