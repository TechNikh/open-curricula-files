---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Robbins%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5cf08015-28bf-4117-bd7e-9efcf39491e8
updated: 1484309404
title: "Robbins' theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ear_decomposition.png
tags:
    - Orientable graphs
    - Related results
    - Algorithms and Complexity
    - Notes
categories:
    - Graph connectivity
---
In graph theory, Robbins' theorem, named after Herbert Robbins (1939), states that the graphs that have strong orientations are exactly the 2-edge-connected graphs. That is, it is possible to choose a direction for each edge of an undirected graph G, turning into a directed graph that has a path from every vertex to every other vertex, if and only if G is connected and has no bridge.
