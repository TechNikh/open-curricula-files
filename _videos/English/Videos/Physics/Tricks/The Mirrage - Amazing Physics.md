---
version: 1
type: video
provider: YouTube
id: fKgKIoxK2Jo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5d5f02b5-b851-4bcd-8ce9-38bac1027f3b
updated: 1486069660
title: 'The Mirrage - Amazing Physics'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/fKgKIoxK2Jo/default.jpg
    - https://i3.ytimg.com/vi/fKgKIoxK2Jo/1.jpg
    - https://i3.ytimg.com/vi/fKgKIoxK2Jo/2.jpg
    - https://i3.ytimg.com/vi/fKgKIoxK2Jo/3.jpg
---
