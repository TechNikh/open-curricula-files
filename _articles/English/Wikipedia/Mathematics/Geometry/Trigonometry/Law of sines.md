---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Law_of_sines
offline_file: ""
offline_thumbnail: ""
uuid: 4d8aaed0-9bd3-4441-83ef-961b0bae8684
updated: 1484309347
title: Law of sines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/512px-Johnny-automatic-scales-of-justice.svg_1.png
tags:
    - Proof
    - The ambiguous case of triangle solution
    - Examples
    - Example 1
    - Example 2
    - Relation to the circumcircle
    - Curvature
    - Spherical case
    - Hyperbolic case
    - Unified formulation
    - Higher dimensions
    - History
categories:
    - Trigonometry
---
In trigonometry, the law of sines, sine law, sine formula, or sine rule is an equation relating the lengths of the sides of any shaped triangle to the sines of its angles. According to the law,
