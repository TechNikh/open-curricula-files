---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ballot
offline_file: ""
offline_thumbnail: ""
uuid: 77849227-0ee9-4eb9-abd9-787b2dd9c6e1
updated: 1484310601
title: ballot
categories:
    - Dictionary
---
ballot
     n 1: a document listing the alternatives that is used in voting
     2: a choice that is made by voting; "there were only 17 votes
        in favor of the motion" [syn: {vote}, {voting}, {balloting}]
     v : vote by ballot; "The voters were balloting in this state"
