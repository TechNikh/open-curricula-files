---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ostensibly
offline_file: ""
offline_thumbnail: ""
uuid: 2cd4824d-c712-4814-bc61-b1f7db56f035
updated: 1484310609
title: ostensibly
categories:
    - Dictionary
---
ostensibly
     adv : from appearances alone; "irrigation often produces bumper
           crops from apparently desert land"; "the child is
           seemingly healthy but the doctor is concerned"; "had
           been ostensibly frank as to his purpose while really
           concealing it"-Thomas Hardy; "on the face of it the
           problem seems minor" [syn: {apparently}, {seemingly}, {on
           the face of it}]
