---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polar
offline_file: ""
offline_thumbnail: ""
uuid: 6915d22c-d702-4a01-a49d-d7f949459f17
updated: 1484310405
title: polar
categories:
    - Dictionary
---
polar
     adj 1: having a pair of equal and opposite charges
     2: characterized by opposite extremes; completely opposed; "in
        diametric contradiction to his claims"; "diametrical (or
        opposite) points of view"; "opposite meanings"; "extreme
        and indefensible polar positions" [syn: {diametric}, {diametrical},
         {opposite}]
     3: located at or near or coming from the earth's poles; "polar
        diameter"; "polar zone"; "a polar air mass"; "Antarctica
        is the only polar continent"
     4: of or existing at or near a geographical pole or within the
        Arctic or Antarctic Circles; "polar regions" [ant: {equatorial}]
     5: extremely cold; "an ...
