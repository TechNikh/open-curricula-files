---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manage
offline_file: ""
offline_thumbnail: ""
uuid: 2a719b57-3da1-4cc3-a20c-04e70591cd68
updated: 1484310264
title: manage
categories:
    - Dictionary
---
manage
     v 1: be successful; achieve a goal; "She succeeded in persuading
          us all"; "I managed to carry the box upstairs"; "She
          pulled it off, even though we never thought her capable
          of it"; "The pianist negociated the difficult runs"
          [syn: {pull off}, {negociate}, {bring off}, {carry off}]
          [ant: {fail}]
     2: be in charge of, act on, or dispose of; "I can deal with
        this crew of workers"; "This blender can't handle nuts";
        "She managed her parents' affairs after they got too old"
        [syn: {deal}, {care}, {handle}]
     3: come to terms or deal successfully with; "We got by on just
        a gallon of gas"; "They made ...
