---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhalation
offline_file: ""
offline_thumbnail: ""
uuid: 0811aa40-f7e4-461e-941d-542092572b1d
updated: 1484310323
title: exhalation
categories:
    - Dictionary
---
exhalation
     n 1: exhaled breath [syn: {halitus}]
     2: the act of expelling air from the lungs [syn: {expiration},
        {breathing out}]
