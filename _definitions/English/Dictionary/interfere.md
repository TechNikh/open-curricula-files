---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interfere
offline_file: ""
offline_thumbnail: ""
uuid: e401f820-7d3e-4d0f-ba62-07b4fd643ef4
updated: 1484310563
title: interfere
categories:
    - Dictionary
---
interfere
     v 1: come between so as to be hindrance or obstacle; "Your
          talking interferes with my work!"
     2: get involved, so as to alter or hinder an action, or through
        force or threat of force; "Why did the U.S. not intervene
        earlier in WW II?" [syn: {intervene}, {step in}, {interpose}]
