---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Horizon_effect
offline_file: ""
offline_thumbnail: ""
uuid: 7e7fc6c8-4244-436b-8fec-8629233b6cad
updated: 1484309002
title: Horizon effect
categories:
    - Game artificial intelligence
---
The horizon effect, also known as the horizon problem, is a problem in artificial intelligence where, in many games, the number of possible states or positions is immense and computers can only feasibly search a small portion of it, typically a few plies down the game tree. Thus, for a computer searching only five plies, there is a possibility that it will make a detrimental move, but the effect is not visible because the computer does not search to the depth of the error (i.e. beyond its "horizon").
