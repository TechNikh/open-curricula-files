---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reverse
offline_file: ""
offline_thumbnail: ""
uuid: f6b0d9a7-a813-44ab-8c63-83a9b311d0a4
updated: 1484310330
title: reverse
categories:
    - Dictionary
---
reverse
     adj 1: directed or moving toward the rear; "a rearward glance"; "a
            rearward movement" [syn: {rearward}]
     2: reversed (turned backward) in order or nature or effect
        [syn: {inverse}]
     3: of the transmission gear causing backward movement in a
        motor vehicle; "in reverse gear" [ant: {forward}]
     n 1: a relation of direct opposition; "we thought Sue was older
          than Bill but just the reverse was true" [syn: {contrary},
           {opposite}]
     2: the gears by which the motion of a machine can be reversed
     3: an unfortunate happening that hinders of impedes; something
        that is thwarting or frustrating [syn: {reversal}, ...
