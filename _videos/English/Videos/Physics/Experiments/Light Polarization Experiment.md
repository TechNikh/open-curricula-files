---
version: 1
type: video
provider: YouTube
id: Ra9n2DBZwpE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fad0ed90-7370-426c-9dfa-eda16e0181cd
updated: 1486069635
title: Light Polarization Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ra9n2DBZwpE/default.jpg
    - https://i3.ytimg.com/vi/Ra9n2DBZwpE/1.jpg
    - https://i3.ytimg.com/vi/Ra9n2DBZwpE/2.jpg
    - https://i3.ytimg.com/vi/Ra9n2DBZwpE/3.jpg
---
