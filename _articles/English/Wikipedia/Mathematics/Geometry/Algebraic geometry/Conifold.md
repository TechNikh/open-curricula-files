---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conifold
offline_file: ""
offline_thumbnail: ""
uuid: 8d5b13db-b27e-4633-a4d5-267ac6d00f7e
updated: 1484309138
title: Conifold
categories:
    - Algebraic geometry
---
In mathematics and string theory, a conifold is a generalization of a manifold. Unlike manifolds, conifolds can contain conical singularities, i.e. points whose neighbourhoods look like cones over a certain base. In physics, in particular in flux compactifications of string theory, the base is usually a five-dimensional real manifold, since the typically considered conifolds are complex 3-dimensional (real 6-dimensional) spaces.
