---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique-sum
offline_file: ""
offline_thumbnail: ""
uuid: 1a1bba76-0bde-412e-854b-be9f32e86f3a
updated: 1484309411
title: Clique-sum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Clique-sum.svg.png
tags:
    - Related concepts
    - Application in graph structure theory
    - Generalizations
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a branch of mathematics, a clique-sum is a way of combining two graphs by gluing them together at a clique, analogous to the connected sum operation in topology. If two graphs G and H each contain cliques of equal size, the clique-sum of G and H is formed from their disjoint union by identifying pairs of vertices in these two cliques to form a single shared clique, and then possibly deleting some of the clique edges. A k-clique-sum is a clique-sum in which both cliques have at most k vertices. One may also form clique-sums and k-clique-sums of more than two graphs, by repeated application of the two-graph clique-sum operation.
