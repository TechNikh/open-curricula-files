---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disarray
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587681
title: disarray
categories:
    - Dictionary
---
disarray
     n 1: a mental state characterized by a lack of clear and orderly
          thought and behavior; "a confusion of impressions" [syn:
           {confusion}, {mental confusion}, {confusedness}]
     2: untidiness (especially of clothing and appearance) [syn: {disorderliness}]
     v : bring disorder to [syn: {disorder}] [ant: {order}]
