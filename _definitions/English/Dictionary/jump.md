---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jump
offline_file: ""
offline_thumbnail: ""
uuid: 443685dd-8e71-478e-8156-4c74adab0f1f
updated: 1484310283
title: jump
categories:
    - Dictionary
---
jump
     n 1: a sudden and decisive increase; "a jump in attendance" [syn:
           {leap}]
     2: an abrupt transition; "a successful leap from college to the
        major leagues" [syn: {leap}, {saltation}]
     3: (film) an abrupt transition from one scene to another
     4: a sudden involuntary movement; "he awoke with a start" [syn:
         {startle}, {start}]
     5: descent with a parachute; "he had done a lot of parachuting
        in the army" [syn: {parachuting}]
     6: the act of jumping; propelling yourself off the ground; "he
        advanced in a series of jumps"; "the jumping was
        unexpected" [syn: {jumping}]
     v 1: move forward by leaps and bounds; "The ...
