---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/struggle
offline_file: ""
offline_thumbnail: ""
uuid: 998e844c-78f3-4b6f-8372-a74828d1c011
updated: 1484310290
title: struggle
categories:
    - Dictionary
---
struggle
     n 1: an energetic attempt to achieve something; "getting through
          the crowd was a real struggle"; "he fought a battle for
          recognition" [syn: {battle}]
     2: an open clash between two opposing groups (or individuals);
        "the harder the conflict the more glorious the
        triumph"--Thomas Paine; "police tried to control the
        battle between the pro- and anti-abortion mobs" [syn: {conflict},
         {battle}]
     3: strenuous effort; "the struggle to get through the crowd
        exhausted her"
     v 1: make a strenuous or labored effort; "She struggled for years
          to survive without welfare"; "He fought for breath"
          [syn: ...
