---
version: 1
type: video
provider: YouTube
id: ALqPQSrSVks
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0f4d4813-0fc0-4aa8-8b1e-57ca35a76750
updated: 1486069622
title: "Magic Line HIDDEN in Conic Sections - Pascal's Theorem (Hexagrammum Mysticum)"
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/ALqPQSrSVks/default.jpg
    - https://i3.ytimg.com/vi/ALqPQSrSVks/1.jpg
    - https://i3.ytimg.com/vi/ALqPQSrSVks/2.jpg
    - https://i3.ytimg.com/vi/ALqPQSrSVks/3.jpg
---
