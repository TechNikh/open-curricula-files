---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Estate_liquidation
offline_file: ""
offline_thumbnail: ""
uuid: acfe3577-dd33-4257-9f1b-d03c8326f9c7
updated: 1484309013
title: Estate liquidation
categories:
    - Auctions
---
An estate liquidation is similar to an estate sale in that the main concern or goal is to liquidate the estate (home, garage, sheds and yard) with an estate sale organization while also often adding the contents of a safe deposit box or more, family heirlooms too valuable to be left within the constraints of the family home, real estate, cars, boats, and other transportation including but not limited to motor homes and RVs, animals, livestock and whatever other assets the estate may encompass.
