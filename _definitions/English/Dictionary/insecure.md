---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insecure
offline_file: ""
offline_thumbnail: ""
uuid: c5d82eb3-fb7d-443c-a7cf-12716aef19de
updated: 1484310457
title: insecure
categories:
    - Dictionary
---
insecure
     adj 1: not firm or firmly fixed; likely to fail or give way; "the
            hinge is insecure" [ant: {secure}]
     2: lacking in security or safety; "his fortune was increasingly
        insecure"; "an insecure future" [syn: {unsafe}] [ant: {secure}]
     3: lacking self-confidence or assurance; "an insecure person
        lacking mental stability" [ant: {secure}]
     4: not safe from attack [syn: {unsafe}]
     5: not financially safe or secure; "a bad investment"; "high
        risk investments"; "anything that promises to pay too much
        can't help being risky"; "speculative business
        enterprises" [syn: {bad}, {risky}, {high-risk}, {speculative}]
