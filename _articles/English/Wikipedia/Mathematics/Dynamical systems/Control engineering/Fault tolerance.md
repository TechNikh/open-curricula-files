---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fault_tolerance
offline_file: ""
offline_thumbnail: ""
uuid: 1cb18bb7-5059-4670-900e-26591739a16c
updated: 1484308732
title: Fault tolerance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Graceful_Degradation_of_Transparency.png
tags:
    - Terminology
    - Components
    - Redundancy
    - Criteria
    - Requirements
    - Replication
    - Disadvantages
    - Examples
    - Related terms
categories:
    - Control engineering
---
Fault tolerance is the property that enables a system to continue operating properly in the event of the failure of (or one or more faults within) some of its components. If its operating quality decreases at all, the decrease is proportional to the severity of the failure, as compared to a naively designed system in which even a small failure can cause total breakdown. Fault tolerance is particularly sought after in high-availability or life-critical systems. The ability of maintaining functionality when portions of a system break down is referred to as graceful degradation.[1]
