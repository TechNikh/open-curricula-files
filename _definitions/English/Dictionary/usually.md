---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usually
offline_file: ""
offline_thumbnail: ""
uuid: a98d7a48-e8be-4d40-a87a-187a5c41217a
updated: 1484310355
title: usually
categories:
    - Dictionary
---
usually
     adv : under normal conditions; "usually she was late" [syn: {normally},
            {unremarkably}, {commonly}, {ordinarily}] [ant: {unusually}]
