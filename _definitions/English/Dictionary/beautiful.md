---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beautiful
offline_file: ""
offline_thumbnail: ""
uuid: 239a80b3-47c4-4645-9c47-b40905ce4793
updated: 1484310146
title: beautiful
categories:
    - Dictionary
---
beautiful
     adj 1: delighting the senses or exciting intellectual or emotional
            admiration; "a beautiful child"; "beautiful country";
            "a beautiful painting"; "a beautiful theory"; "a
            beautiful party" [ant: {ugly}]
     2: aesthetically pleasing
     3: (of weather) highly enjoyable; "what a beautiful day"
