---
version: 1
type: video
provider: YouTube
id: Q7dxi4ob2O4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Protein%20Structure%20-%20University%20Of%20Surrey.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7a7010b8-a664-43f0-9c29-28d78ab64c8f
updated: 1486069672
title: Protein Structure | University Of Surrey
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Q7dxi4ob2O4/default.jpg
    - https://i3.ytimg.com/vi/Q7dxi4ob2O4/1.jpg
    - https://i3.ytimg.com/vi/Q7dxi4ob2O4/2.jpg
    - https://i3.ytimg.com/vi/Q7dxi4ob2O4/3.jpg
---
