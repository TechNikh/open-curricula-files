---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_(graph)
offline_file: ""
offline_thumbnail: ""
uuid: 86550aa8-0ac1-4a3d-8165-fe1d339bc6f3
updated: 1484308629
title: Incidence (graph)
categories:
    - Graph theory
---
In graph theory, an incidence is a pair 
  
    
      
        (
        u
        ,
        e
        )
      
    
    {\displaystyle (u,e)}
  
 where 
  
    
      
        u
      
    
    {\displaystyle u}
  
 is a vertex and 
  
    
      
        e
      
    
    {\displaystyle e}
  
 is an edge incident to 
  
    
      
        u
      
    
    {\displaystyle u}
  
.
