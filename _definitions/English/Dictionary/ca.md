---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ca
offline_file: ""
offline_thumbnail: ""
uuid: cb95c24a-21d4-4f03-bde5-5acd4f917207
updated: 1484310303
title: ca
categories:
    - Dictionary
---
Ca
     n 1: a white metallic element that burns with a brilliant light;
          the fifth most abundant element in the earth's crust; an
          important component of most plants and animals [syn: {calcium},
           {atomic number 20}]
     2: a state in the western United States on the Pacific; the 3rd
        largest state; known for earthquakes [syn: {California}, {Golden
        State}, {Calif.}]
