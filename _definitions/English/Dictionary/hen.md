---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484562841
title: hen
categories:
    - Dictionary
---
hen
     n 1: adult female chicken [syn: {biddy}]
     2: adult female bird
     3: flesh of an older chicken suitable for stewing
     4: female of certain aquatic animals e.g. octopus or lobster
