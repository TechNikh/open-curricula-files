---
version: 1
type: video
provider: YouTube
id: BNHLzEv6Mjg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Analyzing%20solutions%20to%20linear%20systems%20graphically%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f6025f38-e101-4042-8a03-ba87181cb730
updated: 1486069557
title: Analyzing solutions to linear systems graphically 1
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/BNHLzEv6Mjg/default.jpg
    - https://i3.ytimg.com/vi/BNHLzEv6Mjg/1.jpg
    - https://i3.ytimg.com/vi/BNHLzEv6Mjg/2.jpg
    - https://i3.ytimg.com/vi/BNHLzEv6Mjg/3.jpg
---
