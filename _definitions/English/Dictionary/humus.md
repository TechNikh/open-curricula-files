---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humus
offline_file: ""
offline_thumbnail: ""
uuid: 2be83c8b-b251-494b-88c0-06cae345acb7
updated: 1484310160
title: humus
categories:
    - Dictionary
---
humus
     n 1: partially decomposed organic matter; the organic component
          of soil
     2: a thick spread made from mashed chickpeas, tahini, lemon
        juice and garlic; used especially as a dip for pita;
        originated in the Middle East [syn: {hummus}, {hommos}, {hoummos},
         {humous}]
