---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Snark_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 0d12f6df-28b9-4bae-81e6-900b5059d412
updated: 1484309386
title: Snark (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Flower_snarkv.svg.png
tags:
    - History
    - Properties
    - Snark theorem
    - List of snarks
categories:
    - Graph coloring
---
In the mathematical field of graph theory, a snark is a simple, connected, bridgeless cubic graph with chromatic index equal to 4. In other words, it is a graph in which every vertex has three neighbors, and the edges cannot be colored by only three colors without two edges of the same color meeting at a point. (By Vizing's theorem, the chromatic index of a cubic graph is 3 or 4.) In order to avoid trivial cases, snarks are often restricted to have girth at least 5.
