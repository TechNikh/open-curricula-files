---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cod
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484349121
title: cod
categories:
    - Dictionary
---
C.O.D.
     adv : collecting the charges upon delivery; "mail a package
           C.O.D." [syn: {COD}, {cash on delivery}]
