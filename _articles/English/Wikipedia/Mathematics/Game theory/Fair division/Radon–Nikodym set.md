---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Radon%E2%80%93Nikodym_set'
offline_file: ""
offline_thumbnail: ""
uuid: fff20897-fdc5-4727-ab54-69a9dd78ad2a
updated: 1484308518
title: Radon–Nikodym set
tags:
    - Example
    - Definitions
    - Efficient RNS partitions
    - History
categories:
    - Fair division
---
In the theory of fair cake-cutting, the Radon–Nikodym set (RNS) is a geometric object that represents a cake, based on how different people evaluate the different parts of the cake.
