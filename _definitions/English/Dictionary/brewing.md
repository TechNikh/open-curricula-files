---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brewing
offline_file: ""
offline_thumbnail: ""
uuid: 9cb0bdc4-965d-46ca-95c7-a397f3e414e5
updated: 1484310607
title: brewing
categories:
    - Dictionary
---
brewing
     n : the production of malt beverages (as beer or ale) from malt
         and hops by grinding and boiling them and fermenting the
         result with yeast
