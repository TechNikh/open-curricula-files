---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buried
offline_file: ""
offline_thumbnail: ""
uuid: 777a399b-60cc-4d0e-9536-7c2f217da8da
updated: 1484310266
title: buried
categories:
    - Dictionary
---
buried
     adj 1: covered from view; "her face buried (or hidden) in her
            hands"; "a secret buried deep within herself" [syn: {hidden}]
     2: placed in a grave; "the hastily buried corpses" [syn: {inhumed},
         {interred}] [ant: {unburied}]
