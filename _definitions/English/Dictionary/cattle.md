---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cattle
offline_file: ""
offline_thumbnail: ""
uuid: 0be0a540-56b7-4fc3-9e3a-df8dc5ebd24d
updated: 1484310451
title: cattle
categories:
    - Dictionary
---
cattle
     n : domesticated bovine animals as a group regardless of sex or
         age; "so many head of cattle"; "wait till the cows come
         home"; "seven thin and ill-favored kine"- Bible; "a team
         of oxen" [syn: {cows}, {kine}, {oxen}, {Bos taurus}]
