---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distributive
offline_file: ""
offline_thumbnail: ""
uuid: 7176b589-d323-4791-820f-a76173e81a66
updated: 1484310142
title: distributive
categories:
    - Dictionary
---
distributive
     adj 1: serving to distribute or allot or disperse [ant: {collective}]
     2: tending to disperse [syn: {dispersive}]
