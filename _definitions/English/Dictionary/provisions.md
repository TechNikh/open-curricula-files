---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provisions
offline_file: ""
offline_thumbnail: ""
uuid: ef879b47-fa4c-43ca-9f8c-c9f1dbd3ef4f
updated: 1484310451
title: provisions
categories:
    - Dictionary
---
provisions
     n : a stock or supply of foods [syn: {commissariat}, {provender},
          {viands}, {victuals}]
