---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/throw
offline_file: ""
offline_thumbnail: ""
uuid: d428b05d-20f1-42ee-8d1d-29f0b91042f5
updated: 1484310236
title: throw
categories:
    - Dictionary
---
throw
     n 1: the act of throwing (propelling something through the air
          with a rapid movement of the arm and wrist); "the
          catcher made a good throw to second base"
     2: a single chance or instance; "he couldn't afford $50 a
        throw"
     3: the maximum movement available to a pivoted or reciprocating
        piece by a cam [syn: {stroke}, {cam stroke}]
     4: the distance that something can be thrown; "it is just a
        stone's throw from here"
     5: bedclothes consisting of a lightweight cloth covering (an
        afghan or bedspread) that is casually thrown over
        something
     6: the throwing of an object in order to determine an outcome
       ...
