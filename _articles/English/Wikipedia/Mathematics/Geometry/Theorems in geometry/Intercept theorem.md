---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intercept_theorem
offline_file: ""
offline_thumbnail: ""
uuid: fc4503fa-4428-4489-98f6-7561de72fa4c
updated: 1484309116
title: Intercept theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Intercept_theorem-_Triangles.svg.png
tags:
    - Formulation
    - Related concepts
    - Similarity and similar triangles
    - Scalar multiplication in vector spaces
    - Applications
    - Algebraic formulation of compass and ruler constructions
    - The construction of a decimal number
    - Dividing a line segment in a given ratio
    - Measuring and survey
    - Height of the Cheops pyramid
    - Measuring the width of a river
    - Parallel lines in triangles and trapezoids
    - Proof of the theorem
    - Claim 1
    - Claim 2
    - Claim 3
    - Claim 4
    - Notes
categories:
    - Theorems in geometry
---
The intercept theorem, also known as Thales' theorem (not to be confused with another theorem with that name), is an important theorem in elementary geometry about the ratios of various line segments that are created if two intersecting lines are intercepted by a pair of parallels. It is equivalent to the theorem about ratios in similar triangles. Traditionally it is attributed to Greek mathematician Thales.[1]
