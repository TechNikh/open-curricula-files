---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perfect
offline_file: ""
offline_thumbnail: ""
uuid: 03c2caf9-ffa7-4ff3-8d78-3a027fd8a43e
updated: 1484310154
title: perfect
categories:
    - Dictionary
---
perfect
     adj 1: being complete of its kind and without defect or blemish; "a
            perfect circle"; "a perfect reproduction"; "perfect
            happiness"; "perfect manners"; "a perfect specimen";
            "a perfect day" [ant: {imperfect}]
     2: without qualification; used informally as (often pejorative)
        intensifiers; "an arrant fool"; "a complete coward"; "a
        consummate fool"; "a double-dyed villain"; "gross
        negligence"; "a perfect idiot"; "pure folly"; "what a
        sodding mess"; "stark staring mad"; "a thoroughgoing
        villain"; "utter nonsense" [syn: {arrant(a)}, {complete(a)},
         {consummate(a)}, {double-dyed(a)}, ...
