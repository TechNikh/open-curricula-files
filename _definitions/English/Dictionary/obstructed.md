---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obstructed
offline_file: ""
offline_thumbnail: ""
uuid: a954ae05-ba9d-453a-9e7b-cfd5e22c43cf
updated: 1484310200
title: obstructed
categories:
    - Dictionary
---
obstructed
     adj : shut off to passage or view or hindered from action; "a
           partially obstructed passageway"; "an obstructed view";
           "justice obstructed is not justice" [ant: {unobstructed}]
