---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stake
offline_file: ""
offline_thumbnail: ""
uuid: 5dbad0af-aed8-4bd1-841c-3924c0376293
updated: 1484310273
title: stake
categories:
    - Dictionary
---
stake
     n 1: (law) a right or legal share of something; a financial
          involvement with something; "they have interests all
          over the world"; "a stake in the company's future" [syn:
           {interest}]
     2: a pole or stake set up to mark something (as the start or
        end of a race track); "a pair of posts marked the goal";
        "the corner of the lot was indicated by a stake" [syn: {post}]
     3: instrument of execution consisting of a vertical post that a
        victim is tied to for burning
     4: the money risked on a gamble [syn: {stakes}, {bet}, {wager}]
     v 1: put at risk; "I will stake my good reputation for this"
          [syn: {venture}, ...
