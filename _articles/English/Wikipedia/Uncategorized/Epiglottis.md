---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epiglottis
offline_file: ""
offline_thumbnail: ""
uuid: eb746ff2-919f-4167-9fd0-412199fb9461
updated: 1484308378
title: Epiglottis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray958.png
tags:
    - Structure
    - Histology
    - Development
    - Function
    - Gag reflex
    - Speech sounds
    - Clinical significance
    - Inflammation
    - History
    - Additional images
categories:
    - Uncategorized
---
The epiglottis is a flap made of elastic cartilage tissue covered with a mucous membrane, attached to the entrance of the larynx. It projects obliquely upwards behind the tongue and the hyoid bone, pointing dorsally. It stands open during breathing, allowing air into the larynx. During swallowing, it closes to prevent aspiration, forcing the swallowed liquids or food to go down the esophagus instead. It is thus the valve that diverts passage to either the trachea or the esophagus.
