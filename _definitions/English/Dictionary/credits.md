---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/credits
offline_file: ""
offline_thumbnail: ""
uuid: 41978c96-980a-42db-acec-ce6243e1e04c
updated: 1484310611
title: credits
categories:
    - Dictionary
---
credits
     n : a list of acknowledgements of those who contributed to the
         creation of a film (usually run at the end of the film)
