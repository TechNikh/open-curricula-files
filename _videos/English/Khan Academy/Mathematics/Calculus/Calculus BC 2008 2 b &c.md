---
version: 1
type: video
provider: YouTube
id: S4oOSgTj9C8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Calculus%20BC%202008%202%20b%20%26c.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4d23e003-f26e-43b0-b36a-6af296a35f97
updated: 1486069690
title: 'Calculus BC 2008 2 b &c'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/S4oOSgTj9C8/default.jpg
    - https://i3.ytimg.com/vi/S4oOSgTj9C8/1.jpg
    - https://i3.ytimg.com/vi/S4oOSgTj9C8/2.jpg
    - https://i3.ytimg.com/vi/S4oOSgTj9C8/3.jpg
---
