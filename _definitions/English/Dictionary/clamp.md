---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clamp
offline_file: ""
offline_thumbnail: ""
uuid: 5bb77d0b-67a4-466b-becd-beaf2a34e4a1
updated: 1484310228
title: clamp
categories:
    - Dictionary
---
clamp
     n : a device (used by carpenters) that holds things firmly
         together
     v 1: fasten or fix with a clamp; "clamp the chair together until
          the glue has hardened"
     2: impose or inflict forcefully; "The military government
        clamped a curfew onto the capital"
