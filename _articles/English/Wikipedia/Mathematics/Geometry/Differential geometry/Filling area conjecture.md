---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Filling_area_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: a8fa8047-084e-4603-835b-73b90dd0ff36
updated: 1484309219
title: Filling area conjecture
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Steiner%2527s_Roman_Surface.gif'
tags:
    - Explanation
    - "Relation to Pu's inequality"
categories:
    - Differential geometry
---
In Riemannian geometry, an area of mathematics, Mikhail Gromov's filling area conjecture asserts that among all possible fillings of the Riemannian circle of length 2π by a surface with the strongly isometric property, the round hemisphere has the least area. Here the Riemannian circle refers to the unique closed 1-dimensional Riemannian manifold of total 1-volume 2π and Riemannian diameter π.
