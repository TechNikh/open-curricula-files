---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heteroclinic_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: ff9a3668-572a-4264-ae18-98d7b22ddd66
updated: 1484308696
title: Heteroclinic bifurcation
categories:
    - Bifurcation theory
---
In mathematics, particularly dynamical systems, a heteroclinic bifurcation is a global bifurcation involving a heteroclinic cycle. Heteroclinic bifurcations come in two types: resonance bifurcations and transverse bifurcations. Both types of bifurcation will result in the change of stability of the heteroclinic cycle.
