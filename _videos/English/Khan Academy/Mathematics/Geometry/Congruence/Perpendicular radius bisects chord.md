---
version: 1
type: video
provider: YouTube
id: q7eF5Ci944U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Congruence/Perpendicular%20radius%20bisects%20chord.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 11cbb60e-0a4f-4bbf-b0ad-463f851c5ef6
updated: 1486069575
title: Perpendicular radius bisects chord
categories:
    - Congruence
thumbnail_urls:
    - https://i3.ytimg.com/vi/q7eF5Ci944U/default.jpg
    - https://i3.ytimg.com/vi/q7eF5Ci944U/1.jpg
    - https://i3.ytimg.com/vi/q7eF5Ci944U/2.jpg
    - https://i3.ytimg.com/vi/q7eF5Ci944U/3.jpg
---
Simple proof using RSH postulate to show that a radius perpendicular to a chord bisects it

Watch the next lesson: https://www.khanacademy.org/math/geometry/congruent-triangles/cong_triangle/v/congruent-triangle-proof-example?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/congruent-triangles/cong_triangle/v/more-on-why-ssa-is-not-a-postulate?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
