---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/controversial
offline_file: ""
offline_thumbnail: ""
uuid: 5fef3994-32a7-44f9-867c-931b9194a98d
updated: 1484310186
title: controversial
categories:
    - Dictionary
---
controversial
     adj : marked by or capable of arousing controversy; "the issue of
           the death penalty is highly controversial"; "Rushdie's
           controversial book"; "a controversial decision on
           affirmative action" [ant: {uncontroversial}]
