---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presidency
offline_file: ""
offline_thumbnail: ""
uuid: d4387f9d-77bf-44ad-a1ae-b0603a3b298f
updated: 1484310603
title: presidency
categories:
    - Dictionary
---
presidency
     n 1: the tenure of a president; "things were quiet during the
          Eisenhower administration" [syn: {presidential term}, {administration}]
     2: the office and function of president; "Andrew Jackson
        expanded the power of the presidency beyond what was
        customary before his time" [syn: {presidentship}]
