---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lose
offline_file: ""
offline_thumbnail: ""
uuid: 7ac12a48-34ee-43e6-a6fe-b8cb9f4e9c1d
updated: 1484310290
title: lose
categories:
    - Dictionary
---
lose
     v 1: fail to keep or to maintain; cease to have, either
          physically or in an abstract sense; "She lost her purse
          when she left it unattended on her seat" [ant: {keep}]
     2: fail to win; "We lost the battle but we won the war" [ant: {win}]
     3: suffer the loss of a person through death or removal; "She
        lost her husband in the war"; "The couple that wanted to
        adopt the child lost her when the biological parents
        claimed her"
     4: place (something) where one cannot find it again; "I
        misplaced my eyeglasses" [syn: {misplace}, {mislay}]
     5: miss from one's possessions; lose sight of; "I've lost my
        glasses again!" ...
