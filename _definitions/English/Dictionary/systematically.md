---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/systematically
offline_file: ""
offline_thumbnail: ""
uuid: a929f576-619c-4248-8e19-9ef0c6aa8281
updated: 1484310469
title: systematically
categories:
    - Dictionary
---
systematically
     adv : in a systematic or consistent manner; "they systematically
           excluded women" [syn: {consistently}] [ant: {unsystematically},
            {inconsistently}]
