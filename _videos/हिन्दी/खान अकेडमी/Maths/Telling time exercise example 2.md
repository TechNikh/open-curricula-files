---
version: 1
type: video
provider: YouTube
id: bDg4LI6a6Ms
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a8941bec-a97d-4aff-ae45-3d8ab73233db
updated: 1486069611
title: Telling time exercise example 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/bDg4LI6a6Ms/default.jpg
    - https://i3.ytimg.com/vi/bDg4LI6a6Ms/1.jpg
    - https://i3.ytimg.com/vi/bDg4LI6a6Ms/2.jpg
    - https://i3.ytimg.com/vi/bDg4LI6a6Ms/3.jpg
---
