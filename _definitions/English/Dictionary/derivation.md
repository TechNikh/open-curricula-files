---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/derivation
offline_file: ""
offline_thumbnail: ""
uuid: a870661f-5227-4d47-b544-b65bd3a7561a
updated: 1484310216
title: derivation
categories:
    - Dictionary
---
derivation
     n 1: the source from which something derives (i.e. comes or
          issues); "he prefers shoes of Italian derivation"
     2: (historical linguistics) an explanation of the historical
        origins of a word or phrase [syn: {deriving}, {etymologizing}]
     3: a line of reasoning that shows how a conclusion follows
        logically from accepted propositions
     4: (descriptive linguistics) the process whereby new words are
        formed from existing words or bases by affixation:
        `singer' from `sing'; `undo' from `do'
     5: inherited properties shared with others of your bloodline
        [syn: {ancestry}, {lineage}, {filiation}]
     6: drawing of fluid or ...
