---
version: 1
type: video
provider: YouTube
id: PFcGiMLwjeY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Physics/Thermodynamic%20entropy%20definition%20clarification.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eaf773c7-ee1d-49c5-b732-cb066c681a07
updated: 1486069587
title: Thermodynamic entropy definition clarification
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/PFcGiMLwjeY/default.jpg
    - https://i3.ytimg.com/vi/PFcGiMLwjeY/1.jpg
    - https://i3.ytimg.com/vi/PFcGiMLwjeY/2.jpg
    - https://i3.ytimg.com/vi/PFcGiMLwjeY/3.jpg
---
Clarifying that the thermodynamic definition of Entropy requires a reversible system. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/reconciling-thermodynamic-and-state-definitions-of-entropy?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/proof-s-or-entropy-is-a-valid-state-variable?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
