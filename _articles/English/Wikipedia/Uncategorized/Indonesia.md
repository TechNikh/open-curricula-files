---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indonesia
offline_file: ""
offline_thumbnail: ""
uuid: 40edcb61-bfb5-4ec5-a516-e750f272a78b
updated: 1484308344
title: Indonesia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nicolaas_Pieneman_-_The_Submission_of_Prince_Dipo_Negoro_to_General_De_Kock.jpg
tags:
    - Etymology
    - History
    - Early history
    - Colonial era
    - Modern era
    - geography
    - Climate
    - Geology
    - biodiversity
    - Environment
    - Politics
    - government
    - Party
    - Administrative divisions
    - Foreign relations
    - Military
    - Economy
    - Transportation
    - Demographics
    - Ethnic groups
    - Languages
    - Urban centres
    - religion
    - Problems
    - Education
    - science and technology
    - Aerospace
    - Infrastructure
    - Internet
    - Tourism
    - culture
    - art
    - Architecture
    - Crafts
    - Clothing
    - Music
    - Dance
    - Cuisine
    - Theatre
    - Sports
    - Cinema
    - Literature
    - Media
categories:
    - Uncategorized
---
Indonesia (i/ˌɪndəˈniːʒə/ IN-də-NEE-zhə or /ˌɪndoʊˈniːziə/ IN-doh-NEE-zee-ə; Indonesian: [ɪndonesia]), officially the Republic of Indonesia (Indonesian: Republik Indonesia [rɛpublik ɪndonesia]), is a unitary sovereign state and transcontinental country located mainly in Southeast Asia with some territories in Oceania. Situated between the Indian and Pacific oceans, it is the world's largest island country, with more than thirteen thousand islands.[8][9] At 1,904,569 square kilometres (735,358 sq mi), Indonesia is the world's 14th-largest country in terms of land area and world's 7th-largest country in terms of combined sea and land area.[10] It has an estimated ...
