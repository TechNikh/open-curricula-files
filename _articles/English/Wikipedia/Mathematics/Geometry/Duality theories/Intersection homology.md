---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intersection_homology
offline_file: ""
offline_thumbnail: ""
uuid: 9982ed70-4311-4f92-a80d-6a99c6dfe54a
updated: 1484309234
title: Intersection homology
tags:
    - Goresky–MacPherson approach
    - Stratifications
    - Perversities
    - Singular intersection homology
    - Small resolutions
    - Sheaf theory
    - Properties of the complex IC(X)
    - Notes
categories:
    - Duality theories
---
In topology, a branch of mathematics, intersection homology is an analogue of singular homology especially well-suited for the study of singular spaces, discovered by Mark Goresky and Robert MacPherson in the fall of 1974 and developed by them over the next few years.
