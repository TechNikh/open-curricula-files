---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passion
offline_file: ""
offline_thumbnail: ""
uuid: ec83fab0-bce2-43d3-8952-1555b26737f7
updated: 1484310431
title: passion
categories:
    - Dictionary
---
passion
     n 1: strong feeling or emotion [syn: {passionateness}]
     2: intense passion or emotion [syn: {heat}, {warmth}]
     3: something that is desired intensely; "his rage for fame
        destroyed him" [syn: {rage}]
     4: an irrational but irresistible motive for a belief or action
        [syn: {mania}, {cacoethes}]
     5: a feeling of strong sexual desire
     6: any object of warm affection or devotion; "the theater was
        her first love" or "he has a passion for cock fighting";
        [syn: {love}]
     7: the suffering of Jesus at the crucifixion [syn: {Passion of
        Christ}]
