---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recent
offline_file: ""
offline_thumbnail: ""
uuid: afcb5f1f-faf2-4273-ab9b-2577659fd66d
updated: 1484310279
title: recent
categories:
    - Dictionary
---
recent
     adj 1: being new in a time not long past; "recent graduates"; "a
            recent addition to the house"; "recent buds on the
            apple trees"
     2: of the immediate past or just previous to the present time;
        "a late development"; "their late quarrel"; "his recent
        trip to Africa"; "in recent months"; "a recent issue of
        the journal" [syn: {late(a)}]
     3: near to or not long before the present; "recent times"; "of
        recent origin"
