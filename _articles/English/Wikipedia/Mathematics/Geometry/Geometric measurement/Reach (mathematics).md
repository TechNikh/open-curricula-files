---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reach_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 280c62e5-e190-4270-af9f-b20c3cd8477b
updated: 1484309245
title: Reach (mathematics)
categories:
    - Geometric measurement
---
Let X be a subset of Rn. Then reach of X is defined as
