---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traffic
offline_file: ""
offline_thumbnail: ""
uuid: 6d6338cd-f823-41a8-b40b-305d666c9590
updated: 1484310480
title: traffic
categories:
    - Dictionary
---
traffic
     n 1: the aggregation of things (pedestrians or vehicles) coming
          and going in a particular locality during a specified
          period of time
     2: buying and selling; especially illicit trade
     3: the amount of activity over a communication system during a
        given period of time; "heavy traffic overloaded the trunk
        lines"; "traffic on the internet is lightest during the
        night"
     4: social or verbal interchange (usually followed by `with')
        [syn: {dealings}]
     v 1: deal illegally; "traffic drugs"
     2: trade or deal a commodity; "They trafficked with us for
        gold"
     [also: {trafficking}, {trafficked}]
