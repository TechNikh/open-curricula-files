---
version: 1
type: video
provider: YouTube
id: E_BjzLpHiA4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 86c9e93d-5046-4756-a98b-162aa3d1601c
updated: 1486069619
title: Example 3 Completing the square
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/E_BjzLpHiA4/default.jpg
    - https://i3.ytimg.com/vi/E_BjzLpHiA4/1.jpg
    - https://i3.ytimg.com/vi/E_BjzLpHiA4/2.jpg
    - https://i3.ytimg.com/vi/E_BjzLpHiA4/3.jpg
---
