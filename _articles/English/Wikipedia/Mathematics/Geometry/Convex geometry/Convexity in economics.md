---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convexity_in_economics
offline_file: ""
offline_thumbnail: ""
uuid: 5b5dea0e-c376-43b1-9a82-2d9f654ede82
updated: 1484309189
title: Convexity in economics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Extreme_points.svg_0.png
tags:
    - Preliminaries
    - Real vector spaces
    - Convex sets
    - Convex hull
    - 'Duality: Intersecting half-spaces'
    - Supporting hyperplane theorem
    - Economics
    - Non‑convexity
    - Nonsmooth analysis
    - Notes
categories:
    - Convex geometry
---
Convexity is an important topic in economics.[1] In the Arrow–Debreu model of general economic equilibrium, agents have convex budget sets and convex preferences: At equilibrium prices, the budget hyperplane supports the best attainable indifference curve.[2] The profit function is the convex conjugate of the cost function.[1][2] Convex analysis is the standard tool for analyzing textbook economics.[1] Non‑convex phenomena in economics have been studied with nonsmooth analysis, which generalizes convex analysis.[3]
