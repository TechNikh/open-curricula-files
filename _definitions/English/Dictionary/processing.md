---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/processing
offline_file: ""
offline_thumbnail: ""
uuid: 825006e4-de8f-43eb-8387-3bd11bea8714
updated: 1484310321
title: processing
categories:
    - Dictionary
---
processing
     n : preparing or putting through a prescribed procedure; "the
         processing of newly arrived immigrants"; "the processing
         of ore to obtain minerals"
