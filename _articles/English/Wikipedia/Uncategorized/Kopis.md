---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kopis
offline_file: ""
offline_thumbnail: ""
uuid: f8589d0e-1caa-4d4b-9265-7d5ecb91b9f4
updated: 1484308455
title: Kopis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Greek-Persian_duel.jpg
tags:
    - characteristics
    - use
categories:
    - Uncategorized
---
The term kopis (from Greek κοπίς, plural kopides[1] from κόπτω - koptō, "to cut, to strike";[2] alternatively a derivation from the Ancient Egyptian term khopesh for a cutting sword has been postulated[3]) in Ancient Greece could describe a heavy knife with a forward-curving blade, primarily used as a tool for cutting meat, for ritual slaughter and animal sacrifice,[citation needed] or refer to a single edged cutting or "cut and thrust" sword with a similarly shaped blade.
