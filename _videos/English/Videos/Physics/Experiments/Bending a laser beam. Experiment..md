---
version: 1
type: video
provider: YouTube
id: zTx7UoPXvr4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 67a8b8ed-e11d-4cf4-abe5-8336267dd1e4
updated: 1486069635
title: Bending a laser beam. Experiment.
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/zTx7UoPXvr4/default.jpg
    - https://i3.ytimg.com/vi/zTx7UoPXvr4/1.jpg
    - https://i3.ytimg.com/vi/zTx7UoPXvr4/2.jpg
    - https://i3.ytimg.com/vi/zTx7UoPXvr4/3.jpg
---
