---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kolmogorov_automorphism
offline_file: ""
offline_thumbnail: ""
uuid: cbdcd785-6a5e-4671-a923-74405c7e3597
updated: 1484308777
title: Kolmogorov automorphism
tags:
    - Formal definition
    - Properties
categories:
    - Ergodic theory
---
In mathematics, a Kolmogorov automorphism, K-automorphism, K-shift or K-system is an invertible, measure-preserving automorphism defined on a standard probability space that obeys Kolmogorov's zero-one law.[1] All Bernoulli automorphisms are K-automorphisms (one says they have the K-property), but not vice versa. Many ergodic dynamical systems have been shown to have the K-property, although more recent research has shown that many of these are in fact Bernoulli automorphisms.
