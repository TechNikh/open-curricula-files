---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chow%E2%80%93Rashevskii_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 48309aaa-fe37-45d6-848b-3620a58971ef
updated: 1484309110
title: Chow–Rashevskii theorem
categories:
    - Theorems in geometry
---
In sub-Riemannian geometry, the Chow–Rashevskii theorem (also known as Chow's theorem) asserts that any two points of a connected sub-Riemannian manifold are connected by a horizontal path in the manifold. It is named after Wei-Liang Chow who proved it in 1939, and Petr Konstanovich Rashevskii, who proved it independently in 1938.
