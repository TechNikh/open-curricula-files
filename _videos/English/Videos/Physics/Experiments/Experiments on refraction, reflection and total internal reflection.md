---
version: 1
type: video
provider: YouTube
id: gDA_nDXM-ck
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 464642fe-cc9f-423e-a4fe-8d980018141c
updated: 1486069640
title: >
    Experiments on refraction, reflection and total internal
    reflection
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/gDA_nDXM-ck/default.jpg
    - https://i3.ytimg.com/vi/gDA_nDXM-ck/1.jpg
    - https://i3.ytimg.com/vi/gDA_nDXM-ck/2.jpg
    - https://i3.ytimg.com/vi/gDA_nDXM-ck/3.jpg
---
