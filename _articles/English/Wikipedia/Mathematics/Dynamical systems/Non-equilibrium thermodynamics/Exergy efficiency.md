---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exergy_efficiency
offline_file: ""
offline_thumbnail: ""
uuid: 1ef3d332-7c47-42c3-9e82-99a3a9ebbc65
updated: 1484308805
title: Exergy efficiency
tags:
    - Motivation
    - Definition
    - Application
    - Carnot
    - Second law efficiency under maximum power
categories:
    - Non-equilibrium thermodynamics
---
Exergy efficiency (also known as the second-law efficiency or rational efficiency) computes the efficiency of a process taking the second law of thermodynamics into account.
