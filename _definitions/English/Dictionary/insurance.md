---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insurance
offline_file: ""
offline_thumbnail: ""
uuid: 984129c9-cb6b-4291-919b-cd2640a74345
updated: 1484310453
title: insurance
categories:
    - Dictionary
---
insurance
     n 1: promise of reimbursement in the case of loss; paid to people
          or companies so concerned about hazards that they have
          made prepayments to an insurance company
     2: written contract or certificate of insurance; "you should
        have read the small print on your policy" [syn: {policy},
        {insurance policy}]
     3: protection against future loss [syn: {indemnity}]
