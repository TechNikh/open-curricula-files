---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coherent_duality
offline_file: ""
offline_thumbnail: ""
uuid: 6a0e6839-0261-41dc-b193-2e4d93e6201c
updated: 1484309231
title: Coherent duality
tags:
    - Adjoint functor point of view
    - |
        Construction of the
        
        
        
        
        f
        
        !
        
        
        
        
        {\displaystyle f^{!}}
        
        pseudofunctor using rigid dualizing complexes
    - Notes
categories:
    - Duality theories
---
In mathematics, coherent duality is any of a number of generalisations of Serre duality, applying to coherent sheaves, in algebraic geometry and complex manifold theory, as well as some aspects of commutative algebra that are part of the 'local' theory.
