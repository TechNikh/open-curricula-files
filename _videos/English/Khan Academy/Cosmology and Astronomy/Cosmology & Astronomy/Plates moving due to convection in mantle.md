---
version: 1
type: video
provider: YouTube
id: f8GK2oEN-uI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Plates%20moving%20due%20to%20convection%20in%20mantle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4270d3e0-dc4b-4cfb-a0a8-288a70bfe4e7
updated: 1486069686
title: Plates moving due to convection in mantle
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/f8GK2oEN-uI/default.jpg
    - https://i3.ytimg.com/vi/f8GK2oEN-uI/1.jpg
    - https://i3.ytimg.com/vi/f8GK2oEN-uI/2.jpg
    - https://i3.ytimg.com/vi/f8GK2oEN-uI/3.jpg
---
