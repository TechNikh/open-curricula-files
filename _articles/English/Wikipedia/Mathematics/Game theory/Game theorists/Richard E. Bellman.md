---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard_E._Bellman
offline_file: ""
offline_thumbnail: ""
uuid: 6665ffbf-1a28-4bd8-babe-0aaa9bf00e6e
updated: 1484308517
title: Richard E. Bellman
tags:
    - Biography
    - Work
    - Bellman equation
    - Hamilton–Jacobi–Bellman equation
    - Curse of dimensionality
    - Bellman–Ford algorithm
    - Publications
    - Articles
categories:
    - Game theorists
---
Richard Ernest Bellman (August 26, 1920 – March 19, 1984) was an American applied mathematician, who introduced dynamic programming in 1953, and important contributions in other fields of mathematics.
