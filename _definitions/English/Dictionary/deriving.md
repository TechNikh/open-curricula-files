---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deriving
offline_file: ""
offline_thumbnail: ""
uuid: bea01b46-ff05-4cf0-aea4-7ec40e571ec7
updated: 1484310431
title: deriving
categories:
    - Dictionary
---
deriving
     n : (historical linguistics) an explanation of the historical
         origins of a word or phrase [syn: {derivation}, {etymologizing}]
