---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cousin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619242
title: cousin
categories:
    - Dictionary
---
cousin
     n : the child of your aunt or uncle [syn: {first cousin}, {cousin-german},
          {full cousin}]
