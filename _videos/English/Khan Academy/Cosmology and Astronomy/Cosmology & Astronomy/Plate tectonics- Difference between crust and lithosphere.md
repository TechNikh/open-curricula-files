---
version: 1
type: video
provider: YouTube
id: f2BWsPVN7c4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Plate%20tectonics-%20Difference%20between%20crust%20and%20lithosphere.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e83c04cc-8833-4561-939a-64b47752932b
updated: 1486069688
title: 'Plate tectonics- Difference between crust and lithosphere'
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/f2BWsPVN7c4/default.jpg
    - https://i3.ytimg.com/vi/f2BWsPVN7c4/1.jpg
    - https://i3.ytimg.com/vi/f2BWsPVN7c4/2.jpg
    - https://i3.ytimg.com/vi/f2BWsPVN7c4/3.jpg
---
