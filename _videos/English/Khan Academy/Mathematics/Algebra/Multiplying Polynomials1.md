---
version: 1
type: video
provider: YouTube
id: HB48COey2O8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Multiplying%20Polynomials1.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5acca5f5-7a35-40ad-844d-27906c697631
updated: 1486069563
title: Multiplying Polynomials1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/HB48COey2O8/default.jpg
    - https://i3.ytimg.com/vi/HB48COey2O8/1.jpg
    - https://i3.ytimg.com/vi/HB48COey2O8/2.jpg
    - https://i3.ytimg.com/vi/HB48COey2O8/3.jpg
---
