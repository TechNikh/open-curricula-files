---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immunity
offline_file: ""
offline_thumbnail: ""
uuid: a6dd74e3-77a2-4104-834f-7ec0bb2be0b4
updated: 1484310567
title: immunity
categories:
    - Dictionary
---
immunity
     n 1: the state of not being susceptible; "unsusceptibility to
          rust" [syn: {unsusceptibility}] [ant: {susceptibility}]
     2: (medicine) the condition in which an organism can resist
        disease [syn: {resistance}]
     3: the quality of being unaffected by something; "immunity to
        criticism"
     4: an act exempting someone; "he was granted immunity from
        prosecution" [syn: {exemption}, {granting immunity}]
