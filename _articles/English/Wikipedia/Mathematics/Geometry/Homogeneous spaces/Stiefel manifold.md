---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stiefel_manifold
offline_file: ""
offline_thumbnail: ""
uuid: ccc8f25e-978e-4a70-b20d-9018793fccdf
updated: 1484309247
title: Stiefel manifold
tags:
    - Topology
    - As a homogeneous space
    - Uniform measure
    - Special cases
    - Functoriality
    - As a principal bundle
    - Homotopy
categories:
    - Homogeneous spaces
---
In mathematics, the Stiefel manifold Vk(Rn) is the set of all orthonormal k-frames in Rn. That is, it is the set of ordered k-tuples of orthonormal vectors in Rn. It is named after Swiss mathematician Eduard Stiefel. Likewise one can define the complex Stiefel manifold Vk(Cn) of orthonormal k-frames in Cn and the quaternionic Stiefel manifold Vk(Hn) of orthonormal k-frames in Hn. More generally, the construction applies to any real, complex, or quaternionic inner product space.
