---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kutch
offline_file: ""
offline_thumbnail: ""
uuid: 33477248-ff72-44b6-9d55-8adc67d2fd45
updated: 1484308425
title: Kutch district
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-The_%2527Ten_Indus_Scripts%2527_discovered_near_the_northern_gateway_of_the_Dholavira_citadel.jpg'
tags:
    - History
    - geography
    - Rivers and dams
    - Divisions
    - Wildlife sanctuaries and reserves
    - Demographics
    - Education
    - culture
    - language
    - people
    - Food and drink
    - Economy
    - Mineral
    - Cement and Power
    - Forestry
    - salt
    - Textile Art
    - religion
    - In culture
categories:
    - Uncategorized
---
Kutch district (also spelled as Kachchh) is a district of Gujarat state in western India. Covering an area of 45,652 km²,[1] it is the largest district of India. The population of Kutch is 2,092,371.[2] It has 10 Talukas, 939 villages and 6 Municipalities.[2]
