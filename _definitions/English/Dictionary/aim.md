---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aim
offline_file: ""
offline_thumbnail: ""
uuid: de24acb0-15a9-4417-a13d-8ada5121dc50
updated: 1484310230
title: aim
categories:
    - Dictionary
---
aim
     n 1: an anticipated outcome that is intended or that guides your
          planned actions; "his intent was to provide a new
          translation"; "good intentions are not enough"; "it was
          created with the conscious aim of answering immediate
          needs"; "he made no secret of his designs" [syn: {purpose},
           {intent}, {intention}, {design}]
     2: the goal intended to be attained (and which is believed to
        be attainable); "the sole object of her trip was to see
        her children" [syn: {object}, {objective}, {target}]
     3: the action of directing something at an object; "he took aim
        and fired"
     4: the direction or path along which ...
