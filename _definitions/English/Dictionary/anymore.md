---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anymore
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479561
title: anymore
categories:
    - Dictionary
---
anymore
     adv : at the present or from now on; usually used with a negative;
           "Alice doesn't live here anymore"; "the children
           promised not to quarrel any more" [syn: {any longer}]
