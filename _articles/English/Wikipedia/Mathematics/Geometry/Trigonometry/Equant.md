---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equant
offline_file: ""
offline_thumbnail: ""
uuid: cbf40a4b-2311-4849-b700-0782e90f6037
updated: 1484309343
title: Equant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Ptolemaic_elements.svg.png
tags:
    - Placement
    - Equation
    - Discovery and use
    - Criticism
categories:
    - Trigonometry
---
Equant (or punctum aequans) is a mathematical concept developed by Claudius Ptolemy in the 2nd century AD to account for the observed motion of the planets. The equant is used to explain the observed speed change in planetary orbit during different stages of the orbit. This planetary concept allowed Ptolemy to keep the theory of uniform circular motion alive by stating that the path of heavenly bodies was uniform around one point and circular around another point.
