---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anatole_Katok
offline_file: ""
offline_thumbnail: ""
uuid: 68ec746e-0896-430e-b13b-7eb9fd68ba93
updated: 1484308692
title: Anatole Katok
tags:
    - Early life and education
    - Work and research
    - Teaching
    - Honors and recognition
    - Selected monographs
categories:
    - Dynamical systems theorists
---
Anatole Borisovich Katok (Russian: Анатолий Борисович Каток; born August 9, 1944 in Washington DC) is an American mathematician with Russian origins. Katok is the Director of the Center for Dynamics and Geometry at the Pennsylvania State University. His field of research is the theory of dynamical systems.
