---
version: 1
type: video
provider: YouTube
id: DtDaQPbKV9s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 548f55af-35c5-47a2-8354-654647cd09d9
updated: 1486069639
title: Refraction Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/DtDaQPbKV9s/default.jpg
    - https://i3.ytimg.com/vi/DtDaQPbKV9s/1.jpg
    - https://i3.ytimg.com/vi/DtDaQPbKV9s/2.jpg
    - https://i3.ytimg.com/vi/DtDaQPbKV9s/3.jpg
---
