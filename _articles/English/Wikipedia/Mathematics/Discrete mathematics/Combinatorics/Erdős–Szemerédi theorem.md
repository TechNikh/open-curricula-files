---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Szemer%C3%A9di_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 46a3d29e-4391-43d3-b431-9600ad55a29c
updated: 1484308573
title: Erdős–Szemerédi theorem
categories:
    - Combinatorics
---
In arithmetic combinatorics, the Erdős–Szemerédi theorem, proven by Paul Erdős and Endre Szemerédi in 1983,[1] states that, for every finite set of real numbers, either the pairwise sums or the pairwise products of the numbers in the set form a significantly larger set. More precisely, it asserts the existence of positive constants c and 
  
    
      
        ε
      
    
    {\displaystyle \varepsilon }
  
 such that
