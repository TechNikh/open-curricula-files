---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embankment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484368441
title: embankment
categories:
    - Dictionary
---
embankment
     n : a long artificial mound of stone or earth; built to hold
         back water or to support a road or as protection
