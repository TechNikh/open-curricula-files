---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conversation
offline_file: ""
offline_thumbnail: ""
uuid: 357600f8-d336-49ab-9aad-7ee4b1df1098
updated: 1484310150
title: conversation
categories:
    - Dictionary
---
conversation
     n : the use of speech for informal exchange of views or ideas or
         information etc.
