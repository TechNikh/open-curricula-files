---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_database
offline_file: ""
offline_thumbnail: ""
uuid: a7d98794-2947-4440-93f6-62aca98aaf17
updated: 1484309405
title: Graph database
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/308px-GraphDatabase_PropertyGraph.png
tags:
    - Description
    - Properties
    - History
    - List of graph databases
    - APIs and graph query-programming languages
categories:
    - Graph databases
---
In computing, a graph database is a database that uses graph structures for semantic queries with nodes, edges and properties to represent and store data. A key concept of the system is the graph (or edge or relationship), which directly relates data items in the store. The relationships allow data in the store to be linked together directly, and in many cases retrieved with a single operation.
