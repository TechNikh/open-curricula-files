---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deterrent
offline_file: ""
offline_thumbnail: ""
uuid: e0cb3846-aca4-45a9-9c19-6205db82c08d
updated: 1484310180
title: deterrent
categories:
    - Dictionary
---
deterrent
     adj : tending to deter; "the deterrent effects of high prices"
     n : something immaterial that interferes with or delays action
         or progress [syn: {hindrance}, {impediment}, {balk}, {baulk},
          {check}, {handicap}]
