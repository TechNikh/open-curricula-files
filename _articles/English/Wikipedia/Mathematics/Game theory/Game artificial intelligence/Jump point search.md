---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jump_point_search
offline_file: ""
offline_thumbnail: ""
uuid: 6f9c0532-b2d0-4817-a3a0-b82058883919
updated: 1484309006
title: Jump point search
categories:
    - Game artificial intelligence
---
In computer science, Jump Point Search (JPS) is an optimization to the A* search algorithm for uniform-cost grids. It reduces symmetries in the search procedure by means of graph pruning,[1] eliminating certain nodes in the grid based on assumptions that can be made about the current node's neighbors, as long as certain conditions relating to the grid are satisfied. As a result, the algorithm can consider long "jumps" along straight (horizontal, vertical and diagonal) lines in the grid, rather than the small steps from one grid position to the next that ordinary A* considers.[2]
