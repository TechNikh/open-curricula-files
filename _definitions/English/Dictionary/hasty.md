---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hasty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461501
title: hasty
categories:
    - Dictionary
---
hasty
     adj 1: excessively quick; "made a hasty exit"; "a headlong rush to
            sell" [syn: {headlong}]
     2: done with very great haste and without due deliberation;
        "hasty marriage seldom proveth well"- Shakespeare; "hasty
        makeshifts take the place of planning"- Arthur Geddes;
        "rejected what was regarded as an overhasty plan for
        reconversion"; "wondered whether they had been rather
        precipitate in deposing the king" [syn: {overhasty}, {precipitate},
         {precipitant}, {precipitous}]
     [also: {hastiest}, {hastier}]
