---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/license
offline_file: ""
offline_thumbnail: ""
uuid: 1b6f0a6b-0ce2-4657-8757-23eb4e78319f
updated: 1484310464
title: license
categories:
    - Dictionary
---
license
     n 1: a legal document giving official permission to do something
          [syn: {licence}, {permit}]
     2: freedom to deviate deliberately from normally applicable
        rules or practices (especially in behavior or speech)
        [syn: {licence}]
     3: excessive freedom; lack of due restraint; "when liberty
        becomes license dictatorship is near"- Will Durant; "the
        intolerable license with which the newspapers break...the
        rules of decorum"- Edmund Burke [syn: {licence}]
     4: the act of giving a formal (usually written) authorization
        [syn: {permission}, {permit}]
     v : authorize officially; "I am licensed to practice law in this
      ...
