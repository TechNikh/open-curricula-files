---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_mechanics
offline_file: ""
offline_thumbnail: ""
uuid: 6eb68d08-0957-4b78-b450-9aab260c83a7
updated: 1484308794
title: Geometric mechanics
tags:
    - Momentum map and reduction
    - Variational principles
    - Geometric integrators
    - History
    - Applications
    - Notes
categories:
    - Hamiltonian mechanics
---
Geometric mechanics is a branch of mathematics applying particular geometric methods to many areas of mechanics, from mechanics of particles and rigid bodies to fluid mechanics to control theory.
