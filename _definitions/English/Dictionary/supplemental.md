---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supplemental
offline_file: ""
offline_thumbnail: ""
uuid: f3b8d07b-30f2-4755-9a14-90f9ad25f557
updated: 1484096617
title: supplemental
categories:
    - Dictionary
---
supplemental
     adj 1: functioning in a subsidiary or supporting capacity; "the
            main library and its auxiliary branches" [syn: {auxiliary},
             {subsidiary}, {supplementary}]
     2: added to complete or make up a deficiency; "produced
        supplementary volumes"; "additional reading" [syn: {supplementary},
         {additional}]
