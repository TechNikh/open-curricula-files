---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursive_definition
offline_file: ""
offline_thumbnail: ""
uuid: ebe1e6ba-fd03-4219-bb73-b16352ce786b
updated: 1484308681
title: Recursive definition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-KochFlake.svg.png
tags:
    - Form of recursive definitions
    - Examples of recursive definitions
    - Elementary functions
    - Prime numbers
    - Non-negative even numbers
    - Well formed formulas
categories:
    - Theoretical computer science
---
A recursive definition (or inductive definition) in mathematical logic and computer science is used to define the elements in a set in terms of other elements in the set (Aczel 1978:740ff).
