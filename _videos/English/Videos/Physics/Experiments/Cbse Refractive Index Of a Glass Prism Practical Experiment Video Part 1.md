---
version: 1
type: video
provider: YouTube
id: GfQ7HV4ThrQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32dbe8bb-3f30-45ff-bad1-b40b16d743db
updated: 1486069639
title: >
    Cbse Refractive Index Of a Glass Prism Practical Experiment
    Video Part 1
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GfQ7HV4ThrQ/default.jpg
    - https://i3.ytimg.com/vi/GfQ7HV4ThrQ/1.jpg
    - https://i3.ytimg.com/vi/GfQ7HV4ThrQ/2.jpg
    - https://i3.ytimg.com/vi/GfQ7HV4ThrQ/3.jpg
---
