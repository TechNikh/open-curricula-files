---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quite
offline_file: ""
offline_thumbnail: ""
uuid: ff0cc248-8430-43f3-9b0f-58605d46d9f4
updated: 1484310277
title: quite
categories:
    - Dictionary
---
quite
     adv 1: to a degree (not used with a negative); "quite tasty";
            "quite soon"; "quite ill"; "quite rich" [syn: {rather}]
     2: to the greatest extent; completely; "you're quite right";
        "she was quite alone"; "was quite mistaken"; "quite the
        opposite"; "not quite finished"; "did not quite make it"
     3: of an unusually noticeable or exceptional or remarkable kind
        (not used with a negative); "her victory was quite
        something"; "she's quite a girl"; "quite a film"; "quite a
        walk"; "we've had quite an afternoon" [syn: {quite a}, {quite
        an}]
     4: actually or truly or to an extreme; "was quite a sudden
        change"; ...
