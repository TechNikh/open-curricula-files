---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/variety
offline_file: ""
offline_thumbnail: ""
uuid: 8592bcee-8104-4085-961f-5b3f3982e49c
updated: 1484310323
title: variety
categories:
    - Dictionary
---
variety
     n 1: a collection containing a variety of sorts of things; "a
          great assortment of cars was on display"; "he had a
          variety of disorders"; "a veritable smorgasbord of
          religions" [syn: {assortment}, {mixture}, {mixed bag}, {miscellany},
           {miscellanea}, {salmagundi}, {smorgasbord}, {potpourri},
           {motley}]
     2: noticeable heterogeneity; "a diversity of possibilities";
        "the range and variety of his work is amazing" [syn: {diverseness},
         {diversity}, {multifariousness}]
     3: (biology) a taxonomic category consisting of members of a
        species that differe from others of the same species in
        minor but ...
