---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moving
offline_file: ""
offline_thumbnail: ""
uuid: 0740d7b7-9f45-4646-8bde-d4e0eb9d9cd0
updated: 1484310337
title: moving
categories:
    - Dictionary
---
moving
     adj 1: in motion; "a constantly moving crowd"; "the moving parts of
            the machine" [ant: {nonmoving}]
     2: arousing or capable of arousing deep emotion; "she laid her
        case of destitution before him in a very moving letter"-
        N. Hawthorne [ant: {unmoving}]
     3: used of a series of photographs presented so as to create
        the illusion of motion; "Her ambition was to be in moving
        pictures or `the movies'" [ant: {still}]
