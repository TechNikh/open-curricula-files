---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/court
offline_file: ""
offline_thumbnail: ""
uuid: 3726e207-29e5-44ab-bd82-af7f2664672c
updated: 1484310467
title: court
categories:
    - Dictionary
---
court
     n 1: an assembly (including one or more judges) to conduct
          judicial business [syn: {tribunal}, {judicature}]
     2: the sovereign and his advisers who are the governing power
        of a state [syn: {royal court}]
     3: a specially marked area within which a game is played;
        "players had to reserve a court in advance"
     4: a room in which a law court sits; "television cameras were
        admitted in the courtroom" [syn: {courtroom}]
     5: a yard wholly or partly surrounded by walls or buildings;
        "the house was built around an inner court" [syn: {courtyard}]
     6: the residence of a sovereign or nobleman; "the king will
        visit the duke's ...
