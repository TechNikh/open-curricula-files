---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drunken
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451661
title: drunken
categories:
    - Dictionary
---
drunken
     adj : given to or marked by the consumption of alcohol; "a
           bibulous fellow"; "a bibulous evening"; "his boozy
           drinking companions"; "thick boozy singing"; "a drunken
           binge"; "two drunken gentleman holding each other up";
           "sottish behavior" [syn: {bibulous}, {boozy}, {sottish}]
