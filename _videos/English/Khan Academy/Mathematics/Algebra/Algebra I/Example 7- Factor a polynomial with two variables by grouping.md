---
version: 1
type: video
provider: YouTube
id: HXIj16mjfgk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%207-%20Factor%20a%20polynomial%20with%20two%20variables%20by%20grouping.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c7575b6d-bc37-4b62-9fa0-6118d8a08ac0
updated: 1486069563
title: 'Example 7- Factor a polynomial with two variables by grouping'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/HXIj16mjfgk/default.jpg
    - https://i3.ytimg.com/vi/HXIj16mjfgk/1.jpg
    - https://i3.ytimg.com/vi/HXIj16mjfgk/2.jpg
    - https://i3.ytimg.com/vi/HXIj16mjfgk/3.jpg
---
