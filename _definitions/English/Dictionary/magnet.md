---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnet
offline_file: ""
offline_thumbnail: ""
uuid: f54818a6-0b09-4d3c-badd-3df2764b3426
updated: 1484310194
title: magnet
categories:
    - Dictionary
---
magnet
     n 1: (physics) a device that attracts iron and produces a
          magnetic field
     2: a characteristic that provides pleasure and attracts;
        "flowers are an attractor for bees" [syn: {attraction}, {attractor},
         {attracter}, {attractive feature}]
