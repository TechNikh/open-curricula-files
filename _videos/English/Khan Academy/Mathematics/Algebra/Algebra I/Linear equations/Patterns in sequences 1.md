---
version: 1
type: video
provider: YouTube
id: Zj-a_9cd5jc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Patterns%20in%20sequences%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 555c2e60-a03e-4a9b-a180-8eea29f5caa7
updated: 1486069561
title: Patterns in sequences 1
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Zj-a_9cd5jc/default.jpg
    - https://i3.ytimg.com/vi/Zj-a_9cd5jc/1.jpg
    - https://i3.ytimg.com/vi/Zj-a_9cd5jc/2.jpg
    - https://i3.ytimg.com/vi/Zj-a_9cd5jc/3.jpg
---
