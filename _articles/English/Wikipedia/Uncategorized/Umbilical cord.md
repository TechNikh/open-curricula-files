---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Umbilical_cord
offline_file: ""
offline_thumbnail: ""
uuid: 039bd11f-b115-49bb-aef8-46b24901cdf7
updated: 1484308366
title: Umbilical cord
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Umbilicalcord.jpg
tags:
    - Physiology in humans
    - Development and composition
    - Connection to fetal circulatory system
    - Physiological postnatal occlusion
    - Problems and abnormalities
    - Medical protocols and procedures
    - Clamping and cutting
    - Early versus delayed clamping
    - Umbilical nonseverance
    - Umbilical cord catheterization
    - Blood sampling
    - Storage of cord blood
    - The umbilical cord in other animals
    - Other uses for the term "umbilical cord"
    - Cancer-causing toxins in human umbilical cords
    - Additional images
categories:
    - Uncategorized
---
In placental mammals, the umbilical cord (also called the navel string,[1] birth cord or funiculus umbilicalis) is a conduit between the developing embryo or fetus and the placenta. During prenatal development, the umbilical cord is physiologically and genetically part of the fetus and, (in humans), normally contains two arteries (the umbilical arteries) and one vein (the umbilical vein), buried within Wharton's jelly. The umbilical vein supplies the fetus with oxygenated, nutrient-rich blood from the placenta. Conversely, the fetal heart pumps deoxygenated, nutrient-depleted blood through the umbilical arteries back to the placenta.
