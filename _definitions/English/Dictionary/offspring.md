---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offspring
offline_file: ""
offline_thumbnail: ""
uuid: b1df9f41-a662-4f8e-bb54-5ad5009005eb
updated: 1484310301
title: offspring
categories:
    - Dictionary
---
offspring
     n 1: the immediate descendants of a person; "she was the mother
          of many offspring"; "he died without issue" [syn: {progeny},
           {issue}]
     2: something that comes into existence as a result;
        "industrialism prepared the way for acceptance of the
        French Revolution's various socialistic offspring"; "this
        skyscraper is the solid materialization of his efforts"
        [syn: {materialization}, {materialisation}]
     3: any immature animal [syn: {young}]
