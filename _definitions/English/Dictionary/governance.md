---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/governance
offline_file: ""
offline_thumbnail: ""
uuid: 27333e78-02e3-45b6-ac31-356f665af4c0
updated: 1484310529
title: governance
categories:
    - Dictionary
---
governance
     n 1: the persons (or committees or departments etc.) who make up
          a body for the purpose of administering something; "he
          claims that the present administration is corrupt"; "the
          governance of an association is responsible to its
          members"; "he quickly became recognized as a member of
          the establishment" [syn: {administration}, {governing
          body}, {establishment}, {brass}, {organization}, {organisation}]
     2: the act of governing; exercising authority; "regulations for
        the governing of state prisons"; "he had considerable
        experience of government" [syn: {government}, {governing},
         {government ...
