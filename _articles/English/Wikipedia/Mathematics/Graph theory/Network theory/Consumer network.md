---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Consumer_network
offline_file: ""
offline_thumbnail: ""
uuid: 1428c117-8436-4873-b97d-ed5be4913de2
updated: 1484309434
title: Consumer network
tags:
    - Economics
    - Marketing
    - Notes and references
categories:
    - Network theory
---
The notion of consumer networks expresses the idea that people’s embeddedness in social networks affects their behavior as consumers. Interactions within consumer networks such as information exchange and imitation can affect demand and market outcomes in ways not considered in the neoclassical theory of consumer choice.
