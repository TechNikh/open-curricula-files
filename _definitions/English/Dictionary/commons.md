---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commons
offline_file: ""
offline_thumbnail: ""
uuid: 288295a4-b2db-4709-a149-5cb2b2e753f1
updated: 1484310549
title: commons
categories:
    - Dictionary
---
commons
     n 1: a piece of open land for recreational use in an urban area;
          "they went for a walk in the park" [syn: {park}, {common},
           {green}]
     2: a pasture subject to common use [syn: {common land}]
     3: class composed of persons lacking noble or knightly or
        gentle rank [syn: {commonalty}, {commonality}]
