---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contracted
offline_file: ""
offline_thumbnail: ""
uuid: 00b10bf4-177c-4235-8df6-30e28dc73097
updated: 1484310335
title: contracted
categories:
    - Dictionary
---
contracted
     adj : reduced in size or pulled together; "the contracted pupils
           of her eyes" [ant: {expanded}]
