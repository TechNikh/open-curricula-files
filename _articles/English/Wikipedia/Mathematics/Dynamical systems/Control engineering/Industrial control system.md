---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Industrial_control_system
offline_file: ""
offline_thumbnail: ""
uuid: 77fd9290-ef51-4283-bda4-b91ddb5cd9c6
updated: 1484308735
title: Industrial control system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kontrollrom_Tyssedal.jpg
tags:
    - A historical perspective
    - Overview of control evolution
    - DCSs
    - DCS Structure
    - PLCs
    - Embedded control
    - Additional reading
categories:
    - Control engineering
---
Industrial control system (ICS) is a general term that encompasses several types of control systems and associated instrumentation used in industrial production, including supervisory control and data acquisition (SCADA) systems, distributed control systems (DCS), and other smaller control system configurations such as programmable logic controllers (PLC) often found in the industrial sectors and critical infrastructures.[1]
