---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approach
offline_file: ""
offline_thumbnail: ""
uuid: 97f02562-c440-4919-a35d-0f9c7f4cdcbd
updated: 1484310405
title: approach
categories:
    - Dictionary
---
approach
     n 1: ideas or actions intended to deal with a problem or
          situation; "his approach to every problem is to draw up
          a list of pros and cons"; "an attack on inflation"; "his
          plan of attack was misguided" [syn: {attack}, {plan of
          attack}]
     2: the act of drawing spatially closer to something; "the
        hunter's approach scattered the geese" [syn: {approaching},
         {coming}]
     3: a way of entering or leaving; "he took a wrong turn on the
        access to the bridge" [syn: {access}]
     4: the final path followed by an aircraft as it is landing
        [syn: {approach path}, {glide path}, {glide slope}]
     5: the event of one ...
