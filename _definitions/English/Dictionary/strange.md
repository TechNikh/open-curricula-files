---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strange
offline_file: ""
offline_thumbnail: ""
uuid: bcc5bc7a-3b06-4ff4-bef4-0c64f8931c9a
updated: 1484310181
title: strange
categories:
    - Dictionary
---
strange
     adj 1: being definitely out of the ordinary and unexpected;
            slightly odd or even a bit weird; "a strange
            exaltation that was indefinable"; "a strange
            fantastical mind"; "what a strange sense of humor she
            has" [syn: {unusual}] [ant: {familiar}]
     2: not known before; "used many strange words"; "saw many
        strange faces in the crowd"; "don't let anyone unknown
        into the house" [syn: {unknown}]
     3: not at ease or comfortable; "felt strange among so many
        important people"
