---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fermented
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483821
title: fermented
categories:
    - Dictionary
---
fermented
     adj : having undergone fermentation; "hard cider" [syn: {hard}]
