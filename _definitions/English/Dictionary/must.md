---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/must
offline_file: ""
offline_thumbnail: ""
uuid: 101373ea-d8e0-44f4-8e30-1d04ccd74c27
updated: 1484310303
title: must
categories:
    - Dictionary
---
must
     adj : highly recommended; "a book that is must reading" [syn: {must(a)}]
     n 1: a necessary or essential thing; "seat belts are an absolute
          must"
     2: grape juice before or during fermentation
     3: the quality of smelling or tasting old or stale or mouldy
        [syn: {mustiness}, {moldiness}]
