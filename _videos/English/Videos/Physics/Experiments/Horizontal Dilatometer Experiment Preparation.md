---
version: 1
type: video
provider: YouTube
id: JeVEyUeAz4s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1b344310-5395-4174-808a-da67f34c64f7
updated: 1486069635
title: Horizontal Dilatometer Experiment Preparation
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/JeVEyUeAz4s/default.jpg
    - https://i3.ytimg.com/vi/JeVEyUeAz4s/1.jpg
    - https://i3.ytimg.com/vi/JeVEyUeAz4s/2.jpg
    - https://i3.ytimg.com/vi/JeVEyUeAz4s/3.jpg
---
