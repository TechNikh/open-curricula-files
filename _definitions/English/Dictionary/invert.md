---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invert
offline_file: ""
offline_thumbnail: ""
uuid: 297f6ff7-968d-45ca-a6cb-03859789baa5
updated: 1484310373
title: invert
categories:
    - Dictionary
---
invert
     v 1: make an inversion (in a musical composition); "here the
          theme is inverted"
     2: turn inside out or upside down [syn: {reverse}]
