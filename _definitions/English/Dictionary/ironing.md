---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ironing
offline_file: ""
offline_thumbnail: ""
uuid: 338fd741-e336-4ca1-b5ee-29212f6d0050
updated: 1484310593
title: ironing
categories:
    - Dictionary
---
ironing
     n 1: garments (clothes or linens) that are to be (or have been)
          ironed; "there was a basketful of ironing to do"
     2: the work of ironing washed clothes
