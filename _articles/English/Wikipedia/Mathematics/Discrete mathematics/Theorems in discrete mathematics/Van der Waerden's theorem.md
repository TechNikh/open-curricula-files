---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Van_der_Waerden%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5455cbf2-12e9-4eea-bbac-65f1a9db6dcd
updated: 1484308664
title: "Van der Waerden's theorem"
tags:
    - "Proof of Van der Waerden's theorem (in a special case)"
    - Proof
categories:
    - Theorems in discrete mathematics
---
Van der Waerden's theorem is a theorem in the branch of mathematics called Ramsey theory. Van der Waerden's theorem states that for any given positive integers r and k, there is some number N such that if the integers {1, 2, ..., N} are colored, each with one of r different colors, then there are at least k integers in arithmetic progression all of the same color. The least such N is the Van der Waerden number W(r, k). It is named after the Dutch mathematician B. L. van der Waerden.[1]
