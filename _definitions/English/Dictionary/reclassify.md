---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reclassify
offline_file: ""
offline_thumbnail: ""
uuid: be6cac56-40da-47a6-8410-6c962275fbf0
updated: 1484310218
title: reclassify
categories:
    - Dictionary
---
reclassify
     v : classify anew, change the previous classification; "The
         zoologists had to reclassify the mollusks after they
         found new species"
     [also: {reclassified}]
