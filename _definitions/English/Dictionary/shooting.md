---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shooting
offline_file: ""
offline_thumbnail: ""
uuid: 684dcbce-da94-4237-90a6-52e753e94b63
updated: 1484310178
title: shooting
categories:
    - Dictionary
---
shooting
     n 1: the act of firing a projectile; "his shooting was slow but
          accurate" [syn: {shot}]
     2: killing someone by gunfire; "when the shooting stopped there
        were three dead bodies"
