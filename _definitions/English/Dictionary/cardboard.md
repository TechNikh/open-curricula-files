---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cardboard
offline_file: ""
offline_thumbnail: ""
uuid: 20ecf42b-e6fb-4db8-bbc8-e6bf9f39b3a6
updated: 1484310249
title: cardboard
categories:
    - Dictionary
---
cardboard
     adj 1: resembling cardboard especially in flimsiness; "apartments
            with cardboard walls" [syn: {flimsy}]
     2: without substance; "cardboard caricatures of historical
        figures" [syn: {unlifelike}]
     n : a stiff moderately thick paper [syn: {composition board}]
