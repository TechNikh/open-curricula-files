---
title: Groundwater
article_ref: https://en.wikipedia.org/wiki/Groundwater
tags:
    - Aquifers
    - Water cycle
    - Issues
    - Overview
    - Overdraft
    - Subsidence
    - Seawater intrusion
    - pollution
    - Government regulations
    - Rule of Capture
    - Riparian Rights
    - Environmental protection of groundwater
    - Others
    - Reasonable Use Rule (American Rule)
    - Groundwater scrutiny upon real estate property transactions in the US
categories:
    - Water Shed
---
