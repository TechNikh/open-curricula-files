---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bridging_model
offline_file: ""
offline_thumbnail: ""
uuid: ef446310-17d3-443b-9b32-cc488dc521eb
updated: 1484308667
title: Bridging model
categories:
    - Theoretical computer science
---
In computer science, a bridging model is an abstract model of a computer which provides a conceptual bridge between the physical implementation of the machine and the abstraction available to a programmer of that machine; in other words, it is intended to provide a common level of understanding between hardware and software engineers.
