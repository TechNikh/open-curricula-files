---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diminish
offline_file: ""
offline_thumbnail: ""
uuid: 184b96c8-6182-4e71-bd97-259879ac118c
updated: 1484310563
title: diminish
categories:
    - Dictionary
---
diminish
     v 1: decrease in size, extent, or range; "The amount of homework
          decreased towards the end of the semester"; "The cabin
          pressure fell dramatically"; "her weight fall to under a
          hundred pounds"; "his voice fell to a whisper" [syn: {decrease},
           {lessen}, {fall}] [ant: {increase}]
     2: lessen the authority, dignity, or reputation of; "don't
        belittle your colleagues" [syn: {belittle}]
