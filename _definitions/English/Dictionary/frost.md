---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frost
offline_file: ""
offline_thumbnail: ""
uuid: 19d15da6-8a26-4f93-9bf4-e008c4d23a98
updated: 1484310148
title: frost
categories:
    - Dictionary
---
frost
     n 1: ice crystals forming a white deposit (especially on objects
          outside) [syn: {hoar}, {hoarfrost}, {rime}]
     2: weather cold enough to cause freezing [syn: {freeze}]
     3: the formation of frost or ice on a surface [syn: {icing}]
     4: United States poet famous for his lyrical poems on country
        life in New England (1874-1963) [syn: {Robert Frost}, {Robert
        Lee Frost}]
     v 1: decorate with frosting; "frost a cake" [syn: {ice}]
     2: provide with a rough or speckled surface or appearance;
        "frost the glass"; "she frosts her hair"
     3: cover with frost; "ice crystals frosted the glass"
     4: damage by frost; "The icy precipitation ...
