---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moving_sofa_problem
offline_file: ""
offline_thumbnail: ""
uuid: 0f1b92f2-c28b-47e5-84f7-d76bd9647199
updated: 1484308612
title: Moving sofa problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hammersley_sofa_animated.gif
tags:
    - History
    - Lower and upper bounds
categories:
    - Discrete geometry
---
The moving sofa problem or sofa problem is a two-dimensional idealisation of real-life furniture-moving problems, and asks for the rigid two-dimensional shape of largest area A that can be maneuvered through an L-shaped planar region with legs of unit width.[1] The area A thus obtained is referred to as the sofa constant. The exact value of the sofa constant is an open problem.
