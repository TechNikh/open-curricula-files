---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Temperature%E2%80%93entropy_diagram'
offline_file: ""
offline_thumbnail: ""
uuid: 0e94babf-bc58-4f8c-83ec-c50d5acfe807
updated: 1484308860
title: Temperature–entropy diagram
categories:
    - Thermodynamics
---
A temperature entropy diagram, or T-s diagram, is used in thermodynamics to visualize changes to temperature and specific entropy during a thermodynamic process or cycle. It is a useful and common tool, particularly because it helps to visualize the heat transfer during a process. For reversible (ideal) processes, the area under the T-s curve of a process is the heat transferred to the system during that process.[1]
