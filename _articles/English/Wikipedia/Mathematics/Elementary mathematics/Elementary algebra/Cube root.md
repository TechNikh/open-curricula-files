---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cube_root
offline_file: ""
offline_thumbnail: ""
uuid: 5223eb95-817c-4ab2-89d8-90967ad998b6
updated: 1484308903
title: Cube root
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/288px-Cube-root_function.svg.png
tags:
    - Formal definition
    - Real numbers
    - Complex numbers
    - Impossibility of compass-and-straightedge construction
    - Numerical methods
    - Appearance in solutions of third and fourth degree equations
    - History
categories:
    - Elementary algebra
---
In mathematics, a cube root of a number x is a number such that a3 = x. All real numbers (except zero) have exactly one real cube root and a pair of complex conjugate cube roots, and all nonzero complex numbers have three distinct complex cube roots. For example, the real cube root of 8, denoted 3√8, is 2, because 23 = 8, while the other cube roots of 8 are −1 + √3i and −1 − √3i. The three cube roots of −27i are
