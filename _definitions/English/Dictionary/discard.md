---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discard
offline_file: ""
offline_thumbnail: ""
uuid: c134c451-e7a7-4555-8594-724c08056d8f
updated: 1484310341
title: discard
categories:
    - Dictionary
---
discard
     n 1: anything that is cast aside or discarded
     2: (cards) the act of throwing out a useless card or to failing
        to follow suit
     3: getting rid something that is regarded as useless or
        undesirable [syn: {throwing away}]
     v : throw or cast away; "Put away your worries" [syn: {fling}, {toss},
          {toss out}, {toss away}, {chuck out}, {cast aside}, {dispose},
          {throw out}, {cast out}, {throw away}, {cast away}, {put
         away}]
