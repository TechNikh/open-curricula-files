---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shock
offline_file: ""
offline_thumbnail: ""
uuid: 3109bcb3-6ee1-484c-bc82-9a4f69a39b50
updated: 1484310200
title: shock
categories:
    - Dictionary
---
shock
     n 1: the feeling of distress and disbelief that you have when
          something bad happens accidentally; "his mother's
          deathleft him in a daze"; "he was numb with shock" [syn:
           {daze}, {stupor}]
     2: the violent interaction of individuals or groups entering
        into combat; "the armies met in the shock of battle" [syn:
         {impact}]
     3: a reflex response to the passage of electric current through
        the body; "subjects received a small electric shock when
        they mae the wrong response"; "electricians get accustomed
        to occasional shocks" [syn: {electric shock}, {electrical
        shock}]
     4: (pathology) bodily collapse ...
