---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beam
offline_file: ""
offline_thumbnail: ""
uuid: 41bdccb8-6fb8-41dd-9638-634f82819685
updated: 1484310220
title: beam
categories:
    - Dictionary
---
beam
     n 1: a signal transmitted along a narrow path; guides pilots in
          darkness or bad weather [syn: {radio beam}]
     2: long thick piece of wood or metal or concrete, etc., used in
        construction
     3: a column of light (as from a beacon) [syn: {beam of light},
        {light beam}, {ray}, {ray of light}, {shaft}, {shaft of
        light}, {irradiation}]
     4: a group of nearly parallel lines of electromagnetic
        radiation [syn: {ray}, {electron beam}]
     5: (nautical) breadth amidships
     6: a gymnastic apparatus used by women gymnasts [syn: {balance
        beam}]
     v 1: smile radiantly; express joy through one's facial expression
     2: emit light; ...
