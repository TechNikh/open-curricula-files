---
version: 1
type: video
provider: YouTube
id: -w-VTw0tQlE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b184389a-9d8f-4973-989f-cb5f2d2af3f9
updated: 1486069682
title: 'Circuit Analysis- Crash Course Physics #30'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/-w-VTw0tQlE/default.jpg
    - https://i3.ytimg.com/vi/-w-VTw0tQlE/1.jpg
    - https://i3.ytimg.com/vi/-w-VTw0tQlE/2.jpg
    - https://i3.ytimg.com/vi/-w-VTw0tQlE/3.jpg
---
