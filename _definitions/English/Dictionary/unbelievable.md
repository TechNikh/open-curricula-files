---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unbelievable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556601
title: unbelievable
categories:
    - Dictionary
---
unbelievable
     adj 1: beyond belief or understanding; "at incredible speed"; "the
            book's plot is simply incredible" [syn: {incredible}]
            [ant: {credible}]
     2: having a probability to low to inspire belief [syn: {improbable},
         {unconvincing}, {unlikely}]
