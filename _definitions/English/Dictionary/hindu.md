---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hindu
offline_file: ""
offline_thumbnail: ""
uuid: cbcabb90-36db-4c49-81d6-091c25545c55
updated: 1484310443
title: hindu
categories:
    - Dictionary
---
Hindu
     adj : of or relating to or supporting Hinduism; "the Hindu faith"
           [syn: {Hindi}, {Hindoo}]
     n 1: a native or inhabitant of Hindustan or India [syn: {Hindoo},
           {Hindustani}]
     2: a person who adheres to Hinduism [syn: {Hindoo}]
