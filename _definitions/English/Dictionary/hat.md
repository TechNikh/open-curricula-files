---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hat
offline_file: ""
offline_thumbnail: ""
uuid: 26664c2f-685f-44c9-a195-334cedeb83ce
updated: 1484310150
title: hat
categories:
    - Dictionary
---
hat
     n 1: headdress that protects the head from bad weather; has
          shaped crown and usually a brim [syn: {chapeau}, {lid}]
     2: an informal term for a person's role; "he took off his
        politician's hat and talked frankly"
     v 1: put on or wear a hat; "He was unsuitably hatted"
     2: furnish with a hat
     [also: {hatting}, {hatted}]
