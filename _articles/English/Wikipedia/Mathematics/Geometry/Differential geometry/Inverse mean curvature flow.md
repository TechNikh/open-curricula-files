---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inverse_mean_curvature_flow
offline_file: ""
offline_thumbnail: ""
uuid: 721d5f25-a683-45a9-9627-34f048fb1441
updated: 1484309224
title: Inverse mean curvature flow
tags:
    - 'Example: a round sphere'
    - 'Generalization: weak IMCF'
    - Monotonicity of the Hawking mass
    - Applications
categories:
    - Differential geometry
---
In the field of differential geometry in mathematics, inverse mean curvature flow (IMCF) is an example of a geometric flow of hypersurfaces of a Riemannian manifold (for example, smooth surfaces in 3-dimensional Euclidean space). Intuitively, a family of surfaces evolves under IMCF if the outward normal speed at which a point on the surface moves is given by the reciprocal of the mean curvature of the surface. For example, a round sphere evolves under IMCF by expanding outward uniformly at an exponentially growing rate (see below). In general, this flow does not exist (for example, if a point on the surface has zero mean curvature), and even if it does, it generally develops singularities. ...
