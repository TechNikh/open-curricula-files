---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emitter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484612641
title: emitter
categories:
    - Dictionary
---
emitter
     n : the electrode in a transistor where electrons originate
