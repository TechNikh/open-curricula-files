---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pulp
offline_file: ""
offline_thumbnail: ""
uuid: 542b746a-9509-4138-befa-bbeea97e0db3
updated: 1484310387
title: pulp
categories:
    - Dictionary
---
pulp
     n 1: any soft or soggy mass; "he pounded it to a pulp" [syn: {mush}]
     2: a soft moist part of a fruit [syn: {flesh}]
     3: a mixture of cellulose fibers
     4: an inexpensive magazine printed on poor quality paper [syn:
        {pulp magazine}]
     5: the soft inner part of a tooth
     v 1: remove the pulp from, as from a fruit
     2: reduce to pulp; "pulp fruit"; "pulp wood"
