---
version: 1
type: video
provider: YouTube
id: W_Q17tqw_7A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Oxidative%20Phosphorylation%20and%20Chemiosmosis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 201b29b9-95fd-4d22-83e3-e22be18e1e1d
updated: 1486069581
title: Oxidative Phosphorylation and Chemiosmosis
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/W_Q17tqw_7A/default.jpg
    - https://i3.ytimg.com/vi/W_Q17tqw_7A/1.jpg
    - https://i3.ytimg.com/vi/W_Q17tqw_7A/2.jpg
    - https://i3.ytimg.com/vi/W_Q17tqw_7A/3.jpg
---
Oxidative Phosphorylation and Chemiosmosis (along with slight correction of previous video)
More free lessons at: http://www.khanacademy.org/video?v=W_Q17tqw_7A
