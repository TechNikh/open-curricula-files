---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surging
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580601
title: surging
categories:
    - Dictionary
---
surging
     adj : characterized by great swelling waves or surges; "billowy
           storm clouds"; "the restless billowing sea"; "surging
           waves" [syn: {billowy}, {billowing(a)}]
