---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prosthaphaeresis
offline_file: ""
offline_thumbnail: ""
uuid: 2a9fba05-1a7e-46e8-85ef-84421e348ade
updated: 1484309347
title: Prosthaphaeresis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-RechtwKugeldreieck.svg.png
tags:
    - History and motivation
    - The identities
    - The algorithm
    - Decreasing the error
    - Reverse identities
categories:
    - Trigonometry
---
Prosthaphaeresis (from the Greek προσθαφαίρεσις) was an algorithm used in the late 16th century and early 17th century for approximate multiplication and division using formulas from trigonometry. For the 25 years preceding the invention of the logarithm in 1614, it was the only known generally applicable way of approximating products quickly. Its name comes from the Greek prosthesis and aphaeresis, meaning addition and subtraction, two steps in the process.[1][2]
