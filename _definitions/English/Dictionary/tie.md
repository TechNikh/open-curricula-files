---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tie
offline_file: ""
offline_thumbnail: ""
uuid: 4cc17888-5015-4b41-83bb-651887bc793d
updated: 1484310375
title: tie
categories:
    - Dictionary
---
tie
     n 1: neckwear consisting of a long narrow piece of material worn
          (mostly by men) under a collar and tied in knot at the
          front; "he stood in front of the mirror tightening his
          necktie"; "he wore a vest and tie" [syn: {necktie}]
     2: a social or business relationship; "a valuable financial
        affiliation"; "he was sorry he had to sever his ties with
        other members of the team"; "many close associations with
        England" [syn: {affiliation}, {association}, {tie-up}]
     3: the finish of a contest in which the score is tied and the
        winner is undecided; "the game ended in a draw"; "their
        record was 3 wins, 6 losses and a ...
