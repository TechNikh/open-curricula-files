---
version: 1
type: video
provider: YouTube
id: 9B7Ix2VQEGo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 54408417-749c-44b7-991c-3a540e438fba
updated: 1486069684
title: 'The Big Bang, Cosmology part 1- Crash Course Astronomy #42'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/9B7Ix2VQEGo/default.jpg
    - https://i3.ytimg.com/vi/9B7Ix2VQEGo/1.jpg
    - https://i3.ytimg.com/vi/9B7Ix2VQEGo/2.jpg
    - https://i3.ytimg.com/vi/9B7Ix2VQEGo/3.jpg
---
