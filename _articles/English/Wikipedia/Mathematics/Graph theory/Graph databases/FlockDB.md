---
version: 1
type: article
id: https://en.wikipedia.org/wiki/FlockDB
offline_file: ""
offline_thumbnail: ""
uuid: a1acd134-6e52-4e1b-9e49-40a557668926
updated: 1484309405
title: FlockDB
categories:
    - Graph databases
---
FlockDB is an open source distributed, fault-tolerant graph database for managing wide but shallow network graphs.[2] It was initially used by Twitter to store relationships between users, e.g. followings and favorites. FlockDB differs from other graph databases, e.g. Neo4j in that it is not designed for multi-hop graph traversal but rather for rapid set operations, not unlike the primary use-case for Redis sets.[3] Since it is still in the process of being packaged for outside of Twitter use, the code is still very rough and hence there is no stable release available yet. FlockDB was posted on GitHub shortly after Twitter released its Gizzard framework, which it uses to query the FlockDB ...
