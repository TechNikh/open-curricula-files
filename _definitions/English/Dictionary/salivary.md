---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salivary
offline_file: ""
offline_thumbnail: ""
uuid: c6c452c0-97f5-4845-a38c-484ec61f1093
updated: 1484310343
title: salivary
categories:
    - Dictionary
---
salivary
     adj : of or relating to saliva; "salivary gland"
