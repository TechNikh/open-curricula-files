---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marriage
offline_file: ""
offline_thumbnail: ""
uuid: c3f73e7f-ff21-4be4-a16d-5d8b7cab74cc
updated: 1484310448
title: marriage
categories:
    - Dictionary
---
marriage
     n 1: the state of being a married couple voluntarily joined for
          life (or until divorce); "a long and happy marriage";
          "God bless this union" [syn: {matrimony}, {union}, {spousal
          relationship}, {wedlock}]
     2: two people who are married to each other; "his second
        marriage was happier than the first"; "a married couple
        without love" [syn: {married couple}, {man and wife}]
     3: the act of marrying; the nuptial ceremony; "their marriage
        was conducted in the chapel" [syn: {wedding}, {marriage
        ceremony}]
     4: a close and intimate union; "the marriage of music and
        dance"; "a marriage of ideas"
