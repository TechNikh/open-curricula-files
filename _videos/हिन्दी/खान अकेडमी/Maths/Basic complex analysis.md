---
version: 1
type: video
provider: YouTube
id: c6kAoQ9eTqE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5090304a-5143-498b-bcc0-47c6eb359ef9
updated: 1486069613
title: Basic complex analysis
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/c6kAoQ9eTqE/default.jpg
    - https://i3.ytimg.com/vi/c6kAoQ9eTqE/1.jpg
    - https://i3.ytimg.com/vi/c6kAoQ9eTqE/2.jpg
    - https://i3.ytimg.com/vi/c6kAoQ9eTqE/3.jpg
---
