---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stochastic_cellular_automaton
offline_file: ""
offline_thumbnail: ""
uuid: 09ea728b-f39f-4545-8686-94e0258e1d31
updated: 1484308822
title: Stochastic cellular automaton
tags:
    - PCA as Markov stochastic processes
    - Examples of stochastic cellular automaton
    - Majority cellular automaton
    - Relation to random fields
    - Cellular Potts model
    - Additional reading
categories:
    - Self-organization
---
Stochastic cellular automata or 'probabilistic cellular automata' (PCA) or 'random cellular automata' or locally interacting Markov chains[1][2] are an important extension of cellular automaton. Cellular automata are a discrete-time dynamical system of interacting entities, whose state is discrete.
