---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confused
offline_file: ""
offline_thumbnail: ""
uuid: 46bf7791-ea41-40bd-af0b-56da12b5e312
updated: 1484310140
title: confused
categories:
    - Dictionary
---
confused
     adj 1: perplexed by many conflicting situations or statements;
            filled with bewilderment; "obviously bemused by his
            questions"; "bewildered and confused"; "a cloudy and
            confounded philosopher"; "just a mixed-up kid"; "she
            felt lost on the first day of school" [syn: {baffled},
             {befuddled}, {bemused}, {bewildered}, {confounded}, {lost},
             {mazed}, {mixed-up}, {at sea}]
     2: lacking orderly continuity; "a confused set of
        instructions"; "a confused dream about the end of the
        world"; "disconnected fragments of a story"; "scattered
        thoughts" [syn: {disconnected}, {disjointed}, ...
