---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lane
offline_file: ""
offline_thumbnail: ""
uuid: e691203c-bd9f-4b2a-9f51-bf71b5a35122
updated: 1484310166
title: lane
categories:
    - Dictionary
---
lane
     n 1: a narrow way or road
     2: a well-defined track or path; for e.g. swimmers or lines of
        traffic
