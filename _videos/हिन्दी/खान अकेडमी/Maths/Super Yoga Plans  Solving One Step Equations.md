---
version: 1
type: video
provider: YouTube
id: eP3LPrCpzOw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d8e61d84-5ff2-466b-babc-a7a45a42e937
updated: 1486069605
title: 'Super Yoga Plans  Solving One Step Equations'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/eP3LPrCpzOw/default.jpg
    - https://i3.ytimg.com/vi/eP3LPrCpzOw/1.jpg
    - https://i3.ytimg.com/vi/eP3LPrCpzOw/2.jpg
    - https://i3.ytimg.com/vi/eP3LPrCpzOw/3.jpg
---
