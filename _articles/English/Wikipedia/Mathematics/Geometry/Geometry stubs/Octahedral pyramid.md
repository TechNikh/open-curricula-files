---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Octahedral_pyramid
offline_file: ""
offline_thumbnail: ""
uuid: 2d9a7312-80aa-4c55-8245-b532833cca25
updated: 1484309365
title: Octahedral pyramid
tags:
    - Occurrences of the octahedral pyramid
    - Other polytopes
    - Square-pyramidal pyramid
categories:
    - Geometry stubs
---
In 4-dimensional geometry, the octahedral pyramid is bounded by one octahedron on the base and 8 triangular pyramid cells which meet at the apex. Since an octahedron has a circumradius divided by edge length less than one,[1] the triangular pyramids can made with regular faces (as regular tetrahedrons) by computing the appropriate height.
