---
version: 1
type: video
provider: YouTube
id: 8dsTvBaUMvw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 73263dd4-e4cd-49ed-8875-ef153ec03bb6
updated: 1486069673
title: mRNA Translation (Basic)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8dsTvBaUMvw/default.jpg
    - https://i3.ytimg.com/vi/8dsTvBaUMvw/1.jpg
    - https://i3.ytimg.com/vi/8dsTvBaUMvw/2.jpg
    - https://i3.ytimg.com/vi/8dsTvBaUMvw/3.jpg
---
