---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transform
offline_file: ""
offline_thumbnail: ""
uuid: d1434cd7-7754-4356-a3c1-687bb20a9ffb
updated: 1484310176
title: transform
categories:
    - Dictionary
---
transform
     v 1: subject to a mathematical transformation
     2: change or alter in form, appearance, or nature; "This
        experience transformed her completely"; "She transformed
        the clay into a beautiful sculpture"; "transubstantiate
        one element into another" [syn: {transmute}, {transubstantiate}]
     3: change in outward structure or looks; "He transformed into a
        monster"; "The salesman metamorphosed into an ugly beetle"
        [syn: {transmute}, {metamorphose}]
     4: change from one form or medium into another; "Braque
        translated collage into oil" [syn: {translate}]
     5: convert (one form of energy) to another; "transform energy
        to ...
