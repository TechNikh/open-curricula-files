---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Quasi-crystals_(supramolecular)
offline_file: ""
offline_thumbnail: ""
uuid: 0b7d1008-8e00-4333-a740-c556008020de
updated: 1484308816
title: Quasi-crystals (supramolecular)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Formation_of_merocyanine_dipoles_upon_irradiation.jpg
categories:
    - Self-organization
---
Self-organized structures termed "quasi-crystals" were originally described in 1978 by the Israeli scientist Valeri A. Krongauz of the Weizmann Institute of Science, in the Nature paper, Quasi-crystals from irradiated photochromic dyes in an applied electric field. [1]
