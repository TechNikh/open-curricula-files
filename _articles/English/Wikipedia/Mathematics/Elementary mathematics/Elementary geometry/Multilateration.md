---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multilateration
offline_file: ""
offline_thumbnail: ""
uuid: 71619093-d6b9-4745-a6bc-5ada885f5340
updated: 1484308935
title: Multilateration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HyperboloidOfTwoSheets.svg.png
tags:
    - Principle
    - 'Surveillance application: locating a transmitter from multiple receiver sites'
    - 'Navigation application: locating a receiver from multiple transmitter sites'
    - TDOA geometry
    - Rectangular/Cartesian coordinates
    - Spherical coordinates
    - Measuring the time difference in a TDOA system
    - Solution algorithms
    - Overview
    - Solution with limited computational resources
    - Two-dimensional solution
    - Accuracy
    - Example applications
    - Simplification
    - Notes
categories:
    - Elementary geometry
---
Multilateration (MLAT) is a navigation technique based on the measurement of the difference in distance to two stations at known locations that broadcast signals at known times. Unlike measurements of absolute distance or angle, measuring the difference in distance between two stations results in an infinite number of locations that satisfy the measurement. When these possible locations are plotted, they form a hyperbolic curve. To locate the exact location along that curve, multilateration relies on multiple measurements: a second measurement taken to a different pair of stations will produce a second curve, which intersects with the first. When the two curves are compared, a small number ...
