---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vector_projection
offline_file: ""
offline_thumbnail: ""
uuid: 67ba7062-bf3c-4514-a6cc-1ba1cf671caf
updated: 1484309338
title: Vector projection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Projection_and_rejection.png
tags:
    - Notation
    - Definitions based on angle θ
    - Scalar projection
    - Vector projection
    - Vector rejection
    - Definitions in terms of a and b
    - Scalar projection
    - Vector projection
    - Vector rejection
    - Properties
    - Scalar projection
    - Vector projection
    - Vector rejection
    - Matrix representation
    - Uses
    - Generalizations
categories:
    - Transformation (function)
---
The vector projection of a vector a on (or onto) a nonzero vector b (also known as the vector component or vector resolution of a in the direction of b) is the orthogonal projection of a onto a straight line parallel to b. It is a vector parallel to b, defined as
