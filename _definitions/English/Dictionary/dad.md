---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dad
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408881
title: dad
categories:
    - Dictionary
---
dad
     n : an informal term for a father; probably derived from baby
         talk [syn: {dada}, {daddy}, {pa}, {papa}, {pappa}, {pater},
          {pop}]
