---
version: 1
type: video
provider: YouTube
id: f-wz_ZzSDdg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Multiplying%20and%20dividing%20rational%20expressions%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9fee3b96-5591-4c20-95f9-8cc15918e8de
updated: 1486069565
title: Multiplying and dividing rational expressions 2
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/f-wz_ZzSDdg/default.jpg
    - https://i3.ytimg.com/vi/f-wz_ZzSDdg/1.jpg
    - https://i3.ytimg.com/vi/f-wz_ZzSDdg/2.jpg
    - https://i3.ytimg.com/vi/f-wz_ZzSDdg/3.jpg
---
