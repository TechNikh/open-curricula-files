---
version: 1
type: video
provider: YouTube
id: E94G5FyhofI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Capacitance%20Animation%20Lesson%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 460995ec-3f4c-4ea7-976d-be442036b99d
updated: 1486069653
title: What is Capacitance Animation Lesson 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/E94G5FyhofI/default.jpg
    - https://i3.ytimg.com/vi/E94G5FyhofI/1.jpg
    - https://i3.ytimg.com/vi/E94G5FyhofI/2.jpg
    - https://i3.ytimg.com/vi/E94G5FyhofI/3.jpg
---
