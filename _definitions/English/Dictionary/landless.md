---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landless
offline_file: ""
offline_thumbnail: ""
uuid: 85f9b410-fbe4-4ce2-8cd3-d5eff4564b24
updated: 1484310439
title: landless
categories:
    - Dictionary
---
landless
     adj : owning no land; "the landless peasantry" [ant: {landed}]
