---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_geometry_of_curves
offline_file: ""
offline_thumbnail: ""
uuid: 896f6693-4a97-44c9-93a1-00464929c42b
updated: 1484309202
title: Affine geometry of curves
tags:
    - The affine frame
    - Discrete invariant
    - Curvature
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, the affine geometry of curves is the study of curves in an affine space, and specifically the properties of such curves which are invariant under the special affine group 
  
    
      
        
          SL
        
        (
        n
        ,
        
          R
        
        )
        ⋉
        
          
            R
          
          
            n
          
        
        .
      
    
    {\displaystyle {\mbox{SL}}(n,\mathbb {R} )\ltimes \mathbb {R} ^{n}.}
