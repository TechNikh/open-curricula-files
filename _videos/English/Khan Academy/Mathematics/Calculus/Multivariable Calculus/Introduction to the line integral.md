---
version: 1
type: video
provider: YouTube
id: _60sKaoRmhU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Introduction%20to%20the%20line%20integral.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7060d3c7-7495-4cef-9f6c-31915d1faedb
updated: 1486069689
title: Introduction to the line integral
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/_60sKaoRmhU/default.jpg
    - https://i3.ytimg.com/vi/_60sKaoRmhU/1.jpg
    - https://i3.ytimg.com/vi/_60sKaoRmhU/2.jpg
    - https://i3.ytimg.com/vi/_60sKaoRmhU/3.jpg
---
