---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orientation_sheaf
offline_file: ""
offline_thumbnail: ""
uuid: 78a024f9-afaa-40a2-892e-02eb088b052f
updated: 1484309297
title: Orientation sheaf
categories:
    - Orientation (geometry)
---
In algebraic topology, the orientation sheaf on a manifold X of dimension n is a locally constant sheaf oX on X such that the stalk of oX at a point x is
