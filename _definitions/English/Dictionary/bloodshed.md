---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bloodshed
offline_file: ""
offline_thumbnail: ""
uuid: 1b8f0cf9-c7d9-4485-b1dc-4b2ec0719217
updated: 1484310589
title: bloodshed
categories:
    - Dictionary
---
bloodshed
     n 1: the shedding of blood resulting in murder; "he avenged the
          blood of his kinsmen" [syn: {blood}, {gore}]
     2: indiscriminate slaughter [syn: {bloodbath}, {bloodletting},
        {battue}]
