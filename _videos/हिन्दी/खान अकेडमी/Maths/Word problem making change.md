---
version: 1
type: video
provider: YouTube
id: BjrTVTau8YU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 258635bd-d248-46ed-827c-28db35359205
updated: 1486069605
title: Word problem making change
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/BjrTVTau8YU/default.jpg
    - https://i3.ytimg.com/vi/BjrTVTau8YU/1.jpg
    - https://i3.ytimg.com/vi/BjrTVTau8YU/2.jpg
    - https://i3.ytimg.com/vi/BjrTVTau8YU/3.jpg
---
