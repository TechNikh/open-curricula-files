---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/construction
offline_file: ""
offline_thumbnail: ""
uuid: f75ada70-9db6-4360-9099-5b9c05e77a34
updated: 1484310262
title: construction
categories:
    - Dictionary
---
construction
     n 1: the act of constructing or building something; "during the
          construction we had to take a detour"; "his hobby was
          the building of boats" [syn: {building}]
     2: the commercial activity involved in constructing buildings;
        "their main business is home construction"; "workers in
        the building trades" [syn: {building}]
     3: a thing constructed; a complex construction or entity; "the
        structure consisted of a series of arches"; "she wore her
        hair in an amazing construction of whirls and ribbons"
        [syn: {structure}]
     4: a group of words that form a constituent of a sentence and
        are considered as a ...
