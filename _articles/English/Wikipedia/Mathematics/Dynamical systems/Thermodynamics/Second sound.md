---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Second_sound
offline_file: ""
offline_thumbnail: ""
uuid: 98afd5e0-2500-44b9-915b-8fedec1f03da
updated: 1484308860
title: Second sound
tags:
    - Second sound in helium II
    - Second sound in other media
    - Applications
    - Bibliography
categories:
    - Thermodynamics
---
Second sound is a quantum mechanical phenomenon in which heat transfer occurs by wave-like motion, rather than by the more usual mechanism of diffusion. Heat takes the place of pressure in normal sound waves. This leads to a very high thermal conductivity. It is known as "second sound" because the wave motion of heat is similar to the propagation of sound in air.
