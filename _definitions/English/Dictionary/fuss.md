---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fuss
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484552041
title: fuss
categories:
    - Dictionary
---
fuss
     n 1: an excited state of agitation; "he was in a dither"; "there
          was a terrible flap about the theft" [syn: {dither}, {pother},
           {tizzy}, {flap}]
     2: an angry disturbance; "he didn't want to make a fuss"; "they
        had labor trouble"; "a spot of bother" [syn: {trouble}, {bother},
         {hassle}]
     3: a quarrel about petty points [syn: {bicker}, {bickering}, {spat},
         {tiff}, {squabble}, {pettifoggery}]
     4: a rapid bustling commotion [syn: {bustle}, {hustle}, {flurry},
         {ado}, {stir}]
     v 1: worry unnecessarily or excessively; "don't fuss too much
          over the grandchildren--they are quite big now" [syn: {niggle},
       ...
