---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hallmark
offline_file: ""
offline_thumbnail: ""
uuid: 67993efe-e1d6-4781-b4cb-c5d0b9ad78cf
updated: 1484310531
title: hallmark
categories:
    - Dictionary
---
hallmark
     n 1: a distinctive characteristic or attribute [syn: {trademark},
           {earmark}, {stylemark}]
     2: a mark on an article of trade to indicate its origin and
        authenticity [syn: {authentication}, {assay-mark}]
