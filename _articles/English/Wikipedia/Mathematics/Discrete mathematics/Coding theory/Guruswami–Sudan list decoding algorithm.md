---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Guruswami%E2%80%93Sudan_list_decoding_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 3787d944-21ba-46ca-be7d-07c515bdde64
updated: 1484308551
title: Guruswami–Sudan list decoding algorithm
tags:
    - "Algorithm 1 (Sudan's list decoding algorithm)"
    - Problem statement
    - Algorithm
    - Analysis
    - Algorithm 2 (Guruswami–Sudan list decoding algorithm)
    - Definition
    - Multiplicity
    - General definition of multiplicity
    - Algorithm
    - Analysis
    - Interpolation step
    - Factorization step
categories:
    - Coding theory
---
In coding theory, list decoding is an alternative to unique decoding of error-correcting codes for large error rates. Using unique decoder one can correct up to 
  
    
      
        δ
        
          /
        
        2
      
    
    {\displaystyle \delta /2}
  
fraction of errors. But when error rate is greater than 
  
    
      
        δ
        
          /
        
        2
      
    
    {\displaystyle \delta /2}
  
, unique decoder will not able to output the correct result. List decoding overcomes that issue. List decoding can correct more than 
  
    
      
        δ
        
          /
        
        2
      
    
    {\displaystyle \delta /2}
  
 fraction of ...
