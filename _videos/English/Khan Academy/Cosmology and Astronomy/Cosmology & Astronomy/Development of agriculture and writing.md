---
version: 1
type: video
provider: YouTube
id: iU58AIjh3YA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Development%20of%20agriculture%20and%20writing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 21db1a94-0dc0-417b-bdf7-45bc9e7ad0ec
updated: 1486069685
title: Development of agriculture and writing
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/iU58AIjh3YA/default.jpg
    - https://i3.ytimg.com/vi/iU58AIjh3YA/1.jpg
    - https://i3.ytimg.com/vi/iU58AIjh3YA/2.jpg
    - https://i3.ytimg.com/vi/iU58AIjh3YA/3.jpg
---
