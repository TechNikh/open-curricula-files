---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strong_Nash_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 81f32f37-357b-42fc-9dc0-e89b3d294e46
updated: 1484309035
title: Strong Nash equilibrium
categories:
    - Game theory equilibrium concepts
---
A strong Nash equilibrium is a Nash equilibrium in which no coalition, taking the actions of its complements as given, can cooperatively deviate in a way that benefits all of its members.[1] While the Nash concept of stability defines equilibrium only in terms of unilateral deviations, strong Nash equilibrium allows for deviations by every conceivable coalition.[2] This equilibrium concept is particularly useful in areas such as the study of voting systems, in which there are typically many more players than possible outcomes, and so plain Nash equilibria are far too abundant.
