---
version: 1
type: video
provider: YouTube
id: RJlEH5Jz80w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Electron%20Configurations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 34b245ca-0f56-4848-a77e-774290f57ddd
updated: 1486069587
title: Electron Configurations
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/RJlEH5Jz80w/default.jpg
    - https://i3.ytimg.com/vi/RJlEH5Jz80w/1.jpg
    - https://i3.ytimg.com/vi/RJlEH5Jz80w/2.jpg
    - https://i3.ytimg.com/vi/RJlEH5Jz80w/3.jpg
---
Introduction to using the periodic table to determine electron configurations
