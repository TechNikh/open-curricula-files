---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stained
offline_file: ""
offline_thumbnail: ""
uuid: e6aa6786-cb8b-4014-8772-804edfa5c83f
updated: 1484310148
title: stained
categories:
    - Dictionary
---
stained
     adj 1: marked or dyed or discolored with foreign matter; "a badly
            stained tablecloth"; "tear-stained cheeks" [ant: {unstained}]
     2: having a coating of stain or varnish [syn: {varnished}]
     3: especially of reputation; "the senator's seriously damaged
        reputation"; "a flyblown reputation"; "a tarnished
        reputation"; "inherited a spotted name" [syn: {besmirched},
         {damaged}, {flyblown}, {spotted}, {sullied}, {tainted}, {tarnished}]
