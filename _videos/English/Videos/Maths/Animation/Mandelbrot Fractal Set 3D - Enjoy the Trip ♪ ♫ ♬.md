---
version: 1
type: video
provider: YouTube
id: mxrwz3fijjA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bbaa0698-55eb-4515-868c-71a2414bca4f
updated: 1486069645
title: 'Mandelbrot Fractal Set 3D - Enjoy the Trip ♪ ♫ ♬'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/mxrwz3fijjA/default.jpg
    - https://i3.ytimg.com/vi/mxrwz3fijjA/1.jpg
    - https://i3.ytimg.com/vi/mxrwz3fijjA/2.jpg
    - https://i3.ytimg.com/vi/mxrwz3fijjA/3.jpg
---
