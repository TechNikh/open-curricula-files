---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distance
offline_file: ""
offline_thumbnail: ""
uuid: ca2180c8-2ed3-4e1d-bd86-ddfebd019827
updated: 1484310284
title: distance
categories:
    - Dictionary
---
distance
     n 1: the property created by the space between two objects or
          points
     2: a distant region; "I could see it in the distance"
     3: size of the gap between two places; "the distance from New
        York to Chicago"; "he determined the length of the
        shortest line segment joining the two points" [syn: {length}]
     4: indifference by personal withdrawal; "emotional distance"
        [syn: {aloofness}]
     5: the interval between two times; "the distance from birth to
        death"; "it all happened in the space of 10 minutes" [syn:
         {space}]
     6: a remote point in time; "if that happens it will be at some
        distance in the future"; "at ...
