---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annually
offline_file: ""
offline_thumbnail: ""
uuid: f2df5263-9e8f-4e71-870a-c3e3df083d54
updated: 1484310515
title: annually
categories:
    - Dictionary
---
annually
     adv 1: without missing a year; "they travel to China annually"
            [syn: {yearly}, {every year}, {each year}]
     2: by the year; every year (usually with reference to a sum of
        money paid or received); "he earned $100,000 per annum";
        "we issue six volumes per annum" [syn: {per annum}, {p.a.},
         {per year}, {each year}]
