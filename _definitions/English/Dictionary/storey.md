---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/storey
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572921
title: storey
categories:
    - Dictionary
---
storey
     n : structure consisting of a room or set of rooms comprising a
         single level of a multilevel building; "what level is the
         office on?" [syn: {floor}, {level}, {story}]
