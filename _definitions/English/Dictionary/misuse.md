---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misuse
offline_file: ""
offline_thumbnail: ""
uuid: 7e001c32-5cc1-42c8-a110-ee559a02350f
updated: 1484310192
title: misuse
categories:
    - Dictionary
---
misuse
     n : improper or excessive use [syn: {abuse}]
     v 1: apply to a wrong thing or person; apply badly or
          incorrectly; "The words are misapplied in this context";
          "You are misapplying the name of this religious group"
          [syn: {misapply}]
     2: change the inherent purpose or function of something; "Don't
        abuse the system"; "The director of the factory misused
        the funds intended for the health care of his workers"
        [syn: {pervert}, {abuse}]
