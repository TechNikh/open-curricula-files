---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cave
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477281
title: cave
categories:
    - Dictionary
---
cave
     n : an underground enclosure with access from the surface of the
         ground or from the sea
     v 1: hollow out as if making a cave or opening; "The river was
          caving the banks" [syn: {undermine}]
     2: explore natural caves [syn: {spelunk}]
