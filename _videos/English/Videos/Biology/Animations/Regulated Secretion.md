---
version: 1
type: video
provider: YouTube
id: guqCEa7Y4RA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Regulated%20Secretion.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a5ae91f6-0cc1-4500-8c36-37ecaca3dd2b
updated: 1486069666
title: Regulated Secretion
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/guqCEa7Y4RA/default.jpg
    - https://i3.ytimg.com/vi/guqCEa7Y4RA/1.jpg
    - https://i3.ytimg.com/vi/guqCEa7Y4RA/2.jpg
    - https://i3.ytimg.com/vi/guqCEa7Y4RA/3.jpg
---
