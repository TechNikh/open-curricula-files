---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arguably
offline_file: ""
offline_thumbnail: ""
uuid: aff137d6-16c8-4e06-bde9-0e7fe2e299ce
updated: 1484310601
title: arguably
categories:
    - Dictionary
---
arguably
     adv : as can be shown by argument; "she is arguably the best"
