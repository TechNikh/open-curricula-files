---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Hilbert_series_and_Hilbert_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 048bac50-a12f-4ce2-b5a4-6b8e486c14d5
updated: 1484309153
title: Hilbert series and Hilbert polynomial
tags:
    - Definitions and main properties
    - Graded algebra and polynomial rings
    - Properties of Hilbert series
    - Additivity
    - Quotient by a non-zero divisor
    - Hilbert series and Hilbert polynomial of a polynomial ring
    - Shape of the Hilbert series and dimension
    - "Degree of a projective variety and Bézout's theorem"
    - Computation of Hilbert series and Hilbert polynomial
categories:
    - Algebraic geometry
---
Given a graded commutative algebra finitely generated over a field, the Hilbert function, Hilbert polynomial, and Hilbert series are three strongly related notions which measure the growth of the dimension of its homogeneous components.
