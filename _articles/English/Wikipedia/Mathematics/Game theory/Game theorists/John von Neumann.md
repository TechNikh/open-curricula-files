---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_von_Neumann
offline_file: ""
offline_thumbnail: ""
uuid: 564429ac-a809-4e70-b82d-b7596fe65f17
updated: 1484309029
title: John von Neumann
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-NeumannVonMargitta.jpg
tags:
    - Early life and education
    - Family background
    - Child prodigy
    - University studies
    - Early career and private life
    - Mathematics
    - Set theory
    - Ergodic theory
    - Operator theory
    - Measure theory
    - Lattice theory
    - Mathematical formulation of quantum mechanics
    - Quantum logic
    - Game theory
    - Mathematical economics
    - Linear programming
    - Mathematical statistics
    - Fluid dynamics
    - Mastery of mathematics
    - Nuclear weapons
    - Manhattan Project
    - Atomic Energy Commission
    - Mutual assured destruction
    - Computing
    - Cellular automata, DNA and the universal constructor
    - Weather systems
    - Cognitive abilities
    - Mathematical legacy
    - Death
    - Honors
    - Selected works
    - Notes
categories:
    - Game theorists
---
John von Neumann (/vɒn ˈnɔɪmən/; Hungarian: Neumann János Lajos, pronounced [ˈnɒjmɒn ˈjaːnoʃ ˈlɒjoʃ]; December 28, 1903 – February 8, 1957) was a Hungarian-American pure and applied mathematician, physicist, inventor, computer scientist, and polymath. He made major contributions to a number of fields, including mathematics (foundations of mathematics, functional analysis, ergodic theory, geometry, topology, and numerical analysis), physics (quantum mechanics, hydrodynamics, and quantum statistical mechanics), economics (game theory), computing (Von Neumann architecture, linear programming, self-replicating machines, stochastic computing), and statistics.
