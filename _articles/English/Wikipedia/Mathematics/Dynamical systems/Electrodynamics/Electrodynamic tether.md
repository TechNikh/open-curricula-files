---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrodynamic_tether
offline_file: ""
offline_thumbnail: ""
uuid: 17d1ec26-0a4b-4861-9cc0-8e96f850a498
updated: 1484308760
title: Electrodynamic tether
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/333px-STS-75_Tethered_Satellite_System_deployment.jpg
tags:
    - Tether propulsion
    - Uses for ED tethers
    - Electrodynamic tether fundamentals
    - Tethers as generators
    - Voltage and current
    - Voltage across conductor
    - Current in conductor
    - Tether current
    - 'Current collection / emission for an EDT system: theory and technology'
    - Bare conductive tethers
    - Orbit motion limited (OML) theory
    - Deviations from OML theory in a non-flowing plasma
    - Flowing plasma effect
    - Endbody collection
    - Passive collection theory
    - Flowing plasma electron collection mode
    - Flowing plasma ion collection model
    - Porous endbodies
    - Other current collection methods
    - Space charge limits across plasma sheaths
    - Electron emitters
    - Thermionic cathode (TC)
    - Electron field emitter arrays (FEAs)
    - Hollow cathode
    - Plasma collection and emission summary
    - Electrodynamic tether system fundamentals
    - Bare tether system derivation
    - Derivations
    - Interstellar travel
categories:
    - Electrodynamics
---
Electrodynamic tethers (EDTs) are long conducting wires, such as one deployed from a tether satellite, which can operate on electromagnetic principles as generators, by converting their kinetic energy to electrical energy, or as motors, converting electrical energy to kinetic energy.[1] Electric potential is generated across a conductive tether by its motion through a planet's magnetic field.
