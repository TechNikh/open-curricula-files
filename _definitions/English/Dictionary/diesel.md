---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diesel
offline_file: ""
offline_thumbnail: ""
uuid: 33a3a7a9-a53b-4e15-8b4d-9900e5da74d3
updated: 1484310521
title: diesel
categories:
    - Dictionary
---
Diesel
     n 1: German engineer (born in France) who invented the diesel
          engine (1858-1913) [syn: {Rudolf Diesel}, {Rudolf
          Christian Karl Diesel}]
     2: an internal-combustion engine that burns heavy oil [syn: {diesel
        engine}, {diesel motor}]
