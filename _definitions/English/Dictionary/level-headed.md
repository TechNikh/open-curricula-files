---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/level-headed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413861
title: level-headed
categories:
    - Dictionary
---
levelheaded
     adj : exercising or showing good judgment; "healthy scepticism";
           "a healthy fear of rattlesnakes"; "the healthy attitude
           of French laws"; "healthy relations between labor and
           management"; "an intelligent solution"; "a sound
           approach to the problem"; "sound advice"; "no sound
           explanation for his decision" [syn: {healthy}, {intelligent},
            {sound}]
