---
version: 1
type: video
provider: YouTube
id: 1OrGMtlKuQM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b806fd52-73de-4296-a35a-edad34049ef8
updated: 1486069639
title: >
    Cbse Class 12 focal length convex lens Physics Practical
    Experiment Video
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/1OrGMtlKuQM/default.jpg
    - https://i3.ytimg.com/vi/1OrGMtlKuQM/1.jpg
    - https://i3.ytimg.com/vi/1OrGMtlKuQM/2.jpg
    - https://i3.ytimg.com/vi/1OrGMtlKuQM/3.jpg
---
