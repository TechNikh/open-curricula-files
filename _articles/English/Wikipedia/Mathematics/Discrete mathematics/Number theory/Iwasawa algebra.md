---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iwasawa_algebra
offline_file: ""
offline_thumbnail: ""
uuid: d225c2c1-ebcb-430b-87e4-09a2ec784166
updated: 1484308649
title: Iwasawa algebra
tags:
    - Iwasawa algebra of the p-adic integers
    - Finitely generated modules
    - "Iwasawa's theorem"
    - Higher rank and non-commutative Iwasawa algebras
categories:
    - Number theory
---
In mathematics, the Iwasawa algebra Λ(G) of a profinite group G is a variation of the group ring of G with p-adic coefficients that take the topology of G into account. More precisely, Λ(G) is the inverse limit of the group rings Zp(G/H) as H  runs through the open normal subgroups of G. Commutative Iwasawa algebras were introduced by Iwasawa (1959) in his study of Zp extensions in Iwasawa theory, and non-commutative Iwasawa algebras of compact p-adic analytic groups were introduced by Lazard (1965).
