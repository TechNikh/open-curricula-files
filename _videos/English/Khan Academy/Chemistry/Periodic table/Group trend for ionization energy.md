---
version: 1
type: video
provider: YouTube
id: HBi8xjMchZc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Periodic%20table/Group%20trend%20for%20ionization%20energy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0f18119c-0af1-4dc7-8544-394255614ca2
updated: 1486069587
title: Group trend for ionization energy
categories:
    - Periodic table
thumbnail_urls:
    - https://i3.ytimg.com/vi/HBi8xjMchZc/default.jpg
    - https://i3.ytimg.com/vi/HBi8xjMchZc/1.jpg
    - https://i3.ytimg.com/vi/HBi8xjMchZc/2.jpg
    - https://i3.ytimg.com/vi/HBi8xjMchZc/3.jpg
---
Explaining trend in ionization energy down a group using concepts of nuclear charge, electron shielding, and distance of electrons from nucleus. Created by Jay.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/periodic-table/periodic-table-trends-bonding/v/period-trend-for-ionization-energy?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/periodic-table/periodic-table-trends-bonding/v/ionization-energy-trends?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
