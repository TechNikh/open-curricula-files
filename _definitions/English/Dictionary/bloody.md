---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bloody
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484562181
title: bloody
categories:
    - Dictionary
---
bloody
     adj 1: having or covered with or accompanied by blood; "a bloody
            nose"; "your scarf is all bloody"; "the effects will
            be violent and probably bloody"; "a bloody fight"
            [ant: {bloodless}]
     2: (used of persons) informal intensifiers; "what a bally (or
        blinking) nuisance"; "a bloody fool"; "a crashing bore";
        "you flaming idiot" [syn: {bally(a)}, {blinking(a)}, {bloody(a)},
         {blooming(a)}, {crashing(a)}, {flaming(a)}, {fucking(a)}]
     adv : extremely; "you are bloody right"; "Why are you so all-fired
           aggressive?" [syn: {damn}, {all-fired}]
     v : cover with blood; "bloody your hands"
     [also: ...
