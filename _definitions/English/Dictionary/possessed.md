---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possessed
offline_file: ""
offline_thumbnail: ""
uuid: cfea5f76-1a94-4c40-946c-1f82cbd72f12
updated: 1484310518
title: possessed
categories:
    - Dictionary
---
possessed
     adj 1: influenced or controlled by a powerful force such as a
            strong emotion; "by love possessed" [syn: {obsessed},
            {possessed(p)}]
     2: in a murderous frenzy as if possessed by a demon; "the
        soldier was completely amuck"; "berserk with grief"; "a
        berserk worker smashing windows" [syn: {amuck}, {amok}, {berserk},
         {demoniac}, {demoniacal}, {possessed(p)}]
