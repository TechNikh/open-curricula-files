---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/downstairs
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460181
title: downstairs
categories:
    - Dictionary
---
downstairs
     adj : on or of lower floors of a building; "the downstairs (or
           downstair) phone" [syn: {downstair}] [ant: {upstairs}]
     adv : on a floor below; "the tenants live downstairs" [syn: {down
           the stairs}, {on a lower floor}, {below}] [ant: {upstairs}]
