---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abundant
offline_file: ""
offline_thumbnail: ""
uuid: c36bc78d-a091-4700-96d1-3cbaa498a470
updated: 1484310403
title: abundant
categories:
    - Dictionary
---
abundant
     adj : present in great quantity; "an abundant supply of water"
           [ant: {scarce}]
