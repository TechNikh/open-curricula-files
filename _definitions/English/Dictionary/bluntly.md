---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bluntly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484622361
title: bluntly
categories:
    - Dictionary
---
bluntly
     adv : in a blunt direct manner; "he spoke bluntly"; "he stated his
           opinion flat-out"; "he was criticized roundly" [syn: {bluffly},
            {brusquely}, {flat out}, {roundly}]
