---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stag_hunt
offline_file: ""
offline_thumbnail: ""
uuid: b76888f9-d10d-4378-835d-eae7f454e7d6
updated: 1484308527
title: Stag hunt
tags:
    - Formal definition
    - The stag hunt and social cooperation
    - Examples of the stag hunt
categories:
    - Evolutionary game theory
---
In game theory, the stag hunt is a game that describes a conflict between safety and social cooperation. Other names for it or its variants include "assurance game", "coordination game", and "trust dilemma". Jean-Jacques Rousseau described a situation in which two individuals go out on a hunt. Each can individually choose to hunt a stag or hunt a hare. Each player must choose an action without knowing the choice of the other. If an individual hunts a stag, they must have the cooperation of their partner in order to succeed. An individual can get a hare by themself, but a hare is worth less than a stag. This has been taken to be a useful analogy for social cooperation, such as international ...
