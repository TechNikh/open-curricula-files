---
version: 1
type: video
provider: YouTube
id: mdulzEfQXDE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c1a9a2e8-e4a4-450f-892d-6640ac7cd098
updated: 1486069682
title: 'Electric Fields- Crash Course Physics #26'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/mdulzEfQXDE/default.jpg
    - https://i3.ytimg.com/vi/mdulzEfQXDE/1.jpg
    - https://i3.ytimg.com/vi/mdulzEfQXDE/2.jpg
    - https://i3.ytimg.com/vi/mdulzEfQXDE/3.jpg
---
