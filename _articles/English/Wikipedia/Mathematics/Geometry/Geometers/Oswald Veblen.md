---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oswald_Veblen
offline_file: ""
offline_thumbnail: ""
uuid: 9d7e0db2-61c5-4f11-8732-157182cc8cf6
updated: 1484309103
title: Oswald Veblen
tags:
    - Background
    - Career
    - Accomplishments
    - Personal life
    - Books by O. Veblen
categories:
    - Geometers
---
Oswald Veblen (June 24, 1880 – August 10, 1960) was an American mathematician, geometer and topologist, whose work found application in atomic physics and the theory of relativity. He proved the Jordan curve theorem in 1905;[1] while this was long considered the first rigorous proof, many now also consider Jordan's original proof rigorous.
