---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/add
offline_file: ""
offline_thumbnail: ""
uuid: f9247952-fb32-49d1-81b7-a3298af6bd10
updated: 1484310339
title: add
categories:
    - Dictionary
---
ADD
     n : a condition (mostly in boys) characterized by behavioral and
         learning disorders [syn: {attention deficit disorder}, {attention
         deficit hyperactivity disorder}, {ADHD}, {hyperkinetic
         syndrome}, {minimal brain dysfunction}, {minimal brain
         damage}, {MBD}]
     v 1: make an addition (to); join or combine or unite with others;
          increase the quality, quantity, zise or scope of; "We
          added two students to that dorm room"; "She added a
          personal note to her letter"; "Add insult to injury";
          "Add some extra plates to the dinner table" [ant: {take
          away}]
     2: state or say further; "`It doesn't matter,' ...
