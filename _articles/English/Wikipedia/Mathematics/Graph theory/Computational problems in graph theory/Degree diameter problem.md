---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Degree_diameter_problem
offline_file: ""
offline_thumbnail: ""
uuid: 8b452d1b-4958-4d73-963d-2688cccd561e
updated: 1484309391
title: Degree diameter problem
categories:
    - Computational problems in graph theory
---
In graph theory, the degree diameter problem is the problem of finding the largest possible graph G (in terms of the size of its vertex set V) of diameter k such that the largest degree of any of the vertices in G is at most d. The size of G is bounded above by the Moore bound; for 1 < k and 2 < d only the Petersen graph, the Hoffman-Singleton graph, and maybe a graph of diameter k = 2 and degree d = 57 attain the Moore bound. In general the largest degree-diameter graphs are much smaller in size than the Moore bound.
