---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visibility
offline_file: ""
offline_thumbnail: ""
uuid: 594f7120-941f-4556-9878-8f69015b61c8
updated: 1484310226
title: visibility
categories:
    - Dictionary
---
visibility
     n 1: quality or fact or degree of being visible; perceptible by
          the eye or obvious to the eye; "low visibility caused by
          fog" [syn: {visibleness}] [ant: {invisibility}]
     2: degree of exposure to public notice; "that candidate does
        not have sufficient visibility to win an election"; "he
        prefers a low profile" [syn: {profile}]
     3: capability of providing a clear unobstructed view; "a
        windshield with good visibility"
