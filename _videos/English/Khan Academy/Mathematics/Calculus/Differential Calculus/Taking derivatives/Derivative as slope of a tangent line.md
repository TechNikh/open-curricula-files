---
version: 1
type: video
provider: YouTube
id: ANyVpMS3HL4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Derivative%20as%20slope%20of%20a%20tangent%20line.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 67c760a0-91ea-40e8-a01f-5e4d67566cd7
updated: 1486069692
title: Derivative as slope of a tangent line
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/ANyVpMS3HL4/default.jpg
    - https://i3.ytimg.com/vi/ANyVpMS3HL4/1.jpg
    - https://i3.ytimg.com/vi/ANyVpMS3HL4/2.jpg
    - https://i3.ytimg.com/vi/ANyVpMS3HL4/3.jpg
---
