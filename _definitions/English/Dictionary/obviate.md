---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obviate
offline_file: ""
offline_thumbnail: ""
uuid: 2273abbe-a97a-4836-a164-fe9397618e66
updated: 1484310189
title: obviate
categories:
    - Dictionary
---
obviate
     v 1: do away with [syn: {rid of}, {eliminate}] [ant: {necessitate}]
     2: prevent the occurrence of; prevent from happening; "Let's
        avoid a confrontation"; "head off a confrontation"; "avert
        a strike" [syn: {debar}, {deflect}, {avert}, {head off}, {stave
        off}, {fend off}, {avoid}, {ward off}]
