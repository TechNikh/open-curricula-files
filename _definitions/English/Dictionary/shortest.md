---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shortest
offline_file: ""
offline_thumbnail: ""
uuid: bc466810-2cbc-4a22-b86a-60ab6180ff9e
updated: 1484310221
title: shortest
categories:
    - Dictionary
---
shortest
     adj : most direct; "took the shortest and most direct route to
           town" [syn: {short}]
