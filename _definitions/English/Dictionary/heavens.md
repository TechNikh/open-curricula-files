---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heavens
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484511121
title: heavens
categories:
    - Dictionary
---
heavens
     n : the apparent surface of the imaginary sphere on which
         celestial bodies appear to be projected [syn: {celestial
         sphere}, {sphere}, {empyrean}, {firmament}, {vault of
         heaven}, {welkin}]
