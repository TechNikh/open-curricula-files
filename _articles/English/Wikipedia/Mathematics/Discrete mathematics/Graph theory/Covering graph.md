---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_graph
offline_file: ""
offline_thumbnail: ""
uuid: a045cb43-12fc-4de8-a3ad-341844320c48
updated: 1484308612
title: Covering graph
tags:
    - Definition
    - Examples
    - Double cover
    - Universal cover
    - Examples of universal covers
    - Topological crystal
    - Planar cover
    - Voltage graphs
    - Notes
categories:
    - Graph theory
---
In the mathematical discipline of graph theory, a graph C is a covering graph of another graph G if there is a covering map from the vertex set of C to the vertex set of G. A covering map f is a surjection and a local isomorphism: the neighbourhood of a v vertex in C is mapped bijectively onto the neighbourhood of f(v) in G.
