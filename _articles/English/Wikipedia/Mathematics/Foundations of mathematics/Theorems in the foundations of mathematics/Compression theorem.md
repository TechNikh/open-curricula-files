---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Compression_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 3dd71833-9353-4439-943b-d6847026c94d
updated: 1484308991
title: Compression theorem
categories:
    - Theorems in the foundations of mathematics
---
The theorem states that there exists no largest complexity class, with computable boundary, which contains all computable functions.
