---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/involved
offline_file: ""
offline_thumbnail: ""
uuid: c86a6baf-ffc1-4b48-9be2-6cc55b0ea6ee
updated: 1484310363
title: involved
categories:
    - Dictionary
---
involved
     adj 1: connected by participation or association or use; "we
            accomplished nothing, simply because of the large
            number of people involved"; "the problems involved";
            "the involved muscles"; "I don't want to get
            involved"; "everyone involved in the bribery case has
            been identified" [ant: {uninvolved}]
     2: entangled or hindered as if e.g. in mire; "the difficulties
        in which the question is involved"; "brilliant leadership
        mired in details and confusion" [syn: {mired}]
     3: emotionally involved [syn: {involved with(p)}]
     4: highly involved or intricate; "the Byzantine tax structure";
        ...
