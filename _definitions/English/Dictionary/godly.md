---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/godly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569261
title: godly
categories:
    - Dictionary
---
godly
     adj 1: showing great reverence for god; "a godly man"; "leading a
            godly life" [syn: {reverent}, {worshipful}]
     2: emanating from God; "divine judgment"; "divine guidance";
        "everything is black1 or white...satanic or
        godlyt"-Saturday Rev. [syn: {divine}]
     [also: {godliest}, {godlier}]
