---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evident
offline_file: ""
offline_thumbnail: ""
uuid: d2c36df9-f6ca-4a95-a42d-f9da89ae7530
updated: 1484310299
title: evident
categories:
    - Dictionary
---
evident
     adj 1: clearly apparent or obvious to the mind or senses; "the
            effects of the drought are apparent to anyone who sees
            the parched fields"; "evident hostility"; "manifest
            disapproval"; "patent advantages"; "made his meaning
            plain"; "it is plain that he is no reactionary"; "in
            plain view" [syn: {apparent}, {manifest}, {patent}, {plain}]
     2: capable of being seen or noticed; "a discernible change in
        attitude"; "a clearly evident erasure in the manuscript";
        "an observable change in behavior" [syn: {discernible}, {observable}]
