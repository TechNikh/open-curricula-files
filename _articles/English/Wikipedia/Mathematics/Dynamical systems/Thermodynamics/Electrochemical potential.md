---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrochemical_potential
offline_file: ""
offline_thumbnail: ""
uuid: 81e32708-906a-4cfa-9348-95ea9fd048c9
updated: 1484308840
title: Electrochemical potential
tags:
    - Introduction
    - Conflicting terminologies
    - Definition and usage
    - Incorrect usage
categories:
    - Thermodynamics
---
In electrochemistry, the electrochemical potential, μ, sometimes abbreviated to ECP, is a thermodynamic measure of chemical potential that does not omit the energy contribution of electrostatics. Electrochemical potential is expressed in the unit of J/mol.
