---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coinduction
offline_file: ""
offline_thumbnail: ""
uuid: 40d663ee-9ec6-45dc-b5fb-a9d63b212267
updated: 1484308664
title: Coinduction
categories:
    - Theoretical computer science
---
Coinduction is the mathematical dual to structural induction. Coinductively defined types are known as codata and are typically infinite data structures, such as streams.
