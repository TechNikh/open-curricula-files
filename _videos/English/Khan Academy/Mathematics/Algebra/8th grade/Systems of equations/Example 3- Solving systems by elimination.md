---
version: 1
type: video
provider: YouTube
id: Dqp6xOeR3Ls
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Example%203-%20Solving%20systems%20by%20elimination.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5aa2592c-3845-4673-ac05-f4cf2ac680f9
updated: 1486069557
title: 'Example 3- Solving systems by elimination'
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Dqp6xOeR3Ls/default.jpg
    - https://i3.ytimg.com/vi/Dqp6xOeR3Ls/1.jpg
    - https://i3.ytimg.com/vi/Dqp6xOeR3Ls/2.jpg
    - https://i3.ytimg.com/vi/Dqp6xOeR3Ls/3.jpg
---
