---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sown
offline_file: ""
offline_thumbnail: ""
uuid: b73c5924-f4b0-4819-b9fe-b378837652a6
updated: 1484310301
title: sown
categories:
    - Dictionary
---
sow
     n : an adult female hog
     v 1: place (seeds) in or on the ground for future growth; "She
          sowed sunflower seeds" [syn: {sough}, {seed}]
     2: introduce into an environment; "sow suspicion or beliefs"
        [syn: {sough}]
     3: place seeds in or on (the ground); "sow the ground with
        sunflower seeds" [syn: {inseminate}, {sow in}]
     [also: {sown}]
