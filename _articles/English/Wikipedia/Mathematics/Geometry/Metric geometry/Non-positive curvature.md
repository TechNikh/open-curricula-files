---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-positive_curvature
offline_file: ""
offline_thumbnail: ""
uuid: 6bb68c9a-26b2-4a18-a8f2-301c8f062fc6
updated: 1484309270
title: Non-positive curvature
categories:
    - Metric geometry
---
In mathematics, spaces of non-positive curvature occur in many contexts and form a generalization of hyperbolic geometry. In the category of Riemannian manifolds, one can consider the sectional curvature of the manifold and require that this curvature be everywhere less than or equal to zero. The notion of curvature extends to the category of geodesic metric spaces, where one can use comparison triangles to quantify the curvature of a space; in this context, non-positively curved spaces are known as (locally) CAT(0) spaces.
