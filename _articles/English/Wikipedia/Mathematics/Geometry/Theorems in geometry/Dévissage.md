---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/D%C3%A9vissage'
offline_file: ""
offline_thumbnail: ""
uuid: 4f71e6f5-a173-4916-83bf-1183c44f876e
updated: 1484309117
title: Dévissage
tags:
    - "Grothendieck's dévissage theorem"
    - "Gruson and Raynaud's relative dévissages"
    - Bibliography
categories:
    - Theorems in geometry
---
In algebraic geometry, dévissage is a technique introduced by Alexander Grothendieck for proving statements about coherent sheaves on noetherian schemes. Dévissage is an adaptation of a certain kind of noetherian induction. It has many applications, including the proof of generic flatness and the proof that higher direct images of coherent sheaves under proper morphisms are coherent.
