---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toy
offline_file: ""
offline_thumbnail: ""
uuid: 6fcc1267-3148-4588-b608-62e870a9a897
updated: 1484310527
title: toy
categories:
    - Dictionary
---
toy
     n 1: an artifact designed to be played with [syn: {plaything}]
     2: a nonfunctional replica of something else (frequently used
        as a modifier); "a toy stove"
     3: copy that reproduces something in greatly reduced size [syn:
         {miniature}]
     4: any of several breeds of very small dogs kept purely as pets
        [syn: {toy dog}]
     v 1: behave carelessly or indifferently; "Play about with a young
          girl's affection" [syn: {dally}, {play}, {flirt}]
     2: manipulate manually or in one's mind or imagination; "She
        played nervously with her wedding ring"; "Don't fiddle
        with the screws"; "He played with the idea of running for
        the ...
