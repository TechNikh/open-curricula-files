---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/originator
offline_file: ""
offline_thumbnail: ""
uuid: e8b40247-eb19-485e-9fb0-aa7086b5f80d
updated: 1484310391
title: originator
categories:
    - Dictionary
---
originator
     n : someone who creates new things [syn: {conceiver}, {mastermind}]
