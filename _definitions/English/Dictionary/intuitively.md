---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intuitively
offline_file: ""
offline_thumbnail: ""
uuid: df10ed55-2424-4081-a2cd-33d5909e1737
updated: 1484310445
title: intuitively
categories:
    - Dictionary
---
intuitively
     adv : in an intuitive manner; "inventors seem to have chosen
           intuitively a combination of explosive and aggressive
           sounds as warning signals to be used on automobiles"
