---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observable
offline_file: ""
offline_thumbnail: ""
uuid: fef82548-1d6c-4919-b4c4-e4e8c54267eb
updated: 1484310309
title: observable
categories:
    - Dictionary
---
observable
     adj : capable of being seen or noticed; "a discernible change in
           attitude"; "a clearly evident erasure in the
           manuscript"; "an observable change in behavior" [syn: {discernible},
            {evident}]
