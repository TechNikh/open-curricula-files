---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/white-collar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487361
title: white-collar
categories:
    - Dictionary
---
white-collar
     adj : of or designating salaried professional or clerical  work or
           workers; "the coal miner's son aspired to a
           white-collar occupation as a bookkeeper" [ant: {blue-collar}]
