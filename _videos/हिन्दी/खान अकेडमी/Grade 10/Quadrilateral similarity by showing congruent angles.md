---
version: 1
type: video
provider: YouTube
id: 7mw6u5E3pEI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 256833f3-1d26-485a-a440-cdd05eab08b8
updated: 1486069618
title: Quadrilateral similarity by showing congruent angles
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/7mw6u5E3pEI/default.jpg
    - https://i3.ytimg.com/vi/7mw6u5E3pEI/1.jpg
    - https://i3.ytimg.com/vi/7mw6u5E3pEI/2.jpg
    - https://i3.ytimg.com/vi/7mw6u5E3pEI/3.jpg
---
