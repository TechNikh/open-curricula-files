---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paint
offline_file: ""
offline_thumbnail: ""
uuid: 45c05671-ecae-47c5-a049-a243b03886f2
updated: 1484310377
title: paint
categories:
    - Dictionary
---
paint
     n 1: a substance used as a coating to protect or decorate a
          surface (especially a mixture of pigment suspended in a
          liquid); dries to form a hard coating
     2: (basketball) a space (including the foul line) in front of
        the basket at each end of a basketball court; usually
        painted a different color from the rest of the court; "he
        hit a jump shot from the top of the key"; "he dominates
        play in the paint" [syn: {key}]
     3: makeup consisting of a pink or red powder applied to the
        cheeks [syn: {rouge}, {blusher}]
     v 1: make a painting; "he painted all day in the garden"; "He
          painted a painting of the ...
