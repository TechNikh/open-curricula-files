---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Inequalities_in_information_theory
offline_file: ""
offline_thumbnail: ""
uuid: b95a759b-7d80-40a4-8dc4-4a0b03d45b18
updated: 1484308770
title: Inequalities in information theory
tags:
    - Shannon-type inequalities
    - Non-Shannon-type inequalities
    - Lower bounds for the Kullback–Leibler divergence
    - "Gibbs' inequality"
    - "Kullback's inequality"
    - "Pinsker's inequality"
    - Other inequalities
    - Hirschman uncertainty
    - "Tao's inequality"
categories:
    - Entropy and information
---
