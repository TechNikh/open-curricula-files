---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exemplified
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640721
title: exemplified
categories:
    - Dictionary
---
exemplify
     v 1: be characteristic of; "This compositional style is
          exemplified by this fugue" [syn: {represent}]
     2: clarify by giving an example of [syn: {illustrate}, {instance}]
     [also: {exemplified}]
