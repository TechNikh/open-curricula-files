---
version: 1
type: video
provider: YouTube
id: sabP2TXDWGs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Diffraction%20%26%20Interference.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7c1e5744-6d67-4e00-83ac-f765e0dad6bf
updated: 1486069652
title: 'Diffraction & Interference'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/sabP2TXDWGs/default.jpg
    - https://i3.ytimg.com/vi/sabP2TXDWGs/1.jpg
    - https://i3.ytimg.com/vi/sabP2TXDWGs/2.jpg
    - https://i3.ytimg.com/vi/sabP2TXDWGs/3.jpg
---
