---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484311981
title: liked
categories:
    - Dictionary
---
liked
     adj : found pleasant or attractive; often used as a combining
           form; "a well-liked teacher" [ant: {disliked}]
