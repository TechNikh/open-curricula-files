---
version: 1
type: video
provider: YouTube
id: ySFtitDsCIs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ced53a9c-365b-4c54-be16-2f100dc95083
updated: 1486069607
title: Mean Value Theorem
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ySFtitDsCIs/default.jpg
    - https://i3.ytimg.com/vi/ySFtitDsCIs/1.jpg
    - https://i3.ytimg.com/vi/ySFtitDsCIs/2.jpg
    - https://i3.ytimg.com/vi/ySFtitDsCIs/3.jpg
---
