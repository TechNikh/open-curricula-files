---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honey
offline_file: ""
offline_thumbnail: ""
uuid: 9ed014bd-3eaf-4a5d-8aaa-7cbb70f28662
updated: 1484310457
title: honey
categories:
    - Dictionary
---
honey
     adj : having the color of honey
     n 1: a sweet yellow liquid produced by bees
     2: a beloved person; used as terms of endearment [syn: {beloved},
         {dear}, {dearest}, {loved one}, {love}]
     v : sweeten with honey
     [also: {honied}]
