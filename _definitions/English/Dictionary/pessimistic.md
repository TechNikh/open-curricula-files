---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pessimistic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413201
title: pessimistic
categories:
    - Dictionary
---
pessimistic
     adj : expecting the worst in this worst of all possible worlds
           [ant: {optimistic}]
