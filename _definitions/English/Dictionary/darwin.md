---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/darwin
offline_file: ""
offline_thumbnail: ""
uuid: 9e014f92-3bfa-4cd2-8f0a-fe8ad2b33420
updated: 1484310290
title: darwin
categories:
    - Dictionary
---
Darwin
     n 1: English natural scientist who formulated a theory of
          evolution by natural selection (1809-1882) [syn: {Charles
          Darwin}, {Charles Robert Darwin}]
     2: provincial capital of the Northern Territory of Australia
