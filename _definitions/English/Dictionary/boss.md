---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boss
offline_file: ""
offline_thumbnail: ""
uuid: 18341bc4-d45c-4bc1-906c-596472ef7fbb
updated: 1484310154
title: boss
categories:
    - Dictionary
---
boss
     adj : exceptionally good; "a boss hand at carpentry"; "his brag
           cornfield" [syn: {brag}]
     n 1: a person who exercises control over workers; "if you want to
          leave early you have to ask the foreman" [syn: {foreman},
           {chief}, {gaffer}, {honcho}]
     2: a person responsible for hiring workers; "the boss hired
        three more men for the new job" [syn: {hirer}]
     3: a person who exercises control and makes decisions; "he is
        his own boss now"
     4: a leader in a political party who controls votes and
        dictates appointments; "party bosses have a reputation for
        corruption" [syn: {party boss}, {political boss}]
     5: a ...
