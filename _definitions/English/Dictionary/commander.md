---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commander
offline_file: ""
offline_thumbnail: ""
uuid: d8059cb4-2b81-4050-846b-d91baf60b9c2
updated: 1484310163
title: commander
categories:
    - Dictionary
---
commander
     n 1: an officer in command of a military unit [syn: {commanding
          officer}, {commandant}]
     2: someone in an official position of authority who can command
        or control others
     3: a commissioned naval officer who ranks above a lieutenant
        commander and below a captain
     4: an officer in the airforce [syn: {air force officer}]
