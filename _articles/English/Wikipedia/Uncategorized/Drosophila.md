---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drosophila
offline_file: ""
offline_thumbnail: ""
uuid: a89bdb5d-f901-4baf-8ba9-4c19e692f34e
updated: 1484308345
title: Drosophila
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Drosophila_residua_head.jpg
tags:
    - Etymology
    - Morphology
    - Lifecycle and ecology
    - Habitat
    - Reproduction
    - Laboratory-cultured animals
    - Microbiome
    - Predators
    - Systematics
    - Drosophila species genome project
categories:
    - Uncategorized
---
Drosophila (/drəˈsɒfᵻlə, drɒ-, droʊ-/[1][2]) is a genus of small flies, belonging to the family Drosophilidae, whose members are often called "fruit flies" or (less frequently) pomace flies, vinegar flies, or wine flies, a reference to the characteristic of many species to linger around overripe or rotting fruit. They should not be confused with the Tephritidae, a related family, which are also called fruit flies (sometimes referred to as "true fruit flies"); tephritids feed primarily on unripe or ripe fruit, with many species being regarded as destructive agricultural pests, especially the Mediterranean fruit fly. One species of Drosophila in particular, D. melanogaster, has been ...
