---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Response_reactions
offline_file: ""
offline_thumbnail: ""
uuid: 2c08191b-d9be-42cf-9132-56a725d20018
updated: 1484308855
title: Response reactions
categories:
    - Thermodynamics
---
The theory of response reactions (RERs) or response equilibria was elaborated for the thermodynamic systems in which more than one equilibrium is established simultaneously.[1] It is based on detailed analysis of the Hessian determinant. The theory derives the sensitivity coefficient as the sum of the contributions of individual RERs. Thus all phenomena which are in apparent contradiction to the Le Chatelier principle could be interpreted. With the help of RERs the equilibrium coupling was defined.[2] RERs could be derived based either on the species.[3] or on the stoichiometrically independent reactions of a parallel system. The set of RERs is unambiguous in a given system; and the number ...
