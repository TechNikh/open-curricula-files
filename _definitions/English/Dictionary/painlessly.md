---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/painlessly
offline_file: ""
offline_thumbnail: ""
uuid: cc2a3304-f314-4ae0-9f5e-bca7f4b65f7c
updated: 1484310563
title: painlessly
categories:
    - Dictionary
---
painlessly
     adv : without pain; "after the surgery, she could move her arms
           painlessly" [ant: {painfully}]
