---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Immune_response
offline_file: ""
offline_thumbnail: ""
uuid: 63a86f12-7148-4556-ae8b-66dc36f96602
updated: 1484308377
title: Immune system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Neutrophil_with_anthrax_copy.jpg
tags:
    - History of immunology
    - Layered defense
    - Innate immune system
    - Surface barriers
    - Inflammation
    - Complement system
    - Cellular barriers
    - Natural killer cells
    - Adaptive immune system
    - Lymphocytes
    - Killer T cells
    - Helper T cells
    - Gamma delta T cells
    - B lymphocytes and antibodies
    - Alternative adaptive immune system
    - Immunological memory
    - Passive memory
    - Active memory and immunization
    - Disorders of human immunity
    - Immunodeficiencies
    - Autoimmunity
    - Hypersensitivity
    - Other mechanisms and evolution
    - Tumor immunology
    - Physiological regulation
    - Vitamin D
    - Sleep and rest
    - Nutrition and diet
    - Manipulation in medicine
    - Immunosuppression
    - Immunostimulation
    - Theoretical approaches to the immune system
    - Predicting immunogenicity
    - Manipulation by pathogens
categories:
    - Uncategorized
---
The immune system is a host defense system comprising many biological structures and processes within an organism that protects against disease. To function properly, an immune system must detect a wide variety of agents, known as pathogens, from viruses to parasitic worms, and distinguish them from the organism's own healthy tissue. In many species, the immune system can be classified into subsystems, such as the innate immune system versus the adaptive immune system, or humoral immunity versus cell-mediated immunity. In humans, the blood–brain barrier, blood–cerebrospinal fluid barrier, and similar fluid–brain barriers separate the peripheral immune system from the neuroimmune ...
