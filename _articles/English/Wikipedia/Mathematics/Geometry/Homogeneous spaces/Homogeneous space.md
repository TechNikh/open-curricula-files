---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homogeneous_space
offline_file: ""
offline_thumbnail: ""
uuid: 38b8216c-e75d-4c14-8d9a-8160f2fa7c04
updated: 1484309249
title: Homogeneous space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Torus.png
tags:
    - Formal definition
    - Examples
    - Geometry
    - Homogeneous spaces as coset spaces
    - Example
    - Prehomogeneous vector spaces
    - Homogeneous spaces in physics
categories:
    - Homogeneous spaces
---
In mathematics, particularly in the theories of Lie groups, algebraic groups and topological groups, a homogeneous space for a group G is a non-empty manifold or topological space X on which G acts transitively. The elements of G are called the symmetries of X. A special case of this is when the group G in question is the automorphism group of the space X – here "automorphism group" can mean isometry group, diffeomorphism group, or homeomorphism group. In this case X is homogeneous if intuitively X looks locally the same at each point, either in the sense of isometry (rigid geometry), diffeomorphism (differential geometry), or homeomorphism (topology). Some authors insist that the action ...
