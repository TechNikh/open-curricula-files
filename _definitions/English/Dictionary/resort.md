---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resort
offline_file: ""
offline_thumbnail: ""
uuid: 8524b686-5aac-4c20-a7fd-c7d8198a89a4
updated: 1484310249
title: resort
categories:
    - Dictionary
---
resort
     n 1: a hotel located in a resort area [syn: {resort hotel}, {holiday
          resort}]
     2: a frequently visited place [syn: {haunt}, {hangout}, {repair},
         {stamping ground}]
     3: something or someone turned to for assistance or security;
        "his only recourse was the police"; "took refuge in lying"
        [syn: {recourse}, {refuge}]
     4: act of turning to for assistance; "have recourse to the
        courts"; "an appeal to his uncle was his last resort"
        [syn: {recourse}, {refuge}]
     v 1: have recourse to; "The government resorted to rationing
          meat" [syn: {fall back}, {recur}]
     2: move, travel, or proceed toward some place; "He ...
