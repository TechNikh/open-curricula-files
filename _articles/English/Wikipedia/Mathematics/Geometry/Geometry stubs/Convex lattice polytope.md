---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_lattice_polytope
offline_file: ""
offline_thumbnail: ""
uuid: e055919b-ca69-444e-b73e-b9e0f0f9c5a9
updated: 1484309359
title: Convex lattice polytope
categories:
    - Geometry stubs
---
A convex lattice polytope (also called Z-polyhedron or Z-polytope) is a geometric object playing an important role in discrete geometry and combinatorial commutative algebra. It is a polytope in a Euclidean space Rn which is a convex hull of finitely many points in the integer lattice Zn ⊂ Rn. Such objects are prominently featured in the theory of toric varieties, where they correspond to polarized projective toric varieties.
