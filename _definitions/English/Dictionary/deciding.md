---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deciding
offline_file: ""
offline_thumbnail: ""
uuid: f1f5f150-1cc6-49e4-981d-24e336127c42
updated: 1484310473
title: deciding
categories:
    - Dictionary
---
deciding
     adj : having the power or quality of deciding; "the crucial
           experiment"; "cast the deciding vote"; "the
           determinative (or determinant) battle" [syn: {crucial},
            {deciding(a)}, {determinant}, {determinative}, {determining(a)}]
     n : the cognitive process of reaching a decision; "a good
         executive must be good at decision making" [syn: {decision
         making}]
