---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oppose
offline_file: ""
offline_thumbnail: ""
uuid: 1f871c03-acce-4656-801f-3b78cb80caf1
updated: 1484310234
title: oppose
categories:
    - Dictionary
---
oppose
     v 1: be against; express opposition to; "We oppose the ban on
          abortion"
     2: fight against or resist strongly; "The senator said he would
        oppose the bill"; "Don't fight it!" [syn: {fight}, {fight
        back}, {fight down}, {defend}]
     3: oppose with equal weight or force [syn: {counterbalance}]
     4: set into opposition or rivalry; "let them match their best
        athletes against ours"; "pit a chess player against the
        Russian champion"; "He plays his two children off against
        each other" [syn: {pit}, {match}, {play off}]
     5: act against or in opposition to; "She reacts negatively to
        everything I say" [syn: {react}]
     ...
