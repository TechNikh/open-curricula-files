---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boredom
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570461
title: boredom
categories:
    - Dictionary
---
boredom
     n : the feeling of being bored by something tedious [syn: {ennui},
          {tedium}]
