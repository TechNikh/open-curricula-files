---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coin_flipping
offline_file: ""
offline_thumbnail: ""
uuid: 94d751a1-558d-4f7f-bc74-eafefe768f4d
updated: 1484309040
title: Coin flipping
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Coin_tossing.JPG
tags:
    - History
    - Process
    - Three-way
    - Use in dispute resolution
    - Politics
    - Australia
    - Canada
    - Philippines
    - United Kingdom
    - United States
    - Physics
    - Counterintuitive properties
    - Mathematics
    - Telecommunications
    - Lotteries
    - Clarifying feelings
    - In fiction
    - Coin landing on its edge in fiction
    - Footnotes
categories:
    - Games (probability)
---
Coin flipping, coin tossing, or heads or tails is the practice of throwing a coin in the air to choose between two alternatives, sometimes to resolve a dispute between two parties. It is a form of sortition which inherently has only two possible and equally likely outcomes.
