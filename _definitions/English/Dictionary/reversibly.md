---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reversibly
offline_file: ""
offline_thumbnail: ""
uuid: adebc4df-43ce-42ed-9998-a739092a7143
updated: 1484310429
title: reversibly
categories:
    - Dictionary
---
reversibly
     adv : in a reversible manner; "reversibly convertible"
