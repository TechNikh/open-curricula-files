---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impeccable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500561
title: impeccable
categories:
    - Dictionary
---
impeccable
     adj 1: without fault or error; "faultless logic"; "speaks
            impeccable French"; "timing and technique were
            immaculate"; "an immaculate record" [syn: {faultless},
             {immaculate}]
     2: not capable of sin
