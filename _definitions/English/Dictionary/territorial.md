---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/territorial
offline_file: ""
offline_thumbnail: ""
uuid: 49de2165-ba95-4c11-9699-79513f1e7979
updated: 1484310529
title: territorial
categories:
    - Dictionary
---
territorial
     adj 1: of or relating to a territory; "the territorial government
            of the Virgin Islands"; "territorial claims made by a
            country"
     2: displaying territoriality; defending a territory from
        intruders; "territorial behavior"; "strongly territorial
        birds" [ant: {nonterritorial}]
     3: of or relating to the local vicinity; "territorial waters"
        [ant: {extraterritorial}]
     n 1: nonprofessional soldier member of a territorial military
          unit
     2: a territorial military unit [syn: {territorial reserve}]
