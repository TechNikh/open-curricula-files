---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sudan
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484602681
title: sudan
categories:
    - Dictionary
---
Sudan
     n 1: a republic in northeastern Africa on the Red Sea; achieved
          independence from Egypt and the United Kingdom in 1956;
          involved in state-sponsored terrorism [syn: {Republic of
          the Sudan}, {Soudan}]
     2: a region of northern Africa south of the Sahara and Libyan
        deserts; extends from the Atlantic to the Red Sea [syn: {Soudan}]
