---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/postgraduate
offline_file: ""
offline_thumbnail: ""
uuid: a7ba1888-f6ac-4dae-892a-e3a40f1a39b4
updated: 1484310479
title: postgraduate
categories:
    - Dictionary
---
postgraduate
     adj : of or relating to studies beyond a bachelor's degree;
           "graduate courses" [syn: {graduate(a)}]
     n : a student who continues studies after graduation [syn: {graduate
         student}, {grad student}]
