---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hazel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484644921
title: hazel
categories:
    - Dictionary
---
hazel
     adj : of a light brown or yellowish brown color
     n 1: Australian tree grown especially for ornament and its
          fine-grained wood and bearing edible nuts [syn: {hazel
          tree}, {Pomaderris apetala}]
     2: the fine-grained wood of a hazelnut tree (genus Corylus) and
        the hazel tree (Australian genus Pomaderris)
     3: any of several shrubs or small trees of the genus Corylus
        bearing edible nuts enclosed in a leafy husk [syn: {hazelnut},
         {hazelnut tree}]
