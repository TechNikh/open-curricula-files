---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/johnson
offline_file: ""
offline_thumbnail: ""
uuid: b64d663b-b703-48d3-b635-df90c36b863e
updated: 1484310158
title: johnson
categories:
    - Dictionary
---
Johnson
     n 1: English writer and lexicographer (1709-1784) [syn: {Samuel
          Johnson}, {Dr. Johnson}]
     2: 36th President of the United States; was elected Vice
        President and succeeded Kennedy when Kennedy was
        assassinated (1908-1973) [syn: {Lyndon Johnson}, {Lyndon
        Baines Johnson}, {LBJ}, {President Johnson}, {President
        Lyndon Johnson}]
     3: 17th President of the United States; was elected Vice
        President and succeeded Lincoln when Lincoln was
        assassinated; was impeached but acquitted by one vote
        (1808-1875) [syn: {Andrew Johnson}, {President Johnson}, {President
        Andrew Johnson}]
