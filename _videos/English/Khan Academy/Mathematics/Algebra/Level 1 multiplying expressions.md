---
version: 1
type: video
provider: YouTube
id: Sc0e6xrRJYY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Level%201%20multiplying%20expressions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 012bc3ee-a1be-4795-8f72-0fbe7380d535
updated: 1486069571
title: Level 1 multiplying expressions
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Sc0e6xrRJYY/default.jpg
    - https://i3.ytimg.com/vi/Sc0e6xrRJYY/1.jpg
    - https://i3.ytimg.com/vi/Sc0e6xrRJYY/2.jpg
    - https://i3.ytimg.com/vi/Sc0e6xrRJYY/3.jpg
---
(Ax+By)(Ax+By)
More free lessons at: http://www.khanacademy.org/video?v=Sc0e6xrRJYY
