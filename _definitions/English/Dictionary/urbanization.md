---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urbanization
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484614081
title: urbanization
categories:
    - Dictionary
---
urbanization
     n 1: the condition of being urbanized [syn: {urbanisation}]
     2: the social process whereby cities grow and societies become
        more urban [syn: {urbanisation}]
