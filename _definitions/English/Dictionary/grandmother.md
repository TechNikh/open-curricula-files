---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandmother
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439001
title: grandmother
categories:
    - Dictionary
---
grandmother
     n : the mother of your father or mother [syn: {grandma}, {granny},
          {grannie}, {gran}]
