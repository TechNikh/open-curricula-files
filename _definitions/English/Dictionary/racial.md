---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/racial
offline_file: ""
offline_thumbnail: ""
uuid: 7f40c0e3-3afc-42ae-9644-d023b79e284b
updated: 1484310563
title: racial
categories:
    - Dictionary
---
racial
     adj 1: of or related to genetically distinguished groups of people;
            "racial groups"
     2: of or characteristic of race or races or arising from
        differences among groups; "racial differences"; "racial
        discrimination" [ant: {nonracial}]
