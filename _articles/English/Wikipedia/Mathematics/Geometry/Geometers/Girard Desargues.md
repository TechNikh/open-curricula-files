---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Girard_Desargues
offline_file: ""
offline_thumbnail: ""
uuid: ebc5966d-cb67-42a7-83d9-b3d747acfa44
updated: 1484309078
title: Girard Desargues
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-G%25C3%25A9rard_Desargues.jpeg'
categories:
    - Geometers
---
Girard Desargues (French: [dezaʁɡ]; 21 February 1591 – September 1661) was a French mathematician and engineer, who is considered one of the founders of projective geometry.[1] Desargues' theorem, the Desargues graph, and the crater Desargues on the Moon are named in his honour.
