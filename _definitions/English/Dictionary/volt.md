---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/volt
offline_file: ""
offline_thumbnail: ""
uuid: 05880bef-4e70-451f-9d67-77df5919a5cc
updated: 1484310202
title: volt
categories:
    - Dictionary
---
volt
     n : a unit of potential equal to the potential difference
         between two points on a conductor carrying a current of 1
         ampere when the power dissipated between the two points
         is 1 watt; equivalent to the potential difference across
         a resistance of 1 ohm when 1 ampere of current flows
         through it [syn: {V}]
