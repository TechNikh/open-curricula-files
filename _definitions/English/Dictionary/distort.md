---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distort
offline_file: ""
offline_thumbnail: ""
uuid: bfe1b54d-a907-41f6-b3f3-03cb1a483aa9
updated: 1484310414
title: distort
categories:
    - Dictionary
---
distort
     v 1: make false by mutilation or addition; as of a message or
          story [syn: {falsify}, {garble}, {warp}]
     2: form into a spiral shape; "The cord is all twisted" [syn: {twist},
         {twine}] [ant: {untwist}]
     3: twist and press out of shape [syn: {contort}, {deform}, {wring}]
     4: affect as in thought or feeling; "My personal feelings color
        my judgment in this case"; "The sadness tinged his life"
        [syn: {tinge}, {color}, {colour}]
     5: alter the shape of (something) by stress; "His body was
        deformed by leprosy" [syn: {deform}, {strain}]
