---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Junk_foods
offline_file: ""
offline_thumbnail: ""
uuid: 234f8bf7-b455-4717-804b-a06faa35f01e
updated: 1484308372
title: Junk food
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-USMC-100629-M-3215R-002.jpg
tags:
    - Origin of the term
    - Definitions
    - Popularity and appeal
    - Health effects
    - Anti-junk food measures
    - Taxation
    - Advertising restriction
    - Behavior problems
categories:
    - Uncategorized
---
Junk food is a pejorative term for cheap food containing high levels of calories from sugar or fat with little fiber, protein, vitamins or minerals.[1][2][3] Junk food can also refer to high protein food like meat prepared with saturated fat - which some believe may be unhealthy, although some studies have shown no correlation between saturated fat and cardiovascular diseases;[4] many hamburger outlets, fried chicken outlets and the like supply food considered as junk food.[5][better source needed]
