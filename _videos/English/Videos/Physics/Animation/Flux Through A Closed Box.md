---
version: 1
type: video
provider: YouTube
id: 5ENl4vn82bc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Flux%20Through%20A%20Closed%20Box.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 130e75f0-16ce-43c4-bece-e9240ede02f1
updated: 1486069652
title: Flux Through A Closed Box
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/5ENl4vn82bc/default.jpg
    - https://i3.ytimg.com/vi/5ENl4vn82bc/1.jpg
    - https://i3.ytimg.com/vi/5ENl4vn82bc/2.jpg
    - https://i3.ytimg.com/vi/5ENl4vn82bc/3.jpg
---
