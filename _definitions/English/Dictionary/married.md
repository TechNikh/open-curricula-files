---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/married
offline_file: ""
offline_thumbnail: ""
uuid: 866f82a5-a11f-4acc-9e18-4444ea4d3651
updated: 1484310471
title: married
categories:
    - Dictionary
---
marry
     v 1: take in marriage [syn: {get married}, {wed}, {conjoin}, {hook
          up with}, {get hitched with}, {espouse}]
     2: perform a marriage ceremony; "The minister married us on
        Saturday"; "We were wed the following week"; "The couple
        got spliced on Hawaii" [syn: {wed}, {tie}, {splice}]
     [also: {married}]
