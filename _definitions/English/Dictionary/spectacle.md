---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectacle
offline_file: ""
offline_thumbnail: ""
uuid: d309ec5b-d8a9-46a5-9f90-10c43c689bf5
updated: 1484310565
title: spectacle
categories:
    - Dictionary
---
spectacle
     n 1: something or someone seen (especially a notable or unusual
          sight); "the tragic spectacle of cripples trying to
          escape"
     2: an elaborate and remarkable display on a lavish scale
     3: a blunder that makes you look ridiculous; used in the phrase
        `make a spectacle of' yourself
