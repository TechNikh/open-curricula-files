---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotropic_activity
offline_file: ""
offline_thumbnail: ""
uuid: e5e6efe8-4e02-4f75-9159-499f9e92d53d
updated: 1484308764
title: Chaotropic activity
categories:
    - Entropy
---
Chaotropicity describes the entropic disordering of lipid bilayers and other biomacromolecules which is caused by substances dissolved in water. According to the original usage[1] and work carried out on cellular stress mechanisms and responses,[2][3][4] chaotropic substances do not necessarily disorder the structure of water.[5]
