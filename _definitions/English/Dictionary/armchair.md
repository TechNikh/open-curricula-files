---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/armchair
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456761
title: armchair
categories:
    - Dictionary
---
armchair
     adj : remote from actual involvement; "armchair warriors in the
           Pentagon"; "an armchair anthropologist" [syn: {armchair(a)}]
     n : chair with a support on each side for arms
