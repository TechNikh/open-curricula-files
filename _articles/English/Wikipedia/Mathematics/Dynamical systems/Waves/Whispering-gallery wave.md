---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Whispering-gallery_wave
offline_file: ""
offline_thumbnail: ""
uuid: f3446c4e-c540-416a-b7cd-41497f697600
updated: 1484308912
title: Whispering-gallery wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-WGMVisualized.jpg
tags:
    - Introduction
    - Other acoustic whispering-gallery waves
    - Whispering-gallery waves for light
    - Whispering-gallery waves for other systems
categories:
    - Waves
---
Whispering-gallery waves, or whispering-gallery modes, are a type of wave that can travel around a concave surface. Originally discovered for sound waves in the whispering gallery of St Paul’s Cathedral, they can exist for light and for other waves, with important applications in nondestructive testing, lasing, cooling and sensing, as well as in astronomy.
