---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-confident
offline_file: ""
offline_thumbnail: ""
uuid: 3e9037f6-b566-4486-8baa-1786061ed27a
updated: 1484308428
title: Self-confidence
tags:
    - History
    - Theories and correlations with other variables and factors
    - Self-confidence as an intra-psychological variable
    - Relationship to social influences
    - Variation between different categorical groups
    - children
    - Students
    - Men versus women
    - Self-confidence in different cultures
    - Athletes
    - Measures
    - >
        Various systemic-level theories and concepts related to
        self-confidence
    - Wheel of Wellness
    - Implicit vs. explicit
categories:
    - Uncategorized
---
The concept self-confidence as commonly used is self-assurance in one's personal judgment, ability, power, etc. One increases self-confidence from experiences of having mastered particular activities.[1] It is a positive[2] belief that in the future one can generally accomplish what one wishes to do. Self-confidence is not the same as self-esteem, which is an evaluation of one’s own worth, whereas self-confidence is more specifically trust in one’s ability to achieve some goal, which one meta-analysis suggested is similar to generalization of self-efficacy.[3] Abraham Maslow and many others after him have emphasized the need to distinguish between self-confidence as a generalized ...
