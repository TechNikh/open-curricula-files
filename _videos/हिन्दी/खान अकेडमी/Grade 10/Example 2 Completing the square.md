---
version: 1
type: video
provider: YouTube
id: e_OKePbnqb4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21d94d65-d3dc-4e8b-9511-3e35a8b2ccde
updated: 1486069617
title: Example 2 Completing the square
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/e_OKePbnqb4/default.jpg
    - https://i3.ytimg.com/vi/e_OKePbnqb4/1.jpg
    - https://i3.ytimg.com/vi/e_OKePbnqb4/2.jpg
    - https://i3.ytimg.com/vi/e_OKePbnqb4/3.jpg
---
