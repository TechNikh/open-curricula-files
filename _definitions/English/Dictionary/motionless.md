---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motionless
offline_file: ""
offline_thumbnail: ""
uuid: 25ab7e83-c11e-4ac7-95cd-83ccc1acfd63
updated: 1484310148
title: motionless
categories:
    - Dictionary
---
motionless
     adj : not in physical motion; "the inertia of an object at rest"
           [syn: {inactive}, {static}, {still}]
