---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Root_nodules
offline_file: ""
offline_thumbnail: ""
uuid: 6eec7320-38b7-4790-982b-ece1f441b24b
updated: 1484308366
title: Root nodule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Root-nodule01.jpg
tags:
    - Root nodule symbiosis
    - Leguminous family
    - Non-leguminous
    - Classification
    - Nodulation
    - Connection to root structure
    - In other species
categories:
    - Uncategorized
---
Root nodules occur on the roots of plants (primarily Fabaceae) that associate with symbiotic nitrogen-fixing bacteria. Under nitrogen-limiting conditions, capable plants form a symbiotic relationship with a host-specific strain of bacteria known as rhizobia. This process has evolved multiple times within the Fabaceae, as well as in other species found within the Rosid clade.[1] The Fabaceae include legume crops such as beans and peas.
