---
version: 1
type: video
provider: YouTube
id: EPMNWBpxJhg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a778e797-27dd-44ee-82b6-32c6a2db3dfe
updated: 1486069611
title: Multiplication word problem example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/EPMNWBpxJhg/default.jpg
    - https://i3.ytimg.com/vi/EPMNWBpxJhg/1.jpg
    - https://i3.ytimg.com/vi/EPMNWBpxJhg/2.jpg
    - https://i3.ytimg.com/vi/EPMNWBpxJhg/3.jpg
---
