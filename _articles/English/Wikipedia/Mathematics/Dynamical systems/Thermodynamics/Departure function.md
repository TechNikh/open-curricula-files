---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Departure_function
offline_file: ""
offline_thumbnail: ""
uuid: 17b09c19-d756-4e5c-8aa9-db938dc48298
updated: 1484308838
title: Departure function
tags:
    - General Expressions
    - Departure functions for Peng-Robinson equation of state
    - Correlated terms
categories:
    - Thermodynamics
---
In thermodynamics, a departure function is defined for any thermodynamic property as the difference between the property as computed for an ideal gas and the property of the species as it exists in the real world, for a specified temperature T and pressure P. Common departure functions include those for enthalpy, entropy, and internal energy.
