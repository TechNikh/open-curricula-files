---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bayesian-optimal_pricing
offline_file: ""
offline_thumbnail: ""
uuid: 1d9f6717-632b-450a-ba49-57d994bba077
updated: 1484309045
title: Bayesian-optimal pricing
tags:
    - Single item and single buyer
    - Single item and many buyers
    - Buyers with independent and identical valuations
    - Buyers with independent and different valuations
    - Different items and one unit-demand buyer
    - Different items and many unit-demand buyers
    - Many unit-demand buyers and sellers
categories:
    - Mechanism design
---
Bayesian-optimal pricing (BO pricing) is a kind of algorithmic pricing in which a seller determines the sell-prices based on probabilistic assumptions on the valuations of the buyers. It is a simple kind of a Bayesian-optimal mechanism, in which the price is determined in advance without collecting actual buyers' bids.
