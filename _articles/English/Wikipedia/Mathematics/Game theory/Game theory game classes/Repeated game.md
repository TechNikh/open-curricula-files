---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Repeated_game
offline_file: ""
offline_thumbnail: ""
uuid: 553aab6f-e844-482b-b484-6f7438a7f32f
updated: 1484309040
title: Repeated game
tags:
    - Finitely vs infinitely repeated games
    - Infinitely repeated games
    - Finitely repeated games
    - "Repeated prisoner's dilemma"
    - Solving repeated games
    - Incomplete information
categories:
    - Game theory game classes
---
In game theory, a repeated game (supergame or iterated game) is an extensive form game which consists in some number of repetitions of some base game (called a stage game). The stage game is usually one of the well-studied 2-person games. It captures the idea that a player will have to take into account the impact of his current action on the future actions of other players; this is sometimes called his reputation. The presence of different equilibrium properties is present because the threat of retaliation is real, since one will play the game again with the same person. It can be proved that every strategy that has a payoff greater than the minmax payoff can be a Nash Equilibrium, which ...
