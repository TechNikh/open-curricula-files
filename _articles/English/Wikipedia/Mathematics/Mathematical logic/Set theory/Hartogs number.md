---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hartogs_number
offline_file: ""
offline_thumbnail: ""
uuid: f54e0ba6-6c75-4aeb-8ea9-6c6708e49172
updated: 1484309504
title: Hartogs number
categories:
    - Set theory
---
In mathematics, specifically in axiomatic set theory, a Hartogs number is a particular kind of cardinal number. It was shown by Friedrich Hartogs in 1915, from ZF alone (that is, without using the axiom of choice), that there is a least well-ordered cardinal greater than a given well-ordered cardinal.
