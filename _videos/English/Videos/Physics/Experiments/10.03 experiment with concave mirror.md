---
version: 1
type: video
provider: YouTube
id: -8XRABa6aC4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 097c9f43-e111-4b73-9950-612db9360a78
updated: 1486069639
title: 10.03 experiment with concave mirror
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/-8XRABa6aC4/default.jpg
    - https://i3.ytimg.com/vi/-8XRABa6aC4/1.jpg
    - https://i3.ytimg.com/vi/-8XRABa6aC4/2.jpg
    - https://i3.ytimg.com/vi/-8XRABa6aC4/3.jpg
---
