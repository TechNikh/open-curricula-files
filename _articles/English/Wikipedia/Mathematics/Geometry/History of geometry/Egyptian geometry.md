---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Egyptian_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 522c3713-66a5-4c66-827e-3efdfcee5232
updated: 1484309244
title: Egyptian geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Egypt.svg_0.png
tags:
    - area
    - Volumes
    - Seqed
categories:
    - History of geometry
---
Egyptian geometry refers to geometry as it was developed and used in Ancient Egypt. Ancient Egyptian mathematics as discussed here spans a time period ranging from ca. 3000 BC to ca 300 BC.
