---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flatness_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: f1d045e3-13e3-4474-81db-e8b19e3b3a3b
updated: 1484309363
title: Flatness (mathematics)
categories:
    - Geometry stubs
---
In mathematics, the flatness (symbol: ⏥) of a surface is the degree to which it approximates a mathematical plane. The term is often generalized for higher-dimensional manifolds to describe the degree to which they approximate the Euclidean space of the same dimensionality. (See curvature.)
