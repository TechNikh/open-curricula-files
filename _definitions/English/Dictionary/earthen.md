---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earthen
offline_file: ""
offline_thumbnail: ""
uuid: 5642d0e3-c6fa-4cc6-8085-a50e47939b19
updated: 1484310262
title: earthen
categories:
    - Dictionary
---
earthen
     adj : made of earth (or baked clay); "an earthen pot"
