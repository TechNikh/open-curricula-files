---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peasant
offline_file: ""
offline_thumbnail: ""
uuid: 97d235d5-db96-4fac-aa8a-dbe5a1daec53
updated: 1484310561
title: peasant
categories:
    - Dictionary
---
peasant
     n 1: a country person [syn: {provincial}, {bucolic}]
     2: one of a (chiefly European) class of agricultural laborers
     3: a crude uncouth ill-bred person lacking culture or
        refinement [syn: {barbarian}, {boor}, {churl}, {Goth}, {tyke},
         {tike}]
