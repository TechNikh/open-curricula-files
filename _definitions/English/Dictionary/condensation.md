---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/condensation
offline_file: ""
offline_thumbnail: ""
uuid: 0ea99e40-ee25-439f-9309-5bade23c1f2c
updated: 1484310224
title: condensation
categories:
    - Dictionary
---
condensation
     n 1: (psychoanalysis) an unconscious process whereby two ideas or
          images combine into a single symbol; especially in
          dreams
     2: the process of changing from a gaseous to a liquid or solid
        state
     3: atmospheric moisture that has condensed because of cold
        [syn: {condensate}]
     4: the process or result of becoming smaller or pressed
        together; "the contraction of a gas on cooling" [syn: {compression},
         {contraction}]
     5: a shortened version of a written work [syn: {abridgement}, {abridgment},
         {capsule}]
     6: the act of increasing the density of something [syn: {condensing}]
