---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Friedman%E2%80%99s_SSCG_function'
offline_file: ""
offline_thumbnail: ""
uuid: 35075e71-1d11-4f7e-85cb-c735d0057e77
updated: 1484308662
title: Friedman’s SSCG function
categories:
    - Theorems in discrete mathematics
---
In mathematics, a simple subcubic graph is a finite simple graph in which each vertex has degree at most three. Suppose we have a sequence of simple subcubic graphs G1, G2, ... such that each graph Gi has at most i + k vertices (for some integer k) and for no i < j is Gi homeomorphically embeddable into (i.e. is a graph minor of) Gj.
