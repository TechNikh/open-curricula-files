---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thumb
offline_file: ""
offline_thumbnail: ""
uuid: 9ffbff8e-90a6-40c8-92fd-7f42b4c1c9dd
updated: 1484310309
title: thumb
categories:
    - Dictionary
---
thumb
     n 1: the thick short innermost digit of the forelimb [syn: {pollex}]
     2: the part of a glove that provides a covering for the thumb
     3: a convex molding having a cross section in the form of a
        quarter of a circle or of an ellipse [syn: {ovolo}, {quarter
        round}]
     v 1: travel by getting free rides from motorists [syn: {hitchhike},
           {hitch}]
     2: look through a book or other written material; "He thumbed
        through the report"; "She leafed through the volume" [syn:
         {flick}, {flip}, {riffle}, {leaf}, {riff}]
     3: feel or handle with the fingers; "finger the binding of the
        book" [syn: {finger}]
