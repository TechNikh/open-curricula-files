---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mixed
offline_file: ""
offline_thumbnail: ""
uuid: 43bc1885-3c28-41c5-b91a-41b230e6e5ff
updated: 1484310339
title: mixed
categories:
    - Dictionary
---
mixed
     adj 1: caused to combine or unite [syn: {amalgamated}, {intermingled},
             {integrated}]
     2: consisting of a haphazard assortment of different kinds
        (even to the point of incongruity); "an arrangement of
        assorted spring flowers"; "assorted sizes"; "miscellaneous
        accessories"; "a mixed program of baroque and contemporary
        music"; "a motley crew"; "sundry sciences commonly known
        as social"- I.A.Richards [syn: {assorted}, {miscellaneous},
         {motley}, {sundry(a)}]
     3: involving or composed of different races; "interracial
        schools"; "a mixed neighborhood" [syn: {interracial}]
