---
version: 1
type: video
provider: YouTube
id: TRAbZxQHlVw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/The%20Gravity%20of%20the%20Situation-%20Crash%20Course%20Astronomy%20%237.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d99ae9fa-7111-4e16-97a0-39384c05ff52
updated: 1486069684
title: 'The Gravity of the Situation- Crash Course Astronomy #7'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/TRAbZxQHlVw/default.jpg
    - https://i3.ytimg.com/vi/TRAbZxQHlVw/1.jpg
    - https://i3.ytimg.com/vi/TRAbZxQHlVw/2.jpg
    - https://i3.ytimg.com/vi/TRAbZxQHlVw/3.jpg
---
