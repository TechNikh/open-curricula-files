---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rotation
offline_file: ""
offline_thumbnail: ""
uuid: f326039d-f429-4ce8-8108-99de71255e6c
updated: 1484310366
title: rotation
categories:
    - Dictionary
---
rotation
     n 1: the act of rotating as if on an axis; "the rotation of the
          dancer kept time with the music" [syn: {rotary motion}]
     2: (mathematics) a transformation in which the coordinate axes
        are rotated by a fixed angle about the origin
     3: a single complete turn (axial or orbital); "the plane made
        three rotations before it crashed"; "the revolution of the
        earth about the sun takes one year" [syn: {revolution}, {gyration}]
     4: a planned recurrent sequence (of crops or personnel etc.);
        "crop rotation makes a balanced demand on the fertility of
        the soil"; "the manager had only four starting pitchers in
        his rotation"
