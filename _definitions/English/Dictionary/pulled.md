---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pulled
offline_file: ""
offline_thumbnail: ""
uuid: 5658cd42-5333-4ad8-b730-2ef77a8ebf7a
updated: 1484310366
title: pulled
categories:
    - Dictionary
---
pulled
     adj : drawn toward the source of the force; "this exercise must be
           done with the arms pulled back"
