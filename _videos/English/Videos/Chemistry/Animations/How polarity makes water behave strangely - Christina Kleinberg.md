---
version: 1
type: video
provider: YouTube
id: ASLUY2U1M-8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/How%20polarity%20makes%20water%20behave%20strangely%20-%20Christina%20Kleinberg.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8a4a4646-2c87-4c4d-a02f-8189b1f067df
updated: 1486069665
title: 'How polarity makes water behave strangely - Christina Kleinberg'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ASLUY2U1M-8/default.jpg
    - https://i3.ytimg.com/vi/ASLUY2U1M-8/1.jpg
    - https://i3.ytimg.com/vi/ASLUY2U1M-8/2.jpg
    - https://i3.ytimg.com/vi/ASLUY2U1M-8/3.jpg
---
