---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotic_hysteresis
offline_file: ""
offline_thumbnail: ""
uuid: b0ae46fa-bfe9-47df-b2f3-72fc2fc51252
updated: 1484308702
title: Chaotic hysteresis
categories:
    - Bifurcation theory
---
A nonlinear dynamical system exhibits chaotic hysteresis if it simultaneously exhibits chaotic dynamics (chaos theory) and hysteresis. As the latter involves the persistence of a state, such as magnetization, after the causal or exogenous force or factor is removed, it involves multiple equilibria for given sets of control conditions. Such systems generally exhibit sudden jumps from one equilibrium state to another (sometimes amenable to analysis using catastrophe theory). If chaotic dynamics appear either prior to or just after such jumps, or are persistent throughout each of the various equilibrium states, then the system is said to exhibit chaotic hysteresis. Chaotic dynamics are ...
