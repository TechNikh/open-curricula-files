---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afforestation
offline_file: ""
offline_thumbnail: ""
uuid: 732d6ca1-03ab-4239-896d-464fb4e5d7e3
updated: 1484310170
title: afforestation
categories:
    - Dictionary
---
afforestation
     n : the conversion of bare or cultivated land into forest
         (originally for the purpose of hunting)
