---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dimension
offline_file: ""
offline_thumbnail: ""
uuid: 38f56fb9-e850-4874-b238-1658660f18c0
updated: 1484309241
title: Dimension
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/236px-Squarecubetesseract.png
tags:
    - In mathematics
    - Complex dimension
    - Dimension of a vector space
    - Manifolds
    - Varieties
    - Krull dimension
    - Lebesgue covering dimension
    - Inductive dimension
    - Hausdorff dimension
    - Hilbert spaces
    - In physics
    - Spatial dimensions
    - Time
    - Additional dimensions
    - Networks and dimension
    - In literature
    - In philosophy
    - More dimensions
    - Topics by dimension
categories:
    - Geometric measurement
---
In physics and mathematics, the dimension of a mathematical space (or object) is informally defined as the minimum number of coordinates needed to specify any point within it.[1][2] Thus a line has a dimension of one because only one coordinate is needed to specify a point on it – for example, the point at 5 on a number line. A surface such as a plane or the surface of a cylinder or sphere has a dimension of two because two coordinates are needed to specify a point on it – for example, both a latitude and longitude are required to locate a point on the surface of a sphere. The inside of a cube, a cylinder or a sphere is three-dimensional because three coordinates are needed to locate ...
