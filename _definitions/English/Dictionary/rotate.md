---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rotate
offline_file: ""
offline_thumbnail: ""
uuid: fbddc4d9-f216-4471-9c06-b0e11e030849
updated: 1484310365
title: rotate
categories:
    - Dictionary
---
rotate
     v 1: turn on or around an axis or a center; "The Earth revolves
          around the Sun"; "The lamb roast rotates on a spit over
          the fire" [syn: {revolve}, {go around}]
     2: exchange on a regular basis; "We rotate the lead soprano
        every night"
     3: cause to turn on an axis or center; "Rotate the handle"
        [syn: {circumvolve}]
     4: perform a job or duty on a rotating basis; "Interns have to
        rotate for a few months"
     5: turn outward; "These birds can splay out their toes";
        "ballet dancers can rotate their legs out by 90 degrees"
        [syn: {turn out}, {splay}, {spread out}]
     6: plant or grow in a fixed cyclic order of ...
