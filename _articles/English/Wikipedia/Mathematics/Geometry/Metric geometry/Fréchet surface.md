---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fr%C3%A9chet_surface'
offline_file: ""
offline_thumbnail: ""
uuid: 1255b0f8-4e78-4cb7-8577-419b8e89acfe
updated: 1484309262
title: Fréchet surface
tags:
    - Definitions
    - Properties
categories:
    - Metric geometry
---
In mathematics, a Fréchet surface is an equivalence class of parametrized surfaces in a metric space. In other words, a Fréchet surface is a way of thinking about surfaces independently of how they are "written down" (parametrized). The concept is named after the French mathematician Maurice Fréchet.
