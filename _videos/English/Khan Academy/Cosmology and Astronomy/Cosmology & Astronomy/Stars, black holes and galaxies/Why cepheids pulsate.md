---
version: 1
type: video
provider: YouTube
id: X_3QAB3o4Vw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Why%20cepheids%20pulsate.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4b8aaefa-2382-44dc-b4d1-2816f5155833
updated: 1486069686
title: Why cepheids pulsate
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/X_3QAB3o4Vw/default.jpg
    - https://i3.ytimg.com/vi/X_3QAB3o4Vw/1.jpg
    - https://i3.ytimg.com/vi/X_3QAB3o4Vw/2.jpg
    - https://i3.ytimg.com/vi/X_3QAB3o4Vw/3.jpg
---
