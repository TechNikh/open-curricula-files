---
version: 1
type: video
provider: YouTube
id: GXgMd2JE16w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b50eac6a-ab84-40e3-8238-62e88756c796
updated: 1486069625
title: The Power of One Percent
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/GXgMd2JE16w/default.jpg
    - https://i3.ytimg.com/vi/GXgMd2JE16w/1.jpg
    - https://i3.ytimg.com/vi/GXgMd2JE16w/2.jpg
    - https://i3.ytimg.com/vi/GXgMd2JE16w/3.jpg
---
