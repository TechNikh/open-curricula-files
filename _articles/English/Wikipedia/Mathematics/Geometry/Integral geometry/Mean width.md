---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mean_width
offline_file: ""
offline_thumbnail: ""
uuid: 33786774-1ad6-443f-b809-e5a7d4130e9b
updated: 1484309255
title: Mean width
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Width_in_dir_n_for_mean_width.png
tags:
    - Mean widths of convex bodies in low dimensions
    - One dimension
    - Two dimensions
    - Three dimensions
categories:
    - Integral geometry
---
In geometry, the mean width is a measure of dimension length of the "size" of a body; see Hadwiger's theorem for more about the available measures of bodies. In 
  
    
      
        n
      
    
    {\displaystyle n}
  
 dimensions, one has to consider 
  
    
      
        (
        n
        −
        1
        )
      
    
    {\displaystyle (n-1)}
  
-dimensional hyperplanes perpendicular to a given direction 
  
    
      
        
          
            
              n
              ^
            
          
        
      
    
    {\displaystyle {\hat {n}}}
  
 in 
  
    
      
        
          S
          
            n
            −
            1
          
       ...
