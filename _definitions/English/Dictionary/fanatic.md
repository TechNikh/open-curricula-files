---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fanatic
offline_file: ""
offline_thumbnail: ""
uuid: f23fe52c-9cce-4cfb-877a-fe3282f97c3d
updated: 1484310551
title: fanatic
categories:
    - Dictionary
---
fanatic
     adj : marked by excessive enthusiasm for and intense devotion to a
           cause or idea; "rabid isolationist" [syn: {fanatical},
           {overzealous}, {rabid}]
     n : a person motivated by irrational enthusiasm (as for a
         cause); "A fanatic is one who can't change his mind and
         won't change the subject"--Winston Churchill [syn: {fiend}]
