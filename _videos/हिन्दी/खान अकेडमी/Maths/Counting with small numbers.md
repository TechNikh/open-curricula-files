---
version: 1
type: video
provider: YouTube
id: 6xj4aw72HWM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 716ba0de-5869-4f4b-99da-c92b950c2d56
updated: 1486069609
title: Counting with small numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/6xj4aw72HWM/default.jpg
    - https://i3.ytimg.com/vi/6xj4aw72HWM/1.jpg
    - https://i3.ytimg.com/vi/6xj4aw72HWM/2.jpg
    - https://i3.ytimg.com/vi/6xj4aw72HWM/3.jpg
---
