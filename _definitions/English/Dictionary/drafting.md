---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drafting
offline_file: ""
offline_thumbnail: ""
uuid: edfb730d-d4cf-4910-b1f8-cf27ddb5ee6c
updated: 1484310595
title: drafting
categories:
    - Dictionary
---
drafting
     n 1: writing a first version to be filled out and polished later
     2: the craft of drawing blueprints [syn: {mechanical drawing}]
     3: the creation of artistic drawings; "he learned drawing from
        his father" [syn: {drawing}, {draftsmanship}]
