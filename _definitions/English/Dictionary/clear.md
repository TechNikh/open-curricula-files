---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clear
offline_file: ""
offline_thumbnail: ""
uuid: e87a3460-c74d-4e0f-bbb7-f7cffef7047e
updated: 1484310281
title: clear
categories:
    - Dictionary
---
clear
     adj 1: clear to the mind; "a clear and present danger"; "a clear
            explanation"; "a clear case of murder"; "a clear
            indication that she was angry"; "gave us a clear idea
            of human nature" [ant: {unclear}]
     2: free from confusion or doubt; "a complex problem requiring a
        clear head"; "not clear about what is expected of us"
     3: affording free passage or view; "a clear view"; "a clear
        path to victory" [syn: {open}]
     4: free from cloudiness; allowing light to pass through; "clear
        water"; "clear plastic bags"; "clear glass"; "the air is
        clear and clean" [ant: {opaque}]
     5: free from contact or proximity ...
