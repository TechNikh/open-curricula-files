---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gear
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423881
title: gear
categories:
    - Dictionary
---
gear
     n 1: a toothed wheel that engages another toothed mechanism in
          order to change the speed or direction of transmitted
          motion [syn: {gear wheel}, {cogwheel}]
     2: a mechanism for transmitting motion by gears for some
        specific purpose (as the steering gear of a vehicle) [syn:
         {gear mechanism}]
     3: equipment consisting of miscellaneous articles needed for a
        particular operation or sport etc. [syn: {paraphernalia},
        {appurtenances}]
     v : set the level or character of; "She pitched her speech to
         the teenagers in the audience" [syn: {pitch}]
