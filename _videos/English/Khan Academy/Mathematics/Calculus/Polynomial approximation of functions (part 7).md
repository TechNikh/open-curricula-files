---
version: 1
type: video
provider: YouTube
id: bC5Lahh4Aus
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximation%20of%20functions%20%28part%207%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 58ddd638-f29f-4e6c-8561-7a4ff3ed3d90
updated: 1486069692
title: Polynomial approximation of functions (part 7)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/bC5Lahh4Aus/default.jpg
    - https://i3.ytimg.com/vi/bC5Lahh4Aus/1.jpg
    - https://i3.ytimg.com/vi/bC5Lahh4Aus/2.jpg
    - https://i3.ytimg.com/vi/bC5Lahh4Aus/3.jpg
---
