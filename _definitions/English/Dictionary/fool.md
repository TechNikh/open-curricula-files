---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fool
offline_file: ""
offline_thumbnail: ""
uuid: b8f09f8b-e01b-434f-bec7-ebdb0b2e383c
updated: 1484310146
title: fool
categories:
    - Dictionary
---
fool
     n 1: a person who lacks good judgment [syn: {sap}, {saphead}, {muggins},
           {tomfool}]
     2: a person who is gullible and easy to take advantage of [syn:
         {chump}, {gull}, {mark}, {patsy}, {fall guy}, {sucker}, {soft
        touch}, {mug}]
     3: a professional clown employed to entertain a king or
        nobleman in the middle ages [syn: {jester}, {motley fool}]
     v 1: make a fool or dupe of [syn: {gull}, {befool}]
     2: spend frivolously and unwisely; "Fritter away one's
        inheritance" [syn: {fritter}, {frivol away}, {dissipate},
        {shoot}, {fritter away}, {fool away}]
     3: fool or hoax; "The immigrant was duped because he trusted
        ...
