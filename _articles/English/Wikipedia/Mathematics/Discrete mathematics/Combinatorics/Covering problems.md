---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_problems
offline_file: ""
offline_thumbnail: ""
uuid: 5de475b8-3715-471d-b14e-74239110afdd
updated: 1484308577
title: Covering problems
tags:
    - General LP formulation
    - Other uses
    - Notes
categories:
    - Combinatorics
---
In combinatorics and computer science, covering problems are computational problems that ask whether a certain combinatorial structure 'covers' another, or how large the structure has to be to do that. Covering problems are minimization problems and usually linear programs, whose dual problems are called packing problems.
