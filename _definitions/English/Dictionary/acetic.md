---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acetic
offline_file: ""
offline_thumbnail: ""
uuid: 36ca26fc-320d-4f54-bd2e-f5092f4f4329
updated: 1484310379
title: acetic
categories:
    - Dictionary
---
acetic
     adj : relating to or containing acetic acid
