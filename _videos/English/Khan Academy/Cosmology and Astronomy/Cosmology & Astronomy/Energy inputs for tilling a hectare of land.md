---
version: 1
type: video
provider: YouTube
id: 5I9dH5im24U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Energy%20inputs%20for%20tilling%20a%20hectare%20of%20land.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b2038c0f-c0d1-44bf-9303-07ca2237f559
updated: 1486069686
title: Energy inputs for tilling a hectare of land
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/5I9dH5im24U/default.jpg
    - https://i3.ytimg.com/vi/5I9dH5im24U/1.jpg
    - https://i3.ytimg.com/vi/5I9dH5im24U/2.jpg
    - https://i3.ytimg.com/vi/5I9dH5im24U/3.jpg
---
