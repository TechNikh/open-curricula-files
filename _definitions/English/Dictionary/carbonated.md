---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carbonated
offline_file: ""
offline_thumbnail: ""
uuid: ff0f4465-68ef-4740-9eb2-4df6b4534856
updated: 1484310383
title: carbonated
categories:
    - Dictionary
---
carbonated
     adj : having carbonation (especially artificially carbonated)
