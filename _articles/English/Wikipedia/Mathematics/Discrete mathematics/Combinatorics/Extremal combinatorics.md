---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extremal_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 5dbd57db-2651-4604-ac3c-5e6246c0f5a1
updated: 1484308572
title: Extremal combinatorics
categories:
    - Combinatorics
---
Extremal combinatorics is a field of combinatorics, which is itself a part of mathematics. Extremal combinatorics studies how large or how small a collection of finite objects (numbers, graphs, vectors, sets, etc.) can be, if it has to satisfy certain restrictions.
