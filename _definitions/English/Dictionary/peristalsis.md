---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peristalsis
offline_file: ""
offline_thumbnail: ""
uuid: d6ddf378-a2a4-4fb1-a687-5530b0d28465
updated: 1484310337
title: peristalsis
categories:
    - Dictionary
---
peristalsis
     n : the process of wave-like muscle contractions of the
         alimentary tract that moves food along [syn: {vermiculation}]
         [ant: {anastalsis}]
     [also: {peristalses} (pl)]
