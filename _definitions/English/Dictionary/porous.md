---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/porous
offline_file: ""
offline_thumbnail: ""
uuid: b8ede082-d431-47c9-aef1-3b9a57e7448c
updated: 1484310437
title: porous
categories:
    - Dictionary
---
porous
     adj 1: able to absorb fluids; "the partly porous walls of our
            digestive system"; "compacting the soil to make it
            less porous"
     2: full of pores or vessels or holes [syn: {poriferous}] [ant:
        {nonporous}]
     3: allowing passage in and out; "our unfenced and largely
        unpoliced border inevitably has been very porous" [syn: {holey}]
