---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/literature
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484430721
title: literature
categories:
    - Dictionary
---
literature
     n 1: creative writing of recognized artistic value
     2: the humanistic study of a body of literature; "he took a
        course in Russian lit" [syn: {lit}]
     3: published writings in a particular style on a particular
        subject; "the technical literature"; "one aspect of
        Waterloo has not yet been treated in the literature"
     4: the profession or art of a writer; "her place in literature
        is secure"
