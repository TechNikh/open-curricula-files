---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hill
offline_file: ""
offline_thumbnail: ""
uuid: 238e0abc-c2a4-4f56-8d5d-52ceac2a63be
updated: 1484310435
title: hill
categories:
    - Dictionary
---
hill
     n 1: a local and well-defined elevation of the land
     2: structure consisting of an artificial heap or bank usually
        of earth or stones; "they built small mounds to hide
        behind" [syn: {mound}]
     3: United States railroad tycoon (1838-1916) [syn: {J. J. Hill},
         {James Jerome Hill}]
     4: risque English comedian (1925-1992) [syn: {Benny Hill}, {Alfred
        Hawthorne}]
     5: (baseball) the slight elevation on which the pitcher stands
        [syn: {mound}, {pitcher's mound}]
     v : form into a hill
