---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Computer_Games_Association
offline_file: ""
offline_thumbnail: ""
uuid: 53a04f76-9685-4ff6-a7c2-8713d517e3f6
updated: 1484309004
title: International Computer Games Association
categories:
    - Game artificial intelligence
---
The International Computer Games Association (ICGA) was founded as the International Computer Chess Association (ICCA) in 1977 by computer chess programmers to organise championship events for computer programs and to facilitate the sharing of technical knowledge via the ICCA Journal.
