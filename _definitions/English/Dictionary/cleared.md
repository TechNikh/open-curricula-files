---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cleared
offline_file: ""
offline_thumbnail: ""
uuid: 2cf7930b-c54d-48dc-9472-6e5ad6c6d246
updated: 1484310254
title: cleared
categories:
    - Dictionary
---
cleared
     adj 1: rid of objects or obstructions such as e.g. trees and brush;
            "cleared land"; "cleared streets free of fallen trees
            and debris"; "a cleared passage through the
            underbrush"; "played poker on the cleared dining room
            table" [ant: {uncleared}]
     2: freed from any question of guilt; "is absolved from all
        blame"; "was now clear of the charge of cowardice"; "his
        official honor is vindicated" [syn: {absolved}, {clear}, {exculpated},
         {exonerated}, {vindicated}]
