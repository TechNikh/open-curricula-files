---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loyal
offline_file: ""
offline_thumbnail: ""
uuid: 686fe32b-9b80-4576-9e18-1ee1549c8df3
updated: 1484310138
title: loyal
categories:
    - Dictionary
---
loyal
     adj 1: steadfast in allegiance or duty; "loyal subjects"; "loyal
            friends stood by him" [ant: {disloyal}]
     2: inspired by love for your country [syn: {patriotic}] [ant: {unpatriotic}]
     3: unwavering in devotion to friend or vow or cause; "a firm
        ally"; "loyal supporters"; "the true-hearted soldier...of
        Tippecanoe"- Campaign song for William Henry Harrison;
        "fast friends" [syn: {firm}, {truehearted}, {fast(a)}]
