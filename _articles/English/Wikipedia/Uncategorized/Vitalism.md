---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vitalism
offline_file: ""
offline_thumbnail: ""
uuid: 49493598-7f19-4573-a1e0-284dd3caaa4e
updated: 1484308394
title: Vitalism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Sample_of_Urea.jpg
tags:
    - Philosophy
    - Science
    - Relationship to emergentism
    - Mesmerism
    - Alternative Medicine
    - Criticism
    - Notes
categories:
    - Uncategorized
---
Vitalism is a discredited scientific hypothesis that "living organisms are fundamentally different from non-living entities because they contain some non-physical element or are governed by different principles than are inanimate things". [1] a Where vitalism explicitly invokes a vital principle, that element is often referred to as the "vital spark", "energy" or "élan vital", which some equate with the soul.
