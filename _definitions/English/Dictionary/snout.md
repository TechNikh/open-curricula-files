---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/snout
offline_file: ""
offline_thumbnail: ""
uuid: 97ddb3ad-944d-4729-8d35-6c2a2cd8a706
updated: 1484310459
title: snout
categories:
    - Dictionary
---
snout
     n 1: a long projecting or anterior elongation of an animal's
          head; especially the nose [syn: {neb}]
     2: informal terms for the nose [syn: {beak}, {honker}, {hooter},
         {nozzle}, {snoot}, {schnozzle}, {schnoz}]
     3: beaklike projection of the anterior part of the head of
        certain insects such as e.g. weevils [syn: {rostrum}]
