---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spectral_clustering
offline_file: ""
offline_thumbnail: ""
uuid: b178f3b5-7e32-47b6-be7b-1b7bdfafbe05
updated: 1484309376
title: Spectral clustering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graf2.svg.png
tags:
    - Algorithms
    - Relationship with k-means
    - Measures to compare clusterings
categories:
    - Algebraic graph theory
---
In multivariate statistics and the clustering of data, spectral clustering techniques make use of the spectrum (eigenvalues) of the similarity matrix of the data to perform dimensionality reduction before clustering in fewer dimensions. The similarity matrix is provided as an input and consists of a quantitative assessment of the relative similarity of each pair of points in the dataset.
