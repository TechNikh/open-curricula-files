---
version: 1
type: video
provider: YouTube
id: -1ygI3tNOZk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bc03d1fd-4639-418d-84eb-e39ad13d8e7b
updated: 1486069611
title: Division 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/-1ygI3tNOZk/default.jpg
    - https://i3.ytimg.com/vi/-1ygI3tNOZk/1.jpg
    - https://i3.ytimg.com/vi/-1ygI3tNOZk/2.jpg
    - https://i3.ytimg.com/vi/-1ygI3tNOZk/3.jpg
---
