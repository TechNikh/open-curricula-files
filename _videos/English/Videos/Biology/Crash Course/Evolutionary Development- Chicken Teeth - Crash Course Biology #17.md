---
version: 1
type: video
provider: YouTube
id: 9sjwlxQ_6LI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Evolutionary%20Development-%20Chicken%20Teeth%20-%20Crash%20Course%20Biology%20%2317.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fa847f31-d104-4898-8a42-1ea0d41b80e3
updated: 1486069678
title: 'Evolutionary Development- Chicken Teeth - Crash Course Biology #17'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/9sjwlxQ_6LI/default.jpg
    - https://i3.ytimg.com/vi/9sjwlxQ_6LI/1.jpg
    - https://i3.ytimg.com/vi/9sjwlxQ_6LI/2.jpg
    - https://i3.ytimg.com/vi/9sjwlxQ_6LI/3.jpg
---
