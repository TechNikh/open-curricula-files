---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urbanisation
offline_file: ""
offline_thumbnail: ""
uuid: 5fa9793b-cc0b-436e-83fa-3f471e471edb
updated: 1484310479
title: urbanisation
categories:
    - Dictionary
---
urbanisation
     n 1: the condition of being urbanized [syn: {urbanization}]
     2: the social process whereby cities grow and societies become
        more urban [syn: {urbanization}]
