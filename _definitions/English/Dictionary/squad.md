---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/squad
offline_file: ""
offline_thumbnail: ""
uuid: 463521b3-ce8b-4fae-adb1-80ce263770be
updated: 1484310178
title: squad
categories:
    - Dictionary
---
squad
     n 1: a smallest army unit
     2: a cooperative unit [syn: {team}]
