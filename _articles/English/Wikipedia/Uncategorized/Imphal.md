---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imphal
offline_file: ""
offline_thumbnail: ""
uuid: 49e179d0-ee85-4aa0-990a-9b92b4efb2d6
updated: 1484308413
title: Imphal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Imphal_War_cemetery.jpg
tags:
    - History
    - Geography and Climate
    - Landmarks of Imphal
    - Kangla
    - Hiyangthang Lairembi Temple Complex
    - Shree Govindajee Temple
    - Imphal War Cemetery
    - "Women's Market (Ima Keithel)"
    - Transport
    - air
    - Rail
    - Demographics
    - Education
    - Universities
    - Technical colleges
    - Medical colleges
categories:
    - Uncategorized
---
The city of Imphal (/ˈɪmpəl/,  Imphal.ogg (help·info)) is the capital of the Indian state of Manipur. The ruins of the Palace of Kangla, the royal seat of the erstwhile Kingdom of Manipur, are in the city centre, surrounded by a moat.
