---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/figurative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484586961
title: figurative
categories:
    - Dictionary
---
figurative
     adj 1: (used of the meanings of words or text) not literal; using
            figures of speech; "figurative language" [syn: {nonliteral}]
            [ant: {literal}]
     2: consisting of or forming human or animal figures; "a figural
        design"; "the figurative art of the humanistic tradition"-
        Herbert Read [syn: {figural}]
