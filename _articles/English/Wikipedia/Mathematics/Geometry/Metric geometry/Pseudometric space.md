---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pseudometric_space
offline_file: ""
offline_thumbnail: ""
uuid: 4a4493b3-3c3c-4fec-b5c8-f17bede3eb34
updated: 1484309273
title: Pseudometric space
tags:
    - Definition
    - Examples
    - Topology
    - Metric identification
    - Notes
categories:
    - Metric geometry
---
In mathematics, a pseudometric space is a generalized metric space in which the distance between two distinct points can be zero. In the same way as every normed space is a metric space, every seminormed space is a pseudometric space. Because of this analogy the term semimetric space (which has a different meaning in topology) is sometimes used as a synonym, especially in functional analysis.
