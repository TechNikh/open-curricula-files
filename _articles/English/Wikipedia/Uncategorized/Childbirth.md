---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Child_birth
offline_file: ""
offline_thumbnail: ""
uuid: ac7158b7-1588-46f3-8159-982165654aeb
updated: 1484308366
title: Childbirth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Postpartum_baby2.jpg
tags:
    - Signs and symptoms
    - Descriptions
    - Psychological
    - Normal birth
    - Onset of labour
    - 'First stage: latent phase'
    - 'First stage: active phase'
    - 'Second stage: fetal expulsion'
    - 'Third stage: placenta delivery'
    - Fourth stage
    - Management
    - Preparation
    - Active management
    - Labour induction and elective cesarean
    - Pain control
    - Non pharmaceutical
    - Pharmaceutical
    - Augmentation
    - Episiotomy
    - Instrumental delivery
    - Multiple births
    - Support
    - Fetal monitoring
    - External monitoring
    - Internal monitoring
    - Collecting stem cells
    - Complications
    - Pre-term
    - Labour complications
    - Obstructed labour
    - Maternal complications
    - Fetal complications
    - Mechanical fetal injury
    - Neonatal infection
    - Neonatal death
    - Intrapartum asphyxia
    - Society and culture
    - Facilities
    - Associated professions
    - Costs
categories:
    - Uncategorized
---
Childbirth, also known as labour and delivery, is the ending of a pregnancy by one or more babies leaving a woman's uterus.[1] In 2015 there were about 135 million births globally.[2] About 15 million were born before 37 weeks of gestation,[3] while between 3 and 12% were born after 42 weeks.[4] In the developed world most deliveries occur in hospital,[5][6] while in the developing world most births take place at home with the support of a traditional birth attendant.[7]
