---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ultralimit
offline_file: ""
offline_thumbnail: ""
uuid: 3cc59d76-25ea-4925-b285-cf26ec9aa21a
updated: 1484309272
title: Ultralimit
tags:
    - Ultrafilters
    - Limit of a sequence of points with respect to an ultrafilter
    - Ultralimit of metric spaces with specified base-points
    - On basepoints in the case of uniformly bounded spaces
    - Basic properties of ultralimits
    - Asymptotic cones
    - Examples
    - Footnotes
    - Basic References
categories:
    - Metric geometry
---
In mathematics, an ultralimit is a geometric construction that assigns to a sequence of metric spaces Xn a limiting metric space. The notion of an ultralimit captures the limiting behavior of finite configurations in the spaces Xn and uses an ultrafilter to avoid the process of repeatedly passing to subsequences to ensure convergence. An ultralimit is a generalization of the notion of Gromov-Hausdorff convergence of metric spaces.
