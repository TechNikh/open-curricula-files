---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radioactive_isotopes
offline_file: ""
offline_thumbnail: ""
uuid: 2b506d7c-bf35-493f-9a8f-760304deafa6
updated: 1484308378
title: Radionuclide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon_5.png
tags:
    - Origin
    - Natural
    - Nuclear fission
    - Synthetic
    - Uses
    - Examples
    - Americium-241
    - Gadolinium-153
    - Impacts on organisms
    - >
        Summary table for classes of nuclides, "stable" and
        radioactive
    - List of commercially available radionuclides
    - Gamma emission only
    - Beta emission only
    - Alpha emission only
    - Multiple radiation emitters
    - Notes
categories:
    - Uncategorized
---
A radionuclide (radioactive nuclide, radioisotope or radioactive isotope) is an atom that has excess nuclear energy, making it unstable. This excess energy can be either emitted from the nucleus as gamma radiation, or create and emit from the nucleus a new particle (alpha particle or beta particle), or transfer this excess energy to one of its electrons, causing that electron to be ejected as a conversion electron. During those processes, the radionuclide is said to undergo radioactive decay.[1] These emissions constitute ionizing radiation. The unstable nucleus is more stable following the emission, but will sometimes undergo further decay. Radioactive decay is a random process at the ...
