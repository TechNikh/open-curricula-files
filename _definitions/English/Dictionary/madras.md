---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/madras
offline_file: ""
offline_thumbnail: ""
uuid: ffd08c54-e37b-4c3d-9e04-4f265239526d
updated: 1484310581
title: madras
categories:
    - Dictionary
---
Madras
     n 1: a state in southeastern India on the Bay of Bengal (south of
          Andhra Pradesh); formerly Madras [syn: {Tamil Nadu}]
     2: a city in Tamil Nadu on the Bay of Bengal; formerly Madras
        [syn: {Chennai}]
     3: a light patterned cotton cloth
