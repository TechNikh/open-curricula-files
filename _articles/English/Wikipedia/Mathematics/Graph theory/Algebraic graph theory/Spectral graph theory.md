---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spectral_graph_theory
offline_file: ""
offline_thumbnail: ""
uuid: b27eff89-4c9d-4d06-b936-ee689a70b675
updated: 1484309373
title: Spectral graph theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Isospectral_enneahedra.svg.png
tags:
    - Isospectral graphs
    - Cheeger inequality
    - Cheeger constant
    - Cheeger inequality
    - Historical outline
categories:
    - Algebraic graph theory
---
In mathematics, spectral graph theory is the study of properties of a graph in relationship to the characteristic polynomial, eigenvalues, and eigenvectors of matrices associated to the graph, such as its adjacency matrix or Laplacian matrix.
