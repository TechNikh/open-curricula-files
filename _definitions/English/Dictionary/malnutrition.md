---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malnutrition
offline_file: ""
offline_thumbnail: ""
uuid: c7c77d30-aa2f-4b9e-ab62-6d42d1e242c4
updated: 1484310539
title: malnutrition
categories:
    - Dictionary
---
malnutrition
     n : a state of poor nutrition; can result from insufficient or
         excessive or unbalanced diet or from inability to absorb
         foods
