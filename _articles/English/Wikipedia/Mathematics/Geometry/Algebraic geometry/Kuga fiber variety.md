---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kuga_fiber_variety
offline_file: ""
offline_thumbnail: ""
uuid: 4fe6eb02-8a8b-4841-a498-4e3f54780d5d
updated: 1484309156
title: Kuga fiber variety
categories:
    - Algebraic geometry
---
In algebraic geometry, a Kuga fiber variety, introduced by Kuga (1966), is a fiber space whose fibers are abelian varieties and whose base space is an arithmetic quotient of a Hermitian symmetric space.
