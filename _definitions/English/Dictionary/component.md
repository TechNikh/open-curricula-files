---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/component
offline_file: ""
offline_thumbnail: ""
uuid: bdef9030-55d1-4b2e-9d76-fd0bc4212818
updated: 1484310337
title: component
categories:
    - Dictionary
---
component
     n 1: an abstract part of something; "jealousy was a component of
          his character"; "two constituents of a musical
          composition are melody and harmony"; "the grammatical
          elements of a sentence"; "a key factor in her success";
          "humor: an effective ingredient of a speech" [syn: {constituent},
           {element}, {factor}, {ingredient}]
     2: something determined in relation to something that includes
        it; "he wanted to feel a part of something bigger than
        himself"; "I read a portion of the manuscript"; "the
        smaller component is hard to reach" [syn: {part}, {portion},
         {component part}]
     3: an artifact ...
