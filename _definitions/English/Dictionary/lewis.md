---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lewis
offline_file: ""
offline_thumbnail: ""
uuid: 8d5a324d-9a4b-4ceb-a6c8-884933862cc0
updated: 1484310401
title: lewis
categories:
    - Dictionary
---
Lewis
     n 1: United States rock star singer and pianist (born in 1935)
          [syn: {Jerry Lee Lewis}]
     2: United States athlete who won gold medals at the Olympics
        for his skill in sprinting and jumping (born in 1961)
        [syn: {Carl Lewis}, {Frederick Carleton Lewis}]
     3: United States explorer and soldier who lead led an
        expedition from St. Louis to the mouth of the Columbia
        River (1774-1809) [syn: {Meriwether Lewis}]
     4: United States labor leader who was president of the United
        Mine Workers of America from 1920 to 1960 and president of
        the Congress of Industrial Organizations from 1935 to 1940
        (1880-1969) [syn: {John ...
