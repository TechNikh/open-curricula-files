---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frank_Morgan_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 8697de4d-7ac0-4b9e-bdd5-ee18a39da7ba
updated: 1484309093
title: Frank Morgan (mathematician)
tags:
    - Current work
    - Awards and honors
    - Books
    - Notes
categories:
    - Geometers
---
Frank Morgan is an American mathematician and the Webster Atwell '21 Professor of Mathematics at Williams College, specialising in geometric measure theory and minimal surfaces.
