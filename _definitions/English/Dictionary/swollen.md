---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swollen
offline_file: ""
offline_thumbnail: ""
uuid: a0adbf0c-68f1-4884-9862-078fffd16a42
updated: 1484310158
title: swollen
categories:
    - Dictionary
---
swell
     adj : very good; "he did a bully job"; "a neat sports car"; "had a
           great time at the party"; "you look simply smashing"
           [syn: {bang-up}, {bully}, {corking}, {cracking}, {dandy},
            {great}, {groovy}, {keen}, {neat}, {nifty}, {not
           bad(p)}, {peachy}, {slap-up}, {smashing}]
     n 1: the undulating movement of the surface of the open sea [syn:
           {crestless wave}]
     2: a rounded elevation (especially one on an ocean floor)
     3: a crescendo followed by a decrescendo
     4: a man who is much concerned with his dress and appearance
        [syn: {dandy}, {dude}, {fop}, {gallant}, {sheik}, {beau},
        {fashion plate}, ...
