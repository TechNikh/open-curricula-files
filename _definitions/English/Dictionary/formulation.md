---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formulation
offline_file: ""
offline_thumbnail: ""
uuid: 5da757f6-3709-478a-b3a8-e038fbf05d6f
updated: 1484310595
title: formulation
categories:
    - Dictionary
---
formulation
     n 1: a substance prepared according to a formula [syn: {preparation}]
     2: inventing or contriving an idea or explanation and
        formulating it mentally [syn: {conceptualization}, {conceptualisation}]
     3: the style of expressing yourself; "he suggested a better
        formulation"; "his manner of expression showed how much he
        cared" [syn: {expression}]
