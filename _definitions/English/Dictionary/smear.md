---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smear
offline_file: ""
offline_thumbnail: ""
uuid: 539d4877-1dcd-423f-a5bf-73033010f041
updated: 1484310330
title: smear
categories:
    - Dictionary
---
smear
     n 1: slanderous defamation [syn: {vilification}, {malignment}]
     2: a thin tissue or blood sample spread on a glass slide and
        stained for cytologic examination and diagnosis under a
        microscope [syn: {cytologic smear}, {cytosmear}]
     3: a blemish made by dirt; "he had a smudge on his cheek" [syn:
         {smudge}, {spot}, {blot}, {daub}, {smirch}, {slur}]
     4: an act that brings discredit to the person who does it; "he
        made a huge blot on his copybook" [syn: {blot}, {smirch},
        {spot}, {stain}]
     v 1: stain by smearing or daubing with a dirty substance
     2: make a smudge on; soil by smudging [syn: {blur}, {smudge}, {smutch}]
     3: ...
