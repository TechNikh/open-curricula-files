---
version: 1
type: video
provider: YouTube
id: mzjZO6yA1oo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a056cdbe-ed63-4638-b865-77d0cb2b3748
updated: 1486069626
title: Atva Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/mzjZO6yA1oo/default.jpg
    - https://i3.ytimg.com/vi/mzjZO6yA1oo/1.jpg
    - https://i3.ytimg.com/vi/mzjZO6yA1oo/2.jpg
    - https://i3.ytimg.com/vi/mzjZO6yA1oo/3.jpg
---
