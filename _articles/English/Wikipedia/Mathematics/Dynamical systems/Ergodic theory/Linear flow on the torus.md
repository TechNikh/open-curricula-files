---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_flow_on_the_torus
offline_file: ""
offline_thumbnail: ""
uuid: acf18303-c4d3-4412-8700-84673e6438e3
updated: 1484308777
title: Linear flow on the torus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Irrational_Rotation_on_a_2_Torus.png
categories:
    - Ergodic theory
---
In mathematics, especially in the area of mathematical analysis known as dynamical systems theory, a linear flow on the torus is a flow on the n-dimensional torus
