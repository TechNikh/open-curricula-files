---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/listen
offline_file: ""
offline_thumbnail: ""
uuid: 29325b58-6d91-43a4-95f5-6240bff5c36d
updated: 1484310369
title: listen
categories:
    - Dictionary
---
listen
     v 1: hear with intention; "Listen to the sound of this cello"
     2: listen and pay attention; "Listen to your father"; "We must
        hear the expert before we make a decision" [syn: {hear}, {take
        heed}]
     3: pay close attention to; give heed to; "Heed the advice of
        the old men" [syn: {heed}, {mind}]
