---
version: 1
type: video
provider: YouTube
id: zGpbSGj_vfE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Polar%20coordinates%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d046682-d099-4f3f-a4cf-4764961afbd9
updated: 1486069696
title: Polar coordinates 2
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/zGpbSGj_vfE/default.jpg
    - https://i3.ytimg.com/vi/zGpbSGj_vfE/1.jpg
    - https://i3.ytimg.com/vi/zGpbSGj_vfE/2.jpg
    - https://i3.ytimg.com/vi/zGpbSGj_vfE/3.jpg
---
