---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thirteen
offline_file: ""
offline_thumbnail: ""
uuid: 4cd99684-3b07-4bc7-9d70-4ec8da6456a8
updated: 1484310395
title: thirteen
categories:
    - Dictionary
---
thirteen
     adj : being one more than twelve [syn: {13}, {xiii}]
     n : the cardinal number that is the sum of twelve and one [syn:
         {13}, {XIII}, {baker's dozen}, {long dozen}]
