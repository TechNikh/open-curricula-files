---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adenine
offline_file: ""
offline_thumbnail: ""
uuid: 14898d97-349f-4d1b-87a8-e72668db6f53
updated: 1484310295
title: adenine
categories:
    - Dictionary
---
adenine
     n : (biochemistry) purine base found in DNA and RNA; pairs with
         thymine in DNA and with uracil in RNA [syn: {A}]
