---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/median
offline_file: ""
offline_thumbnail: ""
uuid: d94b47a3-0bcb-47be-bafa-4e6d59540761
updated: 1484310156
title: median
categories:
    - Dictionary
---
median
     adj 1: relating to or constituting the middle value of an ordered
            set of values (or the average of the middle two in an
            even-numbered set); "the median value of 17, 20, and
            36 is 20"; "the median income for the year was
            $15,000" [syn: {median(a)}, {average}]
     2: dividing an animal into right and left halves [syn: {medial}]
     3: relating to or situated in or extending toward the middle
        [syn: {medial}]
     n : the value below which 50% of the cases fall [syn: {median
         value}]
