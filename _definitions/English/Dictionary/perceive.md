---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perceive
offline_file: ""
offline_thumbnail: ""
uuid: 86529424-2a11-47e9-a972-8d596494049d
updated: 1484310355
title: perceive
categories:
    - Dictionary
---
perceive
     v 1: to become aware of through the senses; "I could perceive the
          ship coming over the horizon" [syn: {comprehend}]
     2: become conscious of; "She finally perceived the futility of
        her protest"
