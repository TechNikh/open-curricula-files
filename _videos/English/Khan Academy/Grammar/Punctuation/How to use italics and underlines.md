---
version: 1
type: video
provider: YouTube
id: aqef1Wx4D6w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/How%20to%20use%20italics%20and%20underlines.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cd029cd3-4817-4ba3-970f-b5022f2a35b6
updated: 1486069601
title: How to use italics and underlines
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/aqef1Wx4D6w/default.jpg
    - https://i3.ytimg.com/vi/aqef1Wx4D6w/1.jpg
    - https://i3.ytimg.com/vi/aqef1Wx4D6w/2.jpg
    - https://i3.ytimg.com/vi/aqef1Wx4D6w/3.jpg
---
Learn how to use italics and underlines when writing the titles of works or when emphasizing a particular word or phrase.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/miscellaneous-punctuation/e/italics--underlines--and-quotes/

Watch the next lesson: https://www.khanacademy.org/miscellaneous-punctuation/v/quotation-marks/

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
