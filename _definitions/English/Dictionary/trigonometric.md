---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trigonometric
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484357221
title: trigonometric
categories:
    - Dictionary
---
trigonometric
     adj : of or relating to or according to the principles of
           trigonometry; "trigonometric function"
