---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-violent
offline_file: ""
offline_thumbnail: ""
uuid: a859cd9a-8004-464e-95cb-2334134cf47c
updated: 1484310543
title: non-violent
categories:
    - Dictionary
---
nonviolent
     adj 1: abstaining (on principle) from the use of violence [ant: {violent}]
     2: achieved without bloodshed; "an unbloody transfer of power"
        [syn: {unbloody}]
