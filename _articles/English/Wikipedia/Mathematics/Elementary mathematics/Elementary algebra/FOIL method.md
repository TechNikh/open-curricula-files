---
version: 1
type: article
id: https://en.wikipedia.org/wiki/FOIL_method
offline_file: ""
offline_thumbnail: ""
uuid: f0cb85e7-aab4-4da6-ba4f-789db8706ad5
updated: 1484308909
title: FOIL method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-MonkeyFaceFOILRule.JPG
tags:
    - History
    - Examples
    - The distributive law
    - Reverse FOIL
    - Table as an alternative to FOIL
    - Generalizations
categories:
    - Elementary algebra
---
In elementary algebra, FOIL is a mnemonic for the standard method of multiplying two binomials[1]—hence the method may be referred to as the FOIL method. The word FOIL is an acronym for the four terms of the product:
