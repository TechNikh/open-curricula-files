---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balance
offline_file: ""
offline_thumbnail: ""
uuid: f05b6840-b4b4-4ec6-a9b4-ecb0078f7fca
updated: 1484310275
title: balance
categories:
    - Dictionary
---
balance
     n 1: a state of equilibrium [ant: {imbalance}]
     2: a scale for weighing; depends on pull of gravity
     3: equality between the totals of the credit and debit sides of
        an account
     4: harmonious arrangement or relation of parts or elements
        within a whole (as in a design); "in all perfectly
        beautiful objects there is found the opposition of one
        part to another and a reciprocal balance"- John Ruskin
        [syn: {proportion}]
     5: equality of distribution [syn: {equilibrium}, {equipoise}, {counterbalance}]
     6: something left after other parts have been taken away;
        "there was no remainder"; "he threw away the rest"; "he
      ...
