---
version: 1
type: video
provider: YouTube
id: ahdKdxsTj8E
offline_file: ""
offline_thumbnail: ""
uuid: 981fdfbd-e67c-4fc0-851c-61ef23bff17b
updated: 1484040398
title: 'Example 2- Adding polynomials'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/ahdKdxsTj8E/default.jpg
    - https://i3.ytimg.com/vi/ahdKdxsTj8E/1.jpg
    - https://i3.ytimg.com/vi/ahdKdxsTj8E/2.jpg
    - https://i3.ytimg.com/vi/ahdKdxsTj8E/3.jpg
---
