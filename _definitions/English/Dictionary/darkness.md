---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/darkness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535361
title: darkness
categories:
    - Dictionary
---
darkness
     n 1: absence of light or illumination [syn: {dark}] [ant: {light}]
     2: an unilluminated area; "he moved off into the darkness"
        [syn: {dark}, {shadow}]
     3: absence of moral or spiritual values; "the powers of
        darkness" [syn: {iniquity}, {wickedness}, {dark}]
     4: an unenlightened state; "he was in the dark concerning their
        intentions"; "his lectures dispelled the darkness" [syn: {dark}]
     5: having a dark or somber color [ant: {lightness}]
     6: a swarthy complexion [syn: {duskiness}, {swarthiness}]
