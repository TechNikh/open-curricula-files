---
version: 1
type: video
provider: YouTube
id: uwMYpTYsNZM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Helper%20T%20Cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 515fa4c3-bec5-4de9-983a-307137a71694
updated: 1486069579
title: Helper T Cells
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/uwMYpTYsNZM/default.jpg
    - https://i3.ytimg.com/vi/uwMYpTYsNZM/1.jpg
    - https://i3.ytimg.com/vi/uwMYpTYsNZM/2.jpg
    - https://i3.ytimg.com/vi/uwMYpTYsNZM/3.jpg
---
Introduction to helper T cells and their role in activating B cells
More free lessons at: http://www.khanacademy.org/video?v=uwMYpTYsNZM
