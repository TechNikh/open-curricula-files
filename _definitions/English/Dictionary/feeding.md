---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feeding
offline_file: ""
offline_thumbnail: ""
uuid: 14b93e25-b4a8-4a34-b7e0-0125e7a6510e
updated: 1484310271
title: feeding
categories:
    - Dictionary
---
feeding
     n 1: the act of consuming food [syn: {eating}]
     2: the act of supplying food and nourishment [syn: {alimentation}]
