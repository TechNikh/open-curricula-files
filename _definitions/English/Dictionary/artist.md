---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/artist
offline_file: ""
offline_thumbnail: ""
uuid: 54aa1f52-2cc4-4fb3-9529-bdc6091fdb6f
updated: 1484310551
title: artist
categories:
    - Dictionary
---
artist
     n : a person whose creative work shows sensitivity and
         imagination [syn: {creative person}]
