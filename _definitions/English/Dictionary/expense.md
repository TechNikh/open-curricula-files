---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expense
offline_file: ""
offline_thumbnail: ""
uuid: 111fbba7-dd6b-4d10-b2d7-2b70d1494ade
updated: 1484310462
title: expense
categories:
    - Dictionary
---
expense
     n 1: amounts paid for goods and services that may be currently
          tax deductible (as opposed to capital expenditures)
          [syn: {disbursal}, {disbursement}]
     2: a detriment or sacrifice; "at the expense of"
     3: money spent to perform work and usually reimbursed by an
        employer; "he kept a careful record of his expenses at the
        meeting"
