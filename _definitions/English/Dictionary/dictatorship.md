---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dictatorship
offline_file: ""
offline_thumbnail: ""
uuid: 554a91d4-ae1d-4acb-be61-1613f3a8af0c
updated: 1484310557
title: dictatorship
categories:
    - Dictionary
---
dictatorship
     n : a form of government in which the ruler is an absolute
         dictator (not restricted by a constitution or laws or
         opposition etc.) [syn: {absolutism}, {authoritarianism},
         {Caesarism}, {despotism}, {monocracy}, {one-man rule}, {shogunate},
          {Stalinism}, {totalitarianism}, {tyranny}]
