---
version: 1
type: annotation
id: 'http://build.opencurricula.technikh.com/sites/default/files/chapter_3%20Transportation%20-%20The%20circulatory%20system.pdf'
uuid: 91f612aa-16c9-408e-8a72-53e2702f8f47
updated: "1480663681"
annotations:
    - 
        quote: living organisms
        id: https://en.wikipedia.org/wiki/Living_organisms
        uuid: 14a4e04a-e711-4042-aff6-e1aac3e2a572
        text: Organism
        comment: >
            In biology, an organism is any contiguous living system,
            such as an animal, plant, fungus, archaeon, or bacterium.
            All known types of organisms are capable of some degree of
            response to stimuli, reproduction, growth and development
            and homeostasis. An organism consists of one or more cells;
            when it has one cell it is known as a unicellular organism;
            and when it has more than one it is known as a multicellular
            organism. Most unicellular organisms are of microscopic size
            and are thus classified as microorganisms. Humans are
            multicellular organisms composed of many trillions of cells
            grouped into specialized tissues and organs.
    - 
        quote: higher animals
        id: https://en.wikipedia.org/wiki/Higher_animals
        uuid: 8562903f-1c2f-4ea0-9e93-3e8f7dab0bcc
        text: Evolution of biological complexity
        comment: "The evolution of biological complexity is one important outcome of the process of evolution. Evolution has produced some remarkably complex organisms - although the actual level of complexity is very hard to define or measure accurately in biology, with properties such as gene content, the number of cell types or morphology all being used to assess an organism's complexity.[1][2]"
    - 
        quote: bulk movement
        id: https://en.wikipedia.org/wiki/Bulk_movement
        uuid: 6c30baf6-1521-4803-ac91-42aa3274d50e
        text: Bulk movement
        comment: >
            In cell biology, bulk flow is the process by which proteins
            with a sorting signal travel to and from different cellular
            compartments. Proteins often have sorting signals, either
            transport signals or retention signals, specifying if a
            protein will translocate to another compartment within the
            cell or is retained in the current, membrane-bound,
            compartment in which it is already located respectively. For
            instance, proteins with the KDEL sorting signal are
            specified to return to the endoplasmic reticulum from the
            Golgi (see vesicular transport). However, proteins lacking a
            sorting signal will increase in concentration in a specific
            compartment until it reaches bulk concentration in the donor
            ...
    - 
        quote: heart beat
        id: https://en.wikipedia.org/wiki/Heart_beat
        uuid: 543b0fc3-962f-4610-af5d-d2027248f971
        text: Heartbeat
        comment: null
    - 
        quote: middle fingers
        id: https://en.wikipedia.org/wiki/Middle_fingers
        uuid: f69e0c54-ff24-4f96-b077-1660b0e0dd35
        text: Middle finger
        comment: >
            The middle finger, long finger, or tall finger is the third
            digit of the human hand, located between the index finger
            and the ring finger. It is usually the longest finger. In
            anatomy, it is also called the third finger, digitus medius,
            digitus tertius, or digitus III.
    - 
        quote: senior citizens
        id: https://en.wikipedia.org/wiki/Senior_citizens
        uuid: 8decbae4-5433-4481-8bab-808e7df3332d
        text: Old age
        comment: 'Old age refers to ages nearing or surpassing the life expectancy of human beings, and is thus the end of the human life cycle. In October 2016, scientists identified the maximum human lifespan at an average age of 115, with an absolute upper limit of 125 years.[1][2] Terms and euphemisms for old people include, old people (worldwide usage), seniors (American usage), senior citizens (British and American usage), older adults (in the social sciences[3]), the elderly, and elders (in many cultures—including the cultures of aboriginal people).'
    - 
        quote: vital organ
        id: https://en.wikipedia.org/wiki/Vital_organ
        uuid: 040f56de-dc6b-4527-a61d-13b219a989d7
        text: Organ (anatomy)
        comment: 'In biology, an organ or viscus is a collection of tissues joined in a structural unit to serve a common function.[1] In anatomy, a viscus (/ˈvɪskəs/) is an internal organ, and viscera (/ˈvɪsərə/) is the plural form.[2][3]'
    - 
        quote: rib cage
        id: https://en.wikipedia.org/wiki/Rib_cage
        uuid: c98dac60-40c1-430e-b8da-4f8924378925
        text: Rib cage
        comment: >
            The rib cage is an arrangement of bones in the thorax of all
            vertebrates except the lamprey and the frog. It is formed by
            the vertebral column, ribs, and sternum and encloses the
            heart and lungs. In humans, the rib cage, also known as the
            thoracic cage, is a bony and cartilaginous structure which
            surrounds the thoracic cavity and supports the pectoral
            girdle (shoulder girdle), forming a core portion of the
            human skeleton. A typical human rib cage consists of 24
            ribs, the sternum (with xiphoid process), costal cartilages,
            and the 12 thoracic vertebrae. Together with the skin and
            associated fascia and muscles, the rib cage makes up the
            thoracic wall and provides attachments for the muscles of
            ...
    - 
        quote: soda straws
        id: https://en.wikipedia.org/wiki/Soda_straws
        uuid: 93959b4d-b88c-4add-aa29-76ffd42e03f7
        text: Soda straw
        comment: >
            A soda straw (or simply straw) is a speleothem in the form
            of a hollow mineral cylindrical tube. They are also known as
            tubular stalactites. Soda straws grow in places where water
            leaches slowly through cracks in rock, such as on the roofs
            of caves. A soda straw can turn into a stalactite if the
            hole at the bottom is blocked, or if the water begins
            flowing on the outside surface of the hollow tube.
    - 
        quote: vena cava
        id: https://en.wikipedia.org/wiki/Vena_cava
        uuid: ad29dbed-c661-42ea-add3-30abe439e30a
        text: Venae cavae
        comment: 'The venae cavae (from the Latin for "hollow veins", singular "vena cava") are large veins (venous trunks) that return deoxygenated blood from the body into the heart. In humans they are called the superior vena cava and the inferior vena cava, and both empty into the right atrium.[1] They are located slightly off-center, toward the right side of the body.'
    - 
        quote: pear shaped
        id: https://en.wikipedia.org/wiki/Pear_shaped
        uuid: ca7a62a5-7465-49c8-9170-ced205b7333d
        text: Pear-shaped
        comment: ""
    - 
        quote: naked eye
        id: https://en.wikipedia.org/wiki/Naked_eye
        uuid: 709a25ba-fce8-4170-b1d4-3654efd4706d
        text: Naked eye
        comment: >
            Naked eye, also called bare eye or unaided eye, is the
            practice of engaging in visual perception unaided by a
            magnifying or light-collecting optical device, such as a
            telescope or microscope. Vision corrected to normal acuity
            using corrective lenses is considered "naked". In astronomy,
            the naked eye may be used to observe events that can be
            viewed without equipment, such as an astronomical
            conjunction, the passage of a comet, or a meteor shower. Sky
            lore and various tests demonstrate an impressive wealth of
            phenomena that can be seen with the unaided eye.
    - 
        quote: cardiac cycle
        id: https://en.wikipedia.org/wiki/Cardiac_cycle
        uuid: c42df978-6204-4f2f-9faf-e07cbe447f53
        text: Cardiac cycle
        comment: 'The cardiac cycle refers to a complete heartbeat from its generation to the beginning of the next beat, and so includes the diastole, the systole, and the intervening pause. The frequency of the cardiac cycle is described by the heart rate, which is typically expressed as beats per minute. Each beat of the heart involves five major stages. The first two stages, often considered together as the "ventricular filling" stage, involve the movement of blood from the atria into the ventricles. The next three stages involve the movement of blood from the ventricles to the pulmonary artery (in the case of the right ventricle) and the aorta (in the case of the left ventricle).[1]'
    - 
        quote: embryonic development
        id: https://en.wikipedia.org/wiki/Embryonic_development
        uuid: fb4688cd-fb59-4c61-abe5-00819c34d1c5
        text: Embryogenesis
        comment: >
            Embryogenesis is the process by which the embryo forms and
            develops. In mammals, the term refers chiefly to early
            stages of prenatal development, whereas the terms fetus and
            fetal development describe later stages.
    - 
        quote: foot wear
        id: https://en.wikipedia.org/wiki/Foot_wear
        uuid: 4766d8b7-b2e7-4ac4-9b78-797808158e58
        text: Footwear
        comment: >
            Footwear refers to garments worn on the feet, which
            originally serves to purpose of protection against
            adversities of the environment, usually regarding ground
            textures and temperature. Footwear in the manner of shoes
            therefore primarily serves the purpose to ease the
            locomotion and prevent injuries. Secondly footwear can also
            be used for fashion and adornment as well as to indicate the
            status or rank of the person within a social structure.
            Socks and other hosiery are typically worn additionally
            between the feet and other footwear for further comfort and
            relief.
    - 
        quote: limiting membrane
        id: https://en.wikipedia.org/wiki/Limiting_membrane
        uuid: 0f86aa1b-b0e8-4547-9780-750b2b773196
        text: Limiting membrane
        comment: null
    - 
        quote: jelly fish
        id: https://en.wikipedia.org/wiki/Jelly_fish
        uuid: 8d679cf5-c5ac-407b-9bf6-6a4d8fafddd3
        text: Jellyfish
        comment: 'Jellyfish, or jellies,[1] are softbodied free-swimming aquatic animals with a gelatinous umbrella-shaped bell and trailing tentacles. The bell can pulsate to acquire locomotion, while the stinging tentacles can be utilized to capture prey by emitting toxins. Jellyfish species are classified in the subphylum Medusozoa which makes up a major part of the phylum Cnidaria, although not all Medusozoa species are considered to be jellyfish.'
    - 
        quote: young adult
        id: https://en.wikipedia.org/wiki/Young_adult
        uuid: 0b787ac1-8ea1-4d9d-adfa-1fce2c9a2f3c
        text: Young adult
        comment: null
    - 
        quote: genetic disorder
        id: https://en.wikipedia.org/wiki/Genetic_disorder
        uuid: a4ca02f5-80a2-4e1d-9d38-bf95c6a7669f
        text: Genetic disorder
        comment: >
            A genetic disorder is a genetic problem caused by one or
            more abnormalities in the genome, especially a condition
            that is present from birth (congenital). Most genetic
            disorders are quite rare and affect one person in every
            several thousands or millions.
    - 
        quote: vascular bundles
        id: https://en.wikipedia.org/wiki/Vascular_bundles
        uuid: bb799888-a55c-44cd-88bb-e13927f014b4
        text: Vascular bundle
        comment: 'A vascular bundle is a part of the transport system in vascular plants. The transport itself happens in vascular tissue, which exists in two forms: xylem and phloem. Both these tissues are present in a vascular bundle, which in addition will include supporting and protective tissues.'
    - 
        quote: mustard seeds
        id: https://en.wikipedia.org/wiki/Mustard_seeds
        uuid: 49ef9180-3777-4bbf-864f-7924d38e76bd
        text: Mustard seed
        comment: 'Mustard seeds are the small round seeds of various mustard plants. The seeds are usually about 1 to 2 millimetres (0.039 to 0.079 in) in diameter and may be colored from yellowish white to black. They are important spice in many regional foods and may come from one of three different plants: black mustard (Brassica nigra), brown Indian mustard (B. juncea), or white mustard (B. hirta/Sinapis alba).'
    - 
        quote: cover slip
        id: https://en.wikipedia.org/wiki/Cover_slip
        uuid: bc7bec8b-f419-4ef2-bf6f-5a4436e05ede
        text: Cover slip
        comment: "A cover slip, coverslip or cover glass is a thin flat piece of transparent material, usually square or rectangular, about 20 mm (4/5 in) wide and a fraction of a millimetre thick, that is placed over objects for viewing with a microscope. The object is usually held between the cover slip and a somewhat thicker microscope slide, which rests on the microscope's stage or slide holder and provides the physical support for the object and slip."
    - 
        quote: ground level
        id: https://en.wikipedia.org/wiki/Ground_level
        uuid: fbe6a1f5-c9c6-4d98-8bc9-7aed923450ba
        text: Ground level
        comment: null
    - 
        quote: rain fall
        id: https://en.wikipedia.org/wiki/Rain_fall
        uuid: 2357c1e9-8294-4fde-881f-6835001845ea
        text: Rain Fall
        comment: 'Rain Fall (レイン・フォール 雨の牙, Rein Fōru: Ame no Kiba?) is a 2009 Japanese/Australian action thriller film written and directed by Max Mannix. In the film a half-Japanese half-American hitman protects the daughter of one of his victims against the CIA. It was based on the novel Rain Fall by Barry Eisler.[1]'
    - 
        quote: growing season
        id: https://en.wikipedia.org/wiki/Growing_season
        uuid: a1f1c492-4454-481b-91f8-d590aef8c6d9
        text: Growing season
        comment: >
            The growing season is the part of the year during which
            local weather conditions (i.e. rainfall and temperature)
            permit normal plant growth. While each plant or crop has a
            specific growing season that depends on its genetic
            adaptation, growing seasons can generally be grouped into
            macro-environmental classes.
    - 
        quote: big mango
        id: https://en.wikipedia.org/wiki/Big_mango
        uuid: e8e01dc3-73af-47d7-9482-dcb9b92e803b
        text: "Australia's big things"
        comment: >
            The big things of Australia are a loosely related set of
            large structures, some of which are novelty architecture and
            some are sculptures. There are estimated to be over 150 such
            objects around the country, the first being the Big Scotsman
            in Medindie, Adelaide, which was built in 1963. There are
            big things in every state and territory in Australia.
        image:
            id: >
                http://build.opencurricula.technikh.com/sites/default/files/articles/thumbnails/800px-Flag_of_Australia.svg.png
            uuid: 25a9ea8e-3b1d-4115-a8c3-1603d8e9774c
            comment: ""
    - 
        quote: electrically charged
        id: https://en.wikipedia.org/wiki/Electrically_charged
        uuid: d3778630-5eef-4736-b085-66df4d7b9b31
        text: Electric charge
        comment: 'Electric charge is the physical property of matter that causes it to experience a force when placed in an electromagnetic field. There are two types of electric charges: positive and negative. Like charges repel and unlike attract. An object is negatively charged if it has an excess of electrons, and is otherwise positively charged or uncharged. The SI derived unit of electric charge is the coulomb (C). In electrical engineering, it is also common to use the ampere-hour (Ah), and, in chemistry, it is common to use the elementary charge (e) as a unit. The symbol Q often denotes charge. Early knowledge of how charged substances interact is now called classical electrodynamics, and is still ...'
    - 
        quote: amino acids
        id: https://en.wikipedia.org/wiki/Amino_acids
        uuid: 24260552-aed0-4757-958e-936d87da0e7f
        text: Amino acid
        comment: "Amino acids are biologically important organic compounds containing amine (-NH2) and carboxyl (-COOH) functional groups, along with a side-chain (R group) specific to each amino acid.[1][2][3] The key elements of an amino acid are carbon, hydrogen, oxygen, and nitrogen, though other elements are found in the side-chains of certain amino acids. About 500 amino acids are known (though only 20 appear in the genetic code) and can be classified in many ways.[4] They can be classified according to the core structural functional groups' locations as alpha- (α-), beta- (β-), gamma- (γ-) or delta- (δ-) amino acids; other categories relate to polarity, pH level, and side-chain group type ..."
    - 
        quote: wire netting
        id: https://en.wikipedia.org/wiki/Wire_netting
        uuid: ea09f1f3-b750-494c-b0f2-021416397f14
        text: Chain-link fencing
        comment: >
            A chain-link fence (also referred to as wire netting,
            wire-mesh fence, chain-wire fence, cyclone fence, hurricane
            fence, or diamond-mesh fence) is a type of woven fence
            usually made from galvanized or LLDPE-coated steel wire. The
            wires run vertically and are bent into a zig-zag pattern so
            that each "zig" hooks with the wire immediately on one side
            and each "zag" with the wire immediately on the other. This
            forms the characteristic diamond pattern seen in this type
            of fence.
    - 
        quote: interatrial septum
        id: https://en.wikipedia.org/wiki/Interatrial_septum
        uuid: 10975226-4158-4ef2-ab38-dc7f8371725a
        text: Interatrial septum
        comment: ""
    - 
        quote: school teachers
        id: https://en.wikipedia.org/wiki/School_teachers
        uuid: 2cb96503-18af-437d-920c-e11e6a1b9701
        text: Teacher
        comment: ""
    - 
        quote: enlarged liver
        id: https://en.wikipedia.org/wiki/Enlarged_liver
        uuid: c7fcde97-4ddb-4a3f-8c75-358bdeb5c2ca
        text: Hepatomegaly
        comment: 'Hepatomegaly is the condition of having an enlarged liver.[1] It is a non-specific medical sign having many causes, which can broadly be broken down into infection, hepatic tumours, or metabolic disorder. Often, hepatomegaly will present as an abdominal mass. Depending on the cause, it may sometimes present along with jaundice.[2]'
    - 
        quote: brittle bones
        id: https://en.wikipedia.org/wiki/Brittle_bones
        uuid: b62ef37e-29eb-402a-94d9-e6a9562de8a6
        text: Osteogenesis imperfecta
        comment: 'Osteogenesis imperfecta (OI) is a group of genetic disorders that mainly affect the bones. It results in bones that break easily. The severity may be mild to severe.[1] Other symptoms may include a blue tinge to the whites of the eye, short height, loose joints, hearing loss, breathing problems, and problems with the teeth.[1][3]'
    - 
        quote: antenatal diagnosis
        id: https://en.wikipedia.org/wiki/Antenatal_diagnosis
        uuid: 4684ab7d-6c34-4ca4-9c3c-263e7a114916
        text: Prenatal diagnosis
        comment: >
            Prenatal diagnosis and prenatal screening are aspects of
            prenatal care that focus on detecting anatomic and
            physiologic problems with the zygote, embryo, or fetus as
            early as possible, either before gestation even starts (as
            in preimplantation genetic diagnosis) or as early in
            gestation as practicable. They use medical tests to detect
            problems such as neural tube defects, chromosome
            abnormalities, and gene mutations that would lead to genetic
            disorders and birth defects, such as spina bifida, cleft
            palate, Tay–Sachs disease, sickle cell anemia,
            thalassemia, cystic fibrosis, muscular dystrophy, and
            fragile X syndrome. The screening focuses on finding
            problems among a large population with ...
    - 
        quote: bone marrow
        id: https://en.wikipedia.org/wiki/Bone_marrow
        uuid: 972ec638-b60e-473f-b853-4d7ca8b5f8d7
        text: Bone marrow
        comment: "Bone marrow is the flexible tissue in the interior of bones. In humans, red blood cells are produced by cores of bone marrow in the heads of long bones in a process known as hematopoiesis.[2] On average, bone marrow constitutes 4% of the total body mass of humans; in an adult having 65 kilograms of mass (143 lbs), bone marrow typically accounts for approximately 2.6 kilograms (5.7 lb). The hematopoietic component of bone marrow produces approximately 500 billion blood cells per day, which use the bone marrow vasculature as a conduit to the body's systemic circulation.[3] Bone marrow is also a key component of the lymphatic system, producing the lymphocytes that support the body's immune ..."
    - 
        quote: marrow transplant
        id: https://en.wikipedia.org/wiki/Marrow_transplant
        uuid: c1080bdd-ce22-4006-a850-306319449084
        text: Hematopoietic stem cell transplantation
        comment: "Hematopoietic stem cell transplantation (HSCT) is the transplantation of multipotent hematopoietic stem cells, usually derived from bone marrow, peripheral blood, or umbilical cord blood.[1][2] It may be autologous (the patient's own stem cells are used), allogeneic (the stem cells come from a donor) or syngeneic (from an identical twin).[1][2] It is a medical procedure in the field of hematology, most often performed for patients with certain cancers of the blood or bone marrow, such as multiple myeloma or leukemia.[2] In these cases, the recipient's immune system is usually destroyed with radiation or chemotherapy before the transplantation. Infection and graft-versus-host disease are ..."
---
