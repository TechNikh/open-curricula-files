---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slap
offline_file: ""
offline_thumbnail: ""
uuid: 4a8f7718-87b9-4c29-93f2-0603794f4970
updated: 1484310168
title: slap
categories:
    - Dictionary
---
slap
     n 1: a blow from a flat object (as an open hand) [syn: {smack}]
     2: the act of smacking something; a blow delivered with an open
        hand [syn: {smack}, {smacking}]
     adv : directly; "he ran bang into the pole"; "ran slap into her"
           [syn: {bang}, {slapdash}, {smack}, {bolt}]
     v : hit with something flat, like a paddle or the open hand;
         "The impatient teacher slapped the student"; "a gunshot
         slapped him on the forehead"
     [also: {slapping}, {slapped}]
