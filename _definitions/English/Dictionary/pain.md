---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pain
offline_file: ""
offline_thumbnail: ""
uuid: 95f8a12f-8c47-4a16-9f90-8a6e177bc29d
updated: 1484310384
title: pain
categories:
    - Dictionary
---
pain
     n 1: a symptom of some physical hurt or disorder; "the patient
          developed severe pain and distension" [syn: {hurting}]
     2: emotional distress; a fundamental feeling that people try to
        avoid; "the pain of loneliness" [syn: {painfulness}] [ant:
         {pleasure}]
     3: a somatic sensation of acute discomfort; "as the intensity
        increased the sensation changed from tickle to pain" [syn:
         {painful sensation}]
     4: a bothersome annoying person; "that kid is a terrible pain"
        [syn: {pain in the neck}, {nuisance}]
     5: something or someone that causes trouble; a source of
        unhappiness; "washing dishes was a nuisance before we ...
