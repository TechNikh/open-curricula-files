---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supplying
offline_file: ""
offline_thumbnail: ""
uuid: ae588434-ca3c-4a9d-9ffb-0425a4c39d83
updated: 1484310226
title: supplying
categories:
    - Dictionary
---
supplying
     n : the activity of supplying or providing something [syn: {provision},
          {supply}]
