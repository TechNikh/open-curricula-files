---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Checkmate
offline_file: ""
offline_thumbnail: ""
uuid: 1312796c-595b-4a2d-9d42-e0447b1a54a4
updated: 1484309017
title: Checkmate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-CheckmateProper.jpg
tags:
    - Examples
    - Etymology
    - History
    - Two major pieces
    - Basic checkmates
    - King and queen
    - Avoid stalemate
    - King and rook
    - Avoid stalemate
    - King and two bishops
    - Avoid stalemate
    - King, bishop and knight
    - Avoid stalemate
    - Common checkmates
    - Back-rank mate
    - "Scholar's mate"
    - "Fool's mate"
    - Smothered mate
    - Rare checkmates
    - "Stamma's mate"
    - Unusual mates
    - Two and three knights
    - Bibliography
categories:
    - Chess theory
---
Checkmate (often shortened to mate) is a game position in chess in which a player's king is in check (threatened with capture) and there is no way to remove the threat. Checkmating the opponent wins the game.
