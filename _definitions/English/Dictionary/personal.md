---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/personal
offline_file: ""
offline_thumbnail: ""
uuid: 400d5aa8-b53b-4034-93fe-3db0ed47b0da
updated: 1484310451
title: personal
categories:
    - Dictionary
---
personal
     adj 1: concerning or affecting a particular person or his or her
            private life and personality; "a personal favor"; "for
            your personal use"; "personal papers"; "I have
            something personal to tell you"; "a personal God"; "he
            has his personal bank account and she has hers" [ant:
            {impersonal}]
     2: particular to a given individual
     3: of or arising from personality; "personal magnetism"
     4: intimately concerning a person's body or physical being;
        "personal hygiene"
     5: indicating grammatical person; "personal verb endings"
     n : a short newspaper article about a particular person or group
