---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homogenisation
offline_file: ""
offline_thumbnail: ""
uuid: cc0e9aea-a43f-49d9-a6ad-94ae0aae8c60
updated: 1484310529
title: homogenisation
categories:
    - Dictionary
---
homogenisation
     n : the act of making something homogeneous or uniform in
         composition; "the homogenization of cream"; "the
         network's homogenization of political news" [syn: {homogenization}]
