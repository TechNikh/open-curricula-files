---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advisory
offline_file: ""
offline_thumbnail: ""
uuid: 776ad6fe-f89c-4fe9-8786-3408200cf1f5
updated: 1484310431
title: advisory
categories:
    - Dictionary
---
advisory
     adj : giving advice; "an advisory memorandum", "his function was
           purely consultative" [syn: {consultative}, {consultatory},
            {consultive}]
