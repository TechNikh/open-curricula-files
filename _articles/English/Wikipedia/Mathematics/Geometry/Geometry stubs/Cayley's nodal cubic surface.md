---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cayley%27s_nodal_cubic_surface'
offline_file: ""
offline_thumbnail: ""
uuid: 4f0be22e-dcca-4dcf-a6c6-06f468959a88
updated: 1484309356
title: "Cayley's nodal cubic surface"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-CayleyCubic.png
categories:
    - Geometry stubs
---
In algebraic geometry, the Cayley surface, named after Arthur Cayley, is a cubic nodal surface in 3-dimensional projective space with four conical points. It can be given by the equation
