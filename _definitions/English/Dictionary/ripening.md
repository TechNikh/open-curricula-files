---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ripening
offline_file: ""
offline_thumbnail: ""
uuid: 2db0cc47-9a00-43f0-8214-24904b76db01
updated: 1484310429
title: ripening
categories:
    - Dictionary
---
ripening
     n 1: coming to full development; becoming mature [syn: {maturation},
           {maturement}]
     2: acquiring desirable qualities by being left undisturbed for
        some time [syn: {aging}, {ageing}]
