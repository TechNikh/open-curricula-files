---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accompaniment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484641921
title: accompaniment
categories:
    - Dictionary
---
accompaniment
     n 1: an event or situation that happens at the same time as or in
          connection with another [syn: {concomitant}, {co-occurrence}]
     2: a subordinate musical part; provides background for more
        important parts [syn: {musical accompaniment}, {backup}, {support}]
     3: the act of accompanying someone or something in order to
        protect them [syn: {escort}]
