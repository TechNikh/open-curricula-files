---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Traffic_wave
offline_file: ""
offline_thumbnail: ""
uuid: efb3ab8e-c7d0-4c15-9628-645c13df0897
updated: 1484308898
title: Traffic wave
tags:
    - mitigation
    - History
categories:
    - Waves
---
Traffic waves, also called stop waves or traffic shocks, are traveling disturbances in the distribution of cars on a highway. Traffic waves travel backwards relative to the cars themselves. [1] Relative to a fixed spot on the road the wave can move with, or against the traffic, or even be stationary (when the waves moves away from the traffic with exactly the same speed as the traffic). Traffic waves are a type of traffic jam. A deeper understanding of traffic waves is a goal of the physical study of traffic flow, in which traffic itself can often be seen using techniques similar to those used in fluid dynamics.
