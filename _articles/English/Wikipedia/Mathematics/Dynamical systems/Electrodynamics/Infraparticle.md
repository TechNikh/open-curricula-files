---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infraparticle
offline_file: ""
offline_thumbnail: ""
uuid: cc8ebf33-441f-46a2-b285-6fcd48096263
updated: 1484308759
title: Infraparticle
categories:
    - Electrodynamics
---
An infraparticle is an electrically charged particle and its surrounding cloud of soft photons—of which there are infinite number, by virtue of the infrared divergence of quantum electrodynamics.[1] That is, it is a dressed particle rather than a bare particle. Whenever electric charges accelerate they emit Bremsstrahlung radiation, whereby an infinite number of the virtual soft photons become real particles. However, only a finite number of these photons are detectable, the remainder falling below the measurement threshold.[2]
