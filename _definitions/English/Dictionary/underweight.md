---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underweight
offline_file: ""
offline_thumbnail: ""
uuid: a5bac800-6955-4ddd-a032-b5afb51c06b4
updated: 1484310535
title: underweight
categories:
    - Dictionary
---
underweight
     adj : having unattractive thinness; "a child with skinny freckled
           legs"; "a long scrawny neck" [syn: {scraggy}, {scrawny},
            {skinny}, {weedy}]
