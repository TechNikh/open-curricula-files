---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faced
offline_file: ""
offline_thumbnail: ""
uuid: 5dac837d-0124-4c1a-ade0-c67f0ef1e765
updated: 1484310246
title: faced
categories:
    - Dictionary
---
faced
     adj : having a face or facing especially of a specified kind or
           number; often used in combination; "a neatly faced
           terrace" [ant: {faceless}]
