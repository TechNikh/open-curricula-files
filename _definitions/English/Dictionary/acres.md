---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acres
offline_file: ""
offline_thumbnail: ""
uuid: 5749e7df-a183-41d1-a142-f3f1c3d86a67
updated: 1484310264
title: acres
categories:
    - Dictionary
---
acres
     n : extensive landed property (especially in the country)
         retained by the owner for his own use; "the family owned
         a large estate on Long Island" [syn: {estate}, {land}, {landed
         estate}, {demesne}]
