---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specialisation
offline_file: ""
offline_thumbnail: ""
uuid: a06e02bd-fcdc-4f7a-99b2-277ebbe464af
updated: 1484310475
title: specialisation
categories:
    - Dictionary
---
specialisation
     n 1: (biology) the structural adaptation of some body part for a
          particular function; "cell differentiation in the
          developing embryo" [syn: {specialization}, {differentiation}]
     2: the act of specializing; making something suitable for a
        special purpose [syn: {specialization}]
     3: the special line of work you have adopted as your career;
        "his specialization is gastroenterology" [syn: {specialization},
         {specialty}, {speciality}, {specialism}]
