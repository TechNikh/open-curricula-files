---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rainshadow
offline_file: ""
offline_thumbnail: ""
uuid: 78577f4d-d780-4149-818f-6ed56e2b5576
updated: 1484308438
title: Rain shadow
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/375px-Rainshadow_copy.jpg
tags:
    - Description
    - Regions of notable rain shadow
    - Asia
    - South America
    - North America and the Caribbean
    - Europe
    - Africa
    - Oceania
categories:
    - Uncategorized
---
A rain shadow is a dry area on the leeward side of a mountainous area (away from the wind). The mountains block the passage of rain-producing weather systems and cast a "shadow" of dryness behind them. Wind and moist air is drawn by the prevailing winds towards the top of the mountains, where it condenses and precipitates before it crosses the top. The air, without much moisture left, advances behind the mountains creating a drier side called the "rain shadow".
