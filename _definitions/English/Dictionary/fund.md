---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fund
offline_file: ""
offline_thumbnail: ""
uuid: 1a5ee45e-19b7-431f-b195-21b75c8d1bbb
updated: 1484310455
title: fund
categories:
    - Dictionary
---
fund
     n 1: a reserve of money set aside for some purpose [syn: {monetary
          fund}]
     2: a supply of something available for future use; "he brought
        back a large store of Cuban cigars" [syn: {store}, {stock}]
     3: a financial institution that sells shares to individuals and
        invests in securities issued by other companies [syn: {investment
        company}, {investment trust}, {investment firm}]
     v 1: convert (short-term floating debt) into long-term debt that
          bears fixed interest and is represented by bonds
     2: place or store up in a fund for accumulation
     3: provide a fund for the redemption of principal or payment of
        interest
  ...
