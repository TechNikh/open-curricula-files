---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extensionality
offline_file: ""
offline_thumbnail: ""
uuid: 2913246a-c104-40c5-b65e-c30972056ff3
updated: 1484309504
title: Extensionality
tags:
    - Example
    - In mathematics
categories:
    - Set theory
---
In logic, extensionality, or extensional equality, refers to principles that judge objects to be equal if they have the same external properties. It stands in contrast to the concept of intensionality, which is concerned with whether the internal definitions of objects are the same.
