---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/couple
offline_file: ""
offline_thumbnail: ""
uuid: 0cb65176-a12d-419b-a527-ab98e730ffa5
updated: 1484310281
title: couple
categories:
    - Dictionary
---
couple
     n 1: a small indefinite number; "he's coming for a couple of
          days"
     2: a pair of people who live together; "a married couple from
        Chicago" [syn: {mates}, {match}]
     3: a pair who associate with one another; "the engaged couple";
        "an inseparable twosome" [syn: {twosome}, {duo}, {duet}]
     4: two items of the same kind [syn: {pair}, {twosome}, {twain},
         {brace}, {span}, {yoke}, {couplet}, {distich}, {duo}, {duet},
         {dyad}, {duad}]
     5: something joined by two equal and opposite forces that act
        along parallel lines
     v 1: bring two objects, ideas, or people together; "This fact is
          coupled to the other one"; ...
