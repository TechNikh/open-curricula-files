---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triaprism
offline_file: ""
offline_thumbnail: ""
uuid: 869f20b9-012b-45b8-be5c-e85fd15f44f2
updated: 1484309287
title: Triaprism
tags:
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry of 6 dimensions or higher, a triaprism (or triprism) is a polytope resulting from the Cartesian product of three polytopes, each of two dimensions or higher. The Cartesian product of an a-polytope, a b-polytope, and a c-polytope is an (a+b+c)-polytope, where a, b and c are 2-polytopes (polygon) or higher.
