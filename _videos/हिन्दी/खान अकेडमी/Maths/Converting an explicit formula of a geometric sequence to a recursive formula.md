---
version: 1
type: video
provider: YouTube
id: Jz5nhf9Qbeg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 18251335-532a-4c56-b99e-a017563297fa
updated: 1486069617
title: >
    Converting an explicit formula of a geometric sequence to a
    recursive formula
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Jz5nhf9Qbeg/default.jpg
    - https://i3.ytimg.com/vi/Jz5nhf9Qbeg/1.jpg
    - https://i3.ytimg.com/vi/Jz5nhf9Qbeg/2.jpg
    - https://i3.ytimg.com/vi/Jz5nhf9Qbeg/3.jpg
---
