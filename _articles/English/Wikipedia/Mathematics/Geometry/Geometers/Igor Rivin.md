---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Igor_Rivin
offline_file: ""
offline_thumbnail: ""
uuid: 827af448-4fe9-4264-a751-8070bc373aaf
updated: 1484309100
title: Igor Rivin
categories:
    - Geometers
---
Igor Rivin (born 1961 in Moscow, USSR) is a Russian-Canadian mathematician, working in various fields of pure and applied mathematics, computer science, and materials science. He is the Regius Professor of Mathematics at the University of St. Andrews.
