---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hurwitz_class_number
offline_file: ""
offline_thumbnail: ""
uuid: 8e0c1409-d281-4fb1-88b4-e5052c170988
updated: 1484308645
title: Hurwitz class number
categories:
    - Number theory
---
In mathematics, the Hurwitz class number H(N), introduced by Adolf Hurwitz, is a modification of the class number of positive definite binary quadratic forms of discriminant –N, where forms are weighted by 2/g for g the order of their automorphism group, and where H(0) = –1/12.
