---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Water_retention_on_mathematical_surfaces
offline_file: ""
offline_thumbnail: ""
uuid: cfc78b61-f853-4dfb-bcc2-bcc47078029f
updated: 1484308751
title: Water retention on mathematical surfaces
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Drinking_water.jpg
tags:
    - Magic squares
    - Random surfaces
    - Algorithms
categories:
    - Critical phenomena
---
Water retention on mathematical surfaces refers to the water caught in ponds on a surface of cells of various heights on a regular array such as a square lattice, where water is rained down on every cell in the system. The boundaries of the system are open and allow water to flow out. Water will be trapped in ponds, and eventually all ponds will fill to their maximum height, with any additional water flowing over spillways and out the boundaries of the system. The problem is to find the amount of water trapped or retained for a given surface. This has been studied extensively for two mathematical surfaces: magic squares and random surfaces.
