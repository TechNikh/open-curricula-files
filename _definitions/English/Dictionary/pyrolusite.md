---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyrolusite
offline_file: ""
offline_thumbnail: ""
uuid: 8c98ebfb-1540-461d-adf6-80dcfacbb219
updated: 1484310409
title: pyrolusite
categories:
    - Dictionary
---
pyrolusite
     n : a mineral consisting of manganese dioxide; an important
         source of manganese
