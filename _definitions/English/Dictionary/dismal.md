---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dismal
offline_file: ""
offline_thumbnail: ""
uuid: c547928b-36b7-44a4-a0c4-4086f99f26d0
updated: 1484310448
title: dismal
categories:
    - Dictionary
---
dismal
     adj 1: depressing in character or appearance; "drove through dingy
            streets"; "the dismal prison twilight"- Charles
            Dickens; "drab old buildings"; "a dreary mining town";
            "gloomy tenements"; "sorry routine that follows on the
            heels of death"- B.A.Williams [syn: {dingy}, {drab}, {drear},
             {dreary}, {gloomy}, {sorry}]
     2: causing dejection; "a blue day"; "the dark days of the war";
        "a week of rainy depressing weather"; "a disconsolate
        winter landscape"; "the first dismal dispiriting days of
        November"; "a dark gloomy day"; "grim rainy weather" [syn:
         {blue}, {dark}, {depressing}, ...
