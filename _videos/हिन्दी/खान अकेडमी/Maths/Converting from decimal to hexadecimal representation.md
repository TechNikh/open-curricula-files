---
version: 1
type: video
provider: YouTube
id: lr8Qk7UPD-A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f8728a24-d015-4888-9208-1fa88455ab2b
updated: 1486069609
title: Converting from decimal to hexadecimal representation
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/lr8Qk7UPD-A/default.jpg
    - https://i3.ytimg.com/vi/lr8Qk7UPD-A/1.jpg
    - https://i3.ytimg.com/vi/lr8Qk7UPD-A/2.jpg
    - https://i3.ytimg.com/vi/lr8Qk7UPD-A/3.jpg
---
