---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acpi
offline_file: ""
offline_thumbnail: ""
uuid: 4649af10-6c7b-41c9-a58b-85762239a270
updated: 1484308327
title: Advanced Configuration and Power Interface
tags:
    - Architecture
    - ACPI Component Architecture (ACPICA)
    - History
    - Operating Systems
    - OSPM responsibilities
    - Power states
    - Global states
    - Device states
    - Processor states
    - Performance states
    - Hardware interface
    - Firmware interface
    - Security risks
categories:
    - Uncategorized
---
In computing, the Advanced Configuration and Power Interface (ACPI) specification provides an open standard that operating systems can use to perform discovery and configuration of computer hardware components, to perform power management by, for example, putting unused components to sleep, and to do status monitoring. Internally, ACPI advertises the available components and their functions to the operating system kernel using instruction lists ("methods") provided through the system firmware (UEFI or BIOS), which the kernel parses; and then executes the desired operations (such as the initialization of hardware components) using an embedded minimal virtual machine.
