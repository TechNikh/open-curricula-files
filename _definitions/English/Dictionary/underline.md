---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underline
offline_file: ""
offline_thumbnail: ""
uuid: bd05f968-a90f-400e-b0c3-ed45dabdeb8a
updated: 1484310467
title: underline
categories:
    - Dictionary
---
underline
     n : a line drawn underneath (especially under written matter)
         [syn: {underscore}]
     v 1: give extra weight to (a communication); "Her gesture
          emphasized her words" [syn: {underscore}, {emphasize}, {emphasise}]
     2: draw a line or lines underneath to call attention to [syn: {underscore}]
