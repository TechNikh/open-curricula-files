---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indicative
offline_file: ""
offline_thumbnail: ""
uuid: 4137aa86-65ae-418d-9331-36b27226652c
updated: 1484310448
title: indicative
categories:
    - Dictionary
---
indicative
     adj 1: relating to the mood of verbs that is used simple
            declarative statements; "indicative mood" [syn: {declarative}]
     2: (usually followed by `of') pointing out or revealing
        clearly;  "actions indicative of fear" [syn: {indicatory},
         {revelatory}, {significative}, {suggestive}]
     n : a mood (grammatically unmarked) that represents the act or
         state as an objective fact [syn: {indicative mood}, {declarative
         mood}, {declarative}, {common mood}, {fact mood}]
