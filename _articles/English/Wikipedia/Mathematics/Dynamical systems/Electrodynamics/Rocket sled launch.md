---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rocket_sled_launch
offline_file: ""
offline_thumbnail: ""
uuid: 21a0de3e-3c2a-4709-a362-9d7d7955df87
updated: 1484308762
title: Rocket sled launch
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Maglifter.jpg
tags:
    - Overview of the problem
    - An example
    - Benefits of high altitude launches
    - Compatibility with reusable launch vehicles
    - Rocket sled launches in fiction
categories:
    - Electrodynamics
---
A rocket sled launch, also known as "ground based launch assist", "catapult launch assist", and "sky ramp launch", is a proposed method for launching space vehicles. With this concept the launch vehicle is supported by an eastward pointing rail or maglev track that goes up the side of a mountain while an externally applied force is used to accelerate the launch vehicle to a given velocity. Using an externally applied force for the initial acceleration reduces the propellant the launch vehicle needs to carry to reach orbit. This allows the launch vehicle to carry a larger payload and reduces the cost of getting to orbit. When the amount of velocity added to the launch vehicle by the ground ...
