---
version: 1
type: video
provider: YouTube
id: _8XQzdE3Lns
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 328c96a3-4cbf-4f1c-bc41-9208ab02f49f
updated: 1486069639
title: "Experiment sobre la refracció de la llum (provoquem un miratge 'low cost')"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/_8XQzdE3Lns/default.jpg
    - https://i3.ytimg.com/vi/_8XQzdE3Lns/1.jpg
    - https://i3.ytimg.com/vi/_8XQzdE3Lns/2.jpg
    - https://i3.ytimg.com/vi/_8XQzdE3Lns/3.jpg
---
