---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fill
offline_file: ""
offline_thumbnail: ""
uuid: 11d7fffb-61c4-48ec-be3f-37d7ae688d74
updated: 1484310344
title: fill
categories:
    - Dictionary
---
fill
     n 1: a quantity sufficient to satisfy; "he ate his fill of
          potatoes"; "she had heard her fill of gossip"
     2: any material that fills a space or container; "there was not
        enough fill for the trench" [syn: {filling}]
     v 1: make full, also in a metaphorical sense; "fill a container";
          "fill the child with pride" [syn: {fill up}, {make full}]
          [ant: {empty}]
     2: become full; "The pool slowly filled with water"; "The
        theater filled up slowly" [syn: {fill up}] [ant: {empty}]
     3: occupy the whole of; "The liquid fills the container" [syn:
        {occupy}]
     4: assume, as of positions or roles; "She took the job as
        ...
