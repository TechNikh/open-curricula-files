---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digraph_realization_problem
offline_file: ""
offline_thumbnail: ""
uuid: dfcd81b7-c34f-46c1-87c9-4fca4127c8be
updated: 1484309391
title: Digraph realization problem
tags:
    - Solutions
    - Other notations
    - Related problems
categories:
    - Computational problems in graph theory
---
The digraph realization problem is a decision problem in graph theory. Given pairs of nonnegative integers 
  
    
      
        (
        (
        
          a
          
            1
          
        
        ,
        
          b
          
            1
          
        
        )
        ,
        …
        ,
        (
        
          a
          
            n
          
        
        ,
        
          b
          
            n
          
        
        )
        )
      
    
    {\displaystyle ((a_{1},b_{1}),\ldots ,(a_{n},b_{n}))}
  
, the problem asks whether there is a labeled simple directed graph such that each vertex 
  
    
      
        
          v
 ...
