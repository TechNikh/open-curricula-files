---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Propene
offline_file: ""
offline_thumbnail: ""
uuid: c076fe7c-bd70-4aa4-80f4-088aa33ab9f6
updated: 1484308398
title: Propene
tags:
    - Properties
    - Occurrence in nature
    - Production
    - Uses
    - Reactions
    - Combustion
    - Environmental safety
    - Storage and handling
    - Pharmacology
categories:
    - Uncategorized
---
Propene, also known as propylene or methyl ethylene, is an unsaturated organic compound having the chemical formula C3H6. It has one double bond, and is the second simplest member of the alkene class of hydrocarbons.
