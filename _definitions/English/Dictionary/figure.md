---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/figure
offline_file: ""
offline_thumbnail: ""
uuid: 456f14e6-f76f-459d-a749-204f5279d54e
updated: 1484310341
title: figure
categories:
    - Dictionary
---
figure
     n 1: a diagram or picture illustrating textual material; "the
          area covered can be seen from Figure 2" [syn: {fig}]
     2: alternative names for the body of a human being; "Leonardo
        studied the human body"; "he has a strong physique"; "the
        spirit is willing but the flesh is weak" [syn: {human body},
         {physical body}, {material body}, {soma}, {build}, {physique},
         {anatomy}, {shape}, {bod}, {chassis}, {frame}, {form}, {flesh}]
     3: one of the elements that collectively form a system of
        numbers; "0 and 1 are digits" [syn: {digit}]
     4: a model of a bodily form (especially of a person); "he made
        a figure of Santa ...
