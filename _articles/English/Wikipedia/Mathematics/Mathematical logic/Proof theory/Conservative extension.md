---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conservative_extension
offline_file: ""
offline_thumbnail: ""
uuid: b09ce3fc-7010-4833-bb8d-6d7ec8f8f571
updated: 1484309492
title: Conservative extension
tags:
    - Examples
    - Model-theoretic conservative extension
categories:
    - Proof theory
---
In mathematical logic, a conservative extension is a supertheory of a theory which is often convenient for proving theorems, but proves no new theorems about the language of the original theory.
