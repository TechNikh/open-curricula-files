---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/statistical
offline_file: ""
offline_thumbnail: ""
uuid: 261f8c37-117d-461d-a50c-7b516db0977d
updated: 1484310236
title: statistical
categories:
    - Dictionary
---
statistical
     adj : of or relating to statistics; "statistical population"
