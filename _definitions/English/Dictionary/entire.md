---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entire
offline_file: ""
offline_thumbnail: ""
uuid: fd0db3fc-3dca-45f9-a0c4-4c0061ca9ce7
updated: 1484310361
title: entire
categories:
    - Dictionary
---
entire
     adj 1: constituting the full quantity or extent; complete; "an
            entire town devastated by an earthquake"; "gave full
            attention"; "a total failure" [syn: {full}, {total}]
     2: constituting the undiminished entirety; lacking nothing
        essential especially not damaged; "a local motion keepeth
        bodies integral"- Bacon; "was able to keep the collection
        entire during his lifetime"; "fought to keep the union
        intact" [syn: {integral}, {intact}]
     3: (of leaves or petals) having a smooth edge; not broken up
        into teeth or lobes
     4: (used of domestic animals) sexually competent; "an entire
        horse" [syn: {intact}]
 ...
