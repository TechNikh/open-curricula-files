---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disappointed
offline_file: ""
offline_thumbnail: ""
uuid: f137e880-7fba-4714-924c-f1d2d04784cf
updated: 1484310451
title: disappointed
categories:
    - Dictionary
---
disappointed
     adj : disappointingly unsuccessful; "disappointed expectations and
           thwarted ambitions"; "their foiled attempt to capture
           Calais"; "many frustrated poets end as pipe-smoking
           teachers"; "his best efforts were thwarted" [syn: {defeated},
            {discomfited}, {foiled}, {frustrated}, {thwarted}]
