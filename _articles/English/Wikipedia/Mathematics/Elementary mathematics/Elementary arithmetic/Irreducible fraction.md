---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irreducible_fraction
offline_file: ""
offline_thumbnail: ""
uuid: d0d386b0-3475-4ba1-920e-75aafad6e26a
updated: 1484308913
title: Irreducible fraction
tags:
    - Examples
    - Uniqueness
    - Applications
    - Generalization
categories:
    - Elementary arithmetic
---
An irreducible fraction (or fraction in lowest terms or reduced fraction) is a fraction in which the numerator and denominator are integers that have no other common divisors than 1 (and -1, when negative numbers are considered).[1] In other words, a fraction a⁄b is irreducible if and only if a and b are coprime, that is, if a and b have a greatest common divisor of 1. In higher mathematics, "irreducible fraction" may also refer to rational fractions such that the numerator and the denominator are coprime polynomials.[2] Every positive rational number can be represented as an irreducible fraction in exactly one way.[3]
