---
version: 1
type: video
provider: YouTube
id: tCqYFmaRE1U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d6fc9f97-3b4a-4139-9168-7eef8ec038fd
updated: 1486069629
title: 'Learn Hindi Grammar - SARVANAM (सर्वनाम)  Pronouns'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/tCqYFmaRE1U/default.jpg
    - https://i3.ytimg.com/vi/tCqYFmaRE1U/1.jpg
    - https://i3.ytimg.com/vi/tCqYFmaRE1U/2.jpg
    - https://i3.ytimg.com/vi/tCqYFmaRE1U/3.jpg
---
