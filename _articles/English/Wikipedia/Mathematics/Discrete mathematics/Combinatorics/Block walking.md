---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Block_walking
offline_file: ""
offline_thumbnail: ""
uuid: f960de5f-98f4-404f-9dc4-a3e9a2da8ad1
updated: 1484308577
title: Block walking
tags:
    - An example block walking problem
    - Solution by brute force
    - Combinatorial solution
    - Other problems with known block walking combinatorial proofs
categories:
    - Combinatorics
---
In combinatorial mathematics, block walking is a method useful in thinking about sums of combinations graphically as "walks" on Pascal's triangle. As the name suggests, block walking problems involve counting the number of ways an individual can walk from one corner A of a city block to another corner B of another city block given restrictions on the number of blocks the person may walk, the directions the person may travel, the distance from A to B, et cetera.
