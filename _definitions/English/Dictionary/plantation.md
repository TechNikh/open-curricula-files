---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plantation
offline_file: ""
offline_thumbnail: ""
uuid: 7ad883f7-4499-47bd-9cf3-a0d36fd7e52e
updated: 1484310258
title: plantation
categories:
    - Dictionary
---
plantation
     n 1: an estate where cash crops are grown on a large scale
          (especially in tropical areas)
     2: a newly established colony (especially in the colonization
        of North America); "the practice of sending convicted
        criminals to serve on the Plantations was common in the
        17th century"
     3: garden consisting of a small cultivated wood without
        undergrowth [syn: {grove}, {woodlet}, {orchard}]
