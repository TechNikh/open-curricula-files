---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stationery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628241
title: stationery
categories:
    - Dictionary
---
stationery
     n : paper cut to an appropriate size for writing letters;
         usually with matching envelopes [syn: {letter paper}]
