---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chance
offline_file: ""
offline_thumbnail: ""
uuid: 48e88b9d-5968-4c35-9984-353c368d1cc1
updated: 1484310293
title: chance
categories:
    - Dictionary
---
chance
     adj : occurring or appearing or singled out by chance; "their
           accidental meeting led to a renewal of their
           friendship"; "seek help from casual passers-by"; "a
           casual meeting"; "a chance occurrence" [syn: {accidental},
            {casual}, {chance(a)}]
     n 1: a possibility due to a favorable combination of
          circumstances; "the holiday gave us the opportunity to
          visit Washington"; "now is your chance" [syn: {opportunity}]
     2: an unknown and unpredictable phenomenon that causes an event
        to result one way rather than another; "bad luck caused
        his downfall"; "we ran into each other by pure chance"
        ...
