---
version: 1
type: video
provider: YouTube
id: yQP4UJhNn0I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 270906cc-14a5-46fd-bf70-a32ee427c0fe
updated: 1486069665
title: Just How Small is an Atom?
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/yQP4UJhNn0I/default.jpg
    - https://i3.ytimg.com/vi/yQP4UJhNn0I/1.jpg
    - https://i3.ytimg.com/vi/yQP4UJhNn0I/2.jpg
    - https://i3.ytimg.com/vi/yQP4UJhNn0I/3.jpg
---
