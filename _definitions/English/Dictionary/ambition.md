---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ambition
offline_file: ""
offline_thumbnail: ""
uuid: 0a1878e8-ac34-4559-be16-be0c04ab5d16
updated: 1484310174
title: ambition
categories:
    - Dictionary
---
ambition
     n 1: a cherished desire; "his ambition is to own his own
          business" [syn: {aspiration}, {dream}]
     2: a strong drive for success [syn: {ambitiousness}]
     v : have as one's ambition
