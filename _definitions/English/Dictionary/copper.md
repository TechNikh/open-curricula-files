---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/copper
offline_file: ""
offline_thumbnail: ""
uuid: 498a3e29-70c9-4187-88b1-cf080bffd93e
updated: 1484310230
title: copper
categories:
    - Dictionary
---
copper
     n 1: a ductile malleable reddish-brown corrosion-resistant
          diamagnetic metallic element; occurs in various minerals
          but is the only metal that occurs abundantly in large
          masses; used as an electrical and thermal conductor
          [syn: {Cu}, {atomic number 29}]
     2: a copper penny
     3: uncomplimentary terms for a policeman [syn: {bull}, {cop}, {fuzz},
         {pig}]
     4: a reddish brown the color of polished copper [syn: {copper
        color}]
     5: any of various small butterflies of the family Lycaenidae
        having copper colored wings
     v : coat with a layer of copper
