---
version: 1
type: article
id: https://en.wikipedia.org/wiki/162_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 6e5e70a6-1767-473a-9760-2decf8e48cad
updated: 1484308989
title: 162 (number)
categories:
    - Integers
---
Having only 2 and 3 as its prime divisors, 162 is a 3-smooth number.[1] 162 is also a polygonal number[2] and an abundant number.[3]
