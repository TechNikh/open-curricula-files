---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hyphen
offline_file: ""
offline_thumbnail: ""
uuid: b255ee0c-7f7a-4ae7-9d8f-809b238249bd
updated: 1484310421
title: hyphen
categories:
    - Dictionary
---
hyphen
     n : a punctuation mark (-) used between parts of a compound word
         or between the syllables of a word when the word is
         divided at the end of a line of text [syn: {dash}]
     v : divide or connect with a hyphen; "hyphenate these words and
         names" [syn: {hyphenate}]
