---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strongly
offline_file: ""
offline_thumbnail: ""
uuid: fd627e01-8405-49a3-bb5c-97e0a24ef962
updated: 1484310389
title: strongly
categories:
    - Dictionary
---
strongly
     adv 1: with strength or in a strong manner; "argues very strongly
            for his proposal"; "he was strongly opposed to the
            government" [ant: {weakly}]
     2: in a powerful manner; "the federal government replaced the
        powerfully pro-settler Sir Godfrey Huggins with the even
        tougher and more determined ex-trade unionist" [syn: {powerfully}]
