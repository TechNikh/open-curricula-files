---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seem
offline_file: ""
offline_thumbnail: ""
uuid: 8d058655-73dd-41b0-8306-28911f9fa406
updated: 1484310220
title: seem
categories:
    - Dictionary
---
seem
     v 1: give a certain impression or have a certain outward aspect;
          "She seems to be sleeping"; "This appears to be a very
          difficult problem"; "This project looks fishy"; "They
          appeared like people who had not eaten or slept for a
          long time" [syn: {look}, {appear}]
     2: seem to be true, probable, or apparent; "It seems that he is
        very gifted"; "It appears that the weather in California
        is very bad" [syn: {appear}]
     3: appear to exist; "There seems no reason to go ahead with the
        project now"
     4: appear to one's own mind or opinion; "I seem to be
        misunderstood by everyone"; "I can't seem to learn these
  ...
