---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Julian
offline_file: ""
offline_thumbnail: ""
uuid: 023918a9-4729-4672-a7f4-55199cc6ae1b
updated: 1484308479
title: Julian
tags:
    - Variations
    - people
    - Antiquity
    - Arts
    - Politics
    - religion
    - Sciences
    - Sports
    - Other
    - Fictional
categories:
    - Uncategorized
---
Julian is a common male given name in Germany, Australia, the United Kingdom, Ireland, the Netherlands, France (as Julien), Italy (as Giuliano), Spain, Latin America (as Julián in Spanish and Juliano or Julião in Portuguese) and elsewhere.
