---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/performance
offline_file: ""
offline_thumbnail: ""
uuid: 5031241c-902a-41ef-b7f3-d577de078353
updated: 1484310447
title: performance
categories:
    - Dictionary
---
performance
     n 1: a dramatic or musical entertainment; "they listened to ten
          different performances"; "the play ran for 100
          performances"; "the frequent performances of the
          symphony testify to its popularity" [syn: {public
          presentation}]
     2: the act of performing; of doing something successfully;
        using knowledge as distinguished from merely possessing
        it; "they criticised his performance as mayor";
        "experience generally improves performance" [syn: {execution},
         {carrying out}, {carrying into action}]
     3: the act of presenting a play or a piece of music or other
        entertainment; "we congratulated him on ...
