---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lobe
offline_file: ""
offline_thumbnail: ""
uuid: 64fb5d1c-269f-4b7c-9127-ff18f38cc125
updated: 1484310309
title: lobe
categories:
    - Dictionary
---
lobe
     n 1: (anatomy) a somewhat rounded subdivision of a bodily organ
          or part; "ear lobe"
     2: (botany) a part into which a leaf is divided
     3: the enhanced response of an antenna in a given direction as
        indicated by a loop in its radiation pattern
     4: a rounded projection that is part of a larger structure
