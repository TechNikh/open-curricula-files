---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_bundle
offline_file: ""
offline_thumbnail: ""
uuid: d2e12a85-4f53-4029-bce2-ac06c645f229
updated: 1484309202
title: Affine bundle
tags:
    - Formal definition
    - Properties
    - Notes
categories:
    - Differential geometry
---
