---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Series_multisection
offline_file: ""
offline_thumbnail: ""
uuid: cfb9628b-196e-43f5-985e-9ba4b3ae1be5
updated: 1484308599
title: Series multisection
categories:
    - Combinatorics
---
In mathematics, a multisection of a power series is a new power series composed of equally spaced terms extracted unaltered from the original series. Formally, if one is given a power series
