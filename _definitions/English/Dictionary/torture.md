---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/torture
offline_file: ""
offline_thumbnail: ""
uuid: 4cb7575b-80b9-4a1f-b4a8-f72da7cc45e7
updated: 1484310611
title: torture
categories:
    - Dictionary
---
torture
     n 1: extreme mental distress [syn: {anguish}, {torment}]
     2: unbearable physical pain [syn: {torment}]
     3: intense feelings of suffering; acute mental or physical
        pain; "an agony of doubt"; "the torments of the damned"
        [syn: {agony}, {torment}]
     4: the act of distorting something so it seems to mean
        something it was not intended to mean [syn: {distortion},
        {overrefinement}, {straining}, {twisting}]
     5: the act of torturing someone; "it required unnatural
        torturing to extract a confession" [syn: {torturing}]
     v 1: torment emotionally or mentally [syn: {torment}, {excruciate},
           {rack}]
     2: subject to ...
