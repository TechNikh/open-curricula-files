---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tick-tock
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496001
title: tick-tock
categories:
    - Dictionary
---
ticktock
     n : steady recurrent ticking sound as made by a clock [syn: {tocktact},
          {tictac}]
     v : make a sound like a clock or a timer; "the clocks were
         ticking"; "the grandfather clock beat midnight" [syn: {tick},
          {ticktack}, {beat}]
