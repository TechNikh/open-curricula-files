---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trend
offline_file: ""
offline_thumbnail: ""
uuid: 08e9337d-8cc7-4155-ab17-1fb9da2767d8
updated: 1484310401
title: trend
categories:
    - Dictionary
---
trend
     n 1: a general direction in which something tends to move; "the
          shoreward tendency of the current"; "the trend of the
          stock market" [syn: {tendency}]
     2: general line of orientation; "the river takes a southern
        course"; "the northeastern trend of the coast" [syn: {course}]
     3: a general tendency to change (as of opinion); "not openly
        liberal but that is the trend of the book"; "a broad
        movement of the electorate to the right" [syn: {drift}, {movement}]
     4: the popular taste at a given time; "leather is the latest
        vogue"; "he followed current trends"; "the 1920s had a
        style of their own" [syn: {vogue}, ...
