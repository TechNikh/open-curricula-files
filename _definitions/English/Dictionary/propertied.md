---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propertied
offline_file: ""
offline_thumbnail: ""
uuid: 417c337a-784e-4727-b0db-1a4c6f10c20c
updated: 1484310603
title: propertied
categories:
    - Dictionary
---
propertied
     adj : owning land or securities as a principal source of revenue
           [syn: {property-owning}]
