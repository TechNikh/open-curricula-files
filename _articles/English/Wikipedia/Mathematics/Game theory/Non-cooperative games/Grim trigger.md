---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grim_trigger
offline_file: ""
offline_thumbnail: ""
uuid: f751ede1-2b31-4e6a-8f05-2d2b44dfe6d9
updated: 1484309050
title: Grim trigger
categories:
    - Non-cooperative games
---
In game theory, grim trigger (also called the grim strategy or just grim) is a trigger strategy for a repeated game, such as an iterated prisoner's dilemma. Initially, a player using grim trigger will cooperate, but as soon as the opponent defects (thus satisfying the trigger condition), the player using grim trigger will defect for the remainder of the iterated game. Since a single defect by the opponent triggers defection forever, grim trigger is the most strictly unforgiving of strategies in an iterated game.
