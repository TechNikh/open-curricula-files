---
version: 1
type: video
provider: YouTube
id: E1j8W64NQ0Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Applying%20rational%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e03c1143-6a43-4c20-8656-3780d8fa9e52
updated: 1486069561
title: Applying rational equations 2
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/E1j8W64NQ0Y/default.jpg
    - https://i3.ytimg.com/vi/E1j8W64NQ0Y/1.jpg
    - https://i3.ytimg.com/vi/E1j8W64NQ0Y/2.jpg
    - https://i3.ytimg.com/vi/E1j8W64NQ0Y/3.jpg
---
