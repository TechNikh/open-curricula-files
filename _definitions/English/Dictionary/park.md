---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/park
offline_file: ""
offline_thumbnail: ""
uuid: 31689257-b221-44ba-8e48-92e0a6fb7964
updated: 1484310168
title: park
categories:
    - Dictionary
---
park
     n 1: a large area of land preserved in its natural state as
          public property; "there are laws that protect the
          wildlife in this park" [syn: {parkland}]
     2: a piece of open land for recreational use in an urban area;
        "they went for a walk in the park" [syn: {commons}, {common},
         {green}]
     3: a facility in which ball games are played (especially
        baseball games); "take me out to the ballpark" [syn: {ballpark}]
     4: Scottish explorer in Africa (1771-1806) [syn: {Mungo Park}]
     5: a lot where cars are parked [syn: {parking lot}, {car park},
         {parking area}]
     6: a gear position that acts as a parking brake; "the put ...
