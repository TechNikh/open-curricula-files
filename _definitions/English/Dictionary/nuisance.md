---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nuisance
offline_file: ""
offline_thumbnail: ""
uuid: fbc9486a-7eeb-4361-8725-d49ef9947a18
updated: 1484310144
title: nuisance
categories:
    - Dictionary
---
nuisance
     n 1: (law) a broad legal concept including anything that disturbs
          the reasonable use of your property or endangers life
          and health or is offensive
     2: a bothersome annoying person; "that kid is a terrible pain"
        [syn: {pain}, {pain in the neck}]
