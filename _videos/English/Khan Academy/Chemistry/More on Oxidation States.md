---
version: 1
type: video
provider: YouTube
id: GA88JI4AymY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/More%20on%20Oxidation%20States.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e3314f75-5a38-4037-a463-51f0a0c2e9aa
updated: 1486069585
title: More on Oxidation States
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/GA88JI4AymY/default.jpg
    - https://i3.ytimg.com/vi/GA88JI4AymY/1.jpg
    - https://i3.ytimg.com/vi/GA88JI4AymY/2.jpg
    - https://i3.ytimg.com/vi/GA88JI4AymY/3.jpg
---
More practice calculating oxidation states
