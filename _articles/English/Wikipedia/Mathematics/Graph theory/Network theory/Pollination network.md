---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pollination_network
offline_file: ""
offline_thumbnail: ""
uuid: 91c6dc67-d281-43a1-afda-9e2a85604d03
updated: 1484309440
title: Pollination network
tags:
    - Nested structure of pollination networks
    - Modularity of networks
    - Species loss and robustness to collapse
categories:
    - Network theory
---
A pollination network is a bipartite mutualistic network in which plants and pollinators are the nodes, and the pollination interactions form the links between these nodes.[1] The pollination network is bipartite as interactions only exist between two distinct, non-overlapping sets of species, but not within the set: a pollinator can never be pollinated, unlike in a predator-prey network where a predator can be depredated.[2] A pollination network is two-modal, i.e., it includes only links connecting plant and animal communities.[3]
