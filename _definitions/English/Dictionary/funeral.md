---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funeral
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484581681
title: funeral
categories:
    - Dictionary
---
funeral
     n : a ceremony at which a dead person is buried or cremated;
         "hundreds of people attended his funeral"
