---
version: 1
type: video
provider: YouTube
id: l128tW1H5a8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Urinary%20System%2C%20part%201-%20Crash%20Course%20A%26P%20%2338.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4ceacb34-7140-40df-b717-c420a562ee61
updated: 1486069681
title: 'Urinary System, part 1- Crash Course A&P #38'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/l128tW1H5a8/default.jpg
    - https://i3.ytimg.com/vi/l128tW1H5a8/1.jpg
    - https://i3.ytimg.com/vi/l128tW1H5a8/2.jpg
    - https://i3.ytimg.com/vi/l128tW1H5a8/3.jpg
---
