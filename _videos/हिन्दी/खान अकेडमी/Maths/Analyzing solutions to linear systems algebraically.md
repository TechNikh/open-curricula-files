---
version: 1
type: video
provider: YouTube
id: jeWlzbaCvxk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b6511ffd-e87e-415d-b145-71924b1a2b46
updated: 1486069607
title: Analyzing solutions to linear systems algebraically
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/jeWlzbaCvxk/default.jpg
    - https://i3.ytimg.com/vi/jeWlzbaCvxk/1.jpg
    - https://i3.ytimg.com/vi/jeWlzbaCvxk/2.jpg
    - https://i3.ytimg.com/vi/jeWlzbaCvxk/3.jpg
---
