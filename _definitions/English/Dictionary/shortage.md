---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shortage
offline_file: ""
offline_thumbnail: ""
uuid: 6a09966a-9d20-45dd-9ee8-6e53d6825fdc
updated: 1484310290
title: shortage
categories:
    - Dictionary
---
shortage
     n 1: the property of being an amount by which something is less
          than expected or required [syn: {deficit}, {shortfall}]
     2: an acute insufficiency [syn: {dearth}, {famine}]
