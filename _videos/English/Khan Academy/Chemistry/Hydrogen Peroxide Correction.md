---
version: 1
type: video
provider: YouTube
id: YPjtG5PA-iY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Hydrogen%20Peroxide%20Correction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 33f8a683-6f1a-44a3-b59a-271c858284e6
updated: 1486069589
title: Hydrogen Peroxide Correction
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/YPjtG5PA-iY/default.jpg
    - https://i3.ytimg.com/vi/YPjtG5PA-iY/1.jpg
    - https://i3.ytimg.com/vi/YPjtG5PA-iY/2.jpg
    - https://i3.ytimg.com/vi/YPjtG5PA-iY/3.jpg
---
Correcting an error in the last video regarding hydrogen peroxide.
