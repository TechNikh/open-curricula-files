---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contain
offline_file: ""
offline_thumbnail: ""
uuid: 7b4ca63e-b816-4724-80a6-1d9ff6d04df1
updated: 1484310303
title: contain
categories:
    - Dictionary
---
contain
     v 1: include or contain; have as a component; "A totally new idea
          is comprised in this paper"; "The record contains many
          old songs from the 1930's" [syn: {incorporate}, {comprise}]
     2: contain or hold; have within; "The jar carries wine"; "The
        canteen holds fresh water"; "This can contains water"
        [syn: {hold}, {bear}, {carry}]
     3: lessen the intensity of; temper; hold in restraint; hold or
        keep within limits; "moderate your alcohol intake"; "hold
        your tongue"; "hold your temper"; "control your anger"
        [syn: {control}, {hold in}, {hold}, {check}, {curb}, {moderate}]
     4: be divisible by; "24 contains 6"
     ...
