---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loge
offline_file: ""
offline_thumbnail: ""
uuid: c36c43a9-e653-4c13-8532-9b56dc02cb6d
updated: 1484310140
title: loge
categories:
    - Dictionary
---
loge
     n 1: balcony consisting of the forward section of a theater
          mezzanine
     2: private area in a theater or grandstand where a small group
        can watch the performance; "the royal box was empty" [syn:
         {box}]
