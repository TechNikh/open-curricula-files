---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Craig%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ea8372dc-09c5-48a0-81de-6962a29e1dad
updated: 1484308994
title: "Craig's theorem"
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, Craig's theorem states that any recursively enumerable set of well-formed formulas of a first-order language is (primitively) recursively axiomatizable. This result is not related to the well-known Craig interpolation theorem, although both results are named after the same mathematician, William Craig.
