---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extension_by_definitions
offline_file: ""
offline_thumbnail: ""
uuid: e2b5d0ba-3983-4f8d-a08c-f4db4e4c6d59
updated: 1484309492
title: Extension by definitions
tags:
    - Definition of relation symbols
    - Definition of function symbols
    - Extensions by definitions
    - Examples
    - Bibliography
categories:
    - Proof theory
---
In mathematical logic, more specifically in the proof theory of first-order theories, extensions by definitions formalize the introduction of new symbols by means of a definition. For example, it is common in naive set theory to introduce a symbol 
  
    
      
        ∅
      
    
    {\displaystyle \emptyset }
  
 for the set which has no member. In the formal setting of first-order theories, this can be done by adding to the theory a new constant 
  
    
      
        ∅
      
    
    {\displaystyle \emptyset }
  
 and the new axiom 
  
    
      
        ∀
        x
        (
        x
        ∉
        ∅
        )
      
    
    {\displaystyle \forall x(x\notin ...
