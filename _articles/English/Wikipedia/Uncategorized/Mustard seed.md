---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mustard_seeds
offline_file: ""
offline_thumbnail: ""
uuid: 49ef9180-3777-4bbf-864f-7924d38e76bd
updated: 1484308355
title: Mustard seed
tags:
    - Regional usage
    - Cultivation
    - Production
    - Religious significance
categories:
    - Uncategorized
---
Mustard seeds are the small round seeds of various mustard plants. The seeds are usually about 1 to 2 millimetres (0.039 to 0.079 in) in diameter and may be colored from yellowish white to black. They are important spice in many regional foods and may come from one of three different plants: black mustard (Brassica nigra), brown Indian mustard (B. juncea), or white mustard (B. hirta/Sinapis alba).
