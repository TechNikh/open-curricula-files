---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/piano
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484430061
title: piano
categories:
    - Dictionary
---
piano
     adj : used chiefly as a direction or description in music; "the
           piano passages in the composition" [syn: {soft}] [ant:
           {forte}]
     n 1: a stringed instrument that is played by depressing keys that
          cause hammers to strike tuned strings and produce sounds
          [syn: {pianoforte}, {forte-piano}]
     2: (music) low loudness [syn: {pianissimo}]
     adv : used as a direction in music; to be played relatively softly
           [syn: {softly}] [ant: {forte}]
