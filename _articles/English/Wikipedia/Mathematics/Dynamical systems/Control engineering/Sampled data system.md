---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sampled_data_system
offline_file: ""
offline_thumbnail: ""
uuid: bcde7d4d-764c-41b4-b197-2ae358a1e678
updated: 1484308743
title: Sampled data system
categories:
    - Control engineering
---
In systems science, a sampled-data system is a control system in which a continuous-time plant is controlled with a digital device. Under periodic sampling, the sampled-data system is time-varying but also periodic; thus, it may be modeled by a simplified discrete-time system obtained by discretizing the plant. However, this discrete model does not capture the inter-sample behavior of the real system, which may be critical in a number of applications.
