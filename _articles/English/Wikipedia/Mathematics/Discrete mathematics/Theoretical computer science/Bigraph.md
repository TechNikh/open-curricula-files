---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bigraph
offline_file: ""
offline_thumbnail: ""
uuid: 93cae1fc-1770-4691-8181-3e723f2666b1
updated: 1484308671
title: Bigraph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/230px-Bigraphs-sharing-example.svg.png
tags:
    - Anatomy of a bigraph
    - Foundations
    - Extensions and variants
    - Bigraphs with sharing
    - Bibliography
categories:
    - Theoretical computer science
---
A bigraph (often used in the plural bigraphs) can be modelled as the superposition of a graph (the link graph) and a set of trees (the place graph).[1][2]
