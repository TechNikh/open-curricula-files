---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Logistello
offline_file: ""
offline_thumbnail: ""
uuid: a4cb5a4e-9db4-46b5-93fe-1c3b633d252b
updated: 1484309011
title: Logistello
categories:
    - Game artificial intelligence
---
Logistello is a computer program that plays the game Othello, also known as Reversi. Logistello was written by Michael Buro and is regarded as a strong player, having beaten the human world champion Takeshi Murakami six games to none in 1997 — the best Othello programs are now much stronger than any human player. Logistello's evaluation function is based on disc patterns and features over a million numerical parameters which were tuned using linear regression.
