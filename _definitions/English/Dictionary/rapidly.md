---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rapidly
offline_file: ""
offline_thumbnail: ""
uuid: 43eba45f-2633-4c00-833f-7f82217a4fad
updated: 1484310236
title: rapidly
categories:
    - Dictionary
---
rapidly
     adv : with rapid movements; "he works quickly" [syn: {quickly}, {speedily},
            {chop-chop}, {apace}] [ant: {slowly}]
