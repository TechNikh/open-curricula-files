---
version: 1
type: video
provider: YouTube
id: ASTP-Dpf4Ss
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 996de32c-01b6-4ec4-81f7-1695f2a1fcf2
updated: 1486069664
title: 'Chapter 1- Introduction to Analytical Chemistry'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ASTP-Dpf4Ss/default.jpg
    - https://i3.ytimg.com/vi/ASTP-Dpf4Ss/1.jpg
    - https://i3.ytimg.com/vi/ASTP-Dpf4Ss/2.jpg
    - https://i3.ytimg.com/vi/ASTP-Dpf4Ss/3.jpg
---
