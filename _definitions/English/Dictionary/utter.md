---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647141
title: utter
categories:
    - Dictionary
---
utter
     adj 1: without qualification; used informally as (often pejorative)
            intensifiers; "an arrant fool"; "a complete coward";
            "a consummate fool"; "a double-dyed villain"; "gross
            negligence"; "a perfect idiot"; "pure folly"; "what a
            sodding mess"; "stark staring mad"; "a thoroughgoing
            villain"; "utter nonsense" [syn: {arrant(a)}, {complete(a)},
             {consummate(a)}, {double-dyed(a)}, {everlasting(a)},
            {gross(a)}, {perfect(a)}, {pure(a)}, {sodding(a)}, {stark(a)},
             {staring(a)}, {thoroughgoing(a)}, {utter(a)}]
     2: total; "dead silence"; "utter seriousness" [syn: {dead(a)},
        ...
