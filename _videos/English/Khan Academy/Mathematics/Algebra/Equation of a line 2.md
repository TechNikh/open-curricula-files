---
version: 1
type: video
provider: YouTube
id: -BuoPowT86M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Equation%20of%20a%20line%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b2aa518-7834-417b-836b-e8e16308fab0
updated: 1486069559
title: Equation of a line 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/-BuoPowT86M/default.jpg
    - https://i3.ytimg.com/vi/-BuoPowT86M/1.jpg
    - https://i3.ytimg.com/vi/-BuoPowT86M/2.jpg
    - https://i3.ytimg.com/vi/-BuoPowT86M/3.jpg
---
