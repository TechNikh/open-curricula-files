---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adverb
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468281
title: adverb
categories:
    - Dictionary
---
adverb
     n 1: the word class that qualifies verbs or clauses
     2: a word that modifies something other than a noun
