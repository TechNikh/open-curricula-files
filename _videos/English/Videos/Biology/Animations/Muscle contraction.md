---
version: 1
type: video
provider: YouTube
id: gJ309LfHQ3M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Muscle%20contraction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e9dbb964-5a7f-4bbc-b816-c1275042aca3
updated: 1486069666
title: Muscle contraction
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/gJ309LfHQ3M/default.jpg
    - https://i3.ytimg.com/vi/gJ309LfHQ3M/1.jpg
    - https://i3.ytimg.com/vi/gJ309LfHQ3M/2.jpg
    - https://i3.ytimg.com/vi/gJ309LfHQ3M/3.jpg
---
