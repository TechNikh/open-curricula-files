---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carbon_dating
offline_file: ""
offline_thumbnail: ""
uuid: 4264b100-d29a-40b5-96fb-319bf4e19aba
updated: 1484308378
title: Radiocarbon dating
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon_4.png
tags:
    - Background
    - History
    - Physical and chemical details
    - Principles
    - Carbon exchange reservoir
    - Dating considerations
    - Atmospheric variation
    - Isotopic fractionation
    - Reservoir effects
    - Contamination
    - Samples
    - Material considerations
    - Preparation and size
    - Measurement and results
    - Beta counting
    - Accelerator mass spectrometry
    - Calculations
    - Errors and reliability
    - Calibration
    - Reporting dates
    - Use in archaeology
    - Interpretation
    - Notable applications
    - Pleistocene/Holocene boundary in Two Creeks Fossil Forest
    - Dead Sea Scrolls
    - Impact
    - Notes
    - Sources
categories:
    - Uncategorized
---
Radiocarbon dating (also referred to as carbon dating or carbon-14 dating) is a method for determining the age of an object containing organic material by using the properties of radiocarbon (14C), a radioactive isotope of carbon.
