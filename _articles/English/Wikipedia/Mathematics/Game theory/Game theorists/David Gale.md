---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Gale
offline_file: ""
offline_thumbnail: ""
uuid: ac03a21f-3ac4-46e0-9e4a-b1977055cd7f
updated: 1484308513
title: David Gale
tags:
    - Contribution
    - Awards and honors
    - Selected publications
    - Notes
categories:
    - Game theorists
---
David Gale (December 13, 1921 – March 7, 2008) was an American mathematician and economist. He was a professor emeritus at the University of California, Berkeley, affiliated with the departments of mathematics, economics, and industrial engineering and operations research. He has contributed to the fields of mathematical economics, game theory, and convex analysis.
