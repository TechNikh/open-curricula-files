---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_A._Catlin
offline_file: ""
offline_thumbnail: ""
uuid: 6d753795-c729-4172-8695-999f847f8d57
updated: 1484309459
title: Paul A. Catlin
categories:
    - Graph theorists
---
Paul Allen Catlin ((1948-06-25)June 25, 1948 – April 20, 1995(1995-04-20)) was a mathematician, professor of mathematics and Doctor of Mathematics, known for his valuable contributions to graph theory and number theory. He wrote one of the most cited papers in the series of chromatic numbers and Brooks' theorem, entitled Hajós graph coloring conjecture: variations and counterexamples.[1][2][3]
