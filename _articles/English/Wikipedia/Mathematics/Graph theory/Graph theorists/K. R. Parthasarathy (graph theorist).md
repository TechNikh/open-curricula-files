---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/K._R._Parthasarathy_(graph_theorist)
offline_file: ""
offline_thumbnail: ""
uuid: 920c62c3-0c96-4c76-a8e7-22f9e0226d14
updated: 1484309475
title: K. R. Parthasarathy (graph theorist)
categories:
    - Graph theorists
---
K. R. Parthasarathy is a professor emeritus of graph theory from the Department of Mathematics, Indian Institute of Technology Madras, Chennai. He received his Ph.D. (1966) in graph theory [1] from the Indian Institute of Technology Kharagpur. Parthasarathy is known for his work (with his student G. Ravindra) on strong perfect graph conjecture. Parthasarathy guided and refereed Ph.D. students in graph theory, among them S. A. Choudum. Parthasarathy wrote a book on graph theory - Basic Graph Theory, K. R. Parthasarathy - 1994 - Tata McGraw-Hill New York.
