---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Many-one_reduction
offline_file: ""
offline_thumbnail: ""
uuid: 177c8e56-1449-4371-a64b-5cd2d1446061
updated: 1484309487
title: Many-one reduction
tags:
    - Definitions
    - Formal languages
    - Subsets of natural numbers
    - Many-one equivalence and 1 equivalence
    - Many-one completeness (m-completeness)
    - Many-one reductions with resource limitations
    - Properties
    - reading
categories:
    - Computability theory
---
In computability theory and computational complexity theory, a many-one reduction is a reduction which converts instances of one decision problem into instances of a second decision problem. Reductions are thus used to measure the relative computational difficulty of two problems.
