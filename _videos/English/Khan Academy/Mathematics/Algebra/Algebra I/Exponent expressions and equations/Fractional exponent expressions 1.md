---
version: 1
type: video
provider: YouTube
id: xjCnmPvcmOo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Fractional%20exponent%20expressions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fbb4de4e-639d-4878-876a-90bd9d190e9e
updated: 1486069559
title: Fractional exponent expressions 1
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/xjCnmPvcmOo/default.jpg
    - https://i3.ytimg.com/vi/xjCnmPvcmOo/1.jpg
    - https://i3.ytimg.com/vi/xjCnmPvcmOo/2.jpg
    - https://i3.ytimg.com/vi/xjCnmPvcmOo/3.jpg
---
