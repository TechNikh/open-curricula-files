---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hang
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484563981
title: hang
categories:
    - Dictionary
---
hang
     n 1: a special way of doing something; "he had a bent for it";
          "he had a special knack for getting into trouble"; "he
          couldn't get the hang of it" [syn: {bent}, {knack}]
     2: the way a garment hangs; "he adjusted the hang of his coat"
     3: a gymnastic exercise performed on the rings or horizontal
        bar or parallel bars when the gymnast's weight is
        supported by the arms
     v 1: be suspended or hanging; "The flag hung on the wall"
     2: cause to be hanging or suspended; "Hang that picture on the
        wall" [syn: {hang up}]
     3: kill by hanging; "The murdered was hanged on Friday" [syn: {string
        up}]
     4: let drop or droop; ...
