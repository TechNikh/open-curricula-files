---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lissajous_orbit
offline_file: ""
offline_thumbnail: ""
uuid: 480b3dab-64ef-47c2-8186-bc53889ac3c8
updated: 1484309345
title: Lissajous orbit
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Lissajous_orbit_l2.jpg
tags:
    - Spacecraft using Lissajous orbits
    - Fictional appearances
categories:
    - Trigonometry
---
In orbital mechanics, a Lissajous orbit (pronounced: [li.sa.ʒu]), named after Jules Antoine Lissajous, is a quasi-periodic orbital trajectory that an object can follow around a Lagrangian point of a three-body system without requiring any propulsion. Lyapunov orbits around a Lagrangian point are curved paths that lie entirely in the plane of the two primary bodies. In contrast, Lissajous orbits include components in this plane and perpendicular to it, and follow a Lissajous curve. Halo orbits also include components perpendicular to the plane, but they are periodic, while Lissajous orbits are not.[1]
