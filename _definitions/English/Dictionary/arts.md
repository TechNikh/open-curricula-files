---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arts
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484639221
title: arts
categories:
    - Dictionary
---
arts
     n : studies intended to provide general knowledge and
         intellectual skills (rather than occupational or
         professional skills); "the college of arts and sciences"
         [syn: {humanistic discipline}, {humanities}, {liberal
         arts}]
