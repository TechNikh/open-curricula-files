---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fermat_Prize
offline_file: ""
offline_thumbnail: ""
uuid: 39814233-8eff-4948-8e75-7ddaac240ec9
updated: 1484308553
title: Fermat Prize
tags:
    - Previous prize winners
    - Junior Fermat Prize
categories:
    - Number theory
---
The spirit of the prize is focused on rewarding the results of researches accessible to the greatest number of professional mathematicians within these fields. The Fermat prize was created in 1989 and is awarded once every two years in Toulouse by the Institut de Mathématiques de Toulouse. The amount of the Fermat prize has been fixed at 20,000 Euros for the twelfth edition (2011).
