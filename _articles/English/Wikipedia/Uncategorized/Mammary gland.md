---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mammary_glands
offline_file: ""
offline_thumbnail: ""
uuid: e86b092d-a34c-40c8-93e9-9e2e50cf6fe6
updated: 1484308378
title: Mammary gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Breast_anatomy_normal_scheme.png
tags:
    - Structure
    - Histology
    - Development
    - Biochemistry
    - Timeline
    - Before birth
    - growth
    - Pregnancy
    - Postmenopausal
    - Physiology
    - Hormonal control
    - Pregnancy
    - Weaning
    - Clinical significance
    - Other mammals
    - General
    - Evolution
    - Additional images
    - Bibliography
categories:
    - Uncategorized
---
A mammary gland is an exocrine gland in mammals that produces milk to feed young offspring. Mammals get their name from the word "mammary". The mammary glands are arranged in organs such as the breasts in primates (for example, humans and chimpanzees), the udder in ruminants (for example, cows, goats, and deer), and the dugs of other animals (for example, dogs and cats). Lactorrhea, the occasional production of milk by the glands, can occur in any mammal, but in most mammals lactation, the production of enough milk for nursing, occurs only in phenotypic females who have gestated in recent months or years. It is directed by hormonal guidance from sex steroids. In a few mammalian species, ...
