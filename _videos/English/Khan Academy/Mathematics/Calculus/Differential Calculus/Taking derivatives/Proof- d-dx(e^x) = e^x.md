---
version: 1
type: video
provider: YouTube
id: sSE6_fK3mu0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Proof-%20d-dx%28e%5Ex%29%20%3D%20e%5Ex.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a98d19a6-d33f-4ff5-9f54-28ec5e023a29
updated: 1486069692
title: 'Proof- d/dx(e^x) = e^x'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/sSE6_fK3mu0/default.jpg
    - https://i3.ytimg.com/vi/sSE6_fK3mu0/1.jpg
    - https://i3.ytimg.com/vi/sSE6_fK3mu0/2.jpg
    - https://i3.ytimg.com/vi/sSE6_fK3mu0/3.jpg
---
