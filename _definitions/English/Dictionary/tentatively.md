---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tentatively
offline_file: ""
offline_thumbnail: ""
uuid: e7489fb7-6a97-4306-9d1a-9f8758d104b6
updated: 1484310397
title: tentatively
categories:
    - Dictionary
---
tentatively
     adv : in a tentative manner; "we agreed tentatively on a dinner
           date"
