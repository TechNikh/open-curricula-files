---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trachea
offline_file: ""
offline_thumbnail: ""
uuid: c6c97767-64d9-424c-b99c-48ea5fe84612
updated: 1484310160
title: trachea
categories:
    - Dictionary
---
trachea
     n 1: membranous tube with cartilaginous rings that conveys
          inhaled air from the larynx to the bronchi [syn: {windpipe}]
     2: one of the tubules forming the respiratory system of most
        insects and many arachnids
     [also: {tracheae} (pl)]
