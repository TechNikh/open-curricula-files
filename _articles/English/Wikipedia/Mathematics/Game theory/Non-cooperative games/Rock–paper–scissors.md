---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rock%E2%80%93paper%E2%80%93scissors'
offline_file: ""
offline_thumbnail: ""
uuid: 6ca15b93-b914-47ab-af3a-7b885781e5a1
updated: 1484309057
title: Rock–paper–scissors
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rock-paper-scissors.svg.png
tags:
    - Game play
    - History
    - Asian origin
    - Spread beyond Asia
    - Strategies
    - Algorithms
    - Instances of use in real-life scenarios
    - American case
    - Auction house match
    - In video games
    - Analogs in nature
    - Lizard mating strategies
    - Bacteria
    - Tournaments
    - International championship
    - Sanctioned
    - UK championships
    - USARPS tournaments
    - Team Olimpik Championships 2012
    - National XtremeRPS Competition 2007–2008
    - Guinness Book of World Records
    - World Series
    - Variations
    - Additional weapons
categories:
    - Non-cooperative games
---
Rock–paper–scissors is a zero-sum hand game usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand. These shapes are "rock" (✊ a simple fist), "paper" (✋ a flat hand), and "scissors" (✌️ a fist with the index and middle fingers together forming a V). The game has only three possible outcomes other than a tie: a player who decides to play rock will beat another player who has chosen scissors ("rock crushes scissors") but will lose to one who has played paper ("paper covers rock"); a play of paper will lose to a play of scissors ("scissors cut paper"). If both players choose the same shape, the game is tied and is ...
