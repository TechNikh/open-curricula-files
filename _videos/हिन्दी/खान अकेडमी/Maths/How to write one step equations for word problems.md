---
version: 1
type: video
provider: YouTube
id: ZLktq91gB7U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 40cdcf10-829e-4d86-893a-008b68dcd09a
updated: 1486069609
title: How to write one step equations for word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZLktq91gB7U/default.jpg
    - https://i3.ytimg.com/vi/ZLktq91gB7U/1.jpg
    - https://i3.ytimg.com/vi/ZLktq91gB7U/2.jpg
    - https://i3.ytimg.com/vi/ZLktq91gB7U/3.jpg
---
