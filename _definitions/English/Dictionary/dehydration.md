---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dehydration
offline_file: ""
offline_thumbnail: ""
uuid: 8a937005-713f-48e0-8831-4087f63fbece
updated: 1484310424
title: dehydration
categories:
    - Dictionary
---
dehydration
     n 1: dryness resulting from the removal of water [syn: {desiccation}]
     2: depletion of bodily fluids
     3: the process of extracting moisture [syn: {desiccation}, {drying
        up}, {evaporation}]
