---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abbreviated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484374261
title: abbreviated
categories:
    - Dictionary
---
abbreviated
     adj 1: (of clothing) very short; "an abbreviated swimsuit"; "a
            brief bikini" [syn: {brief}]
     2: cut short in duration; "the abbreviated speech"; "a
        curtailed visit"; "her shortened life was clearly the
        result of smoking"; "an unsatisfactory truncated
        conversation" [syn: {shortened}, {truncated}]
