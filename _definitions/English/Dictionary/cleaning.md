---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cleaning
offline_file: ""
offline_thumbnail: ""
uuid: 26bc5db9-f331-4788-8060-40cb4a25d54c
updated: 1484310244
title: cleaning
categories:
    - Dictionary
---
cleaning
     n : the act of making something clean; "he gave his shoes a good
         cleaning" [syn: {cleansing}, {cleanup}]
