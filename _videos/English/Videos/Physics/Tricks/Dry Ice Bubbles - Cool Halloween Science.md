---
version: 1
type: video
provider: YouTube
id: KG-qWeZm-LA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2dc3e6de-4c4f-4383-a949-42fb246c7362
updated: 1486069658
title: 'Dry Ice Bubbles - Cool Halloween Science'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/KG-qWeZm-LA/default.jpg
    - https://i3.ytimg.com/vi/KG-qWeZm-LA/1.jpg
    - https://i3.ytimg.com/vi/KG-qWeZm-LA/2.jpg
    - https://i3.ytimg.com/vi/KG-qWeZm-LA/3.jpg
---
