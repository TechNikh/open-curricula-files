---
version: 1
type: video
provider: YouTube
id: UUwmo5qaTuI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Graphing%20systems%20of%20inequalities%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1db05c99-21b7-4bfd-b1bc-d559becaa31d
updated: 1486069565
title: Graphing systems of inequalities 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/UUwmo5qaTuI/default.jpg
    - https://i3.ytimg.com/vi/UUwmo5qaTuI/1.jpg
    - https://i3.ytimg.com/vi/UUwmo5qaTuI/2.jpg
    - https://i3.ytimg.com/vi/UUwmo5qaTuI/3.jpg
---
