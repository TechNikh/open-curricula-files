---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enquire
offline_file: ""
offline_thumbnail: ""
uuid: c4377b20-1495-4cac-9a0d-a6969f78eb03
updated: 1484310518
title: enquire
categories:
    - Dictionary
---
enquire
     v 1: inquire about; "I asked about their special today"; "He had
          to ask directions several times" [syn: {ask}, {inquire}]
     2: conduct an inquiry or investigation of; "The district
        attorney's office investigated reports of possible
        irregularities"; "inquire into the disappearance of the
        rich old lady" [syn: {investigate}, {inquire}]
     3: have a wish or desire to know something; "He wondered who
        had built this beautiful church" [syn: {wonder}, {inquire}]
