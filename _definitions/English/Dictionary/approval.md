---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approval
offline_file: ""
offline_thumbnail: ""
uuid: 6ef9ea29-66bf-46f9-966f-1ca802cca5bb
updated: 1484310477
title: approval
categories:
    - Dictionary
---
approval
     n 1: the formal act of giving approval; "he gave the project his
          blessing"; "his decision merited the approval of any
          sensible person" [syn: {blessing}, {approving}] [ant: {disapproval}]
     2: a feeling of liking something or someone good; "although she
        fussed at them, she secretly viewed all her children with
        approval" [ant: {disapproval}]
     3: acceptance as satisfactory; "he bought it on approval" [syn:
         {favorable reception}, {favourable reception}]
     4: a message expressing a favorable opinion; "words of approval
        seldom passed his lips" [syn: {commendation}] [ant: {disapproval}]
