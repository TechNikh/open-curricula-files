---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quarrying
offline_file: ""
offline_thumbnail: ""
uuid: 118bb9ae-7687-4b59-959a-67f5def3596a
updated: 1484310537
title: quarrying
categories:
    - Dictionary
---
quarrying
     n : the extraction of building stone or slate from an open
         surface quarry
