---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maintenance
offline_file: ""
offline_thumbnail: ""
uuid: 944f64bf-c8c9-4c1e-88d1-d91ce192dabb
updated: 1484310260
title: maintenance
categories:
    - Dictionary
---
maintenance
     n 1: activity involved in maintaining something in good working
          order; "he wrote the manual on car care" [syn: {care}, {upkeep}]
     2: means of maintenance of a family or group
     3: court-ordered support paid by one spouse to another after
        they are separated [syn: {alimony}]
     4: the act of sustaining life by food or providing a means of
        subsistence; "they were in want of sustenance"; "fishing
        was their main sustainment" [syn: {sustenance}, {sustentation},
         {sustainment}, {upkeep}]
