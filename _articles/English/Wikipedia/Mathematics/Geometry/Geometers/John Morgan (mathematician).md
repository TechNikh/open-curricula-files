---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Morgan_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 5dd66b4e-b580-4220-a27e-6085a2140dd1
updated: 1484309093
title: John Morgan (mathematician)
tags:
    - life
    - Awards and honors
    - Selected publications
    - Articles
    - Books
categories:
    - Geometers
---
John Willard Morgan (born March 21, 1946) is an American mathematician, with contributions to topology and geometry. He is currently the director of the Simons Center for Geometry and Physics at Stony Brook University.
