---
version: 1
type: video
provider: YouTube
id: pWX1-euasU8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c4c1b185-7d71-4ae3-b35d-e0527bb638ff
updated: 1486069622
title: 'How to trick Your Math Teacher 3 - Math Tricks Revealed'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/pWX1-euasU8/default.jpg
    - https://i3.ytimg.com/vi/pWX1-euasU8/1.jpg
    - https://i3.ytimg.com/vi/pWX1-euasU8/2.jpg
    - https://i3.ytimg.com/vi/pWX1-euasU8/3.jpg
---
