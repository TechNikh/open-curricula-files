---
version: 1
type: video
provider: YouTube
id: Xg3FIaQSWpQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55d1d8be-159e-426c-beb0-7b051c22fe05
updated: 1486069630
title: 'Using Suffixes in Hindi  Class 3'
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xg3FIaQSWpQ/default.jpg
    - https://i3.ytimg.com/vi/Xg3FIaQSWpQ/1.jpg
    - https://i3.ytimg.com/vi/Xg3FIaQSWpQ/2.jpg
    - https://i3.ytimg.com/vi/Xg3FIaQSWpQ/3.jpg
---
