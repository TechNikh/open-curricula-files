---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demonstrate
offline_file: ""
offline_thumbnail: ""
uuid: e9c72abd-1ad0-4e3b-8269-bb856d070079
updated: 1484310567
title: demonstrate
categories:
    - Dictionary
---
demonstrate
     v 1: show or demonstrate something to an interested audience;
          "She shows her dogs frequently"; "We will demo the new
          software in Washington" [syn: {show}, {demo}, {exhibit},
           {present}]
     2: establish the validity of something, as by an example,
        explanation or experiment; "The experiment demonstrated
        the instability of the compound"; "The mathematician
        showed the validity of the conjecture" [syn: {prove}, {establish},
         {show}, {shew}] [ant: {disprove}]
     3: provide evidence for; stand as proof of; show by one's
        behavior, attitude, or external attributes; "His high
        fever attested to his ...
