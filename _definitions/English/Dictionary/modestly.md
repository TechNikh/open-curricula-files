---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modestly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508481
title: modestly
categories:
    - Dictionary
---
modestly
     adv : in a modest manner; "the dissertation was entitled,
           modestly, `Remarks about a play by Shakespeare'" [syn:
           {with modesty}] [ant: {immodestly}]
