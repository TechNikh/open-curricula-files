---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monitor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484634001
title: monitor
categories:
    - Dictionary
---
monitor
     n 1: display consisting of a device that takes signals from a
          computer and displays them on a CRT screen [syn: {monitoring
          device}]
     2: someone who supervises (an examination) [syn: {proctor}]
     3: someone who gives a warning so that a mistake can be avoided
        [syn: {admonisher}, {reminder}]
     4: an iron-clad vessel built by Federal forces to do battle
        with the Merrimac
     5: electronic equipment that is used to check the quality or
        content of electronic transmissions
     6: a piece of electronic equipment that keeps track of the
        operation of a system continuously and warns of trouble
     7: any of various large ...
