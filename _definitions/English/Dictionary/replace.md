---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/replace
offline_file: ""
offline_thumbnail: ""
uuid: 2d9aae80-1030-4fc7-ab62-2ee10caf5c96
updated: 1484310258
title: replace
categories:
    - Dictionary
---
replace
     v 1: substitute a person or thing for (another that is broken or
          inefficient or lost or no longer working or yielding
          what is expected); "He replaced the old razor blade";
          "We need to replace the secretary that left a month
          ago"; "the insurance will replace the lost income";
          "This antique vase can never be replaced"
     2: take the place or move into the position of; "Smith replaced
        Miller as CEO after Miller left"; "the computer has
        supplanted the slide rule"; "Mary replaced Susan as the
        team's captain and the highest-ranked player in the
        school" [syn: {supplant}, {supersede}, {supervene upon}]
 ...
