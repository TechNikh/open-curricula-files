---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pinch
offline_file: ""
offline_thumbnail: ""
uuid: 1bb41239-260a-4419-921b-d195325f3811
updated: 1484310344
title: pinch
categories:
    - Dictionary
---
pinch
     n 1: a painful or straitened circumstance; "the pinch of the
          recession"
     2: an injury resulting from getting some body part squeezed
     3: a slight but appreciable addition; "this dish could use a
        touch of garlic" [syn: {touch}, {hint}, {tinge}, {mite}, {jot},
         {speck}, {soupcon}]
     4: a sudden unforeseen crisis (usually involving danger) that
        requires immediate action; "he never knew what to do in an
        emergency" [syn: {emergency}, {exigency}]
     5: small sharp biting [syn: {nip}]
     6: a squeeze with the fingers [syn: {tweak}]
     7: the act of apprehending (especially apprehending a
        criminal); "the policeman on the ...
