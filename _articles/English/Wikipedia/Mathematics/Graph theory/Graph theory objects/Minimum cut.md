---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimum_cut
offline_file: ""
offline_thumbnail: ""
uuid: 6a5624e0-8bb9-432f-95e0-7be085876c82
updated: 1484309443
title: Minimum cut
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Min_cut_example.svg_0.png
tags:
    - Number of minimum cuts
categories:
    - Graph theory objects
---
[1] In graph theory, a minimum cut of a graph is a cut (a partition of the vertices of a graph into two disjoint subsets that are joined by at least one edge) that is minimal in some sense. Variations of the minimum cut include:
