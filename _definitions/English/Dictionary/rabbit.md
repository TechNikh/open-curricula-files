---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rabbit
offline_file: ""
offline_thumbnail: ""
uuid: 25da4d2e-5d6f-4f68-80e8-67086c35ed43
updated: 1484310283
title: rabbit
categories:
    - Dictionary
---
rabbit
     n 1: any of various burrowing animals of the family Leporidae
          having long ears and short tails; some domesticated and
          raised for pets or food [syn: {coney}, {cony}]
     2: the fur of a rabbit [syn: {lapin}]
     3: flesh of any of various rabbits or hares (wild or
        domesticated) eaten as food [syn: {hare}]
     v : hunt rabbits
