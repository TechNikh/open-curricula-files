---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amp
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484352121
title: amp
categories:
    - Dictionary
---
amp
     n 1: the basic unit of electric current adopted under the Systeme
          International d'Unites; "a typical household circuit
          carries 15 to 50 amps" [syn: {ampere}, {A}]
     2: a nucleotide found in muscle cells and important in
        metabolism; reversibly convertible to ADP and ATP [syn: {adenosine
        monophosphate}, {adenylic acid}]
