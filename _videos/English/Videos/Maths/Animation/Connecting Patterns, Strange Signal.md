---
version: 1
type: video
provider: YouTube
id: bTAUHjFdSIY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fe8e129b-46f0-47c2-a24b-602e311bb7a7
updated: 1486069644
title: Connecting Patterns, Strange Signal
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bTAUHjFdSIY/default.jpg
    - https://i3.ytimg.com/vi/bTAUHjFdSIY/1.jpg
    - https://i3.ytimg.com/vi/bTAUHjFdSIY/2.jpg
    - https://i3.ytimg.com/vi/bTAUHjFdSIY/3.jpg
---
