---
version: 1
type: video
provider: YouTube
id: XZDGrbyz0v0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Approximating%20functions%20with%20polynomials%20%28part%203%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7f93dc7e-1789-4743-8534-3013267a6734
updated: 1486069694
title: Approximating functions with polynomials (part 3)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/XZDGrbyz0v0/default.jpg
    - https://i3.ytimg.com/vi/XZDGrbyz0v0/1.jpg
    - https://i3.ytimg.com/vi/XZDGrbyz0v0/2.jpg
    - https://i3.ytimg.com/vi/XZDGrbyz0v0/3.jpg
---
