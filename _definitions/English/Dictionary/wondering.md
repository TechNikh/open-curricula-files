---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wondering
offline_file: ""
offline_thumbnail: ""
uuid: 9b557a8f-c1b2-4684-958e-6af70a113bd9
updated: 1484310154
title: wondering
categories:
    - Dictionary
---
wondering
     adj : showing curiosity; "if someone saw a man climbing a light
           post they might get inquisitive"; "raised a speculative
           eyebrow" [syn: {inquisitive}, {speculative}, {questioning},
            {wondering(a)}]
