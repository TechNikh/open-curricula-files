---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thirsty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479141
title: thirsty
categories:
    - Dictionary
---
thirsty
     adj 1: needing moisture; "thirsty fields under a rainless sky"
     2: feeling a need or desire to drink; "after playing hard the
        children were thirsty" [ant: {hungry}]
     3: (usually followed by `for') extremely desirous; "athirst for
        knowledge"; "hungry for recognition"; "thirsty for
        informaton" [syn: {athirst(p)}, {hungry(p)}, {thirsty(p)}]
     4: able to take in large quantities of moisture; "thirsty
        towels"
     [also: {thirstiest}, {thirstier}]
