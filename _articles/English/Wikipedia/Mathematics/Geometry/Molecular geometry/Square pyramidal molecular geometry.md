---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Square_pyramidal_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: efc60586-a2d5-45ed-b894-c093a518867a
updated: 1484309280
title: Square pyramidal molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Xenon-oxytetrafluoride-3D-vdW.png
tags:
    - As a transition state in Berry Pseudorotation
    - Examples
categories:
    - Molecular geometry
---
In molecular geometry, square based pyramidal geometry describes the shape of certain compounds with the formula ML5 where L is a ligand. If the ligand atoms were connected, the resulting shape would be that of a pyramid with a square base. The point group symmetry involved is of type C4v. The geometry is common for certain main group compounds that have a stereochemically active lone pair, as described by VSEPR theory. Certain compounds crystallize in both the trigonal bipyramidal and the square pyramidal structures, notably [Ni(CN)5]3−.[1]
