---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surmounted
offline_file: ""
offline_thumbnail: ""
uuid: 6b967ba6-e742-4cbe-9f8b-385c4dc28b44
updated: 1484310156
title: surmounted
categories:
    - Dictionary
---
surmounted
     adj : having something on top; "columns surmounted by statues"
