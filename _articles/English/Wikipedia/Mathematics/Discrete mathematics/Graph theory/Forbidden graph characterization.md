---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Forbidden_graph_characterization
offline_file: ""
offline_thumbnail: ""
uuid: b9fda97f-5a97-4e72-b70b-f36f98764252
updated: 1484308622
title: Forbidden graph characterization
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, many important families of graphs can be described by a finite set of individual graphs that do not belong to the family and further exclude all graphs from the family which contain any of these forbidden graphs as (induced) subgraph or minor. A prototypical example of this phenomenon is Kuratowski's theorem, which states that a graph is planar (can be drawn without crossings in the plane) if and only if it does not contain either of two forbidden graphs, the complete graph K5 and the complete bipartite graph K3,3. For Kuratowski's theorem, the notion of containment is that of graph homeomorphism, in which a subdivision of one graph appears as a ...
