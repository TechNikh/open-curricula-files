---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Klein_configuration
offline_file: ""
offline_thumbnail: ""
uuid: c729f14b-e0bc-4c32-a37a-760a73207b05
updated: 1484309156
title: Klein configuration
categories:
    - Algebraic geometry
---
In geometry, the Klein configuration, studied by Klein (1870), is a geometric configuration related to Kummer surfaces that consists of 60 points and 60 planes, with each point lying on 15 planes and each plane passing through 15 points.
