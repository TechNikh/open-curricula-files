---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Weller%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 985a91be-0463-46a0-9798-f1abb02b9c32
updated: 1484308513
title: "Weller's theorem"
tags:
    - Background
    - Notation
    - Proof Sketch
    - Calculating the price measure
    - Example
    - Generalizations and extensions
    - Algorithms
    - Limitations
categories:
    - Fair division
---
Weller's theorem[1] is a theorem in economics. It says that a heterogeneous resource ("cake") can be divided among n partners with different valuations in a way that is both Pareto-efficient (PE) and envy-free (EF). Thus, it is possible to divide a cake fairly without compromising on economic efficiency.
