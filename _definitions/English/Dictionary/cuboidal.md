---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cuboidal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484361961
title: cuboidal
categories:
    - Dictionary
---
cuboidal
     adj : shaped like a cube [syn: {cubelike}, {cube-shaped}, {cubical},
            {cubiform}, {cuboid}]
