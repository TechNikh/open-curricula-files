---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Peter_L._Hurd
offline_file: ""
offline_thumbnail: ""
uuid: e14a505e-2265-49ac-825a-8576b2571f82
updated: 1484308509
title: Peter L. Hurd
tags:
    - Research
    - Evolution of animal signalling
    - Aggressiveness
    - Digit ratio
    - Academic history
categories:
    - Game theorists
---
Peter L. Hurd is an academic specialising in biology. He is an Associate Professor aligned to the Department of Psychology's Biocognition Unit, and the University's Centre for Neuroscience at the University of Alberta. His research primarily focuses on the study of the evolution of aggressive behaviour, including investigation of aggression, communication and other social behaviour which takes place between animals with conflicting interests. Major tools for this research are mathematical modeling (principally game theory and genetic algorithms). He is also interested in how the process of sexual differentiation produces individual differences in social behaviour.
