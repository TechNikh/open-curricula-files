---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supporting_hyperplane
offline_file: ""
offline_thumbnail: ""
uuid: a909e5db-c1af-4fbc-b214-304b761195d3
updated: 1484309193
title: Supporting hyperplane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Supporting_hyperplane1.svg.png
categories:
    - Convex geometry
---
In geometry, a supporting hyperplane of a set 
  
    
      
        S
      
    
    {\displaystyle S}
  
 in Euclidean space 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 is a hyperplane that has both of the following two properties:
