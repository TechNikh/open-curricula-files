---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flood
offline_file: ""
offline_thumbnail: ""
uuid: bb8845f6-a29f-4b2d-a941-42781dc4b657
updated: 1484310461
title: flood
categories:
    - Dictionary
---
flood
     n 1: the rising of a body of water and its overflowing onto
          normally dry land; "plains fertilized by annual
          inundations" [syn: {inundation}, {deluge}, {alluvion}]
     2: an overwhelming number or amount; "a flood of requests"; "a
        torrent of abuse" [syn: {inundation}, {deluge}, {torrent}]
     3: light that is a source of artificial illumination having a
        broad beam; used in photography [syn: {floodlight}, {flood
        lamp}, {photoflood}]
     4: a large flow [syn: {overflow}, {outpouring}]
     5: the act of flooding; filling to overflowing [syn: {flowage}]
     6: the inward flow of the tide; "a tide in the affairs of men
        which, ...
