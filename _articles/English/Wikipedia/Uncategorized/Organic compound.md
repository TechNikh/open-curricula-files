---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Organic_molecules
offline_file: ""
offline_thumbnail: ""
uuid: 34dd4b55-4ea3-46eb-a895-85b230f63c71
updated: 1484308372
title: Organic compound
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Methane-2D-stereo.svg.png
tags:
    - History
    - Vitalism
    - Modern classification
    - Classification
    - Natural compounds
    - Synthetic compounds
    - Biotechnology
    - Nomenclature
    - Databases
    - Structure determination
categories:
    - Uncategorized
---
An organic compound is any member of a large class of gaseous, liquid, or solid chemical compounds whose molecules contain carbon. For historical reasons discussed below, a few types of carbon-containing compounds, such as carbides, carbonates, simple oxides of carbon (such as CO and CO2), and cyanides are considered inorganic.[1] The distinction between organic and inorganic carbon compounds, while "useful in organizing the vast subject of chemistry... is somewhat arbitrary".[2]
