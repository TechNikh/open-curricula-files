---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breakfast
offline_file: ""
offline_thumbnail: ""
uuid: 47bdde33-a3f5-4d96-bc9f-5c10daf23fba
updated: 1484310313
title: breakfast
categories:
    - Dictionary
---
breakfast
     n : the first meal of the day (usually in the morning)
     v 1: eat an early morning meal; "We breakfast at seven"
     2: provide breakfast for
