---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_in_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: e8b70ce2-8b4f-4ccc-806f-90625157ed33
updated: 1484309317
title: Symmetry in mathematics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-E8Petrie.svg.png
tags:
    - Symmetry in geometry
    - Symmetry in calculus
    - Even and odd functions
    - Even functions
    - Odd functions
    - Integrating
    - Series
    - Symmetry in linear algebra
    - Symmetry in matrices
    - Symmetry in abstract algebra
    - Symmetric groups
    - Symmetric polynomials
    - Examples
    - Symmetric tensors
    - Galois theory
    - Automorphisms of algebraic objects
    - Examples
    - Symmetry in representation theory
    - 'Symmetry in quantum mechanics: bosons and fermions'
    - Symmetry in set theory
    - Symmetric relation
    - Symmetry in metric spaces
    - Isometries of a space
    - Symmetries of differential equations
    - Symmetry in probability
    - Bibliography
categories:
    - Symmetry
---
Symmetry occurs not only in geometry, but also in other branches of mathematics. Symmetry is a type of invariance: the property that something does not change under a set of transformations.
