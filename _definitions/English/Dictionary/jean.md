---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jean
offline_file: ""
offline_thumbnail: ""
uuid: c494549e-102b-4298-934f-77fc3221deab
updated: 1484310293
title: jean
categories:
    - Dictionary
---
jean
     n 1: (usually plural) close-fitting pants of heavy denim for
          casual wear [syn: {blue jean}, {denim}]
     2: a coarse durable twill-weave cotton fabric [syn: {denim}, {dungaree}]
