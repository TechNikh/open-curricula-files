---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/totalitarianism
offline_file: ""
offline_thumbnail: ""
uuid: c21ff8c4-c887-487d-b753-adc6c6b03f2c
updated: 1484310597
title: totalitarianism
categories:
    - Dictionary
---
totalitarianism
     n 1: a form of government in which the ruler is an absolute
          dictator (not restricted by a constitution or laws or
          opposition etc.) [syn: {dictatorship}, {absolutism}, {authoritarianism},
           {Caesarism}, {despotism}, {monocracy}, {one-man rule},
          {shogunate}, {Stalinism}, {tyranny}]
     2: the principle of complete and unrestricted power in
        government [syn: {absolutism}, {totalism}]
