---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Birkhoff%27s_theorem_(electromagnetism)'
offline_file: ""
offline_thumbnail: ""
uuid: b091b52a-a03d-4bca-9773-353821813de9
updated: 1484308751
title: "Birkhoff's theorem (electromagnetism)"
categories:
    - Electrodynamics
---
In physics, in the context of electromagnetism, Birkhoff's theorem concerns spherically symmetric static solutions of Maxwell's field equations of electromagnetism.
