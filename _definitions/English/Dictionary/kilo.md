---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kilo
offline_file: ""
offline_thumbnail: ""
uuid: e3e754e3-7c4e-4c1d-bebb-b50ccdf31bb6
updated: 1484310196
title: kilo
categories:
    - Dictionary
---
kilo
     n : one thousand grams; the basic unit of mass adopted under the
         Systeme International d'Unites; "a kilogram is
         approximately 2.2 pounds" [syn: {kilogram}, {kg}]
