---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Punjab
offline_file: ""
offline_thumbnail: ""
uuid: 25bc1406-5a7d-4a05-9bac-41905ae7a6db
updated: 1484308419
title: Punjab (region)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pope1880Panjab3.jpg
tags:
    - Etymology
    - Physical geography
    - Climate
    - History
    - Timeline
    - People of the Punjab
    - Ethnic background
    - Languages
    - Religions
    - Punjabi festivals
    - Punjabi clothing
    - Economy
    - Photo gallery
    - Notes
categories:
    - Uncategorized
---
The Punjab (i/pʌnˈdʒɑːb/, /ˈpʌndʒɑːb/, /pʌnˈdʒæb/, /ˈpʌndʒæb/), also spelled Panjab, panj-āb, land of "five rivers"[1] (Punjabi: پنجاب (Shahmukhi); ਪੰਜਾਬ (Gurumukhi)), is a geographical and cultural region in the northern part of South Asia, comprising areas of eastern Pakistan and northern India. Not being a political unit, the extent of the region is the subject of debate and focuses on historical events to determine its boundaries.
