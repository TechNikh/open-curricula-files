---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spongy
offline_file: ""
offline_thumbnail: ""
uuid: afe74c50-d6dd-46a1-9a51-5b019ee7d0ec
updated: 1484310387
title: spongy
categories:
    - Dictionary
---
spongy
     adj 1: resembling a sponge in having soft porous texture and
            compressibility; "spongy bread" [syn: {squashy}, {squishy},
             {spongelike}]
     2: like a sponge in being able to absorb liquids and yield it
        back when compressed [syn: {spongelike}]
     [also: {spongiest}, {spongier}]
