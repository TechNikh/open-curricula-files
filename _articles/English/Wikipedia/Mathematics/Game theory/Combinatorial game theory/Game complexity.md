---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Game_complexity
offline_file: ""
offline_thumbnail: ""
uuid: 0a94f150-2429-40fa-a185-f04e23613520
updated: 1484308535
title: Game complexity
tags:
    - Measures of game complexity
    - State-space complexity
    - Game tree size
    - Decision trees
    - Decision complexity
    - Game-tree complexity
    - Computational complexity
    - 'Example: tic-tac-toe (noughts and crosses)'
    - Complexities of some well-known games
    - Notes and references
categories:
    - Combinatorial game theory
---
Combinatorial game theory has several ways of measuring game complexity. This article describes five of them: state-space complexity, game tree size, decision complexity, game-tree complexity, and computational complexity.
