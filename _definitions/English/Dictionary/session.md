---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/session
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484577481
title: session
categories:
    - Dictionary
---
session
     n 1: a meeting for execution of a group's functions; "it was the
          opening session of the legislature"
     2: the time during which a school holds classes; "they had to
        shorten the school term" [syn: {school term}, {academic
        term}, {academic session}]
     3: a meeting devoted to a particular activity; "a filming
        session"; "a gossip session"
     4: a meeting of spiritualists; "the seance was held in the
        medium's parlor" [syn: {seance}, {sitting}]
