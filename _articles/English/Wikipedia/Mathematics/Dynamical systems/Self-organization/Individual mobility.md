---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Individual_mobility
offline_file: ""
offline_thumbnail: ""
uuid: 6c3d135c-3051-446c-b0c9-79f41018ac8d
updated: 1484308819
title: Individual mobility
tags:
    - Data
    - characteristics
    - Predictability
    - Applications
categories:
    - Self-organization
---
As a part of Network science[citation needed], individual human mobility is an emergent field which is dedicated to extracting patterns that govern human movements. Understanding human mobility has many applications in diverse areas, including spread of diseases,[1][2] mobile viruses,[3] city planning,[4][5][6] traffic engineering,[7][8] financial market forecasting,[9] and nowcasting of economic well-being.[10][11]
