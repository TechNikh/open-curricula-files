---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ricardo_Ma%C3%B1%C3%A9'
offline_file: ""
offline_thumbnail: ""
uuid: 770b49e7-d303-44a9-8e1e-6169ef46fe08
updated: 1484308684
title: Ricardo Mañé
categories:
    - Dynamical systems theorists
---
Ricardo Mañé Ramirez (Montevideo, 14 January 1948 – Montevideo, 9 March 1995) was a Uruguayan and Brazilian mathematician, known for his contributions to dynamical systems and ergodic theory. He was a doctoral student of Jacob Palis at IMPA.[1]
