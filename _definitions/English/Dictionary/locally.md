---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/locally
offline_file: ""
offline_thumbnail: ""
uuid: 147bcceb-5e0d-4549-92ce-143c8cb21182
updated: 1484310439
title: locally
categories:
    - Dictionary
---
locally
     adv 1: by a particular locality; "it was locally decided"
     2: to a restricted area of the body; "apply this medicine
        topically" [syn: {topically}]
