---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baking
offline_file: ""
offline_thumbnail: ""
uuid: 8264e748-a4aa-4bbc-9fb1-ed9a28489818
updated: 1484310379
title: baking
categories:
    - Dictionary
---
baking
     adj : as hot as if in an oven [syn: {baking hot}]
     n 1: making bread or cake or pastry etc.
     2: cooking by dry heat in an oven
