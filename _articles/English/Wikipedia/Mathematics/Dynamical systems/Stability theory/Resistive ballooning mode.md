---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resistive_ballooning_mode
offline_file: ""
offline_thumbnail: ""
uuid: 4d414850-c875-4736-adb6-1ad967ad6cb0
updated: 1484308832
title: Resistive ballooning mode
categories:
    - Stability theory
---
The resistive ballooning mode (RBM) is an instability occurring in magnetized plasmas, particularly in magnetic confinement devices such as tokamaks, when the pressure gradient is opposite to the effective gravity created by a magnetic field.
