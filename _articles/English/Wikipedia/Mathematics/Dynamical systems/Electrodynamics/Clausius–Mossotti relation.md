---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Clausius%E2%80%93Mossotti_relation'
offline_file: ""
offline_thumbnail: ""
uuid: 948dc3a2-d3c6-464c-a0e3-c2ec9233a579
updated: 1484308749
title: Clausius–Mossotti relation
categories:
    - Electrodynamics
---
The Clausius–Mossotti relation expresses the dielectric constant (relative permittivity) εr of a material in terms of the atomic polarizibility α of the material's constituent atoms and/or molecules, or a homogeneous mixture thereof. It is named after Ottaviano-Fabrizio Mossotti and Rudolf Clausius. It is equivalent to the Lorentz–Lorenz equation. It may be expressed as:[1][2]
