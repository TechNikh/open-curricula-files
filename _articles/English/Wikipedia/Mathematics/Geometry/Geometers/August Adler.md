---
version: 1
type: article
id: https://en.wikipedia.org/wiki/August_Adler
offline_file: ""
offline_thumbnail: ""
uuid: fc55d6c3-8de9-404d-b9ae-7f75b706666a
updated: 1484309069
title: August Adler
categories:
    - Geometers
---
August Adler (24 January 1863, Opava, Austrian Silesia – 17 October 1923, Vienna) was a Czech and Austrian mathematician noted for using the theory of inversion to provide an alternate proof of Mascheroni's compass and straightedge construction theorem.
