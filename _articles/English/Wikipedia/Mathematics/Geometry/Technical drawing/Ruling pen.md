---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ruling_pen
offline_file: ""
offline_thumbnail: ""
uuid: ad68ec62-3833-43fe-98ca-8e9cbb0233ff
updated: 1484309332
title: Ruling pen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Reissfeder_1.jpg
categories:
    - Technical drawing
---
A ruling pen contains ink in a slot between two flexible metal jaws, which are tapered to a point. It enables precise rendering of the thinnest lines.[1] The line width can be adjusted by an adjustment screw connecting the jaws.[2] The adjustment screw can optionally have a number dial.
