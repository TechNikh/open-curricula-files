---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maryam_Mirzakhani
offline_file: ""
offline_thumbnail: ""
uuid: bc114416-05c6-4982-a6aa-66bf9d7ed912
updated: 1484308692
title: Maryam Mirzakhani
tags:
    - Early life and education
    - Research work
    - Personal life
    - Awards and honors
categories:
    - Dynamical systems theorists
---
Maryam Mirzakhani (Persian: مریم میرزاخانی‎‎; born May 13, 1977) is an Iranian-American mathematician and a professor of mathematics at Stanford University.[5][6][7]
