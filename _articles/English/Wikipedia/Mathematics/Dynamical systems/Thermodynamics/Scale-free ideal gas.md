---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scale-free_ideal_gas
offline_file: ""
offline_thumbnail: ""
uuid: dd5cf592-8480-45f8-b598-fa816c42d062
updated: 1484308859
title: Scale-free ideal gas
categories:
    - Thermodynamics
---
The scale-free ideal gas (SFIG) is a physical model assuming a collection of non-interacting elements with an stochastic proportional growth. It is the scale-invariant version of an ideal gas. Some cases of city-population, electoral results and cites to scientific journals can be approximately considered scale-free ideal gases.[1]
