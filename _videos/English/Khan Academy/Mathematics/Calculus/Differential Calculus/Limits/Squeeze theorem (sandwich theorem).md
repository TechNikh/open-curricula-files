---
version: 1
type: video
provider: YouTube
id: igJdDN-DPgA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Limits/Squeeze%20theorem%20%28sandwich%20theorem%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bad8f6fc-1f91-4c69-a021-7aeb1e948da4
updated: 1486069693
title: Squeeze theorem (sandwich theorem)
categories:
    - Limits
thumbnail_urls:
    - https://i3.ytimg.com/vi/igJdDN-DPgA/default.jpg
    - https://i3.ytimg.com/vi/igJdDN-DPgA/1.jpg
    - https://i3.ytimg.com/vi/igJdDN-DPgA/2.jpg
    - https://i3.ytimg.com/vi/igJdDN-DPgA/3.jpg
---
