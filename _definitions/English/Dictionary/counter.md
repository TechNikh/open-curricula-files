---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/counter
offline_file: ""
offline_thumbnail: ""
uuid: 4d1bc1db-1491-476a-b1b4-a92fce8abaa0
updated: 1484310565
title: counter
categories:
    - Dictionary
---
counter
     adj : indicating opposition or resistance [syn: {antagonistic}]
     n 1: table consisting of a horizontal surface over which business
          is transacted
     2: game equipment used in various card or board games
     3: a calculator that keeps a record of the number of times
        something happens [syn: {tabulator}]
     4: a piece of furniture that stands at the side of a dining
        room; has shelves and drawers [syn: {buffet}, {sideboard}]
     5: a person who counts things
     6: a quick reply to a question or remark (especially a witty or
        critical one); "it brought a sharp rejoinder from the
        teacher" [syn: {rejoinder}, {retort}, {return}, ...
