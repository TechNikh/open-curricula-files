---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_copper_pillar_bump
offline_file: ""
offline_thumbnail: ""
uuid: 7ae03ec8-6d5e-4995-9d0a-a96e8053e135
updated: 1484308868
title: Thermal copper pillar bump
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thermal-bump-substrate.png
tags:
    - A brief history of solder and flip chip/chip scale packaging
    - Copper pillar solder bumping
    - Thin-film thermoelectric technology
    - Thermal copper pillar bump
    - Thermal copper pillar bump structure
    - Applications
    - General cooling
    - Precision temperature control
    - Hotspot cooling
    - Power generation
    - White Papers, Articles and Application Notes
categories:
    - Thermodynamics
---
The thermal copper pillar bump, also known as the "thermal bump", is a thermoelectric device made from thin-film thermoelectric material embedded in flip chip interconnects (in particular copper pillar solder bumps) for use in electronics and optoelectronic packaging, including: flip chip packaging of CPU and GPU integrated circuits (chips), laser diodes, and semiconductor optical amplifiers (SOA). Unlike conventional solder bumps that provide an electrical path and a mechanical connection to the package, thermal bumps act as solid-state heat pumps and add thermal management functionality locally on the surface of a chip or to another electrical component. The diameter of a thermal bump is ...
