---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wave
offline_file: ""
offline_thumbnail: ""
uuid: 217b515d-f0bb-43dc-8dcd-e81611a383f1
updated: 1484310335
title: wave
categories:
    - Dictionary
---
wave
     n 1: one of a series of ridges that moves across the surface of a
          liquid (especially across a large body of water) [syn: {moving
          ridge}]
     2: a movement like that of an ocean wave; "a wave of settlers";
        "troops advancing in waves"
     3: (physics) a movement up and down or back and forth [syn: {undulation}]
     4: something that rises rapidly; "a wave of emotion swept over
        him"; "there was a sudden wave of buying before the market
        closed"; "a wave of conservatism in the country led by the
        hard right"
     5: the act of signaling by a movement of the hand [syn: {waving},
         {wafture}]
     6: a hairdo that creates ...
