---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forefront
offline_file: ""
offline_thumbnail: ""
uuid: f1fc0aab-60f3-4803-aa1d-1c26db1fad09
updated: 1484310189
title: forefront
categories:
    - Dictionary
---
forefront
     n 1: the part in the front or nearest the viewer; "he was in the
          forefront"; "he was at the head of the column" [syn: {head}]
     2: the position of greatest advancement; the leading position
        in any movement or field [syn: {vanguard}, {cutting edge}]
