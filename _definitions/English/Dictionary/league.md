---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/league
offline_file: ""
offline_thumbnail: ""
uuid: c47b2ffa-addd-4f79-ade0-344d95b0c4e9
updated: 1484310555
title: league
categories:
    - Dictionary
---
league
     n 1: an association of sports teams that organizes matches for
          its members [syn: {conference}]
     2: an association of states or organizations or individuals for
        common action
     3: an obsolete unit of distance of variable length (usually 3
        miles)
     v : unite to form a league
