---
version: 1
type: article
id: https://en.wikipedia.org/wiki/L(2,1)-coloring
offline_file: ""
offline_thumbnail: ""
uuid: 1d13dddf-11ff-48a1-9754-16a501c1162a
updated: 1484309386
title: L(2,1)-coloring
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-L%25282%252C1%2529-coloring_of_C6.png'
categories:
    - Graph coloring
---
L(2, 1)-coloring is a particular case of L(h, k)-coloring which is in fact a proper coloring. In L(2, 1)-coloring of a graph, G, the vertices of the graph G is colored or labelled in such a way that the adjacent vertices get labels that differ by at least two. Also the vertices that are at a distance of two from each other get labels that differ by at least one.[1]
