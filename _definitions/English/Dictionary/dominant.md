---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominant
offline_file: ""
offline_thumbnail: ""
uuid: 62eb9b8a-df56-4506-95c5-b66cfd9ce80b
updated: 1484310301
title: dominant
categories:
    - Dictionary
---
dominant
     adj 1: exercising influence or control; "television plays a
            dominant role in molding public opinion"; "the
            dominant partner in the marriage" [ant: {subordinate}]
     2: of genes; producing the same phenotype whether its allele is
        identical or dissimilar [ant: {recessive}]
     n : (music) the fifth note of the diatonic scale
