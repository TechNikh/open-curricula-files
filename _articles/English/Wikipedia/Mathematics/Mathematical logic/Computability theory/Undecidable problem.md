---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Undecidable_problem
offline_file: ""
offline_thumbnail: ""
uuid: 97efd434-cdfe-49c2-8cf4-f4c6a431b82e
updated: 1484309492
title: Undecidable problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_22.png
tags:
    - In computability theory
    - "Relationship with Gödel's incompleteness theorem"
    - Examples of undecidable problems
    - Examples of undecidable statements
categories:
    - Computability theory
---
In computability theory and computational complexity theory, an undecidable problem is a decision problem for which it is known to be impossible to construct a single algorithm that always leads to a correct yes-or-no answer.
