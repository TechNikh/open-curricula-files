---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retired
offline_file: ""
offline_thumbnail: ""
uuid: 82b3f5aa-6ac3-4c87-a0e6-ac46614c63b6
updated: 1484310469
title: retired
categories:
    - Dictionary
---
retired
     adj 1: no longer active in your work or profession
     2: honorably retired from assigned duties and retaining your
        title along with the additional title `emeritus' as in
        `professor emeritus'; `retired from assigned duties' need
        not imply that one is inactive [syn: {emeritus}]
     3: not allowed to continue to bat or run; "he was tagged out at
        second on a close play"; "he fanned out" [syn: {out(p)}]
        [ant: {safe(p)}]
     4: (of a ship) withdrawn from active service; "the ship was
        placed out of service after the war" [syn: {out of service}]
     5: discharged as too old for use or work; especially with a
        pension; "a ...
