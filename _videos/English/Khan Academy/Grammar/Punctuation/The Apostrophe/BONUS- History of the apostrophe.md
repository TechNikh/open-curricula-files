---
version: 1
type: video
provider: YouTube
id: u-FSLjbuv_s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/BONUS-%20History%20of%20the%20apostrophe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6f88bf50-5feb-4232-bc84-9609b63bfc68
updated: 1486069599
title: 'BONUS- History of the apostrophe'
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/u-FSLjbuv_s/default.jpg
    - https://i3.ytimg.com/vi/u-FSLjbuv_s/1.jpg
    - https://i3.ytimg.com/vi/u-FSLjbuv_s/2.jpg
    - https://i3.ytimg.com/vi/u-FSLjbuv_s/3.jpg
---
The apostrophe has a bizarre history, including being the name for something that's not really related to grammar at all. David, Paige, and special guest Jake explain.


Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/bonus-history-of-the-possessive-apostrophe 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/choosing-between-its-and-its

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
