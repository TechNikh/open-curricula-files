---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ice
offline_file: ""
offline_thumbnail: ""
uuid: e61c2ba4-0941-491f-97e5-818403fd00f8
updated: 1484310273
title: ice
categories:
    - Dictionary
---
ice
     n 1: water frozen in the solid state; "Americans like ice in
          their drinks" [syn: {water ice}]
     2: the frozen part of a body of water
     3: diamonds; "look at the ice on that dame!" [syn: {sparkler}]
     4: a flavored sugar topping used to coat and decorate cakes
        [syn: {frosting}, {icing}]
     5: a frozen dessert with fruit flavoring (especially one
        containing no milk) [syn: {frappe}]
     6: amphetamine used in the form of a crystalline hydrochloride;
        used as a stimulant to the nervous system and as an
        appetite suppressant [syn: {methamphetamine}, {methamphetamine
        hydrochloride}, {Methedrine}, {meth}, {deoxyephedrine}, ...
