---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orientation_of_churches
offline_file: ""
offline_thumbnail: ""
uuid: e3fd9b4d-6eb6-4b82-9014-bb4a5bdc7cab
updated: 1484309297
title: Orientation of churches
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Church-plan.png
tags:
    - History
    - Inexactitude of orientation
categories:
    - Orientation (geometry)
---
Orientation of churches is the architectural feature by which the point of main interest in the interior, where the altar is placed, often within an apse, is towards the east (Latin: oriens). The main entrance is accordingly placed at the west end.
