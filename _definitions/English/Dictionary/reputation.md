---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reputation
offline_file: ""
offline_thumbnail: ""
uuid: f051515a-6a91-4f0a-9a90-a4dd96963302
updated: 1484310166
title: reputation
categories:
    - Dictionary
---
reputation
     n 1: the state of being held in high esteem and honor [syn: {repute}]
          [ant: {disrepute}]
     2: notoriety for some particular characteristic; "his
        reputation for promiscuity"
     3: the general estimation that the public has for a person; "he
        acquired a reputation as an actor before he started
        writing"; "he was a person of bad report" [syn: {report}]
