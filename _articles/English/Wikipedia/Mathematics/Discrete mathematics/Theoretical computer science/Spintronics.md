---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spintronics
offline_file: ""
offline_thumbnail: ""
uuid: 2f68c119-ed56-438a-a9a4-4cd25a35717c
updated: 1484308675
title: Spintronics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Spin_Injection.jpg
tags:
    - History
    - Theory
    - Metal-based devices
    - Spintronic-logic devices
    - Applications
    - Semiconductor-based spintronic devices
    - Applications
    - Storage media
categories:
    - Theoretical computer science
---
Spintronics (a portmanteau meaning spin transport electronics[1][2][3]), also known as spinelectronics or fluxtronics, is the study of the intrinsic spin of the electron and its associated magnetic moment, in addition to its fundamental electronic charge, in solid-state devices.
