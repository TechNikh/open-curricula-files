---
version: 1
type: video
provider: YouTube
id: IOXxFaHbIXg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/How%20Molecules%20are%20formed%20-%20-%20Animated%20Lesson%20for%20kids.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e73b5a04-9c33-40c9-a3b5-84c8e93b9fac
updated: 1486069664
title: 'How Molecules are formed ? - Animated Lesson for kids'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/IOXxFaHbIXg/default.jpg
    - https://i3.ytimg.com/vi/IOXxFaHbIXg/1.jpg
    - https://i3.ytimg.com/vi/IOXxFaHbIXg/2.jpg
    - https://i3.ytimg.com/vi/IOXxFaHbIXg/3.jpg
---
