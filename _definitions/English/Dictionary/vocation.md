---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vocation
offline_file: ""
offline_thumbnail: ""
uuid: 2bc5ff48-490f-495f-9a0c-d3e6580d273d
updated: 1484310599
title: vocation
categories:
    - Dictionary
---
vocation
     n 1: the particular occupation for which you are trained [syn: {career},
           {calling}]
     2: a body of people doing the same kind of work [syn: {occupational
        group}]
