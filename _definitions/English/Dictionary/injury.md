---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/injury
offline_file: ""
offline_thumbnail: ""
uuid: c7ccca9c-1f7e-407c-a229-28f3b5f05fd7
updated: 1484310455
title: injury
categories:
    - Dictionary
---
injury
     n 1: any physical damage to the body caused by violence or
          accident or fracture etc. [syn: {hurt}, {harm}, {trauma}]
     2: an accident that results in physical damage or hurt [syn: {accidental
        injury}]
     3: a casualty to military personnel resulting from combat [syn:
         {wound}, {combat injury}]
     4: an act that injures someone
