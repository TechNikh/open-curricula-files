---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crore
offline_file: ""
offline_thumbnail: ""
uuid: 65bddf34-9030-4374-8430-88027bf17c95
updated: 1484310517
title: crore
categories:
    - Dictionary
---
crore
     n : the number that is represented as a one followed by 7 zeros;
         ten million
