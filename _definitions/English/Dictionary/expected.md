---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expected
offline_file: ""
offline_thumbnail: ""
uuid: 1df2df9e-431d-4e10-a19a-06173d675384
updated: 1484310313
title: expected
categories:
    - Dictionary
---
expected
     adj 1: considered likely or probable to happen or arrive; "prepared
            for the expected attack" [ant: {unexpected}]
     2: looked forward to as probable
     3: expected to become or be; in prospect; "potential clients";
        "expected income" [syn: {likely}, {potential}]
