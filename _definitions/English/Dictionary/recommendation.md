---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recommendation
offline_file: ""
offline_thumbnail: ""
uuid: 72590c3b-2f79-4e40-9a26-ec8661be2f7e
updated: 1484310397
title: recommendation
categories:
    - Dictionary
---
recommendation
     n 1: something (as a course of action) that is recommeended as
          advisable
     2: something that recommends (or expresses commendation) of a
        person or thing as worthy or desirable [syn: {testimonial},
         {good word}]
     3: any quality or characteristic that gains a person a
        favorable reception or acceptance or admission; "her
        pleasant personality is already a recommendation"; "his
        wealth was not a passport into the exclusive circles of
        society" [syn: {passport}]
