---
version: 1
type: video
provider: YouTube
id: ZrMltpK6iAw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1995d7fe-8d09-4535-98c4-fd80ec10ff1a
updated: 1486069682
title: 'Voltage, Electric Energy, and Capacitors- Crash Course Physics #27'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZrMltpK6iAw/default.jpg
    - https://i3.ytimg.com/vi/ZrMltpK6iAw/1.jpg
    - https://i3.ytimg.com/vi/ZrMltpK6iAw/2.jpg
    - https://i3.ytimg.com/vi/ZrMltpK6iAw/3.jpg
---
