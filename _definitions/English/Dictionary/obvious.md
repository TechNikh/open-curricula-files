---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obvious
offline_file: ""
offline_thumbnail: ""
uuid: e59df5cb-0b32-4e58-8834-1df9e870f234
updated: 1484310443
title: obvious
categories:
    - Dictionary
---
obvious
     adj 1: easily perceived or understood; "obvious errors" [ant: {unobvious}]
     2: easily perceived by the senses or grasped by the mind; "a
        perceptible sense of expectation in the court"; "an
        obvious (or palpable) lie" [syn: {perceptible}]
     3: obvious to the eye or mind; "a tower conspicuous at a great
        distance"; "wore conspicuous neckties"; "made herself
        conspicuous by her exhibitionistic preening" [syn: {conspicuous}]
        [ant: {inconspicuous}]
     4: obvious to the eye; "a visible change of expression" [syn: {visible}]
