---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonmetals
offline_file: ""
offline_thumbnail: ""
uuid: ed9decd7-eeee-42e4-b289-99573e35e990
updated: 1484308384
title: Nonmetal
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Periodic_table_%2528polyatomic%2529.svg_2.png'
tags:
    - Definition and properties
    - Applicable elements
    - Categories
    - Polyatomic nonmetals
    - Diatomic nonmetals
    - Noble gases
    - Elemental gases
    - Organogens, CHONPS and biogens
    - Other nonmetals
    - Comparison of properties
    - Allotropes
    - Abundance and extraction
    - Applications in common
    - discovery
    - 'Antiquity: C, S'
    - '17th century: P'
    - '18th century: H, O, N, Cl'
    - 'Early 19th century: I, Se, Br'
    - 'Late 19th century: He, F, Ar, Kr, Ne, Xe, Rn'
    - Notes
    - Citations
    - Monographs
categories:
    - Uncategorized
---
In chemistry, a nonmetal (or non-metal) is a chemical element that mostly lacks metallic attributes. Physically, nonmetals tend to be highly volatile (easily vaporized), have low elasticity, and are good insulators of heat and electricity; chemically, they tend to have high ionization energy and electronegativity values, and gain or share electrons when they react with other elements or compounds. Seventeen elements are generally classified as nonmetals; most are gases (hydrogen, helium, nitrogen, oxygen, fluorine, neon, chlorine, argon, krypton, xenon and radon); one is a liquid (bromine), and a few are solids (carbon, phosphorus, sulfur, selenium, and iodine).
