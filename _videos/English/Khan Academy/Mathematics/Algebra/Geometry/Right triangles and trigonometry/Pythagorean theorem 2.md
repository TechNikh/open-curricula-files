---
version: 1
type: video
provider: YouTube
id: O64YFlX1_aI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Geometry/Right%20triangles%20and%20trigonometry/Pythagorean%20theorem%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6e36404b-bd03-454a-ac2d-dbda735d4864
updated: 1486069559
title: Pythagorean theorem 2
categories:
    - Right triangles and trigonometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/O64YFlX1_aI/default.jpg
    - https://i3.ytimg.com/vi/O64YFlX1_aI/1.jpg
    - https://i3.ytimg.com/vi/O64YFlX1_aI/2.jpg
    - https://i3.ytimg.com/vi/O64YFlX1_aI/3.jpg
---
