---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magistrate
offline_file: ""
offline_thumbnail: ""
uuid: 6b0a703c-65dd-4211-b774-5cfd5f47f5ce
updated: 1484310599
title: magistrate
categories:
    - Dictionary
---
magistrate
     n : a public official authorized to decide questions bought
         before a court of justice [syn: {judge}, {justice}, {jurist}]
