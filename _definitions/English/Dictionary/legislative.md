---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legislative
offline_file: ""
offline_thumbnail: ""
uuid: 6096b461-1588-4db2-ba49-a1b270d8d296
updated: 1484310593
title: legislative
categories:
    - Dictionary
---
legislative
     adj 1: relating to a legislature or composed of members of a
            legislature; "legislative council"
     2: of or relating to or created by legislation; "legislative
        proposal"
