---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pakistani
offline_file: ""
offline_thumbnail: ""
uuid: 73fb1ad8-4daa-42c9-a861-2a855e364125
updated: 1484310174
title: pakistani
categories:
    - Dictionary
---
Pakistani
     adj : of or relating to Pakistan or its people or language;
           "Pakistani mountain passes"; "Pakistani soldiers"
     n : a native or inhabitant of Pakistan
