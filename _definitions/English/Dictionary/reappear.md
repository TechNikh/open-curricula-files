---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reappear
offline_file: ""
offline_thumbnail: ""
uuid: cd5b8850-d73b-406f-874d-b3a793cd0a03
updated: 1484310381
title: reappear
categories:
    - Dictionary
---
reappear
     v : appear again; "The sores reappeared on her body"; "Her
         husband reappeared after having left her years ago" [syn:
          {re-emerge}]
