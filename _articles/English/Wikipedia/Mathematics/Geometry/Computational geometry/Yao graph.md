---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yao_graph
offline_file: ""
offline_thumbnail: ""
uuid: e06ae944-a83d-40cd-b017-1305884dc6bf
updated: 1484309179
title: Yao graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Yao_graph.svg.png
categories:
    - Computational geometry
---
In computational geometry, the Yao graph, named after Andrew Yao, is a kind of geometric spanner, a weighted undirected graph connecting a set of geometric points with the property that, for every pair of points in the graph, their shortest path has a length that is within a constant factor of their Euclidean distance.
