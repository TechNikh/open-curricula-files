---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_motion
offline_file: ""
offline_thumbnail: ""
uuid: 4b4a8646-4cdd-4820-b7ab-e6a2c1581759
updated: 1484309258
title: Hyperbolic motion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ultraparallel_theorem.svg.png
tags:
    - Motions on the hyperbolic plane
    - Introduction of metric in the Poincaré half-plane model
    - Use of semi-circle Z
    - Disk model motions
categories:
    - Inversive geometry
---
In geometry, hyperbolic motions are isometric automorphisms of a hyperbolic space. Under composition of mappings, the hyperbolic motions form a continuous group. This group is said to characterize the hyperbolic space. Such an approach to geometry was cultivated by Felix Klein in his Erlangen program. The idea of reducing geometry to its characteristic group was developed particularly by Mario Pieri in his reduction of the primitive notions of geometry to merely point and motion.
