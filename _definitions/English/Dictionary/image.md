---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/image
offline_file: ""
offline_thumbnail: ""
uuid: 2405a83b-d25c-49ce-ac13-ad848784ce45
updated: 1484310221
title: image
categories:
    - Dictionary
---
image
     n 1: an iconic mental representation; "her imagination forced
          images upon her too awful to contemplate" [syn: {mental
          image}]
     2: a visual representation (of an object or scene or person or
        abstraction) produced on a surface; "they showed us the
        pictures of their wedding"; "a movie is a series of images
        projected so rapidly that the eye integrates them" [syn: {picture},
         {icon}, {ikon}]
     3: (Jungian psychology) a personal facade that one presents to
        the world; "a public image is as fragile as Humpty Dumpty"
        [syn: {persona}]
     4: a standard or typical example; "he is the prototype of good
        ...
