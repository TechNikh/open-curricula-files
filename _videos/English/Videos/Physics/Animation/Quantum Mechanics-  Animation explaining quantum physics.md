---
version: 1
type: video
provider: YouTube
id: iVpXrbZ4bnU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b51438ee-ff12-48d2-9e66-2f10dd6e8cb7
updated: 1486069654
title: 'Quantum Mechanics-  Animation explaining quantum physics'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/iVpXrbZ4bnU/default.jpg
    - https://i3.ytimg.com/vi/iVpXrbZ4bnU/1.jpg
    - https://i3.ytimg.com/vi/iVpXrbZ4bnU/2.jpg
    - https://i3.ytimg.com/vi/iVpXrbZ4bnU/3.jpg
---
