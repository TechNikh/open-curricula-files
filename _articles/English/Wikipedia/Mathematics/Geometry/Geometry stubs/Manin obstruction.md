---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manin_obstruction
offline_file: ""
offline_thumbnail: ""
uuid: 16adac32-b0db-42c1-8dd3-ef32f7177097
updated: 1484309365
title: Manin obstruction
categories:
    - Geometry stubs
---
In mathematics, in the field of arithmetic algebraic geometry, the Manin obstruction (named after Yuri Manin) is attached to a variety X over a global field, which measures the failure of the Hasse principle for X. If the value of the obstruction is non-trivial, then X may have points over all local fields but not over the global field.
