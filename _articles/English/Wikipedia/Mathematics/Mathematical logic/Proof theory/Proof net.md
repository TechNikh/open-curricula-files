---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_net
offline_file: ""
offline_thumbnail: ""
uuid: eb7f5b03-acc2-41a8-8b59-423f70ae05db
updated: 1484309497
title: Proof net
tags:
    - Correctness criteria
    - Sources
categories:
    - Proof theory
---
In proof theory, proof nets are a geometrical method of representing proofs that eliminates two forms of bureaucracy that differentiates proofs: (A) irrelevant syntactical features of regular proof calculi such as the natural deduction calculus and the sequent calculus, and (B) the order of rules applied in a derivation. In this way, the formal properties of proof identity correspond more closely to the intuitively desirable properties. Proof nets were introduced by Jean-Yves Girard.
