---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-violent
offline_file: ""
offline_thumbnail: ""
uuid: af4eb7f1-2abb-480c-9696-80ee406b4569
updated: 1484308471
title: Nonviolence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Portrait_Gandhi.jpg
tags:
    - Forms
    - Pragmatic
    - Ethical
    - Methods
    - Acts of protest
    - Noncooperation
    - Nonviolent intervention
    - Revolution
    - Criticism
    - Research
categories:
    - Uncategorized
---
Nonviolence (from Sanskrit ahimṣā, non-violence, "lack of desire to harm or kill") is the personal practice of being harmless to self and others under every condition. It comes from the belief that hurting people, animals or the environment is unnecessary to achieve an outcome and refers to a general philosophy of abstention from violence based on moral, religious or spiritual principles.[1]
