---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/widespread
offline_file: ""
offline_thumbnail: ""
uuid: d8001f70-0abd-4ddc-abc0-51bc5894792b
updated: 1484310467
title: widespread
categories:
    - Dictionary
---
widespread
     adj 1: widely circulated or diffused; "a widespread doctrine";
            "widespread fear of nuclear war"
     2: distributed over a considerable extent; "far-flung trading
        operations"; "the West's far-flung mountain ranges";
        "widespread nuclear fallout" [syn: {far-flung}]
