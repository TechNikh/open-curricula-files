---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preceding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484327281
title: preceding
categories:
    - Dictionary
---
preceding
     adj 1: existing or coming before [syn: {preceding(a)}] [ant: {succeeding(a)}]
     2: preceding in time or order [syn: {antecedent}] [ant: {subsequent}]
     3: of a person who has held and relinquished a position or
        office; "a retiring member of the board" [syn: {past(a)},
        {preceding(a)}, {retiring(a)}]
