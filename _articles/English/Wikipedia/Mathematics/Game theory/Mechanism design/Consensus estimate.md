---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Consensus_estimate
offline_file: ""
offline_thumbnail: ""
uuid: 05fd2df6-c6ab-49c3-af24-6cc0f7323779
updated: 1484309046
title: Consensus estimate
categories:
    - Mechanism design
---
Consensus estimate is a technique for designing truthful mechanisms in a prior-free mechanism design setting. The technique was introduced for digital goods auctions[1] and later extended to more general settings.[2]
