---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revolting
offline_file: ""
offline_thumbnail: ""
uuid: e0c44592-5b97-4b28-86e3-27e08a631623
updated: 1484310583
title: revolting
categories:
    - Dictionary
---
revolting
     adj : highly offensive; arousing aversion or disgust; "a
           disgusting smell"; "distasteful language"; "a loathsome
           disease"; "the idea of eating meat is repellent to me";
           "revolting food"; "a wicked stench" [syn: {disgusting},
            {disgustful}, {distasteful}, {foul}, {loathly}, {loathsome},
            {repellent}, {repellant}, {repelling}, {skanky}, {wicked},
            {yucky}]
