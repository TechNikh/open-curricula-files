---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Eckmann%E2%80%93Hilton_duality'
offline_file: ""
offline_thumbnail: ""
uuid: 46dc69c5-4a7d-481f-8099-cd779e439915
updated: 1484309232
title: Eckmann–Hilton duality
categories:
    - Duality theories
---
In the mathematical disciplines of algebraic topology and homotopy theory, Eckmann–Hilton duality in its most basic form, consists of taking a given diagram for a particular concept and reversing the direction of all arrows, much as in category theory with the idea of the opposite category. A significantly deeper form argues that the dual notion of a limit is a colimit allows us to change the Eilenberg–Steenrod axioms for homology to give axioms for cohomology. It is named after Beno Eckmann and Peter Hilton.
