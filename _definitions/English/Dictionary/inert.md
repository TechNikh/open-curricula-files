---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inert
offline_file: ""
offline_thumbnail: ""
uuid: af2b80b6-22a3-4827-9ebf-a96682bd2a08
updated: 1484310403
title: inert
categories:
    - Dictionary
---
inert
     adj 1: unable to move or resist motion
     2: having only a limited ability to react chemically; not
        active; "inert matter"; "an indifferent chemical in a
        reaction" [syn: {indifferent}, {neutral}]
     3: slow and apathetic; "she was fat and inert"; "a sluggish
        worker"; "a mind grown torpid in old age" [syn: {sluggish},
         {torpid}]
