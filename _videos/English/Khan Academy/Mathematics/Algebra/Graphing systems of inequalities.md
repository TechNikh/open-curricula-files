---
version: 1
type: video
provider: YouTube
id: TqsRlc02rtc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Graphing%20systems%20of%20inequalities.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cb80739f-1f99-4d7a-8f2c-e55df2eda539
updated: 1486069563
title: Graphing systems of inequalities
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/TqsRlc02rtc/default.jpg
    - https://i3.ytimg.com/vi/TqsRlc02rtc/1.jpg
    - https://i3.ytimg.com/vi/TqsRlc02rtc/2.jpg
    - https://i3.ytimg.com/vi/TqsRlc02rtc/3.jpg
---
