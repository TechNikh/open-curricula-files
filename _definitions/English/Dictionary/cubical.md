---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cubical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484365381
title: cubical
categories:
    - Dictionary
---
cubical
     adj : shaped like a cube [syn: {cubelike}, {cube-shaped}, {cubiform},
            {cuboid}, {cuboidal}]
