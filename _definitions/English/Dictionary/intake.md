---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intake
offline_file: ""
offline_thumbnail: ""
uuid: 10907b29-0e57-4f58-9e58-33612528741a
updated: 1484310349
title: intake
categories:
    - Dictionary
---
intake
     n 1: the process of taking food into the body through the mouth
          (as by eating) [syn: {consumption}, {ingestion}, {uptake}]
     2: an opening through which fluid is admitted to a tube or
        container
