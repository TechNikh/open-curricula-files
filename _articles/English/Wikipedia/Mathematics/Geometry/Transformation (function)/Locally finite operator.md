---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Locally_finite_operator
offline_file: ""
offline_thumbnail: ""
uuid: 3baf4b73-0fa5-4388-95c2-3be8c3b99e0b
updated: 1484309336
title: Locally finite operator
categories:
    - Transformation (function)
---
In mathematics, a linear operator 
  
    
      
        f
        :
        V
        →
        V
      
    
    {\displaystyle f:V\to V}
  
 is called locally finite if the space 
  
    
      
        V
      
    
    {\displaystyle V}
  
 is the union of a family of finite-dimensional 
  
    
      
        f
      
    
    {\displaystyle f}
  
-invariant subspaces.
