---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heavily
offline_file: ""
offline_thumbnail: ""
uuid: 7e78c56f-33f3-4d8c-bb6c-83364362a859
updated: 1484310601
title: heavily
categories:
    - Dictionary
---
heavily
     adv 1: to a considerable degree; "he relied heavily on others'
            data" [syn: {to a great extent}]
     2: in a heavy-footed manner; "he walked heavily up the three
        flights to his room"
     3: with great force; "she hit her arm heavily against the wall"
     4: in a manner designed for heavy duty; "a heavily constructed
        car"; "heavily armed"
     5: slowly as if burdened by much weight; "time hung heavy on
        their hands" [syn: {heavy}]
     6: indulging excessively; "he drank heavily" [syn: {intemperately},
         {hard}] [ant: {lightly}]
