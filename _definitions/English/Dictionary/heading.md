---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heading
offline_file: ""
offline_thumbnail: ""
uuid: d4c0b288-30a5-4aee-a793-1abd1d49345e
updated: 1484310399
title: heading
categories:
    - Dictionary
---
heading
     n 1: a line of text serving to indicate what the passage below it
          is about; "the heading seemed to have little to do with
          the text" [syn: {header}, {head}]
     2: the direction or path along which something moves or along
        which it lies [syn: {bearing}, {aim}]
     3: a horizontal (or nearly horizontal) passageway in a mine;
        "they dug a drift parallel with the vein" [syn: {drift}, {gallery}]
