---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Loose_motions
offline_file: ""
offline_thumbnail: ""
uuid: eceab999-0400-45f0-902b-db10da836f00
updated: 1484308377
title: Diarrhea
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Multiple_rotavirus_particles.jpg
tags:
    - Definition
    - Secretory
    - Osmotic
    - Exudative
    - Inflammatory
    - Dysentery
    - Health effects
    - Differential diagnosis
    - Infections
    - Malabsorption
    - Inflammatory bowel disease
    - Irritable bowel syndrome
    - Other diseases
    - Causes
    - Sanitation
    - Water
    - Nutrition
    - Pathophysiology
    - Evolution
    - Diagnostic approach
    - Prevention
    - Sanitation
    - Hand washing
    - Water
    - Vaccination
    - Nutrition
    - Breastfeeding
    - Others
    - Management
    - fluids
    - Eating
    - Medications
    - Alternative therapies
    - Epidemiology
    - Etymology
categories:
    - Uncategorized
---
Diarrhea, also spelled diarrhoea, is the condition of having at least three loose or liquid bowel movements each day. It often lasts for a few days and can result in dehydration due to fluid loss. Signs of dehydration often begin with loss of the normal stretchiness of the skin and irritable behaviour. This can progress to decreased urination, loss of skin color, a fast heart rate, and a decrease in responsiveness as it becomes more severe. Loose but non-watery stools in babies who are breastfed, however, may be normal.[2]
