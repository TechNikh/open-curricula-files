---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electricity
offline_file: ""
offline_thumbnail: ""
uuid: 7b881bd2-9d8d-41c0-9764-e70e6628c381
updated: 1484310260
title: electricity
categories:
    - Dictionary
---
electricity
     n 1: a physical phenomenon associated with stationary or moving
          electrons and protons
     2: energy made available by the flow of electric charge through
        a conductor; "they built a car that runs on electricity"
        [syn: {electrical energy}]
     3: keen and shared excitement; "the stage crackled with
        electricity whenever she was on it"
