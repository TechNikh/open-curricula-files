---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slaked
offline_file: ""
offline_thumbnail: ""
uuid: 1366b94e-acd5-4c58-a9a1-8b97ba568eb9
updated: 1484310373
title: slaked
categories:
    - Dictionary
---
slaked
     adj : allayed; "his thirst quenched he was able to continue" [syn:
            {quenched}, {satisfied}]
