---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mnemonics_in_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: c566817f-056b-4c94-83db-db9a8f67f150
updated: 1484309345
title: Mnemonics in trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trigonometric_function_quadrant_sign.svg.png
categories:
    - Trigonometry
---
In trigonometry, it is common to use mnemonics to help remember trigonometric identities and the relationships between the various trigonometric functions. For example, the sine, cosine, and tangent ratios in a right triangle can be remembered by representing them as strings of letters, for instance SOH-CAH-TOA in English:
