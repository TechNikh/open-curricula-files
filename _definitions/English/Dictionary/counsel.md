---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/counsel
offline_file: ""
offline_thumbnail: ""
uuid: 592627fa-99bf-4f0f-b690-05351d8c4e2c
updated: 1484310553
title: counsel
categories:
    - Dictionary
---
counsel
     n 1: a lawyer who pleads cases in court [syn: {advocate}, {counselor},
           {counsellor}, {counselor-at-law}, {pleader}]
     2: something that provides direction or advice as to a decision
        or course of action [syn: {guidance}, {counseling}, {counselling},
         {direction}]
     v : give advice to; "The teacher counsels troubled students";
         "The lawyer counselled me when I was accused of tax
         fraud" [syn: {advise}]
     [also: {counselling}, {counselled}]
