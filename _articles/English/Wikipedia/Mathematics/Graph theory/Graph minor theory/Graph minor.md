---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_minor
offline_file: ""
offline_thumbnail: ""
uuid: 14d3ece5-558c-4225-85dc-de7aad16d208
updated: 1484309411
title: Graph minor
tags:
    - Definitions
    - Example
    - Major results and conjectures
    - Minor-closed graph families
    - Variations
    - Topological minors
    - Immersion minor
    - Shallow minors
    - Parity conditions
    - Algorithms
    - Notes
categories:
    - Graph minor theory
---
In graph theory, an undirected graph H is called a minor of the graph G if H can be formed from G by deleting edges and vertices and by contracting edges.
