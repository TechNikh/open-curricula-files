---
version: 1
type: video
provider: YouTube
id: c5D2fhDLs5s
offline_file: ""
offline_thumbnail: ""
uuid: 39bff3f1-9092-43bd-9bef-d5bba570e1ad
updated: 1484040352
title: Complex determinant example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/c5D2fhDLs5s/default.jpg
    - https://i3.ytimg.com/vi/c5D2fhDLs5s/1.jpg
    - https://i3.ytimg.com/vi/c5D2fhDLs5s/2.jpg
    - https://i3.ytimg.com/vi/c5D2fhDLs5s/3.jpg
---
