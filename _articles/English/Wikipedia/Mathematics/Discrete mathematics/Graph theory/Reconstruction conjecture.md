---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reconstruction_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: fa03845b-5d2b-42fc-8c5e-0beee92dc3c8
updated: 1484308632
title: Reconstruction conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-A_graph_and_its_deck_as_described_in_the_Reconstruction_conjecture_of_graph_theory.jpg
tags:
    - Formal statements
    - Verification
    - Reconstructible graph families
    - Recognizable properties
    - Reduction
    - Duality
    - Other structures
categories:
    - Graph theory
---
Informally, the reconstruction conjecture in graph theory says that graphs are determined uniquely by their subgraphs. It is due to Kelly[1] and Ulam.[2][3]
