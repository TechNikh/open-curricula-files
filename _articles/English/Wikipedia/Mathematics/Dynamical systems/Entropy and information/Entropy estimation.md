---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_estimation
offline_file: ""
offline_thumbnail: ""
uuid: 27a7d379-8c0a-4785-a90d-2a3894887c5f
updated: 1484308773
title: Entropy estimation
tags:
    - Histogram estimator
    - Estimates based on sample-spacings
    - Estimates based on nearest-neighbours
    - Bayesian estimator
    - Estimates based on expected entropy
categories:
    - Entropy and information
---
In various science/engineering applications, such as independent component analysis,[1] image analysis,[2] genetic analysis,[3] speech recognition,[4] manifold learning,[5] and time delay estimation[6] it is useful to estimate the differential entropy of a system or process, given some observations.
