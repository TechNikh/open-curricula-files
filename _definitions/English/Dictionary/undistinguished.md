---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undistinguished
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484617621
title: undistinguished
categories:
    - Dictionary
---
undistinguished
     adj : not worthy of notice [syn: {insignificant}]
