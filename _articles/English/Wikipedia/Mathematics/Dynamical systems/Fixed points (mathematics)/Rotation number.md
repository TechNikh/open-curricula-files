---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation_number
offline_file: ""
offline_thumbnail: ""
uuid: 05d983ac-e8df-4533-9b3c-d8ad6e2482cf
updated: 1484308795
title: Rotation number
tags:
    - History
    - Definition
    - Example
    - Properties
categories:
    - Fixed points (mathematics)
---
