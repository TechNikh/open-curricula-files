---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inventor
offline_file: ""
offline_thumbnail: ""
uuid: 03686f4f-38f5-4bb8-bea4-0c48805dee7b
updated: 1484310146
title: inventor
categories:
    - Dictionary
---
inventor
     n : someone who is the first to think of or make something [syn:
          {discoverer}, {artificer}]
