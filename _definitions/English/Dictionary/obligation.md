---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obligation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484548741
title: obligation
categories:
    - Dictionary
---
obligation
     n 1: the social force that binds you to your obligations and the
          courses of action demanded by that force; "we must
          instill a sense of duty in our children"; "every right
          implies a responsibility; every opportunity, an
          obligation; every possession, a duty"- John
          D.Rockefeller Jr [syn: {duty}, {responsibility}]
     2: the state of being obligated to do or pay something; "he is
        under an obligation to finish the job"
     3: a personal relation in which one is indebted for a service
        or favor
     4: a legal agreement specifying a payment or action and the
        penalty for failure to comply
