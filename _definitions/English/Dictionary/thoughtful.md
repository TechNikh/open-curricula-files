---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thoughtful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450101
title: thoughtful
categories:
    - Dictionary
---
thoughtful
     adj 1: having intellectual depth; "a deeply thoughtful essay"
     2: exhibiting or characterized by careful thought; "a
        thoughtful paper" [ant: {thoughtless}]
     3: acting with or showing thought and good sense; "a sensible
        young man" [syn: {sensible}]
     4: taking heed; giving close and thoughtful attention; "heedful
        of the warnings"; "so heedful a writer"; "heedful of what
        they were doing" [syn: {heedful}, {attentive}] [ant: {heedless}]
     5: showing consideration and anticipation of needs; "it was
        thoughtful of you to bring flowers"; "a neighbor showed
        thoughtful attention" [syn: {kind}]
