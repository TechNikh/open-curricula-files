---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tutte%E2%80%93Berge_formula'
offline_file: ""
offline_thumbnail: ""
uuid: e604f13f-73b4-4885-82cb-c84a69a41781
updated: 1484308669
title: Tutte–Berge formula
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Class-2-planar-3-regular.svg.png
tags:
    - Statement
    - Explanation
    - "Relation to Tutte's theorem"
categories:
    - Theorems in discrete mathematics
---
In the mathematical discipline of graph theory the Tutte–Berge formula is a characterization of the size of a maximum matching in a graph. It is a generalization of Tutte's theorem on perfect matchings, and is named after W. T. Tutte (who proved Tutte's theorem) and Claude Berge (who proved its generalization).
