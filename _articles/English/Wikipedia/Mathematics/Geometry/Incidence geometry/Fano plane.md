---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fano_plane
offline_file: ""
offline_thumbnail: ""
uuid: 142ddbd8-9053-4450-b58c-58f378f0d0ed
updated: 1484309251
title: Fano plane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fano_plane.svg_1.png
tags:
    - Homogeneous coordinates
    - Symmetries
    - Configurations
    - Group-theoretic construction
    - Block design theory
    - Matroid theory
    - Steiner system
    - Fano three-space
    - Notes
categories:
    - Incidence geometry
---
In finite geometry, the Fano plane (after Gino Fano) is the finite projective plane of order 2, having the smallest possible number of points and lines, 7 each, with 3 points on every line and 3 lines through every point. The standard notation for this plane, as a member of a family of projective spaces, is PG(2,2) where PG stands for "Projective Geometry", the first parameter is the geometric dimension and the second parameter is the order.
