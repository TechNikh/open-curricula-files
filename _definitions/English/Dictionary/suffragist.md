---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffragist
offline_file: ""
offline_thumbnail: ""
uuid: 9df5633f-a3cd-4d74-9f7f-4ba7373c680d
updated: 1484310551
title: suffragist
categories:
    - Dictionary
---
suffragist
     n : an advocate of the extension of voting rights (especially to
         women)
