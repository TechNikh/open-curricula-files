---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enzo_Martinelli
offline_file: ""
offline_thumbnail: ""
uuid: 7ea2af74-de6f-4b71-8c7b-501a4ae57517
updated: 1484309088
title: Enzo Martinelli
tags:
    - Biography
    - life
    - Academic career
    - Honours
    - Personality traits
    - Work
    - Research activity
    - Teaching activity
    - Selected publications
    - Notes
    - Biographical and general references
    - Scientific references
    - Proceedings of conferences dedicated to Enzo Martinelli
categories:
    - Geometers
---
Enzo Martinelli (11 November 1911 – 27 August 1999[1]) was an Italian mathematician, working in the theory of functions of several complex variables: he is best known for his work on the theory of integral representations for holomorphic functions of several variables, notably for discovering the Bochner–Martinelli formula in 1938, and for his work in the theory of multi-dimensional residues.
