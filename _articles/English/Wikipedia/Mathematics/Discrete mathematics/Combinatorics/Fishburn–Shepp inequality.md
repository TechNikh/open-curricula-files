---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fishburn%E2%80%93Shepp_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 42902c5e-7f23-4580-8925-6b93e23dbd15
updated: 1484308573
title: Fishburn–Shepp inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Antennarius_striatus.jpg
categories:
    - Combinatorics
---
In combinatorial mathematics, the Fishburn–Shepp inequality is an inequality for the number of extensions of partial orders to linear orders, found by Fishburn (1984) and Shepp (1982).
