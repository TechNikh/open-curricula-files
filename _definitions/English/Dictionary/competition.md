---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/competition
offline_file: ""
offline_thumbnail: ""
uuid: b3c3565f-f5f5-4ed6-b02e-a42118f61a54
updated: 1484310467
title: competition
categories:
    - Dictionary
---
competition
     n 1: a business relation in which two parties compete to gain
          customers; "business competition can be fiendish at
          times"
     2: an occasion on which a winner is selected from among two or
        more contestants [syn: {contest}]
     3: the act of competing as for profit or a prize; "the teams
        were in fierce contention for first place" [syn: {contention},
         {rivalry}] [ant: {cooperation}]
     4: the contestant you hope to defeat; "he had respect for his
        rivals"; "he wanted to know what the competition was
        doing" [syn: {rival}, {challenger}, {competitor}, {contender}]
