---
version: 1
type: video
provider: YouTube
id: dencuBNp_Ck
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/pH%20of%20a%20Weak%20Acid.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 054500ae-a148-4b6f-8b28-7dc455e5cfe1
updated: 1486069589
title: pH of a Weak Acid
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/dencuBNp_Ck/default.jpg
    - https://i3.ytimg.com/vi/dencuBNp_Ck/1.jpg
    - https://i3.ytimg.com/vi/dencuBNp_Ck/2.jpg
    - https://i3.ytimg.com/vi/dencuBNp_Ck/3.jpg
---
Calculating the pH of a weak acid
More free lessons at: http://www.khanacademy.org/video?v=dencuBNp_Ck
