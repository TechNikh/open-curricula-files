---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forego
offline_file: ""
offline_thumbnail: ""
uuid: 4316dead-54d0-4392-aac1-35ae0c58df04
updated: 1484310547
title: forego
categories:
    - Dictionary
---
forego
     v : be earlier in time; go back further; "Stone tools precede
         bronze tools" [syn: {predate}, {precede}, {antecede}, {antedate}]
         [ant: {postdate}]
     [also: {forewent}, {foregone}]
