---
version: 1
type: article
id: https://en.wikipedia.org/wiki/10_(number)
offline_file: ""
offline_thumbnail: ""
uuid: c57d1ccf-d9e5-4d05-a364-15fd35d6e0ca
updated: 1484308946
title: 10 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Tetractys.svg.png
tags:
    - Common usage and derived terms
    - In mathematics
    - In numeral systems
    - Decimal system
    - Roman numerals
    - Positional numeral systems other than decimal
    - List of basic calculations
    - In science
    - Astronomy
    - In religion and philosophy
    - In money
    - In music
    - In sports and games
    - In technology
    - In other fields
categories:
    - Integers
---
10 (ten i/ˈtɛn/) is an even natural number following 9 and preceding 11. Ten is the base of the decimal numeral system, by far the most common system of denoting numbers in both spoken and written language. The reason for the choice of ten is assumed to be that humans have ten fingers (digits).
