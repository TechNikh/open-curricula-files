---
version: 1
type: video
provider: YouTube
id: PV7K_C63Orc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20Do%20Kidney%20Stones%20Form-%20How%20Can%20We%20Prevent%20Them-%20Animation%20Video%20-%20Renal%20Calculi%20Pathophysiology.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 781d5f4b-3f7b-4bc4-87cb-f923708505b4
updated: 1486069669
title: 'How Do Kidney Stones Form? How Can We Prevent Them? Animation Video - Renal Calculi Pathophysiology'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/PV7K_C63Orc/default.jpg
    - https://i3.ytimg.com/vi/PV7K_C63Orc/1.jpg
    - https://i3.ytimg.com/vi/PV7K_C63Orc/2.jpg
    - https://i3.ytimg.com/vi/PV7K_C63Orc/3.jpg
---
