---
version: 1
type: video
provider: YouTube
id: k61nJkx5aDQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 26254b74-a0e5-4daa-8275-3ca7c681dee1
updated: 1486069670
title: The brain dictionary
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/k61nJkx5aDQ/default.jpg
    - https://i3.ytimg.com/vi/k61nJkx5aDQ/1.jpg
    - https://i3.ytimg.com/vi/k61nJkx5aDQ/2.jpg
    - https://i3.ytimg.com/vi/k61nJkx5aDQ/3.jpg
---
