---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Revenue_equivalence
offline_file: ""
offline_thumbnail: ""
uuid: bcad0671-fcb4-4721-8619-b4f8d59b8324
updated: 1484309046
title: Revenue equivalence
tags:
    - Notation
    - Statement
    - Example
    - Equivalence of Auction Mechanisms in Single Item Auctions
    - Second Price Auction
    - First Price Auction
    - Proof
    - English auction
    - Using Revenue Equivalence to Predict Bidding Functions
    - Second Price Auction
    - First Price Auction
    - All-pay Auctions
    - Implications
    - Limitations
categories:
    - Mechanism design
---
Revenue equivalence is a concept in auction theory that states that given certain conditions, any mechanism that results in the same outcomes (i.e. allocates items to the same bidders) also has the same expected revenue.
