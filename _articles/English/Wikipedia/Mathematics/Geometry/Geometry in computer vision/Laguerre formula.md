---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laguerre_formula
offline_file: ""
offline_thumbnail: ""
uuid: e445a141-39f7-4e53-8844-f93232ecf5ad
updated: 1484309184
title: Laguerre formula
categories:
    - Geometry in computer vision
---
The Laguerre formula (named after Edmond Laguerre) provides the acute angle 
  
    
      
        ϕ
      
    
    {\displaystyle \phi }
  
 between two proper real lines,[1][2] as follows:
