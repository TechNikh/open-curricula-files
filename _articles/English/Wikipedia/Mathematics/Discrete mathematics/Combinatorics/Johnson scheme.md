---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Johnson_scheme
offline_file: ""
offline_thumbnail: ""
uuid: b0ddde19-e3f5-4b63-873b-71144561e842
updated: 1484308585
title: Johnson scheme
categories:
    - Combinatorics
---
In mathematics, the Johnson scheme, named after Selmer M. Johnson, is also known as the triangular association scheme. It consists of the set of all binary vectors X of length ℓ and weight n, such that 
  
    
      
        v
        =
        
          |
          X
          |
        
        =
        
          
            
              (
            
            
              ℓ
              n
            
            
              )
            
          
        
      
    
    {\displaystyle v=\left|X\right|={\binom {\ell }{n}}}
  
.[1][2][3] Two vectors x, y ∈ X are called ith associates if dist(x, y) = 2i for i = 0, 1, ..., n. The eigenvalues are given ...
