---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tormented
offline_file: ""
offline_thumbnail: ""
uuid: fcd16693-5cc9-48a8-a00b-21d4e4e6e9d6
updated: 1484310168
title: tormented
categories:
    - Dictionary
---
tormented
     adj 1: experiencing intense pain especially mental pain; "an
            anguished conscience"; "a small tormented schoolboy";
            "a tortured witness to another's humiliation" [syn: {anguished},
             {tortured}]
     2: tormented or harassed by nightmares or unreasonable fears;
        "hagridden...by visions of an imminent heaven or hell upon
        earth"- C.S.Lewis [syn: {hag-ridden}, {hagridden}]
