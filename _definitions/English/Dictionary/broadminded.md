---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broadminded
offline_file: ""
offline_thumbnail: ""
uuid: 2b089f79-6394-4080-b51e-555931584123
updated: 1484310189
title: broadminded
categories:
    - Dictionary
---
broad-minded
     adj 1: incapable of being shocked; "he was warmhearted, sensible
            and unshockable" [syn: {unshockable}] [ant: {shockable}]
     2: inclined to respect views and beliefs that differ from your
        own; "a judge who is broad-minded but even-handed" [ant: {narrow-minded}]
