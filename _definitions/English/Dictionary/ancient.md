---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ancient
offline_file: ""
offline_thumbnail: ""
uuid: e376ce07-0ffb-47d9-93dd-0fba6f358c06
updated: 1484310281
title: ancient
categories:
    - Dictionary
---
ancient
     adj 1: belonging to times long past especially of the historical
            period before the fall of the Western Roman Empire;
            "ancient history"; "ancient civilizations such as
            those of the Etruscans and Sumerians"; "ancient
            Greece"
     2: very old; "an ancient mariner"
