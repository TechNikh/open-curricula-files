---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desecration
offline_file: ""
offline_thumbnail: ""
uuid: 12e08ad7-a7c4-4466-8e6d-da3a77860833
updated: 1484310188
title: desecration
categories:
    - Dictionary
---
desecration
     n : blasphemous behavior; the act of depriving something of its
         sacred character; "desecration of the Holy Sabbath" [syn:
          {profanation}, {blasphemy}, {sacrilege}]
