---
version: 1
type: article
id: https://en.wikipedia.org/wiki/McMullen_problem
offline_file: ""
offline_thumbnail: ""
uuid: d3f9adc6-b653-4069-8913-2c3082a572cd
updated: 1484308504
title: McMullen problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Pentagon_dual_arrangement.svg.png
tags:
    - Statement
    - Equivalent formulations
    - Gale transform
    - Partition into nearly-disjoint hulls
    - Projective duality
    - Results
categories:
    - Discrete geometry
---
