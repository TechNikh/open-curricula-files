---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bind
offline_file: ""
offline_thumbnail: ""
uuid: 35770317-9792-496e-8c7d-58afb33ea54e
updated: 1484310414
title: bind
categories:
    - Dictionary
---
bind
     n : something that hinders as if with bonds
     v 1: stick to firmly; "Will this wallpaper adhere to the wall?"
          [syn: {adhere}, {hold fast}, {bond}, {stick}, {stick to}]
     2: create social or emotional ties; "The grandparents want to
        bond with the child" [syn: {tie}, {attach}, {bond}]
     3: make fast; tie or secure, with or as if with a rope; "The
        Chinese would bind the feet of their women" [ant: {unbind}]
     4: wrap around with something so as to cover or enclose [syn: {bandage}]
     5: secure with or as if with ropes; "tie down the prisoners";
        "tie up the old newspapes and bring them to the recycling
        shed" [syn: {tie down}, {tie ...
