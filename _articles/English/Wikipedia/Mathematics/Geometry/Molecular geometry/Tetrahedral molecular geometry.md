---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tetrahedral_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 76660193-dee7-44e0-8db0-7156dab98138
updated: 1484309280
title: Tetrahedral molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tetrahedral-3D-balls.png
tags:
    - Examples
    - Main group chemistry
    - Transition metal chemistry
    - Water structure
    - Exceptions and distortions
    - Inverted tetrahedral geometry
    - Planarization
    - Tetrahedral molecules with no central atom
categories:
    - Molecular geometry
---
In a tetrahedral molecular geometry, a central atom is located at the center with four substituents that are located at the corners of a tetrahedron. The bond angles are cos−1(−1/3) = 109.4712206...° ≈ 109.5° when all four substituents are the same, as in methane (CH4).[1][2] The perfectly symmetrical tetrahedron belongs to point group Td, but most tetrahedral molecules are have lower symmetry. Tetrahedral molecules can be chiral.
