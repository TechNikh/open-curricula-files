---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auxiliary_function
offline_file: ""
offline_thumbnail: ""
uuid: 9c857fbc-ac50-4ad5-ab11-33828c3eb017
updated: 1484308622
title: Auxiliary function
tags:
    - Definition
    - Explicit functions
    - "Liouville's transcendence criterion"
    - "Fourier's proof of the irrationality of e"
    - "Hermite's proof of the irrationality of er"
    - "Hermite's proof of the transcendence of e"
    - Auxiliary functions from the pigeonhole principle
    - Auxiliary polynomial theorem
    - A theorem of Lang
    - Interpolation determinants
    - A proof of the Hermite–Lindemann theorem
    - Notes
categories:
    - Number theory
---
In mathematics, auxiliary functions are an important construction in transcendental number theory. They are functions that appear in most proofs in this area of mathematics and that have specific, desirable properties, such as taking the value zero for many arguments, or having a zero of high order at some point.[1]
