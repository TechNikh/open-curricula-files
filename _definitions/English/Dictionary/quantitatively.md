---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantitatively
offline_file: ""
offline_thumbnail: ""
uuid: 05f12225-e36a-4d69-9c35-476a3638dd42
updated: 1484310202
title: quantitatively
categories:
    - Dictionary
---
quantitatively
     adv : in a quantitative manner; "this can be expressed
           quantitatively"
