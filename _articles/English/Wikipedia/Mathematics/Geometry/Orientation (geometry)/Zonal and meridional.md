---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zonal_and_meridional
offline_file: ""
offline_thumbnail: ""
uuid: cda3a02a-8935-4984-8453-48860a83dabb
updated: 1484309302
title: Zonal and meridional
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zonal_band.svg.png
categories:
    - Orientation (geometry)
---
The terms zonal and meridional are used to describe directions on a globe. Zonal means "along a latitude circle" or "in the west–east direction"; while meridional means "along a longitude circle" (aka meridian) or "in the north–south direction".
