---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/winner
offline_file: ""
offline_thumbnail: ""
uuid: 6cdc4fba-28cb-4e8e-8329-b9296a8b28f4
updated: 1484310180
title: winner
categories:
    - Dictionary
---
winner
     n 1: the contestant who wins the contest [syn: {victor}] [ant: {loser}]
     2: a gambler who wins a bet [ant: {loser}]
     3: a person with a record of successes; "his son would never be
        the achiever that his father was"; "only winners need
        apply"; "if you want to be a success you have to dress
        like a success" [syn: {achiever}, {success}, {succeeder}]
        [ant: {failure}]
