---
version: 1
type: video
provider: YouTube
id: _NWc3C3cTNA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Two-dimensional%20motion/Different%20way%20to%20determine%20time%20in%20air.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 148711c5-4fec-4e3c-bb86-8dda1880c6e2
updated: 1486069593
title: Different way to determine time in air
categories:
    - Two-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/_NWc3C3cTNA/default.jpg
    - https://i3.ytimg.com/vi/_NWc3C3cTNA/1.jpg
    - https://i3.ytimg.com/vi/_NWc3C3cTNA/2.jpg
    - https://i3.ytimg.com/vi/_NWc3C3cTNA/3.jpg
---
Another way to determine time in the air given an initial vertical velocity. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/two-dimensional-motion/two-dimensional-projectile-mot/v/horizontally-launched-projectile?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/two-dimensional-motion/two-dimensional-projectile-mot/v/projectile-at-an-angle?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
