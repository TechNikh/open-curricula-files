---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/positively
offline_file: ""
offline_thumbnail: ""
uuid: 42251449-c12e-4789-b376-6e2652bed1c4
updated: 1484310204
title: positively
categories:
    - Dictionary
---
positively
     adv 1: extremely; "it was positively monumental"
     2: so as to be positive; in a positive manner; "she intended
        her remarks to be interpreted positively"
