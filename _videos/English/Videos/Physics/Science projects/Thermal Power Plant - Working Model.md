---
version: 1
type: video
provider: YouTube
id: DU55TkHuyJY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 34061b96-ecb4-4966-9c2a-900600bbf52d
updated: 1486069635
title: 'Thermal Power Plant - Working Model'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/DU55TkHuyJY/default.jpg
    - https://i3.ytimg.com/vi/DU55TkHuyJY/1.jpg
    - https://i3.ytimg.com/vi/DU55TkHuyJY/2.jpg
    - https://i3.ytimg.com/vi/DU55TkHuyJY/3.jpg
---
