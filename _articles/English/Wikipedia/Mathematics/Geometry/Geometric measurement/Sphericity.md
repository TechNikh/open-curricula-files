---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sphericity
offline_file: ""
offline_thumbnail: ""
uuid: cd5db512-ffa7-4897-93ac-357bd74e880c
updated: 1484309245
title: Sphericity
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rounding_%2526_sphericity_EN.svg.png'
tags:
    - Ellipsoidal objects
    - Derivation
    - Sphericity of common objects
categories:
    - Geometric measurement
---
Sphericity is a measure of how spherical (round) an object is. As such, it is a specific example of a compactness measure of a shape. Defined by Wadell in 1935,[1] the sphericity, 
  
    
      
        Ψ
      
    
    {\displaystyle \Psi }
  
, of a particle is: the ratio of the surface area of a sphere (with the same volume as the given particle) to the surface area of the particle:
