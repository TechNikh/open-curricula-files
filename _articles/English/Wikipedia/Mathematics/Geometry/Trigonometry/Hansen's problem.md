---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hansen%27s_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 2dac649e-8ed7-4aea-9c81-b0323025ac87
updated: 1484309341
title: "Hansen's problem"
tags:
    - Solution method overview
    - Solution algorithm
categories:
    - Trigonometry
---
Hansen's problem is a problem in planar surveying, named after the astronomer Peter Andreas Hansen (1795–1874), who worked on the geodetic survey of Denmark. There are two known points A and B, and two unknown points P1 and P2. From P1 and P2 an observer measures the angles made by the lines of sight to each of the other three points. The problem is to find the positions of P1 and P2. See figure; the angles measured are (α1, β1, α2, β2).
