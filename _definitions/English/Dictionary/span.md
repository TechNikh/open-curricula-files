---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/span
offline_file: ""
offline_thumbnail: ""
uuid: 11a16aa2-e424-4593-8204-8b2b15236851
updated: 1484310601
title: span
categories:
    - Dictionary
---
span
     n 1: the complete duration of something; "the job was finished in
          the span of an hour"
     2: the distance or interval between two points
     3: two items of the same kind [syn: {couple}, {pair}, {twosome},
         {twain}, {brace}, {yoke}, {couplet}, {distich}, {duo}, {duet},
         {dyad}, {duad}]
     4: a unit of length based on the width of the expanded human
        hand (usually taken as 9 inches)
     5: a structure that allows people or vehicles to cross an
        obstacle such as a river or canal or railway etc. [syn: {bridge}]
     6: the act of sitting or standing astride [syn: {straddle}]
     v : to cover or extend over an area or time period; ...
