---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hartshorne_ellipse
offline_file: ""
offline_thumbnail: ""
uuid: 88424d49-8ab6-4c99-a6ff-ab406c2dff80
updated: 1484309153
title: Hartshorne ellipse
categories:
    - Algebraic geometry
---
In mathematics, a Hartshorne ellipse is an ellipse in the unit ball bounded by the 4-sphere S4 such that the ellipse and the circle given by intersection of its plane with S4 satisfy the Poncelet condition that there is a triangle with vertices on the circle and edges tangent to the ellipse. They were introduced by Hartshorne (1978), who showed that they correspond to k = 2 instantons on S4.
