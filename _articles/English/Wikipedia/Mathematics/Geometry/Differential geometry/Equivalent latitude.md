---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivalent_latitude
offline_file: ""
offline_thumbnail: ""
uuid: 1f19c5d7-9ffe-4bca-aa5d-42f90b38ed1e
updated: 1484309218
title: Equivalent latitude
categories:
    - Differential geometry
---
In differential geometry, the equivalent latitude is a Lagrangian coordinate . It is often used in atmospheric science, particularly in the study of stratospheric dynamics. Each isoline in a map of equivalent latitude follows the flow velocity and encloses the same area as the latitude line of equivalent value, hence "equivalent latitude." Equivalent latitude is calculated from potential vorticity, from passive tracer simulations and from actual measurements of atmospheric tracers such as ozone.
