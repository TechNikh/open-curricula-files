---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hamper
offline_file: ""
offline_thumbnail: ""
uuid: 38acb4a0-4bd3-4720-8dc9-d7caf83d3ab2
updated: 1484310238
title: hamper
categories:
    - Dictionary
---
hamper
     n 1: a restraint that confines or restricts freedom (especially
          something used to tie down or restrain a prisoner) [syn:
           {shackle}, {bond}, {trammel}, {trammels}]
     2: a basket usually with a cover
     v 1: prevent the progress or free movement of; "He was hampered
          in his efforts by the bad weather"; "the imperilist
          nation wanted to strangle the free trade between the two
          small countries" [syn: {halter}, {cramp}, {strangle}]
     2: put at a disadvantage; "The brace I have to wear is
        hindering my movements" [syn: {handicap}, {hinder}]
