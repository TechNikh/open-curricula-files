---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adversely
offline_file: ""
offline_thumbnail: ""
uuid: 6c7aa7ba-48e6-4463-accd-eb6ba5e4ffca
updated: 1484310480
title: adversely
categories:
    - Dictionary
---
adversely
     adv : in an adverse manner; "she was adversely affected by the new
           regulations"
