---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/group
offline_file: ""
offline_thumbnail: ""
uuid: 890b514e-761c-4798-86a3-4e2356e8f426
updated: 1484310307
title: group
categories:
    - Dictionary
---
group
     n 1: any number of entities (members) considered as a unit [syn:
          {grouping}]
     2: (chemistry) two or more atoms bound together as a single
        unit and forming part of a molecule [syn: {radical}, {chemical
        group}]
     3: a set that is closed, associative, has an identity element
        and every element has an inverse [syn: {mathematical group}]
     v 1: arrange into a group or groups; "Can you group these shapes
          together?"
     2: form a group or group together [syn: {aggroup}]
