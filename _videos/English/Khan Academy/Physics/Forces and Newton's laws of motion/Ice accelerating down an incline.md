---
version: 1
type: video
provider: YouTube
id: Mz2nDXElcoM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Forces%20and%20Newton%27s%20laws%20of%20motion/Ice%20accelerating%20down%20an%20incline.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2634f662-db34-4315-80e7-67f0829131f8
updated: 1486069591
title: Ice accelerating down an incline
categories:
    - "Forces and Newton's laws of motion"
thumbnail_urls:
    - https://i3.ytimg.com/vi/Mz2nDXElcoM/default.jpg
    - https://i3.ytimg.com/vi/Mz2nDXElcoM/1.jpg
    - https://i3.ytimg.com/vi/Mz2nDXElcoM/2.jpg
    - https://i3.ytimg.com/vi/Mz2nDXElcoM/3.jpg
---
Figuring out the acceleration of ice down a plane made of ice. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/inclined-planes-friction/v/force-of-friction-keeping-the-block-stationary?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/inclined-planes-friction/v/inclined-plane-force-components?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
