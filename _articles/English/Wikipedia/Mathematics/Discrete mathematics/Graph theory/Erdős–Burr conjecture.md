---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Burr_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 4bd70955-4ac7-4110-8710-0cca22b5d3b8
updated: 1484308616
title: Erdős–Burr conjecture
tags:
    - Definitions
    - The conjecture
    - Known results
    - Notes
categories:
    - Graph theory
---
In mathematics, the Erdős–Burr conjecture is a problem concerning the Ramsey number of sparse graphs. The conjecture is named after Paul Erdős and Stefan Burr, and is one of many conjectures named after Erdős; it states that the Ramsey number of graphs in any sparse family of graphs should grow linearly in the number of vertices of the graph.
