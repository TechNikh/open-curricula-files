---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sevens
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587981
title: sevens
categories:
    - Dictionary
---
sevens
     n : a card game in which you play your sevens and other cards in
         sequence in the same suit as their sevens; you win if you
         are the first to use all your cards [syn: {fantan}, {parliament}]
