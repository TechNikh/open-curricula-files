---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Free_convolution
offline_file: ""
offline_thumbnail: ""
uuid: 8a3a85b6-2ace-41f0-b2cc-033198874f91
updated: 1484308588
title: Free convolution
tags:
    - Free additive convolution
    - Rectangular free additive convolution
    - Free multiplicative convolution
    - Applications of free convolution
categories:
    - Combinatorics
---
Free convolution is the free probability analog of the classical notion of convolution of probability measures. Due to the non-commutative nature of free probability theory, one has to talk separately about additive and multiplicative free convolution, which arise from addition and multiplication of free random variables (see below; in the classical case, what would be the analog of free multiplicative convolution can be reduced to additive convolution by passing to logarithms of random variables). These operations have some interpretations in terms of empirical spectral measures of random matrices.[1]
