---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_thinking
offline_file: ""
offline_thumbnail: ""
uuid: 80563448-ad77-4d32-8c8c-185403f67bee
updated: 1484309065
title: Strategic thinking
tags:
    - Overview
    - Strategic thinking vs. strategic planning
    - Strategic thinking competencies
categories:
    - Strategy
---
Strategic thinking is defined as a mental or thinking process applied by an individual in the context of achieving success in a game or other endeavor. As a cognitive activity, it produces thought.
