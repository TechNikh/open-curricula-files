---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/then
offline_file: ""
offline_thumbnail: ""
uuid: 3d7c0e90-96ef-476e-98f9-030b05eaa290
updated: 1484310344
title: then
categories:
    - Dictionary
---
then
     adj : at a specific prior time; "the then president" [syn: {then(a)}]
     n : that time; that moment; "we will arrive before then"; "we
         were friends from then on"
     adv 1: subsequently or soon afterward (often used as sentence
            connectors); "then he left"; "go left first, then
            right"; "first came lightning, then thunder"; "we
            watched the late movie and then went to bed"; "and so
            home and to bed" [syn: {so}, {and so}, {and then}]
     2: in that case or as a consequence; "if he didn't take it,
        then who did?"; "keep it then if you want to"; "the case,
        then, is closed"; "you've made up your mind then?"; ...
