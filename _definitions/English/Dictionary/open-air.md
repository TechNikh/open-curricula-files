---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/open-air
offline_file: ""
offline_thumbnail: ""
uuid: cc811d37-7087-4fa4-a398-94d57e7466cf
updated: 1484310443
title: open-air
categories:
    - Dictionary
---
open air
     n : where the air is unconfined; "he wanted to get outdoors a
         little"; "the concert was held in the open air"; "camping
         in the open" [syn: {outdoors}, {out-of-doors}, {open}]
