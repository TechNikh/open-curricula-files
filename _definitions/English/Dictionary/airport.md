---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/airport
offline_file: ""
offline_thumbnail: ""
uuid: 052c51f0-dfce-45f8-a9c7-1db76b31b001
updated: 1484310480
title: airport
categories:
    - Dictionary
---
airport
     n : an airfield equipped with control tower and hangers as well
         as accommodations for passengers and cargo [syn: {airdrome},
          {aerodrome}]
