---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preacher
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500801
title: preacher
categories:
    - Dictionary
---
preacher
     n : someone whose occupation is preaching the gospel [syn: {preacher
         man}, {sermonizer}, {sermoniser}]
