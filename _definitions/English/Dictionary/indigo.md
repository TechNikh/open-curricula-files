---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indigo
offline_file: ""
offline_thumbnail: ""
uuid: 87cc191f-f4c9-4853-bb8e-4ee20d6eee8a
updated: 1484310389
title: indigo
categories:
    - Dictionary
---
indigo
     n 1: a blue dye obtained from plants or made synthetically [syn:
          {anil}, {indigotin}]
     2: deciduous subshrub of southeastern Asia having pinnate
        leaves and clusters of red or purple flowers; a source of
        indigo dye [syn: {indigo plant}, {Indigofera tinctoria}]
     3: a blue-violet color
     [also: {indigoes} (pl)]
