---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Food_colour
offline_file: ""
offline_thumbnail: ""
uuid: d4cc16b9-8720-44b3-946a-8fe5c8ee3f5f
updated: 1484308378
title: Food coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/146px-Foodlogo2.svg.png
tags:
    - Purpose of food coloring
    - Regulation
    - Past use and regulation history
    - Current regulation
    - Global harmonization
    - Artificial coloring
    - In the United States
    - Current seven generally permitted
    - Permitted for limited use in foods
    - Delisted and banned in the US
    - Approved in EU
    - Natural food dyes
    - Dyes and lakes
    - Other uses
    - Criticism and health implications
categories:
    - Uncategorized
---
Food coloring, or color additive, is any dye, pigment or substance that imparts color when it is added to food or drink. They come in many forms consisting of liquids, powders, gels, and pastes. Food coloring is used both in commercial food production and in domestic cooking. Due to its safety and general availability, food coloring is also used in a variety of non-food applications including cosmetics, pharmaceuticals, home craft projects, and medical devices.[2]
