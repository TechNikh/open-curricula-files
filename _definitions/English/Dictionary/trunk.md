---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trunk
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485501
title: trunk
categories:
    - Dictionary
---
trunk
     n 1: the main stem of a tree; usually covered with bark; the bole
          is usually the part that is commercially useful for
          lumber [syn: {tree trunk}, {bole}]
     2: luggage consisting of a large strong case used when
        traveling or for storage
     3: the body excluding the head and neck and limbs; "they moved
        their arms and legs and bodies" [syn: {torso}, {body}]
     4: compartment in an automobile that carries luggage or
        shopping or tools; "he put his golf bag in the trunk"
        [syn: {luggage compartment}, {automobile trunk}]
     5: a long flexible snout as of an elephant [syn: {proboscis}]
