---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/practise
offline_file: ""
offline_thumbnail: ""
uuid: 0367b63c-2fe9-46b0-bdad-a7cf6cfe0430
updated: 1484310236
title: practise
categories:
    - Dictionary
---
practise
     v 1: engage in a rehearsal (of) [syn: {rehearse}, {practice}]
     2: carry out or practice; as of jobs and professions; "practice
        law" [syn: {practice}, {exercise}, {do}]
     3: learn by repetition; "We drilled French verbs every day";
        "Pianists practice scales" [syn: {drill}, {exercise}, {practice}]
