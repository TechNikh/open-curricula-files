---
version: 1
type: video
provider: YouTube
id: 0Sx5aJoAkW8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Understanding%20Logical%20Statements%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2848f80f-7e1f-426b-9871-ede0cb359bfd
updated: 1486069559
title: Understanding Logical Statements 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/0Sx5aJoAkW8/default.jpg
    - https://i3.ytimg.com/vi/0Sx5aJoAkW8/1.jpg
    - https://i3.ytimg.com/vi/0Sx5aJoAkW8/2.jpg
    - https://i3.ytimg.com/vi/0Sx5aJoAkW8/3.jpg
---
