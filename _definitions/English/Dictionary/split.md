---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/split
offline_file: ""
offline_thumbnail: ""
uuid: ed9e97b2-b27d-44c2-8286-f4265d7c0fe7
updated: 1484310366
title: split
categories:
    - Dictionary
---
split
     adj 1: being divided or separated; "split between love and hate"
     2: having been divided; having the unity destroyed;
        "Congress...gave the impression of...a confusing sum of
        disconnected local forces"-Samuel Lubell; "a league of
        disunited nations"- E.B.White; "a fragmented coalition";
        "a split group" [syn: {disconnected}, {disunited}, {fragmented}]
     3: broken or burst apart longitudinally; "after the
        thunderstorm we found a tree with a split trunk"; "they
        tore big juicy chunks from the heart of the split
        watermelon"
     4: having a long rip or tear; "a split lip" [syn: {cut}]
     5: (especially of wood) cut or ...
