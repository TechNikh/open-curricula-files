---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hostel
offline_file: ""
offline_thumbnail: ""
uuid: 4f1315b1-6a49-4f5f-a34d-47d7422df9fe
updated: 1484310484
title: hostel
categories:
    - Dictionary
---
hostel
     n 1: a hotel providing overnight lodging for travelers [syn: {hostelry},
           {inn}, {lodge}]
     2: inexpensive supervised lodging (especially for youths on
        bicycling trips) [syn: {youth hostel}, {student lodging}]
