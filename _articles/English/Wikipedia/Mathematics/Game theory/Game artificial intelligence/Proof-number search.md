---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof-number_search
offline_file: ""
offline_thumbnail: ""
uuid: 0be6beb9-5447-422e-85ce-bbac5f6bc4b1
updated: 1484309006
title: Proof-number search
categories:
    - Game artificial intelligence
---
Proof-number search (short: PN search) is a game tree search algorithm invented by Victor Allis,[1] with applications mostly in endgame solvers, but also for sub-goals during games.
