---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_conjugacy
offline_file: ""
offline_thumbnail: ""
uuid: d8c66952-1b75-4f6e-bd5e-5209a9c03ff7
updated: 1484308879
title: Topological conjugacy
tags:
    - Definition
    - Flows
    - Examples
    - Discussion
    - Topological equivalence
    - Smooth and orbital equivalence
    - Generalizations of dynamic topological conjugacy
categories:
    - Topological dynamics
---
In mathematics, two functions are said to be topologically conjugate to one another if there exists a homeomorphism that will conjugate the one into the other. Topological conjugacy is important in the study of iterated functions and more generally dynamical systems, since, if the dynamics of one iterated function can be solved, then those for any topologically conjugate function follow trivially.
