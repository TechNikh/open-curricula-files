---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Injective_metric_space
offline_file: ""
offline_thumbnail: ""
uuid: 48b0aecc-6e54-4dbd-915e-d6275e4d7bc2
updated: 1484309268
title: Injective metric space
tags:
    - Hyperconvexity
    - Injectivity
    - Examples
    - Properties
categories:
    - Metric geometry
---
In metric geometry, an injective metric space, or equivalently a hyperconvex metric space, is a metric space with certain properties generalizing those of the real line and of L∞ distances in higher-dimensional vector spaces. These properties can be defined in two seemingly different ways: hyperconvexity involves the intersection properties of closed balls in the space, while injectivity involves the isometric embeddings of the space into larger spaces. However it is a theorem of Aronszajn and Panitchpakdi (1956; see e.g. Chepoi 1997) that these two different types of definitions are equivalent.
