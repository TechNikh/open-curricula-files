---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Devil%27s_curve'
offline_file: ""
offline_thumbnail: ""
uuid: 9e5c04c5-93a8-4304-9843-af056cb36a3e
updated: 1484309360
title: "Devil's curve"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Devils_curve_a%253D0.8_b%253D1.svg.png'
categories:
    - Geometry stubs
---
Devil's curves were studied heavily by Gabriel Cramer.
