---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Andr%C3%A9%E2%80%93Oort_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: e98e3e35-fb08-4bb8-8539-9d11f47961e5
updated: 1484309130
title: André–Oort conjecture
tags:
    - Statement
    - Partial results
    - Generalisations
categories:
    - Algebraic geometry
---
In mathematics, the André–Oort conjecture is an open problem in number theory that generalises the Manin–Mumford conjecture. A prototypical version of the conjecture was stated by Yves André in 1989[1] and a more general version was conjectured by Frans Oort in 1995.[2] The modern version is a natural generalisation of these two conjectures.
