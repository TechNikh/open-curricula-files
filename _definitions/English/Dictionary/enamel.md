---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enamel
offline_file: ""
offline_thumbnail: ""
uuid: cc5d77d5-b1b4-4042-b05a-b73d60a5646a
updated: 1484310383
title: enamel
categories:
    - Dictionary
---
enamel
     n 1: hard white substance covering the crown of a tooth [syn: {tooth
          enamel}]
     2: a colored glassy compound (opaque or partially opaque) that
        is fused to the surface of metal or glass or pottery for
        decoration or protection
     3: a paint that dries to a hard glossy finish
     4: any smooth glossy coating that resembles ceramic glaze
     v : coat, inlay, or surface with enamel
     [also: {enamelling}, {enamelled}]
