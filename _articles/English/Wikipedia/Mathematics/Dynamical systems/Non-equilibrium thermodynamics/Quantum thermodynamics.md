---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 0f8a8774-fb29-4e4d-b9c7-36c6f1fcbcf7
updated: 1484308816
title: Quantum thermodynamics
tags:
    - A dynamical view of quantum thermodynamics
    - >
        The emergence of time derivative of first law of
        thermodynamics
    - The emergence of the second law of thermodynamics
    - Entropy
    - Clausius version of the II-law
    - >
        The Quantum and Thermodynamic Adiabatic Conditions and
        Quantum Friction
    - >
        The emergence of the dynamical version of the third law of
        thermodynamics
    - >
        Typicality as a source of emergence of thermodynamical
        phenomena
    - Quantum thermodynamics resource theory
categories:
    - Non-equilibrium thermodynamics
---
Quantum thermodynamics is the study of the relations between two independent physical theories: thermodynamics and quantum mechanics. The two independent theories address the physical phenomena of light and matter. In 1905 Einstein argued that the requirement of consistency between thermodynamics and electromagnetism...:.[1] leads to the conclusion that light is quantized obtaining the relation 
  
    
      
        E
        =
        h
        ν
      
    
    {\displaystyle E=h\nu }
  
. This paper is the dawn of quantum theory. In a few decades quantum theory became established with an independent set of rules.[2] Currently quantum thermodynamics addresses the emergence of ...
