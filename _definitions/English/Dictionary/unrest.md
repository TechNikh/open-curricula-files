---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unrest
offline_file: ""
offline_thumbnail: ""
uuid: c535bff3-f729-450e-9c0f-5380d07153e2
updated: 1484310581
title: unrest
categories:
    - Dictionary
---
unrest
     n 1: a state of agitation or turbulent change or development;
          "the political ferment produced a new leadership";
          "social unrest" [syn: {agitation}, {ferment}, {fermentation}]
     2: a feeling of restless agitation
