---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethnically
offline_file: ""
offline_thumbnail: ""
uuid: 897e44f4-3b7d-4a8f-969f-3badf552ab6d
updated: 1484310180
title: ethnically
categories:
    - Dictionary
---
ethnically
     adv : with respect to ethnicity; "the neighborhood is ethnically
           diverse"
