---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ahmednagar
offline_file: ""
offline_thumbnail: ""
uuid: 6c435d6e-faf6-405c-a470-33e00a5bc409
updated: 1484308438
title: Ahmednagar
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Salabat_Khan%2527s_Dome.png'
tags:
    - History
    - Military base
    - Climate
    - Demographics
    - Notable people
    - Places of interest
    - Transport
    - air
    - Rail
    - Road
    - Politics
    - Media and communication
categories:
    - Uncategorized
---
Ahmednagar  pronunciation (help·info) is a city in Ahmednagar District in the state of Maharashtra, India, about 120 km northeast of Pune and 114 km from Aurangabad. Ahmednagar takes its name from Ahmad Nizam Shah I, who founded the town in 1494 on the site of a battlefield where he won a battle against superior Bahamani forces.[4] It was close to the site of the village of Bhingar.[4] With the breakup of the Bahmani Sultanate, Ahmad established a new sultanate in Ahmednagar, also known as Nizam Shahi dynasty.[5]
