---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Buckytube
offline_file: ""
offline_thumbnail: ""
uuid: ad1e8ef9-bfd4-4716-9bfd-b7f92555b09f
updated: 1484308392
title: Carbon nanotube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Kohlenstoffnanoroehre_Animation_1.gif
tags:
    - Types of carbon nanotubes and related structures
    - Terminology
    - Single-walled
    - Multi-walled
    - Nanotube junctions
    - Torus
    - Nanobud
    - Three-dimensional carbon nanotube architectures
    - Graphenated carbon nanotubes (g-CNTs)
    - Nitrogen-doped carbon nanotubes
    - Peapod
    - Cup-stacked carbon nanotubes
    - Extreme carbon nanotubes
    - Properties
    - Strength
    - Hardness
    - Wettability
    - Kinetic properties
    - Electrical properties
    - Optical properties
    - Thermal properties
    - Defects
    - Safety and health
    - Synthesis
    - Chemical modification
    - Applications
    - Current
    - Potential
    - discovery
categories:
    - Uncategorized
---
Carbon nanotubes (CNTs) are allotropes of carbon with a cylindrical nanostructure. These cylindrical carbon molecules have unusual properties, which are valuable for nanotechnology, electronics, optics and other fields of materials science and technology. Owing to the material's exceptional strength and stiffness, nanotubes have been constructed with length-to-diameter ratio of up to 132,000,000:1,[1] significantly larger than for any other material.
