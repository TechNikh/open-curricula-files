---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vision
offline_file: ""
offline_thumbnail: ""
uuid: e65b020d-632f-4989-b5ac-ee8345ae09c6
updated: 1484310567
title: vision
categories:
    - Dictionary
---
vision
     n 1: a vivid mental image; "he had a vision of his own death"
     2: the ability to see; the faculty of vision [syn: {sight}, {visual
        sense}, {visual modality}]
     3: the perceptual experience of seeing; "the runners emerged
        from the trees into his clear vision"; "he had a visual
        sensation of intense light" [syn: {visual sensation}]
     4: the formation of a mental image of something that is not
        perceived as real and is not present to the senses;
        "popular imagination created a world of demons";
        "imagination reveals what the world could be" [syn: {imagination},
         {imaginativeness}]
     5: a religious or mystical ...
