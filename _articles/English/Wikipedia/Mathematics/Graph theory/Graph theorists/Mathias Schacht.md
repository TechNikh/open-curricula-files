---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mathias_Schacht
offline_file: ""
offline_thumbnail: ""
uuid: be9677a0-8ef0-407d-8625-2695b14c99fb
updated: 1484309476
title: Mathias Schacht
categories:
    - Graph theorists
---
Schacht earned a diploma in business mathematics in 1999 from the Technical University of Berlin.[1] He did his graduate studies at Emory University, completing a Ph.D. in 2004 under the supervision of Vojtěch Rödl.[2] His dissertation, on hypergraph generalizations of the Szemerédi regularity lemma, won the 2006 Richard Rado Prize of the German Mathematical Society.[1] He worked at the Humboldt University of Berlin as a postdoctoral researcher and acting professor from 2004 until 2009, when he moved to the University of Hamburg. He received his habilitation from Humboldt University in 2010, and has been Heisenberg Professor at the University of Hamburg from 2010 to 2015.[1]
