---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defensive
offline_file: ""
offline_thumbnail: ""
uuid: 7c95d42c-8b48-46e9-9aae-658bb29c42a0
updated: 1484310553
title: defensive
categories:
    - Dictionary
---
defensive
     adj 1: intended or appropriate for defending against or deterring
            aggression or attack; "defensive weapons"; "a
            defensive stance" [ant: {offensive}]
     2: attempting to justify or defend in speech or writing [syn: {justificative},
         {justificatory}]
     3: serving as or appropriate for defending or protecting;
        "defensive fortifications"; "defensive dikes to protect
        against floods"
     n : an attitude of defensiveness (especially in the phrase `on
         the defensive') [syn: {defensive attitude}]
