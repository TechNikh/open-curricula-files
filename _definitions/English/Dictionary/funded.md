---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funded
offline_file: ""
offline_thumbnail: ""
uuid: ea4cb76e-a448-47cf-a5b1-4761ef1fb9e3
updated: 1484310579
title: funded
categories:
    - Dictionary
---
funded
     adj : furnished with funds; "well-funded research" [ant: {unfunded}]
