---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polymake
offline_file: ""
offline_thumbnail: ""
uuid: c8976e49-c8df-4baf-8dd5-b1c9063c4790
updated: 1484309176
title: Polymake
tags:
    - Special Features
    - modular
    - rule based computation
    - scripting
    - Polymake applications
    - Common application
    - Fan application
    - Fulton application
    - Graph application
    - Group application
    - Ideal application
    - Matriod application
    - Polytope application
    - Topaz application
    - Tropical application
    - Development History
    - Software packages
    - Used within polymake
    - Used in conjunction with polymake
categories:
    - Computational geometry
---
Albeit primarily a tool to study the combinatorics and the geometry of convex polytopes and polyhedra, it is by now also capable of dealing with simplicial complexes, matroids, polyhedral fans, graphs, tropical objects, toric varieties and other objects.
