---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appearance
offline_file: ""
offline_thumbnail: ""
uuid: 7652b6da-0ea2-4481-b721-b47f7f15e429
updated: 1484310279
title: appearance
categories:
    - Dictionary
---
appearance
     n 1: outward or visible aspect of a person or thing [syn: {visual
          aspect}]
     2: the event of coming into sight [ant: {disappearance}]
     3: formal attendance (in court or at a hearing) of a party in
        an action [syn: {appearing}, {coming into court}]
     4: a mental representation; "I tried to describe his appearance
        to the police"
     5: the act of appearing in public view; "the rookie made a
        brief appearance in the first period"; "it was Bernhardt's
        last appearance in America" [ant: {disappearance}]
     6: pretending that something is the case in order to make a
        good impression; "they try to keep up appearances"; ...
