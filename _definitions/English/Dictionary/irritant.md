---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irritant
offline_file: ""
offline_thumbnail: ""
uuid: 10247c21-d85d-4e3d-ba7f-0276d0f23edc
updated: 1484310174
title: irritant
categories:
    - Dictionary
---
irritant
     n : something that causes irritation and annoyance; "he's a
         thorn in my flesh" [syn: {thorn}]
