---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indiscriminate
offline_file: ""
offline_thumbnail: ""
uuid: a1f2addc-63e6-4d89-86ca-f29edf3a5ba1
updated: 1484310258
title: indiscriminate
categories:
    - Dictionary
---
indiscriminate
     adj 1: failing to make or recognize distinctions
     2: not marked by fine distinctions; "indiscriminate reading
        habits"; "an indiscriminate mixture of colors and styles"
        [ant: {discriminate}]
