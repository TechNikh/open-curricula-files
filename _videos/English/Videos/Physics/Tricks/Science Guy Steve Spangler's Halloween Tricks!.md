---
version: 1
type: video
provider: YouTube
id: GRLOgmmz_EU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 53049a27-63f0-4e07-b591-4187ba537386
updated: 1486069660
title: "Science Guy Steve Spangler's Halloween Tricks!"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/GRLOgmmz_EU/default.jpg
    - https://i3.ytimg.com/vi/GRLOgmmz_EU/1.jpg
    - https://i3.ytimg.com/vi/GRLOgmmz_EU/2.jpg
    - https://i3.ytimg.com/vi/GRLOgmmz_EU/3.jpg
---
