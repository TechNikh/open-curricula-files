---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neck
offline_file: ""
offline_thumbnail: ""
uuid: b4425cdc-98bd-4b1d-a194-4875cd7f1a4f
updated: 1484310327
title: neck
categories:
    - Dictionary
---
neck
     n 1: the part of an organism that connects the head to the rest
          of the body; "he admired her long graceful neck" [syn: {cervix}]
     2: a narrow elongated projecting strip of land
     3: a cut of meat from the neck of an animal
     4: opening for the neck; the part of a garment near the neck
        opening
     v : kiss, embrace, or fondle with sexual passion; "The couple
         were necking in the back seat of the car" [syn: {make out}]
