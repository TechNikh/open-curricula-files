---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dismissal
offline_file: ""
offline_thumbnail: ""
uuid: ab87c399-9fde-4106-9f6a-6d54ca62dec1
updated: 1484310192
title: dismissal
categories:
    - Dictionary
---
dismissal
     n 1: a judgment disposing of the matter without a trial [syn: {judgment
          of dismissal}, {judgement of dismissal}]
     2: official notice that you have been fired from your job [syn:
         {dismission}, {pink slip}]
     3: permission to go; the sending away of someone
     4: the termination of someone's employment (leaving them free
        to depart) [syn: {dismission}, {discharge}, {firing}, {liberation},
         {release}, {sack}, {sacking}]
