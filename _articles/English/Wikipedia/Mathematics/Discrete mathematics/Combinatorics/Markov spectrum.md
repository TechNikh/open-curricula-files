---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Markov_spectrum
offline_file: ""
offline_thumbnail: ""
uuid: f97a92ec-7d38-4a1f-a3ca-7214608c3fbc
updated: 1484308588
title: Markov spectrum
tags:
    - Context
categories:
    - Combinatorics
---
In mathematics, the Markov spectrum devised by Andrey Markov is a complicated set of real numbers arising in the theory of diophantine approximation, and containing all the real numbers larger than Freiman's constant.[1]
