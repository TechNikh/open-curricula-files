---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lender
offline_file: ""
offline_thumbnail: ""
uuid: e3894338-28f8-4b13-8859-b7cc9c997e61
updated: 1484310451
title: lender
categories:
    - Dictionary
---
lender
     n : someone who lends money or gives credit in business matters
         [syn: {loaner}] [ant: {borrower}]
