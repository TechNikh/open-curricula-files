---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero-product_property
offline_file: ""
offline_thumbnail: ""
uuid: 66d12dd6-886a-4f02-b9aa-2bf10c190cb8
updated: 1484308909
title: Zero-product property
tags:
    - Algebraic context
    - Examples
    - Non-examples
    - Application to finding roots of polynomials
    - Notes
categories:
    - Elementary algebra
---
The zero-product property is also known as the rule of zero product, the null factor law or the nonexistence of nontrivial zero divisors. All of the number systems studied in elementary mathematics — the integers 
  
    
      
        
          Z
        
      
    
    {\displaystyle \mathbb {Z} }
  
, the rational numbers 
  
    
      
        
          Q
        
      
    
    {\displaystyle \mathbb {Q} }
  
, the real numbers 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
  
, and the complex numbers 
  
    
      
        
          C
        
      
    
    {\displaystyle \mathbb {C} }
  
 — satisfy the zero-product property. ...
