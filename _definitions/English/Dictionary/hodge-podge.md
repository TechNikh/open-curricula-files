---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hodge-podge
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495221
title: hodge-podge
categories:
    - Dictionary
---
hodgepodge
     n 1: a motley assortment of things [syn: {odds and ends}, {oddments},
           {melange}, {farrago}, {ragbag}, {mishmash}, {mingle-mangle},
           {hotchpotch}, {omnium-gatherum}]
     2: a theory or argument made up of miscellaneous or incongruous
        ideas [syn: {patchwork}, {jumble}]
