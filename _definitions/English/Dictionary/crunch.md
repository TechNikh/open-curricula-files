---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crunch
offline_file: ""
offline_thumbnail: ""
uuid: 7adbaa43-99eb-4850-a71d-c985637b054c
updated: 1484310148
title: crunch
categories:
    - Dictionary
---
crunch
     n 1: the sound of something crunching; "he heard the crunch of
          footsteps on the gravel path"
     2: a critical situation that arises because of a shortage (as a
        shortage of time or money or resources); "an end-of-the
        year crunch"; "a financial crunch"
     3: the act of crushing [syn: {crush}, {compaction}]
     v 1: make crunching noises; "his shoes were crunching on the
          gravel" [syn: {scranch}, {scraunch}, {crackle}]
     2: press or grind with a crunching noise [syn: {cranch}, {craunch},
         {grind}]
     3: chew noisily; "The children crunched the celery sticks"
        [syn: {munch}]
     4: reduce to small pieces or particles by ...
