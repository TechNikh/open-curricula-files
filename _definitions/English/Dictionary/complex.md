---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complex
offline_file: ""
offline_thumbnail: ""
uuid: ee655ea8-95f1-4b7f-9ada-4faaa3decb7a
updated: 1484310319
title: complex
categories:
    - Dictionary
---
complex
     adj : complicated in structure; consisting of interconnected
           parts; "a complex set of variations based on a simple
           folk melody"; "a complex mass of diverse laws and
           customs" [ant: {simple}]
     n 1: a conceptual whole made up of complicated and related parts;
          "the complex of shopping malls, houses, and roads
          created a new town" [syn: {composite}]
     2: a compound described in terms of the central atom to which
        other atoms are bound or coordinated [syn: {coordination
        compound}]
     3: (psychoanalysis) a combination of emotions and impulses that
        have been rejected from awareness but still influence ...
