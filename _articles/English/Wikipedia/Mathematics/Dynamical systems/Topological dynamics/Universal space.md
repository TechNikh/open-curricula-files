---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Universal_space
offline_file: ""
offline_thumbnail: ""
uuid: 6790ce95-41c7-4493-bda3-11f672f962ee
updated: 1484308878
title: Universal space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_8.png
tags:
    - Definition
    - Universal spaces in topological dynamics
categories:
    - Topological dynamics
---
In mathematics, a universal space is a certain metric space that contains all metric spaces whose dimension is bounded by some fixed constant. A similar definition exists in topological dynamics.
