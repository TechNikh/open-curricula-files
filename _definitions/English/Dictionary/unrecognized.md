---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unrecognized
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484607481
title: unrecognized
categories:
    - Dictionary
---
unrecognized
     adj 1: not recognized; "he was unrecognized in his disguise" [syn:
            {unrecognised}]
     2: not having a secure reputation; "short stories by
        unrecognized writiers" [syn: {unrecognised}]
