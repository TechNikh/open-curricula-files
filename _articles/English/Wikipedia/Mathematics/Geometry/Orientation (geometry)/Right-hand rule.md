---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Right-hand_rule
offline_file: ""
offline_thumbnail: ""
uuid: cb5b24b7-8ef7-48e1-8681-e67736e6b4e4
updated: 1484309300
title: Right-hand rule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Right-hand_grip_rule.svg.png
tags:
    - Coordinates
    - Rotation
    - A rotating body
    - Helices and screws
    - The Coriolis effect
    - Electromagnetics
    - "Ampère's right hand screw rule"
    - Application
    - Cross products
    - Applications
categories:
    - Orientation (geometry)
---
Most of the various left- and right-hand rules arise from the fact that the three axes of 3-dimensional space have two possible orientations. This can be seen by holding your hands together, palm up, with the fingers curled. If the curl of your fingers represents a movement from the first or X axis to the second or Y axis then the third or Z axis can point either along your left thumb or right thumb. Left- and right-hand rules arise when dealing with co-ordinate axes, rotation, spirals, electromagnetic fields, mirror images and enantiomers in mathematics and chemistry.
