---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/butene
offline_file: ""
offline_thumbnail: ""
uuid: 4f7baad7-e933-4171-803d-895584a2540d
updated: 1484310419
title: butene
categories:
    - Dictionary
---
butene
     n : any of three isomeric hydrocarbons C4H8; all used in making
         synthetic rubbers [syn: {butylene}]
