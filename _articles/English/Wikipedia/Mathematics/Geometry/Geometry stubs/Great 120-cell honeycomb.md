---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Great_120-cell_honeycomb
offline_file: ""
offline_thumbnail: ""
uuid: 74931f66-ba4e-4c7d-9391-bf1dc3a18a0e
updated: 1484309362
title: Great 120-cell honeycomb
categories:
    - Geometry stubs
---
In the geometry of hyperbolic 4-space, the great 120-cell honeycomb is one of four regular star-honeycombs. With Schläfli symbol {5,5/2,5,3}, it has three great 120-cells around each face. It is dual to the order-5 icosahedral 120-cell honeycomb.
