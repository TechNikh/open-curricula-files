---
version: 1
type: video
provider: YouTube
id: y4C81qAa3pY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Solving%20radical%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a727fe0a-ed6d-4f4f-9732-6adeb8b5402c
updated: 1486069559
title: Solving radical equations 1
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/y4C81qAa3pY/default.jpg
    - https://i3.ytimg.com/vi/y4C81qAa3pY/1.jpg
    - https://i3.ytimg.com/vi/y4C81qAa3pY/2.jpg
    - https://i3.ytimg.com/vi/y4C81qAa3pY/3.jpg
---
