---
version: 1
type: video
provider: YouTube
id: _k-gkl6QSI8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Earth%20Tide%20or%20Body%20Tide.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 09c14076-5b39-41dc-9291-c0c702cf6a78
updated: 1486069653
title: Earth Tide or Body Tide
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/_k-gkl6QSI8/default.jpg
    - https://i3.ytimg.com/vi/_k-gkl6QSI8/1.jpg
    - https://i3.ytimg.com/vi/_k-gkl6QSI8/2.jpg
    - https://i3.ytimg.com/vi/_k-gkl6QSI8/3.jpg
---
