---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spokesperson
offline_file: ""
offline_thumbnail: ""
uuid: 6204ca0a-6b3d-4e99-aed2-2bb870e25070
updated: 1484310589
title: spokesperson
categories:
    - Dictionary
---
spokesperson
     n : an advocate who represents someone else's policy or purpose;
         "the meeting was attended by spokespersons for all the
         major organs of government" [syn: {interpreter}, {representative},
          {voice}]
