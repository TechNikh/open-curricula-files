---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3-fold
offline_file: ""
offline_thumbnail: ""
uuid: 81d0751d-c9e4-402a-96f6-47c45e526601
updated: 1484309350
title: 3-fold
categories:
    - Geometry stubs
---
The Mori program showed that 3-folds have minimal models.
