---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnetic
offline_file: ""
offline_thumbnail: ""
uuid: 3c557eae-949c-4b1f-a285-a17a532a11c9
updated: 1484310196
title: magnetic
categories:
    - Dictionary
---
magnetic
     adj 1: of or relating to or caused by magnetism; "magnetic forces"
     2: having the properties of a magnet; i.e. of attracting iron
        or steel; "the hard disk is covered with a thin coat of
        magnetic material" [syn: {magnetized}, {magnetised}] [ant:
         {antimagnetic}]
     3: capable of being magnetized [ant: {nonmagnetic}]
     4: determined by earth's magnetic fields; "magnetic north";
        "the needle of a magnetic compass points to the magnetic
        north pole" [ant: {geographic}]
     5: having the properties of a magnet; the ability to draw or
        pull; "an attractive force"; "the knife hung on a magnetic
        board" [syn: ...
