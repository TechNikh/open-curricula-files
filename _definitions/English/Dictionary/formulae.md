---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formulae
offline_file: ""
offline_thumbnail: ""
uuid: ec23cbb3-0441-4956-b2f9-31fcac621fe2
updated: 1484310371
title: formulae
categories:
    - Dictionary
---
formula
     n 1: a group of symbols that make a mathematical statement [syn:
          {expression}]
     2: directions for making something [syn: {recipe}]
     3: a conventionalized statement expressing some fundamental
        principle
     4: a representation of a substance using symbols for its
        constituent elements [syn: {chemical formula}]
     5: something regarded as a normative example; "the convention
        of not naming the main character"; "violence is the rule
        not the exception"; "his formula for impressing visitors"
        [syn: {convention}, {normal}, {pattern}, {rule}]
     6: a liquid food for infants
     7: (mathematics) a standard procedure for ...
