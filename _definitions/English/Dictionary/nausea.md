---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nausea
offline_file: ""
offline_thumbnail: ""
uuid: 9c1b4c9a-14bc-4230-b00b-7d47493321fd
updated: 1484310443
title: nausea
categories:
    - Dictionary
---
nausea
     n 1: the state that precedes vomiting [syn: {sickness}]
     2: disgust so strong it makes you feel sick
