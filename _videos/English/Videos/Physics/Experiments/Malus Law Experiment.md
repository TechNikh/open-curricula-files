---
version: 1
type: video
provider: YouTube
id: OgImgIJgDA0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dfbda9e1-ae5b-4b1c-ba44-b0aa65166147
updated: 1486069637
title: Malus Law Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/OgImgIJgDA0/default.jpg
    - https://i3.ytimg.com/vi/OgImgIJgDA0/1.jpg
    - https://i3.ytimg.com/vi/OgImgIJgDA0/2.jpg
    - https://i3.ytimg.com/vi/OgImgIJgDA0/3.jpg
---
