---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heartily
offline_file: ""
offline_thumbnail: ""
uuid: 40491c00-3666-445e-a506-9e8f7976c239
updated: 1484310148
title: heartily
categories:
    - Dictionary
---
heartily
     adv 1: with gusto and without reservation; "the boy threw himself
            heartily into his work"
     2: in a hearty manner; "`Yes,' the children chorused heartily";
        "We welcomed her warmly" [syn: {cordially}, {warmly}]
