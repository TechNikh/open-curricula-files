---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/displace
offline_file: ""
offline_thumbnail: ""
uuid: b753cb94-97df-4c05-b933-5c4f85dc5674
updated: 1484310373
title: displace
categories:
    - Dictionary
---
displace
     v 1: take the place of
     2: force to move; "the refugees were displaced by the war"
        [syn: {force out}]
     3: move (people) forcibly from their homeland into a new and
        foreign environment; "The war uprooted many people" [syn:
        {uproot}, {deracinate}]
     4: cause to move, both in a concrete and in an abstract sense;
        "Move those boxes into the corner, please"; "I'm moving my
        money to another bank"; "The director moved more
        responsibilities onto his new assistant" [syn: {move}]
     5: remove or force from a position of dwelling previously
        occupied; "The new employee dislodged her by moving into
        her office ...
