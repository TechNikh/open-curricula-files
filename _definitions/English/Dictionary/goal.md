---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/goal
offline_file: ""
offline_thumbnail: ""
uuid: 008f887a-587a-47c1-838f-bb2a65f21d63
updated: 1484310443
title: goal
categories:
    - Dictionary
---
goal
     n 1: the state of affairs that a plan is intended to achieve and
          that (when achieved) terminates behavior intended to
          achieve it; "the ends justify the means" [syn: {end}]
     2: a successful attempt at scoring; "the winning goal came with
        less than a minute left to play"
     3: game equipment consisting of the place toward which players
        of a game try to advance a ball or puck in order to score
        points
     4: the place designated as the end (as of a race or journey);
        "a crowd assembled at the finish"; "he was nearly
        exhuasted as their destination came into view" [syn: {finish},
         {destination}]
