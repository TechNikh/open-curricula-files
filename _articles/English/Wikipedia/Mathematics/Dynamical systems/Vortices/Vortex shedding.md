---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_shedding
offline_file: ""
offline_thumbnail: ""
uuid: 1121b68d-8c55-481e-85b9-f5df5dd61f66
updated: 1484308879
title: Vortex shedding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Vortex-street-animation.gif
tags:
    - Governing equation
    - Mitigation of vortex shedding effects
categories:
    - Vortices
---
In fluid dynamics, vortex shedding is an oscillating flow that takes place when a fluid such as air or water flows past a bluff (as opposed to streamlined) body at certain velocities, depending on the size and shape of the body. In this flow, vortices are created at the back of the body and detach periodically from either side of the body. See Von Kármán vortex street. The fluid flow past the object creates alternating low-pressure vortices on the downstream side of the object. The object will tend to move toward the low-pressure zone.
