---
version: 1
type: video
provider: YouTube
id: fVIZmOQBS5M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/U09_L1_T2_we3%20Factoring%20Trinomials%20by%20Grouping%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7e718a5e-83fb-4751-891a-46cc3a2b69c5
updated: 1486069557
title: U09_L1_T2_we3 Factoring Trinomials by Grouping 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/fVIZmOQBS5M/default.jpg
    - https://i3.ytimg.com/vi/fVIZmOQBS5M/1.jpg
    - https://i3.ytimg.com/vi/fVIZmOQBS5M/2.jpg
    - https://i3.ytimg.com/vi/fVIZmOQBS5M/3.jpg
---
