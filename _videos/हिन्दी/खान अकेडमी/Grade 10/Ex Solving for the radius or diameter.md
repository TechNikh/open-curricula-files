---
version: 1
type: video
provider: YouTube
id: pcl4qw8uCiU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ce5c15f2-d73c-4f8a-8038-7500d1e3eee6
updated: 1486069617
title: Ex Solving for the radius or diameter
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/pcl4qw8uCiU/default.jpg
    - https://i3.ytimg.com/vi/pcl4qw8uCiU/1.jpg
    - https://i3.ytimg.com/vi/pcl4qw8uCiU/2.jpg
    - https://i3.ytimg.com/vi/pcl4qw8uCiU/3.jpg
---
