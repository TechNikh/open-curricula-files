---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mother
offline_file: ""
offline_thumbnail: ""
uuid: dc3c8ea3-6f79-4cb7-bc6b-f37d7d2a971f
updated: 1484310309
title: mother
categories:
    - Dictionary
---
mother
     n 1: a woman who has given birth to a child (also used as a term
          of address to your mother); "the mother of three
          children" [syn: {female parent}] [ant: {father}, {father}]
     2: a stringy slimy substance consisting of yeast cells and
        bacteria; forms during fermentation and is added to cider
        or wine to produce vinegar
     3: a term of address for an elderly woman
     4: a condition that is the inspiration for an activity or
        situation; "necessity is the mother of invention"
     v 1: care for like a mother; "She fusses over her husband" [syn:
          {fuss}, {overprotect}]
     2: make children; "Abraham begot Isaac"; "Men often ...
