---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrestrial
offline_file: ""
offline_thumbnail: ""
uuid: 5ec5d02c-540d-4ad5-b536-05dfd85bc00e
updated: 1484310275
title: terrestrial
categories:
    - Dictionary
---
terrestrial
     adj 1: of or relating to or inhabiting the land as opposed to the
            sea or air [syn: {tellurian}, {telluric}, {terrene}]
     2: of or relating to or characteristic of the planet Earth or
        its inhabitants; "planetary rumblings and eructations"-
        L.C.Eiseley ; "the planetary tilt"; "this terrestrial
        ball" [syn: {planetary}]
     3: operating or living or growing on land [syn: {land(a)}]
        [ant: {amphibious}, {aquatic}]
     4: concerned with the world or worldly matters; "mundane
        affairs"; "he developed an immense terrestrial
        practicality" [syn: {mundane}]
     5: of this earth; "transcendental motives for sublunary
      ...
