---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Propyne
offline_file: ""
offline_thumbnail: ""
uuid: 31516610-0f7c-41db-9bac-214b7293d5e5
updated: 1484308401
title: Propyne
tags:
    - Production and equilibrium with propadiene
    - Laboratory methods
    - Use as a rocket fuel
    - Organic chemistry
categories:
    - Uncategorized
---
Propyne (methylacetylene) is an alkyne with the chemical formula CH3C≡CH. It was a component of MAPP gas—along with its isomer propadiene (allene), which was commonly used in gas welding. Unlike acetylene, propyne can be safely condensed.[3]
