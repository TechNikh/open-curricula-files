---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intensity
offline_file: ""
offline_thumbnail: ""
uuid: 9fde8b89-f617-4f48-862b-e2199edb43d3
updated: 1484310218
title: intensity
categories:
    - Dictionary
---
intensity
     n 1: the amount of energy transmitted (as by acoustic or
          electromagnetic radiation); "he adjusted the intensity
          of the sound"; "they measured the station's signal
          strength" [syn: {strength}, {intensity level}]
     2: high level or degree; the property of being intense [syn: {intensiveness}]
     3: the magnitude of sound (usually in a specified direction);
        "the kids played their music at full volume" [syn: {volume},
         {loudness}] [ant: {softness}]
     4: chromatic purity: freedom from dilution with white and hence
        vividness of hue [syn: {saturation}, {chroma}, {vividness}]
