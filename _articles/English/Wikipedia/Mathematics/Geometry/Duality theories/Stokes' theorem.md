---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stokes%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5b3ba042-5a76-4c84-b717-2c5547f8ba37
updated: 1484309237
title: "Stokes' theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Green%2527s-theorem-simple-region.svg.png'
tags:
    - Introduction
    - Formulation for smooth manifolds with boundary
    - Topological preliminaries; integration over chains
    - Underlying principle
    - Generalization to rough sets
    - Special cases
    - Kelvin–Stokes theorem
    - "Green's theorem"
    - In electromagnetism
    - Divergence theorem
    - Notes
categories:
    - Duality theories
---
In vector calculus, and more generally differential geometry, Stokes' theorem (also called the generalized Stokes' theorem) is a statement about the integration of differential forms on manifolds, which both simplifies and generalizes several theorems from vector calculus. Stokes' theorem says that the integral of a differential form ω over the boundary of some orientable manifold Ω is equal to the integral of its exterior derivative dω over the whole of Ω, i.e.,
