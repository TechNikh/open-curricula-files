---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emulsify
offline_file: ""
offline_thumbnail: ""
uuid: 65a55e58-0247-46c2-a6ce-a682d7ab9bd8
updated: 1484310426
title: emulsify
categories:
    - Dictionary
---
emulsify
     v 1: cause to become an emulsion; make into an emulsion [ant: {demulsify}]
     2: form into or become an emulsion; "The solution emulsified"
        [ant: {demulsify}]
     [also: {emulsified}]
