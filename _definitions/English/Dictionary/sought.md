---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sought
offline_file: ""
offline_thumbnail: ""
uuid: 55a3ba14-6f33-4b83-86ab-1b6ecb40bca8
updated: 1484310543
title: sought
categories:
    - Dictionary
---
seek
     n : the movement of a read/write head to a specific data track
         on a disk
     v 1: try to get or reach; "seek a position"; "seek an education";
          "seek happiness"
     2: try to locate or discover, or try to establish the existence
        of; "The police are searching for clues"; "They are
        searching for the missing man in the entire county" [syn:
        {search}, {look for}]
     3: make an effort or attempt; "He tried to shake off his
        fears"; "The infant had essayed a few wobbly steps"; "The
        police attempted to stop the thief"; "He sought to improve
        himself"; "She always seeks to do good in the world" [syn:
         {try}, ...
