---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dong
offline_file: ""
offline_thumbnail: ""
uuid: d3898ba1-623b-4179-8e52-5c086e517d6f
updated: 1484310577
title: dong
categories:
    - Dictionary
---
dong
     n : the basic unit of money in Vietnam
     v : go `ding dong', like a bell [syn: {ding}, {dingdong}]
