---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relationship
offline_file: ""
offline_thumbnail: ""
uuid: 6a3579de-ef7d-4912-8499-d7747c1af94a
updated: 1484310346
title: relationship
categories:
    - Dictionary
---
relationship
     n 1: a relation between people; (`relationship' is often used
          where `relation' would serve, as in `the relationship
          between inflation and unemployment', but the preferred
          usage of `relationship' is for human relations or states
          of relatedness); "the relationship between mothers and
          their children" [syn: {human relationship}]
     2: a state of connectedness between people (especially an
        emotional connection); "he didn't want his wife to know of
        the relationship"
     3: a state involving mutual dealings between people or parties
        or countries
     4: state of relatedness or connection by blood or ...
