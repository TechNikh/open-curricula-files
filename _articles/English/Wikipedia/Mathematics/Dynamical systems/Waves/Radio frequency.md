---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radio_frequency
offline_file: ""
offline_thumbnail: ""
uuid: 410f16ae-f770-47bc-bd80-5f764a6384dc
updated: 1484308894
title: Radio frequency
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon_0.png
tags:
    - Special properties of RF current
    - Radio communication
    - Frequency bands
    - In medicine
    - Effects on the human body
    - Extremely low frequency RF
    - Microwaves
    - General RF exposure
    - As a weapon
    - Measurement
categories:
    - Waves
---
Radio frequency (RF) is any of the electromagnetic wave frequencies that lie in the range extending from around 7003300000000000000♠3 kHz to 7011300000000000000♠300 GHz, which include those frequencies used for communications or radar signals.[1] RF usually refers to electrical rather than mechanical oscillations. However, mechanical RF systems do exist (see mechanical filter and RF MEMS).
