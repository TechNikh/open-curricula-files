---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burden
offline_file: ""
offline_thumbnail: ""
uuid: 52ac9737-d9fe-466f-bdaf-b8940ceff9ab
updated: 1484310471
title: burden
categories:
    - Dictionary
---
burden
     n 1: an onerous or difficult concern; "the burden of
          responsibility"; "that's a load off my mind" [syn: {load},
           {encumbrance}, {incumbrance}, {onus}]
     2: weight to be borne or conveyed [syn: {load}, {loading}]
     3: the central meaning or theme of a speech or literary work
        [syn: {effect}, {essence}, {core}, {gist}]
     4: the central idea that is expanded in a document or discourse
     v 1: weight down with a load [syn: {burthen}, {weight}, {weight
          down}] [ant: {unburden}]
     2: impose a task upon, assign a responsibility to; "He charged
        her with cleaning up all the files over the weekend" [syn:
         {charge}, ...
