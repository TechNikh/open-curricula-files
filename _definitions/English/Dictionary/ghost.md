---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ghost
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453401
title: ghost
categories:
    - Dictionary
---
ghost
     n 1: a mental representation of some haunting experience; "he
          looked like he had seen a ghost"; "it aroused specters
          from his past" [syn: {shade}, {spook}, {wraith}, {specter},
           {spectre}]
     2: a writer who gives the credit of authorship to someone else
        [syn: {ghostwriter}]
     3: the visible disembodied soul of a dead person
     4: a suggestion of some quality; "there was a touch of sarcasm
        in his tone"; "he detected a ghost of a smile on her face"
        [syn: {touch}, {trace}]
     v 1: move like a ghost; "The masked men ghosted across the
          moonlit yard"
     2: haunt like a ghost; pursue; "Fear of illness haunts ...
