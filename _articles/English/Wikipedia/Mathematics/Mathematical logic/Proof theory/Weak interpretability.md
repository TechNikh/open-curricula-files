---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weak_interpretability
offline_file: ""
offline_thumbnail: ""
uuid: 560bbfcc-d7bb-4806-8782-d69cdbba9f5e
updated: 1484309499
title: Weak interpretability
categories:
    - Proof theory
---
In mathematical logic, weak interpretability is a notion of translation of logical theories, introduced together with interpretability by Alfred Tarski in 1953.
