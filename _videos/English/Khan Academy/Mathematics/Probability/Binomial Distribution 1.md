---
version: 1
type: video
provider: YouTube
id: O12yTz_8EOw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Binomial%20Distribution%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c1e3b76b-e2b3-4b71-8694-6e2b160c7a25
updated: 1486069689
title: Binomial Distribution 1
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/O12yTz_8EOw/default.jpg
    - https://i3.ytimg.com/vi/O12yTz_8EOw/1.jpg
    - https://i3.ytimg.com/vi/O12yTz_8EOw/2.jpg
    - https://i3.ytimg.com/vi/O12yTz_8EOw/3.jpg
---
