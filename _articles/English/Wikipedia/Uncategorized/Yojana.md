---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yojana
offline_file: ""
offline_thumbnail: ""
uuid: d13f8e31-1b03-47c0-8f45-fb350a5a49b3
updated: 1484308438
title: Yojana
tags:
    - Yojana as per "Vishnu Purana"
    - Clearly defined
    - Variations on length
categories:
    - Uncategorized
---
A Yojana (Sanskrit : योजन) is a Vedic measure of distance that was used in ancient India. A Yojana is about 12–15 km. (i.e. 4 Kosh = 1 Yojana and 1 kosh is 2 - 3.5 km)
