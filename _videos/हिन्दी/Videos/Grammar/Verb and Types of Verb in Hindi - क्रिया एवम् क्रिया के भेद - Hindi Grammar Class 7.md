---
version: 1
type: video
provider: YouTube
id: muFnKNMnBNg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 68a78794-f689-4f5b-80d9-455d80c2bf65
updated: 1486069630
title: >
    Verb and Types of Verb in Hindi / क्रिया
    एवम् क्रिया के भेद | Hindi
    Grammar Class 7
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/muFnKNMnBNg/default.jpg
    - https://i3.ytimg.com/vi/muFnKNMnBNg/1.jpg
    - https://i3.ytimg.com/vi/muFnKNMnBNg/2.jpg
    - https://i3.ytimg.com/vi/muFnKNMnBNg/3.jpg
---
