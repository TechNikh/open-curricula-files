---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weighing
offline_file: ""
offline_thumbnail: ""
uuid: 6dd72a07-92bc-45a7-b85f-9b72af8e3836
updated: 1484310416
title: weighing
categories:
    - Dictionary
---
weighing
     n : careful consideration; "a little deliberation would have
         deterred them" [syn: {deliberation}, {advisement}]
