---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exact_cover
offline_file: ""
offline_thumbnail: ""
uuid: cba08c07-4507-490f-b871-263c6a783034
updated: 1484308674
title: Exact cover
tags:
    - Formal definition
    - Basic examples
    - Detailed example
    - Representations
    - Standard representation
    - Inverse representation
    - Matrix and hypergraph representations
    - Graph representation
    - Equivalent problems
    - Exact hitting set
    - Exact hitting set example
    - Dual example
    - Finding solutions
    - Generalizations
    - Noteworthy examples
    - Pentomino tiling
    - Sudoku
    - N queens problem
categories:
    - Theoretical computer science
---
In mathematics, given a collection 
  
    
      
        
          
            S
          
        
      
    
    {\displaystyle {\mathcal {S}}}
  
 of subsets of a set X, an exact cover is a subcollection 
  
    
      
        
          
            
              S
            
          
          
            ∗
          
        
      
    
    {\displaystyle {\mathcal {S}}^{*}}
  
 of 
  
    
      
        
          
            S
          
        
      
    
    {\displaystyle {\mathcal {S}}}
  
 such that each element in X is contained in exactly one subset in 
  
    
      
        
          
            
              S
            
          
          
      ...
