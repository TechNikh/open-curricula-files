---
version: 1
type: video
provider: YouTube
id: ZVgBO2cEFiQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 08a0d0e4-4778-4ef9-ac4d-b2a5ab4365c3
updated: 1486069637
title: 'Experiment- Lichtbrechung'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZVgBO2cEFiQ/default.jpg
    - https://i3.ytimg.com/vi/ZVgBO2cEFiQ/1.jpg
    - https://i3.ytimg.com/vi/ZVgBO2cEFiQ/2.jpg
    - https://i3.ytimg.com/vi/ZVgBO2cEFiQ/3.jpg
---
