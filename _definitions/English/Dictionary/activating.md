---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/activating
offline_file: ""
offline_thumbnail: ""
uuid: 9a5442fe-d92d-45fd-b949-91c698426fc2
updated: 1484310188
title: activating
categories:
    - Dictionary
---
activating
     adj : causing motion or action or change [syn: {activating(a)}, {actuating(a)}]
     n : the activity of causing to have energy and be active [syn: {energizing},
          {activation}]
