---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worship
offline_file: ""
offline_thumbnail: ""
uuid: 41e0e7b7-3205-4ed8-bd71-e45f28f95f85
updated: 1484310545
title: worship
categories:
    - Dictionary
---
worship
     n 1: the activity of worshipping
     2: a feeling of profound love and admiration [syn: {adoration}]
     v 1: love unquestioningly and uncritically or to excess; venerate
          as an idol; "Many teenagers idolized the Beatles" [syn:
          {idolize}, {idolise}, {hero-worship}, {revere}]
     2: show devotion to (a deity); "Many Hindus worship Shiva"
     3: attend religious services; "They worship in the traditional
        manner"
     [also: {worshipping}, {worshipped}]
