---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quotient
offline_file: ""
offline_thumbnail: ""
uuid: a9ee30cb-abf9-4099-abb9-0d8dc81686c7
updated: 1484310142
title: quotient
categories:
    - Dictionary
---
quotient
     n 1: the ratio of two quantities to be divided
     2: the number obtained by division
