---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/battlefield
offline_file: ""
offline_thumbnail: ""
uuid: c0b23a04-4761-4de2-8a19-ef17cdb92e90
updated: 1484310563
title: battlefield
categories:
    - Dictionary
---
battlefield
     n : a region where a battle is being (or has been) fought; "they
         made a tour of Civil War battlefields" [syn: {battleground},
          {field of battle}, {field of honor}, {field}]
