---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Technical_illustration
offline_file: ""
offline_thumbnail: ""
uuid: 1976728f-b398-406d-9b83-b22d6964cac8
updated: 1484309336
title: Technical illustration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Interface_lg.jpg
tags:
    - Types of technical illustrations
    - Types of communication
    - Types of drawings
    - Techniques
categories:
    - Technical drawing
---
Technical Illustration is the use of illustration to visually communicate information of a technical nature. Technical illustrations can be components of technical drawings or diagrams. Technical illustrations in general aim "to generate expressive images that effectively convey certain information via the visual channel to the human observer".[1]
