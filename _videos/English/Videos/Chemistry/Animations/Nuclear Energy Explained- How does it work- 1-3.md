---
version: 1
type: video
provider: YouTube
id: rcOFV4y5z8c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Nuclear%20Energy%20Explained-%20How%20does%20it%20work-%201-3.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92a84c7e-d78e-4250-9bc0-c8f13d86bddd
updated: 1486069664
title: 'Nuclear Energy Explained- How does it work? 1/3'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rcOFV4y5z8c/default.jpg
    - https://i3.ytimg.com/vi/rcOFV4y5z8c/1.jpg
    - https://i3.ytimg.com/vi/rcOFV4y5z8c/2.jpg
    - https://i3.ytimg.com/vi/rcOFV4y5z8c/3.jpg
---
