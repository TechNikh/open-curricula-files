---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/place
offline_file: ""
offline_thumbnail: ""
uuid: eaf7424d-bfb2-4089-a826-cac9c11a210f
updated: 1484310344
title: place
categories:
    - Dictionary
---
place
     n 1: a point located with respect to surface features of some
          region; "this is a nice place for a picnic" [syn: {topographic
          point}, {spot}]
     2: any area set aside for a particular purpose; "who owns this
        place?"; "the president was concerned about the property
        across from the White House" [syn: {property}]
     3: an abstract mental location; "he has a special place in my
        thoughts"; "a place in my heart"; "a political system with
        no place for the less prominent groups"
     4: a general vicinity; "He comes from a place near Chicago"
     5: the function or position properly or customarily occupied or
        served by ...
