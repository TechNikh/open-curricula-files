---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chirpily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449921
title: chirpily
categories:
    - Dictionary
---
chirpily
     adv : in a cheerfully buoyant manner; "we accepted the opportunity
           buoyantly" [syn: {buoyantly}]
