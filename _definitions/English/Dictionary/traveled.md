---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traveled
offline_file: ""
offline_thumbnail: ""
uuid: 6121b83b-d46f-4366-8e10-7f1eddb00f04
updated: 1484310277
title: traveled
categories:
    - Dictionary
---
traveled
     adj 1: traveled over or through; sometimes used as a combining term
            [ant: {untraveled}]
     2: familiar with many parts of the world; "a traveled, educated
        man"; "well-traveled people" [syn: {travelled}]
