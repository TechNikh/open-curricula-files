---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/edema
offline_file: ""
offline_thumbnail: ""
uuid: ef6f235c-17b2-4b42-8fc5-02593c19434a
updated: 1484310158
title: edema
categories:
    - Dictionary
---
edema
     n : swelling from excessive accumulation of serous fluid in
         tissue [syn: {oedema}, {hydrops}, {dropsy}]
     [also: {oedemata} (pl), {edemata} (pl)]
