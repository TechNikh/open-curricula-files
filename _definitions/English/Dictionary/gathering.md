---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gathering
offline_file: ""
offline_thumbnail: ""
uuid: 4c58dfd1-8b04-4bae-addc-42e5640f0977
updated: 1484310249
title: gathering
categories:
    - Dictionary
---
gathering
     adj : accumulating and becoming more intense; "the deepening
           gloom"; "felt a deepening love"; "the gathering
           darkness"; "the thickening dusk" [syn: {deepening(a)},
           {gathering(a)}, {thickening(a)}]
     n 1: a group of persons together in one place [syn: {assemblage}]
     2: the social act of assembling; "they demanded the right of
        assembly" [syn: {assembly}, {assemblage}] [ant: {dismantling}]
     3: the act of gathering something [syn: {gather}]
     4: sewing consisting of small folds or puckers made by pulling
        tight a thread in a line of stitching [syn: {gather}]
