---
version: 1
type: video
provider: YouTube
id: Rw_pDVbnfQk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Covalent%20networks%2C%20metallic%20crystals%2C%20and%20ionic%20crystals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 89ff41a5-7c71-4e6c-a686-5c34307ea5da
updated: 1486069585
title: Covalent networks, metallic crystals, and ionic crystals
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/Rw_pDVbnfQk/default.jpg
    - https://i3.ytimg.com/vi/Rw_pDVbnfQk/1.jpg
    - https://i3.ytimg.com/vi/Rw_pDVbnfQk/2.jpg
    - https://i3.ytimg.com/vi/Rw_pDVbnfQk/3.jpg
---
Covalent networks, metallic crystals, and ionic crystals: Some of the strongest molecular structures.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-bonds/copy-of-dot-structures/v/drawing-dot-structures?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-bonds/types-chemical-bonds/v/metallic-nature-trends?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
