---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/far
offline_file: ""
offline_thumbnail: ""
uuid: 48693121-bf30-4906-9201-7c23902b2d10
updated: 1484310319
title: far
categories:
    - Dictionary
---
far
     adj 1: at a great distance in time or space or degree; "we come
            from a far country"; "far corners of the earth"; "the
            far future"; "a far journey"; "the far side of the
            road"; "far from the truth"; "far in the future" [ant:
             {near}]
     2: being of a considerable distance or length; "a far trek"
     3: being the animal or vehicle on the right or being on the
        right side of an animal or vehicle; "the horse on the
        right is the far horse"; "the right side is the far side
        of the horse"
     4: beyond a norm in opinion or actions; "the far right"
     n : a terrorist organization that seeks to overthrow the
        ...
