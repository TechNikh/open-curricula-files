---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fate
offline_file: ""
offline_thumbnail: ""
uuid: 4dd2969b-ca03-462b-b986-af8faaf74191
updated: 1484310319
title: fate
categories:
    - Dictionary
---
fate
     n 1: an event (or a course of events) that will inevitably happen
          in the future [syn: {destiny}]
     2: the ultimate agency that predetermines the course of events
        (often personified as a woman); "we are helpless in the
        face of Destiny" [syn: {Destiny}]
     3: your overall circumstances or condition in life (including
        everything that happens to you); "whatever my fortune may
        be"; "deserved a better fate"; "has a happy lot"; "the
        luck of the Irish"; "a victim of circumstances"; "success
        that was her portion" [syn: {fortune}, {destiny}, {luck},
        {lot}, {circumstances}, {portion}]
     v : decree or designate ...
