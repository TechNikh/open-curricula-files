---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stripes
offline_file: ""
offline_thumbnail: ""
uuid: a520305c-54f6-4294-b113-5d9a84787044
updated: 1484310206
title: stripes
categories:
    - Dictionary
---
stripes
     n : V-shaped sleeve badge indicating military rank and service;
         "they earned their stripes in Kuwait" [syn: {chevron}, {stripe},
          {grade insignia}]
