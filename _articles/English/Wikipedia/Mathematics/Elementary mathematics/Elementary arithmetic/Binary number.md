---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binary_number
offline_file: ""
offline_thumbnail: ""
uuid: 51fb34ba-a88a-48f7-a9f3-1f2dc4f59436
updated: 1484308918
title: Binary number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Bagua-name-earlier.svg.png
tags:
    - History
    - Egypt
    - China
    - India
    - Other cultures
    - Western predecessors to Leibniz
    - Leibniz and the I Ching
    - Later developments
    - Representation
    - Counting in binary
    - Decimal counting
    - Binary counting
    - Fractions
    - Binary arithmetic
    - Addition
    - Long carry method
    - Addition table
    - Subtraction
    - Multiplication
    - Multiplication table
    - Division
    - Square root
    - Bitwise operations
    - Conversion to and from other numeral systems
    - Decimal
    - Hexadecimal
    - Octal
    - Representing real numbers
    - Notes
categories:
    - Elementary arithmetic
---
In mathematics and digital electronics, a binary number is a number expressed in the binary numeral system or base-2 numeral system which represents numeric values using two different symbols: typically 0 (zero) and 1 (one). The base-2 system is a positional notation with a radix of 2. Because of its straightforward implementation in digital electronic circuitry using logic gates, the binary system is used internally by almost all modern computers and computer-based devices. Each digit is referred to as a bit.
