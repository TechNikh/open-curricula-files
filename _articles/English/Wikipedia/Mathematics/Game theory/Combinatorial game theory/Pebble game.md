---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pebble_game
offline_file: ""
offline_thumbnail: ""
uuid: ff66b490-1c53-4311-84c9-1398983856b3
updated: 1484308533
title: Pebble game
categories:
    - Combinatorial game theory
---
In mathematics and computer science, a pebble game is a type of mathematical game played by moving "pebbles" or "markers" on a directed graph. A variety of different pebble games exist. A general definition of pebbling is given below.
