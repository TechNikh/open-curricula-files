---
version: 1
type: video
provider: YouTube
id: 4mUjM1qMaa8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Doppler%20effect%20-%20listener%20in%20motion.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c748602d-ef55-41dd-90ae-76d4c804b715
updated: 1486069653
title: 'Doppler effect - listener in motion'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/4mUjM1qMaa8/default.jpg
    - https://i3.ytimg.com/vi/4mUjM1qMaa8/1.jpg
    - https://i3.ytimg.com/vi/4mUjM1qMaa8/2.jpg
    - https://i3.ytimg.com/vi/4mUjM1qMaa8/3.jpg
---
