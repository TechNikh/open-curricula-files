---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/octet
offline_file: ""
offline_thumbnail: ""
uuid: 618fd72b-3395-468d-bd34-33a96f237b95
updated: 1484310399
title: octet
categories:
    - Dictionary
---
octet
     n 1: the cardinal number that is the sum of seven and one [syn: {eight},
           {8}, {VIII}, {eighter}, {eighter from Decatur}, {octad},
           {ogdoad}, {octonary}]
     2: eight performers or singers who perform together [syn: {octette}]
     3: a set of eight similar things considered as a unit [syn: {octette}]
     4: eight people considered as a unit [syn: {octette}, {eightsome}]
     5: a musical composition written for eight performers [syn: {octette}]
