---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substituting
offline_file: ""
offline_thumbnail: ""
uuid: da07b2c0-76e8-4fd5-ab85-d8b2aa9c9020
updated: 1484310214
title: substituting
categories:
    - Dictionary
---
substituting
     n : working as a substitute for someone who is ill or on leave
         of absence [syn: {subbing}]
