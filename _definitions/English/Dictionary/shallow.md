---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shallow
offline_file: ""
offline_thumbnail: ""
uuid: a7aed7c8-2611-4d68-8ba4-074a9400c55e
updated: 1484310226
title: shallow
categories:
    - Dictionary
---
shallow
     adj 1: lacking physical depth; having little spatial extension
            downward or inward from an outer surface or backward
            or outward from a center; "shallow water"; "a shallow
            dish"; "a shallow cut"; "a shallow closet";
            "established a shallow beachhead"; "hit the ball to
            shallow left field" [ant: {deep}]
     2: not deep or strong; not affecting one deeply; "shallow
        breathing"; "a night of shallow fretful sleep"; "in a
        shallow trance" [ant: {deep}]
     3: lacking depth of intellect or knowledge; concerned only with
        what is obvious; "shallow people"; "his arguments seemed
        shallow and tedious"
 ...
