---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/short
offline_file: ""
offline_thumbnail: ""
uuid: 6b7ae1b4-8c08-494c-8eb7-a3866179e939
updated: 1484310305
title: short
categories:
    - Dictionary
---
short
     adj 1: primarily temporal sense; indicating or being or seeming to
            be limited in duration; "a short life"; "a short
            flight"; "a short holiday"; "a short story"; "only a
            few short months" [ant: {long}]
     2: primarily spatial sense; having little length or lacking in
        length; "short skirts"; "short hair"; "the board was a
        foot short"; "a short toss" [ant: {long}]
     3: low in stature; not tall; "his was short and stocky"; "short
        in stature"; "a short smokestack" [ant: {tall}]
     4: not sufficient to meet a need; "an inadequate income"; "a
        poor salary"; "money is short"; "on short rations"; "food
        is in ...
