---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outflow
offline_file: ""
offline_thumbnail: ""
uuid: fa3bbcab-a0cf-424e-8d42-41807a524987
updated: 1484310459
title: outflow
categories:
    - Dictionary
---
outflow
     n 1: the unwanted discharge of a fluid from some container; "they
          tried to stop the escape of gas from the damaged pipe";
          "he had to clean up the leak" [syn: {escape}, {leak}, {leakage}]
     2: the process of flowing out [syn: {effluence}, {efflux}]
        [ant: {inflow}, {inflow}]
     3: a natural flow of ground water [syn: {spring}, {fountain}, {outpouring},
         {natural spring}]
