---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamental_theorem_of_poker
offline_file: ""
offline_thumbnail: ""
uuid: 15a2ccaa-e480-4663-95e8-b838dfec080e
updated: 1484309059
title: Fundamental theorem of poker
categories:
    - Poker strategy
---
The fundamental theorem of poker is a principle first articulated by David Sklansky that he believes expresses the essential nature of poker as a game of decision-making in the face of incomplete information.
