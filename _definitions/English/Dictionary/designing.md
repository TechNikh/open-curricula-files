---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/designing
offline_file: ""
offline_thumbnail: ""
uuid: 409bf509-35bf-4577-ad25-a3cd900317a7
updated: 1484310527
title: designing
categories:
    - Dictionary
---
designing
     adj : concealing crafty designs for advancing your own interest;
           "a selfish and designing nation obsessed with the dark
           schemes of European intrigue"- W.Churchill; "a scheming
           wife"; "a scheming gold digger" [syn: {scheming}]
     n : the act of working out the form of something (as by making a
         sketch or outline or plan); "he contributed to the design
         of a new instrument" [syn: {design}]
