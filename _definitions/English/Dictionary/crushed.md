---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crushed
offline_file: ""
offline_thumbnail: ""
uuid: e3b6b4ab-0a03-4756-999c-75d227899c45
updated: 1484310343
title: crushed
categories:
    - Dictionary
---
crushed
     adj 1: treated so as to have a permanently wrinkled appearance;
            "crushed velvet"
     2: subdued or brought low in condition or status; "brought
        low"; "a broken man"; "his broken spirit" [syn: {broken},
        {humbled}, {humiliated}, {low}]
     3: broken or pounded into small fragments; used of e.g. ore or
        stone; "paved with crushed bluestone"; "ground glass is
        used as an abrasive" [syn: {ground}]
