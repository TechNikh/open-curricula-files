---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carnallite
offline_file: ""
offline_thumbnail: ""
uuid: 51eafb8a-c176-4f80-9c2f-ae902b711b36
updated: 1484308389
title: Carnallite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Carnallit_cristalls.jpg
tags:
    - Background Information
    - Composition
    - Structure
    - Physical properties
    - Geologic occurrence
    - Uses
categories:
    - Uncategorized
---
Carnallite is an evaporite mineral, a hydrated potassium magnesium chloride with formula KMgCl3·6(H2O). It is variably colored yellow to white, reddish, and sometimes colorless or blue. It is usually massive to fibrous with rare pseudohexagonal orthorhombic crystals. The mineral is deliquescent (absorbs moisture from the surrounding air) and specimens must be stored in an airtight container.
