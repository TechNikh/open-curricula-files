---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484626441
title: summon
categories:
    - Dictionary
---
summon
     v 1: call in an official matter, such as to attend court [syn: {summons},
           {cite}]
     2: ask to come; "summon a lawyer"
     3: gather or bring together; "muster the courage to do
        something"; "she rallied her intellect"; "Summon all your
        courage" [syn: {muster}, {rally}, {come up}, {muster up}]
     4: make ready for action or use; "marshal resources" [syn: {mobilize},
         {mobilise}, {marshal}]
