---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transpose_graph
offline_file: ""
offline_thumbnail: ""
uuid: e5cab834-7024-4ec7-8627-5682c898140f
updated: 1484309450
title: Transpose graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amirali_reverse.jpg
tags:
    - Notation
    - Applications
    - Related concepts
categories:
    - Graph operations
---
In the mathematical and algorithmic study of graph theory, the converse,[1] transpose[2] or reverse[3] of a directed graph G is another directed graph on the same set of vertices with all of the edges reversed compared to the orientation of the corresponding edges in G. That is, if G contains an edge (u,v) then the converse/transpose/reverse of G contains an edge (v,u) and vice versa.
