---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coin
offline_file: ""
offline_thumbnail: ""
uuid: db80360a-2b05-49a5-9087-280c05b998a9
updated: 1484310221
title: coin
categories:
    - Dictionary
---
coin
     n : a metal piece (usually a disc) used as money
     v 1: of phrases or words
     2: form by stamping, punching, or printing; "strike coins";
        "strike a medal" [syn: {mint}, {strike}]
