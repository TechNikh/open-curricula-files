---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asking
offline_file: ""
offline_thumbnail: ""
uuid: d3345771-c981-46ef-ad48-6069dc3e3766
updated: 1484310206
title: asking
categories:
    - Dictionary
---
asking
     adj : relating to the use of or having the nature of an
           interrogation [syn: {interrogative}, {interrogatory}, {asking(a)}]
           [ant: {declarative}, {declarative}]
     n : the verbal act of requesting [syn: {request}]
