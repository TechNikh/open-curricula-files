---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 6a846a4b-4ab5-4e65-b3c2-53b368c55e9d
updated: 1484309147
title: Euler sequence
tags:
    - Statement
    - Geometric interpretation
    - The canonical line bundle of projective spaces
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, the Euler sequence is a particular exact sequence of sheaves on n-dimensional projective space over a ring. It shows that the sheaf of relative differentials is stably isomorphic to an (n + 1)-fold sum of the dual of the Serre twisting sheaf.
