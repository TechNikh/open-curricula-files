---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lime
offline_file: ""
offline_thumbnail: ""
uuid: db8610b6-b81c-4122-b303-f513d5a28539
updated: 1484310349
title: lime
categories:
    - Dictionary
---
lime
     n 1: a caustic substance produced by heating limestone [syn: {calcium
          hydroxide}, {slaked lime}, {hydrated lime}, {calcium
          hydrate}, {caustic lime}, {lime hydrate}]
     2: a white crystalline oxide used in the production of calcium
        hydroxide [syn: {calcium oxide}, {quicklime}, {calx}, {calcined
        lime}, {fluxing lime}, {unslaked lime}, {burnt lime}]
     3: a sticky adhesive that is smeared on small branches to
        capture small birds [syn: {birdlime}]
     4: any of various related trees bearing limes [syn: {lime tree},
         {Citrus aurantifolia}]
     5: any of various deciduous trees of the genus Tilia with
        heart-shaped leaves ...
