---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/environs
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605261
title: environs
categories:
    - Dictionary
---
environs
     n : the area in which something exists or lives; "the
         country--the flat agricultural surround" [syn: {environment},
          {surroundings}, {surround}]
