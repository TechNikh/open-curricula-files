---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worsened
offline_file: ""
offline_thumbnail: ""
uuid: 7096848b-7045-48fc-97d9-d046db3a1c1c
updated: 1484310549
title: worsened
categories:
    - Dictionary
---
worsened
     adj 1: changed for the worse in health or fitness; "I feel worse
            today"; "her cold is worse" [syn: {worse}] [ant: {better}]
     2: made or become worse; impaired; "troubled by the worsened
        economic conditions"; "the worsened diplomatic relations"
