---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tibor_Gallai
offline_file: ""
offline_thumbnail: ""
uuid: 6b96b9f8-3879-4bdf-98ae-241d5d189198
updated: 1484309463
title: Tibor Gallai
tags:
    - His main results
categories:
    - Graph theorists
---
Tibor Gallai (born Tibor Grünwald, 15 July 1912 – 2 January 1992) was a Hungarian mathematician. He worked in combinatorics, especially in graph theory, and was a lifelong friend and collaborator of Paul Erdős. He was a student of Dénes Kőnig and an advisor of László Lovász. He was a corresponding member of the Hungarian Academy of Sciences (1991).
