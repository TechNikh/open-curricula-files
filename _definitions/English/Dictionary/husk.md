---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/husk
offline_file: ""
offline_thumbnail: ""
uuid: 08f2f9b5-19f3-4b64-8318-b2f480708368
updated: 1484310453
title: husk
categories:
    - Dictionary
---
husk
     n 1: material consisting of seed coverings and small pieces of
          stem or leaves that have been separated from the seeds
          [syn: {chaff}, {shuck}, {stalk}, {straw}, {stubble}]
     2: outer membranous covering of some fruits or seeds
     v : remove the husks from; "husk corn" [syn: {shell}]
