---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/effect
offline_file: ""
offline_thumbnail: ""
uuid: 49f883c6-2dfd-4e70-aa6a-eedc71bb6d6e
updated: 1484310330
title: effect
categories:
    - Dictionary
---
effect
     n 1: a phenomenon that follows and is caused by some previous
          phenomenon; "the magnetic effect was greater when the
          rod was lengthwise"; "his decision had depressing
          consequences for business"; "he acted very wise after
          the event" [syn: {consequence}, {outcome}, {result}, {event},
           {issue}, {upshot}]
     2: an outward appearance; "he made a good impression"; "I
        wanted to create an impression of success"; "she retained
        that bold effect in her reproductions of the original
        painting" [syn: {impression}]
     3: (of a law) having legal validity; "the law is still in
        effect" [syn: {force}]
     4: a ...
