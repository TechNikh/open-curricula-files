---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weismann
offline_file: ""
offline_thumbnail: ""
uuid: 7458317c-37e2-487d-a76f-ef8c0270d853
updated: 1484308345
title: Weissmann
tags:
    - Science
    - Medicine
    - Philosophy
    - 'Politics & Law'
    - 'Business & Education'
    - 'Film & Television'
    - Sports
    - Literature
    - Graphics
    - art
    - Music
categories:
    - Uncategorized
---
Weißmann (Weissmann, Weiszmann, Waismann) is a German surname meaning "white man". Common variants in spelling are Weismann, Weissman, Weisman, Waismann.
