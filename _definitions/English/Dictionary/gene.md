---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gene
offline_file: ""
offline_thumbnail: ""
uuid: 7fad08e5-2cc1-448e-b3ef-309a760bf87f
updated: 1484310299
title: gene
categories:
    - Dictionary
---
gene
     n : (genetics) a segment of DNA that is involved in producing a
         polypeptide chain; it can include regions preceding and
         following the coding DNA as well as introns between the
         exons; it is considered a unit of heredity; "genes were
         formerly called factors" [syn: {cistron}, {factor}]
