---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Agriculturists
offline_file: ""
offline_thumbnail: ""
uuid: 07064f1e-af8a-4b60-b483-6bec1c18b1fc
updated: 1484308463
title: Agriculture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Chvojnica_hills_near_Unin.jpg
tags:
    - Etymology and terminology
    - History
    - Agriculture and civilization
    - Types of agriculture
    - Contemporary agriculture
    - Workforce
    - Safety
    - Agricultural production systems
    - Crop cultivation systems
    - Crop statistics
categories:
    - Uncategorized
---
Agriculture is the cultivation and breeding of animals, plants and fungi for food, fiber, biofuel, medicinal plants and other products used to sustain and enhance human life.[1] Agriculture was the key development in the rise of sedentary human civilization, whereby farming of domesticated species created food surpluses that nurtured the development of civilization. The study of agriculture is known as agricultural science. The history of agriculture dates back thousands of years, and its development has been driven and defined by greatly different climates, cultures, and technologies. Industrial agriculture based on large-scale monoculture farming has become the dominant agricultural ...
