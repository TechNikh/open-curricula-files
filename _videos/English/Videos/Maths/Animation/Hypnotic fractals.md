---
version: 1
type: video
provider: YouTube
id: p8lnCfhY3r0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a413f9d2-4efc-4a77-8505-9652f873cbd2
updated: 1486069648
title: Hypnotic fractals
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/p8lnCfhY3r0/default.jpg
    - https://i3.ytimg.com/vi/p8lnCfhY3r0/1.jpg
    - https://i3.ytimg.com/vi/p8lnCfhY3r0/2.jpg
    - https://i3.ytimg.com/vi/p8lnCfhY3r0/3.jpg
---
