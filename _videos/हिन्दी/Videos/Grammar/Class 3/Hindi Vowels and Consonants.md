---
version: 1
type: video
provider: YouTube
id: WLVjV0qjIWc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 92ef8bd7-1577-47d0-a873-9c01a4acd557
updated: 1486069631
title: Hindi Vowels and Consonants
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/WLVjV0qjIWc/default.jpg
    - https://i3.ytimg.com/vi/WLVjV0qjIWc/1.jpg
    - https://i3.ytimg.com/vi/WLVjV0qjIWc/2.jpg
    - https://i3.ytimg.com/vi/WLVjV0qjIWc/3.jpg
---
