---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Institute_for_the_Analysis_of_Global_Security
offline_file: ""
offline_thumbnail: ""
uuid: 4458ccb6-c77c-44df-92f9-709d5f32fa52
updated: 1484309065
title: Institute for the Analysis of Global Security
tags:
    - United States Energy Security Council
    - Mobility Choice Coalition
    - TREM Center
    - Set America Free Coalition
    - Journal of Energy Security
    - Publications
categories:
    - Strategy
---
The Institute for the Analysis of Global Security (IAGS) is a Washington-based non-profit public educational organization dedicated to research and public debate on issues related to energy security. The IAGS seeks to promote public awareness of the strong impact energy has on the world economy and security and to the myriad of technological and policy solutions that could help nations strengthen their energy security.
