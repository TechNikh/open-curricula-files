---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/similarly
offline_file: ""
offline_thumbnail: ""
uuid: 181a764b-5bfa-43ed-b3a4-9e765a23ce3e
updated: 1484310343
title: similarly
categories:
    - Dictionary
---
similarly
     adv : in like or similar manner; "He was similarly affected";
           "some people have little power to do good, and have
           likewise little strength to resist evil"- Samuel
           Johnson [syn: {likewise}]
