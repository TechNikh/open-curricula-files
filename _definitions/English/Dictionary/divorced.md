---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divorced
offline_file: ""
offline_thumbnail: ""
uuid: c5bf8c3d-546c-4a36-9211-612f883b887a
updated: 1484310186
title: divorced
categories:
    - Dictionary
---
divorced
     adj : of someone whose marriage has been legally dissolved
