---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semi-log_plot
offline_file: ""
offline_thumbnail: ""
uuid: c03dad6d-f597-4dbe-8513-2ed5c7385bdd
updated: 1484309331
title: Semi-log plot
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-LogLinScale.svg.png
tags:
    - Equations
    - Real-world examples
    - Phase diagram of water
    - 2009 "swine flu" progression
    - Microbial growth
categories:
    - Technical drawing
---
In science and engineering, a semi-log graph or semi-log plot is a way of visualizing data that are related according to an exponential relationship. One axis is plotted on a logarithmic scale. This kind of plot is useful when one of the variables being plotted covers a large range of values and the other has only a restricted range – the advantage being that it can bring out features in the data that would not easily be seen if both variables had been plotted linearly.[1]
