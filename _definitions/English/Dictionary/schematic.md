---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/schematic
offline_file: ""
offline_thumbnail: ""
uuid: 565c9d8e-bec5-4743-8576-4f830a5cf04f
updated: 1484310337
title: schematic
categories:
    - Dictionary
---
schematic
     adj : represented in simplified or symbolic form [syn: {conventional},
            {formal}]
     n : diagram of an electrical or mechanical system [syn: {schematic
         drawing}]
