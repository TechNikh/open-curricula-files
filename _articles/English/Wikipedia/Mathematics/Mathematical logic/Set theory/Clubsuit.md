---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clubsuit
offline_file: ""
offline_thumbnail: ""
uuid: 8257484e-758c-46ed-92ef-010f07904377
updated: 1484309499
title: Clubsuit
tags:
    - Definition
    - ♣ and ◊
categories:
    - Set theory
---
In mathematics, and particularly in axiomatic set theory, ♣S (clubsuit) is a family of combinatorial principles that are weaker version of the corresponding ◊S; it was introduced in 1975 .
