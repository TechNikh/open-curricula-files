---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wherever
offline_file: ""
offline_thumbnail: ""
uuid: 63fc635b-1181-4eb7-808b-225823d9f771
updated: 1484310531
title: wherever
categories:
    - Dictionary
---
wherever
     adv : in or at or to what place; "I know where he is"; "use it
           wherever necessary" [syn: {where}]
