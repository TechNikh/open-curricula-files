---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eigenmode_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 58dff504-c631-42b8-94b8-b09136e3d0f4
updated: 1484308751
title: Eigenmode expansion
tags:
    - Principles of the EME method
    - Mathematical formulation
    - Strengths of the EME method
    - Limitations of the EME method
categories:
    - Electrodynamics
---
Eigenmode expansion (EME) is a computational electrodynamics modelling technique. It is also referred to as the mode matching technique[1] or the bidirectional eigenmode propagation method (BEP method).[2] Eigenmode expansion is a linear frequency-domain method.
