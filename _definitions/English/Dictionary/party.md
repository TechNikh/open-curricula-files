---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/party
offline_file: ""
offline_thumbnail: ""
uuid: 3f8a08b4-cf26-4663-9055-4826b2f6ae1f
updated: 1484310549
title: party
categories:
    - Dictionary
---
party
     n 1: an organization to gain political power; "in 1992 Perot
          tried to organize a third party at the national level"
          [syn: {political party}]
     2: an occasion on which people can assemble for social
        interaction and entertainment; "he planned a party to
        celebrate Bastille Day"
     3: a band of people associated temporarily in some activity;
        "they organized a party to search for food"; "the company
        of cooks walked into the kitchen" [syn: {company}]
     4: a group of people gathered together for pleasure; "she
        joined the party after dinner"
     5: a person involved in legal proceedings; "the party of the
        first ...
