---
version: 1
type: video
provider: YouTube
id: e43p7wf4jh8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f33cb167-ccea-4bb7-b5f1-3dd4a28ba85e
updated: 1486069622
title: Surd and Indices Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/e43p7wf4jh8/default.jpg
    - https://i3.ytimg.com/vi/e43p7wf4jh8/1.jpg
    - https://i3.ytimg.com/vi/e43p7wf4jh8/2.jpg
    - https://i3.ytimg.com/vi/e43p7wf4jh8/3.jpg
---
