---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Tricapped_trigonal_prismatic_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: c832ad38-c7ac-40ed-89d1-14b0ea809139
updated: 1484309282
title: Tricapped trigonal prismatic molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AX9E0-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, the tricapped trigonal prismatic molecular geometry describes the shape of compounds where nine atoms, groups of atoms, or ligands are arranged around a central atom, defining the vertices of a triaugmented triangular prism (a trigonal prism with an extra atom attached to each of its three rectangular faces).
