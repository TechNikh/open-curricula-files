---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/space
offline_file: ""
offline_thumbnail: ""
uuid: e884f1c1-9d79-4f66-aa20-cf1cd2f2ec88
updated: 1484310290
title: space
categories:
    - Dictionary
---
space
     n 1: the unlimited expanse in which everything is located; "they
          tested his ability to locate objects in space"
     2: an empty area (usually bounded in some way between things);
        "the architect left space in front of the building"; "they
        stopped at an open space in the jungle"; "the space
        between his teeth"
     3: an area reserved for some particular purpose; "the
        laboratory's floor space"
     4: a blank character used to separate successive words in
        writing or printing; "he said the space is the most
        important character in the alphabet" [syn: {blank}]
     5: the interval between two times; "the distance from birth to
 ...
