---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sagitta_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 07b9af07-caa9-4212-af4d-ae763a3708e2
updated: 1484309245
title: Sagitta (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Sagitta.svg.png
tags:
    - Formulae
    - Approximation
    - Applications
categories:
    - Geometric measurement
---
In geometry, the sagitta (sometimes abbreviated as sag[1]) of a circular arc is the distance from the center of the arc to the center of its base.[2] It is used extensively in architecture when calculating the arc necessary to span a certain height and distance and also in optics where it is used to find the depth of a spherical mirror or lens. The name comes directly from Latin sagitta, meaning an arrow.
