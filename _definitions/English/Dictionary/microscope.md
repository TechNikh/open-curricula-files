---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/microscope
offline_file: ""
offline_thumbnail: ""
uuid: 82996da0-24b8-4639-bac1-035bf75b54d4
updated: 1484310206
title: microscope
categories:
    - Dictionary
---
microscope
     n : magnifier of the image of small objects; "the invention of
         the microscope led to the discovery of the cell"
