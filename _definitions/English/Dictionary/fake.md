---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fake
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555221
title: fake
categories:
    - Dictionary
---
fake
     adj 1: fraudulent; having a misleading appearance [syn: {bogus}, {phony},
             {phoney}, {bastard}]
     2: not genuine or real; being an imitation of the genuine
        article; "it isn't fake anything; it's real synthetic
        fur"; "faux pearls"; "false teeth"; "decorated with
        imitation palm leaves"; "a purse of simulated alligator
        hide" [syn: {false}, {faux}, {imitation}, {simulated}]
     n 1: something that is a counterfeit; not what it seems to be
          [syn: {sham}, {postiche}]
     2: a person who makes deceitful pretenses [syn: {imposter}, {impostor},
         {pretender}, {faker}, {fraud}, {sham}, {shammer}, {pseudo},
         {pseud}, ...
