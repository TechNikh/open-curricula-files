---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fulkerson%E2%80%93Chen%E2%80%93Anstee_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 46e29c05-a55e-44d2-9513-3468f4c75166
updated: 1484309456
title: Fulkerson–Chen–Anstee theorem
tags:
    - Theorem statement
    - Stronger versions
    - Other notations
    - Generalization
    - Characterizations for similar problems
categories:
    - Theorems in graph theory
---
The Fulkerson–Chen–Anstee theorem is a result in graph theory, a branch of combinatorics. It provides one of two known approaches solving the digraph realization problem, i.e. it gives a necessary and sufficient condition for pairs of nonnegative integers 
  
    
      
        (
        (
        
          a
          
            1
          
        
        ,
        
          b
          
            1
          
        
        )
        ,
        …
        ,
        (
        
          a
          
            n
          
        
        ,
        
          b
          
            n
          
        
        )
        )
      
    
    {\displaystyle ...
