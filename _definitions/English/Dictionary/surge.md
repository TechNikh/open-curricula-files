---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surge
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619421
title: surge
categories:
    - Dictionary
---
surge
     n 1: a sudden forceful flow [syn: {rush}, {spate}, {upsurge}]
     2: a sudden or abrupt strong increase; "stimulated a surge of
        speculation"; "an upsurge of emotion"; "an upsurge in
        violent crime" [syn: {upsurge}]
     3: a large sea wave [syn: {billow}]
     v 1: rise and move, as in waves or billows; "The army surged
          forward" [syn: {billow}, {heave}]
     2: rise rapidly; "the dollar soared against the yes" [syn: {soar},
         {soar up}, {soar upwards}, {zoom}]
     3: rise or move foward; "surging waves" [syn: {tide}] [ant: {ebb}]
     4: rise or heave upward under the influence of a natural force
        such as a wave; "the boats surged" [syn: ...
