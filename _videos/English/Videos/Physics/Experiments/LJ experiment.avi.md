---
version: 1
type: video
provider: YouTube
id: 9Oz9TsXCa70
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 154811a8-14fa-48e3-93dd-124fb702815c
updated: 1486069636
title: LJ experiment.avi
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/9Oz9TsXCa70/default.jpg
    - https://i3.ytimg.com/vi/9Oz9TsXCa70/1.jpg
    - https://i3.ytimg.com/vi/9Oz9TsXCa70/2.jpg
    - https://i3.ytimg.com/vi/9Oz9TsXCa70/3.jpg
---
