---
version: 1
type: video
provider: YouTube
id: 1qxGKnIwL5U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b3a28965-2009-4f18-9798-0646f6bbc1b0
updated: 1486069634
title: 'How to Make Match Cannon Mini Weapon - TUTORIAL'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/1qxGKnIwL5U/default.jpg
    - https://i3.ytimg.com/vi/1qxGKnIwL5U/1.jpg
    - https://i3.ytimg.com/vi/1qxGKnIwL5U/2.jpg
    - https://i3.ytimg.com/vi/1qxGKnIwL5U/3.jpg
---
