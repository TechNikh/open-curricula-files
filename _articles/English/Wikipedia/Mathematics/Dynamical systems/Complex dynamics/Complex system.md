---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_system
offline_file: ""
offline_thumbnail: ""
uuid: 7cfec461-7327-4161-b925-7156ca4d5ff3
updated: 1484308724
title: Complex system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Club_Lambda.png
tags:
    - History
    - Types
    - Nonlinear systems
    - Chaotic systems
    - Complex adaptive systems
    - features
categories:
    - Complex dynamics
---
A complex system can be also viewed as a system composed of many components which may interact with each other. In many cases it is useful to represent such a system as a network where the nodes represent the components and the links their interactions.
