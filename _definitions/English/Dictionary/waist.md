---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waist
offline_file: ""
offline_thumbnail: ""
uuid: 10b68452-004b-4187-a24b-67e297b1f374
updated: 1484310148
title: waist
categories:
    - Dictionary
---
waist
     n 1: the narrowing of the body between the ribs and hips [syn: {waistline}]
     2: the narrow part of the shoe connecting the heel and the wide
        part of the sole [syn: {shank}]
