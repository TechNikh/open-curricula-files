---
version: 1
type: video
provider: YouTube
id: I80Xx7pA9hQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Muscles%2C%20part%202%20-%20Organismal%20Level-%20Crash%20Course%20A%26P%20%2322.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 55d51ebf-9147-499c-9ce6-71d98e9acc14
updated: 1486069682
title: 'Muscles, part 2 - Organismal Level- Crash Course A&P #22'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/I80Xx7pA9hQ/default.jpg
    - https://i3.ytimg.com/vi/I80Xx7pA9hQ/1.jpg
    - https://i3.ytimg.com/vi/I80Xx7pA9hQ/2.jpg
    - https://i3.ytimg.com/vi/I80Xx7pA9hQ/3.jpg
---
