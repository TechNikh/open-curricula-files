---
version: 1
type: video
provider: YouTube
id: PKXYfpZqKts
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1936ba7e-a018-42df-a984-b03e2de374e5
updated: 1486069636
title: Spectrometer
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/PKXYfpZqKts/default.jpg
    - https://i3.ytimg.com/vi/PKXYfpZqKts/1.jpg
    - https://i3.ytimg.com/vi/PKXYfpZqKts/2.jpg
    - https://i3.ytimg.com/vi/PKXYfpZqKts/3.jpg
---
