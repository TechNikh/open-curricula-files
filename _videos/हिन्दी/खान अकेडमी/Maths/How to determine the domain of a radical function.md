---
version: 1
type: video
provider: YouTube
id: onmfDplXN1o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 64e515b2-61a1-4370-8cda-39b82f994fe9
updated: 1486069613
title: How to determine the domain of a radical function
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/onmfDplXN1o/default.jpg
    - https://i3.ytimg.com/vi/onmfDplXN1o/1.jpg
    - https://i3.ytimg.com/vi/onmfDplXN1o/2.jpg
    - https://i3.ytimg.com/vi/onmfDplXN1o/3.jpg
---
