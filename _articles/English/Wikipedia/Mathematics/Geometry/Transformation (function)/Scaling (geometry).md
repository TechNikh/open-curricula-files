---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scaling_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: d6b16139-625e-4336-81e8-504f25796cd9
updated: 1484309338
title: Scaling (geometry)
tags:
    - Matrix representation
    - Scaling in arbitrary dimensions
    - Using homogeneous coordinates
    - Footnotes
categories:
    - Transformation (function)
---
In Euclidean geometry, uniform scaling (or isotropic scaling[1]) is a linear transformation that enlarges (increases) or shrinks (diminishes) objects by a scale factor that is the same in all directions. The result of uniform scaling is similar (in the geometric sense) to the original. A scale factor of 1 is normally allowed, so that congruent shapes are also classed as similar. Uniform scaling happens, for example, when enlarging or reducing a photograph, or when creating a scale model of a building, car, airplane, etc.
