---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homotopical_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 58da4d45-49a4-40e2-ac12-598cbe005303
updated: 1484309363
title: Homotopical algebra
categories:
    - Geometry stubs
---
In mathematics, homotopical algebra is a collection of concepts comprising the nonabelian aspects of homological algebra as well as possibly the abelian aspects as special cases. The homotopical nomenclature stems from the fact that a common approach to such generalizations is via abstract homotopy theory, as in nonabelian algebraic topology, and in particular the theory of closed model categories.
