---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vadim_G._Vizing
offline_file: ""
offline_thumbnail: ""
uuid: fce7716d-d158-4bd1-837e-2ce4d6f0a51c
updated: 1484309481
title: Vadim G. Vizing
tags:
    - Biography
    - Research results
    - Awards
    - Notes
categories:
    - Graph theorists
---
Vadim Georgievich Vizing (Russian: Вади́м Гео́ргиевич Визинг, Ukrainian: Вадим Георгійович Візінг; born 1937) is a Ukrainian (former Soviet) mathematician known for his contributions to graph theory, and especially for Vizing's theorem stating that the edges of any graph with maximum degree Δ can be colored with at most Δ + 1 colors.
