---
version: 1
type: video
provider: YouTube
id: EFegwhbM1t4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fbff2afe-b422-4360-b82b-e04eabebf816
updated: 1486069609
title: Ways to cut a cube
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/EFegwhbM1t4/default.jpg
    - https://i3.ytimg.com/vi/EFegwhbM1t4/1.jpg
    - https://i3.ytimg.com/vi/EFegwhbM1t4/2.jpg
    - https://i3.ytimg.com/vi/EFegwhbM1t4/3.jpg
---
