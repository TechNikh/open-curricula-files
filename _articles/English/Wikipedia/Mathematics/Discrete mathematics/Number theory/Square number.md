---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Square_number
offline_file: ""
offline_thumbnail: ""
uuid: aa8a7e64-cba3-404d-9449-b7946f0cc0d3
updated: 1484308659
title: Square number
tags:
    - Examples
    - Properties
    - Special cases
    - Odd and even square numbers
    - Notes
categories:
    - Number theory
---
In mathematics, a square number or perfect square is an integer that is the square of an integer;[1] in other words, it is the product of some integer with itself. For example, 9 is a square number, since it can be written as 3 × 3.
