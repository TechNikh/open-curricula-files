---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/author
offline_file: ""
offline_thumbnail: ""
uuid: 7a6d74a3-6a2f-4870-b53a-d78676c28b8d
updated: 1484310547
title: author
categories:
    - Dictionary
---
author
     n 1: writes (books or stories or articles or the like)
          professionally (for pay) [syn: {writer}]
     2: someone who originates or causes or initiates something; "he
        was the generator of several complaints" [syn: {generator},
         {source}]
     v : be the author of; "She authored this play"
