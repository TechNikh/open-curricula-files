---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/original
offline_file: ""
offline_thumbnail: ""
uuid: 2a9e39d4-b55d-4858-9018-ab7caf5862b8
updated: 1484310284
title: original
categories:
    - Dictionary
---
original
     adj 1: preceding all others in time or being as first made or
            performed; "the original inhabitants of the Americas";
            "the book still has its original binding"; "restored
            the house to its original condition"; "the original
            performance of the opera"; "the original cast";
            "retracted his original statement"
     2: (of e.g. information) not secondhand or by way of something
        intermediary; "his work is based on only original, not
        secondary, sources"
     3: being or productive of something fresh and unusual; or being
        as first made or thought of; "a truly original approach";
        "with original ...
