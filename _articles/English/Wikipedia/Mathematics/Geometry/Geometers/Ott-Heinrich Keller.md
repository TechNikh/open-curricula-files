---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ott-Heinrich_Keller
offline_file: ""
offline_thumbnail: ""
uuid: b1ad9522-d47b-4995-88cf-40f403d61b78
updated: 1484309088
title: Ott-Heinrich Keller
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Keller_Kneser.jpg
categories:
    - Geometers
---
Eduard Ott-Heinrich Keller (22 June 1906 in Frankfurt (Main), Germany – 1990 in Halle, Germany) was a German mathematician who worked in the fields of geometry, topology and algebraic geometry. He formulated the celebrated problem which is now called the Jacobian conjecture in 1939.
