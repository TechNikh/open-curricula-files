---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rangareddy
offline_file: ""
offline_thumbnail: ""
uuid: c5ba5f07-9e7d-4279-bef5-d1be65aea706
updated: 1484308337
title: Ranga Reddy district
tags:
    - History
    - geography
    - Demographics
    - Economy
    - Administrative divisions
    - Proposed mandals
    - Existing mandals
    - Assembly Constituencies
categories:
    - Uncategorized
---
Ranga Reddy District (formerly: K. V. Ranga Reddy District and Hyderabad (Rural) District), is a district in the Indian state of Telangana. The district headquarters are located at Shamshabad.[3] The district headquarters are located at Shamshabad.[4] It was named after the former deputy chief minister of the united state of Andhra Pradesh, K.V.Ranga Reddy.[5]
