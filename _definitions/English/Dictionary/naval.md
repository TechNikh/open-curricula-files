---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/naval
offline_file: ""
offline_thumbnail: ""
uuid: 964396ea-bf0c-4761-9ed1-f4e0f59e0515
updated: 1484310477
title: naval
categories:
    - Dictionary
---
naval
     adj : connected with or belonging to or used in a navy; "naval
           history"; "naval commander"; "naval vessels"
