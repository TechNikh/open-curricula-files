---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/premier
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408161
title: premier
categories:
    - Dictionary
---
premier
     adj 1: first in rank or degree; "an architect of premier rank";
            "the prime minister" [syn: {premier(a)}, {prime(a)}]
     2: preceding all others in time; "the premiere showing" [syn: {premiere}]
     n 1: the person who holds the position of head of state in
          England [syn: {Prime Minister}, {PM}]
     2: the person who is head of state (in several countries) [syn:
         {chancellor}, {prime minister}]
     v 1: be performed for the first time; "We premiered the opera of
          the young composer and it was a critical success" [syn:
          {premiere}]
     2: perform a work for the first time [syn: {premiere}]
