---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technologically
offline_file: ""
offline_thumbnail: ""
uuid: e77e1775-a34b-4439-8564-db7c096c8b87
updated: 1484310579
title: technologically
categories:
    - Dictionary
---
technologically
     adv : by means of technology; "technologically impossible"
