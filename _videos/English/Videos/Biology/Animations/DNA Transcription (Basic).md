---
version: 1
type: video
provider: YouTube
id: 5MfSYnItYvg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2d1f918d-f412-4a57-b9d8-8af0c9d1b3eb
updated: 1486069673
title: DNA Transcription (Basic)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/5MfSYnItYvg/default.jpg
    - https://i3.ytimg.com/vi/5MfSYnItYvg/1.jpg
    - https://i3.ytimg.com/vi/5MfSYnItYvg/2.jpg
    - https://i3.ytimg.com/vi/5MfSYnItYvg/3.jpg
---
