---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stroke
offline_file: ""
offline_thumbnail: ""
uuid: b8a8cb2e-16c4-4295-9d74-f8d0e8a9db98
updated: 1484310515
title: stroke
categories:
    - Dictionary
---
stroke
     n 1: (sports) the act of swinging or striking at a ball with a
          club or racket or bat or cue or hand; "it took two
          strokes to get out of the bunker"; "a good shot require
          good balance and tempo"; "he left me an almost
          impossible shot" [syn: {shot}]
     2: the maximum movement available to a pivoted or reciprocating
        piece by a cam [syn: {throw}, {cam stroke}]
     3: a sudden loss of consciousness resulting when the rupture or
        occlusion of a blood vessel leads to oxygen lack in the
        brain [syn: {apoplexy}, {cerebrovascular accident}, {CVA}]
     4: a light touch
     5: a light touch with the hands [syn: {stroking}]
  ...
