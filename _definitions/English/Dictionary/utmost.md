---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utmost
offline_file: ""
offline_thumbnail: ""
uuid: e55a7578-7101-4c19-b655-9454a51056f6
updated: 1484310539
title: utmost
categories:
    - Dictionary
---
utmost
     adj 1: of the greatest possible degree or extent or intensity;
            "extreme cold"; "extreme caution"; "extreme pleasure";
            "utmost contempt"; "to the utmost degree"; "in the
            uttermost distress" [syn: {extreme}, {utmost(a)}, {uttermost(a)}]
     2: highest in extent or degree; "to the last measure of human
        endurance"; "whether they were accomplices in the last
        degree or a lesser one was...to be determined
        individually" [syn: {last}]
     3: (comparatives of `far') most remote in space or time or
        order; "had traveled to the farthest frontier"; "don't go
        beyond the farthermost (or furthermost) tree"; "explored
  ...
