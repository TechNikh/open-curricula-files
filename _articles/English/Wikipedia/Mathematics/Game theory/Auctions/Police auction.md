---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Police_auction
offline_file: ""
offline_thumbnail: ""
uuid: 27b38e5c-3152-4b80-881c-87d5223fc6ea
updated: 1484309013
title: Police auction
tags:
    - United Kingdom
    - USA
    - New Zealand
categories:
    - Auctions
---
A police auction is an auction of goods which have been confiscated by the police and cannot or may not be returned to their original owners. They may also contain surplus and retired police equipment, such as used police cars.
