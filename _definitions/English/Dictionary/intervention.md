---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intervention
offline_file: ""
offline_thumbnail: ""
uuid: 8281b112-3e64-4083-8993-9e61ffeb1c34
updated: 1484310479
title: intervention
categories:
    - Dictionary
---
intervention
     n 1: the act of intervening (as to mediate a dispute) [syn: {intercession}]
     2: a policy of intervening in the affairs of other countries
        [syn: {interference}] [ant: {nonintervention}, {nonintervention}]
     3: (law) a proceeding that permits a person to enter into a
        lawsuit already in progress; admission of person not an
        original party to the suit so that person can protect some
        right or interest that is allegedly affected by the
        proceedings; "the purpose of intervention is to prevent
        unnecessary duplication of lawsuits"
