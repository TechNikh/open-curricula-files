---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ton
offline_file: ""
offline_thumbnail: ""
uuid: 4840e79f-e7cd-49c4-9419-2c2ac18eafed
updated: 1484310486
title: ton
categories:
    - Dictionary
---
ton
     n 1: a United States unit of weight equivalent to 2000 pounds
          [syn: {short ton}, {net ton}]
     2: a British unit of weight equivalent to 2240 pounds [syn: {long
        ton}, {gross ton}]
