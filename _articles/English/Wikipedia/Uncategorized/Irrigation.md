---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irrigated
offline_file: ""
offline_thumbnail: ""
uuid: 17b0a4ec-6ada-4f2a-81f0-7113593e94c4
updated: 1484308432
title: Irrigation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Irrigational_sprinkler.jpg
tags:
    - History
    - China
    - Korea
    - North America
    - Present extent
    - Types of irrigation
    - Surface irrigation
    - Localized irrigation
    - Subsurface textile irrigation
    - Drip irrigation
    - Irrigation using sprinkler systems
    - Irrigation using Center pivot
    - 'Irrigation by Lateral move (side roll, wheel line, wheelmove)[31][32]'
    - Sub-irrigation
    - >
        Irrigation Automatically, non-electric using buckets and
        ropes
    - Irrigation using water condensed from humid air
    - In-ground irrigation
    - Water sources
    - Efficiency
    - Technical challenges
    - Journals
categories:
    - Uncategorized
---
Irrigation is the method in which a controlled amount of water is supplied to plants at regular intervals for agriculture. It is used to assist in the growing of agricultural crops, maintenance of landscapes, and revegetation of disturbed soils in dry areas and during periods of inadequate rainfall. Additionally, irrigation also has a few other uses in crop production, which include protecting plants against frost,[1] suppressing weed growth in grain fields[2] and preventing soil consolidation.[3] In contrast, agriculture that relies only on direct rainfall is referred to as rain-fed or dry land farming.
