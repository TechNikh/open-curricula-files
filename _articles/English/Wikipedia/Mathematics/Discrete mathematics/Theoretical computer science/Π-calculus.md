---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%A0-calculus'
offline_file: ""
offline_thumbnail: ""
uuid: 4d9f6f6d-00e5-426d-99ec-7311f91ba24e
updated: 1484308683
title: Π-calculus
tags:
    - Informal definition
    - Process constructs
    - A small example
    - Formal definition
    - Syntax
    - Structural congruence
    - Reduction semantics
    - The example revisited
    - Labelled semantics
    - Extensions and variants
    - Properties
    - Turing completeness
    - Bisimulations in the π-calculus
    - Early and late bisimilarity
    - Open bisimilarity
    - Early, late and open bisimilarity are distinct
    - Barbed equivalence
    - Applications
    - History
    - Implementations
    - Notes
categories:
    - Theoretical computer science
---
In theoretical computer science, the π-calculus (or pi-calculus) is a process calculus. The π-calculus allows channel names to be communicated along the channels themselves, and in this way it is able to describe concurrent computations whose network configuration may change during the computation.
