---
version: 1
type: video
provider: YouTube
id: 6geworxvh1M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d194ec29-950c-470e-8087-da812eb7f543
updated: 1486069616
title: >
    Evaluating expressions where individual variable values are
    unknown
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/6geworxvh1M/default.jpg
    - https://i3.ytimg.com/vi/6geworxvh1M/1.jpg
    - https://i3.ytimg.com/vi/6geworxvh1M/2.jpg
    - https://i3.ytimg.com/vi/6geworxvh1M/3.jpg
---
