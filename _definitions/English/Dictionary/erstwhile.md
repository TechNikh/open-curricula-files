---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erstwhile
offline_file: ""
offline_thumbnail: ""
uuid: 1d9092ca-37f9-4e32-9a15-1f29760b2c02
updated: 1484310484
title: erstwhile
categories:
    - Dictionary
---
erstwhile
     adj : belonging to some prior time; "erstwhile friend"; "our
           former glory"; "the once capital of the state"; "her
           quondam lover" [syn: {erstwhile(a)}, {former(a)}, {once(a)},
            {onetime(a)}, {quondam(a)}, {sometime(a)}]
     adv : at a previous time; "once he loved her"; "her erstwhile
           writing" [syn: {once}, {formerly}, {at one time}, {erst}]
