---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elliptic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 25acc238-57f2-4b7a-8033-cff2ee8b723f
updated: 1484309168
title: Elliptic geometry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Triangles_%2528spherical_geometry%2529_0.jpg'
tags:
    - Definitions
    - Two dimensions
    - The spherical model
    - Comparison with Euclidean geometry
    - Elliptic space
    - Higher-dimensional spaces
    - Hyperspherical model
    - Projective elliptic geometry
    - Stereographic model
    - Self-consistency
    - Notes
categories:
    - Classical geometry
---
Elliptic geometry, a special case of Riemannian geometry, is a non-Euclidean geometry, in which, given a line L and a point p outside L, there exists no line parallel to L passing through p, as all lines in elliptic geometry intersect. Elliptic geometry has a variety of properties that differ from those of classical Euclidean plane geometry. For example, the sum of the interior angles of any triangle is always greater than 180°.
