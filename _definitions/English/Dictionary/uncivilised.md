---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncivilised
offline_file: ""
offline_thumbnail: ""
uuid: 338c1baf-c19d-4bca-8d92-f61d70839f5b
updated: 1484310575
title: uncivilised
categories:
    - Dictionary
---
uncivilised
     adj : without civilizing influences; "barbarian invaders";
           "barbaric practices"; "a savage people"; "fighting is
           crude and uncivilized especially if the weapons are
           efficient"-Margaret Meade; "wild tribes" [syn: {barbarian},
            {barbaric}, {savage}, {uncivilized}, {wild}]
