---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kazakhstan
offline_file: ""
offline_thumbnail: ""
uuid: a4c38519-494a-4e55-88f4-b675b3e94720
updated: 1484308479
title: Kazakhstan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ancient_Taraz_Kazakhstan.jpg
tags:
    - Etymology
    - History
    - Kazakh Khanate
    - Russian Empire
    - Soviet Union
    - Independence
    - geography
    - Climate
    - wildlife
    - Administrative divisions
categories:
    - Uncategorized
---
Kazakhstan US i/kæzækˈstæn, ˌkɑːzɑːkˈstɑːn/, UK /ˌkæzəkˈstɑːn, -ˈstæn/;[8] Kazakh: Қазақстан, Qazaqstan IPA: [qɑzɑqˈstɑn] ( listen), officially the Republic of Kazakhstan, is a transcontinental country in northern Central Asia[3][9] and Eastern Europe. Kazakhstan is the world's largest landlocked country, and the ninth largest in the world, with an area of 2,724,900 square kilometres (1,052,100 sq mi).[3][10] Kazakhstan is the dominant nation of Central Asia economically, generating 60% of the region's GDP, primarily through its oil/gas industry. It also has vast mineral resources.[11]
