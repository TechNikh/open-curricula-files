---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%E2%88%921'
offline_file: ""
offline_thumbnail: ""
uuid: b95c226e-7ef0-4769-bb3f-db2378be551c
updated: 1484308946
title: −1
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ImaginaryUnit5.svg.png
tags:
    - Algebraic properties
    - Square of −1
    - Square roots of −1
    - Exponentiation to negative integers
    - Inductive dimension
    - Computer representation
categories:
    - Integers
---
In mathematics, −1 is the additive inverse of 1, that is, the number that when added to 1 gives the additive identity element, 0. It is the negative integer greater than negative two (−2) and less than 0.
