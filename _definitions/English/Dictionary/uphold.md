---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uphold
offline_file: ""
offline_thumbnail: ""
uuid: f37dc740-f7d4-4431-a8b5-b5a37c747331
updated: 1484310559
title: uphold
categories:
    - Dictionary
---
uphold
     v 1: keep or maintain in unaltered condition; cause to remain or
          last; "preserve the peace in the family"; "continue the
          family tradition"; "Carry on the old traditions" [syn: {continue},
           {carry on}, {bear on}, {preserve}] [ant: {discontinue}]
     2: stand up for; stick up for; of causes, principles, or ideals
     3: support against an opponent; "The appellate court upheld the
        verdict" [syn: {maintain}]
     [also: {upheld}]
