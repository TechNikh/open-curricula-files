---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Journal_of_Bifurcation_and_Chaos
offline_file: ""
offline_thumbnail: ""
uuid: 8397a2e8-8682-445c-9cc3-31b892717dfd
updated: 1484308712
title: International Journal of Bifurcation and Chaos
categories:
    - Chaos theory
---
International Journal of Bifurcation and Chaos is a peer-reviewed scientific journal published by World Scientific. It was established in 1991 and covers chaos theory and nonlinear science in a diverse range of fields in applied sciences, such as astronomy and zoology, as well as engineering, such as in aerospace and telecommunications.
