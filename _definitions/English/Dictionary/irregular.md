---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irregular
offline_file: ""
offline_thumbnail: ""
uuid: 89d15c4d-803b-474d-a39d-236fc779e3bc
updated: 1484310459
title: irregular
categories:
    - Dictionary
---
irregular
     adj 1: contrary to rule or accepted order or general practice;
            "irregular hiring practices" [ant: {regular}]
     2: (of solids) not having clear dimensions that can be
        measured; volume must be determined with the principle of
        liquid displacement [ant: {regular}]
     3: not occurring at expected times [syn: {unpredictable}]
     4: used of the military; not belonging to or engaged in by
        regular army forces; "irregular troops"; "irregular
        warfare" [ant: {regular}]
     5: deviating from what is usual or common or to be expected;
        often somewhat odd or strange; "these days large families
        are atypical"; "highly ...
