---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Expected_float_entropy
offline_file: ""
offline_thumbnail: ""
uuid: cda193a2-4a19-4e46-86a4-8deb033927e6
updated: 1484308771
title: Expected float entropy
tags:
    - Overview
    - Definitions and connection with Shannon entropy
    - Definitions
    - Connection with Shannon entropy
    - Connection to other mathematical theories of consciousness
categories:
    - Entropy and information
---
Expected float entropy (efe) stems from information theory but differs from Shannon entropy by including relationships as parameters. It is a measure of the expected amount of information required to specify the state of a system (such as an artificial or biological neural network) beyond what is already known about the system from the relationship parameters. For non-random systems, certain choices of the relationship parameters are isolated from other choices in the sense that they give much lower expected float entropy values and, therefore, the system defines relationships. Expected float entropy is an important definition in a recently developed mathematical theory of consciousness in ...
