---
version: 1
type: video
provider: YouTube
id: DSntMn21D6Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3f610aa6-09f6-40c3-af25-2470e25c1fd0
updated: 1486069613
title: Number Sets 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/DSntMn21D6Y/default.jpg
    - https://i3.ytimg.com/vi/DSntMn21D6Y/1.jpg
    - https://i3.ytimg.com/vi/DSntMn21D6Y/2.jpg
    - https://i3.ytimg.com/vi/DSntMn21D6Y/3.jpg
---
