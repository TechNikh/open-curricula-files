---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pupil
offline_file: ""
offline_thumbnail: ""
uuid: ff948b00-3004-4756-8fb9-f5566792b72c
updated: 1484310150
title: pupil
categories:
    - Dictionary
---
pupil
     n 1: a learner who is enrolled in an educational institution
          [syn: {student}, {educatee}]
     2: contractile aperture in the iris of the eye
     3: a young person attending school (up through senior high
        school) [syn: {schoolchild}, {school-age child}]
