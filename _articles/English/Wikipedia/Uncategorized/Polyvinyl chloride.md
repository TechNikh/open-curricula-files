---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pvc
offline_file: ""
offline_thumbnail: ""
uuid: a98e4bba-b791-41f6-9b66-362198faeb2f
updated: 1484308309
title: Polyvinyl chloride
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bis%25282-ethylhexyl%2529phthalate.png'
tags:
    - discovery
    - Production
    - Microstructure
    - Additives to finished polymer
    - Phthalate plasticizers
    - High and low molecular weight phthalates
    - Heat stabilizers
    - Rigid PVC applications
    - Flexible PVC applications
    - Properties
    - Mechanical properties
    - Thermal and fire properties
    - Electrical properties
    - Chemical properties
    - Applications
    - Pipes
    - Electric cables
    - Unplasticized poly(vinyl chloride) (uPVC) for construction
    - Signs
    - Clothing and furniture
    - Healthcare
    - Plasticizers
    - Flooring
    - Other applications
    - Chlorinated PVC
    - Health and safety
    - Degradation
    - Plasticizers
    - EU decisions on phthalates
    - Lead
    - Vinyl chloride monomer
    - Dioxins
    - End-of-life
    - Industry initiatives
    - Restrictions
    - Vinyl gloves in medicine
    - sustainability
    - Bibliography
categories:
    - Uncategorized
---
Polyvinyl chloride (/ˌpɑːlivaɪnəl ˈklɔːraɪd/),[4] more correctly but unusually poly(vinyl chloride), commonly abbreviated PVC, is the world's third-most widely produced synthetic plastic polymer, after polyethylene and polypropylene.[5]
