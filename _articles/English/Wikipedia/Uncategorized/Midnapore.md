---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Midnapore
offline_file: ""
offline_thumbnail: ""
uuid: 12837b98-7b4c-492e-a169-52bee76631be
updated: 1484308463
title: Midnapore
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Khudiram_Bose_calendar_art.jpg
tags:
    - Etymology
    - History
    - Climate and geography
    - Transportation
    - Train
    - Local transportation
    - Infrastructure and economy
    - Internet Services
    - Demographics
    - Police station
categories:
    - Uncategorized
---
Midnapore (Pron: med̪iːniːpur) is the district headquarters of Paschim Medinipur district of the Indian state of West Bengal. It is situated on the banks of the Kangsabati River (variously known as Kasai and Cossye).
