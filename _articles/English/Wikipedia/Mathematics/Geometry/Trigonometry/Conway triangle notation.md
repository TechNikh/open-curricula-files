---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conway_triangle_notation
offline_file: ""
offline_thumbnail: ""
uuid: 3064de25-0834-45b1-abe7-10468f9b8633
updated: 1484309341
title: Conway triangle notation
categories:
    - Trigonometry
---
In geometry, the Conway triangle notation, named after John Horton Conway, allows trigonometric functions of a triangle to be managed algebraically. Given a reference triangle whose sides are a, b and c and whose corresponding internal angles are A, B, and C then the Conway triangle notation is simply represented as follows:
