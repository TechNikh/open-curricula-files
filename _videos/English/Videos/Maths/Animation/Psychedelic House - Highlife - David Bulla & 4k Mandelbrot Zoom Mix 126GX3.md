---
version: 1
type: video
provider: YouTube
id: oQDU_ruO5bQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0e81006d-c019-4109-ad5b-1614e4629338
updated: 1486069645
title: 'Psychedelic House - Highlife - David Bulla & 4k Mandelbrot Zoom Mix 126GX3'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/oQDU_ruO5bQ/default.jpg
    - https://i3.ytimg.com/vi/oQDU_ruO5bQ/1.jpg
    - https://i3.ytimg.com/vi/oQDU_ruO5bQ/2.jpg
    - https://i3.ytimg.com/vi/oQDU_ruO5bQ/3.jpg
---
