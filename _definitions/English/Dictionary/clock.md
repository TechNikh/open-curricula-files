---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clock
offline_file: ""
offline_thumbnail: ""
uuid: f1c04c17-ecdb-47b9-bd92-4c34c81cb2f2
updated: 1484310309
title: clock
categories:
    - Dictionary
---
clock
     n : a timepiece that shows the time of day
     v : measure the time or duration of an event or action or the
         person who performs an action in a certain period of
         time; "he clocked the runners" [syn: {time}]
