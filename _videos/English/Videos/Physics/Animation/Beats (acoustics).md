---
version: 1
type: video
provider: YouTube
id: 3M5sYWjwfeg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Beats%20%28acoustics%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cd9abbb1-2561-4aa9-ad9b-ad7a94b1301b
updated: 1486069653
title: Beats (acoustics)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3M5sYWjwfeg/default.jpg
    - https://i3.ytimg.com/vi/3M5sYWjwfeg/1.jpg
    - https://i3.ytimg.com/vi/3M5sYWjwfeg/2.jpg
    - https://i3.ytimg.com/vi/3M5sYWjwfeg/3.jpg
---
