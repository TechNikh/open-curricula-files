---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Haridwar
offline_file: ""
offline_thumbnail: ""
uuid: de51eee1-c3d5-4962-8375-3c1223dbe78e
updated: 1484308432
title: Haridwar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Haridwar_Aerial_view.jpg
tags:
    - Etymology
    - Seven holy places
    - History
    - Geography and Climate
    - Climate
    - Cityscape
    - Hindu genealogy registers at Haridwar
    - Demographics
    - Places of interest
    - Chandi Devi Temple
    - Mansa Devi Temple
    - Maya Devi Temple
    - Kankhal
    - Piran Kaliyar
    - Neel Dhara Pakshi Vihar
    - Bhimgoda Tank
    - Dudhadhari Barfani Temple
    - Sureshvari Devi Temple
    - Pawan Dham
    - Bharat Mata Mandir
    - Shopping
    - Sapta Rishi Ashram and Sapta Sarovar
    - Parad Shivling
    - Ramanand Ashram
    - Anandamayi Maa Ashram
    - Shantikunj
    - Patanjali Yogpeeth (Trust)
    - Shri Chintamani Parshwnath Jain Shwetambar Mandir
    - Educational institutions
    - Gurukul Kangri University
    - Dev Sanskriti Vishwavidyalaya
    - Uttarakhand Sanskrit University
    - Chinmaya Degree College
    - Other Colleges
    - Other schools
    - Important areas within the city
    - Transport
    - Road
    - Rail
    - air
    - Industry
categories:
    - Uncategorized
---
Haridwar (Pron:ˈhʌrɪˌdwɑ:)  pronunciation (help·info) also spelled Hardwar is an ancient city and municipality in the Haridwar district of Uttarakhand, India. The River Ganga, after flowing for 253 kilometres (157 mi) from its source at Gaumukh at the edge of the Gangotri Glacier, enters the Indo-Gangetic Plains of North India for the first time at Haridwar, which gave the city its ancient name, Gangadwára.
