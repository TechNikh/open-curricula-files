---
version: 1
type: video
provider: YouTube
id: WWOU3MpSsUQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 001e8c6a-2e15-44b8-b385-b72f716cb5d8
updated: 1486069637
title: 'Experiment 9   Polarization   Phys 271   FCW V1 F 2014'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/WWOU3MpSsUQ/default.jpg
    - https://i3.ytimg.com/vi/WWOU3MpSsUQ/1.jpg
    - https://i3.ytimg.com/vi/WWOU3MpSsUQ/2.jpg
    - https://i3.ytimg.com/vi/WWOU3MpSsUQ/3.jpg
---
