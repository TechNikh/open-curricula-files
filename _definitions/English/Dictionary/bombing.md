---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bombing
offline_file: ""
offline_thumbnail: ""
uuid: 4748a2ca-2840-4e94-b18e-c60135148430
updated: 1484310567
title: bombing
categories:
    - Dictionary
---
bombing
     n 1: an attack by dropping bombs [syn: {bombardment}]
     2: the use of bombs for sabotage; a tactic frequently used by
        terrorists
