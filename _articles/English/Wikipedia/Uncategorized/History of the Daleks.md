---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dals
offline_file: ""
offline_thumbnail: ""
uuid: 05996a75-5863-4f45-9441-6c53ea4bc668
updated: 1484308464
title: History of the Daleks
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kaled.jpg
tags:
    - Main history
    - Origins
    - Later stories
    - Genesis of the Daleks
    - Post-Genesis history
    - Civil war
    - The Master on Skaro
    - Time War and aftermath
    - The Cult of Skaro
    - New Dalek Empire
categories:
    - Uncategorized
---
The Daleks (/ˈdɑːlɛks/ DAH-leks) are a fictional extraterrestrial race of mutants from the British science fiction television series Doctor Who. The mutated remains of the Kaled people of the planet Skaro, they travel around in tank-like mechanical casings, and are a race bent on universal conquest and destruction. They are also, collectively, the greatest alien adversaries of the Time Lord known as the Doctor, having evolved over the course of the series from a weak race to monsters capable of destroying even the Time Lords and achieving control of the universe.
