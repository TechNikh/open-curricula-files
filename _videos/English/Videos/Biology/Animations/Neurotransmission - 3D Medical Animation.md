---
version: 1
type: video
provider: YouTube
id: cNaFnRKwpFk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Neurotransmission%20-%203D%20Medical%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4032d231-4411-4dc1-898a-9abb06450105
updated: 1486069669
title: 'Neurotransmission - 3D Medical Animation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/cNaFnRKwpFk/default.jpg
    - https://i3.ytimg.com/vi/cNaFnRKwpFk/1.jpg
    - https://i3.ytimg.com/vi/cNaFnRKwpFk/2.jpg
    - https://i3.ytimg.com/vi/cNaFnRKwpFk/3.jpg
---
