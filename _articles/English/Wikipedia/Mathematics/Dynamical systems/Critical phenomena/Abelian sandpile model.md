---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abelian_sandpile_model
offline_file: ""
offline_thumbnail: ""
uuid: 160c4091-6e4a-4c8b-ba2c-689903442769
updated: 1484308746
title: Abelian sandpile model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Backtang2.png
tags:
    - Definition
    - Sandpile group
    - Self-organized criticality
    - Generalization to directed graphs
    - Least action principle
    - Cultural references
categories:
    - Critical phenomena
---
The Abelian sandpile model, also known as the Bak–Tang–Wiesenfeld model, was the first discovered example of a dynamical system displaying self-organized criticality. It was introduced by Per Bak, Chao Tang and Kurt Wiesenfeld in a 1987 paper.[1]
