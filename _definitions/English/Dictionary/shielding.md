---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shielding
offline_file: ""
offline_thumbnail: ""
uuid: c43e9d52-4c2e-4fbe-9ec0-ed9c98aec449
updated: 1484310377
title: shielding
categories:
    - Dictionary
---
shielding
     n 1: the act of shielding from harm
     2: a shield of lead or concrete intended as a barrier to
        radiation emitted in nuclear decay
     3: shield consisting of an arrangement of metal mesh or plates
        designed to protect electronic equipment from ambient
        electromagnetic interference
