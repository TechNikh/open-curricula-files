---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_game_theorists
offline_file: ""
offline_thumbnail: ""
uuid: d5301808-9303-4f27-b707-35dac83ef7a4
updated: 1484308518
title: List of game theorists
categories:
    - Game theorists
---
This is a list of notable economists, mathematicians, political scientists, and computer scientists whose work has added substantially to the field of game theory. For a list of people in the field of video games rather than game theory, please see list of ludologists.
