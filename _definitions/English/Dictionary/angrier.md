---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/angrier
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479801
title: angrier
categories:
    - Dictionary
---
angry
     adj 1: feeling or showing anger; "angry at the weather"; "angry
            customers"; "an angry silence"; "sending angry letters
            to the papers" [ant: {unangry(p)}]
     2: (of the elements) as if showing violent anger; "angry clouds
        on the horizon"; "furious winds"; "the raging sea" [syn: {furious},
         {raging}, {tempestuous}, {wild}]
     3: severely inflamed and painful; "an angry sore"
     [also: {angriest}, {angrier}]
