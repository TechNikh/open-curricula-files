---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Whirlpool
offline_file: ""
offline_thumbnail: ""
uuid: 7abca207-4648-472b-af07-e5cb03216c8c
updated: 1484308879
title: Whirlpool
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-The_Corryvreckan_Whirlpool_-_geograph-2404815-by-Walter-Baxter.jpg
tags:
    - Notable whirlpools
    - Moskstraumen
    - Saltstraumen
    - Corryvreckan
    - Other notable maelstroms and whirlpools
    - Dangers
    - In literature and popular culture
    - Etymology
categories:
    - Vortices
---
A whirlpool is a body of swirling water produced by the meeting of opposing currents. The vast majority of whirlpools are not very powerful and very small whirlpools can easily be seen when a bath or a sink is draining. More powerful ones in seas or oceans may be termed maelstroms. Vortex is the proper term for any whirlpool that has a downdraft.
