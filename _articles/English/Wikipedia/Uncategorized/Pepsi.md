---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pepsi
offline_file: ""
offline_thumbnail: ""
uuid: 6d59f255-f959-4944-aea0-bacfb4ecaf7c
updated: 1484308460
title: Pepsi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HMB_Bern_New_Bern_Caleb_Bradham.jpg
tags:
    - History
    - Pepsi-Cola trademark
    - Rise
    - Niche marketing
    - Pepsi Perfect
    - Marketing
    - Rivalry with Coca-Cola
    - Pepsiman
    - Car contest in Novosibirsk
    - Ingredients
categories:
    - Uncategorized
---
Pepsi (currently stylized as pepsi and formerly stylized as PEPSI) is an American carbonated soft drink produced and manufactured by PepsiCo. Originally created and developed in 1893 and introduced as "Brad's Drink", it was renamed as Pepsi-Cola on August 28, 1898, and then as Pepsi in 1961. It is currently known in North America alternatively as Pepsi-Cola as of 2014.[1]
