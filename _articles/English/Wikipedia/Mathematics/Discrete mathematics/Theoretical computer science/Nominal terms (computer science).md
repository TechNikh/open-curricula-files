---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Nominal_terms_(computer_science)
offline_file: ""
offline_thumbnail: ""
uuid: 3cbdf39b-9bb3-40ba-bd90-9e45e2891977
updated: 1484308675
title: Nominal terms (computer science)
tags:
    - Motivation
    - Syntax
    - Example embeddings
    - Unification algorithm
    - Relation with higher-order patterns
categories:
    - Theoretical computer science
---
Nominal terms are a metalanguage for embedding object languages with binding constructs into. Intuitively, they may be seen as an extension of first-order terms with support for name binding. Consequently, the native notion of equality between two nominal terms is alpha-equivalence (equivalence up to a permutative renaming of bound names). Nominal terms came out of a programme of research into nominal sets, and have a concrete semantics in those sets.
