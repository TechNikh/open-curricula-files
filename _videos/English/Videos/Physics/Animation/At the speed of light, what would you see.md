---
version: 1
type: video
provider: YouTube
id: BoUc4-q4Ibc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 10b767fa-4558-443e-92b8-41aac14cf159
updated: 1486069656
title: At the speed of light, what would you see?
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/BoUc4-q4Ibc/default.jpg
    - https://i3.ytimg.com/vi/BoUc4-q4Ibc/1.jpg
    - https://i3.ytimg.com/vi/BoUc4-q4Ibc/2.jpg
    - https://i3.ytimg.com/vi/BoUc4-q4Ibc/3.jpg
---
