---
version: 1
type: video
provider: YouTube
id: rjLJIVoQxz4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Calculus%20-%20The%20foundation%20of%20modern%20science.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b5e0d883-e378-4901-97e0-cf767b2cffdd
updated: 1486069657
title: 'Calculus -- The foundation of modern science'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/rjLJIVoQxz4/default.jpg
    - https://i3.ytimg.com/vi/rjLJIVoQxz4/1.jpg
    - https://i3.ytimg.com/vi/rjLJIVoQxz4/2.jpg
    - https://i3.ytimg.com/vi/rjLJIVoQxz4/3.jpg
---
