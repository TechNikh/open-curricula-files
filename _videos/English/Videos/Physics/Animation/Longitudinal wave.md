---
version: 1
type: video
provider: YouTube
id: f66syH8B9D8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Longitudinal%20wave.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8ef0d3fe-1ce3-4edc-b6ae-2768f949d88a
updated: 1486069652
title: Longitudinal wave
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/f66syH8B9D8/default.jpg
    - https://i3.ytimg.com/vi/f66syH8B9D8/1.jpg
    - https://i3.ytimg.com/vi/f66syH8B9D8/2.jpg
    - https://i3.ytimg.com/vi/f66syH8B9D8/3.jpg
---
