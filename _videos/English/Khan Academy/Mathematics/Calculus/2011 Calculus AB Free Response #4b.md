---
version: 1
type: video
provider: YouTube
id: OvMBNVi5bLY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%234b.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0b8360e4-c40a-46df-9e4d-03b4e22a4c51
updated: 1486069692
title: '2011 Calculus AB Free Response #4b'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/OvMBNVi5bLY/default.jpg
    - https://i3.ytimg.com/vi/OvMBNVi5bLY/1.jpg
    - https://i3.ytimg.com/vi/OvMBNVi5bLY/2.jpg
    - https://i3.ytimg.com/vi/OvMBNVi5bLY/3.jpg
---
