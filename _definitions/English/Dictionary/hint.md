---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hint
offline_file: ""
offline_thumbnail: ""
uuid: 4d532540-7726-41ff-9ab3-a98cc96631eb
updated: 1484310309
title: hint
categories:
    - Dictionary
---
hint
     n 1: an indirect suggestion; "not a breath of scandal ever
          touched her" [syn: {intimation}, {breath}]
     2: a slight indication [syn: {clue}]
     3: a slight but appreciable addition; "this dish could use a
        touch of garlic" [syn: {touch}, {tinge}, {mite}, {pinch},
        {jot}, {speck}, {soupcon}]
     4: a just detectable amount; "he speaks French with a trace of
        an accent" [syn: {trace}, {suggestion}]
     5: an indication of potential opportunity; "he got a tip on the
        stock market"; "a good lead for a job" [syn: {tip}, {lead},
         {steer}, {confidential information}, {wind}]
     v : drop a hint; intimate by a hint [syn: {suggest}]
