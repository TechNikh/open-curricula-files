---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/receive
offline_file: ""
offline_thumbnail: ""
uuid: 179b5131-b7c1-4055-acdd-8329f86a1127
updated: 1484310414
title: receive
categories:
    - Dictionary
---
receive
     v 1: get something; come into possession of; "receive payment";
          "receive a gift"; "receive letters from the front" [syn:
           {have}]
     2: receive a specified treatment (abstract); "These aspects of
        civilization do not find expression or receive an
        interpretation"; "His movie received a good review"; "I
        got nothing but trouble for my good intentions" [syn: {get},
         {find}, {obtain}, {incur}]
     3: recieve (perceptual input); "pick up a signal" [syn: {pick
        up}]
     4: of mental or physical states or experiences; "get an idea";
        "experience vertigo"; "get nauseous"; "undergo a strange
        sensation"; "The ...
