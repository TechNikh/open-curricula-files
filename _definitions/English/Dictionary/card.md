---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/card
offline_file: ""
offline_thumbnail: ""
uuid: f2a48f41-6337-4c18-bda0-b69e6c1288d3
updated: 1484310369
title: card
categories:
    - Dictionary
---
card
     n 1: one of a set of small pieces of stiff paper marked in
          various ways and used for playing games or for telling
          fortunes; "he collected cards and traded them with the
          other boys"
     2: a card certifying the identity of the bearer; "he had to
        show his card to get in" [syn: {identity card}]
     3: a rectangular piece of stiff paper used to send messages
        (may have printed greetings or pictures); "they sent us a
        card from Miami"
     4: thin cardboard, usually rectangular
     5: a witty amusing person who makes jokes [syn: {wag}, {wit}]
     6: a sign posted in a public place as an advertisement; "a
        poster advertised ...
