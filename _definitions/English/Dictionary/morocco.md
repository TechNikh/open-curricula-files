---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morocco
offline_file: ""
offline_thumbnail: ""
uuid: fe2d2d91-febd-4d9d-8b1b-5c962ad97bb8
updated: 1484310180
title: morocco
categories:
    - Dictionary
---
Morocco
     n 1: a kingdom (constitutional monarchy) in northwestern Africa
          with a largely Muslim population; achieved independence
          from France in 1956 [syn: {Kingdom of Morocco}, {Maroc},
           {Marruecos}, {Al-Magrib}]
     2: a soft pebble-grained leather made from goatskin; used for
        shoes and book bindings etc.
