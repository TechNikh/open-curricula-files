---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Limit_set
offline_file: ""
offline_thumbnail: ""
uuid: 4f9cbb15-bf78-48be-9816-d4ad496f35fe
updated: 1484308805
title: Limit set
tags:
    - Types
    - Definition for iterated functions
    - Definition for flows
    - Examples
    - Properties
categories:
    - Limit sets
---
In mathematics, especially in the study of dynamical systems, a limit set is the state a dynamical system reaches after an infinite amount of time has passed, by either going forward or backwards in time. Limit sets are important because they can be used to understand the long term behavior of a dynamical system.
