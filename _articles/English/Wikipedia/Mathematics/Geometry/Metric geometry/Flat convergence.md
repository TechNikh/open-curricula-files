---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flat_convergence
offline_file: ""
offline_thumbnail: ""
uuid: 4ebb285f-2ccc-4bfb-ad52-e035a0889ba5
updated: 1484309262
title: Flat convergence
tags:
    - Integral currents
    - Flat norm and flat distance
    - Compactness theorem
categories:
    - Metric geometry
---
In mathematics, flat convergence is a notion for convergence of submanifolds of Euclidean space. It was first introduced by Hassler Whitney in 1957, and then extended to "integral currents" by Federer and Fleming in 1960. It forms a fundamental part of the field of geometric measure theory. The notion was applied to find solutions to Plateau's problem. In 2001 the notion of an integral current was extended to arbitrary metric spaces by Ambrosio and Kirchheim.
