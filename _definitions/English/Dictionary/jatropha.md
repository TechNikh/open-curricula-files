---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jatropha
offline_file: ""
offline_thumbnail: ""
uuid: 964b020c-7a95-4e93-841e-c40fa3ac11fc
updated: 1484310242
title: jatropha
categories:
    - Dictionary
---
Jatropha
     n : a mainly tropical genus of American plant belonging to the
         family Euphorbiaceae [syn: {genus Jatropha}]
