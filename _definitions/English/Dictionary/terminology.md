---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terminology
offline_file: ""
offline_thumbnail: ""
uuid: ecde6247-d295-487a-b971-9edd089cb71a
updated: 1484310206
title: terminology
categories:
    - Dictionary
---
terminology
     n : a system of words used in a particular discipline; "legal
         terminology"; "the language of sociology" [syn: {nomenclature},
          {language}]
