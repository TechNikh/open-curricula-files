---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Golden_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 725afba9-3c19-46c4-bc0b-2963997cbb43
updated: 1484309249
title: Golden ratio
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Golden_ratio_line.svg.png
tags:
    - Calculation
    - History
    - Timeline
    - Applications and observations
    - Aesthetics
    - Architecture
    - Painting
    - Book design
    - Design
    - Music
    - Nature
    - Optimization
    - Perceptual studies
    - Mathematics
    - Irrationality
    - Contradiction from an expression in lowest terms
    - Derivation from irrationality of √5
    - Minimal polynomial
    - Golden ratio conjugate
    - Alternative forms
    - Geometry
    - Dividing a line segment by interior division
    - Dividing a line segment by exterior division
    - Golden triangle, pentagon and pentagram
    - Golden triangle
    - Pentagon
    - "Odom's construction"
    - Pentagram
    - "Ptolemy's theorem"
    - Scalenity of triangles
    - Triangle whose sides form a geometric progression
    - Golden triangle, rhombus, and rhombic triacontahedron
    - Relationship to Fibonacci sequence
    - Symmetries
    - Other properties
    - Decimal expansion
    - Pyramids
    - Mathematical pyramids and triangles
    - Egyptian pyramids
    - Disputed observations
    - References and footnotes
categories:
    - History of geometry
---
In mathematics, two quantities are in the golden ratio if their ratio is the same as the ratio of their sum to the larger of the two quantities. The figure on the right illustrates the geometric relationship. Expressed algebraically, for quantities a and b with a > b > 0,
