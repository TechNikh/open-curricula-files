---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_zeta-function
offline_file: ""
offline_thumbnail: ""
uuid: 4a74fc52-5d3d-42c1-ae39-4acd90be3adb
updated: 1484308782
title: Local zeta-function
tags:
    - Formulation
    - Examples
    - Motivations
    - Riemann hypothesis for curves over finite fields
    - General formulas for the zeta function
categories:
    - Fixed points (mathematics)
---
Suppose that V is a non-singular n-dimensional projective algebraic variety over the field Fq with q elements. In number theory, the local zeta function Z(V, s) of V (sometimes called the congruent zeta function) is defined as
