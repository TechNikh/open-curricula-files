---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Connelly
offline_file: ""
offline_thumbnail: ""
uuid: e902b966-eadc-4f5c-a5f3-f2c79d668b59
updated: 1484309075
title: Robert Connelly
categories:
    - Geometers
---
Robert (Bob) Connelly is a mathematician specializing in discrete geometry and rigidity theory. Connelly received his Ph.D. from University of Michigan in 1969.[1] He is currently a professor at Cornell University.
