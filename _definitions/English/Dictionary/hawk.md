---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hawk
offline_file: ""
offline_thumbnail: ""
uuid: 906b8c4a-2121-4681-949e-40ef0161b919
updated: 1484310275
title: hawk
categories:
    - Dictionary
---
hawk
     n 1: diurnal bird of prey typically having short rounded wings
          and a long tail
     2: an advocate of an aggressive policy on foreign relations
        [syn: {war hawk}] [ant: {dove}]
     3: a square board with a handle underneath; used by masons to
        hold or carry mortar [syn: {mortarboard}]
     v 1: sell or offer for sale from place to place [syn: {peddle}, {monger},
           {huckster}, {vend}, {pitch}]
     2: hunt with hawks; "the Arabs like to hawk in the desert"
     3: clear mucus or food from one's throat; "he cleared his
        throat before he started to speak" [syn: {clear the throat}]
