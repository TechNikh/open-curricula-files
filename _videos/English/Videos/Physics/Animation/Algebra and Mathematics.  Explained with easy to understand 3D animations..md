---
version: 1
type: video
provider: YouTube
id: MJYsXGbxueY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Algebra%20and%20Mathematics.%20%20Explained%20with%20easy%20to%20understand%203D%20animations..webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8d7481f7-bf04-4067-bec9-536b5afe65be
updated: 1486069656
title: 'Algebra and Mathematics.  Explained with easy to understand 3D animations.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/MJYsXGbxueY/default.jpg
    - https://i3.ytimg.com/vi/MJYsXGbxueY/1.jpg
    - https://i3.ytimg.com/vi/MJYsXGbxueY/2.jpg
    - https://i3.ytimg.com/vi/MJYsXGbxueY/3.jpg
---
