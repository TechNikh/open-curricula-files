---
version: 1
type: video
provider: YouTube
id: vKNpQ_KTXvE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Binomial%20Distribution%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e7c0a5cd-6a93-4c34-81ef-4b134bdbcee5
updated: 1486069688
title: Binomial Distribution 3
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/vKNpQ_KTXvE/default.jpg
    - https://i3.ytimg.com/vi/vKNpQ_KTXvE/1.jpg
    - https://i3.ytimg.com/vi/vKNpQ_KTXvE/2.jpg
    - https://i3.ytimg.com/vi/vKNpQ_KTXvE/3.jpg
---
