---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conscious
offline_file: ""
offline_thumbnail: ""
uuid: d58a83c8-9aee-430e-9e73-e8b055ad0338
updated: 1484310323
title: conscious
categories:
    - Dictionary
---
conscious
     adj 1: intentionally conceived; "a conscious effort to speak more
            slowly"; "a conscious policy" [syn: {witting}]
     2: knowing and perceiving; having awareness of surroundings and
        sensations and thoughts; "remained conscious during the
        operation"; "conscious of his faults"; "became conscious
        that he was being followed" [ant: {unconscious}]
     3: (followed by `of') showing realization or recognition of
        something; "few voters seem conscious of the issue's
        importance"; "conscious of having succeeded"; "the careful
        tread of one conscious of his alcoholic load"- Thomas
        Hardy [syn: {conscious(p)}]
