---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discrete
offline_file: ""
offline_thumbnail: ""
uuid: 06c9de76-0317-4148-89bb-c4607fd1b3ef
updated: 1484310389
title: discrete
categories:
    - Dictionary
---
discrete
     adj : constituting a separate entity or part; "a government with
           three discrete divisions"; "on two distinct occasions"
           [syn: {distinct}]
