---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sunflower
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484326141
title: sunflower
categories:
    - Dictionary
---
sunflower
     n : any plant of the genus Helianthus having large flower heads
         with dark disk florets and showy yellow rays [syn: {helianthus}]
