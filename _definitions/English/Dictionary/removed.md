---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/removed
offline_file: ""
offline_thumbnail: ""
uuid: 7f479dac-43a2-4359-a809-fc0efe8ff13c
updated: 1484310316
title: removed
categories:
    - Dictionary
---
removed
     adj 1: taken out of or separated from; "possibility
            is...achievability, abstracted from achievement"-
            A.N.Whitehead [syn: {abstracted}]
     2: far apart in nature; "considerations entirely removed (or
        remote) from politics" [syn: {remote}, {removed(p)}]
     3: far distant in space; "distant lands"; "remote stars"; "a
        remote outpost of civilization"; "a hideaway far removed
        from towns and cities" [syn: {distant}, {remote}]
     4: separated in relationship by a given degree of descent; "a
        cousin once removed" [syn: {removed(p)}]
     5: far distant in time; "distant events"; "the remote past or
        future"; "a ...
