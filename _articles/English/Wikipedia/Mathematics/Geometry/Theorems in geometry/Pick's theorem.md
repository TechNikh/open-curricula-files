---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pick%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 03155a3b-806e-4002-9fe8-abc7f6600b01
updated: 1484309119
title: "Pick's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Pick-theorem.png
tags:
    - Proof
categories:
    - Theorems in geometry
---
Given a simple polygon constructed on a grid of equal-distanced points (i.e., points with integer coordinates) such that all the polygon's vertices are grid points, Pick's theorem provides a simple formula for calculating the area A of this polygon in terms of the number i of lattice points in the interior located in the polygon and the number b of lattice points on the boundary placed on the polygon's perimeter:[1]
