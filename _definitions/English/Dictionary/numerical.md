---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/numerical
offline_file: ""
offline_thumbnail: ""
uuid: e20e7c4e-11c6-4636-ad02-6cf6d39270a4
updated: 1484310421
title: numerical
categories:
    - Dictionary
---
numerical
     adj 1: measured or expressed in numbers; "numerical value"; "the
            numerical superiority of the enemy" [syn: {numeric}]
     2: of or relating to or denoting numbers; "a numeral
        adjective"; "numerical analysis" [syn: {numeral}, {numeric}]
     3: designated by or expressed in numbers; "numerical symbols";
        "a very simple numeric code"; "numerical equations" [syn:
        {numeric}]
     4: relating to or having ability to think in or work with
        numbers; "tests for rating numerical aptitude"; "a
        mathematical whiz" [syn: {mathematical}] [ant: {verbal}]
