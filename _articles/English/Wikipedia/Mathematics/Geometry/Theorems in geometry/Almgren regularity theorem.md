---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Almgren_regularity_theorem
offline_file: ""
offline_thumbnail: ""
uuid: b7fbb29f-919f-4dcc-908f-73d587e56dc0
updated: 1484309107
title: Almgren regularity theorem
categories:
    - Theorems in geometry
---
In mathematical geometric measure theory, the Almgren regularity theorem, proved by Almgren (1983, 2000), states that the singular set of a mass-minimizing surface has codimension at least 2. Almgren's proof of this was 955 pages long.
