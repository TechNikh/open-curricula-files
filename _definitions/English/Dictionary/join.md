---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/join
offline_file: ""
offline_thumbnail: ""
uuid: 9fc1cc44-782b-4889-91fb-956e01fd03b2
updated: 1484310323
title: join
categories:
    - Dictionary
---
join
     n 1: the shape or manner in which things come together and a
          connection is made [syn: {articulation}, {joint}, {juncture},
           {junction}]
     2: a set containing all and only the members of two or more
        given sets; "let C be the union of the sets A and B" [syn:
         {union}, {sum}]
     v 1: become part of; become a member of a group or organization;
          "He joined the Communist Party as a young man" [syn: {fall
          in}, {get together}]
     2: cause to become joined or linked; "join these two parts so
        that they fit together" [syn: {bring together}] [ant: {disjoin}]
     3: come into the company of; "She joined him for a drink"
    ...
