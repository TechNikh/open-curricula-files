---
version: 1
type: video
provider: YouTube
id: bCxMhncR7PU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Combinations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 073d3a9e-de2b-440a-bfc6-7f934259a2ba
updated: 1486069688
title: Combinations
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/bCxMhncR7PU/default.jpg
    - https://i3.ytimg.com/vi/bCxMhncR7PU/1.jpg
    - https://i3.ytimg.com/vi/bCxMhncR7PU/2.jpg
    - https://i3.ytimg.com/vi/bCxMhncR7PU/3.jpg
---
