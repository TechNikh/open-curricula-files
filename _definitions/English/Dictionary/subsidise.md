---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsidise
offline_file: ""
offline_thumbnail: ""
uuid: 196bab27-071e-4b67-a985-dd1bcdc0cf7b
updated: 1484310148
title: subsidise
categories:
    - Dictionary
---
subsidise
     v 1: secure the assistence of by granting a subsidy, as of
          nations or military forces [syn: {subsidize}]
     2: support through subsidies; "The arts in Europe are heavily
        subsidized" [syn: {subsidize}]
