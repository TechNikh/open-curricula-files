---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genetically
offline_file: ""
offline_thumbnail: ""
uuid: 321f9af4-6072-43a8-a309-0b64ec84ef76
updated: 1484310299
title: genetically
categories:
    - Dictionary
---
genetically
     adv : by genetic mechanisms; "genetically passed down talents"
