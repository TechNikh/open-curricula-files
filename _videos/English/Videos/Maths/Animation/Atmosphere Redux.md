---
version: 1
type: video
provider: YouTube
id: xCs3W4EWyYA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e29f70e2-afa7-4f09-b79b-e679b4535d2d
updated: 1486069641
title: Atmosphere Redux
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xCs3W4EWyYA/default.jpg
    - https://i3.ytimg.com/vi/xCs3W4EWyYA/1.jpg
    - https://i3.ytimg.com/vi/xCs3W4EWyYA/2.jpg
    - https://i3.ytimg.com/vi/xCs3W4EWyYA/3.jpg
---
