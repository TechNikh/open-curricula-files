---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polished
offline_file: ""
offline_thumbnail: ""
uuid: c39f181b-4d0f-4cb2-a4db-362bc911119d
updated: 1484310162
title: polished
categories:
    - Dictionary
---
polished
     adj 1: perfected or made shiny and smooth; "his polished prose";
            "in a freshly ironed dress and polished shoes";
            "freshly polished silver" [ant: {unpolished}]
     2: showing a high degree of refinement and the assurance that
        comes from wide social experience; "his polished manner";
        "maintained an urbane tone in his letters" [syn: {refined},
         {svelte}, {urbane}]
     3: (of grains especially rice) having the husk or outer layers
        removed; "polished rice" [syn: {milled}]
     4: (of lumber or stone) to trim and smooth [syn: {dressed}]
