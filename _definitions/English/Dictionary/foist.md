---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foist
offline_file: ""
offline_thumbnail: ""
uuid: 409e96d7-9466-42d9-9361-0e83eedd4af6
updated: 1484310607
title: foist
categories:
    - Dictionary
---
foist
     v 1: to force onto another; "He foisted his work on me"
     2: insert surreptitiously or without warrant
