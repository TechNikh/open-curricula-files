---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Celso_Grebogi
offline_file: ""
offline_thumbnail: ""
uuid: ae2d0209-2809-41e0-82cc-7c82510a1f03
updated: 1484308724
title: Celso Grebogi
tags:
    - Research areas
    - Edited books
categories:
    - Chaos theory
---
Celso Grebogi (born 1947) is a Brazilian theoretical physicist who works in the area of chaos theory. He is one among the pioneers in the nonlinear and complex systems and chaos theory. Currently he works at the University of Aberdeen as the "Sixth Century Chair in Nonlinear and Complex Systems". He has done extensive research in the field of plasma physics before his work on the theory of dynamical systems. He and his colleagues (Edward Ott and James A. Yorke ) have shown with a numerical example that one can convert a chaotic attractor to any one of a large number of possible attracting time-periodic motions by making only small time-dependent perturbations of an available system ...
