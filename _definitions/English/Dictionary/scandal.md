---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scandal
offline_file: ""
offline_thumbnail: ""
uuid: f332c514-cc76-4eb8-9edc-12385d57e531
updated: 1484310573
title: scandal
categories:
    - Dictionary
---
scandal
     n 1: disgraceful gossip about the private lives of other people
          [syn: {dirt}, {malicious gossip}]
     2: a disgraceful event [syn: {outrage}]
