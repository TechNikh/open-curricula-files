---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_partition
offline_file: ""
offline_thumbnail: ""
uuid: 314c262c-de96-4c78-8b53-3a330ea694c4
updated: 1484309391
title: Graph partition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bisected_network.jpg
tags:
    - Problem complexity
    - Problem
    - Analysis
    - Graph partition methods
    - Multi-level methods
    - Spectral partitioning and spectral bisection
    - Other graph partition methods
    - Software tools
    - Bibliography
categories:
    - Computational problems in graph theory
---
In mathematics, the graph partition problem is defined on data represented in the form of a graph G = (V,E), with V vertices and E edges, such that it is possible to partition G into smaller components with specific properties. For instance, a k-way partition divides the vertex set into k smaller components. A good partition is defined as one in which the number of edges running between separated components is small. Uniform graph partition is a type of graph partitioning problem that consists of dividing a graph into components, such that the components are of about the same size and there are few connections between the components. Important applications of graph partitioning include ...
