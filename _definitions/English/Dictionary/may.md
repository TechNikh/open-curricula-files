---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/may
offline_file: ""
offline_thumbnail: ""
uuid: 7963b657-b3c6-4c5c-b353-a30c1e6b036a
updated: 1484310357
title: may
categories:
    - Dictionary
---
May
     n 1: the month following April and preceding June
     2: thorny Eurasian shrub of small tree having dense clusters of
        white to scarlet flowers followed by deep red berries;
        established as an escape in eastern North America [syn: {whitethorn},
         {English hawthorn}, {Crataegus laevigata}, {Crataegus
        oxycantha}]
