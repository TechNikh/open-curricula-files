---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Trakhtenbrot%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b3064d61-66fa-4f10-9ae7-10117dc0924b
updated: 1484309490
title: "Trakhtenbrot's theorem"
tags:
    - Mathematical formulation
    - Theorem
    - Corollary
    - Alternative Proof
categories:
    - Computability theory
---
In Logic, finite model theory, and Computability Theory, Trakhtenbrot's theorem (due to Boris Trakhtenbrot) states that the problem of validity in First-order logic (FO) on the class of all finite models is undecidable. In fact, the class of valid sentences over finite models is not recursively enumerable (though it is co-recursively enumerable).
