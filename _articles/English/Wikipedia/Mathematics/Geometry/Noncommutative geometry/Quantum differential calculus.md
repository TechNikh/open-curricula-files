---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_differential_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 981dd4e7-d4d9-482b-b345-c9ee90f92327
updated: 1484309292
title: Quantum differential calculus
tags:
    - Note
    - Examples
categories:
    - Noncommutative geometry
---
In quantum geometry or noncommutative geometry a quantum differential calculus or noncommutative differential structure on an algebra 
  
    
      
        A
      
    
    {\displaystyle A}
  
 over a field 
  
    
      
        k
      
    
    {\displaystyle k}
  
 means the specification of a space of differential forms over the algebra. The algebra 
  
    
      
        A
      
    
    {\displaystyle A}
  
 here is regarded as a coordinate ring but it is important that it may be noncommutative and hence not an actual algebra of coordinate functions on any actual space, so this represents a point of view replacing the specification of a differentiable structure for an actual ...
