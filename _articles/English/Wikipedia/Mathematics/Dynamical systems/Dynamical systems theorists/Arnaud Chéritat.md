---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Arnaud_Ch%C3%A9ritat'
offline_file: ""
offline_thumbnail: ""
uuid: 9ecfbf3c-ba46-43c7-8ea3-300aafac9f56
updated: 1484308690
title: Arnaud Chéritat
categories:
    - Dynamical systems theorists
---
Arnaud Chéritat (born June 7, 1975) is a French mathematician who works as a director of research at the Institut de Mathématiques de Toulouse.[1] His research concerns complex dynamics and the shape of Julia sets.[2]
