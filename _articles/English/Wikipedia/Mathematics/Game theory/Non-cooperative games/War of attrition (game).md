---
version: 1
type: article
id: https://en.wikipedia.org/wiki/War_of_attrition_(game)
offline_file: ""
offline_thumbnail: ""
uuid: 8f30344f-3254-4676-810c-a75835f7b5cb
updated: 1484309059
title: War of attrition (game)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Bluetank_0.png
tags:
    - Examining the game
    - Dynamic formulation and evolutionarily stable strategy
    - The ESS in popular culture
    - Sources
categories:
    - Non-cooperative games
---
In game theory, the war of attrition is a dynamic timing game in which players choose a time to stop, and fundamentally trade off the strategic gains from outlasting other players and the real costs expended with the passage of time. Its precise opposite is the pre-emption game, in which players elect a time to stop, and fundamentally trade off the strategic costs from outlasting other players and the real gains occasioned by the passage of time. The model was originally formulated by John Maynard Smith;[1] a mixed evolutionarily stable strategy (ESS) was determined by Bishop & Cannings.[2] An example is an all-pay auction, in which the prize goes to the player with the highest bid and each ...
