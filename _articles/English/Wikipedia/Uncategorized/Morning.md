---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Early_morning
offline_file: ""
offline_thumbnail: ""
uuid: 6fff5ebd-1a84-4a50-a1a8-62cc6312c2dd
updated: 1484308372
title: Morning
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Misty_morning02.jpg
tags:
    - Etymology
    - Significance for humans
categories:
    - Uncategorized
---
Morning is the period of time between midnight and noon or, more commonly, the interval between sunrise and noon.[1] Morning precedes afternoon, evening, and night in the sequence of a day. Originally, the term referred to sunrise.
