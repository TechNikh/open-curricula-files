---
version: 1
type: video
provider: YouTube
id: t4zfiBw0hwM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Finance%20%26%20Capital%20Markets/Interest%20and%20debt/Interest%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b9718a01-0561-4f51-a846-8f48d9172553
updated: 1486069694
title: Interest (part 2)
categories:
    - Interest and debt
thumbnail_urls:
    - https://i3.ytimg.com/vi/t4zfiBw0hwM/default.jpg
    - https://i3.ytimg.com/vi/t4zfiBw0hwM/1.jpg
    - https://i3.ytimg.com/vi/t4zfiBw0hwM/2.jpg
    - https://i3.ytimg.com/vi/t4zfiBw0hwM/3.jpg
---
