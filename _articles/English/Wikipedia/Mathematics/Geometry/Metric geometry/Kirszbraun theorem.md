---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kirszbraun_theorem
offline_file: ""
offline_thumbnail: ""
uuid: e1b1e5e7-50d1-4e6c-a0c2-f81da16b4be8
updated: 1484309270
title: Kirszbraun theorem
categories:
    - Metric geometry
---
In mathematics, specifically real analysis and functional analysis, the Kirszbraun theorem states that if U is a subset of some Hilbert space H1, and H2 is another Hilbert space, and
