---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crystallographic_point_group
offline_file: ""
offline_thumbnail: ""
uuid: 2753758e-eeeb-4da3-bf65-bb658d644e3a
updated: 1484309307
title: Crystallographic point group
tags:
    - Notation
    - Schoenflies notation
    - Hermann–Mauguin notation
    - The correspondence between different notations
categories:
    - Symmetry
---
In crystallography, a crystallographic point group is a set of symmetry operations, like rotations or reflections, that leave a central point fixed while moving other directions and faces of the crystal to the positions of features of the same kind. For a periodic crystal (as opposed to a quasicrystal), the group must also be consistent with maintenance of the three-dimensional translational symmetry that defines crystallinity. The macroscopic properties of a crystal would look exactly the same before and after any of the operations in its point group. In the classification of crystals, each point group is also known as a crystal class.
