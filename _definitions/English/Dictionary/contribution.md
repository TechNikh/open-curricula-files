---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contribution
offline_file: ""
offline_thumbnail: ""
uuid: 564d47e6-f186-4dcf-8a87-22171a5f5fb4
updated: 1484310403
title: contribution
categories:
    - Dictionary
---
contribution
     n 1: any one of a number of individual efforts in a common
          endeavor; "I am proud of my contribution to the team's
          success"; "they all did their share of the work" [syn: {part},
           {share}]
     2: a voluntary gift (as of money or service or ideas) made to
        some worthwhile cause [syn: {donation}]
     3: act of giving in common with others for a common purpose
        especially to a charity [syn: {donation}]
     4: an amount of money contributed; "he expected his
        contribution to be repaid with interest"
     5: a writing for publication especially one of a collection of
        writings as an article or story
