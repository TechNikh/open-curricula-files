---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/odds
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546881
title: odds
categories:
    - Dictionary
---
odds
     n 1: the probability of a specified outcome [syn: {likelihood}, {likeliness}]
          [ant: {unlikelihood}, {unlikelihood}]
     2: the ratio by which one better's wager is greater than that
        of another; "he offered odds of two to one" [syn: {betting
        odds}]
