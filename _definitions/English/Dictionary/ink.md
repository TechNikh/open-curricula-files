---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ink
offline_file: ""
offline_thumbnail: ""
uuid: 0c29d644-8bd8-4970-bc61-b9ea794a1ba9
updated: 1484310601
title: ink
categories:
    - Dictionary
---
ink
     n 1: a liquid used for printing or writing or drawing
     2: dark protective fluid ejected into the water by cuttlefish
        and other cephalopods
     v 1: append one's signature to; "They inked the contract"
     2: fill with ink; "ink a pen"
