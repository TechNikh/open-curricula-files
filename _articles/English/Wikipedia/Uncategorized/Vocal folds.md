---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vocal_cords
offline_file: ""
offline_thumbnail: ""
uuid: 19d99306-68ad-40dd-bb02-a326d6a4f7c6
updated: 1484308375
title: Vocal folds
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray956.png
tags:
    - Structure
    - Variations
    - False vocal folds
    - Histology
    - Development
    - In newborns
    - In adults
    - Maturation
    - Macula flavae
    - Impact of phonation
    - Impact of hormones
    - Function
    - Oscillation
    - Clinical significance
    - Wound healing
    - History
    - Etymology
    - Additional images
    - Bibliography
categories:
    - Uncategorized
---
The vocal folds, also known commonly as vocal cords or voice reeds, are composed of twin infoldings of mucous membrane stretched horizontally, from back to front, across the larynx. They vibrate, modulating the flow of air being expelled from the lungs during phonation.[1][2][3]
