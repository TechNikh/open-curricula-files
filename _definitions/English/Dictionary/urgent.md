---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urgent
offline_file: ""
offline_thumbnail: ""
uuid: fd7af683-54f6-4ee4-87bd-30e135a3d66a
updated: 1484310522
title: urgent
categories:
    - Dictionary
---
urgent
     adj : compelling immediate action; "too pressing to permit of
           longer delay"; "the urgent words `Hurry! Hurry!'";
           "bridges in urgent need of repair" [syn: {pressing}]
