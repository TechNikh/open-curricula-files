---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laundry
offline_file: ""
offline_thumbnail: ""
uuid: a9e8d22b-66ea-4600-b209-edf1779739a5
updated: 1484310384
title: laundry
categories:
    - Dictionary
---
laundry
     n 1: garments or white goods that can be cleaned by laundering
          [syn: {wash}, {washing}, {washables}]
     2: workplace where clothes are washed and ironed
