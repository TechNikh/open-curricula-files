---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calendar
offline_file: ""
offline_thumbnail: ""
uuid: 7944d4d5-31a2-4c26-a2be-81322c9594ce
updated: 1484310559
title: calendar
categories:
    - Dictionary
---
calendar
     n 1: a system of timekeeping that defines the beginning and
          length and divisions of the year
     2: a list or register of events (appointments or social events
        or court cases etc); "I have you on my calendar for next
        Monday"
     3: a tabular array of the days (usually for one year)
     v : enter into a calendar
