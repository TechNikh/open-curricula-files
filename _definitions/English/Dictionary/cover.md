---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cover
offline_file: ""
offline_thumbnail: ""
uuid: d6db2536-ae07-46e3-b5cb-61f4a69fad77
updated: 1484310305
title: cover
categories:
    - Dictionary
---
cover
     n 1: a covering that serves to conceal or shelter something;
          "they crouched behind the screen"; "under cover of
          darkness" [syn: {screen}, {covert}, {concealment}]
     2: bedding that keeps a person warm in bed; "he pulled the
        covers over his head and went to sleep" [syn: {blanket}]
     3: the act of concealing the existence of something by
        obstructing the view of it; "the cover concealed their
        guns from enemy aircraft" [syn: {covering}, {screening}, {masking}]
     4: the front and back covering of a book; "the book had a
        leather binding" [syn: {binding}, {book binding}, {back}]
     5: a natural object that covers or ...
