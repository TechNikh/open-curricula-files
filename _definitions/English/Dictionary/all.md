---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/all
offline_file: ""
offline_thumbnail: ""
uuid: 25b332e5-8c69-4d17-8cae-f7bb1d76811f
updated: 1484310343
title: all
categories:
    - Dictionary
---
all
     adj 1: quantifier; used with either mass or count nouns to indicate
            the whole number or amount of or every one of a class;
            "we sat up all night"; "ate all the food"; "all men
            are mortal"; "all parties are welcome" [syn: {all(a)}]
            [ant: {some(a)}, {no(a)}]
     2: completely given to or absorbed by; "became all attention"
     adv : to a complete degree or to the full or entire extent
           (`whole' is often used informally for `wholly'); "he
           was wholly convinced"; "entirely satisfied with the
           meal"; "it was completely different from what we
           expected"; "was completely at fault"; "a totally new
     ...
