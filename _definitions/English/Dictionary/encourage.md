---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encourage
offline_file: ""
offline_thumbnail: ""
uuid: 7d185cb7-908d-4405-aa40-cdc5a291f514
updated: 1484310462
title: encourage
categories:
    - Dictionary
---
encourage
     v 1: contribute to the progress or growth of; "I am promoting the
          use of computers in the classroom" [syn: {promote}, {advance},
           {boost}, {further}]
     2: inspire with confidence; give hope or courage to [ant: {discourage}]
     3: spur on; "His financial success encouraged him to look for a
        wife"
