---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amides
offline_file: ""
offline_thumbnail: ""
uuid: b7bb0aaf-05eb-417f-bfc3-2ea6cadc5023
updated: 1484308403
title: Amide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Amide_types.svg.png
tags:
    - Structure and bonding
    - Nomenclature
    - Pronunciation
    - Properties
    - Basicity
    - Solubility
    - Characterization
    - Applications and occurrence
    - Amide synthesis
    - Other methods
    - Amide reactions
categories:
    - Uncategorized
---
An amide (/ˈæmaɪd/ or /ˈæmɪd/ or /ˈeɪmaɪd/),[1][2][3] also known as an acid amide, is a compound with the functional group RnE(O)xNR′2 (R and R′ refer to H or organic groups). Most common are carboxamides (organic amides) (n = 1, E = C, x = 1), but many other important types of amides are known, including phosphoramides (n = 2, E = P, x = 1 and many related formulas) and sulfonamides (E = S, x = 2).[4] The term amide refers both to classes of compounds and to the functional group (RnE(O)xNR′2) within those compounds.
