---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Translation_of_axes
offline_file: ""
offline_thumbnail: ""
uuid: c2acfad2-ce9d-4752-849d-333a6680605f
updated: 1484309338
title: Translation of axes
tags:
    - Motivation
    - Translation of conic sections
    - Example 1
    - Generalization to several dimensions
    - Translation of quadric surfaces
    - Example 2
    - Notes
categories:
    - Transformation (function)
---
In mathematics, a translation of axes in two dimensions is a mapping from an xy-Cartesian coordinate system to an x'y'-Cartesian coordinate system in which the x' axis is parallel to the x axis and k units away, and the y' axis is parallel to the y axis and h units away. This means that the origin O' of the new coordinate system has coordinates (h, k) in the original system. The positive x' and y' directions are taken to be the same as the positive x and y directions. A point P has coordinates (x, y) with respect to the original system and coordinates (x', y') with respect to the new system, where
