---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wasting
offline_file: ""
offline_thumbnail: ""
uuid: a2b5391e-3897-4cf7-be27-f5bae58efeda
updated: 1484310537
title: wasting
categories:
    - Dictionary
---
wasting
     n 1: any general reduction in vitality and strength of body and
          mind resulting from a debilitating chronic disease [syn:
           {cachexia}, {cachexy}]
     2: a decrease in size of an organ caused by disease or disuse
        [syn: {atrophy}, {wasting away}]
