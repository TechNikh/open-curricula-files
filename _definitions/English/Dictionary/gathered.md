---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gathered
offline_file: ""
offline_thumbnail: ""
uuid: deb3a3a2-8b94-4480-9769-9a117ea832fe
updated: 1484310313
title: gathered
categories:
    - Dictionary
---
gathered
     adj 1: having accumulated or become more intense; "the deepened
            gloom" [syn: {deepened}]
     2: brought together in one place; "the collected works of
        Milton"; "the gathered folds of the skirt" [syn: {collected}]
        [ant: {uncollected}, {uncollected}]
