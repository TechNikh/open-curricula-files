---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wood
offline_file: ""
offline_thumbnail: ""
uuid: 543d90a4-3621-4b4e-9579-b4413ed1a42c
updated: 1484310290
title: wood
categories:
    - Dictionary
---
wood
     n 1: the hard fibrous lignified substance under the bark of trees
     2: the trees and other plants in a large densely wooded area
        [syn: {forest}, {woods}]
     3: United States film actress (1938-1981) [syn: {Natalie Wood}]
     4: English conductor (1869-1944) [syn: {Sir Henry Wood}, {Sir
        Henry Joseph Wood}]
     5: English writer of novels about murders and thefts and
        forgeries (1814-1887) [syn: {Mrs. Henry Wood}, {Ellen
        Price Wood}]
     6: United States painter noted for works based on life in the
        Midwest (1892-1942) [syn: {Grant Wood}]
     7: any wind instrument other than the brass instruments [syn: {woodwind},
         {woodwind ...
