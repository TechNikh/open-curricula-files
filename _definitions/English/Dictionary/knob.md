---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knob
offline_file: ""
offline_thumbnail: ""
uuid: e9edb6be-2e03-4c4b-8b4a-997a31e64a6c
updated: 1484310202
title: knob
categories:
    - Dictionary
---
knob
     n 1: a circular rounded projection or protuberance [syn: {boss}]
     2: a round handle
     3: any thickened enlargement [syn: {node}, {thickening}]
     4: an ornament in the shape of a ball on the hilt of a sword or
        dagger [syn: {pommel}]
     [also: {knobbing}, {knobbed}]
