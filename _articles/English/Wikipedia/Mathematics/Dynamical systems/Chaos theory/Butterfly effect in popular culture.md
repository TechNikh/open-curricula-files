---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Butterfly_effect_in_popular_culture
offline_file: ""
offline_thumbnail: ""
uuid: d3572791-ef66-4952-a14e-c43065c5d1d2
updated: 1484308702
title: Butterfly effect in popular culture
tags:
    - Examples
categories:
    - Chaos theory
---
The butterfly effect is the phenomenon in chaos theory whereby a minor change in circumstances can cause a large change in outcome. The butterfly metaphor was created by Edward Norton Lorenz to emphasize the inherent unpredictable results of small changes in the initial conditions of certain physical systems. The concept was taken up by popular culture, and interpreted to mean that each event could be explained by some small cause, or that small events have a rippling effect that causes much larger events to take place.[1]
