---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/denial
offline_file: ""
offline_thumbnail: ""
uuid: d9dec33e-8e11-414b-91c7-d02e2edf050c
updated: 1484310176
title: denial
categories:
    - Dictionary
---
denial
     n 1: the act of refusing to comply (as with a request); "it
          resulted in a complete denial of his privileges"
     2: the act of asserting that something alleged is not true
     3: (psychiatry) a defense mechanism that denies painful
        thoughts
     4: renunciation of your own interests in favor of the interests
        of others [syn: {abnegation}, {self-abnegation}, {self-denial},
         {self-renunciation}]
     5: a defendant's answer or plea denying the truth of the
        charges against him; "he gave evidence for the defense"
        [syn: {defense}, {defence}, {demurrer}] [ant: {prosecution}]
