---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Michel_Deza
offline_file: ""
offline_thumbnail: ""
uuid: 9816f17e-a534-41a7-a3a9-9d0b35d350c7
updated: 1484309463
title: Michel Deza
tags:
    - Selected papers
    - Books
    - Poetry in Russian
categories:
    - Graph theorists
---
Michel Marie Deza (born 27 April 1939[1] in Moscow) is a Soviet and French mathematician, specializing in combinatorics, discrete geometry and graph theory. He is a retired director of research at the French National Centre for Scientific Research (CNRS), the vice president of the European Academy of Sciences,[2] a research professor at the Japan Advanced Institute of Science and Technology,[3] and one of the three founding editors-in-chief of the European Journal of Combinatorics.[1]
