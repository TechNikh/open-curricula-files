---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broaden
offline_file: ""
offline_thumbnail: ""
uuid: 9e4241b5-2930-4e27-b966-646c79cc63bd
updated: 1484310539
title: broaden
categories:
    - Dictionary
---
broaden
     v 1: make broader; "broaden the road"
     2: extend in scope or range or area; "The law was extended to
        all citizens"; "widen the range of applications"; "broaden
        your horizon"; "Extend your backyard" [syn: {widen}, {extend}]
     3: vary in order to spread risk or to expand; "The company
        diversified" [syn: {diversify}, {branch out}] [ant: {specialize},
         {specialize}]
     4: become broader; "The road broadened"
