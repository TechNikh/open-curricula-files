---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheaply
offline_file: ""
offline_thumbnail: ""
uuid: c8e9bb2c-bf08-4db1-b7c8-3f0cc24239d5
updated: 1484310529
title: cheaply
categories:
    - Dictionary
---
cheaply
     adv 1: in a stingy manner; "their rich uncle treated them rather
            chintzily" [syn: {stingily}, {chintzily}]
     2: in a cheap manner; "a cheaply dressed woman approached him
        in the bar" [syn: {tattily}, {inexpensively}] [ant: {expensively}]
     3: with little expenditure of money; "I bought this car very
        cheaply" [syn: {inexpensively}]
