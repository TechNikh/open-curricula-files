---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enthalpy
offline_file: ""
offline_thumbnail: ""
uuid: ce5abc09-34e6-48ff-ae3b-396dc69436f3
updated: 1484310399
title: enthalpy
categories:
    - Dictionary
---
enthalpy
     n : (thermodynamics) a thermodynamic quantity equal to the
         internal energy of a system plus the product of its
         volume and pressure; "enthalpy is the amount of energy in
         a system capable of doing mechanical work" [syn: {heat
         content}, {total heat}, {H}]
