---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mountain_Wave_Project
offline_file: ""
offline_thumbnail: ""
uuid: 479c7ad3-92d4-4e43-a07b-cade17316704
updated: 1484308893
title: Mountain Wave Project
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/MWP_Logo.jpg
tags:
    - Corporate history
    - Motivation
    - Airborne measurements
    - Record flights
    - Programming objectives
    - Expeditions
    - Project results
    - Awards
    - GEO-TV features
categories:
    - Waves
---
The Mountain Wave Project (MWP) pursues global scientific research of gravity waves and associated turbulence. MWP seeks to develop new scientific insights and knowledge through high altitude and record seeking glider flights with the goal of increasing overall flight safety and improving pilot training.
