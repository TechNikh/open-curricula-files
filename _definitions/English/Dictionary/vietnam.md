---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vietnam
offline_file: ""
offline_thumbnail: ""
uuid: a871ea32-09ad-4ad1-a255-6c5d04d2df94
updated: 1484310567
title: vietnam
categories:
    - Dictionary
---
Viet Nam
     n : a communist state in Indochina on the South China Sea;
         achieved independence from France in 1945 [syn: {Vietnam},
          {Socialist Republic of Vietnam}, {Annam}]
