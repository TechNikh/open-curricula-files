---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barium
offline_file: ""
offline_thumbnail: ""
uuid: f9c2ec59-a98b-4ba7-afca-036c281f96ac
updated: 1484310371
title: barium
categories:
    - Dictionary
---
barium
     n : a soft silvery metallic element of the alkali earth group;
         found in barite [syn: {Ba}, {atomic number 56}]
