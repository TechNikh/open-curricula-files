---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bitboard
offline_file: ""
offline_thumbnail: ""
uuid: 6ea26172-68b4-4fe8-8170-a2e073239b99
updated: 1484309004
title: Bitboard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/242px-SCD_algebraic_notation.svg.png
tags:
    - Short description
    - History
    - Description for all games or applications
    - General technical advantages and disadvantages
    - Processor use
    - Pros
    - Cons
    - Memory use
    - Pros
    - Cons
    - Chess bitboards
    - Standard
    - Rotated
    - Magics
    - Other bitboards
    - Calculators
    - Checkers
    - Chess
    - Articles
    - Code examples
    - Implementations
    - Open source
    - Closed source
    - Othello
    - Word Games
categories:
    - Game artificial intelligence
---
A bitboard, often used for boardgames such as chess, checkers, othello and word games, is a specialization of the bit array data structure, where each bit represents a game position or state, designed for optimization of speed and/or memory or disk use in mass calculations. Bits in the same bitboard relate to each other in the rules of the game, often forming a game position when taken together. Other bitboards are commonly used as masks to transform or answer queries about positions. The "game" may be any game-like system where information is tightly packed in a structured form with "rules" affecting how the individual units or pieces relate.
