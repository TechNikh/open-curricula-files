---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Inter-universal_Teichm%C3%BCller_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 3783d10f-8b5e-47d2-8ddb-f28f13e65cc4
updated: 1484308654
title: Inter-universal Teichmüller theory
categories:
    - Number theory
---
In mathematics, inter-universal Teichmüller theory (IUT) is an arithmetic version of Teichmüller theory for number fields with an elliptic curve, introduced by Shinichi Mochizuki (2012a, 2012b, 2012c, 2012d).[1][2][3][4][5]
