---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partnership_game
offline_file: ""
offline_thumbnail: ""
uuid: a071a410-b21c-472c-9f61-59ab15545af2
updated: 1484309038
title: Partnership game
categories:
    - Game theory game classes
---
In game theory, a partnership game is a symmetric game where both players receive identical payoffs for any strategy set. That is, the payoff for playing strategy a against strategy b receives the same payoff as playing strategy b against strategy a.
