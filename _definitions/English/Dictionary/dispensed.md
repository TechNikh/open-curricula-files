---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispensed
offline_file: ""
offline_thumbnail: ""
uuid: 234283bd-3503-42b0-95e3-413693575122
updated: 1484310601
title: dispensed
categories:
    - Dictionary
---
dispensed
     adj : distributed or weighted out in carefully determined
           portions; "medicines dispensed to the sick"
