---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Watts_and_Strogatz_model
offline_file: ""
offline_thumbnail: ""
uuid: 82a1b778-dac0-4d60-bb4c-1514a8354795
updated: 1484309452
title: Watts and Strogatz model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Watts-Strogatz_small-world_model_100nodes.png
tags:
    - Rationale for the model
    - Algorithm
    - Properties
    - Average path length
    - Clustering coefficient
    - Degree distribution
    - Limitations
categories:
    - Random graphs
---
The Watts–Strogatz model is a random graph generation model that produces graphs with small-world properties, including short average path lengths and high clustering. It was proposed by Duncan J. Watts and Steven Strogatz in their joint 1998 Nature paper.[1] The model also became known as the (Watts) beta model after Watts used 
  
    
      
        β
      
    
    {\displaystyle \beta }
  
 to formulate it in his popular science book Six Degrees.
