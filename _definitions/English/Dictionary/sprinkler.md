---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sprinkler
offline_file: ""
offline_thumbnail: ""
uuid: 67ebc63f-512e-4486-9fa4-f8c79cb668b0
updated: 1484310252
title: sprinkler
categories:
    - Dictionary
---
sprinkler
     n : mechanical device that attaches to a garden hose for
         watering lawn or garden
