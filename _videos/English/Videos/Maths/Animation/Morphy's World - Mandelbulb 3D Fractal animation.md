---
version: 1
type: video
provider: YouTube
id: tsuYZg8k-Zc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5ed4895b-5f31-493f-8f04-a70e399a78e6
updated: 1486069645
title: "Morphy's World - Mandelbulb 3D Fractal animation"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/tsuYZg8k-Zc/default.jpg
    - https://i3.ytimg.com/vi/tsuYZg8k-Zc/1.jpg
    - https://i3.ytimg.com/vi/tsuYZg8k-Zc/2.jpg
    - https://i3.ytimg.com/vi/tsuYZg8k-Zc/3.jpg
---
