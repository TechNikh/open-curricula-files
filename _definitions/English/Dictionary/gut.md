---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gut
offline_file: ""
offline_thumbnail: ""
uuid: 47598220-cb8b-4216-a4c4-26b90bc1ac5a
updated: 1484310361
title: gut
categories:
    - Dictionary
---
gut
     n 1: the part of the alimentary canal between the stomach and the
          anus [syn: {intestine}, {bowel}]
     2: a strong cord made from the intestines of sheep and used in
        surgery [syn: {catgut}]
     v 1: empty completely; destroy the inside of; "Gut the building"
     2: remove the guts of; "gut the sheep"
