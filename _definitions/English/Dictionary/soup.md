---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soup
offline_file: ""
offline_thumbnail: ""
uuid: cca0375e-c2b8-47d5-acb9-12e2ecba03a3
updated: 1484310330
title: soup
categories:
    - Dictionary
---
soup
     n 1: liquid food especially of meat or fish or vegetable stock
          often containing pieces of solid food
     2: any composition having a consistency suggestive of soup
     3: an unfortunate situation; "we're in the soup now"
     v : dope (a racehorse)
