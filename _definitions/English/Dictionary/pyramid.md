---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyramid
offline_file: ""
offline_thumbnail: ""
uuid: fb549292-0d76-4e4c-bd23-fa9084413fd2
updated: 1484310271
title: pyramid
categories:
    - Dictionary
---
pyramid
     n 1: a polyhedron having a polygonal base and triangular sides
          with a common vertex
     2: (stock market) a series of transactions in which the
        speculator increases his holdings by using the rising
        market value of those holdings as margin for further
        purchases
     3: a massive memorial with a square base and four triangular
        sides; built as royal tombs in ancient Egypt [syn: {Great
        Pyramid}]
     v 1: enlarge one's holdings on an exchange on a continued rise by
          using paper profits as margin to buy additional amounts
     2: use or deal in (as of stock or commercial transaction) in a
        pyramid deal
     3: ...
