---
version: 1
type: video
provider: YouTube
id: eQf_EAYGo-k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Stoichiometry%20example%20problem%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31479ee4-3fba-4d89-b68b-544ef97f13ee
updated: 1486069587
title: Stoichiometry example problem 2
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/eQf_EAYGo-k/default.jpg
    - https://i3.ytimg.com/vi/eQf_EAYGo-k/1.jpg
    - https://i3.ytimg.com/vi/eQf_EAYGo-k/2.jpg
    - https://i3.ytimg.com/vi/eQf_EAYGo-k/3.jpg
---
Calculating mass of oxygen needed and grams of product formed from the combustion of 25.0 g of glucose. Created by Sal Khan.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/stoichiometry-ideal/e/ideal_stoichiometry?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/v/stoichiometry-limiting-reagent?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/stoichiometry-ideal/v/stoichiometry-example-problem-1?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
