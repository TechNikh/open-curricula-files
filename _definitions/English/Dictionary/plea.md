---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plea
offline_file: ""
offline_thumbnail: ""
uuid: 3f0bc025-863b-45a7-9783-851ed32888a1
updated: 1484310176
title: plea
categories:
    - Dictionary
---
plea
     n 1: a humble request for help from someone in authority [syn: {supplication}]
     2: (law) a defendant's answer by a factual matter (as
        distinguished from a demurrer)
     3: an answer indicating why a suit should be dismissed
