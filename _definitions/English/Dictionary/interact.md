---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interact
offline_file: ""
offline_thumbnail: ""
uuid: c40ffb93-93a9-40bd-a5e5-e61e5424f3fd
updated: 1484310275
title: interact
categories:
    - Dictionary
---
interact
     v : act together or towards others or with others; "He should
         interact more with his colleagues"
