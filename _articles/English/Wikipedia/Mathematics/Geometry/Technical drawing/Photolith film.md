---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Photolith_film
offline_file: ""
offline_thumbnail: ""
uuid: 07671538-4f08-4ad4-a662-b27ac338d889
updated: 1484309327
title: Photolith film
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Navrh_plosny_spoj_vyroba.png
categories:
    - Technical drawing
---
A photolith film[1] is a transparent film, made with some sort of tranparent plastic (formerly made of acetate). Nowadays, with the use of laser printers and computers, the photolith film can be based on polyester, vegetable paper or laser film paper. It is mainly used in all photolithography processes.
