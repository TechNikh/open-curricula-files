---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instantaneous
offline_file: ""
offline_thumbnail: ""
uuid: 3a41add1-ef5a-4455-9bb6-c084149ee968
updated: 1484310407
title: instantaneous
categories:
    - Dictionary
---
instantaneous
     adj : occurring with no delay; "relief was instantaneous";
           "instant gratification" [syn: {instant(a)}]
