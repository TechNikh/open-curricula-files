---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conglomerate_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 625f8fda-cd9e-4605-a36b-f80b304aea05
updated: 1484309501
title: Conglomerate (set theory)
categories:
    - Set theory
---
In mathematics, a conglomerate is a definable collection of classes, just as a class is a definable collection of sets.[1] A quasi-category is like a category except that its objects and morphisms form conglomerates instead of classes.[1] The subclasses of any class, and in particular, the collection of all classes (every class is a subclass of the class of all sets), form a conglomerate.
