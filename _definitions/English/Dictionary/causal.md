---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/causal
offline_file: ""
offline_thumbnail: ""
uuid: d3fcc0ea-a642-47c5-a350-3b70e9232e2e
updated: 1484310471
title: causal
categories:
    - Dictionary
---
causal
     adj : involving or constituting a cause; causing; "a causal
           relationship between scarcity and higher prices"
