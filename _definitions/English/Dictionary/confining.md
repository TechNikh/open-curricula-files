---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confining
offline_file: ""
offline_thumbnail: ""
uuid: 20d0941e-f301-4ce5-81fd-11305736b086
updated: 1484310577
title: confining
categories:
    - Dictionary
---
confining
     adj 1: restricting the scope or freedom of action [syn: {constraining},
             {constrictive}, {limiting}, {restricting}]
     2: crowded; "close quarters" [syn: {close}]
