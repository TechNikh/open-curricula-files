---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joule
offline_file: ""
offline_thumbnail: ""
uuid: 47f3603e-61ed-4c5e-8c80-d567e4dbc3b8
updated: 1484310232
title: joule
categories:
    - Dictionary
---
joule
     n 1: a unit of electrical energy equal to the work done when a
          current of one ampere passes through a resistance of one
          ohm for one second [syn: {J}, {watt second}]
     2: English physicist who established the mechanical theory of
        heat and discovered the first law of thermodynamics
        (1818-1889) [syn: {James Prescott Joule}]
