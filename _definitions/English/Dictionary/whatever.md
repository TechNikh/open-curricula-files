---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whatever
offline_file: ""
offline_thumbnail: ""
uuid: 57fb5d4e-6c77-4076-8bfd-9aa9ef5eb992
updated: 1484310289
title: whatever
categories:
    - Dictionary
---
whatever
     adj : one or some or every or all without specification; "give me
           any peaches you don't want"; "not any milk is left";
           "any child would know that"; "pick any card"; "any day
           now"; "cars can be rented at almost any airport"; "at
           twilight or any other time"; "beyond any doubt"; "need
           any help we can get"; "give me whatever peaches you
           don't want"; "no milk whatsoever is left" [syn: {any(a)},
            {whatsoever}]
