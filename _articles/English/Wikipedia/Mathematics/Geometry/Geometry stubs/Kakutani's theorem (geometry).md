---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kakutani%27s_theorem_(geometry)'
offline_file: ""
offline_thumbnail: ""
uuid: 7aec4ec8-5015-4740-bd4a-c04b2f541ea6
updated: 1484309363
title: "Kakutani's theorem (geometry)"
categories:
    - Geometry stubs
---
Kakutani's theorem is a result in geometry named after Shizuo Kakutani. It states that every convex body in 3-dimensional space has a circumscribed cube, i.e. a cube all of whose faces touch the body. The result was further generalized by Yamabe and Yujobô to higher dimensions, and by Floyd to other circumscribed parallelepipeds.
