---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-living
offline_file: ""
offline_thumbnail: ""
uuid: b9ddbdb8-cef6-47dd-ab7d-8b40432186f9
updated: 1484310416
title: non-living
categories:
    - Dictionary
---
nonliving
     adj : not endowed with life; "the inorganic world is inanimate";
           "inanimate objects"; "dead stones" [syn: {inanimate}, {dead}]
           [ant: {animate}]
