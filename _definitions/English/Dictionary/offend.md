---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offend
offline_file: ""
offline_thumbnail: ""
uuid: 0d3d58c0-0375-45f0-b099-3aa346ca4fae
updated: 1484310609
title: offend
categories:
    - Dictionary
---
offend
     v 1: cause to feel resentment or indignation; "Her tactless
          remark offended me" [syn: {pique}]
     2: act in disregard of laws and rules; "offend all laws of
        humanity"; "violate the basic laws or human civilization";
        "break a law" [syn: {transgress}, {infract}, {violate}, {go
        against}, {breach}, {break}]
     3: strike with disgust or revulsion; "The scandalous behavior
        of this married woman shocked her friends" [syn: {shock},
        {scandalize}, {scandalise}, {appal}, {appall}, {outrage}]
     4: hurt the feelings of; "She hurt me when she did not include
        me among her guests"; "This remark really bruised me ego"
        [syn: ...
