---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poverty
offline_file: ""
offline_thumbnail: ""
uuid: b2650fd6-b5b6-4108-bf1f-10156dd42b90
updated: 1484310479
title: poverty
categories:
    - Dictionary
---
poverty
     n : the state of having little or no money and few or no
         material possessions [syn: {poorness}, {impoverishment}]
         [ant: {wealth}]
