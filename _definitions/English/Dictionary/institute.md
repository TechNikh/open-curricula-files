---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/institute
offline_file: ""
offline_thumbnail: ""
uuid: cfb1262a-ce91-42a4-b423-09cfda1854f6
updated: 1484310254
title: institute
categories:
    - Dictionary
---
institute
     n : an association organized to promote art or science or
         education
     v 1: set up or lay the groundwork for; "establish a new
          department" [syn: {establish}, {found}, {plant}, {constitute}]
     2: avance or set forth in court; "bring charges", "institute
        proceedings" [syn: {bring}]
