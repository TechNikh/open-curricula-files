---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kerosene
offline_file: ""
offline_thumbnail: ""
uuid: 0b359fd9-c62a-43bb-874f-c1b3b5dd1692
updated: 1484310232
title: kerosene
categories:
    - Dictionary
---
kerosene
     n : a flammable hydrocarbon oil used as fuel in lamps and
         heaters [syn: {kerosine}, {lamp oil}, {coal oil}]
