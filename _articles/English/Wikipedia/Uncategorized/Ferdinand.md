---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ferdinand
offline_file: ""
offline_thumbnail: ""
uuid: a299c307-e6ba-4195-832f-e401aa33c889
updated: 1484308473
title: Ferdinand
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-FerdinandCatholic.jpg
tags:
    - Aristocracy
    - Aragón/León/Castile/Spain
    - Portugal
    - Austria and German states
    - Italian states
    - Naples, Sicily and the Two Sicilies
    - Mantua and Montferrat
    - Parma
    - Tuscany
    - Bulgaria
categories:
    - Uncategorized
---
Ferdinand is a Germanic name composed of the elements frith "protection" , frið "peace" (PIE pri to love, to make peace) or alternatively farð "journey, travel", Proto-Germanic *farthi, abstract noun from root *far- "to fare, travel" (PIE par "to lead, pass over"), and nanth "courage" or nand "ready, prepared" related to Old Spanish, Old High German nendan "to risk, venture."
