---
version: 1
type: video
provider: YouTube
id: TfYCnOvNnFU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b85d8a53-9ca0-4939-b599-bae2adba5cbb
updated: 1486069682
title: 'Traveling Waves- Crash Course Physics #17'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/TfYCnOvNnFU/default.jpg
    - https://i3.ytimg.com/vi/TfYCnOvNnFU/1.jpg
    - https://i3.ytimg.com/vi/TfYCnOvNnFU/2.jpg
    - https://i3.ytimg.com/vi/TfYCnOvNnFU/3.jpg
---
