---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miser
offline_file: ""
offline_thumbnail: ""
uuid: 90584204-44a8-40f9-83c4-88485a055be0
updated: 1484310146
title: miser
categories:
    - Dictionary
---
miser
     n : a stingy hoarder of money and possessions (often living
         miserably)
