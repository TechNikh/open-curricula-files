---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zyablov_bound
offline_file: ""
offline_thumbnail: ""
uuid: d688b31c-c964-48c0-80e9-7178f63d37d4
updated: 1484308567
title: Zyablov bound
tags:
    - Statement of the bound
    - Description
    - Remarks
    - References and External Links
categories:
    - Coding theory
---
In coding theory, the Zyablov bound is a lower bound on the rate 
  
    
      
        R
      
    
    {\displaystyle R}
  
 and relative distance 
  
    
      
        δ
      
    
    {\displaystyle \delta }
  
 of concatenated codes.
