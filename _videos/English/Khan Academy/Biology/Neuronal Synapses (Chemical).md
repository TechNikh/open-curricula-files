---
version: 1
type: video
provider: YouTube
id: Tbq-KZaXiL4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Neuronal%20Synapses%20%28Chemical%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e50b41b1-94b4-4aa9-b677-d96a83816443
updated: 1486069581
title: Neuronal Synapses (Chemical)
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tbq-KZaXiL4/default.jpg
    - https://i3.ytimg.com/vi/Tbq-KZaXiL4/1.jpg
    - https://i3.ytimg.com/vi/Tbq-KZaXiL4/2.jpg
    - https://i3.ytimg.com/vi/Tbq-KZaXiL4/3.jpg
---
How one neuron can stimulate (or inhibit) another neuron at a chemical synapse
More free lessons at: http://www.khanacademy.org/video?v=Tbq-KZaXiL4
