---
version: 1
type: video
provider: YouTube
id: usAqJoVYVSc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20Do%20We%20Cough-%20-%20The%20Mechanism%20of%20Coughing%20-%20Cough%20Reflex%20Animation%20-%20Learn%20Human%20Body.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aa5e317b-1cbb-4db5-ac4a-ec2eead84b59
updated: 1486069666
title: 'How Do We Cough? - The Mechanism of Coughing - Cough Reflex Animation - Learn Human Body'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/usAqJoVYVSc/default.jpg
    - https://i3.ytimg.com/vi/usAqJoVYVSc/1.jpg
    - https://i3.ytimg.com/vi/usAqJoVYVSc/2.jpg
    - https://i3.ytimg.com/vi/usAqJoVYVSc/3.jpg
---
