---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helix
offline_file: ""
offline_thumbnail: ""
uuid: 29a6b092-302d-45cb-9b07-7ac96a9677ee
updated: 1484310295
title: helix
categories:
    - Dictionary
---
helix
     n 1: a curve that lies on the surface of a cylinder or cone and
          cuts the element at a constant angle [syn: {spiral}]
     2: a structure consisting of something wound in a continuous
        series of loops; "a coil of rope" [syn: {coil}, {spiral},
        {volute}, {whorl}]
     3: type genus of the family Helicidae [syn: {genus Helix}]
     [also: {helices} (pl)]
