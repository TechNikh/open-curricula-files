---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humorous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484444101
title: humorous
categories:
    - Dictionary
---
humorous
     adj : full of or characterized by humor; "humorous stories";
           "humorous cartoons"; "in a humorous vein" [syn: {humourous}]
           [ant: {humorless}]
