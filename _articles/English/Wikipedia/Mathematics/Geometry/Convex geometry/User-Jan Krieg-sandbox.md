---
version: 1
type: article
id: https://en.wikipedia.org/wiki/User:Jan_Krieg/sandbox
offline_file: ""
offline_thumbnail: ""
uuid: 61c7af64-61e5-440a-91db-aca7af314093
updated: 1484309193
title: User:Jan Krieg/sandbox
tags:
    - Definition
    - Properties
    - Quermassintegrals
    - Intrinsic volumes
    - "Hadwiger's characterization theorem"
    - Notes
categories:
    - Convex geometry
---
In mathematics, more specifically, in convex geometry, the mixed volume is a way to associate a non-negative number to an 
  
    
      
        n
      
    
    {\displaystyle n}
  
-tuple of convex bodies in the 
  
    
      
        n
      
    
    {\displaystyle n}
  
-dimensional space. This number depends on the size of the bodies and on their relative orientation to each other.
