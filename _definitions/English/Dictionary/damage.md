---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/damage
offline_file: ""
offline_thumbnail: ""
uuid: f46fb51b-621c-497d-82ad-cb4432508145
updated: 1484310333
title: damage
categories:
    - Dictionary
---
damage
     n 1: the occurrence of a change for the worse [syn: {harm}, {impairment}]
     2: loss of military equipment [syn: {equipment casualty}]
     3: the act of damaging something or someone [syn: {harm}, {hurt},
         {scathe}]
     4: the amount of money needed to purchase something; "the price
        of gasoline"; "he got his new car on excellent terms";
        "how much is the damage?" [syn: {price}, {terms}]
     5: a legal injury is any damage resulting from a violation of a
        legal right [syn: {wrong}, {legal injury}]
     v : inflict damage upon; "The snow damaged the roof"; "She
         damaged the car when she hit the tree"
