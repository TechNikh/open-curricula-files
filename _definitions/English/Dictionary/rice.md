---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rice
offline_file: ""
offline_thumbnail: ""
uuid: 638d362a-14cd-430c-81b4-6cfea58f1d6a
updated: 1484310214
title: rice
categories:
    - Dictionary
---
rice
     n 1: grains used as food either unpolished or more often polished
     2: annual or perennial rhizomatous marsh grasses; seed used for
        food; straw used for paper
     3: English lyricist who frequently worked with Andrew Lloyd
        Webber (born in 1944) [syn: {Sir Tim Rice}, {Timothy Miles
        Bindon Rice}]
     4: United States playwright (1892-1967) [syn: {Elmer Rice}, {Elmer
        Leopold Rice}, {Elmer Reizenstein}]
     v : sieve so that it becomes the consistency of rice; "rice the
         potatoes"
