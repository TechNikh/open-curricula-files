---
version: 1
type: video
provider: YouTube
id: 26E0jPNSqwY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d07e7dc7-b7f7-45eb-b2d8-64bee6d597d2
updated: 1486069635
title: Refractive Index of Prism Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/26E0jPNSqwY/default.jpg
    - https://i3.ytimg.com/vi/26E0jPNSqwY/1.jpg
    - https://i3.ytimg.com/vi/26E0jPNSqwY/2.jpg
    - https://i3.ytimg.com/vi/26E0jPNSqwY/3.jpg
---
