---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beetroot
offline_file: ""
offline_thumbnail: ""
uuid: be42e363-5b02-43d6-b8d3-df3fda409cc3
updated: 1484310387
title: beetroot
categories:
    - Dictionary
---
beetroot
     n 1: beet having a massively swollen red root; widely grown for
          human consumption [syn: {Beta vulgaris rubra}]
     2: round red root vegetable [syn: {beet}]
