---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaped
offline_file: ""
offline_thumbnail: ""
uuid: ac736100-8f78-4fe9-9864-f53625c43ebe
updated: 1484310369
title: shaped
categories:
    - Dictionary
---
shaped
     adj 1: shaped to fit by or as if by altering the contours of a
            pliable mass (as by work or effort); "a shaped
            handgrip"; "the molded steel plates"; "the wrought
            silver bracelet" [syn: {molded}, {wrought}]
     2: having the shape of; "a square shaped playing field"
