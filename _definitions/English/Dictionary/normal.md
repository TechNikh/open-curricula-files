---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/normal
offline_file: ""
offline_thumbnail: ""
uuid: e700b8a5-b38d-4c6e-a9cd-83c81352bb7e
updated: 1484310293
title: normal
categories:
    - Dictionary
---
normal
     adj 1: conforming with or constituting a norm or standard or level
            or type or social norm; not abnormal; "serve wine at
            normal room temperature"; "normal diplomatic
            relations"; "normal working hours"; "normal word
            order"; "normal curiosity"; "the normal course of
            events" [ant: {abnormal}]
     2: in accordance with scientific laws [ant: {paranormal}]
     3: being approximately average or within certain limits in e.g.
        intelligence and development; "a perfectly normal child";
        "of normal intelligence"; "the most normal person I've
        ever met" [ant: {abnormal}]
     4: forming a right angle
     n : ...
