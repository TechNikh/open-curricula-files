---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separately
offline_file: ""
offline_thumbnail: ""
uuid: 25ae2bab-6e9f-4888-98f4-032245780a25
updated: 1484310228
title: separately
categories:
    - Dictionary
---
separately
     adv : apart from others; "taken individually, the rooms were, in
           fact, square"; "the fine points are treated singly"
           [syn: {individually}, {singly}, {severally}, {one by
           one}, {on an individual basis}]
