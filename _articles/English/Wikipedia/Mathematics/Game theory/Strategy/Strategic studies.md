---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_studies
offline_file: ""
offline_thumbnail: ""
uuid: 2beab124-eff2-4bde-ac5a-f67b47f79f69
updated: 1484309069
title: Strategic studies
categories:
    - Strategy
---
Strategic studies is an interdisciplinary academic field centered on the study of conflict and peace strategies, often devoting special attention to the relationship between international politics, geostrategy, international diplomacy, international economics, and military power. In the scope of the studies are also subjects such as the role of intelligence, diplomacy, and international cooperation for security and defense. The subject is normally taught at the post-graduate academic or professional, usually strategic-political and strategic-military levels.
