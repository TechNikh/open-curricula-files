---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pitchfork_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: 4fce93bd-3f06-4f37-85f2-ce0d0ad61335
updated: 1484308707
title: Pitchfork bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Pitchfork_bifurcation_supercritical.svg.png
tags:
    - Supercritical case
    - Subcritical case
    - Formal definition
categories:
    - Bifurcation theory
---
In bifurcation theory, a field within mathematics, a pitchfork bifurcation is a particular type of local bifurcation. Pitchfork bifurcations, like Hopf bifurcations have two types - supercritical or subcritical.
