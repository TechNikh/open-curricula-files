---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/now
offline_file: ""
offline_thumbnail: ""
uuid: 25cacf1a-4095-4f58-8a9e-d0bd1fbae9ce
updated: 1484310344
title: now
categories:
    - Dictionary
---
now
     n : the momentary present; "Now is a good time to do it"; "it
         worked up to right now"
     adv 1: at the present moment; "goods now on sale"; "the now-aging
            dictator"; "they are now abroad"; "he is busy at
            present writing a new novel"; "it could happen any
            time now" [syn: {at present}]
     2: in these times; "it is solely by their language that the
        upper classes nowadays are distinguished"- Nancy Mitford;
        "we now rarely see horse-drawn vehicles on city streets";
        "today almost every home has television" [syn: {nowadays},
         {today}]
     3: in the historical present; at this point in the narration of
        ...
