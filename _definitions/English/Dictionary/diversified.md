---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diversified
offline_file: ""
offline_thumbnail: ""
uuid: 145c03d0-5ac4-4fca-aa59-4aafdce84515
updated: 1484310319
title: diversified
categories:
    - Dictionary
---
diversified
     adj : having variety of character or form or components; or having
           increased variety; "a diversified musical program
           ranging from classical to modern"; "diversified
           farming"; "diversified manufacturing"; "diversified
           scenery"; "diversified investments" [ant: {undiversified}]
