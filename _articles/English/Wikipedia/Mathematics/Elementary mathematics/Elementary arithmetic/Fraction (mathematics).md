---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fraction_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 636a53f0-3525-40ad-8fa6-36817a840e8b
updated: 1484308912
title: Fraction (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cake_quarters.svg.png
tags:
    - vocabulary
    - Forms of fractions
    - Simple, common, or vulgar fractions
    - Proper and improper fractions
    - Mixed numbers
    - Ratios
    - Reciprocals and the "invisible denominator"
    - Complex fractions
    - Compound fractions
    - Decimal fractions and percentages
    - Special cases
    - Arithmetic with fractions
    - Equivalent fractions
    - Comparing fractions
    - Addition
    - Adding unlike quantities
    - Subtraction
    - Multiplication
    - Multiplying a fraction by another fraction
    - Multiplying a fraction by a whole number
    - Multiplying mixed numbers
    - Division
    - Converting between decimals and fractions
    - Converting repeating decimals to fractions
    - Fractions in abstract mathematics
    - Algebraic fractions
    - Radical expressions
    - Typographical variations
    - History
    - In formal education
    - Pedagogical tools
    - Documents for teachers
    - Notes
categories:
    - Elementary arithmetic
---
A fraction (from Latin fractus, "broken") represents a part of a whole or, more generally, any number of equal parts. When spoken in everyday English, a fraction describes how many parts of a certain size there are, for example, one-half, eight-fifths, three-quarters. A common, vulgar, or simple fraction (examples: 
  
    
      
        
          
            
              1
              2
            
          
        
      
    
    {\displaystyle {\tfrac {1}{2}}}
  
 and 17/3) consists of an integer numerator displayed above a line (or before a slash), and a non-zero integer denominator, displayed below (or after) that line. Numerators and denominators are also used in fractions ...
