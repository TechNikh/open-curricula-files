---
version: 1
type: video
provider: YouTube
id: PAtBiSFvSRw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 46170e33-2d4f-40bf-8ae5-6e60399eb827
updated: 1486069649
title: 'BluScenes- The Fractal Plane by Christopher Ursitti'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/PAtBiSFvSRw/default.jpg
    - https://i3.ytimg.com/vi/PAtBiSFvSRw/1.jpg
    - https://i3.ytimg.com/vi/PAtBiSFvSRw/2.jpg
    - https://i3.ytimg.com/vi/PAtBiSFvSRw/3.jpg
---
