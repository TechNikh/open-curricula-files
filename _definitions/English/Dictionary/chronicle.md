---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chronicle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484592781
title: chronicle
categories:
    - Dictionary
---
chronicle
     n : a record or narrative description of past events; "a history
         of France"; "he gave an inaccurate account of the plot to
         kill the president"; "the story of exposure to lead"
         [syn: {history}, {account}, {story}]
     v : record in chronological order; make a historical record
