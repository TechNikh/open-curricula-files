---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outskirts
offline_file: ""
offline_thumbnail: ""
uuid: 26de76f8-f24b-4b83-808f-84177d71af39
updated: 1484310480
title: outskirts
categories:
    - Dictionary
---
outskirts
     n : outlying areas (as of a city or town); "they lived on the
         outskirts of Houston"; "they mingled in the outskirts of
         the crowd"
