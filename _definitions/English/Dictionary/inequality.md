---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inequality
offline_file: ""
offline_thumbnail: ""
uuid: 49b2d796-4f6f-4459-9c3a-58f8bc2f65cc
updated: 1484310533
title: inequality
categories:
    - Dictionary
---
inequality
     n : lack of equality; "the growing inequality between rich and
         poor" [ant: {equality}]
