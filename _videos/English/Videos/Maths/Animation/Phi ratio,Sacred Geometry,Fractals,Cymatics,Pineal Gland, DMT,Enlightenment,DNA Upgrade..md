---
version: 1
type: video
provider: YouTube
id: bAItJadasiY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5af6bbb5-2989-47f7-9df0-e8fce9c38cb8
updated: 1486069641
title: >
    Phi ratio,Sacred Geometry,Fractals,Cymatics,Pineal Gland,
    DMT,Enlightenment,DNA Upgrade.
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bAItJadasiY/default.jpg
    - https://i3.ytimg.com/vi/bAItJadasiY/1.jpg
    - https://i3.ytimg.com/vi/bAItJadasiY/2.jpg
    - https://i3.ytimg.com/vi/bAItJadasiY/3.jpg
---
