---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interface
offline_file: ""
offline_thumbnail: ""
uuid: 2f1afe24-3742-43d8-b7b7-b4126336712d
updated: 1484310214
title: interface
categories:
    - Dictionary
---
interface
     n 1: (chemistry) a surface forming a common boundary between two
          things (two objects or liquids or chemical phases)
     2: (computer science) a program that controls a display for the
        user (usually on a computer monitor) and that allows the
        user to interact with the system [syn: {user interface}]
     3: the overlap where two theories or phenomena affect each
        other or have links with each other; "the interface
        between chemistry and biology"
     4: (computer science) computer circuit consisting of the
        hardware and associated circuitry that links one device
        with another (especially a computer and a hard disk drive
     ...
