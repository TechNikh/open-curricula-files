---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clearly
offline_file: ""
offline_thumbnail: ""
uuid: 3dc41fae-f1d2-4de1-93d8-f8d89089adaf
updated: 1484310303
title: clearly
categories:
    - Dictionary
---
clearly
     adv 1: without doubt or question; "they were clearly lost";
            "history has clearly shown the folly of that policy"
     2: in an intelligible manner; "the foreigner spoke to us quite
        intelligibly" [syn: {intelligibly}, {understandably}]
        [ant: {unintelligibly}]
     3: clear to the mind; with distinct mental discernment; "it's
        distinctly possible"; "I could clearly see myself in his
        situation" [syn: {distinctly}]
     4: in an easily perceptible manner; "could be seen clearly
        under the microscope"; "She cried loud and clear" [syn: {clear}]
