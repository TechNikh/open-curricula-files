---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remote
offline_file: ""
offline_thumbnail: ""
uuid: 6b14b4fc-77cf-44ff-94d0-a8b222294ba3
updated: 1484310413
title: remote
categories:
    - Dictionary
---
remote
     adj 1: far distant in space; "distant lands"; "remote stars"; "a
            remote outpost of civilization"; "a hideaway far
            removed from towns and cities" [syn: {distant}, {removed}]
     2: very unlikely; "an outside chance"; "a remote possibility";
        "a remote contingency" [syn: {outside}]
     3: far distant in time; "distant events"; "the remote past or
        future"; "a civilization ten centuries removed from modern
        times" [syn: {distant}, {removed}]
     4: inaccessible and sparsely populated [syn: {backwoods(a)}, {outback(a)}]
     5: far apart in nature; "considerations entirely removed (or
        remote) from politics" [syn: {removed(p)}]
 ...
