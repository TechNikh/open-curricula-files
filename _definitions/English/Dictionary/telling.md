---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telling
offline_file: ""
offline_thumbnail: ""
uuid: 45312277-97d5-409c-b2c0-66fdb1e89538
updated: 1484310531
title: telling
categories:
    - Dictionary
---
telling
     adj 1: disclosing unintentionally; "a telling smile"; "a telltale
            panel of lights"; "a telltale patch of oil on the
            water marked where the boat went down" [syn: {revealing},
             {telltale(a)}]
     2: powerfully persuasive; "a cogent argument"; "a telling
        presentation"; "a weighty argument" [syn: {cogent}, {weighty}]
     3: producing a strong effect; "gave an impressive performance
        as Othello"; "a telling gesture" [syn: {impressive}]
     n 1: an act of narration; "he was the hero according to his own
          relation"; "his endless recounting of the incident
          eventually became unbearable" [syn: {relation}, ...
