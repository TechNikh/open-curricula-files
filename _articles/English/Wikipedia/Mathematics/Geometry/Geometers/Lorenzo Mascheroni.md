---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lorenzo_Mascheroni
offline_file: ""
offline_thumbnail: ""
uuid: 76f0d39b-43ae-4262-8f69-57a939006c65
updated: 1484309091
title: Lorenzo Mascheroni
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Lorenzo_Mascheroni_1.gif
categories:
    - Geometers
---
He was born near Bergamo, Lombardy. At first mainly interested in the humanities (poetry and Greek language), he eventually became professor of mathematics at Pavia.
