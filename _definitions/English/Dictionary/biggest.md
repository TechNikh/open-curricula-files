---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biggest
offline_file: ""
offline_thumbnail: ""
uuid: 805077ff-17bf-42a1-a00f-67149c4b8acc
updated: 1484310283
title: biggest
categories:
    - Dictionary
---
big
     adj 1: above average in size or number or quantity or magnitude or
            extent; "a large city"; "set out for the big city"; "a
            large sum"; "a big (or large) barn"; "a large family";
            "big businesses"; "a big expenditure"; "a large number
            of newspapers"; "a big group of scientists"; "large
            areas of the world" [syn: {large}] [ant: {small}, {small}]
     2: significant; "graduation was a big day in his life"
     3: of very great significance; "deciding to drop the atom bomb
        was a very big decision"; "a momentous event" [syn: {momentous}]
     4: conspicuous in position or importance; "a big figure in the
        movement"; ...
