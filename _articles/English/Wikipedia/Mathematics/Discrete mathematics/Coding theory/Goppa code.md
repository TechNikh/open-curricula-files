---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Goppa_code
offline_file: ""
offline_thumbnail: ""
uuid: 1d97aaf4-2323-44dd-892e-e0307c721dd9
updated: 1484308545
title: Goppa code
tags:
    - Construction
    - Function code
    - Residue code
categories:
    - Coding theory
---
In mathematics, an algebraic geometric code (AG-code), otherwise known as a Goppa code, is a general type of linear code constructed by using an algebraic curve 
  
    
      
        X
      
    
    {\displaystyle X}
  
 over a finite field 
  
    
      
        
          
            F
          
          
            q
          
        
      
    
    {\displaystyle \mathbb {F} _{q}}
  
. Such codes were introduced by Valerii Denisovich Goppa. In particular cases, they can have interesting extremal properties. They should not be confused with Binary Goppa codes that are used, for instance, in the McEliece cryptosystem.
