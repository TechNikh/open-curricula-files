---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/body
offline_file: ""
offline_thumbnail: ""
uuid: 920c5a08-50eb-425a-8ffa-5dd359f4603c
updated: 1484310363
title: body
categories:
    - Dictionary
---
body
     n 1: the entire physical structure of an organism (especially an
          animal or human being); "he felt as if his whole body
          were on fire" [syn: {organic structure}, {physical
          structure}]
     2: body of a dead animal or person; "they found the body in the
        lake" [syn: {dead body}]
     3: a group of persons associated by some common tie or
        occupation and regarded as an entity; "the whole body
        filed out of the auditorium"
     4: the body excluding the head and neck and limbs; "they moved
        their arms and legs and bodies" [syn: {torso}, {trunk}]
     5: an individual 3-dimensional object that has mass and that is
        ...
