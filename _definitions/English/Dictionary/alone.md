---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alone
offline_file: ""
offline_thumbnail: ""
uuid: 70317dd4-219f-4fba-9046-04196a9aaf65
updated: 1484310236
title: alone
categories:
    - Dictionary
---
alone
     adj 1: isolated from others; "could be alone in a crowded room";
            "was alone with her thoughts"; "I want to be alone"
            [syn: {alone(p)}]
     2: lacking companions or companionship; "he was alone when we
        met him"; "she is alone much of the time"; "the lone skier
        on the mountain"; "a lonely fisherman stood on a tuft of
        gravel"; "a lonely soul"; "a solitary traveler" [syn: {alone(p)},
         {lone(a)}, {lonely(a)}, {solitary}]
     3: exclusive of anyone or anything else; "she alone believed
        him"; "cannot live by bread alone"; "I'll have this car
        and this car only" [syn: {alone(p)}, {only}]
     4: radically ...
