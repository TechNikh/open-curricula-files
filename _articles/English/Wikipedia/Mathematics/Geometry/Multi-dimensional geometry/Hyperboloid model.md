---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperboloid_model
offline_file: ""
offline_thumbnail: ""
uuid: 7847d30a-87d4-429d-81ef-a7510102afe6
updated: 1484309283
title: Hyperboloid model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HyperboloidProjection.png
tags:
    - Minkowski quadratic form
    - Straight lines
    - Isometries
    - History
    - Notes and references
categories:
    - Multi-dimensional geometry
---
In geometry, the hyperboloid model, also known as the Minkowski model or the Lorentz model (after Hermann Minkowski and Hendrik Lorentz), is a model of n-dimensional hyperbolic geometry in which points are represented by the points on the forward sheet S+ of a two-sheeted hyperboloid in (n+1)-dimensional Minkowski space and m-planes are represented by the intersections of the (m+1)-planes in Minkowski space with S+. The hyperbolic distance function admits a simple expression in this model. The hyperboloid model of the n-dimensional hyperbolic space is closely related to the Beltrami–Klein model and to the Poincaré disk model as they are projective models in the sense that the isometry ...
