---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/congruence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484359261
title: congruence
categories:
    - Dictionary
---
congruence
     n : the quality of agreeing; being suitable and appropriate
         [syn: {congruity}, {congruousness}] [ant: {incongruity},
         {incongruity}]
