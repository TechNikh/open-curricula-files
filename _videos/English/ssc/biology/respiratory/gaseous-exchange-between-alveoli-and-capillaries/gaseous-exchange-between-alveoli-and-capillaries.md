---
title: Gaseous exchange between alveoli and capillaries
youtube_id: XTMYSGXhJ4E
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
