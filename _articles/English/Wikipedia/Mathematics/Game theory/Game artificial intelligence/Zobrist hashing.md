---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zobrist_hashing
offline_file: ""
offline_thumbnail: ""
uuid: 621d2cd9-8cb8-4a33-83ba-1b2c153318cf
updated: 1484309013
title: Zobrist hashing
tags:
    - Calculation of the hash value
    - Use of the hash value
    - Updating the hash value
    - Wider usage
categories:
    - Game artificial intelligence
---
Zobrist hashing (also referred to as Zobrist keys or Zobrist signatures [1]) is a hash function construction used in computer programs that play abstract board games, such as chess and Go, to implement transposition tables, a special kind of hash table that is indexed by a board position and used to avoid analyzing the same position more than once. Zobrist hashing is named for its inventor, Albert Lindsey Zobrist.[2] It has also been applied as a method for recognizing substitutional alloy configurations in simulations of crystalline materials.[3]
