---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longer
offline_file: ""
offline_thumbnail: ""
uuid: 93ae7151-31a3-4c36-9cef-1c3cfdcefcf2
updated: 1484310328
title: longer
categories:
    - Dictionary
---
longer
     adj : having the greater length of two or the greatest length of
           several; "the longer (or long) edge of the door"; "the
           hypotenuse is the longest (or long) side of a right
           triangle" [syn: {longest}]
     n : a person with a strong desire for something; "a longer for
         money"; "a thirster after blood"; "a yearner for
         knowledge" [syn: {thirster}, {yearner}]
     adv : for more time; "can I stay bit longer?"
