---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/settling
offline_file: ""
offline_thumbnail: ""
uuid: 63041118-69f9-44a0-9aab-f8861a3def02
updated: 1484310475
title: settling
categories:
    - Dictionary
---
settling
     n : a gradual sinking to a lower level [syn: {subsiding}, {subsidence}]
