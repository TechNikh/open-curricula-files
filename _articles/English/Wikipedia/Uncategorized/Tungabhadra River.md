---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tungabhadra
offline_file: ""
offline_thumbnail: ""
uuid: 0d01cfb2-e88a-4fac-96d8-415e39289b65
updated: 1484308435
title: Tungabhadra River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Contemplating_the_river_in_Hampi.jpg
tags:
    - Course
    - Temples
    - Dams
    - Problems
categories:
    - Uncategorized
---
The Tungabhadra River is a river in India that starts and flows through the state of Karnataka during most of its course, before flowing along the border between Karnataka and Andhra Pradesh and ultimately joining the Krishna River along the border of Andhra Pradesh and Telangana. In the epic Ramayana, the Tungabhadra River was known by the name of Pampa.
