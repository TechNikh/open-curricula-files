---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/current
offline_file: ""
offline_thumbnail: ""
uuid: 83213ade-b547-4860-bb23-996f5c8f3368
updated: 1484310244
title: current
categories:
    - Dictionary
---
current
     adj : occurring in or belonging to the present time; "current
           events"; "the current topic"; "current negotiations";
           "current psychoanalytic theories"; "the ship's current
           position" [ant: {noncurrent}]
     n 1: a flow of electricity through a conductor; "the current was
          measured in amperes" [syn: {electric current}]
     2: a steady flow (usually from natural causes); "the raft
        floated downstream on the current"; "he felt a stream of
        air" [syn: {stream}]
     3: dominant course (suggestive of running water) of successive
        events or ideas; "two streams of development run through
        American history"; "stream ...
