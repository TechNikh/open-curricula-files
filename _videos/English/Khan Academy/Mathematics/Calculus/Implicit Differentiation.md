---
version: 1
type: video
provider: YouTube
id: sL6MC-lKOrw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Implicit%20Differentiation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6b3c7314-fa7b-4890-af1f-eb79620cea62
updated: 1486069693
title: Implicit Differentiation
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sL6MC-lKOrw/default.jpg
    - https://i3.ytimg.com/vi/sL6MC-lKOrw/1.jpg
    - https://i3.ytimg.com/vi/sL6MC-lKOrw/2.jpg
    - https://i3.ytimg.com/vi/sL6MC-lKOrw/3.jpg
---
