---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Singleton_bound
offline_file: ""
offline_thumbnail: ""
uuid: 09d13ef5-a4a5-4d82-98d7-6481e7f9cf85
updated: 1484308558
title: Singleton bound
tags:
    - Statement of the bound
    - Proof
    - Linear codes
    - History
    - MDS codes
    - Arcs in projective geometry
    - Notes
categories:
    - Coding theory
---
In coding theory, the Singleton bound, named after Richard Collom Singleton, is a relatively crude upper bound on the size of an arbitrary block code 
  
    
      
        C
      
    
    {\displaystyle C}
  
 with block length 
  
    
      
        n
      
    
    {\displaystyle n}
  
, size 
  
    
      
        M
      
    
    {\displaystyle M}
  
 and minimum distance 
  
    
      
        d
      
    
    {\displaystyle d}
  
.
