---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lucky
offline_file: ""
offline_thumbnail: ""
uuid: 1b6afbea-3be0-48b1-a9a3-4ad7f9873cc1
updated: 1484310150
title: lucky
categories:
    - Dictionary
---
lucky
     adj 1: blessed with good fortune; "considered himself lucky that
            the tornado missed his house"; "a lucky guess"
     2: having or bringing good fortune; "my lucky day" [ant: {unlucky}]
     3: tending to favor or bring good luck; "miracles are
        auspicious accidents"; "encouraging omens"; "a favorable
        time to ask for a raise"; "lucky stars"; "a prosperous
        moment to make a decision" [syn: {auspicious}, {encouraging},
         {favorable}, {favourable}, {prosperous}]
     [also: {luckiest}, {luckier}]
