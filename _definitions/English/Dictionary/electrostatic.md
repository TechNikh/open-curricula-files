---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrostatic
offline_file: ""
offline_thumbnail: ""
uuid: 1d21770b-c1bf-4d56-ae06-27df1c8044b0
updated: 1484310403
title: electrostatic
categories:
    - Dictionary
---
electrostatic
     adj : concerned with or producing or caused by static electricity;
           "an electrostatic generator produces high-voltage
           static electricity" [syn: {static}]
