---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oleic
offline_file: ""
offline_thumbnail: ""
uuid: a92b6758-5fe8-4893-b197-d5cea8eda9b8
updated: 1484308405
title: Oleic acid
tags:
    - Occurrence
    - As an insect pheromone
    - Production and chemical behavior
    - Uses
    - Health effects
categories:
    - Uncategorized
---
Oleic acid is a fatty acid that occurs naturally in various animal and vegetable fats and oils. It is an odorless, colorless oil, although commercial samples may be yellowish. In chemical terms, oleic acid is classified as a monounsaturated omega-9 fatty acid, abbreviated with a lipid number of 18:1 cis-9. It has the formula CH3(CH2)7CH=CH(CH2)7COOH.[2] The term "oleic" means related to, or derived from, olive oil which is predominantly composed of oleic acid. The corresponding stereoisomer trans-9-Octadecenoic acid is called Elaidic acid. These isomers have distinct physical properties and biochemical properties. Elaidic acid, the most abundant trans fatty acid in diet, appear to have an ...
