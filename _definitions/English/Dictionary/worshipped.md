---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worshipped
offline_file: ""
offline_thumbnail: ""
uuid: 363d0246-374c-4879-b248-98184404b398
updated: 1484310541
title: worshipped
categories:
    - Dictionary
---
worship
     n 1: the activity of worshipping
     2: a feeling of profound love and admiration [syn: {adoration}]
     v 1: love unquestioningly and uncritically or to excess; venerate
          as an idol; "Many teenagers idolized the Beatles" [syn:
          {idolize}, {idolise}, {hero-worship}, {revere}]
     2: show devotion to (a deity); "Many Hindus worship Shiva"
     3: attend religious services; "They worship in the traditional
        manner"
     [also: {worshipping}, {worshipped}]
