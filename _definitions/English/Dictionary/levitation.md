---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/levitation
offline_file: ""
offline_thumbnail: ""
uuid: 44953c55-f399-49ee-8e8e-f2a65806ffdf
updated: 1484310366
title: levitation
categories:
    - Dictionary
---
levitation
     n 1: the phenomenon of a person or thing rising into the air by
          apparently supernatural means
     2: movement upward in virtue of lightness [ant: {gravitation}]
     3: the act of raising (a body) from the ground by presumably
        spiritualistic means
