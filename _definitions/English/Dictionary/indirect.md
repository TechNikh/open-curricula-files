---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indirect
offline_file: ""
offline_thumbnail: ""
uuid: 6fc3fe77-7eab-4090-99aa-bef248c99e5c
updated: 1484310479
title: indirect
categories:
    - Dictionary
---
indirect
     adj 1: having intervening factors or persons or influences;
            "reflection from the ceiling provided a soft indirect
            light"; "indirect evidence"; "an indirect cause"
     2: not direct in spatial dimension; not leading by a straight
        line or course to a destination; "sometimes taking an
        indirect path saves time"; "must take an indirect couse in
        sailing" [ant: {direct}]
     3: descended from a common ancestor but through different
        lines; "cousins are collateral relatives"; "an indirect
        descendant of the Stuarts" [syn: {collateral}] [ant: {lineal}]
     4: extended senses; not direct in manner or language or
        ...
