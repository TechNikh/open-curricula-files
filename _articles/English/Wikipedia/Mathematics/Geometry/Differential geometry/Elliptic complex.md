---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elliptic_complex
offline_file: ""
offline_thumbnail: ""
uuid: b1f9d758-5889-4693-8f85-2f3beab2c006
updated: 1484309216
title: Elliptic complex
categories:
    - Differential geometry
---
In mathematics, in particular in partial differential equations and differential geometry, an elliptic complex generalizes the notion of an elliptic operator to sequences. Elliptic complexes isolate those features common to the de Rham complex and the Dolbeault complex which are essential for performing Hodge theory. They also arise in connection with the Atiyah-Singer index theorem and Atiyah-Bott fixed point theorem.
