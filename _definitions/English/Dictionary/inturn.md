---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inturn
offline_file: ""
offline_thumbnail: ""
uuid: 861967c3-cb30-4b9b-9fce-f2c256234c9b
updated: 1484310264
title: inturn
categories:
    - Dictionary
---
in turn
     adv : in proper order or sequence; "talked to each child in turn";
           "the stable became in turn a chapel and then a movie
           theater" [syn: {successively}]
