---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaos_game
offline_file: ""
offline_thumbnail: ""
uuid: c5de4800-2f23-4f26-a08d-9b85461b9e8f
updated: 1484308696
title: Chaos game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Sierpinski_chaos_animated.gif
tags:
    - Restricted chaos game
categories:
    - Chaos theory
---
In mathematics, the term chaos game originally referred to a method of creating a fractal, using a polygon and an initial point selected at random inside it.[1][2] The fractal is created by iteratively creating a sequence of points, starting with the initial random point, in which each point in the sequence is a given fraction of the distance between the previous point and one of the vertices of the polygon; the vertex is chosen at random in each iteration. Repeating this iterative process a large number of times, selecting the vertex at random on each iteration, and throwing out the first few points in the sequence, will often (but not always) produce a fractal shape. Using a regular ...
