---
version: 1
type: video
provider: YouTube
id: u38LjCOvDZU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Protein%20Modification%20%28Golgi%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9665fae4-28d4-4791-80df-9e74114612df
updated: 1486069670
title: Protein Modification (Golgi)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/u38LjCOvDZU/default.jpg
    - https://i3.ytimg.com/vi/u38LjCOvDZU/1.jpg
    - https://i3.ytimg.com/vi/u38LjCOvDZU/2.jpg
    - https://i3.ytimg.com/vi/u38LjCOvDZU/3.jpg
---
