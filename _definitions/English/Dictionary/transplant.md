---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transplant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484601361
title: transplant
categories:
    - Dictionary
---
transplant
     n 1: (surgery) tissue or organ transplanted from a donor to a
          recipient; in some cases the patient can be both donor
          and recipient [syn: {graft}]
     2: an operation moving an organ from one organism (the donor)
        to another (the recipient); "he had a kidney transplant"
        [syn: {transplantation}, {organ transplant}]
     3: the act of uprooting and moving a plant to a new location;
        "the transplant was successful"; "too frequent
        transplanting is not good for plants" [syn: {transplanting},
         {transplantation}]
     v 1: lift and reset in another soil or situation; "Transplant the
          young rice plants" [syn: ...
