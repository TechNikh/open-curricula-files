---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utilise
offline_file: ""
offline_thumbnail: ""
uuid: 2509c716-20f8-450a-bc52-21903d0c7c1a
updated: 1484310279
title: utilise
categories:
    - Dictionary
---
utilise
     v : put into service; make work or employ (something) for a
         particular purpose or for its inherent or natural
         purpose; "use your head!"; "we only use Spanish at home";
         "I can't make use of this tool"; "Apply a magnetic field
         here"; "This thinking was applied to many projects"; "How
         do you utilize this tool?"; "I apply this rule to get
         good results"; "use the plastic bags to store the food";
         "He doesn't know how to use a computer" [syn: {use}, {utilize},
          {apply}, {employ}]
