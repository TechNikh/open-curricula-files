---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gold
offline_file: ""
offline_thumbnail: ""
uuid: c2f36509-bc42-41f7-8977-73fcb9bb940b
updated: 1484310198
title: gold
categories:
    - Dictionary
---
gold
     adj 1: made from or covered with gold; "gold coins"; "the gold dome
            of the Capitol"; "the golden calf"; "gilded icons"
            [syn: {golden}, {gilded}]
     2: having the deep slightly brownish color of gold; "long
        aureate (or golden) hair"; "a gold carpet" [syn: {aureate},
         {gilded}, {gilt}, {golden}]
     n 1: coins made of gold
     2: a deep yellow color; "an amber light illuminated the room";
        "he admired the gold of her hair" [syn: {amber}]
     3: a soft yellow malleable ductile (trivalent and univalent)
        metallic element; occurs mainly as nuggets in rocks and
        alluvial deposits; does not react with most chemicals but
   ...
