---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Derived_algebraic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 0a954ed2-cbdc-4380-8f50-a1e7733ffb93
updated: 1484309145
title: Derived algebraic geometry
tags:
    - Introduction
    - Notes
categories:
    - Algebraic geometry
---
Derived algebraic geometry is a branch of mathematics that generalizes algebraic geometry to a situation where commutative rings, which provide a local chart, are replaced by ring spectra in algebraic topology, whose higher homotopy accounts for the non-discreteness (e.g, Tor) of the structure sheaf. Grothendieck's scheme theory allows the structure sheaf to carry nilpotent elements. Derived algebraic geometry can be thought of as an extension of this, and provides a natural setting for cotangent complexes and such in deformation theory (cf. F. Francis).
