---
version: 1
type: video
provider: YouTube
id: O59Ho11Gdik
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 44fd8494-77ab-48d0-9c55-7d70d57bc05d
updated: 1486069611
title: Comparing numbers of objects
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/O59Ho11Gdik/default.jpg
    - https://i3.ytimg.com/vi/O59Ho11Gdik/1.jpg
    - https://i3.ytimg.com/vi/O59Ho11Gdik/2.jpg
    - https://i3.ytimg.com/vi/O59Ho11Gdik/3.jpg
---
