---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/four
offline_file: ""
offline_thumbnail: ""
uuid: 11232198-a34e-4a3e-8814-ee91e50c6249
updated: 1484310277
title: four
categories:
    - Dictionary
---
four
     adj : being one more than three [syn: {4}, {iv}]
     n : the cardinal number that is the sum of three and one [syn: {4},
          {IV}, {tetrad}, {quatern}, {quaternion}, {quaternary}, {quaternity},
          {quartet}, {quadruplet}, {foursome}, {Little Joe}]
