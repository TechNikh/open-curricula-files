---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/combining
offline_file: ""
offline_thumbnail: ""
uuid: 797d5beb-0e25-4922-8d70-89058e02baa8
updated: 1484310401
title: combining
categories:
    - Dictionary
---
combining
     n 1: an occurrence that results in things being united [syn: {combine}]
     2: the act of combining things to form a new whole [syn: {combination},
         {compounding}]
