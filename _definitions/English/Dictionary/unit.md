---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unit
offline_file: ""
offline_thumbnail: ""
uuid: d7c25ab7-ad5e-42c3-990c-59285db9cbbf
updated: 1484310268
title: unit
categories:
    - Dictionary
---
unit
     n 1: any division of quantity accepted as a standard of
          measurement or exchange; "the dollar is the United
          States unit of currency"; "a unit of wheat is a bushel";
          "change per unit volume" [syn: {unit of measurement}]
     2: an individual or group or structure or other entity regarded
        as a structural or functional constituent of a whole; "the
        reduced the number of units and installations"; "the word
        is a basic linguistic unit"
     3: an organization regarded as part of a larger social group;
        "the coach said the offensive unit did a good job"; "after
        the battle the soldier had trouble rejoining his unit"
       ...
