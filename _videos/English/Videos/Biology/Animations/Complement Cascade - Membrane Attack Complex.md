---
version: 1
type: video
provider: YouTube
id: E_fdPaBBPic
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ac86c17e-0ba3-461b-9abc-1d87d4c461c0
updated: 1486069668
title: 'Complement Cascade - Membrane Attack Complex'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/E_fdPaBBPic/default.jpg
    - https://i3.ytimg.com/vi/E_fdPaBBPic/1.jpg
    - https://i3.ytimg.com/vi/E_fdPaBBPic/2.jpg
    - https://i3.ytimg.com/vi/E_fdPaBBPic/3.jpg
---
