---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nitty-gritty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495701
title: nitty-gritty
categories:
    - Dictionary
---
nitty-gritty
     n : the choicest or most essential or most vital part of some
         idea or experience; "the gist of the prosecutor's
         argument"; "the heart and soul of the Republican Party";
         "the nub of the story" [syn: {kernel}, {substance}, {core},
          {center}, {essence}, {gist}, {heart}, {heart and soul},
         {inwardness}, {marrow}, {meat}, {nub}, {pith}, {sum}]
