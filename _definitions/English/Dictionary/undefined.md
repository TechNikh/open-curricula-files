---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undefined
offline_file: ""
offline_thumbnail: ""
uuid: 7ac4d84e-422f-4de9-a75a-f8353585e1b2
updated: 1484310599
title: undefined
categories:
    - Dictionary
---
undefined
     adj : not precisely limited, determined, or distinguished; "an
           undefined term"; "undefined authority"; "some undefined
           sense of excitement"; "vague feelings of sadness"; "a
           vague uneasiness" [syn: {vague}] [ant: {defined}]
