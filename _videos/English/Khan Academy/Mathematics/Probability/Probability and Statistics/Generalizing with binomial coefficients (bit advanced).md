---
version: 1
type: video
provider: YouTube
id: AOsWph2FNLw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Generalizing%20with%20binomial%20coefficients%20%28bit%20advanced%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6ef2a7ea-6606-4186-b51d-ff6d69d9fc11
updated: 1486069688
title: Generalizing with binomial coefficients (bit advanced)
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/AOsWph2FNLw/default.jpg
    - https://i3.ytimg.com/vi/AOsWph2FNLw/1.jpg
    - https://i3.ytimg.com/vi/AOsWph2FNLw/2.jpg
    - https://i3.ytimg.com/vi/AOsWph2FNLw/3.jpg
---
