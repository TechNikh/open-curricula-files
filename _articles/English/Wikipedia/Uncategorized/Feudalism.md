---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Landlordism
offline_file: ""
offline_thumbnail: ""
uuid: ef71be66-b6e6-458f-b294-b9d12ab955f9
updated: 1484308485
title: Feudalism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rolandfealty.jpg
tags:
    - Definition
    - Etymology
    - History
    - Classic feudalism
    - Vassalage
    - The "Feudal Revolution" in France
    - End of European feudalism
    - Feudal society
    - Historiography
    - Evolution of the concept
categories:
    - Uncategorized
---
Feudalism was a combination of legal and military customs in medieval Europe that flourished between the 9th and 15th centuries. Broadly defined, it was a way of structuring society around relationships derived from the holding of land in exchange for service or labour.
