---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abelian_surface
offline_file: ""
offline_thumbnail: ""
uuid: 49f2c198-7d74-4faa-91d8-26d226be5cb7
updated: 1484309352
title: Abelian surface
categories:
    - Geometry stubs
---
One-dimensional complex tori are just elliptic curves and are all algebraic, but Riemann discovered that most complex tori of dimension 2 are not algebraic. The algebraic ones are called abelian surfaces and are exactly the 2-dimensional abelian varieties. Most of their theory is a special case of the theory of higher-dimensional tori or abelian varieties. Criteria to be a product of two elliptic curves (up to isogeny) were a popular study in the nineteenth century.
