---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Warren_Ambrose
offline_file: ""
offline_thumbnail: ""
uuid: aa46723c-fdf6-48c6-b928-fdfe0e6956ee
updated: 1484309071
title: Warren Ambrose
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Bluetank_1.png
tags:
    - Personal life
    - Career
    - Political activism
categories:
    - Geometers
---
Warren Arthur Ambrose (October 25, 1914 – December 4, 1995) was Professor Emeritus of Mathematics at the Massachusetts Institute of Technology and at the University of Buenos Aires.
