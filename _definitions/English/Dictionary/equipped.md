---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equipped
offline_file: ""
offline_thumbnail: ""
uuid: 745165e9-ca9e-45e2-ac37-933c1c083b8c
updated: 1484310321
title: equipped
categories:
    - Dictionary
---
equip
     v 1: provide with (something) usually for a specific purpose;
          "The expedition was equipped with proper clothing, food,
          and other necessities" [syn: {fit}, {fit out}, {outfit}]
     2: provide with abilities or understanding; "She was never
        equipped to be a dancer"
     [also: {equipping}, {equipped}]
