---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symmetrically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484326561
title: symmetrically
categories:
    - Dictionary
---
symmetrically
     adv : in a symmetrical manner; "they were symmetrically arranged"
           [ant: {asymmetrically}]
