---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phase_response
offline_file: ""
offline_thumbnail: ""
uuid: 90263696-ccd0-44e0-9029-c0d093c732e2
updated: 1484309347
title: Phase response
categories:
    - Trigonometry
---
In signal processing and electrical engineering, phase response is the relationship between the phase of a sinusoidal input and the output signal passing through any device that accepts input and produces an output signal, such as an amplifier or a filter.
