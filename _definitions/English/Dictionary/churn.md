---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/churn
offline_file: ""
offline_thumbnail: ""
uuid: 2cc75f0e-b844-4643-a8a6-376d0f7450f8
updated: 1484310330
title: churn
categories:
    - Dictionary
---
churn
     n : a vessel in which cream is agitated to separate butterfat
         from buttermilk [syn: {butter churn}]
     v 1: stir (cream) vigorously in order to make butter
     2: be agitated; "the sea was churning in the storm" [syn: {boil},
         {moil}, {roil}]
