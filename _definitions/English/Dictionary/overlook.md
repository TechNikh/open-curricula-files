---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overlook
offline_file: ""
offline_thumbnail: ""
uuid: 8a3bd4a6-7931-45f5-b004-f7c5f745d886
updated: 1484310307
title: overlook
categories:
    - Dictionary
---
overlook
     n : a high place affording a good view
     v 1: look past, fail to notice
     2: be oriented in a certain direction; "The house looks out on
        a tennis court"; "The apartment overlooks the Hudson"
        [syn: {look out on}, {look out over}, {look across}]
     3: leave undone or leave out; "How could I miss that typo?";
        "The workers on the conveyor belt miss one out of ten"
        [syn: {neglect}, {pretermit}, {omit}, {drop}, {miss}, {leave
        out}, {overleap}] [ant: {attend to}]
     4: look down on; "The villa dominates the town" [syn: {dominate},
         {command}, {overtop}]
     5: watch over; "I am overlooking her work"
