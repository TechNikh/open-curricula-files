---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contour
offline_file: ""
offline_thumbnail: ""
uuid: 264809df-cd2d-473e-8078-721c51c17370
updated: 1484310254
title: contour
categories:
    - Dictionary
---
contour
     n 1: a line drawn on a map connecting points of equal height
          [syn: {contour line}]
     2: any spatial attributes (especially as defined by outline);
        "he could barely make out their shapes through the smoke"
        [syn: {shape}, {form}, {configuration}, {conformation}]
     3: a feature (or the order or arrangement of features) of
        anything having a complex structure; "the contours of the
        melody"; "it defines a major contour of this
        administration"
     v : form the contours of
