---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/avenue
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632561
title: avenue
categories:
    - Dictionary
---
avenue
     n 1: a line of approach; "they explored every avenue they could
          think of"; "it promises to open new avenues to
          understanding"
     2: a wide street or thoroughfare [syn: {boulevard}]
