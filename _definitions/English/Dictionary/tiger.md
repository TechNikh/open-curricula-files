---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tiger
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477161
title: tiger
categories:
    - Dictionary
---
tiger
     n 1: a fierce or audacious person; "he's a tiger on the tennis
          court"; "it aroused the tiger in me"
     2: large feline of forests in most of Asia having a tawny coat
        with black stripes; endangered [syn: {Panthera tigris}]
