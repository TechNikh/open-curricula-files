---
version: 1
type: article
id: https://en.wikipedia.org/wiki/R._P._Gupta
offline_file: ""
offline_thumbnail: ""
uuid: b2d0462d-34b2-422c-9bd2-8aa29ab95f5d
updated: 1484309463
title: R. P. Gupta
categories:
    - Graph theorists
---
Ram Prakash Gupta was a professor of graph theory[1] at Waterloo, Canada and Ohio State University, Columbus. He received his Ph.D. in graph theory from the Indian Statistical Institute, Calcutta, India in 1968. His advisor was C. R. Rao. Gupta is known for his work in chromatic numbers.
