---
version: 1
type: video
provider: YouTube
id: u9slVq87czg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c37d39c0-e418-4486-8b8b-dedd4f7761b2
updated: 1486069660
title: 'Ice Cube Trick - Cool Science Experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/u9slVq87czg/default.jpg
    - https://i3.ytimg.com/vi/u9slVq87czg/1.jpg
    - https://i3.ytimg.com/vi/u9slVq87czg/2.jpg
    - https://i3.ytimg.com/vi/u9slVq87czg/3.jpg
---
