---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Malpighian_tubule
offline_file: ""
offline_thumbnail: ""
uuid: 529cbb60-d746-4e3c-9e00-3d4b21766dbc
updated: 1484308371
title: Malpighian tubule system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Malpighian_tube.svg.png
tags:
    - Structure
    - General mode of action
    - Alternative modes of action
    - Other uses
categories:
    - Uncategorized
---
The system consists of branching tubules extending from the alimentary canal that absorbs solutes, water, and wastes from the surrounding hemolymph. The wastes then are released from the organism in the form of solid nitrogenous compounds. The system is named after Marcello Malpighi, a seventeenth-century anatomist.
