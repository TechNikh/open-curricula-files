---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Projections_onto_convex_sets
offline_file: ""
offline_thumbnail: ""
uuid: 793c6aa5-e881-4b66-a17e-8d3118487f2b
updated: 1484309193
title: Projections onto convex sets
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Projections_onto_convex_sets_circles.svg.png
tags:
    - Algorithm
    - Related algorithms
categories:
    - Convex geometry
---
In mathematics, projections onto convex sets (POCS), sometimes known as the alternating projection method, is a method to find a point in the intersection of two closed convex sets. It is a very simple algorithm and has been rediscovered many times.[1] The simplest case, when the sets are affine spaces, was analyzed by John von Neumann.[2] [3] The case when the sets are affine spaces is special, since the iterates not only converge to a point in the intersection (assuming the intersection is non-empty) but in fact to the orthogonal projection onto the intersection of the initial iterate. For general closed convex sets, the limit point need not be the projection. Classical work on the case ...
