---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yet
offline_file: ""
offline_thumbnail: ""
uuid: 31f8417d-0cb5-4885-b471-fb79f7be4be4
updated: 1484310240
title: yet
categories:
    - Dictionary
---
yet
     adv 1: up to the present time; "I have yet to see the results";
            "details are yet to be worked out"
     2: used in negative statement to describe a situation that has
        existed up to this point or up to the present time; "So
        far he hasn't called"; "the sun isn't up yet" [syn: {so
        far}, {thus far}, {up to now}, {hitherto}, {heretofore}, {as
        yet}, {til now}, {until now}]
     3: to a greater degree or extent; used with comparisons;
        "looked sick and felt even worse"; "an even (or still)
        more interesting problem"; "still another problem must be
        solved"; "a yet sadder tale" [syn: {even}, {still}]
     4: within an ...
