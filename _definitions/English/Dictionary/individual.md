---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/individual
offline_file: ""
offline_thumbnail: ""
uuid: edc0ae8e-2ac9-4495-baa5-e431348577e1
updated: 1484310297
title: individual
categories:
    - Dictionary
---
individual
     adj 1: being or characteristic of a single thing or person;
            "individual drops of rain"; "please mark the
            individual pages"; "they went their individual ways"
            [ant: {common}]
     2: separate and distinct from others of the same kind; "mark
        the individual pages"; "on a case-by-case basis" [syn: {case-by-case},
         {item-by-item}]
     3: characteristic of or meant for a single person or thing; "an
        individual serving"; "separate rooms"; "single occupancy";
        "a single bed" [syn: {separate}, {single(a)}]
     4: concerning one person exclusively; "we all have individual
        cars"; "each room has a private bath" ...
