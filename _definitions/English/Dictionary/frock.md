---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frock
offline_file: ""
offline_thumbnail: ""
uuid: 9003c24e-0021-41e6-9ec6-91631f70302f
updated: 1484310569
title: frock
categories:
    - Dictionary
---
frock
     n : a one-piece garment for a woman; has skirt and bodice [syn:
         {dress}]
     v : put a frock on
