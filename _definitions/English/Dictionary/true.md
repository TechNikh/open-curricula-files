---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/true
offline_file: ""
offline_thumbnail: ""
uuid: 7da2aae5-4985-425b-a2e0-8748bc19aa11
updated: 1484310279
title: 'true'
categories:
    - Dictionary
---
true
     adj 1: consistent with fact or reality; not false; "the story is
            true"; "it is undesirable to believe a proposition
            when there is no ground whatever for supposing it
            true"- B. Russell; "the true meaning of the statement"
            [ant: {false}]
     2: not synthetic or spurious; of real or natural origin; "real
        mink"; "true gold" [syn: {real}]
     3: conforming to definitive criteria; "the horseshoe crab is
        not a true crab"; "Pythagoras was the first true
        mathematician"
     4: accurately placed or thrown; "his aim was true"; "he was
        dead on target" [syn: {dead on target}]
     5: devoted (sometimes ...
