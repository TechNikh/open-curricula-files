---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homothetic_transformation
offline_file: ""
offline_thumbnail: ""
uuid: ac0c708c-8365-4b40-a7da-20c3191e334d
updated: 1484309336
title: Homothetic transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/334px-Geom_podobnost_stejnolehlest.svg.png
tags:
    - Homothety and uniform scaling
    - Notes
categories:
    - Transformation (function)
---
In mathematics, a homothety (or homothecy, or homogeneous dilation) is a transformation of an affine space determined by a point S called its center and a nonzero number λ called its ratio, which sends
