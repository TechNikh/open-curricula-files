---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beak
offline_file: ""
offline_thumbnail: ""
uuid: 16ccc8ef-8215-48f6-98ff-e9aabe45b4b8
updated: 1484310162
title: beak
categories:
    - Dictionary
---
beak
     n 1: beaklike mouth of animals other than birds (e.g., turtles)
     2: horny projecting mouth of a bird [syn: {bill}, {neb}, {nib},
         {pecker}]
     3: informal terms for the nose [syn: {honker}, {hooter}, {nozzle},
         {snoot}, {snout}, {schnozzle}, {schnoz}]
     v : hit lightly with a picking motion [syn: {peck}, {pick}]
