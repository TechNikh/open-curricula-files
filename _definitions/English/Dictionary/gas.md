---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gas
offline_file: ""
offline_thumbnail: ""
uuid: b4c45c76-0ee8-4925-9155-2259aa2c9f1a
updated: 1484310249
title: gas
categories:
    - Dictionary
---
gas
     n 1: the state of matter distinguished from the solid and liquid
          states by: relatively low density and viscosity;
          relatively great expansion and contraction with changes
          in pressure and temperature; the ability to diffuse
          readily; and the spontaneous tendency to become
          distributed uniformly throughout any container
     2: a fluid in the gaseous state having neither independent
        shape nor volume and being able to expand indefinitely
     3: a volatile flammable mixture of hydrocarbons (hexane and
        heptane and octane etc.) derived from petroleum; used
        mainly as a fuel in internal-combustion engines [syn: ...
