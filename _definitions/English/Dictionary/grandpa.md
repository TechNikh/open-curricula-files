---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandpa
offline_file: ""
offline_thumbnail: ""
uuid: fa09a3f9-74b6-404c-a9d0-f4cb35ade7eb
updated: 1484310309
title: grandpa
categories:
    - Dictionary
---
grandpa
     n : the father of your father or mother [syn: {grandfather}, {gramps},
          {granddad}, {grandad}, {granddaddy}]
