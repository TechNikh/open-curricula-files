---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetical_set
offline_file: ""
offline_thumbnail: ""
uuid: 0b4b86b0-6718-4c65-af0e-6526a8a59c44
updated: 1484309481
title: Arithmetical set
tags:
    - Formal definition
    - Examples
    - Properties
    - Implicitly arithmetical sets
categories:
    - Computability theory
---
In mathematical logic, an arithmetical set (or arithmetic set) is a set of natural numbers that can be defined by a formula of first-order Peano arithmetic. The arithmetical sets are classified by the arithmetical hierarchy.
