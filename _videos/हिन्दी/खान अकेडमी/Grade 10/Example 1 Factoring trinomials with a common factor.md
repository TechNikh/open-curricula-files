---
version: 1
type: video
provider: YouTube
id: c4aaXHe4Z50
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 00bc9ce4-8a27-40f2-a77f-1254db7eb878
updated: 1486069619
title: Example 1 Factoring trinomials with a common factor
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/c4aaXHe4Z50/default.jpg
    - https://i3.ytimg.com/vi/c4aaXHe4Z50/1.jpg
    - https://i3.ytimg.com/vi/c4aaXHe4Z50/2.jpg
    - https://i3.ytimg.com/vi/c4aaXHe4Z50/3.jpg
---
