---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Low_basis_theorem
offline_file: ""
offline_thumbnail: ""
uuid: b25e23f6-ff9c-461f-a2ce-3c4a82572f15
updated: 1484309487
title: Low basis theorem
categories:
    - Computability theory
---
The low basis theorem in computability theory states that every nonempty 
  
    
      
        
          Π
          
            1
          
          
            0
          
        
      
    
    {\displaystyle \Pi _{1}^{0}}
  
 class in 
  
    
      
        
          2
          
            ω
          
        
      
    
    {\displaystyle 2^{\omega }}
  
 (see arithmetical hierarchy) contains a set of low degree (Soare 1987:109). It was first proved by Carl Jockusch and Robert I. Soare in 1972 (Nies 2009:57). Cenzer (1993:53) describes it as "perhaps the most cited result in the theory of 
  
    
      
        
          Π
          
            1
          
       ...
