---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conserved_current
offline_file: ""
offline_thumbnail: ""
uuid: 2bf217df-994d-474f-8f06-3b43c3468afa
updated: 1484309304
title: Conserved current
categories:
    - Symmetry
---
In physics a conserved current is a current, 
  
    
      
        
          j
          
            μ
          
        
      
    
    {\displaystyle j^{\mu }}
  
, that satisfies the continuity equation 
  
    
      
        
          ∂
          
            μ
          
        
        
          j
          
            μ
          
        
        =
        0
      
    
    {\displaystyle \partial _{\mu }j^{\mu }=0}
  
. The continuity equation represents a conservation law, hence the name.
