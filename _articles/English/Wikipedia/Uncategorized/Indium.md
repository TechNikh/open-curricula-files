---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indium
offline_file: ""
offline_thumbnail: ""
uuid: d5de21e3-15b3-44df-8ca7-99050198c7c4
updated: 1484308383
title: Indium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Indium.jpg
tags:
    - Properties
    - Physical
    - Chemical
    - Isotopes
    - Compounds
    - Indium(III)
    - Indium(I)
    - Other oxidation states
    - Organoindium compounds
    - History
    - Occurrence
    - Production
    - Applications
    - Biological role and precautions
    - Bibliography
categories:
    - Uncategorized
---
Indium is a chemical element with symbol In and atomic number 49. It is a post-transition metal that makes up 0.21 parts per million of the Earth's crust. Very soft and malleable, Indium has a melting point higher than sodium and gallium, but lower than lithium or tin. Ferdinand Reich and Hieronymous Theodor Richter discovered it with spectroscopy in 1863, naming it for the indigo blue line in its spectrum. It was isolated the next year.
