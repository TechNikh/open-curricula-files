---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/walk
offline_file: ""
offline_thumbnail: ""
uuid: ada7c2a1-c1fd-4cb7-a8f5-2d456cb42b64
updated: 1484310240
title: walk
categories:
    - Dictionary
---
walk
     n 1: the act of traveling by foot; "walking is a healthy form of
          exercise" [syn: {walking}]
     2: (baseball) an advance to first base by a batter who receives
        four balls; "he worked the pitcher for a base on balls"
        [syn: {base on balls}, {pass}]
     3: manner of walking; "he had a funny walk" [syn: {manner of
        walking}]
     4: the act of walking somewhere; "he took a walk after lunch"
     5: a path set aside for walking; "after the blizzard he
        shoveled the front walk" [syn: {walkway}, {paseo}]
     6: a slow gait of a horse in which two feet are always on the
        ground
     7: careers in general; "it happens in all walks of life" ...
