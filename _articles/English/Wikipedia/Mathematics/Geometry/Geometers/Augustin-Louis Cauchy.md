---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Augustin-Louis_Cauchy
offline_file: ""
offline_thumbnail: ""
uuid: 25259f6c-bacd-4f26-be03-fab71ac6e413
updated: 1484309077
title: Augustin-Louis Cauchy
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cauchy_-_Le%25C3%25A7ons_sur_le_calcul_diff%25C3%25A9rentiel%252C_1829_-_576181_F.jpg'
tags:
    - Biography
    - Youth and education
    - Engineering days
    - Professor at École Polytechnique
    - In exile
    - Last years
    - Work
    - Early work
    - Wave theory, mechanics, elasticity
    - Number theory
    - Complex functions
    - "Cours d'Analyse"
    - "Taylor's theorem"
    - Argument principle, stability
    - Output
    - Politics and religious beliefs
    - Notes
categories:
    - Geometers
---
Baron Augustin-Louis Cauchy FRS FRSE (French: [oɡystɛ̃ lwi koʃi]; 21 August 1789 – 23 May 1857) was a French mathematician reputed as a pioneer of analysis. He was one of the first to state and prove theorems of calculus rigorously, rejecting the heuristic principle of the generality of algebra of earlier authors. He almost singlehandedly founded complex analysis and the study of permutation groups in abstract algebra. A profound mathematician, Cauchy had a great influence over his contemporaries and successors. His writings range widely in mathematics and mathematical physics.
