---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/palate
offline_file: ""
offline_thumbnail: ""
uuid: 87682aac-66ed-4893-8212-735e0217d397
updated: 1484310344
title: palate
categories:
    - Dictionary
---
palate
     n : the upper surface of the mouth that separates the oral and
         nasal cavities [syn: {roof of the mouth}]
