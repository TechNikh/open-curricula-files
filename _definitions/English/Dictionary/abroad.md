---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abroad
offline_file: ""
offline_thumbnail: ""
uuid: 47859f2d-2147-4f04-9df3-a3af5f4dcacf
updated: 1484310441
title: abroad
categories:
    - Dictionary
---
abroad
     adj : in a foreign country; "markets abroad"; "overseas markets"
           [syn: {overseas}]
     adv 1: to or in a foreign country; "they had never travelled
            abroad"
     2: far away from home or one's usual surroundings; "looking
        afield for new lands to conquer"- R.A.Hall [syn: {afield}]
     3: in a place across an ocean [syn: {overseas}, {beyond the sea},
         {over the sea}]
