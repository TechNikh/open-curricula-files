---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refining
offline_file: ""
offline_thumbnail: ""
uuid: 364eaccd-0939-4167-8ba7-76e182edfcd7
updated: 1484310379
title: refining
categories:
    - Dictionary
---
refining
     n : the process of removing impurities (as from oil or metals or
         sugar etc.) [syn: {refinement}, {purification}]
