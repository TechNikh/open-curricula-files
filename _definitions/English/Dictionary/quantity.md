---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantity
offline_file: ""
offline_thumbnail: ""
uuid: ec82c4ac-c2a4-4f42-8ec4-68826c7f6d7a
updated: 1484310333
title: quantity
categories:
    - Dictionary
---
quantity
     n 1: how much there is of something that you can quantify [syn: {measure},
           {amount}]
     2: an adequate or large amount; "he had a quantity of
        ammunition"
     3: something that has a magnitude and can be represented in
        mathematical expressions by a constant or a variable
