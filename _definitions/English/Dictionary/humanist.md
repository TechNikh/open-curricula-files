---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humanist
offline_file: ""
offline_thumbnail: ""
uuid: bfaf7ab2-6ec5-464c-a63f-eb3265c370c4
updated: 1484310413
title: humanist
categories:
    - Dictionary
---
humanist
     adj 1: of or pertaining to Renaissance humanism; "the humanistic
            revival of learning" [syn: {humanistic}]
     2: of or pertaining to a philosophy asserting human dignity and
        man's capacity for fulfillment through reason and
        scientific method and often rejecting religion; "the
        humanist belief in continuous emergent evolution"- Wendell
        Thomas [syn: {humanistic}]
     3: pertaining to or concerned with the humanities; "humanistic
        studies"; "a humane education" [syn: {humanistic}, {humane}]
     4: marked by humanistic values and devotion to human welfare;
        "a humane physician"; "released the prisoner for
        ...
