---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Martin_measure
offline_file: ""
offline_thumbnail: ""
uuid: 2773b617-9b4c-42d2-9f1f-6a8581c0317d
updated: 1484309489
title: Martin measure
categories:
    - Computability theory
---
In descriptive set theory, the Martin measure is a filter on the set of Turing degrees of sets of natural numbers, named after Donald A. Martin. Under the axiom of determinacy it can be shown to be an ultrafilter.
