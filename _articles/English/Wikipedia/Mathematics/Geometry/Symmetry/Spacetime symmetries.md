---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spacetime_symmetries
offline_file: ""
offline_thumbnail: ""
uuid: c4b8e696-d105-453a-85d3-d8f93de15255
updated: 1484309315
title: Spacetime symmetries
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon_3.jpg
tags:
    - Physical motivation
    - Mathematical definition
    - Killing symmetry
    - Homothetic symmetry
    - Affine symmetry
    - Conformal symmetry
    - Curvature symmetry
    - Matter symmetry
    - Local and global symmetries
    - Applications
    - Spacetime classifications
categories:
    - Symmetry
---
Spacetime symmetries are features of spacetime that can be described as exhibiting some form of symmetry. The role of symmetry in physics is important in simplifying solutions to many problems. Spacetime symmetries are used in the study of exact solutions of Einstein's field equations of general relativity.
