---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alpha
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484310061
title: alpha
categories:
    - Dictionary
---
alpha
     adj 1: first in order of importance; "the alpha male in the group
            of chimpanzees"; "the alpha star in a constellation is
            the brightest or main star"
     2: early testing stage of a software or hardware product;
        "alpha version"
     n 1: the 1st letter of the Greek alphabet
     2: the beginning of a series or sequence; "the Alpha and Omega,
        the first and the last, the beginning and the
        end"--Revelations
