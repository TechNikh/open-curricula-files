---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/likewise
offline_file: ""
offline_thumbnail: ""
uuid: 00a326e3-7c9c-409b-92e4-9d0a66e7def5
updated: 1484310527
title: likewise
categories:
    - Dictionary
---
likewise
     adv 1: in like or similar manner; "He was similarly affected";
            "some people have little power to do good, and have
            likewise little strength to resist evil"- Samuel
            Johnson [syn: {similarly}]
     2: in addition; "he has a Mercedes, too" [syn: {besides}, {too},
         {also}, {as well}]
     3: equally; "parents and teachers alike demanded reforms" [syn:
         {alike}]
