---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ferment
offline_file: ""
offline_thumbnail: ""
uuid: 18093e77-e9ca-4514-bc5a-602095949cae
updated: 1484310607
title: ferment
categories:
    - Dictionary
---
ferment
     n 1: a state of agitation or turbulent change or development;
          "the political ferment produced a new leadership";
          "social unrest" [syn: {agitation}, {fermentation}, {unrest}]
     2: a substance capable of bringing about fermentation
     3: a process in which an agent causes an organic substance to
        break down into simpler substances; especially, the
        anaerobic breakdown of sugar into alcohol [syn: {zymosis},
         {zymolysis}, {fermentation}, {fermenting}]
     4: a chemical phenomenon in which an organic molecule splits
        into simpler substances [syn: {fermentation}]
     v 1: be in an agitated or excited state; "The Middle East is
  ...
