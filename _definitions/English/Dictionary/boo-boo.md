---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boo-boo
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495521
title: boo-boo
categories:
    - Dictionary
---
boo-boo
     n : an embarrassing mistake [syn: {blunder}, {blooper}, {bloomer},
          {bungle}, {foul-up}, {fuckup}, {flub}, {botch}, {boner}]
