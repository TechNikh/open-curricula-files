---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cubic_cupola
offline_file: ""
offline_thumbnail: ""
uuid: 32ff173e-556a-4bbf-a97e-255ae43d0b93
updated: 1484309359
title: Cubic cupola
tags:
    - Related polytopes
categories:
    - Geometry stubs
---
In 4-dimensional geometry, the cubic cupola is a 4-polytope bounded by a rhombicuboctahedron, a parallel cube, connected by 6 square prisms, 12 triangular prisms, 8 triangular pyramids.[1]
