---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leguminous
offline_file: ""
offline_thumbnail: ""
uuid: 742a46d6-3d0a-40ae-b9e3-4b8b9ecc461e
updated: 1484310254
title: leguminous
categories:
    - Dictionary
---
leguminous
     adj : relating to or consisting of legumes
