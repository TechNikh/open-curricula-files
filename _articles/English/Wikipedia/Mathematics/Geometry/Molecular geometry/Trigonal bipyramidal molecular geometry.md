---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonal_bipyramidal_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: d8c29f13-9751-408a-a170-3b2030eae8e9
updated: 1484309282
title: Trigonal bipyramidal molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trigonal-bipyramidal-3D-balls.png
tags:
    - Axial and equatorial positions
    - Related geometries with lone pairs
    - Berry pseudorotation
categories:
    - Molecular geometry
---
In chemistry a trigonal bipyramid formation is a molecular geometry with one atom at the center and 5 more atoms at the corners of a triangular dipyramid. This is one geometry for which the bond angles surrounding the central atom atom are not identical (see also pentagonal dipyramid), because there is no geometrical arrangement with five terminal atoms in equivalent positions. Examples of this molecular geometry are phosphorus pentafluoride (PF5), and phosphorus pentachloride (PCl5) in the gas phase.[1]
