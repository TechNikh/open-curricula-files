---
version: 1
type: video
provider: YouTube
id: rSnwXZy3M28
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Chapter%2013%20Part%201%20-%20Types%20of%20RNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 83c31bfc-4792-4036-9cb4-c0302274d7a4
updated: 1486069672
title: 'Chapter 13 Part 1 - Types of RNA'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rSnwXZy3M28/default.jpg
    - https://i3.ytimg.com/vi/rSnwXZy3M28/1.jpg
    - https://i3.ytimg.com/vi/rSnwXZy3M28/2.jpg
    - https://i3.ytimg.com/vi/rSnwXZy3M28/3.jpg
---
