---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supper
offline_file: ""
offline_thumbnail: ""
uuid: 7fa365cd-d2b1-451e-817b-ff1f8ed79534
updated: 1484310148
title: supper
categories:
    - Dictionary
---
supper
     n 1: a light evening meal; served in early evening if dinner is
          at midday or served late in the evening at bedtime
     2: a social gathering where a light evening meal is served;
        "her suppers often included celebrities"
