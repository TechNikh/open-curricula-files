---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plate
offline_file: ""
offline_thumbnail: ""
uuid: 52b5fd67-230a-44d8-81e5-299790b95cc1
updated: 1484310204
title: plate
categories:
    - Dictionary
---
plate
     n 1: a sheet of metal or wood or glass or plastic
     2: (baseball) base consisting of a rubber slab where the batter
        stands; it must be touched by a base runner in order to
        score; "he ruled that the runner failed to touch home"
        [syn: {home plate}, {home base}, {home}]
     3: a full-page illustration (usually on slick paper)
     4: dish on which food is served or from which food is eaten
     5: the quantity contained in a plate [syn: {plateful}]
     6: a rigid layer of the Earth's crust that is believed to drift
        slowly [syn: {crustal plate}]
     7: the thin under portion of the forequarter
     8: a main course served on a plate; "a vegetable ...
