---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concept
offline_file: ""
offline_thumbnail: ""
uuid: bc1c744c-fc58-4d07-ab21-bb58d3e085e6
updated: 1484310361
title: concept
categories:
    - Dictionary
---
concept
     n : an abstract or general idea inferred or derived from
         specific instances [syn: {conception}, {construct}] [ant:
          {misconception}]
