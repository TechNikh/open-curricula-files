---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heron
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484338801
title: heron
categories:
    - Dictionary
---
Heron
     n 1: Greek mathematician and inventor who devised a way to
          determine the area of a triangle and who described
          various mechanical devices (first century) [syn: {Hero},
           {Hero of Alexandria}]
     2: gray or white wading bird with long neck and long legs and
        (usually) long bill
