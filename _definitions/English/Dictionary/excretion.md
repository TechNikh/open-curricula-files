---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excretion
offline_file: ""
offline_thumbnail: ""
uuid: 05ea047f-17b4-46d4-9f2d-aae562061596
updated: 1484310361
title: excretion
categories:
    - Dictionary
---
excretion
     n 1: the bodily process of discharging waste matter [syn: {elimination},
           {evacuation}, {excreting}, {voiding}]
     2: waste matter (as urine or sweat but especially feces)
        discharged from the body [syn: {body waste}, {excreta}, {excrement},
         {excretory product}]
