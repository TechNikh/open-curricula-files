---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/candidate
offline_file: ""
offline_thumbnail: ""
uuid: 08184aa5-c397-4667-ba11-c57758a08641
updated: 1484310601
title: candidate
categories:
    - Dictionary
---
candidate
     n 1: a politician who is running for public office [syn: {campaigner},
           {nominee}]
     2: someone who is considered for something (for an office or
        prize or honor etc.) [syn: {prospect}]
