---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enumerator_(computer_science)
offline_file: ""
offline_thumbnail: ""
uuid: 6f2c0272-bd87-4b4a-ab85-458fdc8806c4
updated: 1484309487
title: Enumerator (computer science)
categories:
    - Computability theory
---
An enumerator is a Turing machine that lists, possibly with repetitions, elements of some set S, which it is said to enumerate. A set enumerated by some enumerator is said to be recursively enumerable.
