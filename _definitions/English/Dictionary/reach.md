---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reach
offline_file: ""
offline_thumbnail: ""
uuid: 77cdcf55-a20c-473e-b2e1-af5fed8be453
updated: 1484310351
title: reach
categories:
    - Dictionary
---
reach
     n 1: the limits within which something can be effective; "range
          of motion"; "he was beyond the reach of their fire"
          [syn: {range}]
     2: an area in which something acts or operates or has power or
        control: "the range of a supersonic jet"; "the ambit of
        municipal legislation"; "within the compass of this
        article"; "within the scope of an investigation"; "outside
        the reach of the law"; "in the political orbit of a world
        power" [syn: {scope}, {range}, {orbit}, {compass}, {ambit}]
     3: the act of physically reaching or thrusting out [syn: {reaching},
         {stretch}]
     4: the limit of capability; "within the ...
