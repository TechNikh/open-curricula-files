---
version: 1
type: video
provider: YouTube
id: XwGNVzMQJaQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Medical%20animation%20of%20DNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a0316e66-abb2-455a-af61-6d49ca2c94f2
updated: 1486069672
title: Medical animation of DNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/XwGNVzMQJaQ/default.jpg
    - https://i3.ytimg.com/vi/XwGNVzMQJaQ/1.jpg
    - https://i3.ytimg.com/vi/XwGNVzMQJaQ/2.jpg
    - https://i3.ytimg.com/vi/XwGNVzMQJaQ/3.jpg
---
