---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gromov%27s_theorem_on_groups_of_polynomial_growth'
offline_file: ""
offline_thumbnail: ""
uuid: e0495a61-9ca1-416e-847e-6ec48b6426da
updated: 1484309266
title: "Gromov's theorem on groups of polynomial growth"
tags:
    - Statement
    - Growth rates of nilpotent groups
    - "Proofs of Gromov's theorem"
    - The gap conjecture
categories:
    - Metric geometry
---
In geometric group theory, Gromov's theorem on groups of polynomial growth, first proved by Mikhail Gromov,[1] characterizes finitely generated groups of polynomial growth, as those groups which have nilpotent subgroups of finite index.
