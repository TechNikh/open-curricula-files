---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exercising
offline_file: ""
offline_thumbnail: ""
uuid: 64a7a4cb-3a83-4291-af9d-fa384302ca8e
updated: 1484310603
title: exercising
categories:
    - Dictionary
---
exercising
     n : the activity of exerting your muscles in various ways to
         keep fit; "the doctor recommended regular exercise"; "he
         did some exercising"; "the physical exertion required by
         his work kept him fit" [syn: {exercise}, {physical
         exercise}, {physical exertion}, {workout}]
