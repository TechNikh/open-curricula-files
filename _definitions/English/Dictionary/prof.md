---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prof
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561101
title: prof
categories:
    - Dictionary
---
prof
     n : someone who is a member of the faculty at a college or
         university [syn: {professor}]
