---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_center
offline_file: ""
offline_thumbnail: ""
uuid: 6cb5ecf6-4d3c-495e-8393-ca9bfc684726
updated: 1484309443
title: Graph center
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graphcenter.svg.png
categories:
    - Graph theory objects
---
The center (or Jordan center[1]) of a graph is the set of all vertices of minimum eccentricity,[2] that is, the set of all vertices A where the greatest distance d(A,B) to other vertices B is minimal. Equivalently, it is the set of vertices with eccentricity equal to the graph's radius.[3] Thus vertices in the center (central points) minimize the maximal distance from other points in the graph.
