---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cauveri
offline_file: ""
offline_thumbnail: ""
uuid: 27f276f5-6ddc-4fbc-94d5-aec3d5cbc577
updated: 1484308438
title: Kaveri
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Karikaala_Chozan_Memorial_Building_%25282%2529_0.jpg'
tags:
    - River course
    - Tributaries
    - History
    - Irrigation
    - Water sharing
    - Significance in Hinduism
    - Prominent towns, religious sites
    - Veneration as a Devi
    - Gallery
categories:
    - Uncategorized
---
The Kaveri (or Cauvery in English) is a large Indian river. The origin of the river is at Talakaveri, Kodagu in Karnataka, flows generally south and east through Karnataka and Tamil Nadu and across the southern Deccan plateau through the southeastern lowlands, emptying into the Bay of Bengal through two principal mouths in Poompuhar, Tamil Nadu.
