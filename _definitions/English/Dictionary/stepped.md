---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stepped
offline_file: ""
offline_thumbnail: ""
uuid: 82bc6e62-1831-4631-ab5b-dded6c6e4cb4
updated: 1484310583
title: stepped
categories:
    - Dictionary
---
step
     n 1: any maneuver made as part of progress toward a goal; "the
          situation called for strong measures"; "the police took
          steps to reduce crime" [syn: {measure}]
     2: the distance covered by a step; "he stepped off ten paces
        from the old tree and began to dig" [syn: {footstep}, {pace},
         {stride}]
     3: the act of changing location by raising the foot and setting
        it down; "he walked with unsteady steps"
     4: support consisting of a place to rest the foot while
        ascending or descending a stairway; "he paused on the
        bottom step" [syn: {stair}]
     5: relative position in a graded series; "always a step
        behind"; ...
