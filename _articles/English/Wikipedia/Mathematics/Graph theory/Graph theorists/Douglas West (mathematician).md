---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Douglas_West_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: d07d8c3a-29da-409e-8a64-7968e534ff15
updated: 1484309479
title: Douglas West (mathematician)
tags:
    - Selected work
    - Books
    - Research work
categories:
    - Graph theorists
---
Douglas Brent West is a professor of graph theory at University of Illinois at Urbana-Champaign. He received his Ph.D. from Massachusetts Institute of Technology in 1978; his advisor was Daniel Kleitman.[1] He is the "W" in G. W. Peck, a pseudonym for a group of six mathematicians that includes West.[2] He is the editor of the journal Discrete Mathematics.
