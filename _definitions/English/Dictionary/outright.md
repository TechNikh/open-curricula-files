---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outright
offline_file: ""
offline_thumbnail: ""
uuid: 2b9c36e7-fc43-4e39-9a12-8089854f0101
updated: 1484310189
title: outright
categories:
    - Dictionary
---
outright
     adj : without reservation or exception [syn: {straight-out}, {unlimited}]
     adv 1: without restrictions or stipulations or further payments;
            "buy outright"
     2: without reservation or concealment; "she asked him outright
        for a divorce"
     3: without any delay; "he was killed outright" [syn: {instantaneously},
         {instantly}, {in a flash}]
