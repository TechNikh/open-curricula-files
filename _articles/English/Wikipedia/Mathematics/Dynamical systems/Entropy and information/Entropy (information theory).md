---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_(information_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 2d74e1f3-e4ff-4242-9593-d65aa83503d9
updated: 1484308767
title: Entropy (information theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Entropy_flip_2_coins.jpg
tags:
    - Introduction
    - Definition
    - Example
    - Rationale
    - Aspects
    - Relationship to thermodynamic entropy
    - Entropy as information content
    - Entropy as a measure of diversity
    - Data compression
    - "World's technological capacity to store and communicate information"
    - Limitations of entropy as information content
    - Limitations of entropy in cryptography
    - Data as a Markov process
    - b-ary entropy
    - Efficiency
    - Characterization
    - Continuity
    - Symmetry
    - Maximum
    - Additivity
    - Further properties
    - Extending discrete entropy to the continuous case
    - Differential entropy
    - Limiting Density of Discrete Points
    - Relative entropy
    - Use in combinatorics
    - Loomis–Whitney inequality
    - Approximation to binomial coefficient
    - Textbooks on information theory
categories:
    - Entropy and information
---
In information theory, systems are modeled by a transmitter, channel, and receiver. The transmitter produces messages that are sent through the channel. The channel modifies the message in some way. The receiver attempts to infer which message was sent. In this context, entropy (more specifically, Shannon entropy) is the expected value (average) of the information contained in each message. 'Messages' can be modeled by any flow of information.
