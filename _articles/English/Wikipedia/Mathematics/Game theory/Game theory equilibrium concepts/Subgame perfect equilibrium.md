---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subgame_perfect_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: aa2ae959-fe82-4867-b47b-4c695c4c1c23
updated: 1484309032
title: Subgame perfect equilibrium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tic_tac_toe.svg.png
tags:
    - Example
    - Finding subgame-perfect equilibria
categories:
    - Game theory equilibrium concepts
---
In game theory, a subgame perfect equilibrium (or subgame perfect Nash equilibrium) is a refinement of a Nash equilibrium used in dynamic games. A strategy profile is a subgame perfect equilibrium if it represents a Nash equilibrium of every subgame of the original game. Informally, this means that if (1) the players played any smaller game that consisted of only one part of the larger game and (2) their behavior represents a Nash equilibrium of that smaller game, then their behavior is a subgame perfect equilibrium of the larger game. Every finite extensive game has a subgame perfect equilibrium.[1]
