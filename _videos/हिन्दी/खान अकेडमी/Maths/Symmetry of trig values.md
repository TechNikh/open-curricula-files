---
version: 1
type: video
provider: YouTube
id: _zZPCuoiU4s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1136717f-c213-4541-98d8-2478ea376364
updated: 1486069611
title: Symmetry of trig values
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/_zZPCuoiU4s/default.jpg
    - https://i3.ytimg.com/vi/_zZPCuoiU4s/1.jpg
    - https://i3.ytimg.com/vi/_zZPCuoiU4s/2.jpg
    - https://i3.ytimg.com/vi/_zZPCuoiU4s/3.jpg
---
