---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_drawing
offline_file: ""
offline_thumbnail: ""
uuid: d66208a0-4491-4981-b08b-075103095914
updated: 1484309331
title: Structural drawing
categories:
    - Technical drawing
---
A structural drawing, a type of Engineering drawing, is a plan or set of plans for how a building or other structure will be built. Structural drawings are generally prepared by registered professional structural engineers, and informed by architectural drawings. They are primarily concerned with the load-carrying members of a structure. They outline the size and types of materials to be used, as well as the general demands for connections. They do not address architectural details like surface finishes, partition walls, or mechanical systems. The structural drawings communicate the design of the building's structure to the building authority to review. They are also become part of the ...
