---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tayfun_S%C3%B6nmez'
offline_file: ""
offline_thumbnail: ""
uuid: 67e92464-026b-46d6-9047-3d49d05e0d58
updated: 1484309029
title: Tayfun Sönmez
tags:
    - Work
    - Student Assignment and School Choice
    - Centralized Kidney Exchange
categories:
    - Game theorists
---
Tayfun Sönmez is a Turkish-American professor of economics at Boston College. He is a Fellow of the Econometric Society[1] and the 2008 winner of the Social Choice and Welfare Prize, which honors scholars under the age of 40 for excellent accomplishment in the area of social choice theory and welfare economics.[2] Sönmez has made significant contributions in the areas of microeconomic theory, mechanism/market design, and game theory. His work has been featured by the U.S. National Science Foundation for its practical relevance.[3]
