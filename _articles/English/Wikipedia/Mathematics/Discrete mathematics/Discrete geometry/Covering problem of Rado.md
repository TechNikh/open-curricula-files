---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_problem_of_Rado
offline_file: ""
offline_thumbnail: ""
uuid: 68e7dd1c-a276-4059-86ae-8540b02fe339
updated: 1484309511
title: Covering problem of Rado
categories:
    - Discrete geometry
---
The covering problem of Rado is an unsolved problem in geometry concerning covering planar sets by squares. It was formulated in 1928 by Tibor Radó and has been generalized to more general shapes and higher dimensions by Richard Rado.
