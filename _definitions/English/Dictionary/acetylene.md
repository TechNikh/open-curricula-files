---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acetylene
offline_file: ""
offline_thumbnail: ""
uuid: e84607dc-9d5b-4ff6-a8c4-aa36469f5490
updated: 1484310414
title: acetylene
categories:
    - Dictionary
---
acetylene
     n : a colorless flammable gas used chiefly in welding and in
         organic synthesis [syn: {ethyne}, {alkyne}]
