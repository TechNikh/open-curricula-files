---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enterprise_planning_system
offline_file: ""
offline_thumbnail: ""
uuid: cb016320-be50-49d7-935f-673b2e3f4b47
updated: 1484309065
title: Enterprise planning system
tags:
    - Purposes
    - Survival
    - Competition
    - Opportunities
    - Vulnerabilities
    - Strategic planning
    - Strategy via analysis
    - Strategy via geography
    - Strategy via projects integration
    - Planning and budgeting
    - Classifications
    - Group planning
    - Transition plan
    - Planning software
categories:
    - Strategy
---
These factors generally fall under PESTLE. PESTLE refers to political, economic, social, technological, legal and environmental factors. Regularly addressing PESTLE factors falls under operations management. Meanwhile, addressing any event, opportunity or challenge in any one or many factors for the first time will involve project management.
