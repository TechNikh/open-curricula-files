---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Friedman_translation
offline_file: ""
offline_thumbnail: ""
uuid: dacfa739-ba63-4935-89e7-ac603feeda62
updated: 1484309495
title: Friedman translation
tags:
    - Definition
    - Application
    - Notes
categories:
    - Proof theory
---
In mathematical logic, the Friedman translation is a certain transformation of intuitionistic formulas. Among other things it can be used to show that the Π02-theorems of various first-order theories of classical mathematics are also theorems of intuitionistic mathematics. It is named after its discoverer, Harvey Friedman.
