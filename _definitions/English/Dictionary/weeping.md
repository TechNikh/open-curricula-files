---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weeping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484622001
title: weeping
categories:
    - Dictionary
---
weeping
     adj : showing sorrow [syn: {dolorous}, {dolourous}, {lachrymose},
           {tearful}]
     n : the process of shedding tears (usually accompanied by sobs
         or other inarticulate sounds); "I hate to hear the crying
         of a child"; "she was in tears" [syn: {crying}, {tears}]
