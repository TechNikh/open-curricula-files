---
version: 1
type: video
provider: YouTube
id: ellMA8cZlu8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a0519213-3e95-491a-a179-2db2ee0fcc81
updated: 1486069618
title: Exponent properties
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ellMA8cZlu8/default.jpg
    - https://i3.ytimg.com/vi/ellMA8cZlu8/1.jpg
    - https://i3.ytimg.com/vi/ellMA8cZlu8/2.jpg
    - https://i3.ytimg.com/vi/ellMA8cZlu8/3.jpg
---
