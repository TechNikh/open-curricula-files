---
version: 1
type: video
provider: YouTube
id: vo9lUx3Yd98
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 62279819-973f-406e-9d72-0c8c872e3cc0
updated: 1486069639
title: 'Refraction - Physics Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/vo9lUx3Yd98/default.jpg
    - https://i3.ytimg.com/vi/vo9lUx3Yd98/1.jpg
    - https://i3.ytimg.com/vi/vo9lUx3Yd98/2.jpg
    - https://i3.ytimg.com/vi/vo9lUx3Yd98/3.jpg
---
