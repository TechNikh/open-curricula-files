---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_Grassmannian_(manifold)
offline_file: ""
offline_thumbnail: ""
uuid: 01242763-7909-4de5-85c0-2c63a6754012
updated: 1484309130
title: Affine Grassmannian (manifold)
categories:
    - Algebraic geometry
---
In mathematics, there are two distinct meanings of the term affine Grassmannian. In one it is the manifold of all k-dimensional affine subspaces of Rn (described on this page), while in the other the affine Grassmannian is a quotient of a group-ring based on formal Laurent series.
