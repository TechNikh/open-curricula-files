---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dialogue
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460721
title: dialogue
categories:
    - Dictionary
---
dialogue
     n 1: a conversation between two persons [syn: {dialog}, {duologue}]
     2: the lines spoken by characters in drama or fiction [syn: {dialog}]
     3: a literary composition in the form of a conversation between
        two people; "he has read Plato's Dialogues in the original
        Greek" [syn: {dialog}]
     4: a discussion intended to produce an agreement; "the buyout
        negotiation lasted several days"; "they disagreed but kept
        an open dialogue"; "talks between Israelis and
        Palestinians" [syn: {negotiation}, {talks}]
