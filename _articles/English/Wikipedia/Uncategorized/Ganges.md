---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ganga
offline_file: ""
offline_thumbnail: ""
uuid: 35d8ab11-b3cd-431d-875e-e37f442f448d
updated: 1484308419
title: Ganges
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Bhagirathi_River_at_Gangotri_0.JPG
tags:
    - Course
    - Geology
    - hydrology
    - History
    - Religious and cultural significance
    - Embodiment of sacredness
    - Avatarana or Descent of the Ganges
    - Redemption of the Dead
    - The purifying Ganges
    - Consort, Shakti, and Mother
    - Ganges in classical Indian iconography
    - Kumbh Mela
    - Irrigation
    - Canals
    - Dams and barrages
    - Economy
    - Tourism
    - Ecology and environment
    - Ganga river dolphin
    - Effects of climate change
    - Pollution and environmental concerns
    - Water shortages
    - mining
    - Notes
    - Sources
categories:
    - Uncategorized
---
The Ganges (/ˈɡændʒiːz/ GAN-jeez), also Ganga (Hindustani: [ˈɡəŋɡaː]) is a trans-boundary river of Asia which flows through the nations of India and Bangladesh. The 2,525 km (1,569 mi) river rises in the western Himalayas in the Indian state of Uttarakhand, and flows south and east through the Gangetic Plain of North India into Bangladesh, where it empties into the Bay of Bengal. It is the third largest river in the world by discharge.
