---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/movement
offline_file: ""
offline_thumbnail: ""
uuid: 3200ed0f-fb7b-440c-9f2d-0eab8c48637b
updated: 1484310341
title: movement
categories:
    - Dictionary
---
movement
     n 1: a change of position that does not entail a change of
          location; "the reflex motion of his eyebrows revealed
          his surprise"; "movement is a sign of life"; "an
          impatient move of his hand"; "gastrointestinal motility"
          [syn: {motion}, {move}, {motility}]
     2: a natural event that involves a change in the position or
        location of something [syn: {motion}]
     3: the act of changing location from one place to another;
        "police controlled the motion of the crowd"; "the movement
        of people from the farms to the cities"; "his move put him
        directly in my path" [syn: {motion}, {move}]
     4: a group of people ...
