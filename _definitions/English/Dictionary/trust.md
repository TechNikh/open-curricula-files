---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trust
offline_file: ""
offline_thumbnail: ""
uuid: 37d14683-9b63-4942-91f2-464c50932147
updated: 1484310593
title: trust
categories:
    - Dictionary
---
trust
     n 1: something (as property) held by one party (the trustee) for
          the benefit of another (the beneficiary); "he is the
          beneficiary of a generous trust set up by his father"
     2: certainty based on past experience; "he wrote the paper with
        considerable reliance on the work of other scientists";
        "he put more trust in his own two legs than in the gun"
        [syn: {reliance}]
     3: the trait of trusting; of believing in the honesty and
        reliability of others; "the experience destroyed his trust
        and personal dignity" [syn: {trustingness}, {trustfulness}]
        [ant: {distrust}]
     4: a consortium of independent organizations ...
