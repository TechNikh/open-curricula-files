---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lages
offline_file: ""
offline_thumbnail: ""
uuid: 25ac2ad5-bb96-4777-ac84-3139cd482740
updated: 1484308440
title: Lages
tags:
    - History
    - Gallery
categories:
    - Uncategorized
---
Lages, formerly Lajens, is a Brazilian municipality located in the central part of the state of Santa Catarina, in the region known in Portuguese as "Planalto Serrano".
