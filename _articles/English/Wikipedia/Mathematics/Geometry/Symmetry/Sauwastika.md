---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sauwastika
offline_file: ""
offline_thumbnail: ""
uuid: dc1cc411-7794-40c8-a8e7-1a45fc94e0ad
updated: 1484309315
title: Sauwastika
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/274px-Sauwastika_swastika_comparison.svg.png
tags:
    - Etymology
    - Claims of a distinction in Indian religions
    - Bibliography
categories:
    - Symmetry
---
The term sauwastika (or sauvastika[1][2]) (as a character: 卍) is sometimes used to distinguish the left-facing from the right-facing swastika symbol, a meaning which developed in 19th century scholarship.[3]
