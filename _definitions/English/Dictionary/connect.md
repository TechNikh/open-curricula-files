---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/connect
offline_file: ""
offline_thumbnail: ""
uuid: c1a38a6f-3f1f-464c-b59e-0c2df7b877f5
updated: 1484310275
title: connect
categories:
    - Dictionary
---
connect
     v 1: connect, fasten, or put together two or more pieces; "Can
          you connect the two loudspeakers?"; "Tie the ropes
          together"; "Link arms" [syn: {link}, {tie}, {link up}]
          [ant: {disconnect}]
     2: make a logical or causal connection; "I cannot connect these
        two pieces of evidence in my mind"; "colligate these
        facts"; "I cannot relate these events at all" [syn: {associate},
         {tie in}, {relate}, {link}, {colligate}, {link up}] [ant:
         {decouple}]
     3: be or become joined or united or linked; "The two streets
        connect to become a highway"; "Our paths joined"; "The
        travelers linked up again at the ...
