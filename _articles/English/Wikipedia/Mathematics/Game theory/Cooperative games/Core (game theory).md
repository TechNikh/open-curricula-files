---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Core_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 2efd54a0-4dcb-4a55-94b4-c0b056447c5c
updated: 1484308531
title: Core (game theory)
tags:
    - Origin
    - Definition
    - Properties
    - Example
    - 'Example 1: Miners'
    - 'Example 2: Gloves'
    - 'Example 3: Shoes'
    - The core in general equilibrium theory
    - The core in voting theory
categories:
    - Cooperative games
---
In game theory, the core is the set of feasible allocations that cannot be improved upon by a subset (a coalition) of the economy's consumers. A coalition is said to improve upon or block a feasible allocation if the members of that coalition are better off under another feasible allocation that is identical to the first except that every member of the coalition has a different consumption bundle that is part of an aggregate consumption bundle that can be constructed from publicly available technology and the initial endowments of each consumer in the coalition.
