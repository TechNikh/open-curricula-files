---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reverse_auction
offline_file: ""
offline_thumbnail: ""
uuid: eca585ba-3e59-4ad5-89f5-da0a3356e4a9
updated: 1484309013
title: Reverse auction
tags:
    - Context
    - History
    - Spectrum auction
    - Notes and references
categories:
    - Auctions
---
A reverse auction is a type of auction in which the roles of buyer and seller are reversed. In an ordinary auction (also known as a forward auction), buyers compete to obtain goods or services by offering increasingly higher prices. In a reverse auction, the sellers compete to obtain business from the buyer and prices will typically decrease as the sellers underbid each other.
