---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bulk_movement
offline_file: ""
offline_thumbnail: ""
uuid: 6c30baf6-1521-4803-ac91-42aa3274d50e
updated: 1484308361
title: Bulk movement
categories:
    - Uncategorized
---
In cell biology, bulk flow is the process by which proteins with a sorting signal travel to and from different cellular compartments. Proteins often have sorting signals, either transport signals or retention signals, specifying if a protein will translocate to another compartment within the cell or is retained in the current, membrane-bound, compartment in which it is already located respectively. For instance, proteins with the KDEL sorting signal are specified to return to the endoplasmic reticulum from the Golgi (see vesicular transport). However, proteins lacking a sorting signal will increase in concentration in a specific compartment until it reaches bulk concentration in the donor ...
