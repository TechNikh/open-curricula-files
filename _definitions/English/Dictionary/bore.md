---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bore
offline_file: ""
offline_thumbnail: ""
uuid: 8fb77455-b5e7-4e78-8ac5-49c45b6587c8
updated: 1484310264
title: bore
categories:
    - Dictionary
---
bear
     n 1: massive plantigrade carnivorous or omnivorous mammals with
          long shaggy coats and strong claws
     2: an investor with a pessimistic market outlook; an investor
        who expects prices to fall and so sells now in order to
        buy later at a lower price [ant: {bull}]
     v 1: have; "bear a resemblance"; "bear a signature"
     2: give birth (to a newborn); "My wife had twins yesterday!"
        [syn: {give birth}, {deliver}, {birth}, {have}]
     3: put up with something or somebody unpleasant; "I cannot bear
        his constant criticism"; "The new secretary had to endure
        a lot of unprofessional remarks"; "he learned to tolerate
        the heat"; ...
