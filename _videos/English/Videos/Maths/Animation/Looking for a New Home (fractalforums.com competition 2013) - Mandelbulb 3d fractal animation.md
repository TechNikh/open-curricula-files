---
version: 1
type: video
provider: YouTube
id: vJ6BYGQecRQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: faaa7e97-02c6-41ec-b378-47b873a38958
updated: 1486069644
title: 'Looking for a New Home (fractalforums.com competition 2013) - Mandelbulb 3d fractal animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vJ6BYGQecRQ/default.jpg
    - https://i3.ytimg.com/vi/vJ6BYGQecRQ/1.jpg
    - https://i3.ytimg.com/vi/vJ6BYGQecRQ/2.jpg
    - https://i3.ytimg.com/vi/vJ6BYGQecRQ/3.jpg
---
