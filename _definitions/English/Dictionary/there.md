---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/there
offline_file: ""
offline_thumbnail: ""
uuid: 3124314b-820e-4798-a2ae-22f27617b32f
updated: 1484310353
title: there
categories:
    - Dictionary
---
there
     n : a location other than here; that place; "you can take it
         from there" [ant: {here}]
     adv 1: in or at that place; "they have lived there for years";
            "it's not there"; "that man [who is] there" [syn: {at
            that place}, {in that location}] [ant: {here}]
     2: in that matter; "I agree with you there" [syn: {in that
        respect}, {on that point}]
     3: to or toward that place; away from the speaker; "go there
        around noon!" [syn: {thither}] [ant: {here}]
