---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exporter
offline_file: ""
offline_thumbnail: ""
uuid: 6e7af5ec-ba1f-4a8a-b7bb-385a5130cf24
updated: 1484310571
title: exporter
categories:
    - Dictionary
---
exporter
     n : a businessperson who transports goods abroad (for sale)
