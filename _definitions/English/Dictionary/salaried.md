---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salaried
offline_file: ""
offline_thumbnail: ""
uuid: fbfc0690-d08c-4427-8884-47f40e1a9e40
updated: 1484310469
title: salaried
categories:
    - Dictionary
---
salaried
     adj 1: receiving a salary; "salaried members of the staff" [ant: {freelance}]
     2: receiving or eligible for compensation; "salaried workers";
        "a stipendiary magistrate" [syn: {compensated}, {remunerated},
         {stipendiary}]
     3: for which money is paid; "a paying job"; "remunerative
        work"; "salaried employment"; "stipendiary services" [syn:
         {compensable}, {paying(a)}, {remunerative}, {stipendiary}]
