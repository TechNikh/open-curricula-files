---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sweetheart
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420701
title: sweetheart
categories:
    - Dictionary
---
sweetheart
     adj : privileged treatment of a favored person or corporation
           (sometimes unethically); "another sweetheart deal based
           on political influence"
     n 1: a person loved by another person [syn: {sweetie}, {steady},
          {truelove}]
     2: any well-liked individual; "he's a sweetheart"
     3: a very attractive or seductive looking woman [syn: {smasher},
         {stunner}, {knockout}, {beauty}, {ravisher}, {peach}, {lulu},
         {looker}, {mantrap}, {dish}]
