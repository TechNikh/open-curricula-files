---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heat_transfer_physics
offline_file: ""
offline_thumbnail: ""
uuid: d838daba-d0e8-439c-8047-9c4c63d3d4a2
updated: 1484308845
title: Heat transfer physics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Equilibrium_Particle_distribution_function.jpg
tags:
    - Introduction
    - Length and time scales
    - Phonon
    - electron
    - Fluid particle
    - Photon
categories:
    - Thermodynamics
---
Heat transfer physics describes the kinetics of energy storage, transport, and transformation by principal energy carriers: phonons (lattice vibration waves), electrons, fluid particles, and photons.[1][2][3][4][5] Heat is energy stored in temperature-dependent motion of particles including electrons, atomic nuclei, individual atoms, and molecules. Heat is transferred to and from matter by the principal energy carriers. The state of energy stored within matter, or transported by the carriers, is described by a combination of classical and quantum statistical mechanics. The energy is also transformed (converted) among various carriers. The heat transfer processes (or kinetics) are governed ...
