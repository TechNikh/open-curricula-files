---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sale
offline_file: ""
offline_thumbnail: ""
uuid: 40dc13e8-ce21-49d6-be32-fbc71da9c5db
updated: 1484310451
title: sale
categories:
    - Dictionary
---
sale
     n 1: the general activity of selling; "they tried to boost
          sales"; "laws limit the sale of handguns"
     2: a particular instance of selling; "he has just made his
        first sale"; "they had to complete the sale before the
        banks closed"
     3: the state of being purchasable; offered or exhibited for
        selling; "you'll find vitamin C for sale at most
        pharmacies"; "the new line of cars will soon be on sale"
     4: an occasion (usually brief) for buying at specially reduced
        prices; "they held a sale to reduce their inventory"; "I
        got some great bargains at their annual sale" [syn: {cut-rate
        sale}, {sales event}]
     5: ...
