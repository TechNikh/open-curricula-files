---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Composite_number
offline_file: ""
offline_thumbnail: ""
uuid: 5ca1ea5f-c546-48b2-90b8-a0bcd16424e5
updated: 1484308942
title: Composite number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Composite_number_Cuisenaire_rods_10_0.png
tags:
    - Types
    - Notes
categories:
    - Elementary number theory
---
A composite number is a positive integer that can be formed by multiplying together two smaller positive integers. Equivalently, it is a positive integer that has at least one divisor other than 1 and itself.[1][2] Every positive integer is composite, prime, or the unit 1, so the composite numbers are exactly the numbers that are not prime and not a unit.[3][4]
