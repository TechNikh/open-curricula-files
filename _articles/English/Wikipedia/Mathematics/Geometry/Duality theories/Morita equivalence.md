---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Morita_equivalence
offline_file: ""
offline_thumbnail: ""
uuid: 24ae862f-1fe2-4e7f-b005-5afbd234c18e
updated: 1484309234
title: Morita equivalence
tags:
    - Motivation
    - Definition
    - Examples
    - Criteria for equivalence
    - Properties preserved by equivalence
    - Further directions
    - Significance in K-theory
categories:
    - Duality theories
---
In abstract algebra, Morita equivalence is a relationship defined between rings that preserves many ring-theoretic properties. It is named after Japanese mathematician Kiiti Morita who defined equivalence and a similar notion of duality in 1958.
