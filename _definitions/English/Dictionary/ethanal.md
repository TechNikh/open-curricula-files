---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethanal
offline_file: ""
offline_thumbnail: ""
uuid: 016c22f2-3ae5-4d2d-b924-bde99bfbac38
updated: 1484310423
title: ethanal
categories:
    - Dictionary
---
ethanal
     n : a colorless volatile water-soluble liquid aldehyde used
         chiefly in manufacture of acetic acid and perfumes and
         drugs [syn: {acetaldehyde}]
