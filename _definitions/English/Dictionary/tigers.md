---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tigers
offline_file: ""
offline_thumbnail: ""
uuid: 14cf2032-7858-413d-a1e2-363f78f32e80
updated: 1484310286
title: tigers
categories:
    - Dictionary
---
Tigers
     n : a terrorist organization in Sri Lanka that began in 1970 as
         a student protest over the limited university access for
         Tamil students; currently seeks to establish an
         independent Tamil state called Eelam; relies on guerilla
         strategy including terrorist tactics that target key
         government and military personnel; "the Tamil Tigers
         perfected suicide bombing as a weapon of war" [syn: {Liberation
         Tigers of Tamil Eelam}, {LTTE}, {Tamil Tigers}, {World
         Tamil Association}, {World Tamil Movement}]
