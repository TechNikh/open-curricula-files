---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prior-free_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: f12e4584-2da8-406b-a1a4-338830719077
updated: 1484309046
title: Prior-free mechanism
tags:
    - Deterministic empirical distribution
    - Random sampling
    - Consensus estimates
categories:
    - Mechanism design
---
A prior-free mechanism (PFM) is a mechanism in which the designer does not have any information on the agents' valuations, not even that they are random variables from some unknown probability distribution.
