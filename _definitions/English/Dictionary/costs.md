---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/costs
offline_file: ""
offline_thumbnail: ""
uuid: dfde1f15-1e46-4068-a04a-4aa2c4585b2b
updated: 1484310258
title: costs
categories:
    - Dictionary
---
costs
     n : pecuniary reimbursement to the winning party for the
         expenses of litigation
