---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/validate
offline_file: ""
offline_thumbnail: ""
uuid: 81ca7271-694b-4aa6-8ae4-eca0d7826ae6
updated: 1484310218
title: validate
categories:
    - Dictionary
---
validate
     v 1: declare or make legally valid [syn: {formalize}, {formalise}]
          [ant: {invalidate}]
     2: prove valid; show or confirm the validity of something [ant:
         {invalidate}]
     3: give evidence for [syn: {corroborate}]
     4: make valid or confirm the validity of; "validate a ticket"
        [ant: {invalidate}]
