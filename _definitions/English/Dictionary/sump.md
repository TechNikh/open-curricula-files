---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sump
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484367961
title: sump
categories:
    - Dictionary
---
sump
     n 1: an oil reservoir in an internal combustion engine
     2: a well or other hole in which water has collected
     3: a covered cistern; waste water and sewage flow into it [syn:
         {cesspool}, {cesspit}, {sink}]
