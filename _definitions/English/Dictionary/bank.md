---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bank
offline_file: ""
offline_thumbnail: ""
uuid: d5272f65-844c-4677-92fb-514adea3c3c4
updated: 1484310445
title: bank
categories:
    - Dictionary
---
bank
     n 1: a financial institution that accepts deposits and channels
          the money into lending activities; "he cashed a check at
          the bank"; "that bank holds the mortgage on my home"
          [syn: {depository financial institution}, {banking
          concern}, {banking company}]
     2: sloping land (especially the slope beside a body of water);
        "they pulled the canoe up on the bank"; "he sat on the
        bank of the river and watched the currents"
     3: a supply or stock held in reserve for future use (especially
        in emergencies)
     4: a building in which commercial banking is transacted; "the
        bank is on the corner of Nassau and ...
