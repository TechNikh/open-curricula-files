---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kn%C3%B6del_number'
offline_file: ""
offline_thumbnail: ""
uuid: 546e38d4-05c0-459c-a8d4-936323018868
updated: 1484308649
title: Knödel number
categories:
    - Number theory
---
In number theory, an n-Knödel number for a given positive integer n is a composite number m with the property that each i < m coprime to m satisfies 
  
    
      
        
          i
          
            m
            −
            n
          
        
        ≡
        1
        
          
          (
          mod
          
          m
          )
        
      
    
    {\displaystyle i^{m-n}\equiv 1{\pmod {m}}}
  
. The concept is named after Walter Knödel.[1]
