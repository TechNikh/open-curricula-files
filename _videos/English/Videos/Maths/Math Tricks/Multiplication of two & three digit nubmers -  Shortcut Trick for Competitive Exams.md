---
version: 1
type: video
provider: YouTube
id: nNaVz3NohIU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83269436-2028-4a37-bacc-7a02e199e2aa
updated: 1486069621
title: 'Multiplication of two & three digit nubmers -  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/nNaVz3NohIU/default.jpg
    - https://i3.ytimg.com/vi/nNaVz3NohIU/1.jpg
    - https://i3.ytimg.com/vi/nNaVz3NohIU/2.jpg
    - https://i3.ytimg.com/vi/nNaVz3NohIU/3.jpg
---
