---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Asafoetida
offline_file: ""
offline_thumbnail: ""
uuid: d37499b1-1384-4e89-a9ff-744cb1efbc37
updated: 1484308355
title: Asafoetida
tags:
    - Uses
    - Cooking
    - Folk medicine
    - Other uses
    - History in the West
    - Cultivation and manufacture
    - Composition
    - Etymology
categories:
    - Uncategorized
---
Asafoetida /æsəˈfɛtᵻdə/[3] is the dried latex (gum oleoresin) exuded from the rhizome or tap root of several species of Ferula, a perennial herb that grows 1 to 1.5 m (3.3 to 4.9 ft) tall. The species is native to the deserts of Iran and mountains of Afghanistan and is mainly cultivated in nearby India.[4] As its name suggests, asafoetida has a fetid smell,[5] but in cooked dishes, it delivers a smooth flavour reminiscent of leeks.
