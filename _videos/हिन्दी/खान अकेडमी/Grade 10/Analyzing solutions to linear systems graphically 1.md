---
version: 1
type: video
provider: YouTube
id: I5nlfJ1lI9U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ee2746b2-8d8b-477e-8861-70b45b02f0db
updated: 1486069618
title: Analyzing solutions to linear systems graphically 1
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/I5nlfJ1lI9U/default.jpg
    - https://i3.ytimg.com/vi/I5nlfJ1lI9U/1.jpg
    - https://i3.ytimg.com/vi/I5nlfJ1lI9U/2.jpg
    - https://i3.ytimg.com/vi/I5nlfJ1lI9U/3.jpg
---
