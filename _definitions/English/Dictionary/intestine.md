---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intestine
offline_file: ""
offline_thumbnail: ""
uuid: 16b6c14a-1036-4ccd-9858-89259080fbe5
updated: 1484310327
title: intestine
categories:
    - Dictionary
---
intestine
     n : the part of the alimentary canal between the stomach and the
         anus [syn: {bowel}, {gut}]
