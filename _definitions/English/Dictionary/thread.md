---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thread
offline_file: ""
offline_thumbnail: ""
uuid: 66047eb6-2491-4102-90ea-83f9ff5aa180
updated: 1484310146
title: thread
categories:
    - Dictionary
---
thread
     n 1: a fine cord of twisted fibers (of cotton or silk or wool or
          nylon etc.) used in sewing and weaving [syn: {yarn}]
     2: any long object resembling a thin line; "a mere ribbon of
        land"; "the lighted ribbon of traffic"; "from the air the
        road was a gray thread"; "a thread of smoke climbed
        upward" [syn: {ribbon}]
     3: the connections that link the various parts of an event or
        argument together; "I couldn't follow his train of
        thought"; "he lost the thread of his argument" [syn: {train
        of thought}]
     4: the raised helical rib going around a screw [syn: {screw
        thread}]
     v 1: to move or cause to move in ...
