---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euclidean_distance
offline_file: ""
offline_thumbnail: ""
uuid: db5e6cc3-da04-4f50-a90a-77e8c49176be
updated: 1484309265
title: Euclidean distance
tags:
    - Definition
    - One dimension
    - Two dimensions
    - Three dimensions
    - n dimensions
    - Squared Euclidean distance
categories:
    - Metric geometry
---
In mathematics, the Euclidean distance or Euclidean metric is the "ordinary" (i.e. straight-line) distance between two points in Euclidean space. With this distance, Euclidean space becomes a metric space. The associated norm is called the Euclidean norm. Older literature refers to the metric as Pythagorean metric. A generalized term for the Euclidean norm is the L2 norm or L2 distance.
