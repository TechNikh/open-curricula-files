---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/picture
offline_file: ""
offline_thumbnail: ""
uuid: ad129e8f-bffa-4d19-826a-adaf32754eca
updated: 1484310284
title: picture
categories:
    - Dictionary
---
picture
     n 1: a visual representation (of an object or scene or person or
          abstraction) produced on a surface; "they showed us the
          pictures of their wedding"; "a movie is a series of
          images projected so rapidly that the eye integrates
          them" [syn: {image}, {icon}, {ikon}]
     2: graphic art consisting of an artistic composition made by
        applying paints to a surface; "a small painting by
        Picasso"; "he bought the painting as an investment"; "his
        pictures hang in the Louvre" [syn: {painting}]
     3: a clear and telling mental image; "he described his mental
        picture of his assailant"; "he had no clear picture of
        ...
