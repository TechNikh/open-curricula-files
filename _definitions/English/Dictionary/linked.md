---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/linked
offline_file: ""
offline_thumbnail: ""
uuid: bfd6cc96-adc6-4a5e-9fa7-4dde7088cf4a
updated: 1484310293
title: linked
categories:
    - Dictionary
---
linked
     adj : connected by a link, as railway cars or trailer trucks [syn:
            {coupled}, {joined}]
