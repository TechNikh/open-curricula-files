---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence'
offline_file: ""
offline_thumbnail: ""
uuid: 041fdd62-06ba-4616-90bd-7d5cee93912e
updated: 1484308771
title: Kullback–Leibler divergence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-KL-Gauss-Example.png
tags:
    - Definition
    - Characterization
    - Motivation
    - Properties
    - >
        Kullback–Leibler divergence for multivariate normal
        distributions
    - Relation to metrics
    - Fisher information metric
    - Relation to other quantities of information theory
    - Kullback–Leibler divergence and Bayesian updating
    - Bayesian experimental design
    - Discrimination information
    - Principle of minimum discrimination information
    - Relationship to available work
    - Quantum information theory
    - Relationship between models and reality
    - Symmetrised divergence
    - Relationship to other probability-distance measures
    - Data differencing
categories:
    - Entropy and information
---
In probability theory and information theory, the Kullback–Leibler divergence,[1][2] also called discrimination information (the name preferred by Kullback[3]), information divergence, information gain, relative entropy, KLIC, KL divergence, is a measure of the difference between two probability distributions P and Q. It is not symmetric in P and Q. In applications, P typically represents the "true" distribution of data, observations, or a precisely calculated theoretical distribution, while Q typically represents a theory, model, description, or approximation of P.
