---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frozen
offline_file: ""
offline_thumbnail: ""
uuid: cdca95c6-82df-4250-842b-10954d56de8f
updated: 1484310258
title: frozen
categories:
    - Dictionary
---
freeze
     n 1: the withdrawal of heat to change something from a liquid to
          a solid [syn: {freezing}]
     2: weather cold enough to cause freezing [syn: {frost}]
     3: an interruption or temporary suspension of progress or
        movement; "a halt in the arms race"; "a nuclear freeze"
        [syn: {halt}]
     4: fixing (of prices or wages etc) at a particular level; "a
        freeze on hiring"
     v 1: change to ice; "The water in the bowl froze" [ant: {boil}]
     2: stop moving or become immobilized; "When he saw the police
        car he froze" [syn: {stop dead}]
     3: be cold; "I could freeze to death in this office when the
        air conditioning is turned on"
   ...
