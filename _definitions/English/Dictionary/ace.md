---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ace
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484347621
title: ace
categories:
    - Dictionary
---
ace
     adj : of the highest quality; "an ace reporter"; "a crack shot";
           "a first-rate golfer"; "a super party"; "played
           top-notch tennis"; "an athlete in tiptop condition";
           "she is absolutely tops" [syn: {A-one}, {crack}, {first-rate},
            {super}, {tiptop}, {topnotch}, {tops(p)}]
     n 1: the smallest whole number or a numeral representing this
          number; "he has the one but will need a two and three to
          go with it"; "they had lunch at one" [syn: {one}, {1}, {I},
           {single}, {unity}]
     2: one of four playing cards in a deck having a single pip on
        its face
     3: someone who is dazzlingly skilled in any field ...
