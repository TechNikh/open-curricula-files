---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_group
offline_file: ""
offline_thumbnail: ""
uuid: e3b0c3b8-e8cc-4584-8385-d88101819876
updated: 1484308538
title: Arithmetic group
tags:
    - History
    - Definition and construction
    - Arithmetic groups
    - Using number fields
    - Examples
    - Arithmetic lattices in semisimple Lie groups
    - The Borel--Harish-Chandra theorem
    - Margulis arithmeticity theorem
    - Arithmetic Fuchsian and Kleinian groups
    - Classification
    - The congruence subgroup problem
    - |
        S
        
        
        {\displaystyle S}
        
        -arithmetic groups
    - Definition
    - Lattices in Lie groups over local fields
    - Some applications
    - Explicit expander graphs
    - Extremal surfaces and graphs
    - Isospectral manifolds
    - Fake projective planes
categories:
    - Number theory
---
In mathematics, an arithmetic group is a group obtained as the integer points of an algebraic group, for example 
  
    
      
        
          
            S
            L
          
          
            2
          
        
        (
        
          Z
        
        )
      
    
    {\displaystyle \mathrm {SL} _{2}(\mathbb {Z} )}
  
. They arise naturally in the study of arithmetic properties of quadratic forms and other classical topics in number theory. They also give rise to very interesting examples of Riemannian manifolds and hence are objects of interest in differential geometry and topology. Finally, these two topics join in the theory of automorphic forms which is ...
