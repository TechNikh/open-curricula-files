---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mere
offline_file: ""
offline_thumbnail: ""
uuid: a4e0a751-23e5-4446-8aab-e27cf0cc1356
updated: 1484310315
title: mere
categories:
    - Dictionary
---
mere
     adj 1: being nothing more than specified; "a mere child" [syn: {mere(a)}]
     2: apart from anything else; without additions or
        modifications; "only the bare facts"; "shocked by the mere
        idea"; "the simple passage of time was enough"; "the
        simple truth" [syn: {bare(a)}, {mere(a)}, {simple(a)}]
     n : a small pond of standing water
