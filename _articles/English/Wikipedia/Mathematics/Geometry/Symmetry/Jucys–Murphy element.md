---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jucys%E2%80%93Murphy_element'
offline_file: ""
offline_thumbnail: ""
uuid: 31af654e-51aa-4ed1-8cf6-b2e4ce69b00e
updated: 1484309311
title: Jucys–Murphy element
categories:
    - Symmetry
---
In mathematics, the Jucys–Murphy elements in the group algebra 
  
    
      
        
          C
        
        [
        
          S
          
            n
          
        
        ]
      
    
    {\displaystyle \mathbb {C} [S_{n}]}
  
 of the symmetric group, named after Algimantas Adolfas Jucys and G. E. Murphy, are defined as a sum of transpositions by the formula:
