---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_space
offline_file: ""
offline_thumbnail: ""
uuid: dfb11e2d-0491-46d1-b8cd-30777cc97150
updated: 1484309481
title: Covering space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Covering_space_diagram.svg.png
tags:
    - Formal definition
    - Alternative definitions
    - Examples
    - Properties
    - Common local properties
    - Homeomorphism of the fibres
    - Lifting properties
    - Equivalence
    - Covering of a manifold
    - Universal covers
    - G-coverings
    - Deck transformation group, regular covers
    - Monodromy action
    - More on the group structure
    - Relations with groupoids
    - Relations with classifying spaces and group cohomology
    - Generalizations
    - Applications
    - Notes
categories:
    - Topological graph theory
---
In mathematics, more specifically algebraic topology, a covering map (also covering projection) is a continuous function p [1] from a topological space, C, to a topological space, X, such that each point in X has an open neighbourhood evenly covered by p (as shown in the image); the precise definition is given below. In this case, C is called a covering space and X the base space of the covering projection. The definition implies that every covering map is a local homeomorphism.
