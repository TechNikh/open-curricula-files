---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mixture
offline_file: ""
offline_thumbnail: ""
uuid: a68ba0a5-345d-4f73-9180-d12b44cb03ff
updated: 1484310339
title: mixture
categories:
    - Dictionary
---
mixture
     n 1: (chemistry) a substance consisting of two or more substances
          mixed together (not in fixed proportions and not with
          chemical bonding)
     2: any foodstuff made by combining different ingredients; "he
        volunteered to taste her latest concoction"; "he drank a
        mixture of beer and lemonade" [syn: {concoction}, {intermixture}]
     3: a collection containing a variety of sorts of things; "a
        great assortment of cars was on display"; "he had a
        variety of disorders"; "a veritable smorgasbord of
        religions" [syn: {assortment}, {mixed bag}, {miscellany},
        {miscellanea}, {variety}, {salmagundi}, {smorgasbord}, ...
