---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fibrifold
offline_file: ""
offline_thumbnail: ""
uuid: b34759eb-8793-4410-bf0d-d5dff9b81e99
updated: 1484309307
title: Fibrifold
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-35_cubic_fibrifold_groups.png
categories:
    - Symmetry
---
In mathematics, a fibrifold is (roughly) a fiber space whose fibers and base spaces are orbifolds. They were introduced by John Horton Conway, Olaf Delgado Friedrichs, and Daniel H. Huson et al. (2001), who introduced a system of notation for 3-dimensional fibrifolds and used this to assign names to the 219 affine space group types. 184 of these are considered reducible, and 35 irreducible.
