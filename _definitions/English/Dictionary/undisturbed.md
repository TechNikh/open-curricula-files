---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undisturbed
offline_file: ""
offline_thumbnail: ""
uuid: 9d3b0863-7675-445f-a038-e685224edfdf
updated: 1484310373
title: undisturbed
categories:
    - Dictionary
---
undisturbed
     adj 1: untroubled by interference or disturbance; "food supplies
            were allowed through unmolested"; "he could pursue his
            studies undisturbed" [syn: {unmolested}]
     2: peaceful without disturbance; "the undisturbed tenor of
        their life"
