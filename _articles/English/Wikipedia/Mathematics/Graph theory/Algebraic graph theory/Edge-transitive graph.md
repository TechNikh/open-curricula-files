---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge-transitive_graph
offline_file: ""
offline_thumbnail: ""
uuid: 08d549bf-f7f3-4c71-bc17-10d7028ba85a
updated: 1484309373
title: Edge-transitive graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Gray_graph_2COL.svg.png
tags:
    - Examples and properties
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, an edge-transitive graph is a graph G such that, given any two edges e1 and e2 of G, there is an automorphism of G that maps e1 to e2.[1]
