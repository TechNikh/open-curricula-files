---
version: 1
type: video
provider: YouTube
id: Mj06h8BeeOA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a839190f-6f86-4504-8c88-67abe35f4885
updated: 1486069684
title: 'White Dwarfs & Planetary Nebulae- Crash Course Astronomy #30'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/Mj06h8BeeOA/default.jpg
    - https://i3.ytimg.com/vi/Mj06h8BeeOA/1.jpg
    - https://i3.ytimg.com/vi/Mj06h8BeeOA/2.jpg
    - https://i3.ytimg.com/vi/Mj06h8BeeOA/3.jpg
---
