---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bernoulli_scheme
offline_file: ""
offline_thumbnail: ""
uuid: 6afd5513-71a0-415f-a62e-762cbbfff9e6
updated: 1484308777
title: Bernoulli scheme
tags:
    - Definition
    - Generalizations
    - Properties
    - Bernoulli automorphism
categories:
    - Ergodic theory
---
In mathematics, the Bernoulli scheme or Bernoulli shift is a generalization of the Bernoulli process to more than two possible outcomes.[1][2] Bernoulli schemes are important in the study of dynamical systems, as most such systems (such as Axiom A systems) exhibit a repellor that is the product of the Cantor set and a smooth manifold, and the dynamics on the Cantor set are isomorphic to that of the Bernoulli shift.[3] This is essentially the Markov partition. The term shift is in reference to the shift operator, which may be used to study Bernoulli schemes. The Ornstein isomorphism theorem[4] shows that Bernoulli shifts are isomorphic when their entropy is equal.
