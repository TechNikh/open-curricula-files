---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collector
offline_file: ""
offline_thumbnail: ""
uuid: 131138e0-3536-4a3b-bd68-fb76bb228053
updated: 1484310168
title: collector
categories:
    - Dictionary
---
collector
     n 1: a person who collects things [syn: {aggregator}]
     2: a person who is employed to collect payments (as for rent or
        taxes) [syn: {gatherer}, {accumulator}]
     3: a crater that has collected cosmic material hitting the
        earth
     4: the electrode in a transistor through which a primary flow
        of carriers leaves the region between the electrodes
