---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cement
offline_file: ""
offline_thumbnail: ""
uuid: 184c4962-e60c-4a6d-81bb-6b80844f6fb9
updated: 1484310256
title: cement
categories:
    - Dictionary
---
cement
     n 1: concrete pavement is sometimes referred to as cement; "they
          stood on the gray cement beside the pool"
     2: a building material that is a powder made of a mixture of
        calcined limestone and clay; used with water and sand or
        gravel to make concrete and mortar
     3: something that hardens to act as adhesive material
     4: any of various materials used by dentists to fill cavities
        in teeth
     5: a specialized bony substance covering the root of a tooth
        [syn: {cementum}]
     v 1: make fast as if with cement; "We cemented our friendship"
     2: cover or coat with cement
     3: bind or join with or as if with cement
