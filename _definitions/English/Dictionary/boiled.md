---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boiled
offline_file: ""
offline_thumbnail: ""
uuid: 3633301b-776f-43f6-8a97-de4afd3e3d4d
updated: 1484310311
title: boiled
categories:
    - Dictionary
---
boiled
     adj : cooked in hot water [syn: {poached}, {stewed}]
