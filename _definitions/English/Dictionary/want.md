---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/want
offline_file: ""
offline_thumbnail: ""
uuid: 3ce366e3-474d-4b27-b9db-0ffbd6217ba1
updated: 1484310351
title: want
categories:
    - Dictionary
---
want
     n 1: a state of extreme poverty [syn: {privation}, {deprivation}]
     2: the state of needing something that is absent or
        unavailable; "there is a serious lack of insight into the
        problem"; "water is the critical deficiency in desert
        regions"; "for want of a nail the shoe was lost" [syn: {lack},
         {deficiency}]
     3: anything that is necessary but lacking; "he had sufficient
        means to meet his simple needs"; "I tried to supply his
        wants" [syn: {need}]
     4: a specific feeling of desire; "he got his wish"; "he was
        above all wishing and desire" [syn: {wish}, {wishing}]
     v 1: feel or have a desire for; want strongly; "I ...
