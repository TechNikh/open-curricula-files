---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pylorus
offline_file: ""
offline_thumbnail: ""
uuid: a092fe8e-a4bd-4022-b8c5-b2290386384c
updated: 1484310327
title: pylorus
categories:
    - Dictionary
---
pylorus
     n : a small circular opening between the stomach and the
         duodenum
     [also: {pylori} (pl)]
