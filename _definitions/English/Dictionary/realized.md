---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realized
offline_file: ""
offline_thumbnail: ""
uuid: 253de9f5-a752-4dfd-bd48-87625e6f3864
updated: 1484310539
title: realized
categories:
    - Dictionary
---
realized
     adj : successfully completed or brought to an end; "his mission
           accomplished he took a vacation"; "the completed
           project"; "the joy of a realized ambition overcame him"
           [syn: {accomplished}, {completed}, {realised}]
