---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/municipal
offline_file: ""
offline_thumbnail: ""
uuid: 4232ee83-72d1-4250-ae35-25b9abad4b68
updated: 1484310480
title: municipal
categories:
    - Dictionary
---
municipal
     adj 1: relating or belonging to or characteristic of a
            municipality; "municipal government"; "municipal
            bonds"; "a municipal park"; "municipal transportation"
     2: of or relating to the government of a municipality;
        "international law...only authorizes a belligerant to
        punish a spy under its municipal law"- J.L.kuntz
