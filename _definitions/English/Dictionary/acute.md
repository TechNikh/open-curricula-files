---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acute
offline_file: ""
offline_thumbnail: ""
uuid: 22faca1e-ad62-4772-b927-e2d814e4f7a1
updated: 1484310214
title: acute
categories:
    - Dictionary
---
acute
     adj 1: having or experiencing a rapid onset and short but severe
            course; "acute appendicitis"; "the acute phase of the
            illness"; "acute patients" [ant: {chronic}]
     2: extremely sharp or intense; "acute pain"; "felt acute
        annoyance"; "intense itching and burning" [syn: {intense}]
     3: having or demonstrating ability to recognize or draw fine
        distinctions; "an acute observer of politics and
        politicians"; "incisive comments"; "icy knifelike
        reasoning"; "as sharp and incisive as the stroke of a
        fang"; "penetrating insight"; "frequent penetrative
        observations" [syn: {discriminating}, {incisive}, {keen},
    ...
