---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/borrowing
offline_file: ""
offline_thumbnail: ""
uuid: c1e02931-2989-4638-a04c-883e753e39f6
updated: 1484310595
title: borrowing
categories:
    - Dictionary
---
borrowing
     n 1: the appropriation (of ideas or words etc) from another
          source; "the borrowing of ancient motifs was very
          apparent" [syn: {adoption}]
     2: obtaining funds from a lender
