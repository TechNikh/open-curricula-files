---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plumbing_drawing
offline_file: ""
offline_thumbnail: ""
uuid: 7a7031b9-60a7-43bd-86dc-cae64ebcd771
updated: 1484309331
title: Plumbing drawing
categories:
    - Technical drawing
---
A plumbing drawing, a type of technical drawing, shows the system of piping for fresh water going into the building and waste going out, both solid and liquid.
