---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threshing
offline_file: ""
offline_thumbnail: ""
uuid: 2ba7ce10-f1c7-416a-bd93-228437dbac23
updated: 1484310521
title: threshing
categories:
    - Dictionary
---
threshing
     n : the separation of grain or seeds from the husks and straw;
         "they used to do the threshing by hand but now there are
         machines to do it"
