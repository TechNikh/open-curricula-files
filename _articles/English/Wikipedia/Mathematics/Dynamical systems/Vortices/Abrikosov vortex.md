---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abrikosov_vortex
offline_file: ""
offline_thumbnail: ""
uuid: 219b12ca-5510-4018-96f1-7c96a61e8425
updated: 1484308879
title: Abrikosov vortex
categories:
    - Vortices
---
In superconductivity, an Abrikosov vortex is a vortex of supercurrent in a type-II superconductor theoretically predicted by Alexei Abrikosov in 1957.[1] The supercurrent circulates around the normal (i.e. non-superconducting) core of the vortex. The core has a size 
  
    
      
        ∼
        ξ
      
    
    {\displaystyle \sim \xi }
  
 — the superconducting coherence length (parameter of a Ginzburg-Landau theory). The supercurrents decay on the distance about 
  
    
      
        λ
      
    
    {\displaystyle \lambda }
  
 (London penetration depth) from the core. Note that in type-II superconductors 
  
    
      
        λ
        >
        ξ
        
          ...
