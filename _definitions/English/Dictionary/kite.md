---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484378641
title: kite
categories:
    - Dictionary
---
kite
     n 1: a bank check that has been fraudulently altered to increase
          its face value
     2: a bank check drawn on insufficient funds at another bank in
        order to take advantage of the float
     3: plaything consisting of a light frame covered with tissue
        paper; flown in wind at end of a string
     4: any of several small graceful hawks of the family
        Accipitridae having long pointed wings and feeding on
        insects and small animals
     v 1: increase the amount (of a check) fraudulently; "He kited
          many checks"
     2: get credit or money by using a bad check; "The businessman
        kited millions of dollars"
     3: soar or fly like a ...
