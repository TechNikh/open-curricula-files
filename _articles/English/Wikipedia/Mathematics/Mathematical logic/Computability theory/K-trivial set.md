---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-trivial_set
offline_file: ""
offline_thumbnail: ""
uuid: 0f922099-b5e0-4a59-920f-7f3b31fcdf3c
updated: 1484309487
title: K-trivial set
tags:
    - Definition
    - Brief history and development
    - Developments 1999–2008
    - Sketch of the construction of a non-computable K-trivial set
    - Equivalent characterizations
    - Lowness for K
    - Lowness for Martin-Löf-randomness
    - Base for Martin-Löf-randomness
    - Developments after 2008
categories:
    - Computability theory
---
In mathematics, a set of natural numbers is called a K-trivial set if its initial segments viewed as binary strings are easy to describe: the prefix-free Kolmogorov complexity is as low as possible, close to that of a computable set. Solovay proved in 1975 that a set can be K-trivial without being computable.
