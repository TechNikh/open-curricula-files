---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glacial
offline_file: ""
offline_thumbnail: ""
uuid: 79908090-d360-40dd-98bb-8a41f05c062e
updated: 1484310424
title: glacial
categories:
    - Dictionary
---
glacial
     adj 1: relating to or derived from a glacier; "glacial deposit"
     2: devoid of warmth and cordiality; expressive of
        unfriendliness or disdain; "a frigid greeting"; "got a
        frosty reception"; "a frozen look on their faces"; "a
        glacial handshake"; "icy stare"; "wintry smile" [syn: {frigid},
         {frosty}, {frozen}, {icy}, {wintry}]
     3: extremely cold; "an arctic climate";  "a frigid day"; "gelid
        waters of the North Atlantic"; "glacial winds"; "icy
        hands"; "polar weather" [syn: {arctic}, {frigid}, {gelid},
         {icy}, {polar}]
