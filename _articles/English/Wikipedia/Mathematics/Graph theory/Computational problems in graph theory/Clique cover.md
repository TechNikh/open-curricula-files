---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique_cover
offline_file: ""
offline_thumbnail: ""
uuid: abe2fe68-5687-4ab4-b3fa-cd825a6e03a1
updated: 1484309391
title: Clique cover
tags:
    - Relation to coloring
    - Computational complexity
    - In special classes of graphs
    - Related problems
categories:
    - Computational problems in graph theory
---
In graph theory, a clique cover or partition into cliques of a given undirected graph is a partition of the vertices of the graph into cliques, subsets of vertices within which every two vertices are adjacent. A minimum clique cover is a clique cover that uses as few cliques as possible. The minimum k for which a clique cover exists is called the clique cover number of the given graph.
