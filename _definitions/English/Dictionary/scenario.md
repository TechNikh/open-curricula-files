---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scenario
offline_file: ""
offline_thumbnail: ""
uuid: 4abc1b5f-2eb7-4d13-b5f0-078508e8d64f
updated: 1484310477
title: scenario
categories:
    - Dictionary
---
scenario
     n 1: an outline or synopsis of a play (or, by extension, of a
          literary work)
     2: a setting for a work of art or literature; "the scenario is
        France during the Reign of Terror"
     3: a postulated sequence of possible events; "planners
        developed several scenarios in case of an attack"
