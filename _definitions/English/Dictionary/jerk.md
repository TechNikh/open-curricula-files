---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jerk
offline_file: ""
offline_thumbnail: ""
uuid: 3650cdf8-ed3b-4c88-bb07-ec356c1b4244
updated: 1484310365
title: jerk
categories:
    - Dictionary
---
jerk
     n 1: a dull stupid fatuous person [syn: {dork}]
     2: an abrupt spasmodic movement [syn: {jerking}, {jolt}]
     3: (mechanics) the rate of change of velocity [syn: {rate of
        acceleration}]
     4: a sudden abrupt pull [syn: {tug}]
     v 1: pull, or move with a sudden movement; "He turned the handle
          and jerked the door open" [syn: {yank}]
     2: move with abrupt, seemingly uncontrolled motions; "The
        patient's legs were jerkings" [syn: {twitch}]
     3: make an uncontrolled, short, jerky motion; "his face is
        twitching" [syn: {twitch}]
     4: jump vertically, with legs stiff and back arched; "the yung
        filly bucked" [syn: {buck}, ...
