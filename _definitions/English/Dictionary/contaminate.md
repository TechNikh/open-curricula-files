---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contaminate
offline_file: ""
offline_thumbnail: ""
uuid: ce76d720-41e3-44b0-b21a-00a2ef804487
updated: 1484310581
title: contaminate
categories:
    - Dictionary
---
contaminate
     v 1: make impure; "The industrial wastes polluted the lake" [syn:
           {pollute}, {foul}]
     2: make radioactive by adding radioactive material; "Don't
        drink the water--it's contaminated" [ant: {decontaminate}]
