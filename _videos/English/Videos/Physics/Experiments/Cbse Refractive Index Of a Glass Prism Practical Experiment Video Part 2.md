---
version: 1
type: video
provider: YouTube
id: Az9RkOeNHQE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3890ccde-4336-46fc-8223-9ee855aa73df
updated: 1486069640
title: >
    Cbse Refractive Index Of a Glass Prism Practical Experiment
    Video Part 2
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Az9RkOeNHQE/default.jpg
    - https://i3.ytimg.com/vi/Az9RkOeNHQE/1.jpg
    - https://i3.ytimg.com/vi/Az9RkOeNHQE/2.jpg
    - https://i3.ytimg.com/vi/Az9RkOeNHQE/3.jpg
---
