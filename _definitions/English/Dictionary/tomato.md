---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tomato
offline_file: ""
offline_thumbnail: ""
uuid: 89cd8d63-9e41-488e-a512-f6d123679d33
updated: 1484310383
title: tomato
categories:
    - Dictionary
---
tomato
     n 1: mildly acid red or yellow pulpy fruit eaten as a vegetable
     2: native to South America; widely cultivated in many varieties
        [syn: {love apple}, {tomato plant}, {Lycopersicon
        esculentum}]
     [also: {tomatoes} (pl)]
