---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entanglement_(graph_measure)
offline_file: ""
offline_thumbnail: ""
uuid: 7d1361d5-b05f-4ac8-9e68-58ccdd6e11d2
updated: 1484309422
title: Entanglement (graph measure)
tags:
    - Definition
    - Properties and applications
categories:
    - Graph invariants
---
In graph theory, entanglement of a directed graph is a number measuring how strongly the cycles of the graph are intertwined. It is defined in terms of a mathematical game in which n cops try to capture a robber, who escapes along the edges of the graph. Similar to other graph measures, such as cycle rank, some algorithmic problems, e.g. parity game, can be efficiently solved on graphs of bounded entanglement.
