---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Widom_scaling
offline_file: ""
offline_thumbnail: ""
uuid: f81ae121-85fb-4f92-aa92-c6d2a9af4b93
updated: 1484308751
title: Widom scaling
categories:
    - Critical phenomena
---
Widom scaling (after Benjamin Widom) is a hypothesis in statistical mechanics regarding the free energy of a magnetic system near its critical point which leads to the critical exponents becoming no longer independent so that they can be parameterized in terms of two values. The hypothesis can be seen to arise as a natural consequence of the block-spin renormalization procedure, when the block size is chosen to be of the same size as the correlation length.[1]
