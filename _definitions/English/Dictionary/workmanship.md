---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/workmanship
offline_file: ""
offline_thumbnail: ""
uuid: 6e96b74a-e214-4830-9063-e3ed60fe0df5
updated: 1484310150
title: workmanship
categories:
    - Dictionary
---
workmanship
     n : skill in an occupation or trade [syn: {craft}, {craftsmanship}]
