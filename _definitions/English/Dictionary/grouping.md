---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grouping
offline_file: ""
offline_thumbnail: ""
uuid: e41f5fad-89b9-473b-8c69-e4c0848d7b92
updated: 1484310549
title: grouping
categories:
    - Dictionary
---
grouping
     n 1: any number of entities (members) considered as a unit [syn:
          {group}]
     2: the activity of putting things together in groups
     3: a system for classifying things into groups [syn: {pigeonholing}]
