---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: bb2a3661-1a63-4d5b-948d-eb620ebd196d
updated: 1484309423
title: Hopcroft–Karp algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HopcroftKarpExample.png
tags:
    - Augmenting paths
    - Algorithm
    - Analysis
    - Comparison with other bipartite matching algorithms
    - Non-bipartite graphs
    - Pseudocode
    - Explanation
    - Notes
categories:
    - Matching
---
In computer science, the Hopcroft–Karp algorithm is an algorithm that takes as input a bipartite graph and produces as output a maximum cardinality matching – a set of as many edges as possible with the property that no two edges share an endpoint. It runs in 
  
    
      
        O
        (
        
          |
        
        E
        
          |
        
        
          
            
              |
            
            V
            
              |
            
          
        
        )
      
    
    {\displaystyle O(|E|{\sqrt {|V|}})}
  
 time in the worst case, where 
  
    
      
        E
      
    
    {\displaystyle E}
  
 is set of edges in the graph, ...
