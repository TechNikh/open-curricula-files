---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Visakapatnam
offline_file: ""
offline_thumbnail: ""
uuid: 3ecef512-c54d-4f68-a411-1cb68c42dd25
updated: 1484308450
title: Visakhapatnam
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Visakhapatnam_Beach_Road_aerial_view_0.jpg
tags:
    - Etymology
    - geography
    - History
    - Buddhist influence
    - Pavurallakonda
    - Sankaram
    - Bavikonda
    - Thotlakonda
    - Later history
    - Climate
    - Demographics
    - language
    - Governance
    - Economy
    - Ports
    - Industrial sector
    - IT/ITES industry
    - Pharma
    - RESOURCES
    - Cityscape
    - Neighbourhoods
    - Landmarks
    - culture
    - Transport
    - Roadways
    - Railways
    - Airport
    - Seaports
    - pollution
    - Education
    - Primary education
    - Secondary education
    - Universities
    - Defence and research
    - Naval base
    - Research organisations
    - Sports
    - Media
    - FM stations in Vizag
    - Notable people
categories:
    - Uncategorized
---
Visakhapatnam (nicknamed Vizag) is the largest city, both in terms of area and population in the Indian state of Andhra Pradesh. It is located on the coast of Bay of Bengal in the north eastern region of the state. It is the administrative headquarters of Visakhapatnam district and also the Financial Capital of Andhra Pradesh.[4] As of 2011[update], the population of the city was recorded as 2,035,922, making it the 14th largest city in the country.[1] The Visakhapatnam Metropolitan Region is the 9th most populous in India with a population of 5,340,000[5]
