---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/medical
offline_file: ""
offline_thumbnail: ""
uuid: 428e388d-aeec-4cf5-8bf5-4f32d0eb1553
updated: 1484310313
title: medical
categories:
    - Dictionary
---
medical
     adj 1: relating to the study or practice of medicine; "the medical
            profession"; "a medical student"; "medical school"
     2: requiring or amenable to treatment by medicine especially as
        opposed to surgery; "medical treatment"; "pheumonia is a
        medical disease" [ant: {surgical}]
     3: of or belonging to Aesculapius or the healing art [syn: {aesculapian}]
     n : a thorough physical examination; includes a variety of tests
         depending on the age and sex and health of the person
         [syn: {checkup}, {medical checkup}, {medical examination},
          {medical exam}, {health check}]
