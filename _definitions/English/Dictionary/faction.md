---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faction
offline_file: ""
offline_thumbnail: ""
uuid: 02435b14-a737-448d-8b6a-cdd297ad2636
updated: 1484310607
title: faction
categories:
    - Dictionary
---
faction
     n 1: a clique (often secret) that seeks power usually through
          intrigue [syn: {cabal}, {junto}, {camarilla}]
     2: a dissenting clique [syn: {sect}]
