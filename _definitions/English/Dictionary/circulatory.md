---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circulatory
offline_file: ""
offline_thumbnail: ""
uuid: a30d6cb4-9a09-49cf-a857-1078107249c6
updated: 1484310315
title: circulatory
categories:
    - Dictionary
---
circulatory
     adj 1: of or relating to circulation [syn: {circulative}]
     2: relating to circulatory system or to circulation of the
        blood
