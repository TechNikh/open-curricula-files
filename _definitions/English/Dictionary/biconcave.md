---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biconcave
offline_file: ""
offline_thumbnail: ""
uuid: 1d7073ac-7dbf-44a2-8e4c-bb09a0e675bf
updated: 1484310208
title: biconcave
categories:
    - Dictionary
---
biconcave
     adj : concave on both sides [syn: {concavo-concave}]
