---
version: 1
type: video
provider: YouTube
id: r9NNP_wvY7w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Metals%20and%20Non-Metals-Class-8th%2C%20VIII%20Science%20Animated%20Video%20Notes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 17061f72-b0e7-4d3b-8cdb-1fbffd5457e3
updated: 1486069662
title: >
    Metals and Non-Metals-Class-8th, VIII Science Animated Video
    Notes
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/r9NNP_wvY7w/default.jpg
    - https://i3.ytimg.com/vi/r9NNP_wvY7w/1.jpg
    - https://i3.ytimg.com/vi/r9NNP_wvY7w/2.jpg
    - https://i3.ytimg.com/vi/r9NNP_wvY7w/3.jpg
---
