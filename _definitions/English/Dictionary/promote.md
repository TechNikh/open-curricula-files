---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promote
offline_file: ""
offline_thumbnail: ""
uuid: 8160637f-70f9-4d16-b700-0445b3cf7775
updated: 1484310413
title: promote
categories:
    - Dictionary
---
promote
     v 1: contribute to the progress or growth of; "I am promoting the
          use of computers in the classroom" [syn: {advance}, {boost},
           {further}, {encourage}]
     2: give a promotion to or assign to a higher position; "John
        was kicked upstairs when a replacement was hired"; "Women
        tend not to advance in the major law firms"; "I got
        promoted after many years of hard work" [syn: {upgrade}, {advance},
         {kick upstairs}, {raise}, {elevate}] [ant: {demote}]
     3: make publicity for; try to sell (a product); "The salesman
        is aggressively pushing the new computer model"; "The
        company is heavily advertizing their new ...
