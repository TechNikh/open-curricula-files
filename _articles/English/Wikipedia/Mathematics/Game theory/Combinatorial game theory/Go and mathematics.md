---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Go_and_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 5192bfc1-e75c-4fbc-b40a-88f611c78870
updated: 1484308533
title: Go and mathematics
tags:
    - Computational complexity
    - Without Ko
    - Japanese Ko Rule
    - Superko rule
    - Complexity of certain Go configurations
    - Legal positions
    - Game tree complexity
    - Notes
categories:
    - Combinatorial game theory
---
The game of Go is one of the most popular games in the world. As a result of its elegant and simple rules, the game has long been an inspiration for mathematical research. Chinese scholars of the 11th century already published work on permutations based on the go board.[citation needed] In more recent years, research of the game by John H. Conway led to the invention of the surreal numbers and contributed to development of combinatorial game theory (with Go Infinitesimals[1] being a specific example of its use in Go).
