---
version: 1
type: video
provider: YouTube
id: jiWxU3jXOFc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c2bae523-7157-4b6f-a69e-b73a2e01b0eb
updated: 1486069660
title: 'The Kinetic King Detonates a Guinness World-Record Stick Bomb -- 2250 Sticks!'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/jiWxU3jXOFc/default.jpg
    - https://i3.ytimg.com/vi/jiWxU3jXOFc/1.jpg
    - https://i3.ytimg.com/vi/jiWxU3jXOFc/2.jpg
    - https://i3.ytimg.com/vi/jiWxU3jXOFc/3.jpg
---
