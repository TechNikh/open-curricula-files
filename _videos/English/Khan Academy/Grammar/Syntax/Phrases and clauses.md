---
version: 1
type: video
provider: YouTube
id: 49EsnvxVQec
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Phrases%20and%20clauses.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d0a86296-6812-4ba5-83f0-50fc72bdeb59
updated: 1486069597
title: Phrases and clauses
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/49EsnvxVQec/default.jpg
    - https://i3.ytimg.com/vi/49EsnvxVQec/1.jpg
    - https://i3.ytimg.com/vi/49EsnvxVQec/2.jpg
    - https://i3.ytimg.com/vi/49EsnvxVQec/3.jpg
---
A phrase is any collection of words that behaves like a part of speech, like a noun phrase (“my brother Stu”), an adjectival phrase (“in a different shade of blue”), or an adverbial phrase (“with elegance and tact”). A clause is any noun phrase plus a verb; they can be sentences, but they don’t always have to be. You’ll see!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/introduction-to-phrases-and-clauses 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/dependent-and-independent-clauses-syntax-khan-academy 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/dangling-modifiers-sentences-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
