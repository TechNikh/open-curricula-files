---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vijayawada
offline_file: ""
offline_thumbnail: ""
uuid: 9a8ffacf-ca78-489b-9ee5-89bcecea6127
updated: 1484308451
title: Vijayawada
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Exterior_View_of_Mall.jpg
tags:
    - Toponomy
    - History
    - geography
    - Climate
    - Demographics
    - Governance
    - Civic administration
    - Law and order
    - Economy
    - culture
    - Cityscape
    - Transport
    - Road
    - Rail
    - air
    - Education
    - Media
    - Sports
categories:
    - Uncategorized
---
Vijayawada is a city on the banks of the Krishna River, in the Indian state of Andhra Pradesh. It is a municipal corporation and the headquarters of Vijayawada (urban) mandal in Krishna district of the state. The city forms a part of Andhra Pradesh Capital Region and the headquarters of Andhra Pradesh Capital Region Development Authority is located in the city.[7] The city is one of the major trading and business centres of the state and hence, it is also known as "The Business Capital of Andhra Pradesh".[8][9] The city is one of the two metropolis in the state, with the other being Visakhapatnam.
