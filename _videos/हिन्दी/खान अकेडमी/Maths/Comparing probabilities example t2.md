---
version: 1
type: video
provider: YouTube
id: atA4xlQvSPU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8fa0b487-2bf1-4c2a-9169-b27b48936b4c
updated: 1486069609
title: Comparing probabilities example t2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/atA4xlQvSPU/default.jpg
    - https://i3.ytimg.com/vi/atA4xlQvSPU/1.jpg
    - https://i3.ytimg.com/vi/atA4xlQvSPU/2.jpg
    - https://i3.ytimg.com/vi/atA4xlQvSPU/3.jpg
---
