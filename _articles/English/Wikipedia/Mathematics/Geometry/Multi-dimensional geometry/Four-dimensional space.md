---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Four-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: 5f8d7406-22d6-46ba-b5c2-6ca546c74bc3
updated: 1484309285
title: Four-dimensional space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/8-cell-simple.gif
tags:
    - History
    - Vectors
    - Orthogonality and vocabulary
    - Geometry
    - Hypersphere
    - Cognition
    - Dimensional analogy
    - Cross-sections
    - Projections
    - Shadows
    - Bounding volumes
    - Visual scope
    - Limitations
categories:
    - Multi-dimensional geometry
---
In mathematics, four-dimensional space ("4D") is a geometric space with four dimensions. It typically is more specifically four-dimensional Euclidean space, generalizing the rules of three-dimensional Euclidean space. It has been studied by mathematicians and philosophers for over two centuries, both for its own interest and for the insights it offered into mathematics and related fields.
