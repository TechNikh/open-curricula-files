---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underestimate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443141
title: underestimate
categories:
    - Dictionary
---
underestimate
     n : an estimation that is too low; an estimate that is less than
         the true or actual value [syn: {underestimation}, {underrating},
          {underreckoning}]
     v 1: assign too low a value to; "Don't underestimate the value of
          this heirlooom-you may sell it at a good price" [syn: {undervalue}]
          [ant: {overvalue}, {overvalue}]
     2: make a deliberately low estimate; "The construction company
        wanted the contract badly and lowballed" [syn: {lowball}]
     3: make too low an estimate of; "he underestimated the work
        that went into the renovation"; "Don't underestimate the
        danger of such a raft trip on this river" [syn: ...
