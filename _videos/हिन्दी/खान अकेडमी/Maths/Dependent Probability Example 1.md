---
version: 1
type: video
provider: YouTube
id: u1sTAZBp7tQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d53c9fdc-196b-43b4-9d94-923fe97d7302
updated: 1486069616
title: Dependent Probability Example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/u1sTAZBp7tQ/default.jpg
    - https://i3.ytimg.com/vi/u1sTAZBp7tQ/1.jpg
    - https://i3.ytimg.com/vi/u1sTAZBp7tQ/2.jpg
    - https://i3.ytimg.com/vi/u1sTAZBp7tQ/3.jpg
---
