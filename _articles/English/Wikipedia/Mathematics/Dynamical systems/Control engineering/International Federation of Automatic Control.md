---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Federation_of_Automatic_Control
offline_file: ""
offline_thumbnail: ""
uuid: ed66f03e-1a72-422d-8153-38bf91ffd676
updated: 1484308739
title: International Federation of Automatic Control
categories:
    - Control engineering
---
The International Federation of Automatic Control (IFAC), founded in September 1957, is a multinational federation of 52 national member organizations (NMO), each one representing the engineering and scientific societies concerned with automatic control in its own country.
