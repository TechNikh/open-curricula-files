---
version: 1
type: article
id: https://en.wikipedia.org/wiki/88_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 25bca1c8-2761-4fc1-806c-a7fe72359b77
updated: 1484308967
title: 88 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-1956_Oldsmobile_88_sedan.jpg
tags:
    - In mathematics
    - In science
    - Cultural significance
    - In Chinese culture
    - In amateur radio
    - In white nationalism
    - In sports
    - In other fields
categories:
    - Integers
---
