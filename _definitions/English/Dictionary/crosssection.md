---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crosssection
offline_file: ""
offline_thumbnail: ""
uuid: 306f9fce-74bf-46cf-8924-dae717ec10ab
updated: 1484310200
title: crosssection
categories:
    - Dictionary
---
cross section
     n 1: a section created by a plane cutting a solid perpendicular
          to its longest axis
     2: a sample meant to be representative of a whole population
     3: (physics) the probability that a particular interaction (as
        capture or ionization) will take place between particles;
        measured in barns
