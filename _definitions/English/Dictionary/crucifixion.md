---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crucifixion
offline_file: ""
offline_thumbnail: ""
uuid: 56b781fb-a77f-4b96-9b47-1ece87e5fafd
updated: 1484310178
title: crucifixion
categories:
    - Dictionary
---
crucifixion
     n 1: the act of executing by a method widespread in the ancient
          world; the victim's hands and feet are bound or nailed
          to a cross
     2: the death of Jesus on the cross
     3: the infliction of extremely painful punishment or suffering
        [syn: {excruciation}]
