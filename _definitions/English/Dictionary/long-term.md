---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/long-term
offline_file: ""
offline_thumbnail: ""
uuid: 74a95064-d051-483f-aa17-775230415493
updated: 1484310464
title: long-term
categories:
    - Dictionary
---
long-term
     adj : relating to or extending over a relatively long time; "the
           long-run significance of the elections"; "the long-term
           reconstruction of countries damaged by the war"; "a
           long-term investment" [syn: {long-run}, {semipermanent}]
