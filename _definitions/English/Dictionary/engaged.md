---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engaged
offline_file: ""
offline_thumbnail: ""
uuid: b119647f-4534-43c2-b38e-cbeb77dc0695
updated: 1484310443
title: engaged
categories:
    - Dictionary
---
engaged
     adj 1: having ones attention or mind or energy engaged; "she keeps
            herself fully occupied with volunteer activities";
            "deeply engaged in conversation" [syn: {occupied}]
     2: involved in military hostilities; "the desperately engaged
        ships continued the fight"
     3: reserved in advance [syn: {booked}, {set-aside(p)}]
     4: (of facilities such as telephones or lavatories) unavailable
        for use by anyone else or indicating unavailability;
        (`engaged' is a British term for a busy telephone line);
        "her line is busy"; "receptionists' telephones are always
        engaged"; "the lavatory is in use"; "kept getting a busy
      ...
