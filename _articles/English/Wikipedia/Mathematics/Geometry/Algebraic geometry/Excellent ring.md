---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Excellent_ring
offline_file: ""
offline_thumbnail: ""
uuid: a5b777e6-1469-4d5a-8ac6-19c482d651af
updated: 1484309147
title: Excellent ring
tags:
    - Definitions
    - Examples
    - Excellent rings
    - A J-2 ring that is not a G-ring
    - A G-ring that is not a J-2 ring
    - A quasi-excellent ring that is not excellent
    - Properties
    - Resolution of singularities
categories:
    - Algebraic geometry
---
In commutative algebra, a quasi-excellent ring is a Noetherian commutative ring that behaves well with respect to the operation of completion, and is called an excellent ring if it is also universally catenary. Excellent rings are one answer to the problem of finding a natural class of "well-behaved" rings containing most of the rings that occur in number theory and algebraic geometry. At one time it seemed that the class of Noetherian rings might be an answer to this problem, but Nagata and others found several strange counterexamples showing that in general Noetherian rings need not be well behaved: for example, a normal Noetherian local ring need not be analytically normal. The class of ...
