---
version: 1
type: video
provider: YouTube
id: WGy_srrdEn4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 73c8218a-c2a6-420a-96b0-f0db0487b6c0
updated: 1486069636
title: Reflection of Light Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/WGy_srrdEn4/default.jpg
    - https://i3.ytimg.com/vi/WGy_srrdEn4/1.jpg
    - https://i3.ytimg.com/vi/WGy_srrdEn4/2.jpg
    - https://i3.ytimg.com/vi/WGy_srrdEn4/3.jpg
---
