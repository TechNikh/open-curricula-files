---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atm
offline_file: ""
offline_thumbnail: ""
uuid: 2dc3a25a-9ce4-4093-b6b2-8eac503cc5a3
updated: 1484310224
title: atm
categories:
    - Dictionary
---
atm
     n 1: a unit of pressure: the pressure that will support a column
          of mercury 760 mm high at sea level and 0 degrees
          centigrade [syn: {standard atmosphere}, {atmosphere}, {standard
          pressure}]
     2: a means of digital communications that is capable of very
        high speeds; suitable for transmission of images or voice
        or video as well as data; "ATM is used for both LAN and
        WAN" [syn: {asynchronous transfer mode}]
     3: an unattended machine (outside some banks) that dispenses
        money when a personal coded card is used [syn: {cash
        machine}, {cash dispenser}, {automated teller machine}, {automatic
        teller ...
