---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Brown_Gardner
offline_file: ""
offline_thumbnail: ""
uuid: 024e002d-4b58-4ba9-b488-993b9a950c0e
updated: 1484309078
title: Robert Brown Gardner
tags:
    - Biography
    - Legacy
    - Selected publications
categories:
    - Geometers
---
Robert Brown (Robby) Gardner (February 27, 1939 – May 5, 1998) was an American mathematician who worked on differential geometry, a field in which he obtained several novel results.[1] He was the author and co-author of three influential books, produced more than fifty papers,[2] eighteen masters students and thirteen Ph.D students.[3][4] His 1991 book, Theory of Exterior Differential Systems is now considered the standard reference for the subject.[1] Robert Bryant, Duke University's Professor of Mathematics and the president of the American Mathematical Society (2015-2017) was a student of his.[5]
