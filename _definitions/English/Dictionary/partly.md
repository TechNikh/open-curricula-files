---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partly
offline_file: ""
offline_thumbnail: ""
uuid: 4c46ad2b-44fe-4821-92ba-3992f008fd96
updated: 1484310327
title: partly
categories:
    - Dictionary
---
partly
     adv : in part; in some degree; not wholly; "I felt partly to
           blame"; "He was partially paralyzed" [syn: {partially},
            {part}] [ant: {wholly}]
