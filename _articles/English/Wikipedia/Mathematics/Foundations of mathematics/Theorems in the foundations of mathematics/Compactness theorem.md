---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Compactness_theorem
offline_file: ""
offline_thumbnail: ""
uuid: f10aeb63-fdf6-4d33-a01d-104b523cd05a
updated: 1484308992
title: Compactness theorem
tags:
    - History
    - Applications
    - Proofs
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the compactness theorem states that a set of first-order sentences has a model if and only if every finite subset of it has a model. This theorem is an important tool in model theory, as it provides a useful method for constructing models of any set of sentences that is finitely consistent.
