---
version: 1
type: video
provider: YouTube
id: 8ytmjeeqzGE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f14cf453-0b0d-4a5b-85c2-45abbd828433
updated: 1486069669
title: 'Voyager Biomedical, Inc. - ARK Technology - 3D Medical Animation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8ytmjeeqzGE/default.jpg
    - https://i3.ytimg.com/vi/8ytmjeeqzGE/1.jpg
    - https://i3.ytimg.com/vi/8ytmjeeqzGE/2.jpg
    - https://i3.ytimg.com/vi/8ytmjeeqzGE/3.jpg
---
