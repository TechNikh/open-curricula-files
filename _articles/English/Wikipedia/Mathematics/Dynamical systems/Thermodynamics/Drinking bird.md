---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drinking_bird
offline_file: ""
offline_thumbnail: ""
uuid: 12263d05-b9f3-4693-ae2e-ee901c96c8d5
updated: 1484308843
title: Drinking bird
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/239px-Goblet_Glass_%2528Banquet%2529.svg.png'
tags:
    - Construction and materials
    - Steps
    - Alternative operation
    - Physical and chemical principles
    - History
    - Notable usage in popular culture
categories:
    - Thermodynamics
---
Drinking birds, also known as insatiable birdies or dipping birds,[1][2][3] are toy heat engines that mimic the motions of a bird drinking from a water source. They are sometimes incorrectly considered examples of a perpetual motion device.
