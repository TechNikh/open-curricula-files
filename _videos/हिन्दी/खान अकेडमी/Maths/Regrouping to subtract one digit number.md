---
version: 1
type: video
provider: YouTube
id: pebfasHW6N8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bc9efb16-c706-4166-b7ea-51c98ecc9a3f
updated: 1486069609
title: Regrouping to subtract one digit number
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/pebfasHW6N8/default.jpg
    - https://i3.ytimg.com/vi/pebfasHW6N8/1.jpg
    - https://i3.ytimg.com/vi/pebfasHW6N8/2.jpg
    - https://i3.ytimg.com/vi/pebfasHW6N8/3.jpg
---
