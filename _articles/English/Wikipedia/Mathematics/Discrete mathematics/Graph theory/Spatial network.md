---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spatial_network
offline_file: ""
offline_thumbnail: ""
uuid: 5f376f51-1c70-4a57-a03f-5160dfa94972
updated: 1484308625
title: Spatial network
tags:
    - Examples
    - Characterizing spatial networks
    - Lattice networks
    - Probability and spatial networks
    - Approach from the theory of space syntax
    - History
categories:
    - Graph theory
---
A spatial network (sometimes also geometric graph) is a graph in which the vertices or edges are spatial elements associated with geometric objects, i.e. the nodes are located in a space equipped with a certain metric.[1] The simplest mathematical realization is a lattice or a random geometric graph, where nodes are distributed uniformly at random over a two-dimensional plane; a pair of nodes are connected if the Euclidean distance is smaller than a given neighborhood radius. Transportation and mobility networks, Internet, mobile phone networks, power grids, social and contact networks and neural networks are all examples where the underlying space is relevant and where the graph's topology ...
