---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Insulin_oscillation
offline_file: ""
offline_thumbnail: ""
uuid: dcee93f1-fcd7-47b9-9b18-aa62effc1e99
updated: 1484308814
title: Insulin oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pancreas_insulin_oscillations.svg.png
tags:
    - Mechanism
    - Clinical significance
categories:
    - Oscillation
---
The insulin concentration in blood increases after meals and gradually returns to basal levels during the next 1–2 hours. However, the basal insulin level is not stable. It oscillates with a regular period of 3-6 min. After a meal the amplitude of these oscillations increases but the periodicity remains constant.[1] The oscillations are believed to be important for insulin sensitivity by preventing downregulation of insulin receptors in target cells.[1] Such downregulation underlies insulin resistance, which is common in type 2 diabetes. It would therefore be advantageous to administer insulin to diabetic patients in a manner mimicking the natural oscillations.[1] The insulin oscillations ...
