---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nellore
offline_file: ""
offline_thumbnail: ""
uuid: 909aa16c-2aab-4dfd-bdfe-3a47169cea3f
updated: 1484308447
title: Nellore
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tikkana_Park%252C_Nellore.jpg'
tags:
    - Etymology
    - History
    - geography
    - Climate
    - Demographics
    - Governance
    - Economy
    - culture
    - Transport
    - Port
    - Establishment and promoters
    - Connectivity and hinterland
    - Phases of development
    - Container terminal
    - Cargo handled
    - Special economic zone
    - Road
    - Rail
    - Education
    - Media
categories:
    - Uncategorized
---
Nellore (Nelluru) is a city in Nellore district of the Indian state of Andhra Pradesh. It is located on the banks of the Penna River in Nellore mandal of Nellore revenue division.[3] It is a municipal corporation and the headquarters of Nellore district. The city is the fourth most populous settlement of the state well known for its agriculture and aquaculture.
