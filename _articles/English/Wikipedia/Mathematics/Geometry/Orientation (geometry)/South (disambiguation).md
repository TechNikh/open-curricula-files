---
version: 1
type: article
id: https://en.wikipedia.org/wiki/South_(disambiguation)
offline_file: ""
offline_thumbnail: ""
uuid: 4b37afa1-3a3a-421f-943a-a93ca70809ff
updated: 1484309302
title: South (disambiguation)
tags:
    - geography
    - Films, books, music and other media
    - Books and written media
    - Film and television titles
    - Music groups
    - Recordings and releases
    - people
    - Other uses
categories:
    - Orientation (geometry)
---
South or The South may also refer to:
