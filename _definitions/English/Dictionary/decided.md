---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decided
offline_file: ""
offline_thumbnail: ""
uuid: 3f502f68-67cb-4476-9436-903459aa0c38
updated: 1484310256
title: decided
categories:
    - Dictionary
---
decided
     adj : recognizable; marked; "noticed a distinct improvement"; "at
           a distinct (or decided) disadvantage" [syn: {distinct}]
