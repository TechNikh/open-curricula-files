---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/berry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554021
title: berry
categories:
    - Dictionary
---
berry
     n 1: any of numerous small and pulpy edible fruits; used as
          desserts or in making jams and jellies and preserves
     2: a small fruit having any of various structures, e.g., simple
        (grape or blueberry) or aggregate (blackberry or
        raspberry)
     3: United States rock singer (born in 1931) [syn: {Chuck Berry},
         {Charles Edward Berry}]
     v : pick or gather berries; "We went berrying in the summer"
     [also: {berried}]
