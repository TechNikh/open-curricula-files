---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/govern
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484601601
title: govern
categories:
    - Dictionary
---
govern
     v 1: bring into conformity with rules or principles or usage;
          impose regulations; "We cannot regulate the way people
          dress"; "This town likes to regulate" [syn: {regulate},
          {regularize}, {regularise}, {order}] [ant: {deregulate}]
     2: direct or strongly influence the behavior of; "His belief in
        God governs his conduct"
     3: exercise authority over; as of nations; "Who is governing
        the country now?" [syn: {rule}]
     4: require to be in a certain grammatical case, voice, or mood;
        "most transitive verbs govern the accusative case in
        German"
