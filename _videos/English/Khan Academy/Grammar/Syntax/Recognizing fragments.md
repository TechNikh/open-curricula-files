---
version: 1
type: video
provider: YouTube
id: xpoZBnXHg3E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Recognizing%20fragments.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a3b640b1-8fef-4995-840c-ad643eb7d17d
updated: 1486069601
title: Recognizing fragments
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/xpoZBnXHg3E/default.jpg
    - https://i3.ytimg.com/vi/xpoZBnXHg3E/1.jpg
    - https://i3.ytimg.com/vi/xpoZBnXHg3E/2.jpg
    - https://i3.ytimg.com/vi/xpoZBnXHg3E/3.jpg
---
A sentence fragment is a chunk of language that hasn’t made it all the way to being a working sentence; it might be missing a verb, or there might not be a subject.  Learn how to turn a fragment into a sentence in this video!


Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/recognizing-fragments 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/run-ons-and-comma-splices-syntax-khan-academy 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/pronoun-antecedent-agreement-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
