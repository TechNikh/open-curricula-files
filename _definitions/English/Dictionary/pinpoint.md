---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pinpoint
offline_file: ""
offline_thumbnail: ""
uuid: 48670377-3285-43db-b0cd-adab027800a1
updated: 1484310391
title: pinpoint
categories:
    - Dictionary
---
pinpoint
     adj : meticulously precise; "pinpoint accuracy" [syn: {pinpoint(a)}]
     n 1: a very brief moment; "they were strangers sharing a pinpoint
          of time together"
     2: a very small spot; "the plane was just a speck in the sky"
        [syn: {speck}]
     3: the sharp point of a pin
     v : locate exactly; "can you pinpoint the position of the
         enemy?"; "The chemists could not nail the identity of the
         chromosome" [syn: {nail}]
