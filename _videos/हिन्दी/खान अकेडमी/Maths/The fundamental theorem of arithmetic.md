---
version: 1
type: video
provider: YouTube
id: tzOZTrZdgYo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dc60733d-3419-4072-81f1-673af8608371
updated: 1486069613
title: The fundamental theorem of arithmetic
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/tzOZTrZdgYo/default.jpg
    - https://i3.ytimg.com/vi/tzOZTrZdgYo/1.jpg
    - https://i3.ytimg.com/vi/tzOZTrZdgYo/2.jpg
    - https://i3.ytimg.com/vi/tzOZTrZdgYo/3.jpg
---
