---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decapitation_strike
offline_file: ""
offline_thumbnail: ""
uuid: f354c2bd-45ac-45e0-860b-4b6c463a4b41
updated: 1484309062
title: Decapitation strike
tags:
    - Non-nuclear use
    - In fiction
categories:
    - Strategy
---
In the theory of nuclear warfare, a decapitation strike is a first strike attack that aims to remove the command and control mechanisms of an opponent[1] in the hope that it will severely degrade or destroy its capacity for nuclear retaliation. It is essentially a subset of a counterforce strike but whereas a counterforce strike seeks to destroy weapons directly, a decapitation strike is designed to remove an enemy's ability to use its weapons.
