---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secretion
offline_file: ""
offline_thumbnail: ""
uuid: b36ce02a-29a9-40bc-92da-9a7eae93c7f3
updated: 1484310357
title: secretion
categories:
    - Dictionary
---
secretion
     n 1: the organic process of synthesizing and releasing some
          substance [syn: {secernment}]
     2: a functionally specialized substance (especially one that is
        not a waste) released from a gland or cell
