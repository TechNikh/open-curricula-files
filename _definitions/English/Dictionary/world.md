---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/world
offline_file: ""
offline_thumbnail: ""
uuid: fa0e3fd1-4b51-4fa9-b31f-c972c7ac241b
updated: 1484310328
title: world
categories:
    - Dictionary
---
world
     adj : involving the entire earth; not limited or provincial in
           scope; "global war"; "global monetary policy"; "neither
           national nor continental but planetary"; "a world
           crisis"; "of worldwide significance" [syn: {global}, {planetary},
            {world(a)}, {worldwide}]
     n 1: all of the inhabitants of the earth; "all the world loves a
          lover"; "she always used `humankind' because `mankind'
          seemed to slight the women" [syn: {human race}, {humanity},
           {humankind}, {human beings}, {humans}, {mankind}, {man}]
     2: everything that exists anywhere; "they study the evolution
        of the universe"; "the biggest tree ...
