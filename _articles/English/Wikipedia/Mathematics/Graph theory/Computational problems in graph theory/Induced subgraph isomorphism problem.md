---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Induced_subgraph_isomorphism_problem
offline_file: ""
offline_thumbnail: ""
uuid: 552a486e-d59d-4a9f-8004-235585826066
updated: 1484309394
title: Induced subgraph isomorphism problem
tags:
    - Problem statement
    - Computational complexity
    - Special cases
    - Differences with the subgraph isomorphism problem
categories:
    - Computational problems in graph theory
---
In complexity theory and graph theory, induced subgraph isomorphism is an NP-complete decision problem that involves finding a given graph as an induced subgraph of a larger graph.
