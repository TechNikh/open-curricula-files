---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excite
offline_file: ""
offline_thumbnail: ""
uuid: e1175289-12b2-46c6-b1e4-11be53a41b3e
updated: 1484310413
title: excite
categories:
    - Dictionary
---
excite
     v 1: arouse or elicit a feeling
     2: act as a stimulant; "The book stimulated her imagination";
        "This play stimulates" [syn: {stimulate}] [ant: {stifle}]
     3: raise to a higher energy level; "excite the atoms" [syn: {energize},
         {energise}]
     4: stir feelings in; "stimulate my appetite"; "excite the
        audience"; "stir emotions" [syn: {stimulate}, {stir}]
     5: cause to be agitated, excited, or roused; "The speaker
        charged up the crowd with his inflammatory remarks" [syn:
        {agitate}, {rouse}, {turn on}, {charge}, {commove}, {charge
        up}] [ant: {calm}]
     6: stimulate sexually; "This movie usually arouses the male
        ...
