---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cab
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484345761
title: cab
categories:
    - Dictionary
---
cab
     n 1: a compartment in front of a motor vehicle where driver sits
     2: small two-wheeled horse-drawn carriage; with two seats and a
        folding hood [syn: {cabriolet}]
     3: a car driven by a person whose job is to take passengers
        where they want to go in exchange for money [syn: {hack},
        {taxi}, {taxicab}]
