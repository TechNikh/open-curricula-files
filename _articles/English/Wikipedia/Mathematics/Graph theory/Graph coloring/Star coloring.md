---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Star_coloring
offline_file: ""
offline_thumbnail: ""
uuid: f11fcbc9-5c46-4f97-94aa-9b1d00113213
updated: 1484309386
title: Star coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_5.jpg
categories:
    - Graph coloring
---
In graph-theoretic mathematics, a star coloring of a graph G is a (proper) vertex coloring in which every path on four vertices uses at least three distinct colors. Equivalently, in a star coloring, the induced subgraphs formed by the vertices of any two colors has connected components that are star graphs. Star coloring has been introduced by Grünbaum (1973). The star chromatic number 
  
    
      
        
          χ
          
            s
          
        
        (
        G
        )
      
    
    {\displaystyle \chi _{s}(G)}
  
 of G is the least number of colors needed to star color G.
