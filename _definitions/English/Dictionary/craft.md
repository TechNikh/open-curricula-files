---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/craft
offline_file: ""
offline_thumbnail: ""
uuid: 27c7a62b-6d21-4807-99a4-174d32f18e4a
updated: 1484310475
title: craft
categories:
    - Dictionary
---
craft
     n 1: the skilled practice of a practical occupation; "he learned
          his trade as an apprentice" [syn: {trade}]
     2: a vehicle designed for navigation in or on water or air or
        through outer space
     3: people who perform a particular kind of skilled work; "he
        represented the craft of brewers"; "as they say in the
        trade" [syn: {trade}]
     4: skill in an occupation or trade [syn: {craftsmanship}, {workmanship}]
     5: shrewdness as demonstrated by being skilled in deception
        [syn: {craftiness}, {cunning}, {foxiness}, {guile}, {slyness},
         {wiliness}]
     v : make by hand and with much skill; "The artisan crafted a
         ...
