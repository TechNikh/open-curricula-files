---
version: 1
type: video
provider: YouTube
id: Ve99biD1KtA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Limits/Proof-%20lim%20%28sin%20x%29-x.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2071c4df-8816-48b6-a85c-ed7b6a482e3a
updated: 1486069692
title: 'Proof- lim (sin x)/x'
categories:
    - Limits
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ve99biD1KtA/default.jpg
    - https://i3.ytimg.com/vi/Ve99biD1KtA/1.jpg
    - https://i3.ytimg.com/vi/Ve99biD1KtA/2.jpg
    - https://i3.ytimg.com/vi/Ve99biD1KtA/3.jpg
---
