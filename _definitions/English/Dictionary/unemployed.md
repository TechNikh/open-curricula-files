---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unemployed
offline_file: ""
offline_thumbnail: ""
uuid: 13d794ea-9f30-4d9d-bf33-f8a169fc58a2
updated: 1484310441
title: unemployed
categories:
    - Dictionary
---
unemployed
     adj : not engaged in a gainful occupation; "unemployed workers
           marched on the capital" [ant: {employed}]
