---
version: 1
type: video
provider: YouTube
id: OttRV5ykP7A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/How%20to%20speed%20up%20chemical%20reactions%20%28and%20get%20a%20date%29%20-%20Aaron%20Sams.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 441803f7-9378-4567-84c5-0cfd6ee0fcbf
updated: 1486069664
title: 'How to speed up chemical reactions (and get a date) - Aaron Sams'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/OttRV5ykP7A/default.jpg
    - https://i3.ytimg.com/vi/OttRV5ykP7A/1.jpg
    - https://i3.ytimg.com/vi/OttRV5ykP7A/2.jpg
    - https://i3.ytimg.com/vi/OttRV5ykP7A/3.jpg
---
