---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propaganda
offline_file: ""
offline_thumbnail: ""
uuid: d7398cea-42c1-4f8c-8182-fb8b0d07825c
updated: 1484310563
title: propaganda
categories:
    - Dictionary
---
propaganda
     n : information that is spread for the purpose of promoting some
         cause
