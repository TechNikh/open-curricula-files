---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lov%C3%A1sz_number'
offline_file: ""
offline_thumbnail: ""
uuid: a10f7587-94c9-40bd-94e9-21014bf2545a
updated: 1484309420
title: Lovász number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Cycle_graph_C5.png
tags:
    - Definition
    - Equivalent expressions
    - Value of ϑ for some well-known graphs
    - Properties
    - Lovász "sandwich theorem"
    - Relation to Shannon capacity
    - Quantum physics
    - Notes
categories:
    - Graph invariants
---
In graph theory, the Lovász number of a graph is a real number that is an upper bound on the Shannon capacity of the graph. It is also known as Lovász theta function and is commonly denoted by ϑ(G). This quantity was first introduced by László Lovász in his 1979 paper On the Shannon Capacity of a Graph.[1]
