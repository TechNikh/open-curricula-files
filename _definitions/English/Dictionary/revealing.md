---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revealing
offline_file: ""
offline_thumbnail: ""
uuid: 045e4c8f-bbb3-4e0e-88df-08689666d26a
updated: 1484310327
title: revealing
categories:
    - Dictionary
---
revealing
     adj 1: disclosing unintentionally; "a telling smile"; "a telltale
            panel of lights"; "a telltale patch of oil on the
            water marked where the boat went down" [syn: {telling},
             {telltale(a)}]
     2: showing or making known; "her dress was scanty and
        revealing" [ant: {concealing}]
     n : the speech act of making something evident [syn: {disclosure},
          {revelation}]
