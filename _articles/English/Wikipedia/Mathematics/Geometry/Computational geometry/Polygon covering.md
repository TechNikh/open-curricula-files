---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polygon_covering
offline_file: ""
offline_thumbnail: ""
uuid: 5ad08bf7-14d9-4125-8c29-48e95f8376e9
updated: 1484309175
title: Polygon covering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Removing_holes_from_a_rectilinear_polygon.png
tags:
    - Basic concepts
    - Covering a rectilinear polygon with squares
    - Covering a rectilinear polygon with rectangles
    - >
        Covering a rectilinear polygon with orthogonally convex
        polygons
    - Covering a rectilinear polygon with star polygons
    - >
        Covering a polygon without acute angles with squares or
        rectangles
    - Covering a polygon with rectangles of a finite family
    - Covering a polygon with triangles
    - Covering a polygon with convex polygons
    - Covering a polygon with star polygons
    - Other combinations
categories:
    - Computational geometry
---
A covering of a polygon is a set of primitive units (e.g. squares) whose union equals the polygon. A polygon covering problem is a problem of finding a covering with a smallest number of units for a given polygon. This is an important class of problems in computational geometry. There are many different polygon covering problems, depending on the type of polygon being covered and on the types of units allowed in the covering. An example polygon covering problem is: given a rectilinear polygon, find a smallest set of squares whose union equals the polygon.
