---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/press
offline_file: ""
offline_thumbnail: ""
uuid: bc8b31cf-dc98-42e9-b43d-b0be15b9917a
updated: 1484310321
title: press
categories:
    - Dictionary
---
press
     n 1: newspaper writers and photographers [syn: {fourth estate}]
     2: the state of urgently demanding notice or attention; "the
        press of business matters" [syn: {imperativeness}, {insistence},
         {insistency}, {pressure}]
     3: the gathering and publishing of news in the form of
        newspapers or magazines [syn: {public press}]
     4: a machine used for printing [syn: {printing press}]
     5: a dense crowd of people [syn: {crush}, {jam}]
     6: a tall piece of furniture that provides storage space for
        clothes; has a door and rails or hooks for hanging clothes
        [syn: {wardrobe}, {closet}]
     7: clamp to prevent wooden rackets from warping ...
