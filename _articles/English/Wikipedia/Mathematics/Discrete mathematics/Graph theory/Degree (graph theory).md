---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Degree_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 33280645-de4d-46f2-8c46-4b0485de5d04
updated: 1484308622
title: Degree (graph theory)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-UndirectedDegrees_%2528Loop%2529.svg.png'
tags:
    - Handshaking lemma
    - Degree sequence
    - Special values
    - Global properties
    - Notes
categories:
    - Graph theory
---
In graph theory, the degree (or valency) of a vertex of a graph is the number of edges incident to the vertex, with loops counted twice.[1] The degree of a vertex 
  
    
      
        v
      
    
    {\displaystyle v}
  
 is denoted 
  
    
      
        deg
        ⁡
        (
        v
        )
      
    
    {\displaystyle \deg(v)}
  
 or 
  
    
      
        deg
        ⁡
        v
      
    
    {\displaystyle \deg v}
  
. The maximum degree of a graph G, denoted by Δ(G), and the minimum degree of a graph, denoted by δ(G), are the maximum and minimum degree of its vertices. In the graph on the right, the maximum degree is 5 and the minimum degree is 0. In a regular ...
