---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spontaneous_symmetry_breaking
offline_file: ""
offline_thumbnail: ""
uuid: 66d66e42-4a2b-40c1-955c-3abb5a73a79c
updated: 1484309317
title: Spontaneous symmetry breaking
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/270px-Mexican_hat_potential_polar.svg.png
tags:
    - Spontaneous symmetry breaking in physics
    - Particle physics
    - Chiral symmetry
    - Higgs mechanism
    - Condensed matter physics
    - Continuous symmetry
    - Dynamical symmetry breaking
    - Generalisation and technical usage
    - 'A pedagogical example: the Mexican hat potential'
    - Other examples
    - Nobel Prize
    - Notes
categories:
    - Symmetry
---
Spontaneous symmetry breaking[1][2][3] is a mode of realization of symmetry breaking in a physical system, where the underlying laws are invariant under a symmetry transformation, but the system as a whole changes under such transformations, in contrast to explicit symmetry breaking. It is a spontaneous process by which a system in a symmetrical state ends up in an asymmetrical state. It thus describes systems where the equations of motion or the Lagrangian obey certain symmetries, but the lowest-energy solutions do not exhibit that symmetry.
