---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stepping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425021
title: stepping
categories:
    - Dictionary
---
step
     n 1: any maneuver made as part of progress toward a goal; "the
          situation called for strong measures"; "the police took
          steps to reduce crime" [syn: {measure}]
     2: the distance covered by a step; "he stepped off ten paces
        from the old tree and began to dig" [syn: {footstep}, {pace},
         {stride}]
     3: the act of changing location by raising the foot and setting
        it down; "he walked with unsteady steps"
     4: support consisting of a place to rest the foot while
        ascending or descending a stairway; "he paused on the
        bottom step" [syn: {stair}]
     5: relative position in a graded series; "always a step
        behind"; ...
