---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/De_Morgan%27s_laws'
offline_file: ""
offline_thumbnail: ""
uuid: df53bab7-9171-4ea4-83fc-8d2f73aeca68
updated: 1484309231
title: "De Morgan's laws"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Demorganlaws.svg.png
tags:
    - Formal notation
    - Substitution form
    - Set theory and Boolean algebra
    - Engineering
    - Text searching
    - History
    - Informal proof
    - Negation of a disjunction
    - Negation of a conjunction
    - Formal proof
    - Part 1
    - Part 2
    - Conclusion
    - Generalising De Morgan duality
    - Extension to predicate and modal logic
categories:
    - Duality theories
---
In propositional logic and boolean algebra, De Morgan's laws[1][2][3] are a pair of transformation rules that are both valid rules of inference. They are named after Augustus De Morgan, a 19th-century British mathematician. The rules allow the expression of conjunctions and disjunctions purely in terms of each other via negation.
