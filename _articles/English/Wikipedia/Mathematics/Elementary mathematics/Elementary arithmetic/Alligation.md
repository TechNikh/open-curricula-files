---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alligation
offline_file: ""
offline_thumbnail: ""
uuid: a1100bf9-1bd6-4f20-bb47-c03ebf100ed5
updated: 1484308907
title: Alligation
tags:
    - Examples
    - Alligation medial
    - Alligation alternate
categories:
    - Elementary arithmetic
---
Alligation is an old and practical method of solving arithmetic problems related to mixtures of ingredients. There are two types of alligation: alligation medial, used to find the quantity of a mixture given the quantities of its ingredients, and alligation alternate, used to find the amount of each ingredient needed to make a mixture of a given quantity. Alligation medial is merely a matter of finding a weighted mean. Alligation alternate is more complicated and involves organizing the ingredients into high and low pairs which are then traded off.
