---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Frucht
offline_file: ""
offline_thumbnail: ""
uuid: 8c6b837b-c56d-4380-b06e-f9717e3808a4
updated: 1484309461
title: Robert Frucht
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Frucht_planar_Lombardi.svg.png
categories:
    - Graph theorists
---
Robert Wertheimer Frucht (later known as Roberto Frucht) (9 August 1906 – 26 June 1997)[1][2] was a German-Chilean mathematician; his research specialty was graph theory and the symmetries of graphs.
