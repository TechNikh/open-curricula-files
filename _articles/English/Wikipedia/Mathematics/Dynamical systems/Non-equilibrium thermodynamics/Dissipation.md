---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dissipation
offline_file: ""
offline_thumbnail: ""
uuid: a0d9147b-ca4d-4bc2-8729-bc09d8416f92
updated: 1484308797
title: Dissipation
tags:
    - Definition
    - Energy
    - Computational physics
    - Mathematics
    - Examples
    - In hydraulic engineering
    - Irreversible processes
    - Waves or oscillations
    - History
categories:
    - Non-equilibrium thermodynamics
---
Dissipation is the result of an irreversible process that takes place in inhomogeneous thermodynamic systems. A dissipative process is a process in which energy (internal, bulk flow kinetic, or system potential) is transformed from some initial form to some final form; the capacity of the final form to do mechanical work is less than that of the initial form. For example, heat transfer is dissipative because it is a transfer of internal energy from a hotter body to a colder one. Following the second law of thermodynamics, the entropy varies with temperature (reduces the capacity of the combination of the two bodies to do mechanical work), but never decreases in an isolated system.
