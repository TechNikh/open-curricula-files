---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/besides
offline_file: ""
offline_thumbnail: ""
uuid: 9ab13287-5549-40a7-9595-4002fff29210
updated: 1484310335
title: besides
categories:
    - Dictionary
---
besides
     adv 1: making an additional point; anyway; "I don't want to go to a
            restaurant; besides, we can't afford it"; "she
            couldn't shelter behind him all the time and in any
            case he wasn't always with her" [syn: {in any case}]
     2: in addition; "he has a Mercedes, too" [syn: {too}, {also}, {likewise},
         {as well}]
