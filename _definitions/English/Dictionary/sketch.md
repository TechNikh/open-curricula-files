---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sketch
offline_file: ""
offline_thumbnail: ""
uuid: 12c5af0a-b678-445d-ad88-754fe9018363
updated: 1484310299
title: sketch
categories:
    - Dictionary
---
sketch
     n 1: preliminary drawing for later elaboration; "he made several
          studies before starting to paint" [syn: {study}]
     2: a brief literary description [syn: {vignette}]
     3: short descriptive summary (of events) [syn: {survey}, {resume}]
     4: a humorous or satirical drawing published in a newspaper or
        magazine [syn: {cartoon}]
     v 1: make a sketch of; "sketch the building" [syn: {chalk out}]
     2: describe roughly or briefly or give the main points or
        summary of; "sketch the outline of the book"; "outline his
        ideas" [syn: {outline}, {adumbrate}]
