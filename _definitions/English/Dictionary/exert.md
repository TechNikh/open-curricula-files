---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exert
offline_file: ""
offline_thumbnail: ""
uuid: efe661bc-dc1e-4394-94fa-600eaf2eab6d
updated: 1484310535
title: exert
categories:
    - Dictionary
---
exert
     v 1: put to use; "exert one's power or influence" [syn: {exercise}]
     2: of power or authority [syn: {wield}, {maintain}]
     3: make a great effort at a mental or physical task; "exert
        oneself"
