---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bark
offline_file: ""
offline_thumbnail: ""
uuid: fc412123-f395-4380-b7ed-c93744c53273
updated: 1484310543
title: bark
categories:
    - Dictionary
---
bark
     n 1: tough protective covering of the woody stems and roots of
          trees and other woody plants
     2: a noise resembling the bark of a dog
     3: a sailing ship with 3 (or more) masts [syn: {barque}]
     4: the sound made by a dog
     v 1: speak in an unfriendly tone; "She barked into the
          dictaphone"
     2: cover with bark
     3: remove the bark of a tree [syn: {skin}]
     4: make barking sounds; "The dogs barked at the stranger"
     5: tan (a skin) with bark tannins
