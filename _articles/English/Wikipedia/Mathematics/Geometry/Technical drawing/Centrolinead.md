---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centrolinead
offline_file: ""
offline_thumbnail: ""
uuid: 016c6a9e-aadd-4538-adbe-f24c2072adb3
updated: 1484309325
title: Centrolinead
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Centrolinead.png
tags:
    - Usage
categories:
    - Technical drawing
---
The centrolinead was invented by Peter Nicholson, a British mathematician and architect, in 1814.[1] It was used to construct 2-point perspective drawings where one or both vanishing points existed outside the drawing board. Draftsmen could use the instrument in pairs; one for each vanishing point on each side of the station point.
