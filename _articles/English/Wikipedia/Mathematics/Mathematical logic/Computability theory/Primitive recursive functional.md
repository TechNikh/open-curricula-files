---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primitive_recursive_functional
offline_file: ""
offline_thumbnail: ""
uuid: 16525f48-390b-4c7e-b495-00da6ade866e
updated: 1484309489
title: Primitive recursive functional
categories:
    - Computability theory
---
In mathematical logic, the primitive recursive functionals are a generalization of primitive recursive functions into higher type theory. They consist of a collection of functions in all pure finite types.
