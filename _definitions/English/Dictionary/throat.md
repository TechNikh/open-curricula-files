---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/throat
offline_file: ""
offline_thumbnail: ""
uuid: 1813309c-26f1-4a17-b337-e0d1529c5cfb
updated: 1484310328
title: throat
categories:
    - Dictionary
---
throat
     n 1: the passage to the stomach and lungs; in the front part of
          the neck below the chin and above the collarbone [syn: {pharynx}]
     2: an opening in the vamp of a shoe at the instep
     3: a passage resembling a throat in shape or function; "the
        throat of the vase"; "the throat of a chimney";
