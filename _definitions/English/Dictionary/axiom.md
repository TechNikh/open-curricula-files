---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/axiom
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484355481
title: axiom
categories:
    - Dictionary
---
axiom
     n 1: a saying that widely accepted on its own merits [syn: {maxim}]
     2: (logic) a proposition that is not susceptible of proof or
        disproof; its truth is assumed to be self-evident
