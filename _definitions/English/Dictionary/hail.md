---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hail
offline_file: ""
offline_thumbnail: ""
uuid: ff58e25f-9586-4aa5-ac2c-c47ec563fb0b
updated: 1484310461
title: hail
categories:
    - Dictionary
---
hail
     n 1: precipitation of ice pellets when there are strong rising
          air currents
     2: enthusiastic greeting
     v 1: praise vociferously; "The critics hailed the young pianist
          as a new Rubinstein" [syn: {acclaim}, {herald}]
     2: be a native of; "She hails from Kalamazoo" [syn: {come}]
     3: call for; "hail a cab"
     4: greet enthusiastically or joyfully [syn: {herald}]
     5: precipitate as small ice particles; "It hailed for an hour"
