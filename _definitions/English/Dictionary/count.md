---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/count
offline_file: ""
offline_thumbnail: ""
uuid: 122611b4-e557-4817-b71e-5b260ea1cfd7
updated: 1484310371
title: count
categories:
    - Dictionary
---
count
     n 1: the total number counted; "a blood count"
     2: the act of counting; "the counting continued for several
        hours" [syn: {counting}, {numeration}, {enumeration}, {reckoning},
         {tally}]
     3: a nobleman (in various countries) having rank equal to a
        British earl
     v 1: determine the number or amount of; "Can you count the books
          on your shelf?"; "Count your change" [syn: {number}, {enumerate},
           {numerate}]
     2: have weight; have import, carry weight; "It does not matter
        much" [syn: {matter}, {weigh}]
     3: show consideration for; take into account; "You must
        consider her age"; "The judge considered the ...
