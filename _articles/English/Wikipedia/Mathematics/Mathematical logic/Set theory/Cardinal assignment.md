---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardinal_assignment
offline_file: ""
offline_thumbnail: ""
uuid: 8a154934-8d03-4a10-b963-f61e9a740b5d
updated: 1484309501
title: Cardinal assignment
categories:
    - Set theory
---
In set theory, the concept of cardinality is significantly developable without recourse to actually defining cardinal numbers as objects in theory itself (this is in fact a viewpoint taken by Frege; Frege cardinals are basically equivalence classes on the entire universe of sets which are equinumerous). The concepts are developed by defining equinumerosity in terms of functions and the concepts of one-to-one and onto (injectivity and surjectivity); this gives us a pseudo-ordering relation
