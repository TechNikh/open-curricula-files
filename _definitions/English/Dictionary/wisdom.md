---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wisdom
offline_file: ""
offline_thumbnail: ""
uuid: eecda604-22ef-4c48-9e48-7a96c3915c04
updated: 1484310172
title: wisdom
categories:
    - Dictionary
---
wisdom
     n 1: accumulated knowledge or erudition or enlightenment
     2: the trait of utilizing knowledge and experience with common
        sense and insight [syn: {wiseness}] [ant: {folly}]
     3: ability to apply knowledge or experience or understanding or
        common sense and insight [syn: {sapience}]
     4: the quality of being prudent and sensible [syn: {wiseness},
        {soundness}]
     5: an Apocryphal book consisting mainly of a meditation on
        wisdom; although ascribed to Solomon it was probably
        written in the first century BC [syn: {Wisdom of Solomon}]
