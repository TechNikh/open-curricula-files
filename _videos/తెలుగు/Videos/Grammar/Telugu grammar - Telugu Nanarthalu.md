---
version: 1
type: video
provider: YouTube
id: 3GfRAdIol-g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca70278e-f14d-435e-bd32-322d4810b6a3
updated: 1486069625
title: 'Telugu grammar - Telugu Nanarthalu'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/3GfRAdIol-g/default.jpg
    - https://i3.ytimg.com/vi/3GfRAdIol-g/1.jpg
    - https://i3.ytimg.com/vi/3GfRAdIol-g/2.jpg
    - https://i3.ytimg.com/vi/3GfRAdIol-g/3.jpg
---
