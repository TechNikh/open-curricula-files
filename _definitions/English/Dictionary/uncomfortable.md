---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncomfortable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576461
title: uncomfortable
categories:
    - Dictionary
---
uncomfortable
     adj 1: conducive to or feeling mental discomfort; "this kind of
            life can prove disruptive and uncomfortable"; "the
            uncomfortable truth"; "grew uncomfortable beneath his
            appraising eye"; "an uncomfortable way of surprising
            me just when I felt surest"; "the teacher's presence
            at the conference made the child very uncomfortable"
            [ant: {comfortable}]
     2: providing or experiencing physical discomfort; "an
        uncomfortable chair"; "an uncomfortable day in the hot
        sun" [ant: {comfortable}]
