---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sugar
offline_file: ""
offline_thumbnail: ""
uuid: 1c9ce47d-7e2f-4ccf-9999-fbc801471cea
updated: 1484310346
title: sugar
categories:
    - Dictionary
---
sugar
     n 1: a white crystalline carbohydrate used as a sweetener and
          preservative [syn: {refined sugar}]
     2: an essential structural component of living cells and source
        of energy for animals; includes simple sugars with small
        molecules as well as macromolecular substances; are
        classified according to the number of monosaccharide
        groups they contain [syn: {carbohydrate}, {saccharide}]
     3: informal terms for money [syn: {boodle}, {bread}, {cabbage},
         {clams}, {dinero}, {dough}, {gelt}, {kale}, {lettuce}, {lolly},
         {lucre}, {loot}, {moolah}, {pelf}, {scratch}, {shekels},
        {simoleons}, {wampum}]
     v : sweeten with ...
