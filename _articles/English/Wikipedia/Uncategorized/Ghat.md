---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ghats
offline_file: ""
offline_thumbnail: ""
uuid: 77d44f30-2a70-495b-be05-d10c57a78101
updated: 1484308420
title: Ghat
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dashashwamedha_ghat_on_the_Ganga%252C_Varanasi.jpg'
tags:
    - Etymology
    - Along the rivers Ganges and Narmada
    - Shmashana ghats
    - Other uses
    - Gallery
categories:
    - Uncategorized
---
As used in many parts of Northern South Asia, the term ghat refers to a series of steps leading down to a body of water, particularly a holy river. In Bengali-speaking regions, this set of stairs can lead down to something as small as a pond or as large as a major river.
