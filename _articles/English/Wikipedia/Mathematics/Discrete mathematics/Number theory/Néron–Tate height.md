---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/N%C3%A9ron%E2%80%93Tate_height'
offline_file: ""
offline_thumbnail: ""
uuid: 6e9c8b3a-2e8c-4a8e-a649-f87c50f309f7
updated: 1484308649
title: Néron–Tate height
tags:
    - Definition and properties
    - The elliptic and abelian regulators
    - Lower bounds for the Néron–Tate height
    - Generalizations
categories:
    - Number theory
---
In number theory, the Néron–Tate height (or canonical height) is a quadratic form on the Mordell-Weil group of rational points of an abelian variety defined over a global field. It is named after André Néron and John Tate.
