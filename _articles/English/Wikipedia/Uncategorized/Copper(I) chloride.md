---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cucl
offline_file: ""
offline_thumbnail: ""
uuid: 82522a11-e455-4176-8544-af8dc60776d5
updated: 1484308313
title: Copper(I) chloride
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-IR_Spectrum_of_Copper%2528I%2529_chloride.png'
tags:
    - History
    - Synthesis
    - Chemical properties
    - Uses
    - In organic synthesis
    - In polymer chemistry
categories:
    - Uncategorized
---
Copper(I) chloride, commonly called cuprous chloride, is the lower chloride of copper, with the formula CuCl. The substance is a white solid sparingly soluble in water, but very soluble in concentrated hydrochloric acid. Impure samples appear green due to the presence of copper(II) chloride.[3]
