---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Optical_heterodyne_detection
offline_file: ""
offline_thumbnail: ""
uuid: 8bd7083f-c089-48c2-9a35-5a5c1978e07b
updated: 1484308893
title: Optical heterodyne detection
tags:
    - >
        Contrast to conventional radio frequency (RF) heterodyne
        detection
    - Energy versus electric field detection
    - Wideband local oscillators for coherent detection
    - Key benefits
    - Gain in the detection
    - Preservation of optical phase
    - >
        Mapping optical frequencies to electronic frequencies allows
        sensitive measurements
    - Noise reduction to shot noise limit
    - Key problems and their solutions
    - AC detection and imaging
    - Speckle and diversity reception
    - Coherent temporal summation
categories:
    - Waves
---
Optical heterodyne detection is the implementation of heterodyne detection principle using a nonlinear optical process. In heterodyne detection, a signal of interest at some frequency is non-linearly mixed with a reference "local oscillator" (LO) that is set at a close-by frequency. The desired outcome is the difference frequency, which carries the information (amplitude, phase, and frequency modulation) of the original higher frequency signal, but is oscillating at a lower more easily processed carrier frequency.
