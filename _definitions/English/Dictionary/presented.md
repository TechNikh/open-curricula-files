---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presented
offline_file: ""
offline_thumbnail: ""
uuid: f11b1a30-5710-4d52-b4f0-8503ff0f2584
updated: 1484310309
title: presented
categories:
    - Dictionary
---
presented
     adj : given formally or officially [syn: {bestowed}, {conferred}]
