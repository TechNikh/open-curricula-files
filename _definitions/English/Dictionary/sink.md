---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sink
offline_file: ""
offline_thumbnail: ""
uuid: 35163534-ae7e-4e57-8160-ba87bfa4d25f
updated: 1484310539
title: sink
categories:
    - Dictionary
---
sink
     n 1: plumbing fixture consisting of a water basin fixed to a wall
          or floor and having a drainpipe
     2: (technology) a process that acts to absorb or remove energy
        or a substance from a system; "the ocean is a sink for
        carbon dioxide" [ant: {source}]
     3: a depression in the ground communicating with a subterranean
        passage (especially in limestone) and formed by solution
        or by collapse of a cavern roof [syn: {sinkhole}, {swallow
        hole}]
     4: a covered cistern; waste water and sewage flow into it [syn:
         {cesspool}, {cesspit}, {sump}]
     v 1: fall or drop to a lower place or level; "He sank to his
          knees" ...
