---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cubane
offline_file: ""
offline_thumbnail: ""
uuid: 5c5c8c37-c6eb-4993-a5ad-f156fee80d7e
updated: 1484309277
title: Cubane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Cuba.svg.png
tags:
    - Synthesis
    - Derivatives
    - Reactions
categories:
    - Molecular geometry
---
Cubane (C8H8) is a synthetic hydrocarbon molecule that consists of eight carbon atoms arranged at the corners of a cube, with one hydrogen atom attached to each carbon atom. A solid crystalline substance, cubane is one of the Platonic hydrocarbons and a member of the prismanes. It was first synthesized in 1964 by Philip Eaton and Thomas Cole.[3] Prior to this work, researchers believed that cubic carbon-based molecules would be too unstable to exist. The cubic shape requires the carbon atoms to adopt an unusually sharp 90° bonding angle, which would be highly strained as compared to the 109.45° angle of a tetrahedral carbon. Once formed, cubane is quite kinetically stable, due to a lack ...
