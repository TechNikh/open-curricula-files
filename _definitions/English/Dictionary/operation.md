---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operation
offline_file: ""
offline_thumbnail: ""
uuid: ac355cd2-ffeb-45c9-b17f-8b84bc89ca0a
updated: 1484310371
title: operation
categories:
    - Dictionary
---
operation
     n 1: a business especially one run on a large scale; "a
          large-scale farming operation"; "a multinational
          operation"; "they paid taxes on every stage of the
          operation"; "they had to consolidate their operations"
     2: a planned activity involving many people performing various
        actions; "they organized a rescue operation"; "the biggest
        police operation in French history"; "running a restaurant
        is quite an operation"; "consolidate the companies various
        operations"
     3: a process or series of acts especially of a practical or
        mechanical nature involved in a particular form of work;
        "the operations ...
