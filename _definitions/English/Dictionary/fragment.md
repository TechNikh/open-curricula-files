---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fragment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484644081
title: fragment
categories:
    - Dictionary
---
fragment
     n 1: a piece broken off or cut off of something else; "a fragment
          of rock"
     2: a broken piece of a brittle artifact [syn: {shard}, {sherd}]
     3: an incomplete piece; "fragments of a play"
     v : break or cause to break into pieces; "The plate fragmented"
         [syn: {break up}, {fragmentize}, {fragmentise}]
