---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyrites
offline_file: ""
offline_thumbnail: ""
uuid: 866e7356-f99a-43d7-8cb6-1f8995916db7
updated: 1484310407
title: pyrites
categories:
    - Dictionary
---
pyrites
     n : any of various metallic-looking sulfides (of which pyrite is
         the commonest)
