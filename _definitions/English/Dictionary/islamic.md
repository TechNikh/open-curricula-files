---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/islamic
offline_file: ""
offline_thumbnail: ""
uuid: dea88661-418e-483c-bdc7-996c31ecf7fe
updated: 1484310186
title: islamic
categories:
    - Dictionary
---
Islamic
     adj : of or relating to or supporting Islamism; "Islamic art"
           [syn: {Muslim}, {Moslem}]
