---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silenced
offline_file: ""
offline_thumbnail: ""
uuid: aed29435-8ab9-4ef2-9305-52feb0b7d265
updated: 1484310607
title: silenced
categories:
    - Dictionary
---
silenced
     adj : reduced to silence; "the silenced crowd waited expectantly"
           [ant: {unsilenced}]
