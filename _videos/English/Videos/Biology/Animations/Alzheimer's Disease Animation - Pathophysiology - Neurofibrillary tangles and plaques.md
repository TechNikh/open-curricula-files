---
version: 1
type: video
provider: YouTube
id: juneXNm-QCs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 522ad671-ad5e-4778-b7c4-17ebb67f67f3
updated: 1486069668
title: "Alzheimer's Disease Animation - Pathophysiology - Neurofibrillary tangles and plaques"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/juneXNm-QCs/default.jpg
    - https://i3.ytimg.com/vi/juneXNm-QCs/1.jpg
    - https://i3.ytimg.com/vi/juneXNm-QCs/2.jpg
    - https://i3.ytimg.com/vi/juneXNm-QCs/3.jpg
---
