---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pictured
offline_file: ""
offline_thumbnail: ""
uuid: 2c8b19e9-5632-4f29-9c67-21f070c867be
updated: 1484310433
title: pictured
categories:
    - Dictionary
---
pictured
     adj 1: seen in the mind as a mental image; "the glory of his
            envisioned future"; "the snow-covered Alps pictured in
            her imagination"; "the visualized scene lacked the
            ugly details of real life" [syn: {envisioned}, {visualized},
             {visualised}]
     2: represented graphically by sketch or design or lines [syn: {depicted},
         {portrayed}]
