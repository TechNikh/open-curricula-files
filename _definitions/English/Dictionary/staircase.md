---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/staircase
offline_file: ""
offline_thumbnail: ""
uuid: a56bb769-1227-4110-a331-aeabacb7f5a2
updated: 1484310399
title: staircase
categories:
    - Dictionary
---
staircase
     n : a way of access consisting of a set of steps [syn: {stairway},
          {stairs}, {steps}]
