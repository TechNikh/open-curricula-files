---
version: 1
type: video
provider: YouTube
id: xPb6HLM3xEQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/AP%20Calculus%20BC%20Exams-%202008%201%20b%26c.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a03be6fb-cf51-4625-aa5e-3bf92d7a1915
updated: 1486069690
title: 'AP Calculus BC Exams- 2008 1 b&c'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/xPb6HLM3xEQ/default.jpg
    - https://i3.ytimg.com/vi/xPb6HLM3xEQ/1.jpg
    - https://i3.ytimg.com/vi/xPb6HLM3xEQ/2.jpg
    - https://i3.ytimg.com/vi/xPb6HLM3xEQ/3.jpg
---
