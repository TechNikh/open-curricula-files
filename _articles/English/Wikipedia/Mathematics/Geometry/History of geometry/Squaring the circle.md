---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Squaring_the_circle
offline_file: ""
offline_thumbnail: ""
uuid: 2c75c97d-cc7b-40de-8d42-366bf69008b7
updated: 1484309249
title: Squaring the circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Quadrature_of_Circle_Cajori_1919.png
tags:
    - History
    - Impossibility
    - Modern approximative constructions
    - Squaring or quadrature as integration
    - Claims of circle squaring
    - Connection with the longitude problem
    - Other modern claims
    - In literature
categories:
    - History of geometry
---
Squaring the circle is a problem proposed by ancient geometers. It is the challenge of constructing a square with the same area as a given circle by using only a finite number of steps with compass and straightedge. More abstractly and more precisely, it may be taken to ask whether specified axioms of Euclidean geometry concerning the existence of lines and circles entail the existence of such a square.
