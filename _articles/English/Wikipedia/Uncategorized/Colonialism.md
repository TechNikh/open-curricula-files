---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pre-colonial
offline_file: ""
offline_thumbnail: ""
uuid: a4c309fe-bc58-44f3-aeef-f867265b43ad
updated: 1484308450
title: Colonialism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Musee-de-lArmee-IMG_0976.jpg
tags:
    - Definitions
    - Types of colonialism
    - Socio-cultural evolution
    - The Other
    - History
    - European empires in 1914
    - British colonies and protectorates
    - French colonies
    - Russian colonies and protectorates
    - German colonies
    - Italian colonies and protectorates
    - Dutch colonies
    - Portuguese colonies
    - Spanish colonies
    - Austro-Hungarian colonies
    - Danish colonies
    - Belgian colonies
    - Numbers of European settlers in the colonies (1500–1914)
    - Other non-European colonialist countries in 1914
    - Australian protectorate
    - New Zealand dependencies
    - United States colonies and protectorates
    - Turkish (Ottoman) colonies
    - Japanese colonies
    - Chinese colonies
    - Neocolonialism
    - Colonialism and the history of thought
    - Precolonialism
    - Universalism
    - Colonialism and geography
    - Colonialism and imperialism
    - Marxist view of colonialism
    - Liberalism, capitalism and colonialism
    - Scientific thought in colonialism, race and gender
    - Post-colonialism
    - Impact of colonialism and colonization
    - Economy, trade and commerce
    - Slaves and indentured servants
    - Military innovation
    - The end of empire
    - Post-independence population movement
    - Impact on health
    - Countering disease
    - Colonial migrations
    - Notes
    - Primary sources
categories:
    - Uncategorized
---
Colonialism is the establishment of a colony in one territory by a political power from another territory, and the subsequent maintenance, expansion, and exploitation of that colony. The term is also used to describe a set of unequal relationships between the colonial power and the colony and often between the colonists and the indigenous peoples.
