---
version: 1
type: video
provider: YouTube
id: q5sG_CkkAs8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/Introduction%20to%20contractions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2674f83c-4009-4d9f-be3a-bb8de42b0261
updated: 1486069601
title: Introduction to contractions
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/q5sG_CkkAs8/default.jpg
    - https://i3.ytimg.com/vi/q5sG_CkkAs8/1.jpg
    - https://i3.ytimg.com/vi/q5sG_CkkAs8/2.jpg
    - https://i3.ytimg.com/vi/q5sG_CkkAs8/3.jpg
---
Apostrophes are great at standing in for missing letters, allowing us to shorten words. Paige and David discuss contractions and the Principle of Least Effort. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/the-apostrophe/e/introduction-to-contractions

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/introduction-to-the-possessive-the-apostrophe-punctuation-khan-academy 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/introduction-to-the-apostrophe-the-apostrophe-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
