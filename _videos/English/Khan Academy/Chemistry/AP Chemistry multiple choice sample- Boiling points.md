---
version: 1
type: video
provider: YouTube
id: JRxFsmvsmHY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/AP%20Chemistry%20multiple%20choice%20sample-%20Boiling%20points.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3e4b3a6b-598b-4c9d-af06-31262f644c54
updated: 1486069587
title: 'AP Chemistry multiple choice sample- Boiling points'
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/JRxFsmvsmHY/default.jpg
    - https://i3.ytimg.com/vi/JRxFsmvsmHY/1.jpg
    - https://i3.ytimg.com/vi/JRxFsmvsmHY/2.jpg
    - https://i3.ytimg.com/vi/JRxFsmvsmHY/3.jpg
---
Comparing the boiling point of nonane and 2,3,4-trifluoropentane. A sample multiple choice problem from the 2014 AP course description.
