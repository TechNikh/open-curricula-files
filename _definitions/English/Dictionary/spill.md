---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spill
offline_file: ""
offline_thumbnail: ""
uuid: a66c92de-cfd1-44fc-b6e3-fe0bd5ec9009
updated: 1484310581
title: spill
categories:
    - Dictionary
---
spill
     n 1: liquid that is spilled; "clean up the spills"
     2: a channel that carries excess water over or around a dam or
        other obstruction [syn: {spillway}, {wasteweir}]
     3: the act of allowing a fluid to escape [syn: {spillage}, {release}]
     4: a sudden drop from an upright position; "he had a nasty
        spill on the ice" [syn: {tumble}, {fall}]
     v 1: cause or allow (a liquid substance) to run or flow from a
          container; "spill the milk"; "splatter water" [syn: {slop},
           {splatter}]
     2: flow, run or fall out and become lost; "The milk spilled
        across the floor"; "The wine spilled onto the table" [syn:
         {run out}]
     3: ...
