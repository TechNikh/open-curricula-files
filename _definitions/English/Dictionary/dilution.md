---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dilution
offline_file: ""
offline_thumbnail: ""
uuid: c121008c-d83f-4c57-8d23-7a5bcd9a612e
updated: 1484310381
title: dilution
categories:
    - Dictionary
---
dilution
     n 1: a diluted solution
     2: weakening (reducing the concentration) by the addition of
        water or a thinner [ant: {concentration}]
