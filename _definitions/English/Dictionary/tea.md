---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tea
offline_file: ""
offline_thumbnail: ""
uuid: 59b56416-9983-4b4e-be3f-4e957a274780
updated: 1484310344
title: tea
categories:
    - Dictionary
---
tea
     n 1: a beverage made by steeping tea leaves in water; "iced tea
          is a cooling drink"
     2: a light midafternoon meal of tea and sandwiches or cakes;
        "an Englishman would interrupt a war to have his afternoon
        tea" [syn: {afternoon tea}, {teatime}]
     3: dried leaves of the tea shrub; used to make tea; "the store
        shelves held many different kinds of tea"; "they threw the
        tea into Boston harbor" [syn: {tea leaf}]
     4: a reception or party at which tea is served; "we met at the
        Dean's tea for newcomers"
     5: a tropical evergreen shrub or small tree extensively
        cultivated in e.g. China and Japan and India; source of
     ...
