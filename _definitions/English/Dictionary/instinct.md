---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instinct
offline_file: ""
offline_thumbnail: ""
uuid: 3759fc76-0171-465d-996b-0ec6d2ec2ba3
updated: 1484310569
title: instinct
categories:
    - Dictionary
---
instinct
     adj : (followed by `with')deeply filled or permeated; "imbued with
           the spirit of the Reformation"; "words instinct with
           love"; "it is replete with misery" [syn: {instinct(p)},
            {replete(p)}]
     n : inborn pattern of behavior often responsive to specific
         stimuli; "the spawning instinct in salmon"; "altruistic
         instincts in social animals" [syn: {inherent aptitude}]
