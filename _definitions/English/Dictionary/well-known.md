---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well-known
offline_file: ""
offline_thumbnail: ""
uuid: 5c6a5f69-9e09-4ae5-a7e6-1e85feff1229
updated: 1484310429
title: well-known
categories:
    - Dictionary
---
well-known
     adj 1: widely or fully known; "a well-known politician";
            "well-known facts"; "a politician who is well known";
            "these facts are well known"
     2: frequently experienced; known closely or intimately; "a
        long-familiar face"; "a well-known voice reached her ears"
        [syn: {long-familiar}, {well-known(a)}]
