---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dilemma
offline_file: ""
offline_thumbnail: ""
uuid: 08709fcd-4cc4-4fe9-98d9-c581ea2ef6ac
updated: 1484310533
title: dilemma
categories:
    - Dictionary
---
dilemma
     n : state of uncertainty or perplexity especially as requiring a
         choice between equally unfavorable options [syn: {quandary}]
