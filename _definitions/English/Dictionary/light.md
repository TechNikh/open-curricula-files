---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/light
offline_file: ""
offline_thumbnail: ""
uuid: 56c6b33e-896d-4f07-9280-d9f97d5d9356
updated: 1484310305
title: light
categories:
    - Dictionary
---
light
     adj 1: of comparatively little physical weight or density; "a light
            load"; "magnesium is a light metal--having a specific
            gravity of 1.74 at 20 degrees C" [ant: {heavy}]
     2: (used of color) having a relatively small amount of coloring
        agent; "light blue"; "light colors such as pastels"; "a
        light-colored powder" [syn: {light-colored}] [ant: {dark}]
     3: of the military or industry; using (or being) relatively
        small or light arms or equipment; "light infantry"; "light
        cavalry"; "light industry"; "light weapons" [ant: {heavy}]
     4: not great in degree or quantity or number; "a light
        sentence"; "a light ...
