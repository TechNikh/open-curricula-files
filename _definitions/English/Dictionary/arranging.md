---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arranging
offline_file: ""
offline_thumbnail: ""
uuid: eed83043-13f7-4487-b9f1-3ede0a20afdd
updated: 1484310399
title: arranging
categories:
    - Dictionary
---
arranging
     n : the act of arranging and adapting a piece of music [syn: {arrangement},
          {transcription}]
