---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Courant_algebroid
offline_file: ""
offline_thumbnail: ""
uuid: efcc3972-4b51-4c4a-866c-89aa8330e207
updated: 1484309215
title: Courant algebroid
tags:
    - Definition
    - Skew-Symmetric Definition
    - Properties
    - Examples
    - Dirac structures
    - Examples
    - Generalized complex structures
    - Examples
categories:
    - Differential geometry
---
In a field of mathematics known as differential geometry, a Courant algebroid is a structure which, in a certain sense, blends the concepts of Lie algebroid and of quadratic Lie algebra. This notion, which plays a fundamental role in the study of Hitchin's generalized complex structures, was originally introduced by Zhang-Ju Liu, Alan Weinstein and Ping Xu in their investigation of doubles of Lie bialgebroids in 1997.[1] Liu, Weinstein and Xu named it after Courant, who had implicitly devised earlier in 1990[2] the standard prototype of Courant algebroid through his discovery of a skew symmetric bracket on 
  
    
      
        T
        M
        ⊕
        
          T
          
      ...
