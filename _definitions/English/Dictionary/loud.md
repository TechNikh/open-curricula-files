---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loud
offline_file: ""
offline_thumbnail: ""
uuid: adef9006-0250-401d-86ab-141427e68937
updated: 1484310144
title: loud
categories:
    - Dictionary
---
loud
     adj 1: characterized by or producing sound of great volume or
            intensity; "a group of loud children"; "loud thunder";
            "her voice was too loud"; "loud trombones" [ant: {soft}]
     2: tastelessly showy; "a flash car"; "a flashy ring"; "garish
        colors"; "a gaudy costume"; "loud sport shirts"; "a
        meretricious yet stylish book"; "tawdry ornaments" [syn: {brassy},
         {cheap}, {flash}, {flashy}, {garish}, {gaudy}, {gimcrack},
         {meretricious}, {tacky}, {tatty}, {tawdry}, {trashy}]
     3: used chiefly as a direction or description in music; "the
        forte passages in the composition" [syn: {forte}] [ant: {piano}]
     adv : with ...
