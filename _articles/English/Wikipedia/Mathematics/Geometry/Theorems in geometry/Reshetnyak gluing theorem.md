---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reshetnyak_gluing_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 134cbc94-d53e-4ce5-a7ba-feccae07aeec
updated: 1484309126
title: Reshetnyak gluing theorem
categories:
    - Theorems in geometry
---
In metric geometry, the Reshetnyak gluing theorem gives information on the structure of a geometric object build by using as building blocks other geometric objects, belonging to a well defined class. Intuitively, it states that a manifold obtained by joining (i.e. "gluing") together, in a precisely defined way, other manifolds having a given property inherit that very same property.
