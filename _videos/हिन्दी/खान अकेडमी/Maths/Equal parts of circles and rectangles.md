---
version: 1
type: video
provider: YouTube
id: V6pZa4DQ5N8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fb12a667-2239-433a-9b75-7c676cf8f08a
updated: 1486069614
title: Equal parts of circles and rectangles
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/V6pZa4DQ5N8/default.jpg
    - https://i3.ytimg.com/vi/V6pZa4DQ5N8/1.jpg
    - https://i3.ytimg.com/vi/V6pZa4DQ5N8/2.jpg
    - https://i3.ytimg.com/vi/V6pZa4DQ5N8/3.jpg
---
