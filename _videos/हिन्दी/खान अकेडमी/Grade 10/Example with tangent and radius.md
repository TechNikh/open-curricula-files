---
version: 1
type: video
provider: YouTube
id: riZ0muH4Om0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a2101136-f0d9-4548-9e1e-20ff650afac6
updated: 1486069617
title: Example with tangent and radius
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/riZ0muH4Om0/default.jpg
    - https://i3.ytimg.com/vi/riZ0muH4Om0/1.jpg
    - https://i3.ytimg.com/vi/riZ0muH4Om0/2.jpg
    - https://i3.ytimg.com/vi/riZ0muH4Om0/3.jpg
---
