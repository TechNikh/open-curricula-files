---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sanitation
offline_file: ""
offline_thumbnail: ""
uuid: 21a7229f-d2c3-4d2e-874c-29a892e43489
updated: 1484310462
title: sanitation
categories:
    - Dictionary
---
sanitation
     n 1: the state of being clean and conducive to health
     2: making something sanitary (free of germs) as by sterilizing
        [syn: {sanitization}, {sanitisation}]
