---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stabilise
offline_file: ""
offline_thumbnail: ""
uuid: 5a0fa30e-548d-4fd8-8005-38777b926da4
updated: 1484310561
title: stabilise
categories:
    - Dictionary
---
stabilise
     v 1: support or hold steady and make steadfast, with or as if
          with a brace; "brace your elbows while working on the
          potter's wheel" [syn: {brace}, {steady}, {stabilize}]
     2: become stable or more stable; "The economy stabilized" [syn:
         {stabilize}] [ant: {destabilize}, {destabilize}]
     3: make stable and keep from fluctuating or put into an
        equilibrium; "The drug stabilized her blood pressure";
        "stabilize prices" [syn: {stabilize}] [ant: {destabilize},
         {destabilize}]
