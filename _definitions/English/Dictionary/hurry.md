---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurry
offline_file: ""
offline_thumbnail: ""
uuid: 1d5bf6c9-7e0b-4120-a325-501cb34cf2bd
updated: 1484310333
title: hurry
categories:
    - Dictionary
---
hurry
     n 1: a condition of urgency making it necessary to hurry; "in a
          hurry to lock the door" [syn: {haste}]
     2: overly eager speed (and possible carelessness); "he soon
        regretted his haste" [syn: {haste}, {hastiness}, {hurriedness},
         {precipitation}]
     3: the act of moving hurriedly and in a careless manner; "in
        his haste to leave he forgot his book" [syn: {haste}, {rush},
         {rushing}]
     v 1: move very fast; "The runner zipped past us at breakneck
          speed" [syn: {travel rapidly}, {speed}, {zip}]
     2: act or move at high speed; "We have to rush!"; "hurry--it's
        late!" [syn: {rush}, {hasten}, {look sharp}, ...
