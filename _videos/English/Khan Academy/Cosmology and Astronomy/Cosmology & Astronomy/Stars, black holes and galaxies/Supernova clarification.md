---
version: 1
type: video
provider: YouTube
id: QMv4kxATfzs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Supernova%20clarification.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1741c668-a757-4225-b06c-e12827a0b0b5
updated: 1486069685
title: Supernova clarification
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/QMv4kxATfzs/default.jpg
    - https://i3.ytimg.com/vi/QMv4kxATfzs/1.jpg
    - https://i3.ytimg.com/vi/QMv4kxATfzs/2.jpg
    - https://i3.ytimg.com/vi/QMv4kxATfzs/3.jpg
---
