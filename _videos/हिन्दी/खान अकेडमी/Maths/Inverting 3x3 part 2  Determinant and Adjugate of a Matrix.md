---
version: 1
type: video
provider: YouTube
id: kThkOjhbtWY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6c34fb95-e6dc-4f52-b04a-5fb2d729d772
updated: 1486069614
title: 'Inverting 3x3 part 2  Determinant and Adjugate of a Matrix'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/kThkOjhbtWY/default.jpg
    - https://i3.ytimg.com/vi/kThkOjhbtWY/1.jpg
    - https://i3.ytimg.com/vi/kThkOjhbtWY/2.jpg
    - https://i3.ytimg.com/vi/kThkOjhbtWY/3.jpg
---
