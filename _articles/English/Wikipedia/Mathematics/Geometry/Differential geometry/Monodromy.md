---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monodromy
offline_file: ""
offline_thumbnail: ""
uuid: 6b277875-3e69-44de-a7c2-b2e6f19fd37e
updated: 1484309228
title: Monodromy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Imaginary_log_analytic_continuation.png
tags:
    - Definition
    - Example
    - Differential equations in the complex domain
    - Topological and geometric aspects
    - Monodromy groupoid and foliations
    - Definition via Galois theory
    - Notes
categories:
    - Differential geometry
---
In mathematics, monodromy is the study of how objects from mathematical analysis, algebraic topology, algebraic geometry and differential geometry behave as they 'run round' a singularity. As the name implies, the fundamental meaning of monodromy comes from 'running round singly'. It is closely associated with covering maps and their degeneration into ramification; the aspect giving rise to monodromy phenomena is that certain functions we may wish to define fail to be single-valued as we 'run round' a path encircling a singularity. The failure of monodromy can be measured by defining a monodromy group: a group of transformations acting on the data that encodes what does happen as we 'run ...
