---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antagonistic
offline_file: ""
offline_thumbnail: ""
uuid: ac7da326-b371-448d-809b-a7a19de5de10
updated: 1484310178
title: antagonistic
categories:
    - Dictionary
---
antagonistic
     adj 1: indicating opposition or resistance [syn: {counter}]
     2: characterized by antagonism or antipathy; "slaves
        antagonistic to their masters"; "antipathetic factions
        within the party" [syn: {antipathetic}, {antipathetical}]
     3: arousing animosity or hostility; "his antagonizing
        brusqueness"; "Europe was antagonistic to the Unites
        States" [ant: {conciliatory}]
     4: used especially of drugs or muscles that counteract or
        neutralize each other's effect [syn: {incompatible}] [ant:
         {synergistic}]
     5: opposing or neutralizing or mitigating an effect by contrary
        action [syn: {counteractive}] [ant: ...
