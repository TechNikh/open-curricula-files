---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negamax
offline_file: ""
offline_thumbnail: ""
uuid: 76c8d73e-599f-45a5-a09d-bc17b201ee97
updated: 1484309006
title: Negamax
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Plain_Negamax.gif
tags:
    - Negamax base algorithm
    - Negamax with alpha beta pruning
    - Negamax with alpha beta pruning and transposition tables
categories:
    - Game artificial intelligence
---
This algorithm relies on the fact that max(a, b) = −min(−a, −b) to simplify the implementation of the minimax algorithm. More precisely, the value of a position to player A in such a game is the negation of the value to player B. Thus, the player on move looks for a move that maximizes the negation of the value resulting from the move: this successor position must by definition have been valued by the opponent. The reasoning of the previous sentence works regardless of whether A or B is on move. This means that a single procedure can be used to value both positions. This is a coding simplification over minimax, which requires that A selects the move with the maximum-valued successor ...
