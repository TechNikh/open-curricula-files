---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frontline
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516881
title: frontline
categories:
    - Dictionary
---
front line
     n : the line along which opposing armies face each other [syn: {battlefront},
          {front}]
