---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Symposium_on_Computational_Geometry
offline_file: ""
offline_thumbnail: ""
uuid: 6edeca7a-74f2-4c74-937b-0be5ea2418d6
updated: 1484309511
title: Symposium on Computational Geometry
categories:
    - Computational geometry
---
The Annual Symposium on Computational Geometry (SoCG) is an academic conference in computational geometry.[1] It was founded in 1985, and in most but not all of its years it has been sponsored by the Association for Computing Machinery's SIGACT and SIGGRAPH special interest groups.[2] After few years of discussion, the computational geometry community decides to leave ACM and organize the conference on its own. The motivations of this decision was the difficulties to organize something with ACM outside USA and the possibility of turning to an open access system of publication. Beginning in 2015 the conference proceedings are published by the Leibniz International Proceedings in Informatics ...
