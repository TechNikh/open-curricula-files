---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ken
offline_file: ""
offline_thumbnail: ""
uuid: 4f9e6c83-25f7-4f3c-b4d8-bb9110b18a48
updated: 1484310459
title: ken
categories:
    - Dictionary
---
ken
     n 1: range of what one can know or understand; "beyond my ken"
          [syn: {cognizance}]
     2: the range of vision; "out of sight of land" [syn: {sight}]
     [also: {kent}, {kenning}, {kenned}]
