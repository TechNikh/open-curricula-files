---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waste
offline_file: ""
offline_thumbnail: ""
uuid: 4e46b377-be1b-48ea-ab04-0d8b50ba27e9
updated: 1484310337
title: waste
categories:
    - Dictionary
---
waste
     adj 1: disposed of as useless; "waste paper" [syn: {cast-off(a)}, {discarded},
             {junked}, {scrap(a)}]
     2: located in a dismal or remote area; desolate; "a desert
        island"; "a godforsaken wilderness crossroads"; "a wild
        stretch of land"; "waste places" [syn: {desert}, {godforsaken},
         {wild}]
     n 1: any materials unused and rejected as worthless or unwanted;
          "they collect the waste once a week"; "much of the waste
          material is carried off in the sewers" [syn: {waste
          material}, {waste matter}, {waste product}]
     2: useless or profitless activity; using or expending or
        consuming thoughtlessly or ...
