---
version: 1
type: article
id: https://en.wikipedia.org/wiki/The_Collapse_of_Chaos
offline_file: ""
offline_thumbnail: ""
uuid: 2f4645c1-d5ba-45e3-9efe-55932abefebf
updated: 1484308709
title: The Collapse of Chaos
categories:
    - Chaos theory
---
The Collapse of Chaos: Discovering Simplicity in a Complex World (1994) is a book about complexity theory and the nature of scientific explanation written by biologist Jack Cohen and mathematician Ian Stewart.
