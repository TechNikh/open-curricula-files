---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conveying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522221
title: conveying
categories:
    - Dictionary
---
conveying
     n : act of transferring property title from one person to
         another [syn: {conveyance}, {conveyance of title}, {conveyancing}]
