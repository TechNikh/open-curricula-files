---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_regular_graph
offline_file: ""
offline_thumbnail: ""
uuid: 55b33ae5-e8ca-4980-bfcf-065eaab64baf
updated: 1484309452
title: Random regular graph
categories:
    - Random graphs
---
A random r-regular graph is a graph selected from 
  
    
      
        
          
            
              G
            
          
          
            n
            ,
            r
          
        
      
    
    {\displaystyle {\mathcal {G}}_{n,r}}
  
, which denotes the probability space of all r-regular graphs on n vertices, where 3 ≤ r < n and nr is even.[1] It is therefore a particular kind of random graph, but the regularity restriction significantly alters the properties that will hold, since most graphs are not regular.
