---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guilt
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487181
title: guilt
categories:
    - Dictionary
---
guilt
     n 1: the state of having committed an offense [syn: {guiltiness}]
          [ant: {innocence}]
     2: remorse caused by feeling responsible for some offence [syn:
         {guilty conscience}, {guilt feelings}, {guilt trip}]
