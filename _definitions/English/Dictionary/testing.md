---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/testing
offline_file: ""
offline_thumbnail: ""
uuid: c56e7745-479b-4cf2-80b2-bbcaba4e11b8
updated: 1484310311
title: testing
categories:
    - Dictionary
---
testing
     n 1: the act of subjecting to experimental test in order to
          determine how well something works; "they agreed to end
          the testing of atomic weapons"
     2: an examination of the characteristics of something; "there
        are laboratories for commercial testing"; "it involved
        testing thousands of children for smallpox"
     3: the act of giving students or candidates a test (as by
        questions) to determine what they know or have learned
        [syn: {examination}]
