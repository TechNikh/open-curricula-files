---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edmonds_matrix
offline_file: ""
offline_thumbnail: ""
uuid: ce01ee9b-decb-456e-a3c8-ebc666efdef0
updated: 1484309376
title: Edmonds matrix
categories:
    - Algebraic graph theory
---
In graph theory, the Edmonds matrix 
  
    
      
        A
      
    
    {\displaystyle A}
  
 of a balanced bipartite graph 
  
    
      
        G
        (
        U
        ,
        V
        ,
        E
        )
      
    
    {\displaystyle G(U,V,E)}
  
 with sets of vertices 
  
    
      
        U
        =
        {
        
          u
          
            1
          
        
        ,
        
          u
          
            2
          
        
        ,
        …
        ,
        
          u
          
            n
          
        
        }
      
    
    {\displaystyle U=\{u_{1},u_{2},\dots ,u_{n}\}}
  
 and 
  
    
      
        V
        =
    ...
