---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plus_and_minus_signs
offline_file: ""
offline_thumbnail: ""
uuid: bbd6397c-692a-4d30-8ce1-00603e346534
updated: 1484308924
title: Plus and minus signs
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Plus_Minus_Hyphen-minus.svg.png
tags:
    - History
    - Plus sign
    - Minus sign
    - Use in elementary education
    - Use as a qualifier
    - Uses in computing
    - Other uses
    - Character codes
    - Alternative plus sign
    - References and footnotes
categories:
    - Elementary arithmetic
---
The plus and minus signs (+ and −) are mathematical symbols used to represent the notions of positive and negative as well as the operations of addition and subtraction. Their use has been extended to many other meanings, more or less analogous. Plus and minus are Latin terms meaning "more" and "less", respectively.
