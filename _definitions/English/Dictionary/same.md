---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/same
offline_file: ""
offline_thumbnail: ""
uuid: da0c1b56-4fb8-438b-a41d-e60e13ecc1a4
updated: 1484310351
title: same
categories:
    - Dictionary
---
same
     adj 1: same in identity; "the same man I saw yesterday"; "never
            wore the same dress twice"; "this road is the same one
            we were on yesterday"; "on the same side of the
            street" [ant: {other}]
     2: closely similar or comparable in kind or quality or quantity
        or degree; "curtains the same color as the walls"; "two
        girls of the same age"; "mother and son have the same blue
        eyes"; "animals of the same species"; "the same rules as
        before"; "two boxes having the same dimensions"; "the same
        day next year" [ant: {different}]
     3: equal in amount or value; "like amounts"; "equivalent
        amounts"; "the same ...
