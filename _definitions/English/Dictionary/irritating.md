---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irritating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449021
title: irritating
categories:
    - Dictionary
---
irritating
     adj 1: causing irritation or annoyance; "tapping an annoying rhythm
            on his glass with his fork"; "aircraft noise is
            particularly bothersome near the airport"; "found it
            galling to have to ask permission"; "an irritating
            delay"; "nettlesome paperwork"; "a pesky mosquito";
            "swarms of pestering gnats"; "a plaguey newfangled
            safety catch"; "a teasing and persistent thought
            annoyed him"; "a vexatious child"; "it is vexing to
            have to admit you are wrong" [syn: {annoying}, {bothersome},
             {galling}, {nettlesome}, {pesky}, {pestering}, {pestiferous},
             {plaguy}, ...
