---
version: 1
type: video
provider: YouTube
id: Ay0B6Kh9Y3Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Fractional%20exponent%20expressions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a3730f73-46fa-4609-8dbf-2a3180af8c2c
updated: 1486069559
title: Fractional exponent expressions 3
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ay0B6Kh9Y3Q/default.jpg
    - https://i3.ytimg.com/vi/Ay0B6Kh9Y3Q/1.jpg
    - https://i3.ytimg.com/vi/Ay0B6Kh9Y3Q/2.jpg
    - https://i3.ytimg.com/vi/Ay0B6Kh9Y3Q/3.jpg
---
