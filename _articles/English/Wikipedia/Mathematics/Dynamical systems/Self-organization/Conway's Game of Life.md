---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life'
offline_file: ""
offline_thumbnail: ""
uuid: 0954dff1-8348-4706-a033-6b97ca8bba44
updated: 1484308828
title: "Conway's Game of Life"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Gospers_glider_gun.gif
tags:
    - Rules
    - Origins
    - Examples of patterns
    - Undecidability
    - Self-replication
    - Iteration
    - Algorithms
    - Variations on Life
    - Music
    - Notable Life programs
categories:
    - Self-organization
---
The "game" is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves, or, for advanced "players", by creating patterns with particular properties.
