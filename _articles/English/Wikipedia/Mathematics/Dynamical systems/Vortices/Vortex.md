---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex
offline_file: ""
offline_thumbnail: ""
uuid: 7e5a3413-4668-492e-ba67-adf9f13dd7c1
updated: 1484308874
title: Vortex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Airplane_vortex_edit.jpg
tags:
    - Properties
    - Vorticity
    - Vortex types
    - Irrotational vortices
    - Rotational vortices
    - Vortex geometry
    - Pressure in a vortex
    - Evolution
    - Two-dimensional modeling
    - Further examples
    - Notes
    - Other
categories:
    - Vortices
---
In fluid dynamics, a vortex is a region in a fluid in which the flow is rotating around an axis line, which may be straight or curved.[1][2] The plural of vortex is either vortices or vortexes.[3][4] Vortices form in stirred fluids, and may be observed in phenomena such as smoke rings, whirlpools in the wake of boat, or the winds surrounding a tornado or dust devil.
