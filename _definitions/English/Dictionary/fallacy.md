---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fallacy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484354521
title: fallacy
categories:
    - Dictionary
---
fallacy
     n : a misconception resulting from incorrect reasoning [syn: {false
         belief}]
