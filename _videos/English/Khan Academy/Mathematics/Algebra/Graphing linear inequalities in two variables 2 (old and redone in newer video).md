---
version: 1
type: video
provider: YouTube
id: lxTQrsUip9g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Graphing%20linear%20inequalities%20in%20two%20variables%202%20%28old%20and%20redone%20in%20newer%20video%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7fc3288a-b044-4680-b03b-2840f98226a7
updated: 1486069559
title: >
    Graphing linear inequalities in two variables 2 (old and
    redone in newer video)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/lxTQrsUip9g/default.jpg
    - https://i3.ytimg.com/vi/lxTQrsUip9g/1.jpg
    - https://i3.ytimg.com/vi/lxTQrsUip9g/2.jpg
    - https://i3.ytimg.com/vi/lxTQrsUip9g/3.jpg
---
