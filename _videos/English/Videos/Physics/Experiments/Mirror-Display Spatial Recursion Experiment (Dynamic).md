---
version: 1
type: video
provider: YouTube
id: xVLoi4ILieE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f28be987-5136-4187-9f92-5f0bc9d18891
updated: 1486069634
title: Mirror/Display Spatial Recursion Experiment (Dynamic)
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/xVLoi4ILieE/default.jpg
    - https://i3.ytimg.com/vi/xVLoi4ILieE/1.jpg
    - https://i3.ytimg.com/vi/xVLoi4ILieE/2.jpg
    - https://i3.ytimg.com/vi/xVLoi4ILieE/3.jpg
---
