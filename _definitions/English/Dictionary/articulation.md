---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/articulation
offline_file: ""
offline_thumbnail: ""
uuid: 0a681b8c-ffe8-4c0f-844d-c51538e15060
updated: 1484310607
title: articulation
categories:
    - Dictionary
---
articulation
     n 1: the aspect of pronunciation that involves bringing
          articulatory organs together so as to shape the sounds
          of speech
     2: the shape or manner in which things come together and a
        connection is made [syn: {join}, {joint}, {juncture}, {junction}]
     3: expressing in coherent verbal form; "the articulation of my
        feelings"; "I gave voice to my feelings" [syn: {voice}]
     4: (anatomy) the point of connection between two bones or
        elements of a skeleton (especially if the articulation
        allows motion) [syn: {joint}, {articulatio}]
     5: the act of joining things in such a way that motion is
        possible
