---
version: 1
type: video
provider: YouTube
id: omFelSZvaJc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Multiplication%20and%20division/Partial%20quotient%20method%20of%20division%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 16b91a27-36af-4392-b369-6afbd691fb57
updated: 1486069567
title: Partial quotient method of division 2
categories:
    - Multiplication and division
thumbnail_urls:
    - https://i3.ytimg.com/vi/omFelSZvaJc/default.jpg
    - https://i3.ytimg.com/vi/omFelSZvaJc/1.jpg
    - https://i3.ytimg.com/vi/omFelSZvaJc/2.jpg
    - https://i3.ytimg.com/vi/omFelSZvaJc/3.jpg
---
Another example of doing long division using the partial quotient method

Watch the next lesson: https://www.khanacademy.org/math/arithmetic/multiplication-division/advanced-mult-div-word-problems/v/multi-step-word-problems-with-whole-numbers-exercise-1?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Missed the previous lesson? 
https://www.khanacademy.org/math/arithmetic/multiplication-division/partial_quotient_division/v/partial-quotient-division?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Arithmetic on Khan Academy: So you're ready to have some arithmetic fun? You've come to the right spot! It's the first "official" math topic and chalked full of fun exercises and great videos which help you start your journey towards math mastery. We'll cover the big ones: addition, subtraction, multiplication, and division, of course. But we don't stop there. We'll get into negative numbers, absolute value, decimals, and fractions, too. Learning math should be fun, and we plan on having some with you. Ready to get started?

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Arithmetic channel: https://www.youtube.com/channel/UCFlJkgTNeC-FYYIQnyE5yzQ?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
