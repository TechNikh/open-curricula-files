---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recharge
offline_file: ""
offline_thumbnail: ""
uuid: 14120fec-39e4-4564-b2e6-f3a20df93684
updated: 1484310258
title: recharge
categories:
    - Dictionary
---
recharge
     v 1: load anew with ammunition, "She reloaded the gun carefully"
          [syn: {reload}]
     2: charge anew; "recharge a battery"
