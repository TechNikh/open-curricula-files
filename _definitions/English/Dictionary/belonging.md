---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belonging
offline_file: ""
offline_thumbnail: ""
uuid: 5778b90d-9d10-490f-aa66-c45c81f2c9a9
updated: 1484310283
title: belonging
categories:
    - Dictionary
---
belonging
     n : happiness felt in a secure relationship; "with his
         classmates he felt a sense of belonging"
