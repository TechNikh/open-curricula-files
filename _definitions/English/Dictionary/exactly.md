---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exactly
offline_file: ""
offline_thumbnail: ""
uuid: e5d9cc75-6b4e-45f1-b8be-1b1818af0333
updated: 1484310311
title: exactly
categories:
    - Dictionary
---
exactly
     adv 1: indicating exactness or preciseness; "he was doing precisely
            (or exactly) what she had told him to do"; "it was
            just as he said--the jewel was gone"; "it has just
            enough salt" [syn: {precisely}, {just}]
     2: just as it should be; "`Precisely, my lord,' he said" [syn:
        {precisely}, {on the nose}, {on the dot}, {on the button}]
     3: in a precise manner; "she always expressed herself
        precisely" [syn: {precisely}, {incisively}] [ant: {imprecisely},
         {imprecisely}]
