---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lead
offline_file: ""
offline_thumbnail: ""
uuid: 554c16f0-975f-4b0a-9b5b-11137a00c42b
updated: 1484310363
title: lead
categories:
    - Dictionary
---
lead
     n 1: a soft heavy toxic malleable metallic element; bluish white
          when freshly cut but tarnishes readily to dull gray;
          "the children were playing with lead soldiers" [syn: {Pb},
           {atomic number 82}]
     2: an advantage held by a competitor in a race; "he took the
        lead at the last turn"
     3: evidence pointing to a possible solution; "the police are
        following a promising lead"; "the trail led straight to
        the perpetrator" [syn: {track}, {trail}]
     4: a position of leadership (especially in the phrase `take the
        lead'); "he takes the lead in any group"; "we were just
        waiting for someone to take the lead"; "they ...
