---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Aristarchus%27_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: ccffffe9-bce6-4257-8794-2d5b72d0942e
updated: 1484309340
title: "Aristarchus' inequality"
categories:
    - Trigonometry
---
Aristarchus' inequality (after the Greek astronomer and mathematician Aristarchus of Samos; c. 310 – c. 230 BCE) is a law of trigonometry which states that if α and β are acute angles (i.e. between 0 and a right angle) and β < α then
