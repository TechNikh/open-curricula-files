---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/closure
offline_file: ""
offline_thumbnail: ""
uuid: 0fd5b95f-97eb-430c-8c38-8a48002ee9b9
updated: 1484310142
title: closure
categories:
    - Dictionary
---
closure
     n 1: approaching a particular destination; a coming closer; a
          narrowing of a gap; "the ship's rapid rate of closing
          gave them little time to avoid a collision" [syn: {closing}]
     2: a rule for limiting or ending debate in a deliberative body
        [syn: {cloture}, {gag rule}, {gag law}]
     3: a Gestalt principle of organization holding that there is an
        innate tendency to perceive incomplete objects as complete
        and to close or fill gaps and to perceive asymmetric
        stimuli as symmetric [syn: {law of closure}]
     4: something settled or resolved; the outcome of decision
        making; "the finally reached a settlement with the ...
