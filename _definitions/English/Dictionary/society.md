---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/society
offline_file: ""
offline_thumbnail: ""
uuid: e0caa875-eb4c-4384-9096-7dcf724e77ed
updated: 1484310284
title: society
categories:
    - Dictionary
---
society
     n 1: an extended social group having a distinctive cultural and
          economic organization
     2: a formal association of people with similar interests; "he
        joined a golf club"; "they formed a small lunch society";
        "men from the fraternal order will staff the soup kitchen
        today" [syn: {club}, {guild}, {gild}, {lodge}, {order}]
     3: the state of being with someone; "he missed their company";
        "he enjoyed the society of his friends" [syn: {company}, {companionship},
         {fellowship}]
     4: the fashionable elite [syn: {high society}, {beau monde}, {smart
        set}, {bon ton}]
