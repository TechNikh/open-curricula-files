---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jewellery
offline_file: ""
offline_thumbnail: ""
uuid: e4059e6c-cd06-4d74-8c0b-7ecb13017092
updated: 1484310409
title: jewellery
categories:
    - Dictionary
---
jewellery
     n : an adornment (as a bracelet or ring or necklace) made of
         precious metals and set with gems (or imitation gems)
         [syn: {jewelry}]
