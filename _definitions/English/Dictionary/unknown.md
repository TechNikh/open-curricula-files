---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unknown
offline_file: ""
offline_thumbnail: ""
uuid: 787fccd2-23d7-4a30-955b-6432e10f82a2
updated: 1484310244
title: unknown
categories:
    - Dictionary
---
unknown
     adj 1: not known; "an unknown amount"; "an unknown island"; "an
            unknown writer"; "an unknown source" [ant: {known}]
     2: being or having an unknown or unnamed source; "a poem by an
        unknown author"; "corporations responsible to nameless
        owners"; "an unnamed donor" [syn: {nameless}, {unidentified},
         {unnamed}]
     3: not known to exist; "things obscurely felt surged up from
        unsuspected depths in her"- Edith Wharton
     4: not famous or acclaimed; "an obscure family"; "unsung heroes
        of the war" [syn: {obscure}, {unsung}]
     5: not known before; "used many strange words"; "saw many
        strange faces in the crowd"; ...
