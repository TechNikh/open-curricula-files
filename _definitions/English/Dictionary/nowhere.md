---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nowhere
offline_file: ""
offline_thumbnail: ""
uuid: a7c21991-8d1f-4f8f-b971-23c0b2aa6604
updated: 1484310453
title: nowhere
categories:
    - Dictionary
---
nowhere
     n : an insignificant place; "he came out of nowhere"
     adv : not anywhere; in or at or to no place; "I am going nowhere"
