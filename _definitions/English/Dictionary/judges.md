---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judges
offline_file: ""
offline_thumbnail: ""
uuid: 9df876cb-c1f4-4ca8-90de-9b018560c81b
updated: 1484310464
title: judges
categories:
    - Dictionary
---
Judges
     n : a book of the Old Testament that tells the history of Israel
         under the leaders known as judges [syn: {Book of Judges}]
