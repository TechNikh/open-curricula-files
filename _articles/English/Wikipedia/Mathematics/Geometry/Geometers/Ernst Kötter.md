---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ernst_K%C3%B6tter'
offline_file: ""
offline_thumbnail: ""
uuid: 51e4570c-4543-420d-83f8-6aa054cedcc9
updated: 1484309090
title: Ernst Kötter
categories:
    - Geometers
---
Ernst Kötter was a German mathematician who graduated in 1884 from Berlin University. His treatise "Fundamentals of a purely geometrical theory of algebraic plane curves" gained the 1886 prize of the Berlin Royal Academy.[2] In 1901, he published his report on "The development of synthetic geometry from Monge to Staudt (1847)";[3] it had been sent to the press as early as 1897, but completion was deferred by Kötter's appointment to Aachen University and a subsequent persisting illness.[4] He constructed a mobile wood model to illustrate the theorems of Dandelin spheres.[5][6]
