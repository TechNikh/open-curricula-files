---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formal_proof
offline_file: ""
offline_thumbnail: ""
uuid: 5254ad32-106f-4265-996c-d83e98905334
updated: 1484309495
title: Formal proof
tags:
    - Background
    - Formal language
    - Formal grammar
    - Formal systems
    - Interpretations
categories:
    - Proof theory
---
A formal proof or derivation is a finite sequence of sentences (called well-formed formulas in the case of a formal language), each of which is an axiom, an assumption, or follows from the preceding sentences in the sequence by a rule of inference. The last sentence in the sequence is a theorem of a formal system. The notion of theorem is not in general effective, therefore there may be no method by which we can always find a proof of a given sentence or determine that none exists. The concept of natural deduction is a generalization of the concept of proof.[1]
