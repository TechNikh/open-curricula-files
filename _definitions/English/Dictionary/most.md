---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/most
offline_file: ""
offline_thumbnail: ""
uuid: cd7dab79-8f4e-480e-8f9c-f5c13eed0bec
updated: 1484310353
title: most
categories:
    - Dictionary
---
most
     adj 1: (superlative of `many' used with count nouns and often
            preceded by `the') quantifier meaning the greatest in
            number; "who has the most apples?"; "most people like
            eggs"; "most fishes have fins" [syn: {most(a)}] [ant:
            {fewest(a)}]
     2: the superlative of `much' that can be used with mass nouns
        and is usually preceded by `the'; a quantifier meaning the
        greatest in amount or extent or degree; "made the most
        money he could"; "what attracts the most attention?";
        "made the most of a bad deal" [syn: {most(a)}] [ant: {least(a)}]
     adv 1: used to form the superlative; "the king cobra is the most
   ...
