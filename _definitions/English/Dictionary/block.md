---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/block
offline_file: ""
offline_thumbnail: ""
uuid: 98e7fa50-66f0-48be-8e3b-ae97ca67ff8c
updated: 1484310316
title: block
categories:
    - Dictionary
---
block
     n 1: a solid piece of something (usually having flat rectangular
          sides); "the pyramids were built with large stone
          blocks"
     2: a rectangular area in a city surrounded by streets and
        usually containing several buildings; "he lives in the
        next block" [syn: {city block}]
     3: a three-dimensional shape with six square or rectangular
        sides [syn: {cube}]
     4: a number or quantity of related things dealt with as a unit;
        "he reserved a large block of seats"; "he held a large
        block of the company's stock"
     5: housing in a large building that is divided into separate
        units; "there is a block of classrooms in ...
