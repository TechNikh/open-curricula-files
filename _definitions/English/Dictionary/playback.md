---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/playback
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484539801
title: playback
categories:
    - Dictionary
---
play back
     v : reproduce (a recording) on a recorder; "The lawyers played
         back the conversation to show that their client was
         innocent" [syn: {replay}]
