---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slimy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615161
title: slimy
categories:
    - Dictionary
---
slimy
     adj : covered with or resembling slime; "a slimy substance covered
           the rocks" [syn: {slimed}]
     [also: {slimiest}, {slimier}]
