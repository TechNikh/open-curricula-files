---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/musician
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535181
title: musician
categories:
    - Dictionary
---
musician
     n 1: someone who plays a musical instrument (as a profession)
          [syn: {instrumentalist}, {player}]
     2: artist who composes or conducts music as a profession
