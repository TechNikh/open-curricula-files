---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Halin%27s_grid_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: db64902f-df39-4165-9bd5-2c04a7e7c0e3
updated: 1484309411
title: "Halin's grid theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tiling_Regular_6-3_Hexagonal.svg.png
tags:
    - Definitions and statement
    - Analogues for finite graphs
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a branch of mathematics, Halin's grid theorem states that the infinite graphs with thick ends are exactly the graphs containing subdivisions of the hexagonal tiling of the plane.[1] It was published by Rudolf Halin (1965), and is a precursor to the work of Robertson and Seymour linking treewidth to large grid minors, which became an important component of the algorithmic theory of bidimensionality.
