---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Long_scale
offline_file: ""
offline_thumbnail: ""
uuid: b4588746-27fa-4d60-8a42-7dd08957ad3c
updated: 1484308363
title: Long and short scales
tags:
    - Comparison
    - History
    - Current usage
    - Short scale users
    - English-speaking
    - Arabic-speaking
    - Other short scale
    - Long scale users
    - Spanish-speaking
    - French-speaking
    - Portuguese-speaking
    - Dutch-speaking
    - Other long scale
    - Using both
    - Using neither
    - Notes on current usage
    - Short scale
    - Long scale
    - Both long and short scale
    - Neither long nor short scale
    - Alternative approaches
categories:
    - Uncategorized
---
For whole numbers less than a thousand million (< 109) the two scales are identical. From a thousand million up (≥ 109) the two scales diverge, using the same words for different numbers; this can cause misunderstanding.
