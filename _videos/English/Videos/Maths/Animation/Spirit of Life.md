---
version: 1
type: video
provider: YouTube
id: jgGVktBBcrg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 79756446-f2c7-4fda-9d77-0f7ad0aa8a5d
updated: 1486069649
title: Spirit of Life
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jgGVktBBcrg/default.jpg
    - https://i3.ytimg.com/vi/jgGVktBBcrg/1.jpg
    - https://i3.ytimg.com/vi/jgGVktBBcrg/2.jpg
    - https://i3.ytimg.com/vi/jgGVktBBcrg/3.jpg
---
