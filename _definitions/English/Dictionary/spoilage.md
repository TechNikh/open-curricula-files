---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spoilage
offline_file: ""
offline_thumbnail: ""
uuid: 61333368-12b0-4b02-887e-06cfd5fb3843
updated: 1484310377
title: spoilage
categories:
    - Dictionary
---
spoilage
     n 1: the amount that has spoiled
     2: the process of becoming spoiled [syn: {spoiling}]
     3: the act of spoiling something by causing damage to it; "her
        spoiling my dress was deliberate" [syn: {spoil}, {spoiling}]
