---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affairs
offline_file: ""
offline_thumbnail: ""
uuid: cd1fa658-7de3-4ee0-bdc6-84961ca16e83
updated: 1484310567
title: affairs
categories:
    - Dictionary
---
affairs
     n 1: matters of personal concern; "get his affairs in order"
          [syn: {personal business}, {personal matters}]
     2: transactions of professional or public interest; "news of
        current affairs"; "great affairs of state"
