---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tooth
offline_file: ""
offline_thumbnail: ""
uuid: 42a024d9-56e0-4b16-8a77-24b74e9fa93b
updated: 1484310383
title: tooth
categories:
    - Dictionary
---
tooth
     n 1: hard bonelike structures in the jaws of vertebrates; used
          for biting and chewing or for attack and defense
     2: something resembling the tooth of an animal
     3: toothlike structure in invertebrates found in the mouth or
        alimentary canal or on a shell
     4: a means of enforcement; "the treaty had no teeth in it"
     5: one of a number of uniform projections on a gear
     [also: {teeth} (pl)]
