---
version: 1
type: video
provider: YouTube
id: SVONb8IBXVQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Liquid%20Light%20-%20Chemical%20Reaction%20with%20Luminol.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 08ee7df6-e65c-4023-8aa1-7f998a314efe
updated: 1486069662
title: 'Liquid Light - Chemical Reaction with Luminol'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/SVONb8IBXVQ/default.jpg
    - https://i3.ytimg.com/vi/SVONb8IBXVQ/1.jpg
    - https://i3.ytimg.com/vi/SVONb8IBXVQ/2.jpg
    - https://i3.ytimg.com/vi/SVONb8IBXVQ/3.jpg
---
