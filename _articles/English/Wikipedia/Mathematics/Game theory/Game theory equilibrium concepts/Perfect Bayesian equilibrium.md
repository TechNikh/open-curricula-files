---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfect_Bayesian_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: a273c988-0a10-4fbf-b021-3dc616a68ec8
updated: 1484309032
title: Perfect Bayesian equilibrium
tags:
    - PBE in signaling games
    - Gift game 1
    - Gift game 2
    - More examples
    - PBE in multi-stage games
    - Repeated public-good game
    - Jump-bidding
categories:
    - Game theory equilibrium concepts
---
In game theory, a Perfect Bayesian Equilibrium (PBE) is an equilibrium concept relevant for dynamic games with incomplete information (sequential Bayesian games). A PBE is a refinement of both Bayesian Nash equilibrium (BNE) and subgame perfect equilibrium (SPE). A PBE has two components - strategies and beliefs:
