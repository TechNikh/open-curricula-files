---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surprisal_analysis
offline_file: ""
offline_thumbnail: ""
uuid: b8a1695e-08fa-43ac-81e1-dd6cb30232b7
updated: 1484308866
title: Surprisal analysis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Schematic_of_Surprisal_Analysis.jpg
tags:
    - History
    - Application
    - Surprisal analysis in physics
    - Surprisal analysis in biology and biomedical sciences
categories:
    - Thermodynamics
---
Surprisal analysis is an information-theoretical analysis technique that integrates and applies principles of thermodymamics and maximal entropy. Surprisal analysis is capable of relating the underlying microscopic properties to the macroscopic bulk properties of a system. It has already been applied to a spectrum of disciplines including engineering, physics, chemistry and biomedical engineering. Recently, it has been extended to characterize the state of living cells, specifically monitoring and characterizing biological processes in real time using transcriptional data.
