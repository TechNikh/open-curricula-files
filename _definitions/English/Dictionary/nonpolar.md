---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonpolar
offline_file: ""
offline_thumbnail: ""
uuid: 1fdb0e31-bd8d-4e4e-bdb6-6889e35f45c8
updated: 1484310407
title: nonpolar
categories:
    - Dictionary
---
nonpolar
     adj : not ionic; "a nonionic substance" [syn: {nonionic}] [ant: {ionic}]
