---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%BCchi_arithmetic'
offline_file: ""
offline_thumbnail: ""
uuid: 532abf32-6bd0-4f73-b75a-ac6338abe3da
updated: 1484309492
title: Büchi arithmetic
tags:
    - Büchi arithmetic and automaton
    - Properties of Büchi arithmetic
categories:
    - Proof theory
---
Büchi arithmetic of base k is the first-order theory of the natural numbers with addition and the function 
  
    
      
        
          V
          
            k
          
        
        (
        x
        )
      
    
    {\displaystyle V_{k}(x)}
  
 which is defined as the largest power of k dividing x, named in honor of the Swiss mathematician Julius Richard Büchi. The signature of Büchi arithmetic contains only the addition operation, 
  
    
      
        
          V
          
            k
          
        
      
    
    {\displaystyle V_{k}}
  
 and equality, omitting the multiplication operation entirely.
