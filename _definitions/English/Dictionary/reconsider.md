---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reconsider
offline_file: ""
offline_thumbnail: ""
uuid: ad721b40-5d43-4444-bf2a-3817ac7ab083
updated: 1484310605
title: reconsider
categories:
    - Dictionary
---
reconsider
     v 1: consider again; give new consideration to; usually with a
          view to changing; "Won't you reconsider your decision?"
     2: consider again (a bill) that had been voted upon before, in
        legislation
