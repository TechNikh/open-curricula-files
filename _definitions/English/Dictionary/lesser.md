---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lesser
offline_file: ""
offline_thumbnail: ""
uuid: 6641832d-a246-4d9d-acc4-d9c9dcf7a900
updated: 1484310426
title: lesser
categories:
    - Dictionary
---
lesser
     adj 1: of less size or importance; "the lesser anteater"; "the
            lesser of two evils" [ant: {greater}]
     2: smaller in size or amount or value; "the lesser powers of
        Europe"; "the lesser anteater"
