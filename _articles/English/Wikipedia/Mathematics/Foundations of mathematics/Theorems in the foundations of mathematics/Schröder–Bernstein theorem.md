---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schr%C3%B6der%E2%80%93Bernstein_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 341b66b3-69a1-4ff7-861d-fe69bb2ba4d4
updated: 1484308999
title: Schröder–Bernstein theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Cantor-Bernstein.png
tags:
    - Proof
    - Original proof
    - History
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In set theory, the Schröder–Bernstein theorem (named after Felix Bernstein and Ernst Schröder, also known as Cantor–Bernstein theorem, or Cantor–Schröder–Bernstein after Georg Cantor who first published it without proof) states that, if there exist injective functions f : A → B and g : B → A between the sets A and B, then there exists a bijective function h : A → B. In terms of the cardinality of the two sets, this means that if |A| ≤ |B| and |B| ≤ |A|, then |A| = |B|; that is, A and B are equipollent. This is a useful feature in the ordering of cardinal numbers.
