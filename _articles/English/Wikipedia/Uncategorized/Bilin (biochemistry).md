---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bile_pigments
offline_file: ""
offline_thumbnail: ""
uuid: 17ae9ee0-2fed-4c82-98ea-028102992c13
updated: 1484308358
title: Bilin (biochemistry)
categories:
    - Uncategorized
---
Bilins, bilanes or bile pigments are biological pigments formed in many organisms as a metabolic product of certain porphyrins. Bilin (also called bilichrome) was named as a bile pigment of mammals, but can also be found in lower vertebrates, invertebrates, as well as red algae, green plants and cyanobacteria. Bilins can range in color from red, orange, yellow or brown to blue or green.
