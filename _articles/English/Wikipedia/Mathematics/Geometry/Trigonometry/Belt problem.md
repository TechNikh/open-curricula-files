---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Belt_problem
offline_file: ""
offline_thumbnail: ""
uuid: 54b4dc01-8f80-4dc7-bd4e-db5410e1e68c
updated: 1484309340
title: Belt problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Belt_Problem_v2.svg.png
tags:
    - Solution
    - Pulley problem
    - Applications
categories:
    - Trigonometry
---
The belt problem is a mathematics problem which requires finding the length of a crossed belt that connects two circular pulleys with radius r1 and r2 whose centers are separated by a distance P. The solution of the belt problem requires trigonometry and the concepts of the bitangent line, the vertical angle, and congruent angles.
