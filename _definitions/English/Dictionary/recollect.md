---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recollect
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484552461
title: recollect
categories:
    - Dictionary
---
recollect
     v : recall knowledge from memory; have a recollection; "I can't
         remember saying any such thing"; "I can't think what her
         last name was"; "can you remember her phone number?"; "Do
         you remember that he once loved you?"; "call up memories"
         [syn: {remember}, {retrieve}, {recall}, {call back}, {call
         up}, {think}] [ant: {forget}]
