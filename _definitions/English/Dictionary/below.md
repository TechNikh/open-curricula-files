---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/below
offline_file: ""
offline_thumbnail: ""
uuid: 07528f8e-3110-4d95-915f-0266b36ee6e5
updated: 1484310315
title: below
categories:
    - Dictionary
---
below
     adv 1: in or to a place that is lower [syn: {at a lower place}, {to
            a lower place}, {beneath}] [ant: {above}]
     2: at a later place; "see below" [ant: {above}]
     3: (in writing) see below; "vide infra" [syn: {infra}]
     4: on a floor below; "the tenants live downstairs" [syn: {downstairs},
         {down the stairs}, {on a lower floor}] [ant: {upstairs}]
     5: further down; "see under for further discussion" [syn: {under}]
