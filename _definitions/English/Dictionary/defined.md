---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defined
offline_file: ""
offline_thumbnail: ""
uuid: 642ed022-9f75-4fd2-bc5e-079e720e064b
updated: 1484310224
title: defined
categories:
    - Dictionary
---
defined
     adj 1: clearly characterized or delimited; "lost in a maze of words
            both defined and undefined"; "each child has clearly
            defined duties" [ant: {undefined}]
     2: showing clearly the outline or profile or boundary; "hills
        defined against the evening sky"; "the setting sun showed
        the outlined figure of a man standing on the hill" [syn: {outlined}]
     3: clearly defined; "I have no formed opinion about the chances
        of success" [syn: {formed}, {settled}]
