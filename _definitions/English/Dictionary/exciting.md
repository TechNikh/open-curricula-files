---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exciting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604361
title: exciting
categories:
    - Dictionary
---
exciting
     adj 1: creating or arousing excitement; "an exciting account of her
            trip" [ant: {unexciting}]
     2: stimulating interest and discussion; "an exciting novel"
