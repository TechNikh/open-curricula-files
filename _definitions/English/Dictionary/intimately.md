---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intimately
offline_file: ""
offline_thumbnail: ""
uuid: ddbec35b-47d9-46e9-8619-e6e013f4d5c0
updated: 1484310353
title: intimately
categories:
    - Dictionary
---
intimately
     adv 1: in a close manner; "the two phenomena are intimately
            connected"; "the person most nearly concerned" [syn: {closely},
             {nearly}]
     2: with great or especially intimate knowledge; "we knew them
        well" [syn: {well}]
