---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fruit_juices
offline_file: ""
offline_thumbnail: ""
uuid: 8c21b6eb-548b-4e06-86dc-1e9a56d80346
updated: 1484308371
title: Juice
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Orange_juice_1.jpg
tags:
    - Preparation
    - Processing
    - Terminology
    - Health effects
    - Comparison to whole fruits
    - Juice bars
    - Chains
    - History
categories:
    - Uncategorized
---
Juice is a liquid (drink) that is naturally contained in fruit and vegetables. It can also refer to liquids that are flavored with these or other biological food sources such as meat and seafood. It is commonly consumed as a beverage or used as an ingredient or flavoring in foods or other beverages. Juice emerged as a popular beverage choice after the development of pasteurization methods allowed for its preservation without fermentation.[1] The Food and Agriculture Organization of the United Nations (FAO) estimated the total world production of citrus fruit juices to be 12,840,318 tonnes in 2012.[2] The biggest fruit juice consumers are German : 39.6 liters consumed annually per person. ...
