---
version: 1
type: video
provider: YouTube
id: utC3l_pji18
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Enthalpy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e537200-bc40-4328-baf6-40fd1600c97d
updated: 1486069654
title: What is Enthalpy
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/utC3l_pji18/default.jpg
    - https://i3.ytimg.com/vi/utC3l_pji18/1.jpg
    - https://i3.ytimg.com/vi/utC3l_pji18/2.jpg
    - https://i3.ytimg.com/vi/utC3l_pji18/3.jpg
---
