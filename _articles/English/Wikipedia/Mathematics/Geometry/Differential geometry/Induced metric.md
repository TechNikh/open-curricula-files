---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Induced_metric
offline_file: ""
offline_thumbnail: ""
uuid: 8a70360c-3adf-4b53-b349-77ca1929da37
updated: 1484309223
title: Induced metric
categories:
    - Differential geometry
---
In mathematics and theoretical physics, the induced metric is the metric tensor defined on a submanifold which is calculated from the metric tensor on a larger manifold into which the submanifold is embedded. It may be calculated using the following formula (written using Einstein summation convention):
