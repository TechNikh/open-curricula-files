---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monoxide
offline_file: ""
offline_thumbnail: ""
uuid: de2ce90b-16cf-4f4a-9a5e-2d4b0b2acd32
updated: 1484310403
title: monoxide
categories:
    - Dictionary
---
monoxide
     n : an oxide containing just one atom of oxygen in the molecule
