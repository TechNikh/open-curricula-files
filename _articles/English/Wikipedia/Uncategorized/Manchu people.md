---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manchu
offline_file: ""
offline_thumbnail: ""
uuid: cf2b11b0-d3bc-4ce2-a35f-191f2c130fc8
updated: 1484308482
title: Manchu people
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25E6%25B8%2585_%25E4%25BD%259A%25E5%2590%258D_%25E3%2580%258A%25E6%25B8%2585%25E5%25A4%25AA%25E7%25A5%2596%25E5%25A4%25A9%25E5%2591%25BD%25E7%259A%2587%25E5%25B8%259D%25E6%259C%259D%25E6%259C%258D%25E5%2583%258F%25E3%2580%258B.jpg'
tags:
    - History
    - Origins and early history
    - Manchu rule over China
    - Modern times
    - Etymology of the ethnonym
    - population
    - Mainland China
    - Distribution
    - Manchu autonomous regions
    - Other areas
categories:
    - Uncategorized
---
The Manchu [note 1] (Manchu: ᠮᠠᠨᠵᡠ; Möllendorff: manju; Abkai: manju; simplified Chinese: 满族; traditional Chinese: 滿族; pinyin: Mǎnzú; Wade–Giles: Man3-tsu2) are an ethnic minority in China and the people from whom Manchuria derives its name.[10] They are sometimes called "red-tasseled Manchus", a reference to the ornamentation on traditional Manchu hats.[11][12]
