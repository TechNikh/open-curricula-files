---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hose
offline_file: ""
offline_thumbnail: ""
uuid: e4642142-444e-4325-a613-d0c1963478f5
updated: 1484310246
title: hose
categories:
    - Dictionary
---
hose
     n 1: socks and stockings and tights collectively (the British
          include underwear as hosiery) [syn: {hosiery}]
     2: man's garment of the 16th and 17th centuries; worn with a
        doublet [syn: {tights}]
     3: a flexible pipe for conveying a liquid or gas [syn: {hosepipe}]
     v : water with a hose; "hose the lawn" [syn: {hose down}]
