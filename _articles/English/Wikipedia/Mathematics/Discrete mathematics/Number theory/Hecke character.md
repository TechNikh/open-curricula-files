---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hecke_character
offline_file: ""
offline_thumbnail: ""
uuid: 8ca4569c-bb7c-4764-a5bf-ea1b8bc2aa28
updated: 1484308645
title: Hecke character
tags:
    - Definition using ideles
    - Definition using ideals
    - Relationship between the definitions
    - Special cases
    - Examples
    - "Tate's thesis"
    - Algebraic Hecke characters
    - Notes
categories:
    - Number theory
---
In number theory, a Hecke character is a generalisation of a Dirichlet character, introduced by Erich Hecke to construct a class of L-functions larger than Dirichlet L-functions, and a natural setting for the Dedekind zeta-functions and certain others which have functional equations analogous to that of the Riemann zeta-function.
