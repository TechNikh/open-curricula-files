---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/migration
offline_file: ""
offline_thumbnail: ""
uuid: 0ce6e189-1b8d-4b2a-8499-d539a625d6fb
updated: 1484310462
title: migration
categories:
    - Dictionary
---
migration
     n 1: the movement of persons from one country or locality to
          another
     2: a group of people migrating together (especially in some
        given time period)
     3: (chemistry) the nonrandom movement of an atom or radical
        from one place to another within a molecule
     4: the periodic passage of groups of animals (especially birds
        or fishes) from one region to another for feeding or
        breeding
