---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cow
offline_file: ""
offline_thumbnail: ""
uuid: a19fa0bc-b211-4a51-b775-6c2e9b4f4462
updated: 1484310328
title: cow
categories:
    - Dictionary
---
cow
     n 1: female of domestic cattle: "`moo-cow' is a child's term"
          [syn: {moo-cow}]
     2: mature female of mammals of which the male is called `bull'
     3: a large unpleasant woman
     v : subdue, restrain, or overcome by affecting with a feeling of
         awe; frighten (as with threats) [syn: {overawe}]
