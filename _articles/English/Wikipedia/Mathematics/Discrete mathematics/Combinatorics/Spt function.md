---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spt_function
offline_file: ""
offline_thumbnail: ""
uuid: 2299ff73-698a-48d5-bd93-6a82b3bcd3f9
updated: 1484308592
title: Spt function
categories:
    - Combinatorics
---
The spt function (smallest parts function) is a function in number theory that counts the sum of the number of smallest parts in each partition of a positive integer. It is related to the partition function.
