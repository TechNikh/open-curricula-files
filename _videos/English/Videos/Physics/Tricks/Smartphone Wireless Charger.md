---
version: 1
type: video
provider: YouTube
id: Gk2-lF5Vq9U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ce95c52c-41b8-472b-a154-9bb6759e93ac
updated: 1486069657
title: Smartphone Wireless Charger
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Gk2-lF5Vq9U/default.jpg
    - https://i3.ytimg.com/vi/Gk2-lF5Vq9U/1.jpg
    - https://i3.ytimg.com/vi/Gk2-lF5Vq9U/2.jpg
    - https://i3.ytimg.com/vi/Gk2-lF5Vq9U/3.jpg
---
