---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518201
title: quest
categories:
    - Dictionary
---
quest
     n 1: a search for an alternative that meets cognitive criteria;
          "the pursuit of love"; "life is more than the pursuance
          of fame"; "a quest for wealth" [syn: {pursuit}, {pursuance}]
     2: the act of searching for something; "a quest for diamonds"
        [syn: {seeking}]
     v 1: make a search (for); "Things that die with their eyes open
          and questing"; "The animal came questing through the
          forest"
     2: search the trail of (game); "The dog went off and quested"
     3: bark with prolonged noises, of dogs [syn: {bay}]
     4: seek alms, as for religious purposes
     5: express the need or desire for; ask for; "She requested an
        ...
