---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moulton_plane
offline_file: ""
offline_thumbnail: ""
uuid: 8a8a6591-b20d-4697-b527-85ea80b7bfe1
updated: 1484309257
title: Moulton plane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Moulton_plane2.svg.png
tags:
    - Formal definition
    - Application
    - Notes
categories:
    - Incidence geometry
---
In incidence geometry, the Moulton plane is an example of an affine plane in which Desargues' theorem does not hold. It is named after the American astronomer Forest Ray Moulton. The points of the Moulton plane are simply the points in the real plane R2 and the lines are the regular lines as well with the exception that for lines with a negative slope, the slope doubles when they pass the y-axis.
