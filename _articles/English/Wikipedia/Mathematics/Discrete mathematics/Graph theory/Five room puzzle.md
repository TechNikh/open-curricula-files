---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Five_room_puzzle
offline_file: ""
offline_thumbnail: ""
uuid: fdf21119-ec7d-4e8e-93e9-2ac434d10c01
updated: 1484308625
title: Five room puzzle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/WallsLines.gif
tags:
    - Solutions
    - Informal proof of impossibility
    - Notes
categories:
    - Graph theory
---
This classical,[1] popular puzzle involves a large rectangle divided into five "rooms". The object of the puzzle is to cross each "wall" of the diagram with a continuous line only once.[2]
