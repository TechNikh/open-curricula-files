---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sharp-SAT
offline_file: ""
offline_thumbnail: ""
uuid: 97096e02-9439-435e-beb0-e967fe79ab63
updated: 1484308585
title: Sharp-SAT
categories:
    - Combinatorics
---
In computational complexity theory, #SAT, or Sharp-SAT, a function problem related to the Boolean satisfiability problem, is the problem of counting the number of satisfying assignments of a given Boolean formula. It is well-known example for the class of counting problems, which are of special interest in computational complexity theory.
