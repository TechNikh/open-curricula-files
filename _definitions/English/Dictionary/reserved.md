---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reserved
offline_file: ""
offline_thumbnail: ""
uuid: 9567dbf2-33da-4c23-8deb-7bbe9d0cbf0e
updated: 1484310583
title: reserved
categories:
    - Dictionary
---
reserved
     adj 1: set aside for the use of a particular person or party [ant:
            {unreserved}]
     2: marked by self-restraint and reticence; "was habitually
        reserved in speech, withholding her opinion"-Victoria
        Sackville-West [ant: {unreserved}]
     3: cool and formal in manner [syn: {restrained}, {reticent}, {unemotional}]
