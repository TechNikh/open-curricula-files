---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nose
offline_file: ""
offline_thumbnail: ""
uuid: 986074ff-0651-4b2f-8cc9-16ba35003972
updated: 1484310351
title: nose
categories:
    - Dictionary
---
nose
     n 1: the organ of smell and entrance to the respiratory tract;
          the prominent part of the face of man or other mammals;
          "he has a cold in the nose" [syn: {olfactory organ}]
     2: a front that resembles a human nose (especially the front of
        an aircraft); "the nose of the rocket heated up on
        reentry"
     3: the front or forward projection of a tool or weapon; "he
        ducked under the nose of the gun"
     4: a small distance; "my horse lost the race by a nose"
     5: the sense of smell (especially in animals); "the hound has a
        good nose"
     6: a natural skill; "he has a nose for good deals"
     7: a projecting spout from which a ...
