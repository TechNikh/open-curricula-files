---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperrectangle
offline_file: ""
offline_thumbnail: ""
uuid: 0ed54617-86ae-4c78-951c-8c4c4618e0ae
updated: 1484309289
title: Hyperrectangle
tags:
    - Types
    - Dual polytope
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry, an n-orthotope[1] (also called a hyperrectangle or a box) is the generalization of a rectangle for higher dimensions, formally defined as the Cartesian product of intervals.
