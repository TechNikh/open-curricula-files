---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stupefied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447281
title: stupefied
categories:
    - Dictionary
---
stupefy
     v 1: make dull or stupid or muddle with drunkenness or
          infatuation [syn: {besot}]
     2: be a mystery or bewildering to; "This beats me!"; "Got me--I
        don't know the answer!"; "a vexing problem"; "This
        question really stuck me" [syn: {perplex}, {vex}, {stick},
         {get}, {puzzle}, {mystify}, {baffle}, {beat}, {pose}, {bewilder},
         {flummox}, {nonplus}, {gravel}, {amaze}, {dumbfound}]
     3: make senseless or dizzy by or as if by a blow; "stun fish"
        [syn: {stun}]
     [also: {stupefied}]
