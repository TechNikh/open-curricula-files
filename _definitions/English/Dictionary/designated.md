---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/designated
offline_file: ""
offline_thumbnail: ""
uuid: e6f9a7e6-05e2-47f7-a731-bfa37fbfe125
updated: 1484310393
title: designated
categories:
    - Dictionary
---
designated
     adj : selected or named for a duty; "designated hitter"
