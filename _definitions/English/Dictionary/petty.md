---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/petty
offline_file: ""
offline_thumbnail: ""
uuid: c6dcd0c5-3523-4369-8831-541bc7a997a7
updated: 1484310515
title: petty
categories:
    - Dictionary
---
petty
     adj 1: inferior in rank or status; "the junior faculty"; "a lowly
            corporal"; "petty officialdom"; "a subordinate
            functionary" [syn: {junior-grade}, {inferior}, {lower},
             {lower-ranking}, {lowly}, {petty(a)}, {secondary}, {subaltern},
             {subordinate}]
     2: (informal terms) small and of little importance; "a fiddling
        sum of money"; "a footling gesture"; "our worries are
        lilliputian compared with those of countries that are at
        war"; "a little (or small) matter"; "Mickey Mouse
        regulations"; "a dispute over niggling details"; "limited
        to petty enterprises"; "piffling efforts"; "giving a
        ...
