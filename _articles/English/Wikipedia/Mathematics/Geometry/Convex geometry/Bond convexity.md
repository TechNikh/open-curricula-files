---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bond_convexity
offline_file: ""
offline_thumbnail: ""
uuid: ee986be2-d9fe-45b7-ba4e-7e387c062875
updated: 1484309189
title: Bond convexity
tags:
    - Calculation of convexity
    - Why bond convexities may differ
    - Mathematical definition
    - How bond duration changes with a changing interest rate
    - Application of convexity
categories:
    - Convex geometry
---
In finance, bond convexity is a measure of the non-linear relationship of bond prices to changes in interest rates, the second derivative of the price of the bond with respect to interest rates (duration is the first derivative). In general, the higher the duration, the more sensitive the bond price is to the change in interest rates. Bond convexity is one of the most basic and widely used forms of convexity in finance.
