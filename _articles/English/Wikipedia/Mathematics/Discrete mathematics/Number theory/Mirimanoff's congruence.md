---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mirimanoff%27s_congruence'
offline_file: ""
offline_thumbnail: ""
uuid: cee58855-920f-4659-9c08-27c0439f99ca
updated: 1484308649
title: "Mirimanoff's congruence"
categories:
    - Number theory
---
In number theory, a branch of mathematics, a Mirimanoff's congruence is one of a collection of expressions in modular arithmetic which, if they hold, entail the truth of Fermat's Last Theorem. Since the theorem has now been proven, these are now of mainly historical significance, though the Mirimanoff polynomials are interesting in their own right. The theorem is due to Dmitry Mirimanoff.
