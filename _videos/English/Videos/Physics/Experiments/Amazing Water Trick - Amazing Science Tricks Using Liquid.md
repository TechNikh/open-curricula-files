---
version: 1
type: video
provider: YouTube
id: G303o8pJzls
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 407797f5-6d4e-41b8-bb74-a37dcd1747fd
updated: 1486069639
title: 'Amazing Water Trick - Amazing Science Tricks Using Liquid'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/G303o8pJzls/default.jpg
    - https://i3.ytimg.com/vi/G303o8pJzls/1.jpg
    - https://i3.ytimg.com/vi/G303o8pJzls/2.jpg
    - https://i3.ytimg.com/vi/G303o8pJzls/3.jpg
---
