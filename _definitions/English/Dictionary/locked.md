---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/locked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448481
title: locked
categories:
    - Dictionary
---
locked
     adj : firmly fastened or secured against opening; "windows and
           doors were all fast"; "a locked closet"; "left the
           house properly secured" [syn: {barred}, {bolted}, {fast},
            {latched}, {secured}]
