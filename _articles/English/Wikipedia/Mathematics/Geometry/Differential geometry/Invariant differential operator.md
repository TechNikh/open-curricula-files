---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Invariant_differential_operator
offline_file: ""
offline_thumbnail: ""
uuid: 8a6d0e4e-c232-43fe-92a6-b92403fff25d
updated: 1484309226
title: Invariant differential operator
tags:
    - Invariance on homogeneous spaces
    - Invariance in terms of abstract indices
    - Examples
    - Conformal invariance
    - Notes
categories:
    - Differential geometry
---
In mathematics and theoretical physics, an invariant differential operator is a kind of mathematical map from some objects to an object of similar type. These objects are typically functions on 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
, functions on a manifold, vector valued functions, vector fields, or, more generally, sections of a vector bundle.
