---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waken
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450461
title: waken
categories:
    - Dictionary
---
waken
     v 1: cause to become awake or conscious; "He was roused by the
          drunken men in the street"; "Please wake me at 6 AM."
          [syn: {awaken}, {wake}, {rouse}, {wake up}, {arouse}]
          [ant: {cause to sleep}]
     2: stop sleeping; "She woke up to the sound of the alarm clock"
        [syn: {wake up}, {awake}, {arouse}, {awaken}, {wake}, {come
        alive}] [ant: {fall asleep}]
