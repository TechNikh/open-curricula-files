---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lov%C3%A1sz_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: fdefc661-276a-4fe3-9394-978053bee58c
updated: 1484308627
title: Lovász conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Steinhaus-Johnson-Trotter-Permutohedron.svg.png
tags:
    - Historical remarks
    - Variants of the Lovász conjecture
    - Hamiltonian cycle
    - Cayley graphs
    - Directed Cayley graph
    - Special cases
    - General groups
categories:
    - Graph theory
---
Originally Lovász stated the problem in the opposite way, but this version became standard. In 1996 Babai published a conjecture sharply contradicting this conjecture,[1] but both conjectures remain widely open. It is not even known if a single counterexample would necessarily lead to a series of counterexamples.
