---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telecom
offline_file: ""
offline_thumbnail: ""
uuid: d719aac4-e9bc-42d8-90a6-29cfb4c7e131
updated: 1484310186
title: telecom
categories:
    - Dictionary
---
telecom
     n : (often plural) systems used in transmitting messages over a
         distance electronically [syn: {telecommunication}]
