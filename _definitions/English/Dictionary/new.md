---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/new
offline_file: ""
offline_thumbnail: ""
uuid: e2d9dcee-0c38-47f1-990a-7cf75808cb0d
updated: 1484310313
title: new
categories:
    - Dictionary
---
new
     adj 1: not of long duration; having just (or relatively recently)
            come into being or been made or acquired or
            discovered; "a new law"; "new cars"; "a new comet"; "a
            new friend"; "a new year"; "the New World" [ant: {old}]
     2: other than the former one(s); different; "they now have a
        new leaders"; "my new car is four years old but has only
        15,000 miles on it"; "ready to take a new direction" [syn:
         {new(a)}]
     3: having no previous example or precedent or parallel; "a time
        of unexampled prosperity" [syn: {unexampled}]
     4: of a kind not seen before; "the computer produced a
        completely novel proof of ...
