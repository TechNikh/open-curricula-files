---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/character
offline_file: ""
offline_thumbnail: ""
uuid: 4426d2e6-7c0f-41eb-a2cf-436776313c3b
updated: 1484310309
title: character
categories:
    - Dictionary
---
character
     n 1: an imaginary person represented in a work of fiction (play
          or film or story); "she is the main character in the
          novel" [syn: {fictional character}, {fictitious
          character}]
     2: a characteristic property that defines the apparent
        individual nature of something; "each town has a quality
        all its own"; "the radical character of our demands" [syn:
         {quality}, {lineament}]
     3: the inherent complex of attributes that determine a persons
        moral and ethical actions and reactions; "education has
        for its object the formation of character"- Herbert
        Spencer [syn: {fiber}, {fibre}]
     4: an actor's ...
