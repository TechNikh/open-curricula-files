---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prewar
offline_file: ""
offline_thumbnail: ""
uuid: 72d63a7c-96ef-4c6b-bdbc-c83a43d8263f
updated: 1484310176
title: prewar
categories:
    - Dictionary
---
prewar
     adj : existing or belonging to a time before a war; "prewar levels
           of industrial production" [ant: {postwar}]
