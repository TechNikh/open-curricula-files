---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lamarckism
offline_file: ""
offline_thumbnail: ""
uuid: 4b4aecc1-c989-4b7d-b834-e75b30a080d8
updated: 1484308345
title: Lamarckism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jean-baptiste_lamarck2.jpg
tags:
    - History
    - "Lamarck's theory"
    - "Weismann's experiment"
    - Neo-Lamarckism
    - Experiments
    - Ideological neo-Lamarckism
    - Critique
    - Epigenetic Lamarckism
    - Contrary views
    - Bibliography
categories:
    - Uncategorized
---
Lamarckism (or Lamarckian inheritance) is the idea that an organism can pass on characteristics that it has acquired during its lifetime to its offspring (also known as heritability of acquired characteristics or soft inheritance). It is named after the French biologist Jean-Baptiste Lamarck (1744–1829), who incorporated the action of soft inheritance into his evolutionary theories as a supplement to his concept of an inherent progressive tendency driving organisms continuously towards greater complexity, in parallel but separate lineages with no extinction. Lamarck did not originate the idea of soft inheritance, which proposes that individual efforts during the lifetime of the organisms ...
