---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grow
offline_file: ""
offline_thumbnail: ""
uuid: 93366266-2ff4-403b-aa98-153d8f72f19f
updated: 1484310295
title: grow
categories:
    - Dictionary
---
grow
     v 1: pass into a condition gradually, take on a specific property
          or attribute; become; "The weather turned nasty"; "She
          grew angry" [syn: {turn}]
     2: become larger, greater, or bigger; expand or gain; "The
        problem grew too large for me"; "Her business grew fast"
     3: increase in size by natural process; "Corn doesn't grow
        here"; "In these forests, mushrooms grow under the trees"
     4: cause to grow or develop; "He grows vegetables in his
        backyard"
     5: develop and reach maturity; undergo maturation; "He matured
        fast"; "The child grew fast" [syn: {mature}, {maturate}]
     6: come into existence; take on form or ...
