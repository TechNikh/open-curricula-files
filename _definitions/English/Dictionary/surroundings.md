---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surroundings
offline_file: ""
offline_thumbnail: ""
uuid: a3691307-5af7-4c61-a25c-e9840363981a
updated: 1484310286
title: surroundings
categories:
    - Dictionary
---
surroundings
     n 1: the environmental condition [syn: {milieu}]
     2: the area in which something exists or lives; "the
        country--the flat agricultural surround" [syn: {environment},
         {environs}, {surround}]
