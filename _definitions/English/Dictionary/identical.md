---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identical
offline_file: ""
offline_thumbnail: ""
uuid: ebcdc8ef-c681-4749-9583-42d5f0994833
updated: 1484310286
title: identical
categories:
    - Dictionary
---
identical
     adj 1: exactly alike; incapable of being perceived as different;
            "rows of identical houses"; "cars identical except for
            their license plates"; "they wore indistinguishable
            hats" [syn: {indistinguishable}]
     2: being the exact same one; not any other:; "this is the
        identical room we stayed in before"; "the themes of his
        stories are one and the same"; "saw the selfsame quotation
        in two newspapers"; "on this very spot"; "the very thing
        he said yesterday"; "the very man I want to see" [syn: {one
        and the same(p)}, {selfsame(a)}, {very(a)}]
     3: (of twins) derived from a single egg or ovum; ...
