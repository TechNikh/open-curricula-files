---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cross-polytope
offline_file: ""
offline_thumbnail: ""
uuid: d187961f-17f7-48fb-b464-26a2617e5f9c
updated: 1484309285
title: Cross-polytope
tags:
    - 4 dimensions
    - Higher dimensions
    - Generalized orthoplex
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry, a cross-polytope,[1] orthoplex,[2] hyperoctahedron, or cocube is a regular, convex polytope that exists in n-dimensions. A 2-orthoplex is a square, a 3-orthoplex is a regular octahedron, and a 4-orthoplex is a 16-cell. Its facets are simplexes of the previous dimension, while the cross-polytope's vertex figure is another cross-polytope from the previous dimension.
