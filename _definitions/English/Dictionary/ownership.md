---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ownership
offline_file: ""
offline_thumbnail: ""
uuid: 0dcc8811-789d-4cdc-95fe-a16ccdc4589b
updated: 1484310467
title: ownership
categories:
    - Dictionary
---
ownership
     n 1: the relation of an owner to the thing possessed; possession
          with the right to transfer possession to others
     2: the act of having and controlling property [syn: {possession}]
     3: the state or fact of being an owner
