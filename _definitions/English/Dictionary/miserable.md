---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miserable
offline_file: ""
offline_thumbnail: ""
uuid: 33a99baf-c78b-4b7c-ba31-27b3d3d88943
updated: 1484310168
title: miserable
categories:
    - Dictionary
---
miserable
     adj 1: very unhappy; full of misery; "he felt depressed and
            miserable"; "a message of hope for suffering
            humanity"; "wretched prisoners huddled in stinking
            cages" [syn: {suffering}, {wretched}]
     2: deserving or inciting pity; "a hapless victim"; "miserable
        victims of war"; "the shabby room struck her as
        extraordinarily pathetic"- Galsworthy; "piteous appeals
        for help"; "pitiable homeless children"; "a pitiful fate";
        "Oh, you poor thing"; "his poor distorted limbs"; "a
        wretched life" [syn: {hapless}, {misfortunate}, {pathetic},
         {piteous}, {pitiable}, {pitiful}, {poor}, {wretched}]
     3: ...
