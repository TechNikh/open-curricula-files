---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blackwell_channel
offline_file: ""
offline_thumbnail: ""
uuid: da841abd-80cd-487f-a333-a0c0fa0e59c8
updated: 1484308506
title: Blackwell channel
categories:
    - Coding theory
---
The Blackwell channel is a deterministic broadcast channel model used in coding theory and information theory. It was first proposed by mathematician David Blackwell.[1] In this model, a transmitter transmits one of three symbols to two receivers. For two of the symbols, both receivers receive exactly what was sent; the third symbol, however, is received differently at each of the receivers. This is one of the simplest examples of a non-trivial capacity result for a non-stochastic channel.
