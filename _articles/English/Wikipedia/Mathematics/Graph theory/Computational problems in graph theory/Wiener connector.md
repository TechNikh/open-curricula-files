---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wiener_connector
offline_file: ""
offline_thumbnail: ""
uuid: bb28de6c-483b-4572-8117-626f69fea2f1
updated: 1484309398
title: Wiener connector
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/page1-440px-SteinerExample_nicer.pdf.jpg
tags:
    - Problem definition
    - Relationship to Steiner tree
    - Computational complexity
    - Hardness
    - Exact algorithms
    - Approximation algorithms
    - Behavior
    - Applications
categories:
    - Computational problems in graph theory
---
In mathematics applied to the study of networks, the Wiener connector, named in honor of chemist Harry Wiener who first introduced the Wiener Index, is a means of maximizing efficiency in connecting specified "query vertices" in a network. Given a connected, undirected graph and a set of query vertices in a graph, the minimum Wiener connector is an induced subgraph that connects the query vertices and minimizes the sum of shortest path distances among all pairs of vertices in the subgraph. In combinatorial optimization, the minimum Wiener connector problem is the problem of finding the minimum Wiener connector. It can be thought of as a version of the classic Steiner tree problem (one of ...
