---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivariant_bundle
offline_file: ""
offline_thumbnail: ""
uuid: e223947a-4e5b-4ccf-835d-c2bbb0e3ef90
updated: 1484309360
title: Equivariant bundle
categories:
    - Geometry stubs
---
In differential geometry, given a compact Lie group G, an equivariant bundle is a fiber bundle such that the total space and the base spaces are both G-spaces and the projection map 
  
    
      
        π
      
    
    {\displaystyle \pi }
  
 between them is equivariant: 
  
    
      
        π
        ∘
        g
        =
        g
        ∘
        π
      
    
    {\displaystyle \pi \circ g=g\circ \pi }
  
 with some extra requirement depending on a typical fiber.
