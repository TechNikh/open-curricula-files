---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pontryagin_duality
offline_file: ""
offline_thumbnail: ""
uuid: 475bc338-fc00-4bd1-b0b2-525dbb854d32
updated: 1484309237
title: Pontryagin duality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-2-adic_integers_with_dual_colorings.svg.png
tags:
    - Introduction
    - Locally compact abelian groups
    - Examples
    - The dual group
    - Examples of dual groups
    - The Pontryagin duality theorem
    - Pontryagin duality and the Fourier transform
    - Haar measure
    - >
        Fourier transform and Fourier inversion formula for
        L1-functions
    - The group algebra
    - |
        Plancherel and
        
        
        
        
        L
        
        2
        
        
        
        
        {\displaystyle L^{2}}
        
        Fourier inversion theorems
    - Bohr compactification and almost-periodicity
    - Categorical considerations
    - Generalizations
    - Non-commutative theory
    - Others
categories:
    - Duality theories
---
In mathematics, specifically in harmonic analysis and the theory of topological groups, Pontryagin duality explains the general properties of the Fourier transform on locally compact groups, such as 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
  
, the circle, or finite cyclic groups. The Pontryagin duality theorem itself states that locally compact abelian groups identify naturally with their bidual.
