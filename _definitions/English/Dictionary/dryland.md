---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dryland
offline_file: ""
offline_thumbnail: ""
uuid: 9b571233-99bb-4e11-b4df-404d1063617f
updated: 1484310547
title: dryland
categories:
    - Dictionary
---
dry land
     n : the solid part of the earth's surface; "the plane turned
         away from the sea and moved back over land"; "the earth
         shook for several minutes"; "he dropped the logs on the
         ground" [syn: {land}, {earth}, {ground}, {solid ground},
         {terra firma}]
