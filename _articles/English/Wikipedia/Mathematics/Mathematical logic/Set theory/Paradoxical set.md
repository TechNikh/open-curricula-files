---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paradoxical_set
offline_file: ""
offline_thumbnail: ""
uuid: 67e82903-890b-4ab4-8558-bf31cf5e6556
updated: 1484309509
title: Paradoxical set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Banach-Tarski_Paradox.svg_0.png
categories:
    - Set theory
---
In set theory, a paradoxical set is a set that has a paradoxical decomposition. A paradoxical decomposition of a set is a partitioning of the set into two subsets, along with an appropriate group of functions that operate on some universe (of which the set in question is a subset), such that each partition can be mapped back onto the entire set using only finitely many distinct functions (or compositions thereof) to accomplish the mapping. Since a paradoxical set as defined requires a suitable group 
  
    
      
        G
      
    
    {\displaystyle G}
  
, it is said to be 
  
    
      
        G
      
    
    {\displaystyle G}
  
-paradoxical, or paradoxical with respect to 
  
 ...
