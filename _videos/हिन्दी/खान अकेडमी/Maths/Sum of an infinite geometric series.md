---
version: 1
type: video
provider: YouTube
id: _V8K8ycsNNg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 51854377-6599-45a8-aad9-e09f2f5c1ca8
updated: 1486069611
title: Sum of an infinite geometric series
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/_V8K8ycsNNg/default.jpg
    - https://i3.ytimg.com/vi/_V8K8ycsNNg/1.jpg
    - https://i3.ytimg.com/vi/_V8K8ycsNNg/2.jpg
    - https://i3.ytimg.com/vi/_V8K8ycsNNg/3.jpg
---
