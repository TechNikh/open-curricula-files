---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confiscated
offline_file: ""
offline_thumbnail: ""
uuid: 905af26a-0a3c-47b2-8716-80e433054da7
updated: 1484310577
title: confiscated
categories:
    - Dictionary
---
confiscated
     adj : taken without permission or consent especially by public
           authority; "the condemned land was used for a highway
           cloverleaf"; "the confiscated liquor was poured down
           the drain" [syn: {appropriated}, {condemned}, {confiscate},
            {seized}, {taken over}]
