---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-resident
offline_file: ""
offline_thumbnail: ""
uuid: 44db740f-14d8-4351-af62-de40bfdc29fd
updated: 1484310517
title: non-resident
categories:
    - Dictionary
---
nonresident
     adj : not living in a particular place or owned by permanent
           residents; "nonresident students who commute to
           classes"; "nonresident real estate" [ant: {resident}]
