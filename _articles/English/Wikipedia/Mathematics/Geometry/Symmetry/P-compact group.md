---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-compact_group
offline_file: ""
offline_thumbnail: ""
uuid: 034cef2a-c60f-4ea7-8656-c537c2185160
updated: 1484309315
title: P-compact group
tags:
    - Examples
    - Classification
    - Notes
categories:
    - Symmetry
---
In mathematics, in particular algebraic topology, a p-compact group is (roughly speaking) a space that is a homotopical version of a compact Lie group, but with all the structure concentrated at a single prime p. This concept was introduced by Dwyer and Wilkerson.[1] Subsequently the name homotopy Lie group has also been used.
