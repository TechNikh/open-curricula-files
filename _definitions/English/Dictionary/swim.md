---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swim
offline_file: ""
offline_thumbnail: ""
uuid: 3aebf72d-b18c-4701-a0fa-9cb2abb53667
updated: 1484310214
title: swim
categories:
    - Dictionary
---
swim
     n : the act of swimming [syn: {swimming}]
     v 1: travel through water; "We had to swim for 20 minutes to
          reach the shore"; "a big fish was swimming in the tank"
     2: be afloat; stay on a liquid surface; not sink [syn: {float}]
        [ant: {sink}]
     [also: {swum}, {swimming}, {swam}]
