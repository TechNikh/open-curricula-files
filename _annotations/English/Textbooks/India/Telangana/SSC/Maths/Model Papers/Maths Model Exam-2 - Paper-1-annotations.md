---
version: 1
type: annotation
id: >
    http://eschool2go.org/sites/default/files/MathsPaper-II_TenthEM.pdf
uuid: 464a2b15-3c10-4d60-a9bd-79d783a3bfdc
updated: "1483806601"
annotations:
    - 
        quote: zeroes
        id: https://en.wikipedia.org/wiki/Zeroes
        uuid: 9eb16abf-eed9-43d6-a8dd-cbd3fd79d577
        text: "0"
        comment: '0 (zero; BrE: /ˈzɪərəʊ/ or AmE: /ˈziːroʊ/) is both a number[1] and the numerical digit used to represent that number in numerals. The number 0 fulfills a central role in mathematics as the additive identity of the integers, real numbers, and many other algebraic structures. As a digit, 0 is used as a placeholder in place value systems. Names for the number 0 in English include zero, nought or (US) naught (/ˈnɔːt/), nil, or—in contexts where at least one adjacent digit distinguishes it from the letter "O"—oh or o (/ˈoʊ/). Informal or slang terms for zero include zilch and zip.[2] Ought and aught (/ˈɔːt/),[3] as well as cipher,[4] have also been used historically.[5]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Estela_C_de_Tres_Zapotes.jpg
            uuid: c81eb355-e4d2-48e9-8b6c-3d57e2a2d0e9
            comment: >
                The back of Olmec stela C from Tres Zapotes, the second
                oldest Long Count date discovered. The numerals 7.16.6.16.18
                translate to September, 32 BC (Julian). The glyphs
                surrounding the date are thought to be one of the few
                surviving examples of ...
---
