---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fancy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484459582
title: fancy
categories:
    - Dictionary
---
fancy
     adj : not plain; decorative or ornamented; "fancy handwriting";
           "fancy clothes" [ant: {plain}]
     n 1: something many people believe that is false; "they have the
          illusion that I am very wealthy" [syn: {illusion}, {fantasy},
           {phantasy}]
     2: fancy was held by Coleridge to be more casual and
        superficial than imagination
     3: a predisposition to like something; "he had a fondness for
        whiskey" [syn: {fondness}, {partiality}]
     v 1: imagine; conceive of; see in one's mind; "I can't see him on
          horseback!"; "I can see what will happen"; "I can see a
          risk in this strategy" [syn: {visualize}, {visualise}, ...
