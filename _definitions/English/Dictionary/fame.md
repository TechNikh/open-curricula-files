---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fame
offline_file: ""
offline_thumbnail: ""
uuid: 550243b5-db95-42db-b05a-5b9384ed3755
updated: 1484310393
title: fame
categories:
    - Dictionary
---
fame
     n 1: the state or quality of being widely honored and acclaimed
          [syn: {celebrity}, {renown}] [ant: {infamy}]
     2: favorable public reputation [ant: {infamy}]
