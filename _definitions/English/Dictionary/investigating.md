---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/investigating
offline_file: ""
offline_thumbnail: ""
uuid: 8a20b6f5-2b05-46f1-b397-9325206d6379
updated: 1484310325
title: investigating
categories:
    - Dictionary
---
investigating
     n : the work of inquiring into something thoroughly and
         systematically [syn: {investigation}]
