---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decision-making
offline_file: ""
offline_thumbnail: ""
uuid: 73ff13f2-c5b9-40f4-b0a3-1c9873fd8620
updated: 1484310325
title: decision-making
categories:
    - Dictionary
---
decision making
     n : the cognitive process of reaching a decision; "a good
         executive must be good at decision making" [syn: {deciding}]
