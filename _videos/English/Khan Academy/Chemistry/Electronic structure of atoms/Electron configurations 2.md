---
version: 1
type: video
provider: YouTube
id: YURReI6OJsg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Electronic%20structure%20of%20atoms/Electron%20configurations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 52f5e873-7fe7-4eb5-ad3f-a27d50ebf1d7
updated: 1486069587
title: Electron configurations 2
categories:
    - Electronic structure of atoms
thumbnail_urls:
    - https://i3.ytimg.com/vi/YURReI6OJsg/default.jpg
    - https://i3.ytimg.com/vi/YURReI6OJsg/1.jpg
    - https://i3.ytimg.com/vi/YURReI6OJsg/2.jpg
    - https://i3.ytimg.com/vi/YURReI6OJsg/3.jpg
---
Figuring out configurations for the d-block elements. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/electron-configurations-for-the-first-period?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/noble-gas-configuration-electronic-structure-of-atoms-chemistry-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
