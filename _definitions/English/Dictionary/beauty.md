---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beauty
offline_file: ""
offline_thumbnail: ""
uuid: a89a2406-f1f6-4675-a466-7610f1019274
updated: 1484310212
title: beauty
categories:
    - Dictionary
---
beauty
     n 1: the qualities that give pleasure to the senses [ant: {ugliness}]
     2: a very attractive or seductive looking woman [syn: {smasher},
         {stunner}, {knockout}, {ravisher}, {sweetheart}, {peach},
         {lulu}, {looker}, {mantrap}, {dish}]
     3: an outstanding example of its kind; "his roses were
        beauties"; "when I make a mistake it's a beaut" [syn: {beaut}]
