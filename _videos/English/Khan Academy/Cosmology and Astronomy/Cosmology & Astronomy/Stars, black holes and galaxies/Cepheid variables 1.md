---
version: 1
type: video
provider: YouTube
id: BWs-ONRDDG4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Cepheid%20variables%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 78ac264c-f3b7-41fe-abdf-4b213914d54d
updated: 1486069686
title: Cepheid variables 1
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/BWs-ONRDDG4/default.jpg
    - https://i3.ytimg.com/vi/BWs-ONRDDG4/1.jpg
    - https://i3.ytimg.com/vi/BWs-ONRDDG4/2.jpg
    - https://i3.ytimg.com/vi/BWs-ONRDDG4/3.jpg
---
