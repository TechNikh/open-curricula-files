---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scattering
offline_file: ""
offline_thumbnail: ""
uuid: f4f3058a-8a3e-416a-af60-c05e815f78c9
updated: 1484310431
title: scattering
categories:
    - Dictionary
---
scattering
     adj : spreading by diffusion [syn: {diffusing(a)}, {diffusive}, {dispersive},
            {disseminative}, {disseminating}, {spreading}]
     n 1: the physical process in which particles are deflected
          haphazardly as a result of collisions
     2: a small number dispersed haphazardly; "the first scatterings
        of green" [syn: {sprinkling}]
     3: a light shower that falls in some locations and not others
        nearby [syn: {sprinkle}, {sprinkling}]
     4: spreading widely or driving off [syn: {dispersion}]
     5: the act of scattering [syn: {scatter}, {strewing}]
