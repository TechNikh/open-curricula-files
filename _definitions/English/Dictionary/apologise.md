---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apologise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628901
title: apologise
categories:
    - Dictionary
---
apologise
     v 1: defend, explain, clear away, or make excuses for by
          reasoning; "rationalize the child's seemingly crazy
          behavior"; "he rationalized his lack of success" [syn: {apologize},
           {excuse}, {justify}, {rationalize}, {rationalise}]
     2: acknowledge faults or shortcomings or failing; "I apologized
        for being late"; "He apologized for the many typoes" [syn:
         {apologize}]
