---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heat
offline_file: ""
offline_thumbnail: ""
uuid: ab737ad3-95e8-4224-a727-f2d8e14198a5
updated: 1484308842
title: Heat
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-171879main_LimbFlareJan12_lg.jpg
tags:
    - History
    - Transfers of energy as heat between two bodies
    - >
        Practical operating devices that harness transfers of energy
        as heat
    - Heat engine
    - Heat pump or refrigerator
    - Macroscopic view of quantity of energy transferred as heat
    - Microscopic view of heat
    - Notation and units
    - Estimation of quantity of heat
    - Internal energy and enthalpy
    - Heat added to a body at constant pressure
    - Entropy
    - Latent and sensible heat
    - Specific heat
    - Relation between heat and temperature
    - Relation between hotness and temperature
    - >
        Rigorous definition of quantity of energy transferred as
        heat
    - >
        Heat, temperature, and thermal equilibrium regarded as
        jointly primitive notions
    - Heat transfer in engineering
    - Quotations
    - Bibliography of cited references
    - Further bibliography
categories:
    - Thermodynamics
---
In physics, heat is energy that spontaneously passes between a system and its surroundings in some way other than through work or the transfer of matter. When a suitable physical pathway exists, heat flows spontaneously from a hotter to a colder body.[1][2][3][4][5][6] The transfer can be by contact between the source and the destination body, as in conduction; or by radiation between remote bodies; or by conduction and radiation through a thick solid wall; or by way of an intermediate fluid body, as in convective circulation; or by a combination of these.[7][8][9]
