---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tower
offline_file: ""
offline_thumbnail: ""
uuid: 7582326e-1a9d-4bf1-9446-41e337d30000
updated: 1484310156
title: tower
categories:
    - Dictionary
---
tower
     n 1: a structure taller than its diameter; can stand alone or be
          attached to a larger building
     2: anything tall and thin approximating the shape of a column
        or tower; "the test tube held a column of white powder";
        "a tower of dust rose above the horizon"; "a thin pillar
        of smoke betrayed their campsite" [syn: {column}, {pillar}]
     3: a powerful small boat designed to pull or push larger ships
        [syn: {tugboat}, {tug}, {towboat}]
     v : appear very large or occupy a commanding position; "The huge
         sculpture predominates over the fountain"; "Large shadows
         loomed on the canyon wall" [syn: {loom}, {predominate}, ...
