---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conquest
offline_file: ""
offline_thumbnail: ""
uuid: f7dd70ab-25d6-400a-b6f2-1e7236b4f29f
updated: 1484310561
title: conquest
categories:
    - Dictionary
---
conquest
     n 1: the act of conquering [syn: {conquering}, {subjection}, {subjugation}]
     2: success in mastering something difficult; "the conquest of
        space"
     3: an act of winning the love or sexual favor of someone [syn:
        {seduction}]
