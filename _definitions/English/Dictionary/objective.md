---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/objective
offline_file: ""
offline_thumbnail: ""
uuid: 012eab09-5684-4f52-8b3c-ed832cfa5eb7
updated: 1484310573
title: objective
categories:
    - Dictionary
---
objective
     adj 1: undistorted by emotion or personal bias; based on observable
            phenomena; "an objective appraisal"; "objective
            evidence" [syn: {nonsubjective}] [ant: {subjective}]
     2: serving as or indicating the object of a verb or of certain
        prepositions and used for certain other purposes;
        "objective case"; "accusative endings" [syn: {accusative}]
     3: emphasizing or expressing things as perceived without
        distortion of personal feelings or interpretation;
        "objective art"
     4: belonging to immediate experience of actual things or
        events; "concrete benefits"; "a concrete example"; "there
        is no objective ...
