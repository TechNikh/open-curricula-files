---
version: 1
type: video
provider: YouTube
id: L0UzEfW6Jj8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cc7a495c-4257-4bfa-b201-93d675bb1be7
updated: 1486069616
title: 30 60 90 triangle side ratios proof
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/L0UzEfW6Jj8/default.jpg
    - https://i3.ytimg.com/vi/L0UzEfW6Jj8/1.jpg
    - https://i3.ytimg.com/vi/L0UzEfW6Jj8/2.jpg
    - https://i3.ytimg.com/vi/L0UzEfW6Jj8/3.jpg
---
