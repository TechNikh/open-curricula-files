---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ludwig_Immanuel_Magnus
offline_file: ""
offline_thumbnail: ""
uuid: 5e809b7e-d17a-4d96-8443-1ab40ea4bbc0
updated: 1484309090
title: Ludwig Immanuel Magnus
categories:
    - Geometers
---
Ludwig Immanuel Magnus (March 15, 1790–September 25, 1861) was a German Jewish mathematician who, in 1831, published a paper about the inversion transformation, which leads to inversive geometry.
