---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/canine
offline_file: ""
offline_thumbnail: ""
uuid: 699b6a32-7b66-4988-ada2-1fb2e12d0fc4
updated: 1484310343
title: canine
categories:
    - Dictionary
---
canine
     adj 1: of or relating to a pointed conical tooth [syn: {laniary}]
     2: of or relating to or characteristic of members of the family
        Canidae
     n 1: one of the four pointed conical teeth (two in each jaw)
          located between the incisors and the premolars [syn: {canine
          tooth}, {eyetooth}, {eye tooth}, {dogtooth}, {cuspid}]
     2: any of various fissiped mammals with nonretractile claws and
        typically long muzzles [syn: {canid}]
