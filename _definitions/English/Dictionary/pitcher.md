---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitcher
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484563321
title: pitcher
categories:
    - Dictionary
---
pitcher
     n 1: (baseball) the person who does the pitching; "our pitcher
          has a sore arm" [syn: {hurler}, {twirler}]
     2: an open vessel with a handle and a spout for pouring [syn: {ewer}]
     3: the quantity contained in a pitcher [syn: {pitcherful}]
     4: the position on a baseball team of the player who throws the
        ball for a batter to try to hit; "he has played every
        position except pitcher"; "they have a southpaw on the
        mound" [syn: {mound}]
