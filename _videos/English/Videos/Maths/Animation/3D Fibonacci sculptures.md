---
version: 1
type: video
provider: YouTube
id: PDwal4PhZv4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8c131e8d-c65d-4501-b83d-db99bb9edf07
updated: 1486069641
title: 3D Fibonacci sculptures
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/PDwal4PhZv4/default.jpg
    - https://i3.ytimg.com/vi/PDwal4PhZv4/1.jpg
    - https://i3.ytimg.com/vi/PDwal4PhZv4/2.jpg
    - https://i3.ytimg.com/vi/PDwal4PhZv4/3.jpg
---
