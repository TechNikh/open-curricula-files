---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectrum
offline_file: ""
offline_thumbnail: ""
uuid: c2a6d40f-69e5-4148-abb0-da7eb1605768
updated: 1484310387
title: spectrum
categories:
    - Dictionary
---
spectrum
     n 1: an ordered array of the components of an emission or wave
     2: broad range of related values or qualities or ideas or
        activities
     [also: {spectra} (pl)]
