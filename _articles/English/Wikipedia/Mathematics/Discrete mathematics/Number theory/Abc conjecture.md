---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abc_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 0dced8f9-1dec-49a1-bc24-b9e1870ffef6
updated: 1484308542
title: Abc conjecture
tags:
    - Formulations
    - Examples of triples with small radical
    - Some consequences
    - Theoretical results
    - Computational results
    - Refined forms, generalizations and related statements
    - The work of Shinichi Mochizuki
    - Notes
categories:
    - Number theory
---
The abc conjecture (also known as the Oesterlé–Masser conjecture) is a conjecture in number theory, first proposed by Joseph Oesterlé (1988) and David Masser (1985). It is stated in terms of three positive integers, a, b and c (hence the name) that are relatively prime and satisfy a + b = c. If d denotes the product of the distinct prime factors of abc, the conjecture essentially states that d is usually not much smaller than c. In other words: if a and b are composed from large powers of primes, then c is usually not divisible by large powers of primes. The precise statement is given below.
