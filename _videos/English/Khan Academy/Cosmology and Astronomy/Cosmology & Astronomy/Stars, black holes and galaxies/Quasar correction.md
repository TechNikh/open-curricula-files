---
version: 1
type: video
provider: YouTube
id: PX_XSnVWlNc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Quasar%20correction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 90ce281e-a87d-4181-8f12-1d6ba154e108
updated: 1486069686
title: Quasar correction
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/PX_XSnVWlNc/default.jpg
    - https://i3.ytimg.com/vi/PX_XSnVWlNc/1.jpg
    - https://i3.ytimg.com/vi/PX_XSnVWlNc/2.jpg
    - https://i3.ytimg.com/vi/PX_XSnVWlNc/3.jpg
---
