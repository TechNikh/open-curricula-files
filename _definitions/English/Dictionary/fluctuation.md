---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fluctuation
offline_file: ""
offline_thumbnail: ""
uuid: 52563c5e-89e3-4e8c-a6b0-632d41be9c40
updated: 1484310315
title: fluctuation
categories:
    - Dictionary
---
fluctuation
     n 1: a wave motion; "the fluctuations of the sea"
     2: an instance of change; the rate or magnitude of change [syn:
         {variation}]
     3: the quality of being unsteady and subject to fluctuations;
        "he kept a record of price fluctuations" [syn: {wavering}]
