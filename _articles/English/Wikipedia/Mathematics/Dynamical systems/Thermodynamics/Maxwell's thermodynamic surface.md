---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Maxwell%27s_thermodynamic_surface'
offline_file: ""
offline_thumbnail: ""
uuid: 30e54654-e8ca-46d6-b758-8a115f6cb773
updated: 1484308849
title: "Maxwell's thermodynamic surface"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Maxwell_thermodynamic_surface.png
tags:
    - Construction of the model
    - Uses of the model
    - Related models
categories:
    - Thermodynamics
---
Maxwell’s thermodynamic surface is an 1874 sculpture[2] made by Scottish physicist James Clerk Maxwell (1831–1879). This model provides a three-dimensional plot of the various states of a fictitious substance with water-like properties.[3] This plot has coordinates volume (x), entropy (y), and energy (z). It was based on the American scientist Josiah Willard Gibbs’ graphical thermodynamics papers of 1873.[4][5] The model, in Maxwell's words, allowed "the principal features of known substances [to] be represented on a convenient scale."[6]
