---
version: 1
type: video
provider: YouTube
id: Xi7o8cMPI0E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Direct%20Current%20Electric%20Motor.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9fddce97-b4c0-4805-b8d8-94ef387d164c
updated: 1486069652
title: Direct Current Electric Motor
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xi7o8cMPI0E/default.jpg
    - https://i3.ytimg.com/vi/Xi7o8cMPI0E/1.jpg
    - https://i3.ytimg.com/vi/Xi7o8cMPI0E/2.jpg
    - https://i3.ytimg.com/vi/Xi7o8cMPI0E/3.jpg
---
