---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entity
offline_file: ""
offline_thumbnail: ""
uuid: ad8403ad-e481-41f7-b745-ec0c5e334202
updated: 1484310405
title: entity
categories:
    - Dictionary
---
entity
     n : that which is perceived or known or inferred to have its own
         distinct existence (living or nonliving)
