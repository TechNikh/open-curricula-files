---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/composed
offline_file: ""
offline_thumbnail: ""
uuid: 0637b372-7e46-4dc6-9c43-089aebc75b72
updated: 1484310414
title: composed
categories:
    - Dictionary
---
composed
     adj 1: made up of individual elements; "if perception is seen as
            composed of isolated sense data..."
     2: serenely self-possessed and free from agitation especially
        in times of stress; "the performer seemed completely
        composed as she stepped onto the stage"; "I felt calm and
        more composed than I had in a long time" [ant: {discomposed}]
