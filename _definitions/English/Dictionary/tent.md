---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tent
offline_file: ""
offline_thumbnail: ""
uuid: 9e490170-3146-4c1e-a3b7-dd1835862463
updated: 1484310154
title: tent
categories:
    - Dictionary
---
tent
     n : a portable shelter (usually of canvas stretched over
         supporting poles and fastened to the ground with ropes
         and pegs); "he pitched his tent near the creek" [syn: {collapsible
         shelter}]
     v : live in or as if in a tent; "Can we go camping again this
         summer?"; "The circus tented near the town"; "The
         houseguests had to camp in the living room" [syn: {camp},
          {encamp}, {camp out}, {bivouac}]
