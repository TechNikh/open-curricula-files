---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Maynard_Smith
offline_file: ""
offline_thumbnail: ""
uuid: 381a028a-de80-4bcd-9940-6e1db868f78c
updated: 1484308506
title: John Maynard Smith
tags:
    - Biography
    - Early years
    - Second degree
    - University of Sussex
    - Evolution and the Theory of Games
    - Evolution of sex and other major transitions in evolution
    - Animal Signals
    - Death
    - Awards and fellowships
    - Publications
    - Notes
    - University of Sussex
    - Media
    - Obituaries
categories:
    - Game theorists
---
John Maynard Smith[a] FRS (6 January 1920 – 19 April 2004) was a British theoretical evolutionary biologist and geneticist.[1] Originally an aeronautical engineer during the Second World War, he took a second degree in genetics under the well-known biologist J. B. S. Haldane. Maynard Smith was instrumental in the application of game theory to evolution and theorised on other problems such as the evolution of sex and signalling theory.
