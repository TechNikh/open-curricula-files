---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integration_along_fibers
offline_file: ""
offline_thumbnail: ""
uuid: ba48c756-1168-46aa-8a84-98a658fb79b1
updated: 1484309223
title: Integration along fibers
tags:
    - Definition
    - Example
    - Projection formula
    - Notes
categories:
    - Differential geometry
---
In differential geometry, the integration along fibers of a k-form yields a 
  
    
      
        (
        k
        −
        m
        )
      
    
    {\displaystyle (k-m)}
  
-form where m is the dimension of the fiber, via "integration".
