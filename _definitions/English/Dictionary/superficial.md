---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superficial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636581
title: superficial
categories:
    - Dictionary
---
superficial
     adj 1: being or affecting or concerned with a surface; not deep or
            penetrating emotionally or intellectually;
            "superficial similarities"; "a superficial mind"; "his
            thinking was superficial and fuzzy"; "superficial
            knowledge"; "the superficial report didn't give the
            true picture"; "only superficial differences" [ant: {profound}]
     2: relating to a surface; "superficial measurements"; "the
        superficial area of the wall"
     3: of little substance or significance; "a few superficial
        editorial changes"; "only trivial objections" [syn: {trivial}]
     4: involving a surface only; "her beauty is only ...
