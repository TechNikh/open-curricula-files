---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/singular
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464021
title: singular
categories:
    - Dictionary
---
singular
     adj 1: unusual or striking; "a remarkable sight"; "such poise is
            singular in one so young" [syn: {remarkable}]
     2: beyond or deviating from the usual or expected; "a curious
        hybrid accent"; "her speech has a funny twang"; "they have
        some funny ideas about war"; "had an odd name"; "the
        peculiar aromatic odor of cloves"; "something definitely
        queer about this town"; "what a rum fellow"; "singular
        behavior" [syn: {curious}, {funny}, {odd}, {peculiar}, {queer},
         {rum}, {rummy}]
     3: being a single and separate person or thing; "can the
        singular person be understood apart from his culture?";
        "every ...
