---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ionosphere
offline_file: ""
offline_thumbnail: ""
uuid: 914d5ffb-05a2-4604-b062-987e5e64da72
updated: 1484310162
title: ionosphere
categories:
    - Dictionary
---
ionosphere
     n : the outer region of the Earth's atmosphere; contains a high
         concentration of free electrons
