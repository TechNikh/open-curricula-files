---
version: 1
type: video
provider: YouTube
id: itsb2SqR-R0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/DNA%2C%20Hot%20Pockets%2C%20%26%20The%20Longest%20Word%20Ever-%20Crash%20Course%20Biology%20%2311.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aa3204f7-bafc-48d1-9d79-d507163b2733
updated: 1486069677
title: 'DNA, Hot Pockets, & The Longest Word Ever- Crash Course Biology #11'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/itsb2SqR-R0/default.jpg
    - https://i3.ytimg.com/vi/itsb2SqR-R0/1.jpg
    - https://i3.ytimg.com/vi/itsb2SqR-R0/2.jpg
    - https://i3.ytimg.com/vi/itsb2SqR-R0/3.jpg
---
