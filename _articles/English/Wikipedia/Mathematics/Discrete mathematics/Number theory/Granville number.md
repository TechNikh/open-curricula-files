---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Granville_number
offline_file: ""
offline_thumbnail: ""
uuid: 430c709a-74ec-44e9-bd57-49421c713af6
updated: 1484308641
title: Granville number
tags:
    - The Granville set
    - General properties
    - S-deficient numbers
    - S-perfect numbers
    - S-abundant numbers
    - Examples
categories:
    - Number theory
---
