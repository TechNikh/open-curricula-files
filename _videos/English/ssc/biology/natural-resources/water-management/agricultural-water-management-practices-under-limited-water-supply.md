---
title: Agricultural Water Management Practices under Limited Water Supply- Lessons from Recent Droughts
youtube_id: 6M--VUR80-8
tags:
    - Drought
    - Supply
    - Whiteboard animation
categories:
    - Water management
---
