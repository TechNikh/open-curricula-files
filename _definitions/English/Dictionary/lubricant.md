---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lubricant
offline_file: ""
offline_thumbnail: ""
uuid: f1d3f482-ac03-4f9e-982f-11e5a5497712
updated: 1484310414
title: lubricant
categories:
    - Dictionary
---
lubricant
     n : a substance capable of reducing friction by making surfaces
         smooth or slippery [syn: {lubricator}, {lubricating
         substance}]
