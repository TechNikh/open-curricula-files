---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skilful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461921
title: skilful
categories:
    - Dictionary
---
skilful
     adj : having or showing knowledge and skill and aptitude; "adept
           in handicrafts"; "an adept juggler"; "an expert job";
           "a good mechanic"; "a practiced marksman"; "a
           proficient engineer"; "a lesser-known but no less
           skillful composer"; "the effect was achieved by
           skillful retouching" [syn: {adept}, {expert}, {good}, {practiced},
            {proficient}, {skillful}]
