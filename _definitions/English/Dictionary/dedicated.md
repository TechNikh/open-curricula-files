---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dedicated
offline_file: ""
offline_thumbnail: ""
uuid: 87bded0b-73e8-42de-8810-cfdedaee81b8
updated: 1484310238
title: dedicated
categories:
    - Dictionary
---
dedicated
     adj 1: devoted to a cause or ideal or purpose; "a dedicated
            dancer"; "dedicated teachers"; "dedicated to the
            proposition that all men are created equal"- A.Lincoln
            [ant: {undedicated}]
     2: solemnly dedicated to or set apart for a high purpose; "a
        life consecrated to science"; "the consecrated chapel"; "a
        chapel dedicated to the dead of World War II" [syn: {consecrated},
         {consecrate}] [ant: {desecrated}]
