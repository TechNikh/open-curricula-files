---
version: 1
type: video
provider: YouTube
id: EPosseKSKk8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Direct%20and%20Indirect%20Objects.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 390b65ba-637c-4db0-8333-595e10825e97
updated: 1486069632
title: Direct and Indirect Objects
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/EPosseKSKk8/default.jpg
    - https://i3.ytimg.com/vi/EPosseKSKk8/1.jpg
    - https://i3.ytimg.com/vi/EPosseKSKk8/2.jpg
    - https://i3.ytimg.com/vi/EPosseKSKk8/3.jpg
---
