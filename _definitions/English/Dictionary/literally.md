---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/literally
offline_file: ""
offline_thumbnail: ""
uuid: 8836ac2c-469f-4af2-b491-ae37ed54fbe8
updated: 1484310549
title: literally
categories:
    - Dictionary
---
literally
     adv 1: in a literal sense; "literally translated"; "he said so
            literally" [ant: {figuratively}]
     2: (intensifier before a figurative expression) without
        exaggeration; "our eyes were literally pinned to TV during
        the Gulf war" [syn: {virtually}]
