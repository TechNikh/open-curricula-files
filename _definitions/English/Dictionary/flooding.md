---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flooding
offline_file: ""
offline_thumbnail: ""
uuid: 227d3322-ed6d-4984-99b1-195796057049
updated: 1484310170
title: flooding
categories:
    - Dictionary
---
flooding
     adj : overfull with water; "swollen rivers and creeks" [syn: {in
           flood(p)}, {overflowing}, {swollen}]
     n : a technique used in behavior therapy; client is flooded with
         experiences of a particular kind until becoming either
         averse to them or numbed to them [syn: {implosion therapy}]
