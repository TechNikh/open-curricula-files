---
version: 1
type: video
provider: YouTube
id: AN4KifV12DA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 52385450-11f4-4075-b203-00c14bb3d6f7
updated: 1486069680
title: 'Water and Solutions -- for Dirty Laundry- Crash Course Chemistry #7'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/AN4KifV12DA/default.jpg
    - https://i3.ytimg.com/vi/AN4KifV12DA/1.jpg
    - https://i3.ytimg.com/vi/AN4KifV12DA/2.jpg
    - https://i3.ytimg.com/vi/AN4KifV12DA/3.jpg
---
