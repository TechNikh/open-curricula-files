---
version: 1
type: video
provider: YouTube
id: QXT4OVM4vXI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Atomic%20Hook-Ups%20-%20Types%20of%20Chemical%20Bonds-%20Crash%20Course%20Chemistry%20%2322.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 21d90b1f-3000-4074-8a27-fbc7bdff92e1
updated: 1486069680
title: 'Atomic Hook-Ups - Types of Chemical Bonds- Crash Course Chemistry #22'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/QXT4OVM4vXI/default.jpg
    - https://i3.ytimg.com/vi/QXT4OVM4vXI/1.jpg
    - https://i3.ytimg.com/vi/QXT4OVM4vXI/2.jpg
    - https://i3.ytimg.com/vi/QXT4OVM4vXI/3.jpg
---
