---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ultraviolet_fixed_point
offline_file: ""
offline_thumbnail: ""
uuid: 4cc10cbc-9958-45d7-a248-b0938590c8b4
updated: 1484308790
title: Ultraviolet fixed point
tags:
    - Specific cases and details
    - Asymptotic safety scenario in quantum gravity
categories:
    - Fixed points (mathematics)
---
In a quantum field theory, one may calculate an effective or running coupling constant that defines the coupling of the theory measured at a given momentum scale. One example of such a coupling constant is the electric charge. In approximate calculations in several quantum field theories, notably quantum electrodynamics and theories of the Higgs particle, the running coupling appears to become infinite at a finite momentum scale. This is sometimes called the Landau pole problem. It is not known whether the appearance of these inconsistencies is an artifact of the approximation, or a real fundamental problem in the theory. However, the problem can be avoided if an ultraviolet or UV fixed ...
