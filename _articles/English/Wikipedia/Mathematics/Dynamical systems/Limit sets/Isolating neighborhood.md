---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isolating_neighborhood
offline_file: ""
offline_thumbnail: ""
uuid: e26fa5e9-ea1d-40fe-890d-896d1877d63c
updated: 1484308797
title: Isolating neighborhood
tags:
    - Definition
    - Conley index theory
    - "Milnor's definition of attractor"
categories:
    - Limit sets
---
In the theory of dynamical systems, an isolating neighborhood is a compact set in the phase space of an invertible dynamical system with the property that any orbit contained entirely in the set belongs to its interior. This is a basic notion in the Conley index theory. Its variant for non-invertible systems is used in formulating a precise mathematical definition of an attractor.
