---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/represent
offline_file: ""
offline_thumbnail: ""
uuid: ae66ac17-fc7b-4e6c-a012-201fc0bfe45e
updated: 1484310309
title: represent
categories:
    - Dictionary
---
represent
     v 1: take the place of or be parallel or equivalent to; "Because
          of the sound changes in the course of history, an 'h' in
          Greek stands for an 's' in Latin" [syn: {stand for}, {correspond}]
     2: express indirectly by an image, form, or model; be a symbol;
        "What does the Statue of Liberty symbolize?" [syn: {typify},
         {symbolize}, {symbolise}, {stand for}]
     3: be representative or typical for; "This period is
        represented by Beethoven"
     4: be a delegate or spokesperson for; represent somebody's
        interest or be a proxy or substitute for, as of
        politicians and office holders representing their
        ...
