---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shirer
offline_file: ""
offline_thumbnail: ""
uuid: 64e8018c-6ac9-4280-8664-d6b162a71ea1
updated: 1484308482
title: William L. Shirer
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bundesarchiv_Bild_183-L10819%252C_Waffenstillstand_von_Compi%25C3%25A8gne%252C_Berichterstatter.jpg'
tags:
    - Early years
    - Pre-war years
    - Reporting the war from Berlin
    - Post-war years
    - Shirer and Murrow
    - Books
    - Non-fiction
    - Fiction
categories:
    - Uncategorized
---
William Lawrence Shirer (February 23, 1904 – December 28, 1993) was an American journalist and war correspondent. He wrote The Rise and Fall of the Third Reich, a history of Nazi Germany that has been read by many and cited in scholarly works for more than 50 years. Originally a foreign correspondent for the Chicago Tribune and the International News Service, Shirer was the first reporter hired by Edward R. Murrow for what would become a CBS radio team of journalists known as "Murrow's Boys." He became known for his broadcasts from Berlin, from the rise of the Nazi dictatorship through the first year of World War II (1940). With Murrow, he organized the first broadcast world news roundup, ...
