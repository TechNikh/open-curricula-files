---
version: 1
type: video
provider: YouTube
id: b_SXwfHQ774
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Network%20Solids%20and%20Carbon-%20Crash%20Course%20Chemistry%20%2334.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 317cdd84-1139-430e-a689-2e0a562a4a87
updated: 1486069680
title: 'Network Solids and Carbon- Crash Course Chemistry #34'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/b_SXwfHQ774/default.jpg
    - https://i3.ytimg.com/vi/b_SXwfHQ774/1.jpg
    - https://i3.ytimg.com/vi/b_SXwfHQ774/2.jpg
    - https://i3.ytimg.com/vi/b_SXwfHQ774/3.jpg
---
