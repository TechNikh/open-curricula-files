---
version: 1
type: video
provider: YouTube
id: TDoGrbpJJ14
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Bacteria.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 65cd1e5b-6805-442e-81af-b6637008665d
updated: 1486069583
title: Bacteria
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/TDoGrbpJJ14/default.jpg
    - https://i3.ytimg.com/vi/TDoGrbpJJ14/1.jpg
    - https://i3.ytimg.com/vi/TDoGrbpJJ14/2.jpg
    - https://i3.ytimg.com/vi/TDoGrbpJJ14/3.jpg
---
Introduction to bacteria
More free lessons at: http://www.khanacademy.org/video?v=TDoGrbpJJ14
