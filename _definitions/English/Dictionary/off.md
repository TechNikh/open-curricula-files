---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/off
offline_file: ""
offline_thumbnail: ""
uuid: d96b5234-caf8-4b54-a208-ee289c704b2a
updated: 1484310295
title: 'off'
categories:
    - Dictionary
---
off
     adj 1: not in operation or operational; "the oven is off"; "the
            lights are off" [ant: {on}]
     2: below a satisfactory level; "an off year for tennis"; "his
        performance was off"
     3: (of events) no longer planned or scheduled; "the wedding is
        definitely off" [syn: {cancelled}] [ant: {on}]
     4: in an unpalatable state; "sour milk" [syn: {sour}, {turned}]
     5: not performing or scheduled for duties; "He's off every
        Tuesday"; "he was off duty when it happened"; "an off-duty
        policeman" [syn: {off(p)}, {off duty(p)}, {off-duty(a)}]
     adv 1: from a particular thing or place or position (`forth' is
            obsolete); "ran away ...
