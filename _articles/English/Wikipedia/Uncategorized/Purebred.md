---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pure_breed
offline_file: ""
offline_thumbnail: ""
uuid: 73bd23c5-6fc6-4f36-ad92-36a047f45345
updated: 1484308377
title: Purebred
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LaMirage_body07.jpg
tags:
    - True breeding
    - Pedigrees
    - Purebreds by animal
    - Purebred dogs
    - Purebred horses
    - Purebred cats
    - Purebred livestock
    - Wild species, landraces, and purebred species
categories:
    - Uncategorized
---
Purebreds, also called purebreeds, are cultivated varieties or cultivars of an animal species, achieved through the process of selective breeding. When the lineage of a purebred animal is recorded, that animal is said to be pedigreed.
