---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/difficult
offline_file: ""
offline_thumbnail: ""
uuid: 650651cd-d5cc-4255-8139-10c1533088f7
updated: 1484310210
title: difficult
categories:
    - Dictionary
---
difficult
     adj 1: not easy; requiring great physical or mental effort to
            accomplish or comprehend or endure; "a difficult
            task"; "nesting places on the cliffs are difficult of
            access"; "difficult times"; "a difficult child";
            "found himself in a difficult situation"; "why is it
            so hard for you to keep a secret?" [syn: {hard}] [ant:
             {easy}]
     2: requiring much effort and trouble; "the mountain climb was
        long, steep, and difficult"
