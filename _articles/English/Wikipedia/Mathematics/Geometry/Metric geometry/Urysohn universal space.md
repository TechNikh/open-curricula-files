---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Urysohn_universal_space
offline_file: ""
offline_thumbnail: ""
uuid: d02fbfb5-37ad-4cc5-9303-22efe7fc1de2
updated: 1484309273
title: Urysohn universal space
tags:
    - Definition
    - Properties
    - Existence and uniqueness
categories:
    - Metric geometry
---
The Urysohn universal space is a certain metric space that contains all separable metric spaces in a particularly nice manner. This mathematics concept is due to Pavel Samuilovich Urysohn.
