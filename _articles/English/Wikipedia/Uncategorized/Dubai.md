---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dubai
offline_file: ""
offline_thumbnail: ""
uuid: df894d98-171d-491d-a61c-4d34461bfb11
updated: 1484308450
title: Dubai
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AlRas_Deira_Mid1960s.jpg
tags:
    - Etymology
    - History
    - Pre-oil Dubai
    - Oil era
    - "Reaching the UAE's Act of Union"
    - Modern Dubai
    - geography
    - Climate
    - Governance and politics
    - Law enforcement
    - Sharia laws
    - Dress code
    - Human rights
    - Demographics
    - Ethnicity and languages
    - religion
    - Minorities
    - Economy
    - Tourism and retail
    - Expo 2020
    - Cityscape
    - Architecture
    - Burj Al Arab
    - Burj Khalifa
    - Dubai Miracle Garden
    - Transportation
    - Road
    - air
    - Metro rail
    - Palm Jumeirah Monorail
    - Tram
    - High speed rail
    - Waterways
    - culture
    - Food
    - Dubai Food Festival
    - Entertainment
    - Dubai Shopping Festival
    - Media
    - Sports
    - cricket
    - Education
    - Healthcare
    - Notable people
    - International relations
    - Twin towns and sister cities
    - Notes
categories:
    - Uncategorized
---
Dubai (/duːˈbaɪ/ doo-BY; Arabic: دبي‎‎ Dubayy, Gulf pronunciation: [dʊˈbɑj]) is the most populous city in the United Arab Emirates (UAE).[3] It is located on the southeast coast of the Persian Gulf and is the capital of the Emirate of Dubai, one of the seven emirates that make up the country. Abu Dhabi and Dubai are the only two emirates to have veto power over critical matters of national importance in the country's legislature.[4] The city of Dubai is located on the emirate's northern coastline and heads up the Dubai-Sharjah-Ajman metropolitan area. Dubai is to host World Expo 2020.[5]
