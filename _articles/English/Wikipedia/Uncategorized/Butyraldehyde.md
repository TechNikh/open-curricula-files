---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butanal
offline_file: ""
offline_thumbnail: ""
uuid: 8fd07afa-7f87-4745-b30b-5d8743b304a7
updated: 1484308401
title: Butyraldehyde
categories:
    - Uncategorized
---
Butyraldehyde, also known as butanal, is an organic compound with the formula CH3(CH2)2CHO. This compound is the aldehyde derivative of butane. It is a colourless flammable liquid with an acrid smell. It is miscible with most organic solvents.
