---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mustard
offline_file: ""
offline_thumbnail: ""
uuid: a29951a5-9ea9-4fb9-b06f-74ad3b741dce
updated: 1484310535
title: mustard
categories:
    - Dictionary
---
mustard
     n 1: any of several cruciferous plants of the genus Brassica
     2: pungent powder or paste prepared from ground mustard seeds
        [syn: {table mustard}]
     3: leaves eaten as cooked greens [syn: {mustard greens}, {leaf
        mustard}, {Indian mustard}]
