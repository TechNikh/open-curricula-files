---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_rate
offline_file: ""
offline_thumbnail: ""
uuid: 13b507d5-4b1a-476e-9efd-cff2155b4e92
updated: 1484308764
title: Entropy rate
categories:
    - Entropy
---
In the mathematical theory of probability, the entropy rate or source information rate of a stochastic process is, informally, the time density of the average information in a stochastic process. For stochastic processes with a countable index, the entropy rate H(X) is the limit of the joint entropy of n members of the process Xk divided by n, as n tends to infinity:
