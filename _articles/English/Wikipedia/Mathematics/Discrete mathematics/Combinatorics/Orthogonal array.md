---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orthogonal_array
offline_file: ""
offline_thumbnail: ""
uuid: 232680b2-a022-42e8-8960-281f68f7d3b7
updated: 1484308592
title: Orthogonal array
tags:
    - Definition
    - Examples
    - Trivial examples
    - Mutually orthogonal latin squares
    - Latin squares, latin cubes and latin hypercubes
    - Latin squares
    - Latin cubes
    - Latin hypercubes
    - History
    - Other constructions
    - Hadamard matrices
    - Codes
    - Applications
    - Threshold schemes
    - Factorial designs
    - Quality control
    - Testing
    - Notes
categories:
    - Combinatorics
---
In mathematics, in the area of combinatorial designs, an orthogonal array is a "table" (array) whose entries come from a fixed finite set of symbols (typically, {1,2,...,n}), arranged in such a way that there is an integer t so that for every selection of t columns of the table, all ordered t-tuples of the symbols, formed by taking the entries in each row restricted to these columns, appear the same number of times. The number t is called the strength of the orthogonal array. Here is a simple example of an orthogonal array with symbol set {1,2} and strength 2:
