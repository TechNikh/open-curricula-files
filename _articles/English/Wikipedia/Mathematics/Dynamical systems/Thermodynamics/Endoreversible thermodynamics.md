---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Endoreversible_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 849036ee-a049-402c-9e7b-3d2120c92361
updated: 1484308842
title: Endoreversible thermodynamics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/NovikovEngine.png
categories:
    - Thermodynamics
---
Endoreversible thermodynamics is a subset of irreversible thermodynamics aimed at making more realistic assumptions about heat transfer than are typically made in reversible thermodynamics. It gives an upper bound on the energy that can be derived from a real process that is lower than that predicted by Carnot for a Carnot cycle, and accommodates the exergy destruction occurring as heat is transferred irreversibly.
