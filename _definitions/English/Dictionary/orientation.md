---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orientation
offline_file: ""
offline_thumbnail: ""
uuid: 5bf3daae-1923-47d5-ac34-797be5d046d4
updated: 1484310196
title: orientation
categories:
    - Dictionary
---
orientation
     n 1: the act of orienting
     2: an integrated set of attitudes and beliefs
     3: position or alignment relative to points of the compass or
        other specific directions
     4: a predisposition in favor of something; "a predilection for
        expensive cars"; "his sexual preferences"; "showed a
        Marxist orientation" [syn: {predilection}, {preference}]
     5: a person's awareness of self with regard to position and
        time and place and personal relationships
     6: a course introducing a new situation or environment [syn: {orientation
        course}]
