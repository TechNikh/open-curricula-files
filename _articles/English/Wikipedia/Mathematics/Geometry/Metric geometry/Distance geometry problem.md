---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance_geometry_problem
offline_file: ""
offline_thumbnail: ""
uuid: 4a7d392e-5fbf-4b5e-baa8-b35234ecf79f
updated: 1484309265
title: Distance geometry problem
tags:
    - Applications
    - Basic issues
    - Cayley–Menger determinants
    - Discretization and orders
    - Software for distance geometry
    - Books and conferences
categories:
    - Metric geometry
---
The distance geometry problem is the characterization and study of sets of points based only on given values of the distances between member pairs.[1][2][3] Therefore distance geometry has immediate relevance where distance values are determined or considered, such as biology,[4] sensor network,[5] surveying, cartography and physics.
