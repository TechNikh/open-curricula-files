---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Choice_question
offline_file: ""
offline_thumbnail: ""
uuid: d0ce04b2-cf89-4686-9976-98fc0d55d2d5
updated: 1484308361
title: Yes–no question
tags:
    - How such questions are posed
    - Ambiguities
    - Answers
    - Suggestibility
categories:
    - Uncategorized
---
In linguistics, a yes–no question, formally known as a polar question, is a question whose expected answer is either "yes" or "no". Formally, they present an exclusive disjunction, a pair of alternatives of which only one is acceptable. In English, such questions can be formed in both positive and negative forms (e.g. "Will you be here tomorrow?" and "Won't you be here tomorrow?").[1]
