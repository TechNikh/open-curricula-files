---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_bound
offline_file: ""
offline_thumbnail: ""
uuid: 46e25261-153b-4623-99f1-5eb02f0a3efc
updated: 1484308563
title: Hamming bound
tags:
    - Background on error-correcting codes
    - Statement of the bound
    - Proof
    - Covering radius and packing radius
    - Perfect codes
    - Notes
categories:
    - Coding theory
---
In mathematics and computer science, in the field of coding theory, the Hamming bound is a limit on the parameters of an arbitrary block code: it is also known as the sphere-packing bound or the volume bound from an interpretation in terms of packing balls in the Hamming metric into the space of all possible words. It gives an important limitation on the efficiency with which any error-correcting code can utilize the space in which its code words are embedded. A code which attains the Hamming bound is said to be a perfect code.
