---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morally
offline_file: ""
offline_thumbnail: ""
uuid: 46c4bc42-b394-4d20-807d-9fe429d91cdd
updated: 1484310591
title: morally
categories:
    - Dictionary
---
morally
     adv 1: with respect to moral principles; "morally unjustified"
     2: in a moral manner; "he acted morally under the
        circumstances" [syn: {virtuously}] [ant: {immorally}]
