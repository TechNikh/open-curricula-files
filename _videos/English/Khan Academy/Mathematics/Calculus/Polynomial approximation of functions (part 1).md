---
version: 1
type: video
provider: YouTube
id: sy132cgqaiU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximation%20of%20functions%20%28part%201%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7d1ec3fe-1db8-4df4-b068-ce571e5b210e
updated: 1486069694
title: Polynomial approximation of functions (part 1)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sy132cgqaiU/default.jpg
    - https://i3.ytimg.com/vi/sy132cgqaiU/1.jpg
    - https://i3.ytimg.com/vi/sy132cgqaiU/2.jpg
    - https://i3.ytimg.com/vi/sy132cgqaiU/3.jpg
---
