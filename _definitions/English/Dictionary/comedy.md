---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comedy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484530741
title: comedy
categories:
    - Dictionary
---
comedy
     n 1: light and humorous drama with a happy ending [ant: {tragedy}]
     2: a comic incident or series of incidents [syn: {drollery}, {clowning},
         {funniness}]
