---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diagonal
offline_file: ""
offline_thumbnail: ""
uuid: 7dd5b07d-0647-49f1-afd3-73a0e5307544
updated: 1484310156
title: diagonal
categories:
    - Dictionary
---
diagonal
     adj 1: at an angle; especially connecting two nonadjacent corners
            of a plane figure or any two corners of a solid that
            are not in the same face; "a diagonal line across the
            page"
     2: having an oblique or slanted direction [syn: {aslant}, {aslope},
         {slanted}, {slanting}, {sloped}, {sloping}]
     n 1: (geometry) a straight line connecting any two vertices of a
          polygon that are not adjacent
     2: a line or cut across a fabric that is not at right angles to
        a side of the fabric [syn: {bias}]
     3: an oblique line of squares of the same color on a
        checkerboard; "the bishop moves on the diagonals"
     ...
