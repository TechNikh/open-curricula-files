---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ohmic
offline_file: ""
offline_thumbnail: ""
uuid: 77cd889f-d329-457c-a550-cdf72f0b5621
updated: 1484310202
title: ohmic
categories:
    - Dictionary
---
ohmic
     adj : of or relating to or measured in ohms
