---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Golden_rectangle
offline_file: ""
offline_thumbnail: ""
uuid: f4cf8601-c337-4c8a-abbc-2866590f7e0a
updated: 1484308927
title: Golden rectangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/225px-SimilarGoldenRectangles.svg.png
tags:
    - Construction
    - Relation to regular polygons and polyhedra
    - Applications
categories:
    - Elementary geometry
---
In geometry, a golden rectangle is a rectangle whose side lengths are in the golden ratio, 
  
    
      
        1
        :
        
          
            
              
                1
                +
                
                  
                    5
                  
                
              
              2
            
          
        
      
    
    {\displaystyle 1:{\tfrac {1+{\sqrt {5}}}{2}}}
  
, which is 
  
    
      
        1
        :
        φ
      
    
    {\displaystyle 1:\varphi }
  
 (the Greek letter phi), where 
  
    
      
        φ
      
    
    {\displaystyle \varphi }
  
 is approximately 1.618.
