---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approaching
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484380621
title: approaching
categories:
    - Dictionary
---
approaching
     adj : of the relatively near future; "the approaching election";
           "this coming Thursday"; "the forthcoming holidays";
           "the upcoming spring fashions" [syn: {coming(a)}, {forthcoming},
            {upcoming}]
     n 1: the event of one object coming closer to another [syn: {approach}]
     2: the temporal property of becoming nearer in time; "the
        approach of winter" [syn: {approach}, {coming}]
     3: the act of drawing spatially closer to something; "the
        hunter's approach scattered the geese" [syn: {approach}, {coming}]
