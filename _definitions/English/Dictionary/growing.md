---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/growing
offline_file: ""
offline_thumbnail: ""
uuid: 002d44cd-567f-462d-9a59-f2bfacaa8a22
updated: 1484310260
title: growing
categories:
    - Dictionary
---
growing
     adj 1: increasing in size or degree or amount; "her growing
            popularity"; "growing evidence of a world depression";
            "a growing city"; "growing businesses"
     2: having or showing vigorous vegetal or animal life;
        "flourishing crops"; "flourishing chicks"; "a growing
        boy"; "fast-growing weeds"; "a thriving deer population"
        [syn: {flourishing}, {thriving}]
     3: relating to or suitable for growth; "the growing season for
        corn"; "good growing weather"
     n 1: (biology) the process of an individual organism growing
          organically; a purely biological unfolding of events
          involved in an organism changing ...
