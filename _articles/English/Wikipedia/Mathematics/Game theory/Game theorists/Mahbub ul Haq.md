---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mahbub_ul_Haq
offline_file: ""
offline_thumbnail: ""
uuid: d10bea3a-c36d-4256-827c-db897fb252de
updated: 1484308513
title: Mahbub ul Haq
tags:
    - Early life
    - Career
    - Achievements
    - Death
    - Tributes from UN
    - >
        The Mahbub ul Haq Award for Outstanding Contribution to
        Human Development
    - Selected works
    - Notes
categories:
    - Game theorists
---
Mahbub ul Haq (Urdu: محبوب الحق‎; 24 February 1934 – 16 July 1998) was a Pakistani game theorist, economist and an international development theorist who served as the 13th Finance Minister of Pakistan from 10 April 1985 till 28 January 1988.[1]
