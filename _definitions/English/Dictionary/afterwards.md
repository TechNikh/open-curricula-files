---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afterwards
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477881
title: afterwards
categories:
    - Dictionary
---
afterwards
     adv : happening at a time subsequent to a reference time; "he
           apologized subsequently"; "he's going to the store but
           he'll be back here later"; "it didn't happen until
           afterward"; "two hours after that" [syn: {subsequently},
            {later}, {afterward}, {after}, {later on}]
