---
version: 1
type: video
provider: YouTube
id: P3QV9ktuYlQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Spring%20potential%20energy%20example%20%28mistake%20in%20math%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ed9bc160-a41b-46d3-9391-e24103eca3db
updated: 1486069593
title: Spring potential energy example (mistake in math)
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/P3QV9ktuYlQ/default.jpg
    - https://i3.ytimg.com/vi/P3QV9ktuYlQ/1.jpg
    - https://i3.ytimg.com/vi/P3QV9ktuYlQ/2.jpg
    - https://i3.ytimg.com/vi/P3QV9ktuYlQ/3.jpg
---
A spring, a frozen loop-d-loop and more! (See if you can find the mistake I made and get the right answer!). Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/work-and-energy/hookes-law/v/lol-diagrams?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/work-and-energy/hookes-law/v/potential-energy-stored-in-a-spring?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
