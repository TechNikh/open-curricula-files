---
version: 1
type: video
provider: YouTube
id: c-M4ZrdIgRs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Ramp%20Race.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a068aacc-92f1-4c12-9cd4-4b0738660615
updated: 1486069652
title: Ramp Race
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/c-M4ZrdIgRs/default.jpg
    - https://i3.ytimg.com/vi/c-M4ZrdIgRs/1.jpg
    - https://i3.ytimg.com/vi/c-M4ZrdIgRs/2.jpg
    - https://i3.ytimg.com/vi/c-M4ZrdIgRs/3.jpg
---
