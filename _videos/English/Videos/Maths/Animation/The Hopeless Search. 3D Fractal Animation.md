---
version: 1
type: video
provider: YouTube
id: pWbMXoDuANA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1c36d3d4-942c-4b87-b72b-202aad65eced
updated: 1486069647
title: The Hopeless Search. 3D Fractal Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/pWbMXoDuANA/default.jpg
    - https://i3.ytimg.com/vi/pWbMXoDuANA/1.jpg
    - https://i3.ytimg.com/vi/pWbMXoDuANA/2.jpg
    - https://i3.ytimg.com/vi/pWbMXoDuANA/3.jpg
---
