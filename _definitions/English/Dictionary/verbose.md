---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verbose
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522161
title: verbose
categories:
    - Dictionary
---
verbose
     adj : using or containing too many words; "long-winded (or windy)
           speakers"; "verbose and ineffective instructional
           methods"; "newspapers of the day printed long wordy
           editorials"; "proceedings were delayed by wordy
           disputes" [syn: {long-winded}, {tedious}, {windy}, {wordy}]
