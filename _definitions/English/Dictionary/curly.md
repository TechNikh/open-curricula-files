---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curly
offline_file: ""
offline_thumbnail: ""
uuid: 4d171895-b6a3-4a9b-aab1-1646e0e85091
updated: 1484310140
title: curly
categories:
    - Dictionary
---
curly
     adj : (of hair) having curls or waves; "they envied her naturally
           curly hair" [ant: {straight}]
     [also: {curliest}, {curlier}]
