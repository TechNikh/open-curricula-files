---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mosquitoes
offline_file: ""
offline_thumbnail: ""
uuid: 5a074cd6-6e5b-4460-8a82-621c4fb0ad45
updated: 1484310539
title: mosquitoes
categories:
    - Dictionary
---
mosquito
     n : two-winged insect whose female has a long proboscis to
         pierce the skin and suck the blood of humans and animals
     [also: {mosquitoes} (pl)]
