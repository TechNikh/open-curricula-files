---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cone_of_curves
offline_file: ""
offline_thumbnail: ""
uuid: 79e4804c-c4c2-4ba0-a00a-5fb0e7846af3
updated: 1484309142
title: Cone of curves
tags:
    - Definition
    - Applications
    - A structure theorem
categories:
    - Algebraic geometry
---
In mathematics, the cone of curves (sometimes the Kleiman-Mori cone) of an algebraic variety 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is a combinatorial invariant of much importance to the birational geometry of 
  
    
      
        X
      
    
    {\displaystyle X}
  
.
