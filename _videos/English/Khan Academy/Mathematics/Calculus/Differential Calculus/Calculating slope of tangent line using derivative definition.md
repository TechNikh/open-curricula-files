---
version: 1
type: video
provider: YouTube
id: IePCHjMeFkE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Calculating%20slope%20of%20tangent%20line%20using%20derivative%20definition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 522a2f3a-5619-4d5f-a18c-7039277978e4
updated: 1486069693
title: >
    Calculating slope of tangent line using derivative
    definition
categories:
    - Differential Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/IePCHjMeFkE/default.jpg
    - https://i3.ytimg.com/vi/IePCHjMeFkE/1.jpg
    - https://i3.ytimg.com/vi/IePCHjMeFkE/2.jpg
    - https://i3.ytimg.com/vi/IePCHjMeFkE/3.jpg
---
