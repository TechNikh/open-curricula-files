---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manchester
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460661
title: manchester
categories:
    - Dictionary
---
Manchester
     n 1: largest city in New Hampshire; located in southeastern New
          Hampshire on the Merrimack river
     2: a city in northwestern England (30 miles east of Liverpool);
        heart of the most densely populated area of England
