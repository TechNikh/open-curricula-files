---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corporate
offline_file: ""
offline_thumbnail: ""
uuid: e4fbb21d-2119-415f-a563-ca9fd58ab904
updated: 1484310597
title: corporate
categories:
    - Dictionary
---
corporate
     adj 1: of or belonging to a corporation; "corporate rates";
            "corporate structure"
     2: possessing or existing in bodily form; "what seemed corporal
        melted as breath into the wind"- Shakespeare; "an
        incarnate spirit"; "`corporate' is an archaic term" [syn:
        {bodied}, {corporal}, {embodied}, {incarnate}]
     3: done by or characteristic of individuals acting together; "a
        joint identity"; "the collective mind"; "the corporate
        good" [syn: {collective}]
     4: organized and maintained as a legal corporation; "a special
        agency set up in corporate form"; "an incorporated town"
        [syn: {incorporated}]
