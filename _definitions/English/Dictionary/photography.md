---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photography
offline_file: ""
offline_thumbnail: ""
uuid: e0498d6f-0704-47f5-bc5a-b6b87ab29902
updated: 1484310547
title: photography
categories:
    - Dictionary
---
photography
     n 1: the act of taking and printing photographs [syn: {picture
          taking}]
     2: the process of producing images of objects on photosensitive
        surfaces
     3: the occupation of taking and printing photographs or making
        movies
