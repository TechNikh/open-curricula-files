---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/childbearing
offline_file: ""
offline_thumbnail: ""
uuid: a4dca5a7-a923-4c00-93c2-ab17d54c7198
updated: 1484310471
title: childbearing
categories:
    - Dictionary
---
childbearing
     adj : relating to or suitable for childbirth; "of childbearing
           age"
     n : the parturition process in human beings; having a baby; the
         process of giving birth to a child [syn: {childbirth}, {accouchement},
          {vaginal birth}]
