---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Briggs%E2%80%93Rauscher_reaction'
offline_file: ""
offline_thumbnail: ""
uuid: ba3b0066-c951-493d-ad10-ea3759226fa2
updated: 1484308802
title: Briggs–Rauscher reaction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-B-ROscGraph2.jpg
tags:
    - History
    - Description
    - Initial conditions
    - Behaviour in time
    - Variants
    - Changing the initial concentrations
    - Changing the organic substrate
    - Continuous flow reactors
    - Two dimensional phase space plots
    - Fluorescent demonstration
    - Use as a biological assay
    - Chemical mechanism
    - videos
    - Effect of temperature
    - Preparations
categories:
    - Non-equilibrium thermodynamics
---
The Briggs–Rauscher oscillating reaction is one of a small number of known oscillating chemical reactions. It is especially well suited for demonstration purposes because of its visually striking colour changes: the freshly prepared colourless solution slowly turns an amber colour, suddenly changing to a very dark blue. This slowly fades to colourless and the process repeats, about ten times in the most popular formulation, before ending as a dark blue liquid smelling strongly of iodine.
