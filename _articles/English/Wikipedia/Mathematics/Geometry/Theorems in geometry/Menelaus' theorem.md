---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Menelaus%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4716b37b-f1da-4446-a52e-1e45981be06f
updated: 1484309116
title: "Menelaus' theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Menelaus%2527_theorem_1.svg.png'
tags:
    - Proof
    - A proof using homothecies
    - History
categories:
    - Theorems in geometry
---
Menelaus' theorem, named for Menelaus of Alexandria, is a proposition about triangles in plane geometry. Given a triangle ABC, and a transversal line that crosses BC, AC and AB at points D, E and F respectively, with D, E, and F distinct from A, B and C, then
