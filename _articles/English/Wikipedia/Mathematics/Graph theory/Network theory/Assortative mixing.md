---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Assortative_mixing
offline_file: ""
offline_thumbnail: ""
uuid: 6228106c-2d86-464f-8229-4f35ea721981
updated: 1484309432
title: Assortative mixing
categories:
    - Network theory
---
In the study of complex networks, assortative mixing, or assortativity, is a bias in favor of connections between network nodes with similar characteristics.[1] In the specific case of social networks, assortative mixing is also known as homophily. The rarer disassortative mixing is a bias in favor of connections between dissimilar nodes.
