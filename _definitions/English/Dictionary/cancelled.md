---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cancelled
offline_file: ""
offline_thumbnail: ""
uuid: dd47a020-8271-4781-8cb9-06a0608d01b6
updated: 1484310168
title: cancelled
categories:
    - Dictionary
---
cancelled
     adj : (of events) no longer planned or scheduled; "the wedding is
           definitely off" [syn: {off}] [ant: {on}]
