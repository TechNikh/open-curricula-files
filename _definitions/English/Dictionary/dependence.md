---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dependence
offline_file: ""
offline_thumbnail: ""
uuid: b47e0284-aab9-4802-ad63-827a4c79110a
updated: 1484310268
title: dependence
categories:
    - Dictionary
---
dependence
     n 1: lack of independence or self-sufficiency [syn: {dependance},
           {dependency}]
     2: being abnormally tolerant to and dependent on something that
        is psychologically or physically habit-forming (especially
        alcohol or narcotic drugs) [syn: {addiction}, {dependency},
         {habituation}]
