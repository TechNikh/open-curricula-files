---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrodynamic_bearing
offline_file: ""
offline_thumbnail: ""
uuid: 87fcccb5-26fe-4011-8ef2-d300b18125cf
updated: 1484308757
title: Electrodynamic bearing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Copper_Cylinder_Cut_through.png
tags:
    - Radial magnetic bearing
    - Avoiding eddy current losses
    - Linear magnetic bearing
categories:
    - Electrodynamics
---
Electrodynamic bearings (EDBs) are contactless electrodynamic suspensions of rotating shafts. Relative to active magnetic bearings (AMB) the passive nature of the levitation achieved by EDBs allows a simpler, more reliable and cheaper solution, opening the field of application to medium and large-scale production.[clarification needed][1][2][3]
