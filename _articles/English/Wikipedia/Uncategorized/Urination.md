---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Urination
offline_file: ""
offline_thumbnail: ""
uuid: 67588cb8-86e6-4628-852f-f2f3b163ce58
updated: 1484308378
title: Urination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Manneken_Pis_Brussel.jpg
tags:
    - Anatomy and physiology
    - Anatomy of the bladder and outlet
    - Physiology
    - Storage phase
    - Voiding phase
    - Voluntary control
    - Experience of urination
    - Disorders
    - Clinical conditions
    - Experimentally induced disorders
    - Deafferentation
    - Denervation
    - Spinal cord injury
    - Techniques
    - Male urination
    - Female urination
    - Young children
    - Fetal urination
    - Urination after injury
    - Facilities
    - Alternative urination tools
    - Social and cultural aspects
    - Toilet training
    - Urination without facilities
    - Standing versus sitting or squatting
    - Males
    - Females
    - Talking about urination
    - Use in language
    - Urination and sexual activity
    - Other species
    - Dog-like mammals (Canidae)
    - Cats (Felidae)
categories:
    - Uncategorized
---
Urination is the release of urine from the urinary bladder through the urethra to the outside of the body. It is the urinary system's form of excretion. It is also known medically as micturition, voiding, uresis, or, rarely, emiction, and known colloquially by various names including tinkling, peeing, weeing, whizzing and pissing.
