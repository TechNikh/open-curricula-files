---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oracle_machine
offline_file: ""
offline_thumbnail: ""
uuid: 9a30516c-d91c-4bcb-8b67-af9667ecd4f3
updated: 1484309487
title: Oracle machine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Blackbox.svg.png
tags:
    - Oracles
    - Definitions
    - Alternative definitions
    - Complexity classes of oracle machines
    - Oracles and halting problems
    - Applications to cryptography
categories:
    - Computability theory
---
In complexity theory and computability theory, an oracle machine is an abstract machine used to study decision problems. It can be visualized as a Turing machine with a black box, called an oracle, which is able to solve certain decision problems in a single operation. The problem can be of any complexity class. Even undecidable problems, like the halting problem, can be used.
