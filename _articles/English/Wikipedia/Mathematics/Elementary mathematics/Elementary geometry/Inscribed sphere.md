---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inscribed_sphere
offline_file: ""
offline_thumbnail: ""
uuid: 3b048196-87ee-4db2-b343-ac7348a6cc0d
updated: 1484308941
title: Inscribed sphere
tags:
    - Interpretations
categories:
    - Elementary geometry
---
In geometry, the inscribed sphere or insphere of a convex polyhedron is a sphere that is contained within the polyhedron and tangent to each of the polyhedron's faces. It is the largest sphere that is contained wholly within the polyhedron, and is dual to the dual polyhedron's circumsphere.
