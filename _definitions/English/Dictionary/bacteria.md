---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bacteria
offline_file: ""
offline_thumbnail: ""
uuid: dc6b9516-1a4e-4bac-a0b1-830245db3bc0
updated: 1484310327
title: bacteria
categories:
    - Dictionary
---
bacteria
     n : (microbiology) single-celled or noncellular spherical or
         spiral or rod-shaped organisms lacking chlorophyll that
         reproduce by fission; important as pathogens and for
         biochemical properties; taxonomy is difficult; often
         considered plants [syn: {bacterium}]
