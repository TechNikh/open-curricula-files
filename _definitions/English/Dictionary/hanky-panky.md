---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hanky-panky
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495821
title: hanky-panky
categories:
    - Dictionary
---
hanky panky
     n : verbal misrepresentation intended to take advantage of you
         in some way [syn: {trickery}, {hocus-pocus}, {slickness},
          {jiggery-pokery}, {skulduggery}, {skullduggery}]
