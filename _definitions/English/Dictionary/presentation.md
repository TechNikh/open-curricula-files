---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presentation
offline_file: ""
offline_thumbnail: ""
uuid: 4e64851e-360c-4007-9183-87c1c013e51b
updated: 1484096561
title: presentation
categories:
    - Dictionary
---
presentation
     n 1: the activity of formally presenting something (as a prize or
          reward); "she gave the trophy but he made the
          presentation"
     2: the act of making something publicly available; presenting
        news or other information by broadcasting or printing it;
        "he prepared his presentation carefully in advance"
     3: a show or display; the act of presenting something to sight
        or view; "the presentation of new data"; "he gave the
        customer a demonstration" [syn: {presentment}, {demonstration}]
     4: the act of presenting a proposal
     5: a visual representation of something [syn: {display}]
     6: formally making a person ...
