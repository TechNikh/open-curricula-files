---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kai_Behrend
offline_file: ""
offline_thumbnail: ""
uuid: 1609ff98-c8e1-43c4-90e1-4c0cbfd8315d
updated: 1484309071
title: Kai Behrend
categories:
    - Geometers
---
His work is in algebraic geometry and he has made important contributions in the theory of algebraic stacks, Gromov–Witten invariants and Donaldson–Thomas theory (cf. Behrend function.) He is also known for Behrend's formula, the generalization of the Grothendieck–Lefschetz trace formula to algebraic stacks. He is the recipient of the 2001 Coxeter–James Prize,[1] the 2011 Jeffery–Williams Prize,[2] and the 2015 CRM-Fields-PIMS Prize.[3]
