---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/differ
offline_file: ""
offline_thumbnail: ""
uuid: daad63bd-cecd-41a0-87c7-2586d04da784
updated: 1484310307
title: differ
categories:
    - Dictionary
---
differ
     v 1: be different; "These two tests differ in only one respect"
          [ant: {equal}]
     2: be of different opinions; "I beg to differ!"; "She disagrees
        with her husband on many questions" [syn: {disagree}, {dissent},
         {take issue}] [ant: {agree}]
