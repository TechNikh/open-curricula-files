---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hectare
offline_file: ""
offline_thumbnail: ""
uuid: 5b18b558-fd17-4089-af37-6acd3492214c
updated: 1484310256
title: hectare
categories:
    - Dictionary
---
hectare
     n : (abbreviated `ha') a unit of surface area equal to 100 ares
         (or 10,000 square meters)
