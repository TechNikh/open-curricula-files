---
version: 1
type: video
provider: YouTube
id: pbLiN8D9gAk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20inequalities/Constructing%2C%20solving%20two-step%20inequality%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 68d431dc-bf00-4cb1-9fb9-962e7462bdae
updated: 1486069561
title: Constructing, solving two-step inequality example
categories:
    - Linear inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/pbLiN8D9gAk/default.jpg
    - https://i3.ytimg.com/vi/pbLiN8D9gAk/1.jpg
    - https://i3.ytimg.com/vi/pbLiN8D9gAk/2.jpg
    - https://i3.ytimg.com/vi/pbLiN8D9gAk/3.jpg
---
