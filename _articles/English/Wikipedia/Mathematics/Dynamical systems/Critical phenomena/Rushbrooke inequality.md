---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rushbrooke_inequality
offline_file: ""
offline_thumbnail: ""
uuid: 64dde366-e7f0-483f-aa43-df9c148b244d
updated: 1484308746
title: Rushbrooke inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Rush-in-concert.jpg
categories:
    - Critical phenomena
---
In statistical mechanics, the Rushbrooke inequality relates the critical exponents of a magnetic system which exhibits a first-order phase transition in the thermodynamic limit for non-zero temperature T.
