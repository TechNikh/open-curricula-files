---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nutritious
offline_file: ""
offline_thumbnail: ""
uuid: 2dcc5137-935c-4ae9-a6b5-19df9556cf61
updated: 1484310531
title: nutritious
categories:
    - Dictionary
---
nutritious
     adj : of or providing nourishment; "good nourishing stew" [syn: {alimentary},
            {alimental}, {nourishing}, {nutrient}, {nutritive}]
