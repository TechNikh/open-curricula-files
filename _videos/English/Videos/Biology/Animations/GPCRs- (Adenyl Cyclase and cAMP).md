---
version: 1
type: video
provider: YouTube
id: 0nA2xhNiAow
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: df46a0b1-99a8-44bd-8611-4c572bae30a7
updated: 1486069669
title: 'GPCRs- (Adenyl Cyclase and cAMP)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/0nA2xhNiAow/default.jpg
    - https://i3.ytimg.com/vi/0nA2xhNiAow/1.jpg
    - https://i3.ytimg.com/vi/0nA2xhNiAow/2.jpg
    - https://i3.ytimg.com/vi/0nA2xhNiAow/3.jpg
---
