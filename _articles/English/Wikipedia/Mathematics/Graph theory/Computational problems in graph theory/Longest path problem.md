---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Longest_path_problem
offline_file: ""
offline_thumbnail: ""
uuid: 344634e4-f5f2-49fd-853b-e0c4db493edd
updated: 1484309392
title: Longest path problem
tags:
    - NP-hardness
    - Acyclic graphs and critical paths
    - Approximation
    - Parameterized complexity
    - Special classes of graphs
categories:
    - Computational problems in graph theory
---
In graph theory and theoretical computer science, the longest path problem is the problem of finding a simple path of maximum length in a given graph. A path is called simple if it does not have any repeated vertices; the length of a path may either be measured by its number of edges, or (in weighted graphs) by the sum of the weights of its edges. In contrast to the shortest path problem, which can be solved in polynomial time in graphs without negative-weight cycles, the longest path problem is NP-hard, meaning that it cannot be solved in polynomial time for arbitrary graphs unless P = NP. Stronger hardness results are also known showing that it is difficult to approximate. However, it ...
