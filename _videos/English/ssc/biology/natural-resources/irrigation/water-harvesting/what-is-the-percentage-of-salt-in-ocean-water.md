---
title: What Is The Percentage Of Salt In Ocean Water?
youtube_id: T0jYrAu_lGo
tags:
    - Seawater (Literature Subject)
    - Table Salt (Ingredient)
    - Ocean (Geographical Feature Category)
categories:
    - Water harvesting
---
