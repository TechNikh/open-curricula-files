---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 21a3b245-52f5-4e5b-8bb2-f9be597d90f7
updated: 1484308771
title: Differential entropy
tags:
    - Definition
    - Properties of differential entropy
    - Maximization in the normal distribution
    - 'Example: Exponential distribution'
    - Differential entropies for various distributions
    - Variants
categories:
    - Entropy and information
---
Differential entropy (also referred to as continuous entropy) is a concept in information theory that began as an attempt by Shannon to extend the idea of (Shannon) entropy, a measure of average surprisal of a random variable, to continuous probability distributions. Unfortunately, Shannon did not derive this formula, and rather just assumed it was the correct continuous analogue of discrete entropy, but it is not. The actual continuous version of discrete entropy is the limiting density of discrete points (LDDP). Differential entropy (described here) is commonly encountered in the literature, but it is a limiting case of the LDDP, and one that loses its fundamental association with ...
