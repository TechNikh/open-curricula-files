---
version: 1
type: video
provider: YouTube
id: B-LhPELGA-8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 521ec9fb-4974-4817-8a61-4d7c110a2ace
updated: 1486069611
title: Basic Addition
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/B-LhPELGA-8/default.jpg
    - https://i3.ytimg.com/vi/B-LhPELGA-8/1.jpg
    - https://i3.ytimg.com/vi/B-LhPELGA-8/2.jpg
    - https://i3.ytimg.com/vi/B-LhPELGA-8/3.jpg
---
