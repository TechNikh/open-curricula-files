---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/individuality
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462042
title: individuality
categories:
    - Dictionary
---
individuality
     n 1: the quality of being individual; "so absorbed by the
          movement that she lost all sense of individuality" [syn:
           {individualism}, {individuation}] [ant: {commonality}]
     2: the distinct personality of an individual regarded as a
        persisting entity; "you can lose your identity when you
        join the army" [syn: {identity}, {personal identity}]
