---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repressive
offline_file: ""
offline_thumbnail: ""
uuid: 415469fe-40ad-4d5d-a5fe-e943b252c6de
updated: 1484310575
title: repressive
categories:
    - Dictionary
---
repressive
     adj : restrictive of action; "a repressive regime"; "an overly
           strict and inhibiting discipline" [syn: {inhibitory}, {repressing}]
