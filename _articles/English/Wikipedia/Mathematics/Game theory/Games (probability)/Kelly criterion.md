---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kelly_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 42259993-0c42-4fd5-96d7-ce17adcc643c
updated: 1484309045
title: Kelly criterion
tags:
    - Statement
    - Proof
    - Bernoulli
    - Multiple horses
    - Application to the stock market
    - Single Asset
    - Many Assets
categories:
    - Games (probability)
---
In probability theory and intertemporal portfolio choice, the Kelly criterion, Kelly strategy, Kelly formula, or Kelly bet, is a formula used to determine the optimal size of a series of bets. In most gambling scenarios, and some investing scenarios under some simplifying assumptions, the Kelly strategy will do better than any essentially different strategy in the long run (that is, over a span of time in which the observed fraction of bets that are successful equals the probability that any given bet will be successful). It was described by J. L. Kelly, Jr, a researcher at Bell Labs, in 1956.[1] The practical use of the formula has been demonstrated.[2][3][4]
