---
version: 1
type: article
id: https://en.wikipedia.org/wiki/V._J._Havel
offline_file: ""
offline_thumbnail: ""
uuid: a973a639-c2c4-4345-b282-92ebb817270d
updated: 1484309463
title: V. J. Havel
categories:
    - Graph theorists
---
He is known for characterizing the degree sequences of undirected graphs.[1]
