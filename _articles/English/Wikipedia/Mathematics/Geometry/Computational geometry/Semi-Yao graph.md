---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semi-Yao_graph
offline_file: ""
offline_thumbnail: ""
uuid: c07aa749-efba-480e-8983-026201289d0e
updated: 1484309176
title: Semi-Yao graph
tags:
    - Construction
    - Properties
categories:
    - Computational geometry
---
The k-semi-Yao graph (k-SYG) of a set of n objects P is a geometric proximity graph, which was first described to present a kinetic data structure for maintenance of all the nearest neighbors on moving objects.[1] It is named for its relation to the Yao graph, which is named after Andrew Yao.
