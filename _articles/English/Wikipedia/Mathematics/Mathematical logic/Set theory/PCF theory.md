---
version: 1
type: article
id: https://en.wikipedia.org/wiki/PCF_theory
offline_file: ""
offline_thumbnail: ""
uuid: 41ec078f-693e-43dd-8e50-87b621b161d2
updated: 1484309509
title: PCF theory
tags:
    - Main definitions
    - Main results
    - Unsolved problems
    - Applications
categories:
    - Set theory
---
PCF theory is the name of a mathematical theory, introduced by Saharon Shelah (1978), that deals with the cofinality of the ultraproducts of ordered sets. It gives strong upper bounds on the cardinalities of power sets of singular cardinals, and has many more applications as well. The abbreviation "PCF" stands for "possible cofinalities".
