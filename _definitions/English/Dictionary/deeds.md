---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deeds
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443681
title: deeds
categories:
    - Dictionary
---
deeds
     n : performance of moral or religious acts; "salvation by
         deeds"; "the reward for good works" [syn: {works}]
