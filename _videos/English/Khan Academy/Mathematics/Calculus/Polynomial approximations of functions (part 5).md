---
version: 1
type: video
provider: YouTube
id: 9AoDucUmO20
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximations%20of%20functions%20%28part%205%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 485c911a-f5c8-45a5-ac14-b604064e206b
updated: 1486069689
title: Polynomial approximations of functions (part 5)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/9AoDucUmO20/default.jpg
    - https://i3.ytimg.com/vi/9AoDucUmO20/1.jpg
    - https://i3.ytimg.com/vi/9AoDucUmO20/2.jpg
    - https://i3.ytimg.com/vi/9AoDucUmO20/3.jpg
---
