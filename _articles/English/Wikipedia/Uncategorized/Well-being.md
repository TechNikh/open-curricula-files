---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Well-being
offline_file: ""
offline_thumbnail: ""
uuid: a826bcc2-b557-4733-8bcc-b53ef01c813f
updated: 1484308428
title: Well-being
tags:
    - Background
    - Cognitive
    - Affective
    - Psychology
    - Education
    - Financial
    - Subjective
    - Ethnic identity
    - Individual roles
    - Sports
    - Environment
    - Notes
    - Additional reading
categories:
    - Uncategorized
---
Well-being, wellbeing,[1] welfare or wellness is a general term for the condition of an individual or group, for example their social, economic, psychological, spiritual or medical state; a high level of well-being means in some sense the individual or group's condition is positive, while low well-being is associated with negative happenings.
