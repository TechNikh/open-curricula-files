---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polish
offline_file: ""
offline_thumbnail: ""
uuid: 4d853000-5514-42c1-96cc-51527b1c5bf6
updated: 1484310551
title: polish
categories:
    - Dictionary
---
Polish
     adj : of or relating to Poland or its people or culture; "Polish
           sausage"
     n 1: the property of being smooth and shiny [syn: {gloss}, {glossiness},
           {burnish}]
     2: a highly developed state of perfection; having a flawless or
        impeccable quality; "they performed with great polish"; "I
        admired the exquisite refinement of his prose"; "almost an
        inspiration which gives to all work that finish which is
        almost art"--Joseph Conrad [syn: {refinement}, {culture},
        {cultivation}, {finish}]
     3: a preparation used in polishing
     4: the Slavic language of Poland
     v 1: (of surfaces) make shine; "shine the silver, ...
