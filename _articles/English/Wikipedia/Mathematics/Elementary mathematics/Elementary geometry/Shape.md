---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shape
offline_file: ""
offline_thumbnail: ""
uuid: 4ff425a9-cc0f-4f32-a342-54b4c099f726
updated: 1484308938
title: Shape
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Congruent_non-congruent_triangles.svg.png
tags:
    - Classification of simple shapes
    - Shape in geometry
    - Equivalence of shapes
    - Congruence and similarity
    - Homeomorphism
    - Shape analysis
    - Similarity classes
categories:
    - Elementary geometry
---
A shape is the form of an object or its external boundary, outline, or external surface, as opposed to other properties such as color, texture, or material composition.
