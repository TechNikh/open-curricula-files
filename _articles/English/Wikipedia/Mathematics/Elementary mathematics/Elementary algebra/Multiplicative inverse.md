---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicative_inverse
offline_file: ""
offline_thumbnail: ""
uuid: 4f04a105-49fe-4630-b3bb-6b4fc02e01d3
updated: 1484308903
title: Multiplicative inverse
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hyperbola_one_over_x.svg.png
tags:
    - Examples and counterexamples
    - Complex numbers
    - calculus
    - Algorithms
    - Reciprocals of irrational numbers
    - Further remarks
    - Applications
    - Notes
categories:
    - Elementary algebra
---
In mathematics, a multiplicative inverse or reciprocal for a number x, denoted by 1/x or x−1, is a number which when multiplied by x yields the multiplicative identity, 1. The multiplicative inverse of a fraction a/b is b/a. For the multiplicative inverse of a real number, divide 1 by the number. For example, the reciprocal of 5 is one fifth (1/5 or 0.2), and the reciprocal of 0.25 is 1 divided by 0.25, or 4. The reciprocal function, the function f(x) that maps x to 1/x, is one of the simplest examples of a function which is its own inverse (an involution).
