---
version: 1
type: video
provider: YouTube
id: ATlUv-AGhEU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 326d4f11-e8f7-48b4-bacb-820372585765
updated: 1486069670
title: Mitosis
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ATlUv-AGhEU/default.jpg
    - https://i3.ytimg.com/vi/ATlUv-AGhEU/1.jpg
    - https://i3.ytimg.com/vi/ATlUv-AGhEU/2.jpg
    - https://i3.ytimg.com/vi/ATlUv-AGhEU/3.jpg
---
