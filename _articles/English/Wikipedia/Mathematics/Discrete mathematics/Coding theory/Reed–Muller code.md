---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Reed%E2%80%93Muller_code'
offline_file: ""
offline_thumbnail: ""
uuid: 793a7637-d0c8-49c7-8b7c-3ca93853c2e2
updated: 1484308556
title: Reed–Muller code
tags:
    - Construction
    - Building a generator matrix
    - Example 1
    - Example 2
    - Properties
    - Proof
    - Alternative construction
    - >
        Construction based on low-degree polynomials over a finite
        field
    - Table of Reed–Muller codes
    - Decoding RM codes
    - Notes
categories:
    - Coding theory
---
Reed–Muller codes are a family of linear error-correcting codes used in communications. Reed–Muller codes belong to the classes of locally testable codes and locally decodable codes, which is why they are useful in the design of probabilistically checkable proofs in computational complexity theory. They are named after Irving S. Reed and David E. Muller. Muller discovered the codes, and Reed proposed the majority logic decoding for the first time. Special cases of Reed–Muller codes include the Walsh–Hadamard code and the Reed–Solomon code.
