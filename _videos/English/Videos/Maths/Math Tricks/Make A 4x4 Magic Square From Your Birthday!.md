---
version: 1
type: video
provider: YouTube
id: xekvNbqSk78
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7d84d7d5-7bec-4e44-8efd-bb70d796d8a2
updated: 1486069623
title: Make A 4x4 Magic Square From Your Birthday!
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/xekvNbqSk78/default.jpg
    - https://i3.ytimg.com/vi/xekvNbqSk78/1.jpg
    - https://i3.ytimg.com/vi/xekvNbqSk78/2.jpg
    - https://i3.ytimg.com/vi/xekvNbqSk78/3.jpg
---
