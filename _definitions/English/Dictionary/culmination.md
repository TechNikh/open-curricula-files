---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/culmination
offline_file: ""
offline_thumbnail: ""
uuid: 6851fb61-d305-4d44-ad97-729a4960db96
updated: 1484310593
title: culmination
categories:
    - Dictionary
---
culmination
     n 1: a final climactic stage; "their achievements stand as a
          culmination of centuries of development" [syn: {apogee}]
     2: (astronomy) a heavenly body's highest celestial point above
        an observer's horizon
     3: the decisive moment in a novel or play; "the deathbed scene
        is the climax of the play" [syn: {climax}]
     4: a concluding action [syn: {completion}, {closing}, {windup},
         {mop up}]
