---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toads_and_Frogs
offline_file: ""
offline_thumbnail: ""
uuid: db90bab6-6b7e-43c7-81b9-1d92e033668b
updated: 1484308528
title: Toads and Frogs
tags:
    - Rules
    - Notation
    - Game-theoretic Values
categories:
    - Combinatorial game theory
---
The combinatorial game Toads and Frogs is a partisan game invented by Richard Guy. This mathematical game was used as an introductory game in the book Winning Ways for your Mathematical Plays.
