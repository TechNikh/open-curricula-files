---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kakinada
offline_file: ""
offline_thumbnail: ""
uuid: 473bf84d-67d7-471d-875b-838e61486613
updated: 1484308331
title: Kakinada
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/221px-MAIN_ROAD.jpg
tags:
    - Etymology
    - geography
    - Climate
    - Demographics
    - Governance
    - Economy
    - Port
    - Industrial sector
    - IT/ITES
    - RESOURCES
    - culture
    - Transport
    - Roadways
    - Railways
    - Seaways
    - Education
    - Tourism
    - Sports
categories:
    - Uncategorized
---
Kakinada  pronunciation (help·info) is a city in the Indian state of Andhra Pradesh. It is a municipal corporation located in Kakinada (urban) mandal of Kakinada revenue division and also the district headquarters of East Godavari district.[4] It is the 4th most populous city of the state.
