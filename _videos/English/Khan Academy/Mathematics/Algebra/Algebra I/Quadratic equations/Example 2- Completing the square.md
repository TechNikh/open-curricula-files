---
version: 1
type: video
provider: YouTube
id: 6agzj3A9IgA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Example%202-%20Completing%20the%20square.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 511206e1-fd70-4927-b996-cac14fc10851
updated: 1486069563
title: 'Example 2- Completing the square'
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6agzj3A9IgA/default.jpg
    - https://i3.ytimg.com/vi/6agzj3A9IgA/1.jpg
    - https://i3.ytimg.com/vi/6agzj3A9IgA/2.jpg
    - https://i3.ytimg.com/vi/6agzj3A9IgA/3.jpg
---
