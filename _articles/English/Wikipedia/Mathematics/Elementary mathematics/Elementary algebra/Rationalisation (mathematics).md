---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rationalisation_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 6c699ece-01b6-470c-ab86-be3a9ad8e0ef
updated: 1484308912
title: Rationalisation (mathematics)
tags:
    - Rationalisation of a monomial square root and cube root
    - Dealing with more square roots
    - Generalizations
categories:
    - Elementary algebra
---
If the denominator is a monomial in some radical, say 
  
    
      
        a
        
          
            
              x
              
                n
              
            
          
          
            k
          
        
        ,
      
    
    {\displaystyle a{\sqrt[{n}]{x}}^{k},}
  
 with k < n, rationalisation consists of multiplying the numerator and the denominator by 
  
    
      
        
          
            
              x
              
                n
              
            
          
          
            n
            −
            k
          
        
        ,
      
    
    {\displaystyle {\sqrt[{n}]{x}}^{n-k},}
  
 and replacing 
 ...
