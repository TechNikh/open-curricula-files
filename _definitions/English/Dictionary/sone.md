---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sone
offline_file: ""
offline_thumbnail: ""
uuid: bbcaf082-d4a8-4283-98c0-5e3e27be9bf7
updated: 1484310437
title: sone
categories:
    - Dictionary
---
sone
     n : a unit of perceived loudness equal to the loudness of a
         1000-hertz tone at 40 dB above threshold
