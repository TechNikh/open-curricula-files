---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/professing
offline_file: ""
offline_thumbnail: ""
uuid: abb697c8-5a4a-4cf1-b448-694b9b897b31
updated: 1484310601
title: professing
categories:
    - Dictionary
---
professing
     n : an open avowal (true or false) of some belief or opinion; "a
         profession of disagreement" [syn: {profession}]
