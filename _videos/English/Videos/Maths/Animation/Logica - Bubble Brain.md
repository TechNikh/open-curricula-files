---
version: 1
type: video
provider: YouTube
id: zUQC4GOOaAs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2bf77d39-3dda-470a-8c9f-2836c2371593
updated: 1486069648
title: 'Logica - Bubble Brain'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/zUQC4GOOaAs/default.jpg
    - https://i3.ytimg.com/vi/zUQC4GOOaAs/1.jpg
    - https://i3.ytimg.com/vi/zUQC4GOOaAs/2.jpg
    - https://i3.ytimg.com/vi/zUQC4GOOaAs/3.jpg
---
