---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brought
offline_file: ""
offline_thumbnail: ""
uuid: b5d88030-69cb-458f-a72c-694453b0f082
updated: 1484310244
title: brought
categories:
    - Dictionary
---
bring
     v 1: take something or somebody with oneself somewhere; "Bring me
          the box from the other room"; "Take these letters to the
          boss"; "This brings me to the main point" [syn: {convey},
           {take}]
     2: cause to come into a particular state or condition; "Long
        hard years of on the job training had brought them to
        their competence"; "bring water to the boiling point"
     3: cause to happen or to occur as a consequence; "I cannot work
        a miracle"; "wreak havoc"; "bring comments"; "play a
        joke"; "The rain brought relief to the drought-stricken
        area" [syn: {work}, {play}, {wreak}, {make for}]
     4: go or come after ...
