---
version: 1
type: video
provider: YouTube
id: Ybe6nqy3nys
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 03ab3e78-2b85-4450-97de-a98b8988356a
updated: 1486069619
title: Example 4 Applying the quadratic formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ybe6nqy3nys/default.jpg
    - https://i3.ytimg.com/vi/Ybe6nqy3nys/1.jpg
    - https://i3.ytimg.com/vi/Ybe6nqy3nys/2.jpg
    - https://i3.ytimg.com/vi/Ybe6nqy3nys/3.jpg
---
