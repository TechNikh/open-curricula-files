---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/piles
offline_file: ""
offline_thumbnail: ""
uuid: 5b517a4f-bb2f-4940-82b1-cc462c4026de
updated: 1484310486
title: piles
categories:
    - Dictionary
---
piles
     n 1: pain caused by venous swelling at or inside the anal
          sphincter [syn: {hemorrhoid}, {haemorrhoid}]
     2: a large number or amount; "made lots of new friends"; "she
        amassed a mountain of newspapers" [syn: {tons}, {dozens},
        {heaps}, {lots}, {mountain}, {scores}, {stacks}, {loads},
        {rafts}, {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
