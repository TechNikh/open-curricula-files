---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uniformity
offline_file: ""
offline_thumbnail: ""
uuid: 05b1f221-1732-402f-8b89-62c93507fdc0
updated: 1484310599
title: uniformity
categories:
    - Dictionary
---
uniformity
     n 1: a condition in which everything is regular and unvarying
     2: the quality of lacking diversity or variation (even to the
        point of boredom) [syn: {uniformness}] [ant: {nonuniformity}]
