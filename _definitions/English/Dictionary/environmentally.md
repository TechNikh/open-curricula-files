---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/environmentally
offline_file: ""
offline_thumbnail: ""
uuid: 0adc737c-14a6-47be-87e2-41afdcdb829e
updated: 1484310529
title: environmentally
categories:
    - Dictionary
---
environmentally
     adv : for the environment; "the new recycling policy is
           environmentally safe"
