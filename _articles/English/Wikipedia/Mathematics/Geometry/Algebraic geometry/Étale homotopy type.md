---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C3%89tale_homotopy_type'
offline_file: ""
offline_thumbnail: ""
uuid: e7fca0f0-409e-40f2-be4b-80a0780dcdd5
updated: 1484309145
title: Étale homotopy type
categories:
    - Algebraic geometry
---
In mathematics, especially in algebraic geometry, the étale homotopy type is an analogue of the homotopy type of topological spaces for algebraic varieties.
