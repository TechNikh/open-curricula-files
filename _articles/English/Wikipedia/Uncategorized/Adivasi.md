---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adivasis
offline_file: ""
offline_thumbnail: ""
uuid: 6d4b811e-bf23-4b3e-9aa5-c9db07fc0789
updated: 1484308455
title: Adivasi
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Women_in_tribal_village%252C_Umaria_district%252C_India_1.jpg'
tags:
    - Connotations of the word adivāsi
    - Scheduled tribes
    - Particularly vulnerable tribal groups
    - Geographical overview
    - History
    - Ancient India
    - Period of Islamic Invasions
    - British period
    - Participation in Indian independence movement
    - List of rebellions
    - Tribal classification criteria and demands
    - Demands for tribal classification
    - Endogamy, exogamy and ethnogenesis
    - Other criteria
    - religion
    - Animism
    - Donyi-Polo
    - Sanamahi
    - Sarna
    - Other tribal animist
    - Hinduism
    - Adivasi roots of modern Hinduism
    - Adivasi sants
    - Sages
    - Maharishis
    - Avatars
    - Other tribals and Hinduism
    - Demands for a separate religious code
    - Tribal system
    - Education
    - Adivasi(STs) Demography in India
    - Education
    - Tribal languages
    - Economy
    - Notable Adivasis
    - Freedom movement
    - 'Politics & Social service'
    - 'Art & Literature'
    - Administration
    - Sports
    - Military
    - Others
    - Gallery
    - Notes
categories:
    - Uncategorized
---
Adivasi (Hindi: आदिवासी, IPA: [aːd̪ɪˈʋaːsi]) are the tribal groups considered the Indigenous people population of South Asia.[1][2][3] Adivasi make up 8.6% of India's population or 104 million, according to the 2011 census, and a large percentage of the Nepalese population.[4][5][6] They comprise a substantial indigenous minority of the population of India and Nepal. The same term Adivasi is used for the ethnic minorities of Bangladesh, Pakistan, the native Tharu people of Nepal, and also the native Vedda people of Sri Lanka (Sinhalese: ආදී වාස).[7][8] The word is also used in the same sense in Nepal, as is another word, janajati (Nepali: जनजाति; ...
