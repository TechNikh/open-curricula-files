---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/music
offline_file: ""
offline_thumbnail: ""
uuid: e18f7639-ac8c-4430-b298-bb3c905de267
updated: 1484310397
title: music
categories:
    - Dictionary
---
music
     n 1: an artistic form of auditory communication incorporating
          instrumental or vocal tones in a structured and
          continuous manner
     2: any agreeable (pleasing and harmonious) sounds; "he fell
        asleep to the music of the wind chimes" [syn: {euphony}]
     3: musical activity (singing or whistling etc.); "his music was
        his central interest"
     4: (music) the sounds produced by singers or musical
        instruments (or reproductions of such sounds)
     5: punishment for one's actions; "you have to face the music";
        "take your medicine" [syn: {medicine}]
