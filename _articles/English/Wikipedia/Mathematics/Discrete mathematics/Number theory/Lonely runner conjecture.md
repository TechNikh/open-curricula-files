---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lonely_runner_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: e693acdf-11f5-41ce-8f8e-ecf33f9aa0d3
updated: 1484308649
title: Lonely runner conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lonely_runner.gif
tags:
    - Formulation
    - Known results
    - Notes
categories:
    - Number theory
---
In number theory, and especially the study of diophantine approximation, the lonely runner conjecture is a conjecture originally due to J. M. Wills in 1967. Applications of the conjecture are widespread in mathematics; they include view obstruction problems[1] and calculating the chromatic number of distance graphs and circulant graphs.[2] The conjecture was given its picturesque name by L. Goddyn in 1998.[3]
