---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Order-5_pentagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 26ffe4b5-9b25-41d1-be42-c9907bdd318f
updated: 1484309369
title: Order-5 pentagonal tiling
tags:
    - Related tilings
categories:
    - Geometry stubs
---
In geometry, the order-5 pentagonal tiling is a regular tiling of the hyperbolic plane. It has Schläfli symbol of {5,5}, constructed from five pentagons around every vertex. As such, it is self-dual.
