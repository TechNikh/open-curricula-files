---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marketing
offline_file: ""
offline_thumbnail: ""
uuid: dabae23a-6e1d-49bc-af92-c625e9cd9ad9
updated: 1484310459
title: marketing
categories:
    - Dictionary
---
marketing
     n 1: the exchange of goods for an agreed sum of money [syn: {selling},
           {merchandising}]
     2: the commercial processes involved in promoting and selling
        and distributing a product or service; "most companies
        have a manager in charge of marketing"
     3: shopping at a market; "does the weekly marketing at the
        supermarket"
