---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Enterprise_appliance_transaction_module
offline_file: ""
offline_thumbnail: ""
uuid: 8c9ec856-5e52-447d-b4fa-559515bfe043
updated: 1484308738
title: Enterprise appliance transaction module
categories:
    - Control engineering
---
WAn enterprise appliance transaction module (EATM) is a device, typically used in the manufacturing automation marketplace, for the transfer of plant floor equipment and product status to manufacturing execution systems (MES), enterprise resource planning (ERP) systems and the like.
