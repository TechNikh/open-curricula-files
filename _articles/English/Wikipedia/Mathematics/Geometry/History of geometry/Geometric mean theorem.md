---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_mean_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 1758dda0-f318-4a2b-b793-712c93d9213a
updated: 1484309245
title: Geometric mean theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Hoehensatz.svg.png
tags:
    - Theorem and application
    - Proof
    - Based on similarity
    - Based on dissection and rearrangement
categories:
    - History of geometry
---
The right triangle altitude theorem or geometric mean theorem is a result in elementary geometry that describes a relation between the lengths of the altitude on the hypotenuse in a right triangle and the two line segments it creates on the hypotenuse. It states that the geometric mean of the two segments equals the altitude.
