---
version: 1
type: video
provider: YouTube
id: ovZkFMuxZNc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Interference%20of%20waves%20on%20the%20surface%20of%20water%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fe3d2248-7d0f-44de-9964-20cbf2f0fd72
updated: 1486069652
title: Interference of waves on the surface of water HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ovZkFMuxZNc/default.jpg
    - https://i3.ytimg.com/vi/ovZkFMuxZNc/1.jpg
    - https://i3.ytimg.com/vi/ovZkFMuxZNc/2.jpg
    - https://i3.ytimg.com/vi/ovZkFMuxZNc/3.jpg
---
