---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diversity
offline_file: ""
offline_thumbnail: ""
uuid: ea0f6891-a69d-4e6c-952b-9775943b711a
updated: 1484310307
title: diversity
categories:
    - Dictionary
---
diversity
     n 1: noticeable heterogeneity; "a diversity of possibilities";
          "the range and variety of his work is amazing" [syn: {diverseness},
           {multifariousness}, {variety}]
     2: the condition or result of being changed
