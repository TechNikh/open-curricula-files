---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respectable
offline_file: ""
offline_thumbnail: ""
uuid: 425b3d70-356a-4d9c-9a54-3d3385bf627b
updated: 1484310168
title: respectable
categories:
    - Dictionary
---
respectable
     adj 1: worthy of respect; "a respectable woman" [ant: {unrespectable}]
     2: deserving of esteem and respect; "all respectable companies
        give guarantees"; "ruined the family's good name" [syn: {estimable},
         {good}, {honorable}]
     3: large in amount or extent or degree; "it cost a considerable
        amount"; "a goodly amount"; "received a hefty bonus"; "a
        respectable sum"; "a tidy sum of money"; "a sizable
        fortune" [syn: {goodly}, {goodish}, {hefty}, {sizable}, {sizeable},
         {tidy}]
