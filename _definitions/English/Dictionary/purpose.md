---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purpose
offline_file: ""
offline_thumbnail: ""
uuid: 055c2433-d9cd-43af-8885-dbbb3377a462
updated: 1484310343
title: purpose
categories:
    - Dictionary
---
purpose
     n 1: an anticipated outcome that is intended or that guides your
          planned actions; "his intent was to provide a new
          translation"; "good intentions are not enough"; "it was
          created with the conscious aim of answering immediate
          needs"; "he made no secret of his designs" [syn: {intent},
           {intention}, {aim}, {design}]
     2: what something is used for; "the function of an auger is to
        bore holes"; "ballet is beautiful but what use is it?"
        [syn: {function}, {role}, {use}]
     3: the quality of being determined to do or achieve something;
        "his determination showed in his every movement"; "he is a
        man of ...
