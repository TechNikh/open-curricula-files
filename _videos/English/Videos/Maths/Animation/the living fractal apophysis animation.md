---
version: 1
type: video
provider: YouTube
id: v7hR54GEygc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 916daf3b-20a8-4bc2-b645-fde7704a180b
updated: 1486069648
title: the living fractal apophysis animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/v7hR54GEygc/default.jpg
    - https://i3.ytimg.com/vi/v7hR54GEygc/1.jpg
    - https://i3.ytimg.com/vi/v7hR54GEygc/2.jpg
    - https://i3.ytimg.com/vi/v7hR54GEygc/3.jpg
---
