---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possession
offline_file: ""
offline_thumbnail: ""
uuid: b8551d36-fb31-49e2-84ed-88ba5622c545
updated: 1484310180
title: possession
categories:
    - Dictionary
---
possession
     n 1: the act of having and controlling property [syn: {ownership}]
     2: anything owned or possessed
     3: being controlled by passion or the supernatural
     4: a mania restricted to one thing or idea [syn: {monomania}]
     5: a territory that is controllled by a ruling state
     6: the trait of resolutely controlling your own behavior [syn:
        {self-control}, {self-possession}, {willpower}, {self-command},
         {self-will}]
     7: (sport) the act of controlling the ball (or puck); "they
        took possession of the ball on their own goal line"
