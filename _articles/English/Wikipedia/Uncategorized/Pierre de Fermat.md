---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fermat
offline_file: ""
offline_thumbnail: ""
uuid: 33a3dacb-21ab-46d6-9bd6-96704f368658
updated: 1484308327
title: Pierre de Fermat
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Beaumont-de-Lomagne_-_Monument_%25C3%25A0_Fermat.jpg'
tags:
    - Biography
    - Work
    - Death
    - Assessment of his work
    - Notes
    - Books referenced
categories:
    - Uncategorized
---
Pierre de Fermat (French: [pjɛːʁ də fɛʁma]; sometime between 31 October to 6 December 1607[1] – 12 January 1665) was a French lawyer at the Parlement of Toulouse, France, and a mathematician who is given credit for early developments that led to infinitesimal calculus, including his technique of adequality. In particular, he is recognized for his discovery of an original method of finding the greatest and the smallest ordinates of curved lines, which is analogous to that of differential calculus, then unknown, and his research into number theory. He made notable contributions to analytic geometry, probability, and optics. He is best known for Fermat's Last Theorem, which he ...
