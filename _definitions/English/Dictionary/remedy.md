---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remedy
offline_file: ""
offline_thumbnail: ""
uuid: b66833e6-18f1-49b5-ad86-646c8951dde8
updated: 1484310379
title: remedy
categories:
    - Dictionary
---
remedy
     n 1: act of correcting an error or a fault or an evil [syn: {redress},
           {remediation}]
     2: a medicine or therapy that cures disease or relieve pain
        [syn: {curative}, {cure}]
     v 1: set straight or right; "remedy these deficiencies"; "rectify
          the inequities in salaries"; "repair an oversight" [syn:
           {rectify}, {remediate}, {repair}, {amend}]
     2: provide relief for; "remedy his illness" [syn: {relieve}]
