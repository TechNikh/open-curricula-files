---
version: 1
type: video
provider: YouTube
id: AqJJ0f2gRGo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 304a6873-ae5e-4105-8f4a-769d83729aa1
updated: 1486069639
title: 'Optical prism - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/AqJJ0f2gRGo/default.jpg
    - https://i3.ytimg.com/vi/AqJJ0f2gRGo/1.jpg
    - https://i3.ytimg.com/vi/AqJJ0f2gRGo/2.jpg
    - https://i3.ytimg.com/vi/AqJJ0f2gRGo/3.jpg
---
