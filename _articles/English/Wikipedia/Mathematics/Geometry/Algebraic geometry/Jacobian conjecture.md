---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jacobian_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 62b2e5c2-538b-47ee-96d0-cce45aeedd4f
updated: 1484309156
title: Jacobian conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Pascal_Kossivi_Adjamagbo.jpg
tags:
    - The Jacobian determinant
    - Formulation of the conjecture
    - Results
categories:
    - Algebraic geometry
---
In mathematics, the Jacobian conjecture is a celebrated problem on polynomials in several variables. It was first posed in 1939 by Ott-Heinrich Keller. It was widely publicized by Shreeram Abhyankar, as an example of a question in the area of algebraic geometry that requires little beyond a knowledge of calculus to state.
