---
version: 1
type: video
provider: YouTube
id: M2N5848SPSA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 118c98b1-ab5a-42a3-a86b-7637a6492887
updated: 1486069636
title: 'Sugar in water - Electro Optics lab experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/M2N5848SPSA/default.jpg
    - https://i3.ytimg.com/vi/M2N5848SPSA/1.jpg
    - https://i3.ytimg.com/vi/M2N5848SPSA/2.jpg
    - https://i3.ytimg.com/vi/M2N5848SPSA/3.jpg
---
