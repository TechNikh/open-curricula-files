---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hell
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469601
title: hell
categories:
    - Dictionary
---
hell
     n 1: any place of pain and turmoil; "the hell of battle"; "the
          inferno of the engine room"; "when you're alone
          Christmas is the pits"; [syn: {hell on earth}, {hellhole},
           {snake pit}, {the pits}, {inferno}]
     2: a cause of difficulty and suffering; "war is hell"; "go to
        blazes" [syn: {blaze}]
     3: (Christianity) the abode of Satan and the forces of evil;
        where sinners suffer eternal punishment; "Hurl'd
        headlong...To bottomless perdition, there to dwell"- John
        Milton; "a demon from the depths of the pit" [syn: {perdition},
         {Inferno}, {infernal region}, {nether region}, {the pit}]
        [ant: {Heaven}]
   ...
