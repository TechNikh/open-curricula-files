---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dog
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484497021
title: dog
categories:
    - Dictionary
---
dog
     n 1: a member of the genus Canis (probably descended from the
          common wolf) that has been domesticated by man since
          prehistoric times; occurs in many breeds; "the dog
          barked all night" [syn: {domestic dog}, {Canis
          familiaris}]
     2: a dull unattractive unpleasant girl or woman; "she got a
        reputation as a frump"; "she's a real dog" [syn: {frump}]
     3: informal term for a man; "you lucky dog"
     4: someone who is morally reprehensible; "you dirty dog" [syn:
        {cad}, {bounder}, {blackguard}, {hound}, {heel}]
     5: a smooth-textured sausage of minced beef or pork usually
        smoked; often served on a bread roll [syn: ...
