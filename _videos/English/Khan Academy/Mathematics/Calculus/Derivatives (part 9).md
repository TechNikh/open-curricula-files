---
version: 1
type: video
provider: YouTube
id: aEP4C_kvcO4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Derivatives%20%28part%209%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4581065b-039d-4496-afff-6d1ec49f934b
updated: 1486069693
title: Derivatives (part 9)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/aEP4C_kvcO4/default.jpg
    - https://i3.ytimg.com/vi/aEP4C_kvcO4/1.jpg
    - https://i3.ytimg.com/vi/aEP4C_kvcO4/2.jpg
    - https://i3.ytimg.com/vi/aEP4C_kvcO4/3.jpg
---
