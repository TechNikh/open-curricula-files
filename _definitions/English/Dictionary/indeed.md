---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indeed
offline_file: ""
offline_thumbnail: ""
uuid: 16f9b93e-8823-4762-8315-276c170eabee
updated: 1484310447
title: indeed
categories:
    - Dictionary
---
indeed
     adv 1: in truth (often tends to intensify); "they said the car
            would break down and indeed it did"; "it is very cold
            indeed"; "was indeed grateful"; "indeed, the rain may
            still come"; "he did so do it!" [syn: {so}]
     2: (used as an interjection) an expression of surprise or
        skepticism or irony etc.; "Wants to marry the butler?
        Indeed!"
