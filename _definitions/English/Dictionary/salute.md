---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salute
offline_file: ""
offline_thumbnail: ""
uuid: 38d72807-c426-4894-ae25-250ad8c201ec
updated: 1484310565
title: salute
categories:
    - Dictionary
---
salute
     n 1: an act of honor or courteous recognition; "a musical salute
          to the composer on his birthday" [syn: {salutation}]
     2: a formal military gesture of respect [syn: {military
        greeting}]
     3: an act of greeting with friendly words and gestures like
        bowing or lifting the hat
     v 1: propose a toast to; "Let us toast the birthday girl!";
          "Let's drink to the New Year" [syn: {toast}, {drink}, {pledge},
           {wassail}]
     2: greet in a friendly way; "I meet this men every day on my
        way to work and he salutes me"
     3: express commendation of; "I salute your courage!"
     4: become noticeable; "a terrible stench saluted ...
