---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Niven%27s_constant'
offline_file: ""
offline_thumbnail: ""
uuid: 5a0e5fe5-5859-4547-8f6d-91b2b8259412
updated: 1484308657
title: "Niven's constant"
categories:
    - Number theory
---
In number theory, Niven's constant, named after Ivan Niven, is the largest exponent appearing in the prime factorization of any natural number n "on average". More precisely, if we define H(1) = 1 and H(n) = the largest exponent appearing in the unique prime factorization of a natural number n > 1, then Niven's constant is given by
