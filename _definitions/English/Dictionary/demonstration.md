---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demonstration
offline_file: ""
offline_thumbnail: ""
uuid: 8eb77491-2d9e-4acb-8f59-03baa29b37ed
updated: 1484310569
title: demonstration
categories:
    - Dictionary
---
demonstration
     n 1: a show or display; the act of presenting something to sight
          or view; "the presentation of new data"; "he gave the
          customer a demonstration" [syn: {presentation}, {presentment}]
     2: a show of military force or preparedness; "he confused the
        enemy with feints and demonstrations"
     3: a public display of group feelings (usually of a political
        nature); "there were violent demonstrations against the
        war" [syn: {manifestation}]
     4: proof by a process of argument or a series of proposition
        proving an asserted conclusion [syn: {monstrance}]
     5: a visual presentation showing how something works; "the
        ...
