---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pseudorandomness
offline_file: ""
offline_thumbnail: ""
uuid: b231f60e-d771-4e85-9539-cade31be3157
updated: 1484308678
title: Pseudorandomness
tags:
    - History
    - Almost random
    - Pseudorandomness in computational complexity
    - Cryptography
    - Monte Carlo method simulations
categories:
    - Theoretical computer science
---
A pseudorandom process is a process that appears to be random but is not. Pseudorandom sequences typically exhibit statistical randomness while being generated by an entirely deterministic causal process. Such a process is easier to produce than a genuinely random one, and has the benefit that it can be used again and again to produce exactly the same numbers - useful for testing and fixing software.
