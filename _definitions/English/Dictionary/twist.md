---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463541
title: twist
categories:
    - Dictionary
---
twist
     n 1: an unforeseen development; "events suddenly took an awkward
          turn" [syn: {turn}, {turn of events}]
     2: an interpretation of a text or action; "they put an
        unsympathetic construction on his conduct" [syn: {construction}]
     3: any clever (deceptive) maneuver; "he would stoop to any
        device to win a point" [syn: {device}, {gimmick}]
     4: the act of rotating rapidly; "he gave the crank a spin"; "it
        broke off after much twisting" [syn: {spin}, {twirl}, {twisting},
         {whirl}]
     5: a sharp strain on muscles or ligaments; "the wrench to his
        knee occurred as he fell"; "he was sidelined with a
        hamstring pull" [syn: ...
