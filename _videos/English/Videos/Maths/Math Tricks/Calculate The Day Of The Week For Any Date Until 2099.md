---
version: 1
type: video
provider: YouTube
id: UsStBxY9STw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bfae6638-e182-4e69-98aa-6c0b30a6af75
updated: 1486069623
title: Calculate The Day Of The Week For Any Date Until 2099
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/UsStBxY9STw/default.jpg
    - https://i3.ytimg.com/vi/UsStBxY9STw/1.jpg
    - https://i3.ytimg.com/vi/UsStBxY9STw/2.jpg
    - https://i3.ytimg.com/vi/UsStBxY9STw/3.jpg
---
