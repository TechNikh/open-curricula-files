---
version: 1
type: video
provider: YouTube
id: grtfGQEc7NA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c17cc2f4-399e-47b0-af62-cd15a14d3418
updated: 1486069649
title: 'Golden Gates - Mandelbulb 3D Fractal Full HD feat. Suduaya "Cellular Memory" Psychedelic Chillout'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/grtfGQEc7NA/default.jpg
    - https://i3.ytimg.com/vi/grtfGQEc7NA/1.jpg
    - https://i3.ytimg.com/vi/grtfGQEc7NA/2.jpg
    - https://i3.ytimg.com/vi/grtfGQEc7NA/3.jpg
---
