---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflex
offline_file: ""
offline_thumbnail: ""
uuid: c6438ffe-1b45-4052-bf8a-037e8f430a64
updated: 1484310160
title: reflex
categories:
    - Dictionary
---
reflex
     adj : without volition or conscious control; "the automatic
           shrinking of the pupils of the eye in strong light"; "a
           reflex knee jerk"; "sneezing is reflexive" [syn: {automatic},
            {reflex(a)}, {reflexive}]
     n : an automatic instinctive unlearned reaction to a stimulus
         [syn: {instinctive reflex}, {innate reflex}, {inborn
         reflex}, {unconditioned reflex}, {physiological reaction}]
