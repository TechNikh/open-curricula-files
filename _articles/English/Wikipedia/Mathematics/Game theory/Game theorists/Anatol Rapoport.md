---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anatol_Rapoport
offline_file: ""
offline_thumbnail: ""
uuid: 25577567-5feb-429c-b63e-c4a28912cc9b
updated: 1484309025
title: Anatol Rapoport
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Anatol_Rapoport.jpg
tags:
    - Biography
    - Work
    - Game theory
    - Social network analysis
    - Conflict and peace studies
    - Publications
categories:
    - Game theorists
---
Anatol Rapoport (Russian: Анато́лий Бори́сович Рапопо́рт; May 22, 1911 – January 20, 2007) was a Russian-born American mathematical psychologist. He contributed to general systems theory, mathematical biology and to the mathematical modeling of social interaction and stochastic models of contagion.
