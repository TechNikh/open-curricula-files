---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/locality
offline_file: ""
offline_thumbnail: ""
uuid: ac23bd53-d810-434b-a302-b37cefb664e7
updated: 1484310262
title: locality
categories:
    - Dictionary
---
locality
     n : a surrounding or nearby region; "the plane crashed in the
         vicinity of Asheville"; "it is a rugged locality"; "he
         always blames someone else in the immediate
         neighborhood"; "I will drop in on you the next time I am
         in this neck of the woods" [syn: {vicinity}, {neighborhood},
          {neighbourhood}, {neck of the woods}]
