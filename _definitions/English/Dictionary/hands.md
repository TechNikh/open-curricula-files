---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hands
offline_file: ""
offline_thumbnail: ""
uuid: 72eafb89-4abd-4a93-92e6-6e7ef221c8fb
updated: 1484310395
title: hands
categories:
    - Dictionary
---
hands
     n 1: (with `in') guardianship over; in divorce cases it is the
          right to house and care for and discipline a child; "my
          fate is in your hands"; "too much power in the
          president's hands"; "your guests are now in my custody";
          "the mother was awarded custody of the children" [syn: {custody}]
     2: the force of workers available [syn: {work force}, {workforce},
         {manpower}, {men}]
