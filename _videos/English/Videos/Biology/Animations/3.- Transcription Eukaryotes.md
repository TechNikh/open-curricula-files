---
version: 1
type: video
provider: YouTube
id: icZjgZozkB8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/3.-%20Transcription%20Eukaryotes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c3cfa3c8-d2f8-4ccc-8ec0-a2d4775e54c0
updated: 1486069670
title: '3.- Transcription Eukaryotes'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/icZjgZozkB8/default.jpg
    - https://i3.ytimg.com/vi/icZjgZozkB8/1.jpg
    - https://i3.ytimg.com/vi/icZjgZozkB8/2.jpg
    - https://i3.ytimg.com/vi/icZjgZozkB8/3.jpg
---
