---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/batting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484471881
title: batting
categories:
    - Dictionary
---
bat
     n 1: nocturnal mouselike mammal with forelimbs modified to form
          membranous wings and anatomical adaptations for
          echolocation by which they navigate [syn: {chiropteran}]
     2: (baseball) a turn batting; "he was at bat when it happened";
        "he got 4 hits in 4 at-bats" [syn: {at-bat}]
     3: a small racket with a long handle used for playing squash
        [syn: {squash racket}, {squash racquet}]
     4: a bat used in playing cricket [syn: {cricket bat}]
     5: a club used for hitting a ball in various games
     v 1: strike with, or as if with a baseball bat; "bat the ball"
     2: wink briefly; "bat one's eyelids" [syn: {flutter}]
     3: have a turn at ...
