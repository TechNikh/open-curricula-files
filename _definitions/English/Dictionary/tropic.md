---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tropic
offline_file: ""
offline_thumbnail: ""
uuid: 007421f9-00b4-4546-b186-994897cac303
updated: 1484310273
title: tropic
categories:
    - Dictionary
---
tropic
     adj 1: relating to or situated in or characteristic of the tropics
            (the region on either side of the equator); "tropical
            islands"; "tropical fruit" [syn: {tropical}]
     2: of weather or climate; hot and humid as in the tropics;
        "tropical weather" [syn: {tropical}]
     n : either of two parallels of latitude about 23.5 degrees north
         and south of the equator representing the points farthest
         north and south at which the sun can shine directly
         overhead and constituting the boundaries of the torrid
         zone or tropics
