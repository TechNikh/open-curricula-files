---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overall
offline_file: ""
offline_thumbnail: ""
uuid: 456c586f-2537-4e88-aea2-7a0abf26f1dc
updated: 1484310411
title: overall
categories:
    - Dictionary
---
overall
     adj 1: involving only main features; "the overall pattern of his
            life"
     2: including everything; "the overall cost"; "the total amount
        owed" [syn: {total}]
     n 1: work clothing consisting of denim trousers (usually with a
          bib and shoulder straps)
     2: a loose protective coverall or smock worn over ordinary
        clothing for dirty work [syn: {boilersuit}, {boilers suit}]
