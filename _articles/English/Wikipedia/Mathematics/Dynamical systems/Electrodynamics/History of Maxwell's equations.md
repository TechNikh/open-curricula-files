---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/History_of_Maxwell%27s_equations'
offline_file: ""
offline_thumbnail: ""
uuid: 656ac6af-d1cd-42c0-9091-314187af8548
updated: 1484308762
title: "History of Maxwell's equations"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_1.png
tags:
    - >
        Relationships among electricity, magnetism, and the speed of
        light
    - "The term Maxwell's equations"
    - On Physical Lines of Force
    - A Dynamical Theory of the Electromagnetic Field
    - A Treatise on Electricity and Magnetism
    - Relativity
    - Notes
categories:
    - Electrodynamics
---
In electromagnetism, one of the fundamental fields of physics, the introduction of Maxwell's equations (mainly in "A Dynamical Theory of the Electromagnetic Field") was one of the most important aggregations of empirical facts in the history of physics. It took place in the nineteenth century, starting from basic experimental observations, and leading to the formulations of numerous mathematical equations, notably by Charles-Augustin de Coulomb, Hans Christian Ørsted, Carl Friedrich Gauss, Jean-Baptiste Biot, Félix Savart, André-Marie Ampère, and Michael Faraday. The apparently disparate laws and phenomena of electricity and magnetism were integrated by James Clerk Maxwell, who ...
