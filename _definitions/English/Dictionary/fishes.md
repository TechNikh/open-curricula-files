---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fishes
offline_file: ""
offline_thumbnail: ""
uuid: 74228795-9d40-4ea4-b156-7142e66e61f6
updated: 1484310271
title: fishes
categories:
    - Dictionary
---
fish
     n 1: any of various mostly cold-blooded aquatic vertebrates
          usually having scales and breathing through gills; "the
          shark is a large fish"; "in the livingroom there was a
          tank of colorful fish"
     2: the flesh of fish used as food; "in Japan most fish is eaten
        raw"; "after the scare about foot-and-mouth disease a lot
        of people started eating fish instead of meat"; "they have
        a chef who specializes in fish"
     3: (astrology) a person who is born while the sun is in Pisces
        [syn: {Pisces}]
     4: the twelfth sign of the zodiac; the sun is in this sign from
        about February 19 to March 20 [syn: {Pisces}, {Pisces ...
