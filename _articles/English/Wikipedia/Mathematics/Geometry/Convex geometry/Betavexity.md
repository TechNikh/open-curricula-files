---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Betavexity
offline_file: ""
offline_thumbnail: ""
uuid: b1ba2bbe-f629-41fd-b830-c2747d4eec2f
updated: 1484309192
title: Betavexity
categories:
    - Convex geometry
---
In investment analysis, betavexity is a form of convexity[1] that is specific to the beta coefficient[2] of a long tailed investment (i.e. mortality risk). It is similar in nature to bond convexity or gamma that are exhibited in financial products such as bonds or options but is specific to portfolios replicating indices of shorter maturities.
