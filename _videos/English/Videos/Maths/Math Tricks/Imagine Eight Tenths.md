---
version: 1
type: video
provider: YouTube
id: NARFDtO7Ii4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de52fed5-f85d-4f3b-b576-13fc0294cf96
updated: 1486069625
title: Imagine Eight Tenths
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/NARFDtO7Ii4/default.jpg
    - https://i3.ytimg.com/vi/NARFDtO7Ii4/1.jpg
    - https://i3.ytimg.com/vi/NARFDtO7Ii4/2.jpg
    - https://i3.ytimg.com/vi/NARFDtO7Ii4/3.jpg
---
