---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proceed
offline_file: ""
offline_thumbnail: ""
uuid: 3733d388-ad4c-4ba2-8deb-866da9223b44
updated: 1484310200
title: proceed
categories:
    - Dictionary
---
proceed
     v 1: continue with one's activities; "I know it's hard," he
          continued, "but there is no choice"; "carry on--pretend
          we are not in the room" [syn: {continue}, {go on}, {carry
          on}]
     2: move ahead; travel onward in time or space; "We proceeded
        towards Washington"; "She continued in the direction of
        the hills"; "We are moving ahead in time now" [syn: {go
        forward}, {continue}]
     3: follow a procedure or take a course; "We should go farther
        in this matter"; "She went through a lot of trouble"; "go
        about the world in a certain manner"; "Messages must go
        through diplomatic channels" [syn: {go}, ...
