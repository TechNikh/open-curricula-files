---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retirement
offline_file: ""
offline_thumbnail: ""
uuid: 47e585ae-8a5e-4f79-ac32-fd3d8f72824f
updated: 1484310469
title: retirement
categories:
    - Dictionary
---
retirement
     n 1: the state of being retired from one's business or occupation
     2: withdrawal from your position or occupation
     3: withdrawal for prayer and study and meditation; "a religious
        retreat" [syn: {retreat}]
