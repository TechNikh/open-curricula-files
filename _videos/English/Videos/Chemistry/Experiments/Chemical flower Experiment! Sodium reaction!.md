---
version: 1
type: video
provider: YouTube
id: RQzFPQ5sy-w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3ce6fdb0-8fb4-4b4d-9906-75f171b210f0
updated: 1486069661
title: Chemical flower Experiment! Sodium reaction!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/RQzFPQ5sy-w/default.jpg
    - https://i3.ytimg.com/vi/RQzFPQ5sy-w/1.jpg
    - https://i3.ytimg.com/vi/RQzFPQ5sy-w/2.jpg
    - https://i3.ytimg.com/vi/RQzFPQ5sy-w/3.jpg
---
