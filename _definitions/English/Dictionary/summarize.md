---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summarize
offline_file: ""
offline_thumbnail: ""
uuid: a226fba1-0640-44fc-a085-d10d8e9dcbb7
updated: 1484310349
title: summarize
categories:
    - Dictionary
---
summarize
     v 1: give a summary (of); "he summed up his results"; "I will now
          summarize" [syn: {sum up}, {summarise}, {resume}]
     2: be a summary of; "The abstract summarizes the main ideas in
        the paper" [syn: {summarise}, {sum}, {sum up}]
