---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reinforced
offline_file: ""
offline_thumbnail: ""
uuid: b8917bab-bb3e-49ae-9291-023c1739c594
updated: 1484310581
title: reinforced
categories:
    - Dictionary
---
reinforced
     adj 1: given added strength or support; "reinforced concrete
            contains steel bars or metal netting" [syn: {strengthened}]
     2: (used of soaps or cleaning agents) having a substance (an
        abrasive or filler) added to increase effectiveness; "the
        built liquid detergents" [syn: {built}]
