---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_characteristic
offline_file: ""
offline_thumbnail: ""
uuid: 19368237-f44f-4bb4-ad3a-f15288ff1aac
updated: 1484309479
title: Euler characteristic
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/459px-V-E%252BF%253D2_Proof_Illustration.svg.png'
tags:
    - Polyhedra
    - Plane graphs
    - "Proof of Euler's formula"
    - Topological definition
    - Properties
    - Homotopy invariance
    - Inclusion-exclusion principle
    - Connected Sum
    - Product property
    - Covering spaces
    - Fibration property
    - Examples
    - Surfaces
    - Soccer ball
    - Arbitrary dimensions
    - Relations to other invariants
    - Generalizations
    - Notes
    - Bibliography
categories:
    - Topological graph theory
---
In mathematics, and more specifically in algebraic topology and polyhedral combinatorics, the Euler characteristic (or Euler number, or Euler–Poincaré characteristic) is a topological invariant, a number that describes a topological space's shape or structure regardless of the way it is bent. It is commonly denoted by 
  
    
      
        χ
      
    
    {\displaystyle \chi }
  
 (Greek lower-case letter chi).
