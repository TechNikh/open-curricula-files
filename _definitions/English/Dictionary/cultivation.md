---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivation
offline_file: ""
offline_thumbnail: ""
uuid: 157a3043-9656-4170-96a8-be95da646b9f
updated: 1484310260
title: cultivation
categories:
    - Dictionary
---
cultivation
     n 1: socialization through training and education
     2: (agriculture) production of food by preparing the land to
        grow crops
     3: a highly developed state of perfection; having a flawless or
        impeccable quality; "they performed with great polish"; "I
        admired the exquisite refinement of his prose"; "almost an
        inspiration which gives to all work that finish which is
        almost art"--Joseph Conrad [syn: {polish}, {refinement}, {culture},
         {finish}]
