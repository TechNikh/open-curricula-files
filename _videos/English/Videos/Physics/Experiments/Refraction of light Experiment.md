---
version: 1
type: video
provider: YouTube
id: xOKFuAKS154
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9c1c955-1291-46b1-9b73-a0dd5de6f423
updated: 1486069635
title: Refraction of light Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/xOKFuAKS154/default.jpg
    - https://i3.ytimg.com/vi/xOKFuAKS154/1.jpg
    - https://i3.ytimg.com/vi/xOKFuAKS154/2.jpg
    - https://i3.ytimg.com/vi/xOKFuAKS154/3.jpg
---
