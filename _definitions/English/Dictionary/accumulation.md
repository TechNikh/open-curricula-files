---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accumulation
offline_file: ""
offline_thumbnail: ""
uuid: ef0d7e58-a154-4836-adce-c81fddabfb4b
updated: 1484310289
title: accumulation
categories:
    - Dictionary
---
accumulation
     n 1: an increase by natural growth or addition [syn: {accretion}]
     2: several things grouped together or considered as a whole
        [syn: {collection}, {aggregation}, {assemblage}]
     3: the act of accumulating [syn: {accrual}, {accruement}]
     4: (finance) profits that are not paid out as dividends but are
        added to the capital base of the corporation
