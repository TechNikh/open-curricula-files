---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_linear_space
offline_file: ""
offline_thumbnail: ""
uuid: b0785e57-2dd8-4634-9d3a-82df7ca0c245
updated: 1484309369
title: Partial linear space
tags:
    - Definition
    - Examples
categories:
    - Geometry stubs
---
A partial linear space (also semilinear or near-linear space) is a basic incidence structure in the field of incidence geometry, that carries slightly less structure than a linear space. The notion is equivalent to that of a linear hypergraph.
