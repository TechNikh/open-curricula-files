---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extend
offline_file: ""
offline_thumbnail: ""
uuid: c66f8085-1885-4142-9143-7c1edf64c9d8
updated: 1484310311
title: extend
categories:
    - Dictionary
---
extend
     v 1: extend in scope or range or area; "The law was extended to
          all citizens"; "widen the range of applications";
          "broaden your horizon"; "Extend your backyard" [syn: {widen},
           {broaden}]
     2: stretch out over a distance, space, time, or scope; run or
        extend between two points or beyond a certain point;
        "Service runs all the way to Cranbury"; "His knowledge
        doesn't go very far"; "My memory extends back to my fourth
        year of life"; "The facts extend beyond a consideration of
        her personal assets" [syn: {run}, {go}, {pass}, {lead}]
     3: span an interval of distance, space or time; "The war
        extended ...
