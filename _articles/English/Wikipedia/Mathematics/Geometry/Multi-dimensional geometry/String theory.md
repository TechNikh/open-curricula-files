---
version: 1
type: article
id: https://en.wikipedia.org/wiki/String_theory
offline_file: ""
offline_thumbnail: ""
uuid: 94de00c4-eca7-4bef-b12b-f64001853fe1
updated: 1484309292
title: String theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Open_and_closed_strings.svg.png
tags:
    - Fundamentals
    - Strings
    - Extra dimensions
    - Dualities
    - Branes
    - M-theory
    - Unification of superstring theories
    - Matrix theory
    - Black holes
    - Bekenstein–Hawking formula
    - Derivation within string theory
    - AdS/CFT correspondence
    - Overview of the correspondence
    - Applications to quantum gravity
    - Applications to quantum field theory
    - Phenomenology
    - Particle physics
    - Cosmology
    - Connections to mathematics
    - Mirror symmetry
    - Monstrous moonshine
    - History
    - Early results
    - First superstring revolution
    - Second superstring revolution
    - Criticism
    - Number of solutions
    - Background independence
    - Sociological issues
    - Notes
    - Citations
    - Bibliography
    - Popularizations
    - General
    - Critical
    - Textbooks
    - For physicists
    - For mathematicians
categories:
    - Multi-dimensional geometry
---
In physics, string theory is a theoretical framework in which the point-like particles of particle physics are replaced by one-dimensional objects called strings. It describes how these strings propagate through space and interact with each other. On distance scales larger than the string scale, a string looks just like an ordinary particle, with its mass, charge, and other properties determined by the vibrational state of the string. In string theory, one of the many vibrational states of the string corresponds to the graviton, a quantum mechanical particle that carries gravitational force. Thus string theory is a theory of quantum gravity.
