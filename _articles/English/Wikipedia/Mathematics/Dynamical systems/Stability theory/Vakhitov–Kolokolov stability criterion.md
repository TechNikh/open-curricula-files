---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vakhitov%E2%80%93Kolokolov_stability_criterion'
offline_file: ""
offline_thumbnail: ""
uuid: da4d6677-e846-4146-b0ae-8c7b7e24988b
updated: 1484308825
title: Vakhitov–Kolokolov stability criterion
tags:
    - Original formulation
    - Generalizations
categories:
    - Stability theory
---
The Vakhitov–Kolokolov stability criterion is a condition for linear stability (sometimes called spectral stability) of solitary wave solutions to a wide class of U(1)-invariant Hamiltonian systems, named after Russian scientists Aleksandr Kolokolov (Александр Александрович Колоколов) and Nazib Vakhitov (Назиб Галиевич Вахитов). The condition for linear stability of a solitary wave 
  
    
      
        u
        (
        x
        ,
        t
        )
        =
        
          ϕ
          
            ω
          
        
        (
        x
        )
        
          e
          
            −
            i
            ω
 ...
