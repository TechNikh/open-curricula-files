---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bare
offline_file: ""
offline_thumbnail: ""
uuid: a1c0ebe3-4f82-48ce-94e0-f47c5b181c78
updated: 1484310366
title: bare
categories:
    - Dictionary
---
bare
     adj 1: denuded of leaves; "the bare branches of winter"
     2: completely unclothed; "bare bodies"; "naked from the waist
        up"; "a nude model" [syn: {au naturel(p)}, {naked}, {nude}]
     3: lacking in amplitude or quantity; "a bare livelihood"; "a
        scanty harvest"; "a spare diet" [syn: {bare(a)}, {scanty},
         {spare}]
     4: without the natural or usual covering; "a bald spot on the
        lawn"; "bare hills" [syn: {bald}, {denuded}, {denudate}]
     5: not having a protective covering; "unsheathed cables"; "a
        bare blade" [syn: {unsheathed}] [ant: {sheathed}]
     6: just barely adequate or within a lower limit; "a bare
        majority"; "a ...
