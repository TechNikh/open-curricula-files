---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planar_cover
offline_file: ""
offline_thumbnail: ""
uuid: 51b447d6-c478-47a8-8b04-f22abdb0de8e
updated: 1484309414
title: Planar cover
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Covering-graph-4.svg.png
tags:
    - Definition
    - Examples
    - Cover-preserving operations
    - "Negami's conjecture"
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a planar cover of a finite graph G is a finite covering graph of G that is itself a planar graph. Every graph that can be embedded into the projective plane has a planar cover; an unsolved conjecture of Seiya Negami states that these are the only graphs with planar covers.[1]
