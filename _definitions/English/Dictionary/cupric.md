---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cupric
offline_file: ""
offline_thumbnail: ""
uuid: e3ede5c4-808c-4b20-a82f-7fd2fef5bc0b
updated: 1484310391
title: cupric
categories:
    - Dictionary
---
cupric
     adj : of or containing divalent copper [syn: {cuprous}]
