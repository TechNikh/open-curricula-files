---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484357281
title: boa
categories:
    - Dictionary
---
boa
     n 1: a long thin fluffy scarf of feathers or fur [syn: {feather
          boa}]
     2: any of several chiefly tropical constrictors with vestigial
        hind limbs
