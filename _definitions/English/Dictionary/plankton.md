---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plankton
offline_file: ""
offline_thumbnail: ""
uuid: 2c70cce6-358c-4843-a0f5-02740a621baa
updated: 1484310264
title: plankton
categories:
    - Dictionary
---
plankton
     n : the aggregate of small plant and animal organisms that float
         or drift in great numbers in fresh or salt water
