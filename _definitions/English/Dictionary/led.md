---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/led
offline_file: ""
offline_thumbnail: ""
uuid: 3666ed90-dad4-45cb-8e42-adbd42465999
updated: 1484310315
title: led
categories:
    - Dictionary
---
lead
     n 1: a soft heavy toxic malleable metallic element; bluish white
          when freshly cut but tarnishes readily to dull gray;
          "the children were playing with lead soldiers" [syn: {Pb},
           {atomic number 82}]
     2: an advantage held by a competitor in a race; "he took the
        lead at the last turn"
     3: evidence pointing to a possible solution; "the police are
        following a promising lead"; "the trail led straight to
        the perpetrator" [syn: {track}, {trail}]
     4: a position of leadership (especially in the phrase `take the
        lead'); "he takes the lead in any group"; "we were just
        waiting for someone to take the lead"; "they ...
