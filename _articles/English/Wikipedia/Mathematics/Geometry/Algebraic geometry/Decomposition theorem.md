---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decomposition_theorem
offline_file: ""
offline_thumbnail: ""
uuid: edadccfb-92ed-4cd6-a272-80d5eb95b8de
updated: 1484309138
title: Decomposition theorem
tags:
    - Statement
    - Decomposition for smooth proper maps
    - Decomposition for projective maps
    - Proofs
categories:
    - Algebraic geometry
---
