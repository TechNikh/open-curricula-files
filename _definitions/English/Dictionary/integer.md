---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integer
offline_file: ""
offline_thumbnail: ""
uuid: 630110dc-ee89-48b6-8e6b-8946dfbe625f
updated: 1484310391
title: integer
categories:
    - Dictionary
---
integer
     n : any of the natural numbers (positive or negative) or zero
         [syn: {whole number}]
