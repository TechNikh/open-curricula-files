---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/King_and_pawn_versus_king_endgame
offline_file: ""
offline_thumbnail: ""
uuid: 8dee4085-5084-4996-996e-cb31f8359cbe
updated: 1484309013
title: King and pawn versus king endgame
tags:
    - Rule of the square
    - Key squares
    - Rook pawn
    - Examples from games
    - Pawns other than rook pawns
    - Knight pawn exception
    - Any key square by any route
    - Opposition
    - Rules
    - Case 1, conditions (b) and (c) are met
    - Case 2, conditions (a) and (c) are met
    - Case 3, conditions (a) and (b) are met
    - Example from Maróczy versus Marshall
    - Case 4, all three conditions are met
    - 'Exception - rook pawn'
    - Defending drawn positions
    - The king is in front of the pawn
    - The king has the opposition
    - The king is on the sixth rank
    - Example from Gligorić versus Fischer
    - Guidelines
    - Examples
    - Kamsky vs. Kramnik
    - Notes
categories:
    - Chess theory
---
The chess endgame with a king and a pawn versus a king is one of the most important and fundamental endgames, other than the basic checkmates (Lasker 1915). It is important to master this endgame, since most other endgames have the potential of reducing to this type of endgame via exchanges of pieces. It is important to be able to tell quickly whether a given position is a win or a draw, and to know the technique for playing it. The crux of this endgame is whether or not the pawn can be promoted (or queened), so checkmate can be forced.
