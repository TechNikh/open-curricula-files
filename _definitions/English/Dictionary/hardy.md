---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardy
offline_file: ""
offline_thumbnail: ""
uuid: 43640f21-5645-4112-b192-04de753c09be
updated: 1484310549
title: hardy
categories:
    - Dictionary
---
hardy
     adj 1: having rugged physical strength; inured to fatigue or
            hardships; "hardy explorers of northern Canada";
            "proud of her tall stalwart son"; "stout seamen";
            "sturdy young athletes" [syn: {stalwart}, {stout}, {sturdy}]
     2: resolute and without fear [syn: {doughty}, {fearless}]
     3: able to survive under unfavorable conditions; "strawberries
        are hardy and easy to grow"; "camels are tough and hardy
        creatures"
     n 1: United States slapstick comedian who played the pompous and
          overbearing member of the Laurel and Hardy duo who made
          many films (1892-1957) [syn: {Oliver Hardy}]
     2: English novelist ...
