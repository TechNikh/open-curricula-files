---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pressurise
offline_file: ""
offline_thumbnail: ""
uuid: 05c8dc66-f3f5-4f72-97c6-45ccf3f596bb
updated: 1484310603
title: pressurise
categories:
    - Dictionary
---
pressurise
     v 1: increase the pressure on a gas or liquid [syn: {supercharge},
           {pressurize}]
     2: maintain a certain pressure; "the airplane cabin is
        pressurized"; "pressurize a space suit" [syn: {pressurize}]
     3: increase the pressure in or of; "The captain will pressurize
        the cabin for the passengers' comfort" [syn: {pressurize}]
        [ant: {depressurize}, {depressurize}]
