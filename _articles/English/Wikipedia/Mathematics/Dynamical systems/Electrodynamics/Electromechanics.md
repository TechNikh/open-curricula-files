---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromechanics
offline_file: ""
offline_thumbnail: ""
uuid: 8aade39a-4c40-48bb-b8b5-081bf6aeffcc
updated: 1484308753
title: Electromechanics
tags:
    - History of electromechanics
    - Modern practice
    - References and notes
categories:
    - Electrodynamics
---
In engineering, electromechanics[1][2] combines electrical and mechanical processes and procedures drawn from electrical engineering and mechanical engineering. Electrical engineering in this context also encompasses electronics engineering.
