---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shifting
offline_file: ""
offline_thumbnail: ""
uuid: 32186181-0366-4508-9c4c-5f22ab3f80f6
updated: 1484310363
title: shifting
categories:
    - Dictionary
---
shifting
     adj 1: continuously moving or changing from position or direction;
            "he drifted into the shifting crowd"; "their nervous
            shifting glances"
     2: continuously varying; "taffeta with shifting colors"
     3: (of soil) unstable; "shifting sands"; "unfirm earth" [syn: {unfirm}]
     n : the act of moving from one place to another; "his constant
         shifting disrupted the class" [syn: {shift}]
