---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repay
offline_file: ""
offline_thumbnail: ""
uuid: bd00b156-df43-4231-9e47-ef00d70a07c6
updated: 1484310518
title: repay
categories:
    - Dictionary
---
repay
     v 1: pay back; "Please refund me my money" [syn: {refund}, {return},
           {give back}]
     2: make repayment for or return something [syn: {requite}]
     3: act or give recompensation in recognition of someone's
        behavior or actions [syn: {reward}, {pay back}]
     4: answer back [syn: {retort}, {come back}, {return}, {riposte},
         {rejoin}]
     [also: {repaid}]
