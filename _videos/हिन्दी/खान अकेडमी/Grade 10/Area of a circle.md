---
version: 1
type: video
provider: YouTube
id: DFmOH98nZIg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 245740c5-3b5d-45a6-a922-a9edd46333f4
updated: 1486069617
title: Area of a circle
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/DFmOH98nZIg/default.jpg
    - https://i3.ytimg.com/vi/DFmOH98nZIg/1.jpg
    - https://i3.ytimg.com/vi/DFmOH98nZIg/2.jpg
    - https://i3.ytimg.com/vi/DFmOH98nZIg/3.jpg
---
