---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/city
offline_file: ""
offline_thumbnail: ""
uuid: 2dcba090-9c12-419d-aa4b-4b316b369ff3
updated: 1484310234
title: city
categories:
    - Dictionary
---
city
     n 1: a large and densely populated urban area; may include
          several independent administrative districts; "Ancient
          Troy was a great city" [syn: {metropolis}, {urban center}]
     2: an incorporated administrative district established by state
        charter; "the city raised the tax rate"
     3: people living in a large densely populated municipality;
        "the city voted for Republicans in 1994" [syn: {metropolis}]
