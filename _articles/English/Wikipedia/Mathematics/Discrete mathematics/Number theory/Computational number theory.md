---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_number_theory
offline_file: ""
offline_thumbnail: ""
uuid: d612cec3-b4f2-41a9-824c-48c2547ef600
updated: 1484308639
title: Computational number theory
categories:
    - Number theory
---
In mathematics and computer science, computational number theory, also known as algorithmic number theory, is the study of algorithms for performing number theoretic computations.
