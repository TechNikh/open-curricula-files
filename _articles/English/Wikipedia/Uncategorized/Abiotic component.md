---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abiotic
offline_file: ""
offline_thumbnail: ""
uuid: 650e58ac-8fd3-420d-9cfc-e29979fbaf85
updated: 1484308342
title: Abiotic component
categories:
    - Uncategorized
---
In biology and ecology, abiotic components or abiotic factors are non-living chemical and physical parts of the environment that affect living organisms and the functioning of ecosystems. Abiotic factors and phenomena associated with them underpin all biology.
