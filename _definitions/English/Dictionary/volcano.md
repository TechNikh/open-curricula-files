---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/volcano
offline_file: ""
offline_thumbnail: ""
uuid: 265dcba0-3bfb-4179-81d1-c3470a75a18d
updated: 1484310162
title: volcano
categories:
    - Dictionary
---
volcano
     n 1: a fissure in the earth's crust (or in the surface of some
          other planet) through which molten lava and gases erupt
          [syn: {vent}]
     2: a mountain formed by volcanic material
     [also: {volcanoes} (pl)]
