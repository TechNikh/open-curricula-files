---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unique
offline_file: ""
offline_thumbnail: ""
uuid: 8fdbab44-1223-43c4-a381-3fc7b9062098
updated: 1483328796
title: unique
categories:
    - Dictionary
---
unique
     adj 1: radically distinctive and without equal; "he is alone in the
            field of microbiology"; "this theory is altogether
            alone in its penetration of the problem"; "Bach was
            unique in his handling of counterpoint"; "craftsmen
            whose skill is unequaled"; "unparalleled athletic
            ability"; "a breakdown of law unparalleled in our
            history" [syn: {alone(p)}, {unequaled}, {unequalled},
            {unparalleled}]
     2: (followed by `to') applying exclusively to a given category
        or condition or locality; "a species unique to Australia"
        [syn: {unique(p)}]
     3: the single one of its kind; "a singular ...
