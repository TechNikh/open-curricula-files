---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eminently
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636521
title: eminently
categories:
    - Dictionary
---
eminently
     adv : in an eminent manner; "two subjects on which he was
           eminently qualified to make an original contribution"
