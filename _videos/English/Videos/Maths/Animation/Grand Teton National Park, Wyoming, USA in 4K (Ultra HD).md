---
version: 1
type: video
provider: YouTube
id: NCTd1IHChug
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 918e3c45-0c46-47cb-a46f-83adc8448b7b
updated: 1486069641
title: Grand Teton National Park, Wyoming, USA in 4K (Ultra HD)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/NCTd1IHChug/default.jpg
    - https://i3.ytimg.com/vi/NCTd1IHChug/1.jpg
    - https://i3.ytimg.com/vi/NCTd1IHChug/2.jpg
    - https://i3.ytimg.com/vi/NCTd1IHChug/3.jpg
---
