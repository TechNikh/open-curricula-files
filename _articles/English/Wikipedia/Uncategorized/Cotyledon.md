---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cotyledon
offline_file: ""
offline_thumbnail: ""
uuid: 7f6de96c-0385-495a-b7fd-d43d87dc8676
updated: 1484308347
title: Cotyledon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cotyledon-Cercis_siliquastrum.jpg
tags:
    - Epigeal versus hypogeal development
    - History
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
A cotyledon (/kɒtᵻˈliːdən/; "seed leaf" from Latin cotyledon,[1] from Greek: κοτυληδών kotylēdōn, gen.: κοτυληδόνος kotylēdonos, from κοτύλη kotýlē "cup, bowl") is a significant part of the embryo within the seed of a plant, and is defined by the Oxford English Dictionary as "The primary leaf in the embryo of the higher plants (Phanerogams); the seed-leaf."[2] Upon germination, the cotyledon may become the embryonic first leaves of a seedling. The number of cotyledons present is one characteristic used by botanists to classify the flowering plants (angiosperms). Species with one cotyledon are called monocotyledonous ("monocots"). Plants with two ...
