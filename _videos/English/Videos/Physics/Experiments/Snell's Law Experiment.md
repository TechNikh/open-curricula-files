---
version: 1
type: video
provider: YouTube
id: -3x-wn82q5M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e6343417-6c1e-456c-9f2a-1e28142e3fdd
updated: 1486069639
title: "Snell's Law Experiment"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/-3x-wn82q5M/default.jpg
    - https://i3.ytimg.com/vi/-3x-wn82q5M/1.jpg
    - https://i3.ytimg.com/vi/-3x-wn82q5M/2.jpg
    - https://i3.ytimg.com/vi/-3x-wn82q5M/3.jpg
---
