---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T
offline_file: ""
offline_thumbnail: ""
uuid: 3923425f-fc90-454d-8e70-1846ab3c3320
updated: 1484308321
title: T
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-T_cursiva.gif
tags:
    - History
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Ancestors and siblings in other alphabets
    - Derived signs, symbols and abbreviations
    - Computing codes
    - Other representations
categories:
    - Uncategorized
---
T (named tee /ˈtiː/[1]) is the 20th letter in the modern English alphabet and the ISO basic Latin alphabet. It is the most commonly used consonant and the second most common letter in English language texts.[2]
