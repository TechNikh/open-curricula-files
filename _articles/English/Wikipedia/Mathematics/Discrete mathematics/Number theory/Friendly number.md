---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Friendly_number
offline_file: ""
offline_thumbnail: ""
uuid: 7e6a131c-c0fe-4798-b2fd-c40d54988970
updated: 1484308644
title: Friendly number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Abundant_number_Cuisenaire_rods_12.png
tags:
    - Examples
    - Solitary numbers
    - Large clubs
    - Notes
categories:
    - Number theory
---
In number theory, friendly numbers are two or more natural numbers with a common abundancy, the ratio between the sum of divisors of a number and the number itself. Two numbers with the same abundancy form a friendly pair; n numbers with the same abundancy form a friendly n-tuple.
