---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimum_k-cut
offline_file: ""
offline_thumbnail: ""
uuid: e1a268d2-e13e-4b32-a2d7-7ed70231cfbf
updated: 1484309394
title: Minimum k-cut
tags:
    - Formal definition
    - Approximations
    - Notes
categories:
    - Computational problems in graph theory
---
In mathematics, the minimum k-cut, is a combinatorial optimization problem that requires finding a set of edges whose removal would partition the graph to k connected components. These edges are referred to as k-cut. The goal is to find the minimum-weight k-cut. This partitioning can have applications in VLSI design, data-mining, finite elements and communication in parallel computing.
