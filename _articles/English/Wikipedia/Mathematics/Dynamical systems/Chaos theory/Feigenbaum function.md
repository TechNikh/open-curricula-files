---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Feigenbaum_function
offline_file: ""
offline_thumbnail: ""
uuid: 7d389aa7-c0ff-490a-a163-60a0484525f9
updated: 1484308707
title: Feigenbaum function
tags:
    - Functional equation
    - Scaling function
categories:
    - Chaos theory
---
In the study of dynamical systems the term Feigenbaum function has been used to describe two different functions introduced by the physicist Mitchell Feigenbaum:
