---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impact
offline_file: ""
offline_thumbnail: ""
uuid: 84bdef51-0dbb-4bfb-8b99-8d34f4920987
updated: 1484310295
title: impact
categories:
    - Dictionary
---
impact
     n 1: the striking of one body against another
     2: a forceful consequence; a strong effect; "the book had an
        important impact on my thinking"; "the book packs a
        wallop" [syn: {wallop}]
     3: influencing strongly; "they resented the impingement of
        American values on European culture" [syn: {impingement},
        {encroachment}]
     4: the violent interaction of individuals or groups entering
        into combat; "the armies met in the shock of battle" [syn:
         {shock}]
     v 1: press or wedge together; pack together
     2: have an effect upon; "Will the new rules affect me?" [syn: {affect},
         {bear upon}, {bear on}, {touch on}, ...
