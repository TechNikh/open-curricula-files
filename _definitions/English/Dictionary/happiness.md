---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/happiness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464742
title: happiness
categories:
    - Dictionary
---
happiness
     n 1: state of well-being characterized by emotions ranging from
          contentment to intense joy [syn: {felicity}] [ant: {unhappiness}]
     2: emotions experienced when in a state of well-being [ant: {sadness}]
