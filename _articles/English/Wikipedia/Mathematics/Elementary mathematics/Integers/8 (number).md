---
version: 1
type: article
id: https://en.wikipedia.org/wiki/8_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 98503f7d-5d01-4137-916e-932a89847552
updated: 1484308941
title: 8 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Evo8glyph.svg.png
tags:
    - In mathematics
    - In numeral systems
    - List of basic calculations
    - Etymology
    - Glyph
    - In science
    - Physics
    - Astronomy
    - chemistry
    - Geology
    - Biology
    - In technology
    - In measurement
    - In culture
    - Architecture
    - In religion, folk belief and divination
    - As a lucky number
    - In astrology
    - In music and dance
    - In film and television
    - In sports and other games
    - In foods
    - In literature
    - In slang
categories:
    - Integers
---
