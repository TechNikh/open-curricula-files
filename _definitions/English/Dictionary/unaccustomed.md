---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unaccustomed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484533861
title: unaccustomed
categories:
    - Dictionary
---
unaccustomed
     adj 1: not habituated to; unfamiliar with; "unaccustomed to wearing
            suits" [ant: {accustomed}]
     2: not customary or usual; "an unaccustomed pleasure"; "many
        varieties of unaccustomed foods"; "a new budget of
        unaccustomed austerity"
