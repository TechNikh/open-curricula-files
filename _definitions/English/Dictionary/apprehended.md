---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apprehended
offline_file: ""
offline_thumbnail: ""
uuid: c07d3c8e-1670-4ef8-836d-fcc30e5444da
updated: 1484310591
title: apprehended
categories:
    - Dictionary
---
apprehended
     adj : fully understood or grasped; "dangers not yet appreciated";
           "these apprehended truths"; "a thing comprehended is a
           thing known as fully as it can be known" [syn: {appreciated},
            {comprehended}]
