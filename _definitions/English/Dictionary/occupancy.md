---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occupancy
offline_file: ""
offline_thumbnail: ""
uuid: a1f44d09-bfb6-4ae5-80f4-2fcb8dad9dbe
updated: 1484310393
title: occupancy
categories:
    - Dictionary
---
occupancy
     n 1: an act of being a tenant or occupant [syn: {tenancy}]
     2: the act of occupying or taking possession of a building;
        "occupation of a building without a certificate of
        occupancy is illegal" [syn: {occupation}, {taking
        possession}, {moving in}]
