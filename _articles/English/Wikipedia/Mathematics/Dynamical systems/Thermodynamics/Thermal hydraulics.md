---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_hydraulics
offline_file: ""
offline_thumbnail: ""
uuid: c5c0d1a9-3a8d-4cd2-bb3c-28f532450756
updated: 1484308868
title: Thermal hydraulics
categories:
    - Thermodynamics
---
Thermal hydraulics (also called thermohydraulics) is the study of hydraulic flow in thermal fluids. A common example is steam generation in power plants and the associated energy transfer to mechanical motion and the change of states of the water while undergoing this process.
