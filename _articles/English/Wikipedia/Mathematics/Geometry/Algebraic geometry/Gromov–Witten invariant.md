---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gromov%E2%80%93Witten_invariant'
offline_file: ""
offline_thumbnail: ""
uuid: 6d55a49f-059c-4067-95bc-f824b10d049c
updated: 1484309149
title: Gromov–Witten invariant
tags:
    - Definition
    - Computational techniques
    - Related invariants and other constructions
    - Application in physics
categories:
    - Algebraic geometry
---
In mathematics, specifically in symplectic topology and algebraic geometry, Gromov–Witten (GW) invariants are rational numbers that, in certain situations, count pseudoholomorphic curves meeting prescribed conditions in a given symplectic manifold. The GW invariants may be packaged as a homology or cohomology class in an appropriate space, or as the deformed cup product of quantum cohomology. These invariants have been used to distinguish symplectic manifolds that were previously indistinguishable. They also play a crucial role in closed type IIA string theory. They are named after Mikhail Gromov and Edward Witten.
