---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-organization
offline_file: ""
offline_thumbnail: ""
uuid: 4d353225-e409-46d0-a2dc-69ccc302451f
updated: 1484308819
title: Self-organization
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Nb3O7%2528OH%2529_self-organization2.jpg'
tags:
    - Overview
    - Principles of self-organization
    - History of the idea
    - Developing views
    - Examples
    - Physics
    - chemistry
    - Biology
    - Computer science
    - Algorithms
    - Networks
    - Cybernetics
    - Human society
    - Economics
    - Collective intelligence
    - Psychology and education
    - Self-organised learning
    - Traffic flow
    - Methodology
    - In the emergence of language
    - In language acquisition
    - In articulatory phonology
    - In diachrony and synchrony
    - Criticism
    - Dissertations and theses on self-organization
categories:
    - Self-organization
---
Self-organization is a process where some form of overall order or coordination arises out of the local interactions between smaller component parts of an initially disordered system. The process of self-organization can be spontaneous, and it is not necessarily controlled by any auxiliary agent outside of the system. It is often triggered by random fluctuations that are amplified by positive feedback. The resulting organization is wholly decentralized or distributed over all the components of the system. As such, the organization is typically robust and able to survive and, even, self-repair substantial damage or perturbations. Chaos theory discusses self-organization in terms of islands ...
