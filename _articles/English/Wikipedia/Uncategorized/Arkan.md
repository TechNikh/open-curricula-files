---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arkan
offline_file: ""
offline_thumbnail: ""
uuid: 42039106-a528-4edf-8d64-8d49d34bd33d
updated: 1484308425
title: Arkan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Arkanov_grob.jpg
tags:
    - Early life
    - Criminal career
    - Western Europe
    - Return to Yugoslavia
    - Role in the Yugoslav Wars
    - Early
    - War
    - Kosovo War and NATO bombing
    - ICTY indictment
    - Assassination
    - Trials
    - Theories
    - Personal
    - In popular culture
    - Biographies
    - Interviews
categories:
    - Uncategorized
---
Željko Ražnatović (Serbian Cyrillic: Жељко Ражнатовић, pronounced [ʐêːʎko raʐnâːtoʋit͡ɕ]; 17 April 1952 – 15 January 2000), better known as Arkan (Аркан), was a Serbian career criminal and commander of a paramilitary force in the Yugoslav Wars, called the Serb Volunteer Guard. He was on Interpol's most wanted list in the 1970s and 1980s for robberies and murders committed in a number of countries across Europe, and was later indicted by the UN for crimes against humanity for his role during the wars. Ražnatović was up until his death the most powerful crime boss in the Balkans. He was assassinated in 2000, before his trial could take place.
