---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brahmin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565301
title: brahmin
categories:
    - Dictionary
---
brahmin
     n 1: a member of a social and cultural elite (especially a
          descendant of an old New England family); "a Boston
          Brahman" [syn: {brahman}]
     2: a member of the highest of the four Hindu varnas;
        "originally all brahmans were priests" [syn: {brahman}]
     3: the highest of the four varnas: the priestly or sacerdotal
        category [syn: {brahman}]
     4: any of several breeds of Indian cattle; especially a large
        American heat and tick resistant grayish humped breed
        evolved in the Gulf States by interbreeding Indian cattle
        and now used chiefly for crossbreeding [syn: {Brahman}, {Brahma},
         {Bos indicus}]
