---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tortoise
offline_file: ""
offline_thumbnail: ""
uuid: 53fa3aab-4ca0-4399-b2c4-5c4d52693900
updated: 1484310286
title: tortoise
categories:
    - Dictionary
---
tortoise
     n : usually herbivorous land turtles having clawed elephant-like
         limbs; worldwide in arid area except Australia and
         Antarctica
