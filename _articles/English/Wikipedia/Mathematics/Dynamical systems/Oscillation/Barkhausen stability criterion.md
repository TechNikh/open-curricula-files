---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barkhausen_stability_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 414cf6d3-fab4-41a5-990b-7f6a9b727870
updated: 1484308813
title: Barkhausen stability criterion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Oscillator_diagram1.svg.png
tags:
    - Limitations
    - Criterion
    - Erroneous version
categories:
    - Oscillation
---
In electronics, the Barkhausen stability criterion is a mathematical condition to determine when a linear electronic circuit will oscillate.[1][2][3] It was put forth in 1921 by German physicist Heinrich Georg Barkhausen (1881–1956).[4] It is widely used in the design of electronic oscillators, and also in the design of general negative feedback circuits such as op amps, to prevent them from oscillating.
