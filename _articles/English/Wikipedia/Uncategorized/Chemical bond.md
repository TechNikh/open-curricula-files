---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemical_bonds
offline_file: ""
offline_thumbnail: ""
uuid: 20ffb39b-0737-4ecf-803b-d3460b95e749
updated: 1484308375
title: Chemical bond
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Electron_dot.svg.png
tags:
    - Overview of main types of chemical bonds
    - History
    - Bonds in chemical formulas
    - Strong chemical bonds
    - Ionic bonding
    - Covalent bond
    - Single and multiple bonds
    - Coordinate covalent bond (Dipolar bond)
    - Metallic bonding
    - Intermolecular bonding
    - Theories of chemical bonding
categories:
    - Uncategorized
---
A chemical bond is a lasting attraction between atoms that enables the formation of chemical compounds. The bond may result from the electrostatic force of attraction between atoms with opposite charges, or through the sharing of electrons as in the covalent bonds. The strength of chemical bonds varies considerably; there are "strong bonds" such as covalent or ionic bonds and "weak bonds" such as Dipole-dipole interaction, the London dispersion force and hydrogen bonding.
