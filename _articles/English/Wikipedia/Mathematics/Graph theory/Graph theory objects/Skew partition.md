---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skew_partition
offline_file: ""
offline_thumbnail: ""
uuid: c15d8dd7-9303-41e7-9432-4d0b0cbeb631
updated: 1484309445
title: Skew partition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Skew_partition.svg.png
tags:
    - Definition
    - Examples
    - Special cases
    - History
    - In structural graph theory
    - Algorithms and Complexity
    - Notes
categories:
    - Graph theory objects
---
In graph theory, a skew partition of a graph is a partition of its vertices into two subsets, such that the induced subgraph formed by one of the two subsets is disconnected and the induced subgraph formed by the other subset is the complement of a disconnected graph. Skew partitions play an important role in the theory of perfect graphs.
