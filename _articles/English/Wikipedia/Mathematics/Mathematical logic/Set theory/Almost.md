---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Almost
offline_file: ""
offline_thumbnail: ""
uuid: 5061574b-765b-4e8e-b353-8ac5679e9e0f
updated: 1484309501
title: Almost
categories:
    - Set theory
---
In other words, an infinite set S that is a subset of another infinite set L, is almost L if the subtracted set L\S is of finite size.
