---
version: 1
type: video
provider: YouTube
id: 9p3t9NOfCtA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/The%20Right%20Hand%20Rule%20and%20the%20Magnetic%20Field%20Straight%20Wire.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 03f1bbf3-eef8-46e4-8c21-d11187ca1d43
updated: 1486069649
title: The Right Hand Rule and the Magnetic Field Straight Wire
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/9p3t9NOfCtA/default.jpg
    - https://i3.ytimg.com/vi/9p3t9NOfCtA/1.jpg
    - https://i3.ytimg.com/vi/9p3t9NOfCtA/2.jpg
    - https://i3.ytimg.com/vi/9p3t9NOfCtA/3.jpg
---
