---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galena
offline_file: ""
offline_thumbnail: ""
uuid: 3f55360c-17d7-4d8a-b37b-7c4a448ca2e1
updated: 1484310409
title: galena
categories:
    - Dictionary
---
galena
     n : soft blue-gray mineral; lead sulfide; a major source of lead
