---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anisotropy_energy
offline_file: ""
offline_thumbnail: ""
uuid: 70e53c57-f1ac-4732-adab-44cedc20e5af
updated: 1484309292
title: Anisotropy energy
categories:
    - Orientation (geometry)
---
Anisotropic energy is energy that is directionally specific. The word anisotropy means "directionally dependent", hence the definition. The most common form of anisotropic energy is magnetocrystalline anisotropy, which is commonly studied in ferrimagnets. In ferrimagnets, there are islands or domains of atoms that are all coordinated in a certain direction; this spontaneous positioning is often called the "easy" direction, indicating that this is the lowest energy state for these atoms. In order to study magnetocrystalline anisotropy, energy (usually in the form of an electric current) is applied to the domain, which causes the crystals to deflect from the "easy" to "hard" positions. The ...
