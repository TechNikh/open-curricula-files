---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/questioning
offline_file: ""
offline_thumbnail: ""
uuid: c055f585-41d0-41a1-96bd-c602d6379702
updated: 1484310579
title: questioning
categories:
    - Dictionary
---
questioning
     adj 1: perplexed (as if being expected to know something that you
            do not know); "he had a quizzical expression" [syn: {quizzical}]
     2: marked by or given to doubt; "a skeptical attitude"; "a
        skeptical listener" [syn: {doubting}, {skeptical}, {sceptical}]
     3: showing curiosity; "if someone saw a man climbing a light
        post they might get inquisitive"; "raised a speculative
        eyebrow" [syn: {inquisitive}, {speculative}, {wondering(a)}]
     n : a request for information [syn: {inquiring}]
