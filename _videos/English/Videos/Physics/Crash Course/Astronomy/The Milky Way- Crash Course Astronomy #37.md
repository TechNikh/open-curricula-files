---
version: 1
type: video
provider: YouTube
id: tj_QPnO8vpQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/The%20Milky%20Way-%20Crash%20Course%20Astronomy%20%2337.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f670a3f1-f7ca-482b-ad77-797c4f41990f
updated: 1486069684
title: 'The Milky Way- Crash Course Astronomy #37'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/tj_QPnO8vpQ/default.jpg
    - https://i3.ytimg.com/vi/tj_QPnO8vpQ/1.jpg
    - https://i3.ytimg.com/vi/tj_QPnO8vpQ/2.jpg
    - https://i3.ytimg.com/vi/tj_QPnO8vpQ/3.jpg
---
