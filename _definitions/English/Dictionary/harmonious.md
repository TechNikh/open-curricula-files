---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harmonious
offline_file: ""
offline_thumbnail: ""
uuid: 7ccde66d-47ba-4265-8949-f65da80c225f
updated: 1484310549
title: harmonious
categories:
    - Dictionary
---
harmonious
     adj 1: musically pleasing [ant: {inharmonious}]
     2: exhibiting equivalence or correspondence among constituents
        of an entity or between different entities [syn: {proportionate},
         {symmetrical}]
     3: suitable and fitting; "the tailored clothes were harmonious
        with her military bearing" [syn: {appropriate}]
     4: existing together in harmony; "harmonious family
        relationships"
