---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trial
offline_file: ""
offline_thumbnail: ""
uuid: 08e8d6c1-099b-40ab-a95d-fafdb88c65f7
updated: 1484310373
title: trial
categories:
    - Dictionary
---
trial
     n 1: (law) legal proceedings consisting of the judicial
          examination of issues by a competent tribunal; "most of
          these complaints are settled before they go to trial"
     2: the act of testing something; "in the experimental trials
        the amount of carbon was measured separately"; "he called
        each flip of the coin a new trial" [syn: {test}, {run}]
     3: (sports) a preliminary competition to determine
        qualifications; "the trials for the semifinals began
        yesterday"
     4: (law) the determination of a person's innocence or guilt by
        due process of law; "he had a fair trial and the jury
        found him guilty"
     5: trying ...
