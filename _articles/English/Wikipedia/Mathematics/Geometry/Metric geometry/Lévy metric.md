---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A9vy_metric'
offline_file: ""
offline_thumbnail: ""
uuid: 756c4c63-a5f4-4cd4-b0c6-b5b0afa16e6b
updated: 1484309270
title: Lévy metric
categories:
    - Metric geometry
---
In mathematics, the Lévy metric is a metric on the space of cumulative distribution functions of one-dimensional random variables. It is a special case of the Lévy–Prokhorov metric, and is named after the French mathematician Paul Lévy.
