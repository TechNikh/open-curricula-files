---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anton_Kotzig
offline_file: ""
offline_thumbnail: ""
uuid: 5391db38-7556-49cb-8ac7-6cf29e478413
updated: 1484309472
title: Anton Kotzig
categories:
    - Graph theorists
---
The Ringel-Kotzig conjecture on graceful labeling of trees is named after him and Gerhard Ringel.
