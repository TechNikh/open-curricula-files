---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Annulus_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 55432636-c367-4a75-ab3f-09b6a4b05a23
updated: 1484308925
title: Annulus (mathematics)
categories:
    - Elementary geometry
---
In mathematics, an annulus (the Latin word for "little ring" is anulus, with plural anuli) is a ring-shaped object — a region bounded by two concentric circles. The adjectival form is annular (as in annular eclipse).
