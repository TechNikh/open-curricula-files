---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mock
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566381
title: mock
categories:
    - Dictionary
---
mock
     adj : constituting a copy or imitation of something; "boys in mock
           battle"
     n : the act of mocking or ridiculing; "they made a mock of him"
     v 1: treat with contempt; "The new constitution mocks all
          democratic principles" [syn: {bemock}]
     2: imitate with mockery and derision; "The children mocked
        their handicapped classmate"
