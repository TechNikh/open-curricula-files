---
version: 1
type: video
provider: YouTube
id: d4bqNf37mBY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Partial%20pressure%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a31323fd-453b-43f1-9873-25433b484fe5
updated: 1486069585
title: Partial pressure example
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/d4bqNf37mBY/default.jpg
    - https://i3.ytimg.com/vi/d4bqNf37mBY/1.jpg
    - https://i3.ytimg.com/vi/d4bqNf37mBY/2.jpg
    - https://i3.ytimg.com/vi/d4bqNf37mBY/3.jpg
---
Figuring out the partial pressures of various gases in a container given the mass percent composition. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/vapor-pressure-example?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/introduction-to-partial-pressure?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
