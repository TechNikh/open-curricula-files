---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moufang_polygon
offline_file: ""
offline_thumbnail: ""
uuid: 2b7bfd69-b8e2-4868-adbc-a9d6f703642d
updated: 1484309253
title: Moufang polygon
tags:
    - Definitions
    - Moufang 3-gons
    - Moufang 4-gons
    - Moufang 6-gons
    - Moufang 8-gons
    - Quadrangular algebras
    - Notes and references
categories:
    - Incidence geometry
---
In mathematics, Moufang polygons are a generalization by Jacques Tits of the Moufang planes studied by Ruth Moufang, and are irreducible buildings of rank two that admit the action of root groups. In a book on the topic, Tits and Weiss[1] classify them all. An earlier theorem, proved independently by Tits and Weiss,[2][3] showed that a Moufang polygon must be a generalized 3-gon, 4-gon, 6-gon, or 8-gon, so the purpose of the aforementioned book was to analyze these four cases.
