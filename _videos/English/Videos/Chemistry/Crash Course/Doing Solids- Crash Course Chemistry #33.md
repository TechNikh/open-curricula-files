---
version: 1
type: video
provider: YouTube
id: bzr-byiSXlA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2876888d-743b-4a4d-be08-948cbda45215
updated: 1486069680
title: 'Doing Solids- Crash Course Chemistry #33'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/bzr-byiSXlA/default.jpg
    - https://i3.ytimg.com/vi/bzr-byiSXlA/1.jpg
    - https://i3.ytimg.com/vi/bzr-byiSXlA/2.jpg
    - https://i3.ytimg.com/vi/bzr-byiSXlA/3.jpg
---
