---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raptor_code
offline_file: ""
offline_thumbnail: ""
uuid: 5da19dc0-1c62-456a-a642-ba8009f6037b
updated: 1484308562
title: Raptor code
tags:
    - Overview
    - Decoding
    - Computational complexity
    - Legal complexity
    - Notes
categories:
    - Coding theory
---
In computer science, raptor codes (rapid tornado;[1] see Tornado codes) are the first known class of fountain codes with linear time encoding and decoding. They were invented by Amin Shokrollahi in 2000/2001 and were first published in 2004 as an extended abstract. Raptor codes are a significant theoretical and practical improvement over LT codes, which were the first practical class of fountain codes.
