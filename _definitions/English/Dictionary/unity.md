---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unity
offline_file: ""
offline_thumbnail: ""
uuid: 1062ebbc-1c93-4885-9d22-a97b1c6aaf4b
updated: 1484310565
title: unity
categories:
    - Dictionary
---
unity
     n 1: an unreduced or unbroken completeness or totality [syn: {integrity},
           {wholeness}]
     2: the smallest whole number or a numeral representing this
        number; "he has the one but will need a two and three to
        go with it"; "they had lunch at one" [syn: {one}, {1}, {I},
         {ace}, {single}]
     3: the quality of being united into one [syn: {oneness}]
