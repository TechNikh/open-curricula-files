---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Least_common_multiple
offline_file: ""
offline_thumbnail: ""
uuid: 582e5458-4b79-4403-ae62-99e4031a4c1a
updated: 1484308918
title: Least common multiple
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Least_common_multiple_chart.png
tags:
    - Overview
    - Notation
    - Example
    - Applications
    - Computing the least common multiple
    - Reduction by the greatest common divisor
    - Finding least common multiples by prime factorization
    - A simple algorithm
    - A method using a table
    - Formulas
    - Fundamental theorem of arithmetic
    - Lattice-theoretic
    - Other
    - The LCM in commutative rings
    - Notes
categories:
    - Elementary arithmetic
---
In arithmetic and number theory, the least common multiple, lowest common multiple, or smallest common multiple of two integers a and b, usually denoted by LCM(a, b), is the smallest positive integer that is divisible by both a and b.[1] Since division of integers by zero is undefined, this definition has meaning only if a and b are both different from zero.[2] However, some authors define lcm(a,0) as 0 for all a, which is the result of taking the lcm to be the least upper bound in the lattice of divisibility.
