---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/governor
offline_file: ""
offline_thumbnail: ""
uuid: 6642a94d-97db-4cc2-ba52-09b057e755e1
updated: 1484310583
title: governor
categories:
    - Dictionary
---
governor
     n 1: the head of a state government
     2: a control that maintains a steady speed in a machine (as by
        controlling the supply of fuel) [syn: {regulator}]
