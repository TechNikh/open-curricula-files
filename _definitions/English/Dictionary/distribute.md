---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distribute
offline_file: ""
offline_thumbnail: ""
uuid: 694dad7d-ceed-43b6-ae26-bdedda5a1209
updated: 1484310569
title: distribute
categories:
    - Dictionary
---
distribute
     v 1: administer or bestow, as in small portions; "administer
          critical remarks to everyone present"; "dole out some
          money"; "shell out pocket money for the children"; "deal
          a blow to someone" [syn: {administer}, {mete out}, {deal},
           {parcel out}, {lot}, {dispense}, {shell out}, {deal out},
           {dish out}, {allot}, {dole out}]
     2: distribute or disperse widely; "The invaders spread their
        language all over the country" [syn: {spread}] [ant: {gather}]
     3: make available; "The publisher wants to distribute the book
        in Asia"
     4: give out; "The teacher handed out the exams" [syn: {give out},
         {hand ...
