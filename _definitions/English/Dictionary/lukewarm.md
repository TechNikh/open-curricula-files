---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lukewarm
offline_file: ""
offline_thumbnail: ""
uuid: 741ce126-acf5-4713-9d9f-f43c8e0500be
updated: 1484310232
title: lukewarm
categories:
    - Dictionary
---
lukewarm
     adj 1: moderately warm; "he hates lukewarm coffee"; "tepid bath
            water" [syn: {tepid}]
     2: feeling or showing little interest or enthusiasm; "a
        halfhearted effort"; "gave only lukewarm support to the
        candidate" [syn: {halfhearted}]
