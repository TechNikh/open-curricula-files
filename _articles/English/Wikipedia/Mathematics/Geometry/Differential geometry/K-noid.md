---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-noid
offline_file: ""
offline_thumbnail: ""
uuid: bc0eea81-434f-4ec0-a92a-97a63a6aa764
updated: 1484309226
title: K-noid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trinoid.png
categories:
    - Differential geometry
---
In differential geometry, a k-noid is a minimal surface with k catenoid openings. In particular, the 3-noid is often called trinoid. The first k-noid minimal surfaces were described by Jorge and Meeks in 1983.[1]
