---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_dimer_models
offline_file: ""
offline_thumbnail: ""
uuid: 56c05d73-75b2-46cf-aba2-26b35b373311
updated: 1484309423
title: Quantum dimer models
categories:
    - Matching
---
Quantum dimer models were introduced to model the physics of resonating valence bond (RVB) states in lattice spin systems. The only degrees of freedom retained from the motivating spin systems are the valence bonds, represented as dimers which live on the lattice bonds. In typical dimer models, the dimers do not overlap ("hardcore constraint").
