---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/village
offline_file: ""
offline_thumbnail: ""
uuid: de6b2346-31dd-489d-a106-191ff1d82b8c
updated: 1484310260
title: village
categories:
    - Dictionary
---
village
     n 1: a community of people smaller than a town [syn: {small town},
           {settlement}]
     2: a settlement smaller than a town [syn: {hamlet}]
     3: a mainly residential district of Manhattan; `the Village'
        became a home for many writers and artists in the 20th
        century [syn: {Greenwich Village}]
