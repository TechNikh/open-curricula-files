---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inability
offline_file: ""
offline_thumbnail: ""
uuid: 8281936b-94d7-489f-9828-ca5e6be62b73
updated: 1484310178
title: inability
categories:
    - Dictionary
---
inability
     n 1: lack of ability (especially mental ability) to do something
          [ant: {ability}]
     2: lacking the power to perform [syn: {unfitness}] [ant: {ability}]
