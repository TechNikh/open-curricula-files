---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teller
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483521
title: teller
categories:
    - Dictionary
---
Teller
     n 1: United States physicist (born in Hungary) who worked on the
          first atom bombs and the first hydrogen bomb (born in
          1908) [syn: {Edward Teller}]
     2: an official appointed to count the votes (especially in
        legislative assembly) [syn: {vote counter}]
     3: an employee of a bank who receives and pays out money [syn:
        {cashier}, {bank clerk}]
     4: someone who tells a story [syn: {narrator}, {storyteller}]
