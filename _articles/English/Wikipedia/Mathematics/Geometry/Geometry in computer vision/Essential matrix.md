---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Essential_matrix
offline_file: ""
offline_thumbnail: ""
uuid: a7680af8-8749-49ab-bfe4-d26199cac290
updated: 1484309181
title: Essential matrix
tags:
    - Function
    - use
    - Derivation and definition
    - Properties of the essential matrix
    - Estimation of the essential matrix
    - Determining R and t from E
    - Finding one solution
    - Showing that it is valid
    - Finding all solutions
    - 3D points from corresponding image points
categories:
    - Geometry in computer vision
---
In computer vision, the essential matrix is a 
  
    
      
        3
        ×
        3
      
    
    {\displaystyle 3\times 3}
  
 matrix, 
  
    
      
        
          E
        
      
    
    {\displaystyle \mathbf {E} }
  
, with some additional properties described below, which relates corresponding points in stereo images assuming that the cameras satisfy the pinhole camera model.
