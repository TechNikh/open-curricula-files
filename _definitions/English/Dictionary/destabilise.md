---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destabilise
offline_file: ""
offline_thumbnail: ""
uuid: aca4f589-00bb-4173-b6f6-8fbfff71cb23
updated: 1484310174
title: destabilise
categories:
    - Dictionary
---
destabilise
     v 1: become unstable; "The economy destabilized rapidly" [syn: {destabilize}]
          [ant: {stabilize}, {stabilize}]
     2: make unstable; "Terrorism destabilized the government" [syn:
         {destabilize}] [ant: {stabilize}, {stabilize}]
