---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Melting
offline_file: ""
offline_thumbnail: ""
uuid: 140c2c2d-4150-4e0d-8c54-2092066c4d84
updated: 1484308853
title: Melting
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Melting_icecubes.gif
tags:
    - Melting as a first-order phase transition
    - Melting criteria
    - Supercooling
    - Melting of amorphous solids (glasses)
    - Related concepts
categories:
    - Thermodynamics
---
Melting, or fusion, is a physical process that results in the phase transition of a substance from a solid to a liquid. This occurs when the internal energy of the solid increases, typically by the application of heat or pressure, which increases the substance's temperature to the melting point. At the melting point, the ordering of ions or molecules in the solid breaks down to a less ordered state, and the solid melts to become a liquid. An object that has melted completely is molten (although this word is typically used for substances that melt only at a high temperature, such as molten iron or molten lava).
