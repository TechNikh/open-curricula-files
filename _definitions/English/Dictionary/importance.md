---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/importance
offline_file: ""
offline_thumbnail: ""
uuid: 47d3641a-625e-48e0-ac7a-29e5d59faffe
updated: 1484310316
title: importance
categories:
    - Dictionary
---
importance
     n 1: the quality of being important and worthy of note; "the
          importance of a well-balanced diet" [ant: {unimportance}]
     2: a prominent status; "a person of importance" [syn: {grandness}]
