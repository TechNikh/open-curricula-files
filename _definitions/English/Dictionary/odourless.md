---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/odourless
offline_file: ""
offline_thumbnail: ""
uuid: cf561c36-e479-45d0-a411-7487dc0fa0a0
updated: 1484310371
title: odourless
categories:
    - Dictionary
---
odourless
     adj : having no odor; "odorless gas"; "odorless flowers" [syn: {odorless},
            {inodorous}] [ant: {odorous}]
