---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recurrence_plot
offline_file: ""
offline_thumbnail: ""
uuid: 75cd00e9-8484-4e75-befd-9bcf5e617ffb
updated: 1484308712
title: Recurrence plot
tags:
    - Background
    - Detailed description
    - Extensions
    - Example
categories:
    - Chaos theory
---
In descriptive statistics and chaos theory, a recurrence plot (RP) is a plot showing, for a given moment in time, the times at which a phase space trajectory visits roughly the same area in the phase space. In other words, it is a graph of
