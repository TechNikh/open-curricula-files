---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simple
offline_file: ""
offline_thumbnail: ""
uuid: 0defeaa9-3b77-464e-aa19-6776f21639a6
updated: 1484310341
title: simple
categories:
    - Dictionary
---
simple
     adj 1: having few parts; not complex or complicated or involved; "a
            simple problem"; "simple mechanisms"; "a simple
            design"; "a simple substance" [ant: {complex}]
     2: easy and not involved or complicated; "an elementary problem
        in statistics"; "elementary, my dear Watson"; "a simple
        game"; "found an uncomplicated solution to the problem"
        [syn: {elementary}, {uncomplicated}, {unproblematic}]
     3: apart from anything else; without additions or
        modifications; "only the bare facts"; "shocked by the mere
        idea"; "the simple passage of time was enough"; "the
        simple truth" [syn: {bare(a)}, {mere(a)}, ...
