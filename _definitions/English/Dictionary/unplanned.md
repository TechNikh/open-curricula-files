---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unplanned
offline_file: ""
offline_thumbnail: ""
uuid: 1342bfa8-b603-4ebc-b37b-d188b7499326
updated: 1484310477
title: unplanned
categories:
    - Dictionary
---
unplanned
     adj 1: without apparent forethought or prompting or planning; "an
            unplanned economy"; "accepts an unplanned order"; "an
            unplanned pregnancy"; "unplanned remarks" [ant: {planned}]
     2: not intentional; "an unintended slight"; "an unintentional
        pun"; "the offense was unintentional"; "an unwitting
        mistake may be overlooked" [syn: {unintentional}, {unwitting}]
