---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncle
offline_file: ""
offline_thumbnail: ""
uuid: 151c4399-c18c-48bf-8ad5-3cf7f4e7eb2d
updated: 1484310473
title: uncle
categories:
    - Dictionary
---
uncle
     n 1: the brother of your father or mother; the husband of your
          aunt [ant: {aunt}]
     2: a source of help and advice and encouragement; "he played
        uncle to lonely students"
