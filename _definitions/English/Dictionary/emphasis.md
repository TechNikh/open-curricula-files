---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphasis
offline_file: ""
offline_thumbnail: ""
uuid: 88924f27-63ae-43b4-8f6d-2e59eb4bfb84
updated: 1484310549
title: emphasis
categories:
    - Dictionary
---
emphasis
     n 1: special importance or significance; "the red light gave the
          central figure increased emphasis"; "the room was
          decorated in shades of gray with distinctive red
          accents" [syn: {accent}]
     2: intensity or forcefulness of expression; "the vehemence of
        his denial"; "his emphasis on civil rights" [syn: {vehemence}]
     3: special and significant stress by means of position or
        repetition e.g.
     4: the relative prominence of a syllable or musical note
        (especially with regard to stress or pitch); "he put the
        stress on the wrong syllable" [syn: {stress}, {accent}]
     [also: {emphases} (pl)]
