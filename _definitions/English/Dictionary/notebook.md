---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notebook
offline_file: ""
offline_thumbnail: ""
uuid: 487021f2-e66c-4806-aee5-6dc7bb365926
updated: 1484310309
title: notebook
categories:
    - Dictionary
---
notebook
     n 1: a book with blank pages for recording notes or memoranda
     2: a small compact portable computer [syn: {notebook computer}]
