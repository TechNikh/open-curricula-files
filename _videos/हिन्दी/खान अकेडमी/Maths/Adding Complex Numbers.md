---
version: 1
type: video
provider: YouTube
id: 3s4CIWsLEXk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ba8aa9ff-afc0-45dd-9b64-5e116767f344
updated: 1486069611
title: Adding Complex Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/3s4CIWsLEXk/default.jpg
    - https://i3.ytimg.com/vi/3s4CIWsLEXk/1.jpg
    - https://i3.ytimg.com/vi/3s4CIWsLEXk/2.jpg
    - https://i3.ytimg.com/vi/3s4CIWsLEXk/3.jpg
---
