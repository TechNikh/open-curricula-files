---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sweat_glands
offline_file: ""
offline_thumbnail: ""
uuid: 6e323c90-1d49-4640-82c0-324dcc0005cd
updated: 1484308368
title: Sweat gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Skin.png
tags:
    - Structure
    - Distribution
    - Animals
    - Types
    - Apocrine
    - Eccrine
    - Apoeccrine
    - Others
    - Sweat
    - Mechanism
    - Stimuli
    - thermal
    - Emotional
    - Gustatory
    - Antiperspirant
    - Pathology
    - Tumors
    - As signs in other illnesses
    - Gallery
    - Notes
categories:
    - Uncategorized
---
Sweat glands, also known as sudoriferous or sudoriparous glands, from Latin sudor, meaning "sweat",[6][7] are small tubular structures of the skin that produce sweat. Sweat glands are a type of exocrine gland, which are glands that produce and secrete substances onto an epithelial surface by way of a duct. There are two main types of sweat glands that differ in their structure, function, secretory product, mechanism of excretion, anatomic distribution, and distribution across species:
