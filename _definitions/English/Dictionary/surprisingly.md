---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surprisingly
offline_file: ""
offline_thumbnail: ""
uuid: 79e1a6ed-1c47-4c24-9a8e-cc48f22ffebc
updated: 1484310416
title: surprisingly
categories:
    - Dictionary
---
surprisingly
     adv 1: in a surprising manner; "he was surprisingly friendly"
     2: in an amazing manner; to everyone's surprise; "amazingly, he
        finished medical school in three years" [syn: {amazingly},
         {astonishingly}]
