---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenomena
offline_file: ""
offline_thumbnail: ""
uuid: 434778ec-a635-4303-824f-d2b7735e7ba0
updated: 1484310216
title: phenomena
categories:
    - Dictionary
---
phenomenon
     n 1: any state or process known through the senses rather than by
          intuition or reasoning
     2: a remarkable development
     [also: {phenomena} (pl)]
