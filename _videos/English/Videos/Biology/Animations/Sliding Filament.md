---
version: 1
type: video
provider: YouTube
id: 0kFmbrRJq4w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Sliding%20Filament.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cd795ff2-3543-4187-8628-d04b3a120a23
updated: 1486069668
title: Sliding Filament
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/0kFmbrRJq4w/default.jpg
    - https://i3.ytimg.com/vi/0kFmbrRJq4w/1.jpg
    - https://i3.ytimg.com/vi/0kFmbrRJq4w/2.jpg
    - https://i3.ytimg.com/vi/0kFmbrRJq4w/3.jpg
---
