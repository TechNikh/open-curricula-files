---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pacify
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484558641
title: pacify
categories:
    - Dictionary
---
pacify
     v 1: cause to be more favorably inclined; gain the good will of;
          "She managed to mollify the angry customer" [syn: {lenify},
           {conciliate}, {assuage}, {appease}, {mollify}, {placate},
           {gentle}, {gruntle}]
     2: fight violence and try to establish peace in (a location);
        "The U.N. troops are working to pacify Bosnia"
     [also: {pacified}]
