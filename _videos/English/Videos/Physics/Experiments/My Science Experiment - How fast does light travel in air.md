---
version: 1
type: video
provider: YouTube
id: 3x5C_N3jP1k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c6163097-b51c-4771-b448-bd1c09e819b3
updated: 1486069639
title: 'My Science Experiment - How fast does light travel in air?'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/3x5C_N3jP1k/default.jpg
    - https://i3.ytimg.com/vi/3x5C_N3jP1k/1.jpg
    - https://i3.ytimg.com/vi/3x5C_N3jP1k/2.jpg
    - https://i3.ytimg.com/vi/3x5C_N3jP1k/3.jpg
---
