---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degree
offline_file: ""
offline_thumbnail: ""
uuid: 828ec516-54c3-4ba6-841d-60f759ca346a
updated: 1484310232
title: degree
categories:
    - Dictionary
---
degree
     n 1: a position on a scale of intensity or amount or quality; "a
          moderate degree of intelligence"; "a high level of care
          is required"; "it is all a matter of degree" [syn: {grade},
           {level}]
     2: a specific identifiable position in a continuum or series or
        especially in a process; "a remarkable degree of
        frankness"; "at what stage are the social sciences?" [syn:
         {level}, {stage}, {point}]
     3: an award conferred by a college or university signifying
        that the recipient has satisfactorily completed a course
        of study; "he earned his degree at Princeton summa cum
        laude" [syn: {academic degree}]
     ...
