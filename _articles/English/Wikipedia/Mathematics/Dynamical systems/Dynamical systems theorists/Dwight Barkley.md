---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dwight_Barkley
offline_file: ""
offline_thumbnail: ""
uuid: 3130805f-4078-4b3b-bb29-b80617fb1ab7
updated: 1484308690
title: Dwight Barkley
tags:
    - Education and career
    - Research
    - Awards
    - Selected publications
categories:
    - Dynamical systems theorists
---
