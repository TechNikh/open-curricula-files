---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hear
offline_file: ""
offline_thumbnail: ""
uuid: c04ba34f-c113-48ba-81a5-f45ace751408
updated: 1484310297
title: hear
categories:
    - Dictionary
---
hear
     v 1: perceive (sound) via the auditory sense
     2: get to know or become aware of, usually accidentally; "I
        learned that she has two grown-up children"; "I see that
        you have been promoted" [syn: {learn}, {get word}, {get
        wind}, {pick up}, {find out}, {get a line}, {discover}, {see}]
     3: examine or hear (evidence or a case) by judicial process;
        "The jury had heard all the evidence"; "The case will be
        tried in California" [syn: {try}]
     4: receive a communication from someone; "We heard nothing from
        our son for five years"
     5: listen and pay attention; "Listen to your father"; "We must
        hear the expert before we ...
