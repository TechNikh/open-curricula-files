---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seed_dispersal
offline_file: ""
offline_thumbnail: ""
uuid: 33af486f-cb95-4a74-9c62-3e3abe7c16e0
updated: 1484308368
title: Seed dispersal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/540px-Epilobium_hirsutum_-_Seed_head_-_Triptych.jpg
tags:
    - Benefits
    - Types
    - Autochory
    - gravity
    - Ballistic dispersal
    - Allochory
    - Wind
    - Water
    - By animals
    - By humans
    - Consequences
categories:
    - Uncategorized
---
Seed dispersal is the movement or transport of seeds away from the parent plant. Plants have very limited mobility and consequently rely upon a variety of dispersal vectors to transport their propagules, including both abiotic and biotic vectors. Seeds can be dispersed away from the parent plant individually or collectively, as well as dispersed in both space and time. The patterns of seed dispersal are determined in large part by the dispersal mechanism and this has important implications for the demographic and genetic structure of plant populations, as well as migration patterns and species interactions. There are five main modes of seed dispersal: gravity, wind, ballistic, water, and by ...
