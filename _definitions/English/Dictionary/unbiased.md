---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unbiased
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484384101
title: unbiased
categories:
    - Dictionary
---
unbiased
     adj 1: characterized by a lack of partiality; "a properly
            indifferent jury"; "an unbiased account of her family
            problems" [syn: {indifferent}, {unbiassed}]
     2: without bias [syn: {unbiassed}]
