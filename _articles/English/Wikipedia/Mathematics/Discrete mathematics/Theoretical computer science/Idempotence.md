---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Idempotence
offline_file: ""
offline_thumbnail: ""
uuid: 0424e58a-1dba-4c37-8415-4f39aec204ef
updated: 1484308671
title: Idempotence
tags:
    - Definitions
    - Unary operation
    - Idempotent elements and binary operations
    - Connections
    - Common examples
    - Functions
    - Formal languages
    - Idempotent ring elements
    - Other examples
    - Computer science meaning
    - Examples
    - Applied examples
categories:
    - Theoretical computer science
---
Idempotence (/ˌaɪdᵻmˈpoʊtəns/ EYE-dəm-POH-təns)[1] is the property of certain operations in mathematics and computer science, that can be applied multiple times without changing the result beyond the initial application. The concept of idempotence arises in a number of places in abstract algebra (in particular, in the theory of projectors and closure operators) and functional programming (in which it is connected to the property of referential transparency).
