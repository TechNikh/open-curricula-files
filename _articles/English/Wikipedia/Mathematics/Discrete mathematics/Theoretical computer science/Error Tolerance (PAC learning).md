---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Error_Tolerance_(PAC_learning)
offline_file: ""
offline_thumbnail: ""
uuid: 150a0e1f-9db0-4114-9c5d-affc0f4e4e8c
updated: 1484308671
title: Error Tolerance (PAC learning)
tags:
    - Error Tolerance (PAC learning)
    - Notation and the Valiant learning model
    - Classification Noise
    - Statistical Query Learning
    - Malicious Classification
    - 'Errors in the inputs: Nonuniform Random Attribute Noise'
categories:
    - Theoretical computer science
---
