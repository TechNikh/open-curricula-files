---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576041
title: pious
categories:
    - Dictionary
---
pious
     adj 1: having or showing or expressing reverence for a deity;
            "pious readings" [ant: {impious}]
     2: devoutly religious; "a god-fearing and law-abiding people"
        H.L.Mencken [syn: {devout}, {god-fearing}]
