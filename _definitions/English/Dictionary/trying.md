---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trying
offline_file: ""
offline_thumbnail: ""
uuid: 731784e6-6918-4874-bc54-2e5e4994b5ef
updated: 1484310277
title: trying
categories:
    - Dictionary
---
trying
     adj 1: hard to endure; "fell upon trying times"
     2: extremely irritating to the nerves; "nerve-racking noise";
        "the stressful days before a war"; "a trying day at the
        office" [syn: {nerve-racking}, {nerve-wracking}, {stressful}]
