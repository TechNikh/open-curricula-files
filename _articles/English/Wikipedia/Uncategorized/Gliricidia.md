---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gliricidia
offline_file: ""
offline_thumbnail: ""
uuid: 3e1c1549-74a2-4457-b989-4961cd138278
updated: 1484308333
title: Gliricidia
categories:
    - Uncategorized
---
Gliricidia is a genus of flowering plants in the legume family, Fabaceae. It belongs to the sub family Faboideae. It is a small, deciduous, ornamental tree. The tree is leafless when in flower and bears fruits during April and May in India and countries with same climate. The small flowers (barely 2 cm long) are pale pink and they are borne in dense clusters on bare twigs. Flowers fade to white or a faint purple with age. The flowers attract a lot of bees and some lycaenid butterflies—particularly the Peablue Lampides boeticus and other native birds.
