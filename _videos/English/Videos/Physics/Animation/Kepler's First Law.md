---
version: 1
type: video
provider: YouTube
id: DvAT4WCF2eM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6c3b46a2-7e43-4717-93bd-48bb037af2b8
updated: 1486069653
title: "Kepler's First Law"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DvAT4WCF2eM/default.jpg
    - https://i3.ytimg.com/vi/DvAT4WCF2eM/1.jpg
    - https://i3.ytimg.com/vi/DvAT4WCF2eM/2.jpg
    - https://i3.ytimg.com/vi/DvAT4WCF2eM/3.jpg
---
