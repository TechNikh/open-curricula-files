---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transmitted
offline_file: ""
offline_thumbnail: ""
uuid: 2b2a0a99-5e32-4a96-8df9-5802baab3b3d
updated: 1484310333
title: transmitted
categories:
    - Dictionary
---
transmit
     v 1: transfer to another; "communicate a disease" [syn: {convey},
           {communicate}]
     2: transmit or serve as the medium for transmission; "Sound
        carries well over water"; "The airwaves carry the sound";
        "Many metals conduct heat" [syn: {conduct}, {convey}, {carry},
         {channel}]
     3: broadcast over the airwaves, as in radio or television; "We
        cannot air this X-rated song" [syn: {air}, {send}, {broadcast},
         {beam}]
     4: send from one person or place to another; "transmit a
        message" [syn: {transfer}, {transport}, {channel}, {channelize},
         {channelise}]
     [also: {transmitting}, {transmitted}]
