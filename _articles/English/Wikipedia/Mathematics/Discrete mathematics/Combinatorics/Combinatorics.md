---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: fe128533-6fb2-4785-adb6-cd87f7cd937b
updated: 1484308563
title: Combinatorics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Plain-bob-minor_2.png
tags:
    - History
    - Approaches and subfields of combinatorics
    - Enumerative combinatorics
    - Analytic combinatorics
    - Partition theory
    - Graph theory
    - Design theory
    - Finite geometry
    - Order theory
    - Matroid theory
    - Extremal combinatorics
    - Probabilistic combinatorics
    - Algebraic combinatorics
    - Combinatorics on words
    - Geometric combinatorics
    - Topological combinatorics
    - Arithmetic combinatorics
    - Infinitary combinatorics
    - Related fields
    - Combinatorial optimization
    - Coding theory
    - Discrete and computational geometry
    - Combinatorics and dynamical systems
    - Combinatorics and physics
    - Notes
categories:
    - Combinatorics
---
Combinatorics is a branch of mathematics concerning the study of finite or countable discrete structures. Aspects of combinatorics include counting the structures of a given kind and size (enumerative combinatorics), deciding when certain criteria can be met, and constructing and analyzing objects meeting the criteria (as in combinatorial designs and matroid theory), finding "largest", "smallest", or "optimal" objects (extremal combinatorics and combinatorial optimization), and studying combinatorial structures arising in an algebraic context, or applying algebraic techniques to combinatorial problems (algebraic combinatorics).
