---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Goose_bumps
offline_file: ""
offline_thumbnail: ""
uuid: 8d3d8027-7651-4fd4-a3b2-ede722fe12cf
updated: 1484308368
title: Goose bumps
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-2003-09-17_Goose_bumps.jpg
tags:
    - Etymology
    - Anatomy and biology
    - Cause
    - Extreme temperatures
    - Intense emotion
    - Ingestion
categories:
    - Uncategorized
---
Goosebumps, goose pimples or goose flesh are the bumps on a person's skin at the base of body hairs which may involuntarily develop when a person is cold or experiences strong emotions such as fear, euphoria or sexual arousal.[1] The medical term for the effect is cutis anserina or horripilation.
