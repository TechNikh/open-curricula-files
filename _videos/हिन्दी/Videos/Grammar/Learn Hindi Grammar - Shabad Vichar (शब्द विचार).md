---
version: 1
type: video
provider: YouTube
id: 1ellheHK_yY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca83bb9c-7852-45e3-a3b4-f69ede845557
updated: 1486069629
title: 'Learn Hindi Grammar - Shabad Vichar (शब्द विचार)'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/1ellheHK_yY/default.jpg
    - https://i3.ytimg.com/vi/1ellheHK_yY/1.jpg
    - https://i3.ytimg.com/vi/1ellheHK_yY/2.jpg
    - https://i3.ytimg.com/vi/1ellheHK_yY/3.jpg
---
