---
version: 1
type: article
id: https://en.wikipedia.org/wiki/MaxDDBS
offline_file: ""
offline_thumbnail: ""
uuid: 44a822c5-f3d1-4889-ac3a-e42e6ebe6e5a
updated: 1484309392
title: MaxDDBS
categories:
    - Computational problems in graph theory
---
Given a connected host graph G, an upper bound for the degree d, and an upper bound for the diameter k, we look for the largest subgraph S of G with maximum degree at most d and diameter at most k. This problem is also referred to as the Degree-Diameter Subgraph Problem, as it contains the degree diameter problem as a special case (namely, by taking a sufficiently large complete graph as a host graph). Despite being a natural generalization of the Degree-Diameter Problem, MaxDDBS only began to be investigated in 2011, while research in the Degree-Diameter Problem has been active since the 1960s. Regarding its computational complexity, the problem is NP-hard, and not in APX (i.e. it cannot ...
