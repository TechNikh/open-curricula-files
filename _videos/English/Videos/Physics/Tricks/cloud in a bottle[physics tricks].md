---
version: 1
type: video
provider: YouTube
id: Vf1h2lMDj-k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e53eb52d-e430-47ee-8037-536c1fd917c1
updated: 1486069658
title: 'cloud in a bottle[physics tricks]'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Vf1h2lMDj-k/default.jpg
    - https://i3.ytimg.com/vi/Vf1h2lMDj-k/1.jpg
    - https://i3.ytimg.com/vi/Vf1h2lMDj-k/2.jpg
    - https://i3.ytimg.com/vi/Vf1h2lMDj-k/3.jpg
---
