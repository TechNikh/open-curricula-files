---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submissively
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651941
title: submissively
categories:
    - Dictionary
---
submissively
     adv : in a servile manner; "he always acts so deferentially around
           his superviser" [syn: {deferentially}]
