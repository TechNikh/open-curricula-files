---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conflict
offline_file: ""
offline_thumbnail: ""
uuid: a508b45e-4dd4-492a-9040-0125af41b856
updated: 1484310441
title: conflict
categories:
    - Dictionary
---
conflict
     n 1: an open clash between two opposing groups (or individuals);
          "the harder the conflict the more glorious the
          triumph"--Thomas Paine; "police tried to control the
          battle between the pro- and anti-abortion mobs" [syn: {struggle},
           {battle}]
     2: opposition between two simultaneous but incompatible
        feelings; "he was immobilized by conflict and indecision"
     3: a hostile meeting of opposing military forces in the course
        of a war; "Grant won a decisive victory in the battle of
        Chickamauga"; "he lost his romantic ideas about war when
        he got into a real engagement" [syn: {battle}, {fight}, {engagement}]
 ...
