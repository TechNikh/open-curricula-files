---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hiroshima
offline_file: ""
offline_thumbnail: ""
uuid: 442fb594-c76b-401f-975f-def03399ecf7
updated: 1484308482
title: Hiroshima
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hiroshima_Metropolitan_Employment_Area.svg.png
tags:
    - History
    - Sengoku period (1589–1871)
    - Imperial period (1871–1939)
    - World War II and the atomic bombing (1939–1945)
    - Postwar period (1945–present)
    - geography
    - Climate
    - Wards
    - Demographics
    - Transportation
categories:
    - Uncategorized
---
Hiroshima (広島市, Hiroshima-shi?) ( listen (help·info)) is the capital of Hiroshima Prefecture and the largest city in the Chūgoku region of western Honshu - the largest island of Japan. The city's name, 広島, means "Broad Island" in Japanese. Hiroshima gained city status on April 1, 1889. On April 1, 1980, Hiroshima became a designated city. As of August 2016[update], the city had an estimated population of 1,196,274. The GDP in Greater Hiroshima, Hiroshima Metropolitan Employment Area, is US$61.3 billion as of 2010.[1][2] Kazumi Matsui has been the city's mayor since April 2011.
