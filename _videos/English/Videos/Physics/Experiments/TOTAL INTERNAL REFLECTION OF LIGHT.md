---
version: 1
type: video
provider: YouTube
id: -Mnz-nIZs0E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c3e6ce7f-21a6-4dd7-b779-e95bf227abb3
updated: 1486069634
title: TOTAL INTERNAL REFLECTION OF LIGHT
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/-Mnz-nIZs0E/default.jpg
    - https://i3.ytimg.com/vi/-Mnz-nIZs0E/1.jpg
    - https://i3.ytimg.com/vi/-Mnz-nIZs0E/2.jpg
    - https://i3.ytimg.com/vi/-Mnz-nIZs0E/3.jpg
---
