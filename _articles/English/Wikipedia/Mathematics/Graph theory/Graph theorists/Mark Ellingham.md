---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mark_Ellingham
offline_file: ""
offline_thumbnail: ""
uuid: f76eafc8-a0d1-4d37-8739-fcdfcfc81aa4
updated: 1484309461
title: Mark Ellingham
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Ellingham-Horton_54-graph.svg.png
categories:
    - Graph theorists
---
Mark Norman Ellingham is a professor of mathematics at Vanderbilt University whose research concerns graph theory.[1] With Joseph D. Horton, he is the discoverer and namesake of the Ellingham–Horton graphs, two cubic 3-vertex-connected bipartite graphs that have no Hamiltonian cycle.[2]
