---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homogeneous_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 6fc39054-f588-4490-b038-e8c5b51aab38
updated: 1484309153
title: Homogeneous polynomial
tags:
    - Properties
    - Homogenization
    - Algebraic forms in general
categories:
    - Algebraic geometry
---
In mathematics, a homogeneous polynomial is a polynomial whose nonzero terms all have the same degree.[1] For example, 
  
    
      
        
          x
          
            5
          
        
        +
        2
        
          x
          
            3
          
        
        
          y
          
            2
          
        
        +
        9
        x
        
          y
          
            4
          
        
      
    
    {\displaystyle x^{5}+2x^{3}y^{2}+9xy^{4}}
  
 is a homogeneous polynomial of degree 5, in two variables; the sum of the exponents in each term is always 5. The polynomial 
  
    
      
        
          x
          
            3
  ...
