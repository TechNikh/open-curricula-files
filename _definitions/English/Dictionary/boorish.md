---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boorish
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415841
title: boorish
categories:
    - Dictionary
---
boorish
     adj : ill-mannered and coarse and contemptible in behavior or
           appearance; "was boorish and insensitive"; "the loutish
           manners of a bully"; "her stupid oafish husband";
           "aristocratic contempt for the swinish multitude" [syn:
            {loutish}, {neanderthal}, {neandertal}, {oafish}, {swinish}]
