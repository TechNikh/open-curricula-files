---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/improving
offline_file: ""
offline_thumbnail: ""
uuid: 56bf9803-1984-4d3d-a787-aa4384b804f5
updated: 1484310377
title: improving
categories:
    - Dictionary
---
improving
     adj : getting higher or more vigorous; "its an up market"; "an
           improving economy" [syn: {up}]
