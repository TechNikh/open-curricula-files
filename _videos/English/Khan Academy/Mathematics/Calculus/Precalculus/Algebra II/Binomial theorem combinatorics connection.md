---
version: 1
type: video
provider: YouTube
id: xF_hJaXUNfE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Algebra%20II/Binomial%20theorem%20combinatorics%20connection.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e259293b-9911-4b54-be79-9f0407052cab
updated: 1486069694
title: Binomial theorem combinatorics connection
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/xF_hJaXUNfE/default.jpg
    - https://i3.ytimg.com/vi/xF_hJaXUNfE/1.jpg
    - https://i3.ytimg.com/vi/xF_hJaXUNfE/2.jpg
    - https://i3.ytimg.com/vi/xF_hJaXUNfE/3.jpg
---
