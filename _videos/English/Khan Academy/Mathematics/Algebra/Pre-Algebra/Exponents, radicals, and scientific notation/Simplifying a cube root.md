---
version: 1
type: video
provider: YouTube
id: drhoIgAhlQM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Exponents%2C%20radicals%2C%20and%20scientific%20notation/Simplifying%20a%20cube%20root.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cfbc45cd-c6a9-49d9-b966-aa06f08f2722
updated: 1486069559
title: Simplifying a cube root
categories:
    - Exponents, radicals, and scientific notation
thumbnail_urls:
    - https://i3.ytimg.com/vi/drhoIgAhlQM/default.jpg
    - https://i3.ytimg.com/vi/drhoIgAhlQM/1.jpg
    - https://i3.ytimg.com/vi/drhoIgAhlQM/2.jpg
    - https://i3.ytimg.com/vi/drhoIgAhlQM/3.jpg
---
