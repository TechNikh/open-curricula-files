---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_rank
offline_file: ""
offline_thumbnail: ""
uuid: a4c38770-a549-4245-b929-a5f03bf5a126
updated: 1484309400
title: Cycle rank
tags:
    - Definition
    - History
    - Examples
    - Computing the cycle rank
    - Applications
    - Star height of regular languages
    - Cholesky factorization in sparse matrix computations
categories:
    - Graph connectivity
---
In graph theory, the cycle rank of a directed graph is a digraph connectivity measure proposed first by Eggan and Büchi (Eggan 1963). Intuitively, this concept measures how close a digraph is to a directed acyclic graph (DAG), in the sense that a DAG has cycle rank zero, while a complete digraph of order n with a self-loop at each vertex has cycle rank n. The cycle rank of a directed graph is closely related to the tree-depth of an undirected graph and to the star height of a regular language. It has also found use in sparse matrix computations (see Bodlaender et al. 1995) and logic (Rossman 2008).
