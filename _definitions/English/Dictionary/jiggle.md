---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jiggle
offline_file: ""
offline_thumbnail: ""
uuid: 31615498-5fa6-49ac-a8b9-23491cbb07a4
updated: 1484310234
title: jiggle
categories:
    - Dictionary
---
jiggle
     n : a slight irregular shaking motion [syn: {joggle}]
     v : move to and fro; "Don't jiggle your finger while the nurse
         is putting on the bandage!" [syn: {joggle}, {wiggle}]
