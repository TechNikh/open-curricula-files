---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multi-index_notation
offline_file: ""
offline_thumbnail: ""
uuid: 756bac1e-d748-4a97-a567-067d8e093c7b
updated: 1484308583
title: Multi-index notation
tags:
    - Definition and basic properties
    - Some applications
    - An example theorem
    - Proof
categories:
    - Combinatorics
---
Multi-index notation is a mathematical notation that simplifies formulas used in multivariable calculus, partial differential equations and the theory of distributions, by generalising the concept of an integer index to an ordered tuple of indices.
