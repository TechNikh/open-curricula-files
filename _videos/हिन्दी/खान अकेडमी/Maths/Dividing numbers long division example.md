---
version: 1
type: video
provider: YouTube
id: r8CP7kH1nUA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9e0df328-3456-40c3-a673-1bea6dae4c0e
updated: 1486069616
title: Dividing numbers long division example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/r8CP7kH1nUA/default.jpg
    - https://i3.ytimg.com/vi/r8CP7kH1nUA/1.jpg
    - https://i3.ytimg.com/vi/r8CP7kH1nUA/2.jpg
    - https://i3.ytimg.com/vi/r8CP7kH1nUA/3.jpg
---
