---
version: 1
type: video
provider: YouTube
id: _q4TeXMQafk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/14.7%20Light%20Harvesting.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b2010dff-12fb-4ed0-8a3a-249c733f66a6
updated: 1486069676
title: 14.7 Light Harvesting
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/_q4TeXMQafk/default.jpg
    - https://i3.ytimg.com/vi/_q4TeXMQafk/1.jpg
    - https://i3.ytimg.com/vi/_q4TeXMQafk/2.jpg
    - https://i3.ytimg.com/vi/_q4TeXMQafk/3.jpg
---
