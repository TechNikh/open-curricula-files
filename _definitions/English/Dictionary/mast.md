---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mast
offline_file: ""
offline_thumbnail: ""
uuid: 847569a9-57d9-4eae-bb77-06f84b866026
updated: 1484310589
title: mast
categories:
    - Dictionary
---
mast
     n 1: a vertical spar for supporting sails
     2: nuts of forest trees (as beechnuts and acorns) accumulated
        on the ground; used especially as food for swine
     3: nuts of forest trees used as feed for swine
     4: any sturdy upright pole
