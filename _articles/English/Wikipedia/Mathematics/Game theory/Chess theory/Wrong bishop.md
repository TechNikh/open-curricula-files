---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wrong_bishop
offline_file: ""
offline_thumbnail: ""
uuid: dcfd078c-99f9-4677-a6bb-9826ab569150
updated: 1484309020
title: Wrong bishop
tags:
    - Rook versus bishop
    - Rook pawn
    - Bishop and rook pawn
    - Rook and rook pawn versus bishop
    - Rook and bishop pawn versus bishop
    - Examples from games
    - Rook and pawn versus bishop and pawn
categories:
    - Chess theory
---
The wrong bishop is a situation in chess endgame when a bishop on the other color of square of the chessboard would either win a game instead of draw or salvage a draw from an inferior position (Rosen 2003:61); in other words, a bishop is unable to guard squares of the other color. This most commonly occurs with a bishop and one of its rook pawns, but it also occurs with a rook versus a bishop, a rook and one rook pawn versus a bishop, and possibly with a rook and one bishop pawn versus a bishop.
