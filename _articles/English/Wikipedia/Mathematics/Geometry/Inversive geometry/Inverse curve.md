---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inverse_curve
offline_file: ""
offline_thumbnail: ""
uuid: 31a00d77-6b0b-4b4b-b3e8-bfda08edff1e
updated: 1484309258
title: Inverse curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Inverse_Curves_Parabola_Cardioid.svg.png
tags:
    - Equations
    - Degrees
    - Examples
    - Particular cases
    - Lines
    - Circles
    - Parabolas with center of inversion at the vertex
    - Conic sections with center of inversion at a focus
    - Ellipses and hyperbolas with center of inversion at a vertex
    - >
        Ellipses and hyperbolas with center of inversion at the
        center
    - Conics with arbitrary center of inversion
    - Anallagmatic curves
categories:
    - Inversive geometry
---
In inversive geometry, an inverse curve of a given curve C is the result of applying an inverse operation to C. Specifically, with respect to a fixed circle with center O and radius k the inverse of a point Q is the point P for which P lies on the ray OQ and OP·OQ = k2. The inverse of the curve C is then the locus of P as Q runs over C. The point O in this construction is called the center of inversion, the circle the circle of inversion, and k the radius of inversion.
