---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sticking
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484358301
title: sticking
categories:
    - Dictionary
---
sticking
     adj : extending out above or beyond a surface or boundary; "the
           jutting limb of a tree"; "massive projected
           buttresses"; "his protruding ribs"; "a pile of boards
           sticking over the end of his truck" [syn: {jutting}, {projected},
            {projecting}, {protruding}, {sticking(p)}, {sticking
           out(p)}]
