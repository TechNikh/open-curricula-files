---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Costa%27s_minimal_surface'
offline_file: ""
offline_thumbnail: ""
uuid: 36831757-3c8a-4ae0-a3ef-d20e4667d5eb
updated: 1484309215
title: "Costa's minimal surface"
categories:
    - Differential geometry
---
In mathematics, Costa's minimal surface, is an embedded minimal surface discovered in 1982 by the Brazilian mathematician Celso José da Costa. It is also a surface of finite topology, which means that it can be formed by puncturing a compact surface. Topologically, it is a thrice-punctured torus.
