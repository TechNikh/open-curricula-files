---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484577121
title: worm
categories:
    - Dictionary
---
worm
     n 1: any of numerous relatively small elongated soft-bodied
          animals especially of the phyla Annelida and
          Chaetognatha and Nematoda and Nemertea and
          Platyhelminthes; also many insect larvae
     2: a person who has a nasty or unethical character undeserving
        of respect [syn: {louse}, {insect}, {dirt ball}]
     3: a software program capable of reproducing itself that can
        spread from one computer to the next over a network;
        "worms take advantage of automatic file sending and
        receiving features found on many computers"
     4: screw thread on a gear with the teeth of a worm wheel or
        rack
     v : to move in a ...
