---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/borax
offline_file: ""
offline_thumbnail: ""
uuid: d1ae60f9-fab5-4078-b44a-1e9232987f6d
updated: 1484310384
title: borax
categories:
    - Dictionary
---
borax
     n : an ore of boron consisting of hydrated sodium borate; used
         as a flux or cleansing agent
     [also: {boraces} (pl)]
