---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ideal_solution
offline_file: ""
offline_thumbnail: ""
uuid: 9fe18ae0-dc5d-43b9-9600-a5c132245af4
updated: 1484308849
title: Ideal solution
tags:
    - Physical origin
    - Formal definition
    - Thermodynamic properties
    - Volume
    - Enthalpy and heat capacity
    - Entropy of mixing
    - Consequences
    - Non-ideality
categories:
    - Thermodynamics
---
In chemistry, an ideal solution or ideal mixture is a solution with thermodynamic properties analogous to those of a mixture of ideal gases. The enthalpy of mixing is zero[1] as is the volume change on mixing by definition; the closer to zero the enthalpy of mixing is, the more "ideal" the behavior of the solution becomes. The vapor pressure of the solution obeys Raoult's law, and the activity coefficient of each component (which measures deviation from ideality) is equal to one.[2]
