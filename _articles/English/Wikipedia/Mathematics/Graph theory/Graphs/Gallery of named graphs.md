---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gallery_of_named_graphs
offline_file: ""
offline_thumbnail: ""
uuid: 70379f49-548f-4666-ae61-14f97e23f517
updated: 1484309414
title: Gallery of named graphs
tags:
    - Individual graphs
    - Highly symmetric graphs
    - Strongly regular graphs
    - Symmetric graphs
    - Semi-symmetric graphs
    - Graph families
    - Complete graphs
    - Complete bipartite graphs
    - Cycles
    - Friendship graphs
    - Fullerene graphs
    - Platonic solids
    - Truncated solids
    - Snarks
    - Star
    - Wheel graphs
categories:
    - Graphs
---
Some of the finite structures considered in graph theory have names, sometimes inspired by the graph's topology, and sometimes after their discoverer. A famous example is the Petersen graph, a concrete graph on 10 vertices that appears as a minimal example or counterexample in many different contexts.
