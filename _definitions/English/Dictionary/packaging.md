---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/packaging
offline_file: ""
offline_thumbnail: ""
uuid: 15aeb31c-eb0c-4cf5-bae0-d9b0f03f9f28
updated: 1484310539
title: packaging
categories:
    - Dictionary
---
packaging
     n 1: the business of packaging; "the packaging of new ideas";
          "packaging for transport"
     2: a message issued in behalf of some product or cause or idea
        or person or institution [syn: {promotion}, {publicity}, {promotional
        material}]
     3: material used to make packages
