---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/psychiatry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484558341
title: psychiatry
categories:
    - Dictionary
---
psychiatry
     n : the branch of medicine dealing with the diagnosis and
         treatment of mental disorders [syn: {psychopathology}, {psychological
         medicine}]
