---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kanyakumari
offline_file: ""
offline_thumbnail: ""
uuid: 092ea610-807f-41da-b0ee-f162acef27b8
updated: 1484308420
title: Kanyakumari
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thiruvalluvar_Statue_at_Night.JPG
tags:
    - History
    - Modern history
    - Demographics
    - geography
    - Tourism
    - Pilgrimage
    - Transportation
    - Road
    - Rail
    - Places of interest
    - Thiruvalluvar Statue
    - Vivekananda Rock Memorial
    - Gandhi Memorial Mandapam
    - Tsunami Memorial Park
    - Bhagavathy Amman Temple
    - Ayya Vaikundar Nizhal Thangal
    - Kamarajar Mani Mantapa Monument
    - Padmanabhapuram Palace
    - Gallery
    - Tourist information
    - Panoramic View
    - Religious significance
categories:
    - Uncategorized
---
Kanyakumari  pronunciation (help·info), also known as Kanniyakumari, formerly known as Cape Comorin, is a town in Kanyakumari District in the state of Tamil Nadu in India. The name comes from the Devi Kanya Kumari Temple in the region. It is the southernmost tip of peninsular India. Kanyakumari town is the southern tip of the Cardamom Hills, an extension of the Western Ghats range. The nearest town is Nagercoil, the administrative headquarters of Kanyakumari District, 22 km (14 mi) away. Kanyakumari has been a town since Sangam period[1][2] and is a popular tourist destination.[3]
