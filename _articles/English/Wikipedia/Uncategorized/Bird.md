---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aves
offline_file: ""
offline_thumbnail: ""
uuid: 0e26f719-890d-4932-ab89-044da1932daa
updated: 1484308344
title: Bird
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Naturkundemuseum_Berlin_-_Archaeopteryx_-_Eichst%25C3%25A4tt.jpg'
tags:
    - Evolution and classification
    - Definition
    - Dinosaurs and the origin of birds
    - Early evolution
    - Early diversity of bird ancestors
    - Diversification of modern birds
    - Classification of bird orders
    - Distribution
    - Anatomy and physiology
    - Skeletal system
    - Excretory system
    - Respiratory and circulatory systems
    - Heart type and features
    - Organisation
    - Nervous system
    - Defence and intraspecific combat
    - Chromosomes
    - Feathers, plumage, and scales
    - Flight
    - Behaviour
    - Diet and feeding
    - Water and drinking
    - Feather care
    - Migration
    - Communication
    - Flocking and other associations
    - Resting and roosting
    - Breeding
    - Social systems
    - Territories, nesting and incubation
    - Parental care and fledging
    - Brood parasites
    - Sexual selection
    - Inbreeding depression
    - Inbreeding avoidance
    - Ecology
    - Relationship with humans
    - Economic importance
    - In religion and mythology
    - In culture and folklore
    - In music
    - Conservation
    - Notes
categories:
    - Uncategorized
---
Birds (Aves), also known as avian dinosaurs,[3] are a group of endothermic vertebrates, characterised by feathers, toothless beaked jaws, the laying of hard-shelled eggs, a high metabolic rate, a four-chambered heart, and a lightweight but strong skeleton. Birds live worldwide and range in size from the 5 cm (2 in) bee hummingbird to the 2.75 m (9 ft) ostrich. They rank as the class of tetrapods with the most living species, at approximately ten thousand, with more than half of these being passerines, sometimes known as perching birds or, less accurately, as songbirds.
