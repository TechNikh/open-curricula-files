---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416501
title: polite
categories:
    - Dictionary
---
polite
     adj 1: showing regard for others in manners, speech, behavior, etc.
            [ant: {impolite}]
     2: marked by refinement in taste and manners; "cultivated
        speech"; "cultured Bostonians"; "cultured tastes"; "a
        genteel old lady"; "polite society" [syn: {civilized}, {civilised},
         {cultivated}, {cultured}, {genteel}]
     3: not rude; marked by satisfactory (or especially minimal)
        adherence to social usages and sufficient but not
        noteworthy consideration for others; "even if he didn't
        like them he should have been civil"- W.S. Maugham [syn: {civil}]
        [ant: {uncivil}]
