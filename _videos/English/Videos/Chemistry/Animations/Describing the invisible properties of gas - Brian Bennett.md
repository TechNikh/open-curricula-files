---
version: 1
type: video
provider: YouTube
id: EHxdVtygP1g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Describing%20the%20invisible%20properties%20of%20gas%20-%20Brian%20Bennett.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 784e922c-1e98-476a-9443-06208f8b9edb
updated: 1486069665
title: 'Describing the invisible properties of gas - Brian Bennett'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/EHxdVtygP1g/default.jpg
    - https://i3.ytimg.com/vi/EHxdVtygP1g/1.jpg
    - https://i3.ytimg.com/vi/EHxdVtygP1g/2.jpg
    - https://i3.ytimg.com/vi/EHxdVtygP1g/3.jpg
---
