---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Cheeger_constant_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 2a83799c-c16a-4c4b-9451-84f88bc291d4
updated: 1484308612
title: Cheeger constant (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NetworkTopology-Ring.png
tags:
    - Definition
    - 'Example: computer networking'
    - Cheeger Inequalities
categories:
    - Graph theory
---
In mathematics, the Cheeger constant (also Cheeger number or isoperimetric number) of a graph is a numerical measure of whether or not a graph has a "bottleneck". The Cheeger constant as a measure of "bottleneckedness" is of great interest in many areas: for example, constructing well-connected networks of computers, card shuffling. The graph theoretical notion originated after the Cheeger isoperimetric constant of a compact Riemannian manifold.
