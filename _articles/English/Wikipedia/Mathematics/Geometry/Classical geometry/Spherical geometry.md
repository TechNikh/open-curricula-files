---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spherical_geometry
offline_file: ""
offline_thumbnail: ""
uuid: a0815215-4ae4-4e23-8c8f-f48ea0d6deb1
updated: 1484309168
title: Spherical geometry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Triangles_%2528spherical_geometry%2529.jpg'
tags:
    - History
    - Greek antiquity
    - Islamic world
    - "Euler's work"
    - Properties
    - "Relation to Euclid's postulates"
    - Notes
categories:
    - Classical geometry
---
Spherical geometry is the geometry of the two-dimensional surface of a sphere. It is an example of a geometry that is not Euclidean. Two practical applications of the principles of spherical geometry are navigation and astronomy.
