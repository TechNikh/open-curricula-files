---
version: 1
type: video
provider: YouTube
id: 7hrISnTa5gg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5cfba9e-240b-4b15-9549-4a02844fd1b2
updated: 1486069616
title: How Many Truffle Eating Guests Attended a Party
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/7hrISnTa5gg/default.jpg
    - https://i3.ytimg.com/vi/7hrISnTa5gg/1.jpg
    - https://i3.ytimg.com/vi/7hrISnTa5gg/2.jpg
    - https://i3.ytimg.com/vi/7hrISnTa5gg/3.jpg
---
