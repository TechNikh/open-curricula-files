---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flattening
offline_file: ""
offline_thumbnail: ""
uuid: f1ebc426-b2b0-42e9-924f-0f1bd03e1927
updated: 1484309341
title: Flattening
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-An_ellipse_with_auxiliary_circle.svg.png
tags:
    - Definitions of flattening
    - Identities involving flattening
    - Numerical values for planets
    - Origin of flattening
categories:
    - Trigonometry
---
Flattening is a measure of the compression of a circle or sphere along a diameter to form an ellipse or an ellipsoid of revolution (spheroid) respectively. Other terms used are ellipticity, or oblateness. The usual notation for flattening is f and its definition in terms of the semi-axes of the resulting ellipse or ellipsoid is
