---
version: 1
type: video
provider: YouTube
id: oi5dlOFAGQQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a54f85c0-607c-4ef8-90b4-71d8276620b5
updated: 1486069613
title: Representing numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/oi5dlOFAGQQ/default.jpg
    - https://i3.ytimg.com/vi/oi5dlOFAGQQ/1.jpg
    - https://i3.ytimg.com/vi/oi5dlOFAGQQ/2.jpg
    - https://i3.ytimg.com/vi/oi5dlOFAGQQ/3.jpg
---
