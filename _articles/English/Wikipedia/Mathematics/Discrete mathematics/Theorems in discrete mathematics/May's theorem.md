---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/May%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: e7165482-1da9-4cb9-b72a-9b1dbafa5174
updated: 1484308662
title: "May's theorem"
categories:
    - Theorems in discrete mathematics
---
In social choice theory, May's theorem states that simple majority voting is the only anonymous, neutral, and positively responsive social choice function between two alternatives. Further, this procedure is resolute when there are an odd number of voters and ties (indecision) are not allowed. Kenneth May first published this theory in 1952.[1]
