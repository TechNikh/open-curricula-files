---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique-width
offline_file: ""
offline_thumbnail: ""
uuid: 808542c3-96c6-4f68-91da-b72a99c5d158
updated: 1484309417
title: Clique-width
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Clique-width_construction.svg.png
tags:
    - Special classes of graphs
    - Bounds
    - Computational complexity
    - Relation to treewidth
    - Notes
categories:
    - Graph invariants
---
In graph theory, the clique-width of a graph 
  
    
      
        G
      
    
    {\displaystyle G}
  
 is a parameter that describes the structural complexity of the graph; it is closely related to treewidth, but unlike treewidth it can be bounded even for dense graphs. It is defined as the minimum number of labels needed to construct 
  
    
      
        G
      
    
    {\displaystyle G}
  
 by means of the following 4 operations :
