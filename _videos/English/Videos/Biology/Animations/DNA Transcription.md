---
version: 1
type: video
provider: YouTube
id: AGzsgTMgSog
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20Transcription.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0da2a3a4-84cc-4d1c-ace2-bc221643d5d4
updated: 1486069672
title: DNA Transcription
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/AGzsgTMgSog/default.jpg
    - https://i3.ytimg.com/vi/AGzsgTMgSog/1.jpg
    - https://i3.ytimg.com/vi/AGzsgTMgSog/2.jpg
    - https://i3.ytimg.com/vi/AGzsgTMgSog/3.jpg
---
