---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fever
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469721
title: fever
categories:
    - Dictionary
---
fever
     n 1: a rise in the temperature of the body; frequently a symptom
          of infection [syn: {febrility}, {febricity}, {pyrexia},
          {feverishness}]
     2: intense nervous anticipation; "in a fever of resentment"
