---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/callus
offline_file: ""
offline_thumbnail: ""
uuid: 43474a8c-5c24-40b8-afd1-97721c9fcef1
updated: 1484310160
title: callus
categories:
    - Dictionary
---
callus
     n 1: an area of skin that is thick or hard from continual
          pressure or friction (as the sole of the foot) [syn: {callosity}]
     2: bony tissue formed during the healing of a fractured bone
     3: (botany) an isolated thickening of tissue, especially a
        stiff protuberance on the lip of an orchid
     v 1: cause a callus to form on; "The long march had callused his
          feet"
     2: form a callus or calluses; "His foot callused"
     [also: {calli} (pl)]
