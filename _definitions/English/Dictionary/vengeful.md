---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vengeful
offline_file: ""
offline_thumbnail: ""
uuid: 61132941-caeb-41ba-8407-a4a5da5c9f73
updated: 1484310555
title: vengeful
categories:
    - Dictionary
---
vengeful
     adj : disposed to seek revenge or intended for revenge; "more
           vindictive than jealous love"- Shakespeare;
           "punishments...essentially vindictive in their nature"-
           M.R.Cohen [syn: {revengeful}, {vindictive}]
