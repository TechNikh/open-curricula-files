---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kruskal%27s_tree_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: efd93c2d-8e56-461b-8dec-9b506f96f106
updated: 1484308659
title: "Kruskal's tree theorem"
tags:
    - "Friedman's finite form"
    - Notes
categories:
    - Theorems in discrete mathematics
---
In mathematics, Kruskal's tree theorem states that the set of finite trees over a well-quasi-ordered set of labels is itself well-quasi-ordered (under homeomorphic embedding).[clarification needed] The theorem was conjectured by Andrew Vázsonyi and proved by Joseph Kruskal (1960); a short proof was given by Nash-Williams (1963).
