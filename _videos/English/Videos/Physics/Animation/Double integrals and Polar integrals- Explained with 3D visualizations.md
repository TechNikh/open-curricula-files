---
version: 1
type: video
provider: YouTube
id: GHBMiscPE-g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b0d2a87c-cc7b-434b-88bf-f63a8b0ca491
updated: 1486069657
title: 'Double integrals and Polar integrals- Explained with 3D visualizations'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/GHBMiscPE-g/default.jpg
    - https://i3.ytimg.com/vi/GHBMiscPE-g/1.jpg
    - https://i3.ytimg.com/vi/GHBMiscPE-g/2.jpg
    - https://i3.ytimg.com/vi/GHBMiscPE-g/3.jpg
---
