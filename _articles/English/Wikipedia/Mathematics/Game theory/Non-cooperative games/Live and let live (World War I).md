---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Live_and_let_live_(World_War_I)
offline_file: ""
offline_thumbnail: ""
uuid: 69d23349-f119-494c-b498-f9b11cf5d0f0
updated: 1484309057
title: Live and let live (World War I)
tags:
    - Examples
    - Research
    - Game theory
categories:
    - Non-cooperative games
---
Live and let live is the spontaneous rise of non-aggressive co-operative behaviour that developed during the First World War particularly during prolonged periods of trench warfare on the Western Front. Perhaps one of the most famous examples of this is the Christmas Truce of 1914.
