---
version: 1
type: video
provider: YouTube
id: c_N9G3N9Ubc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Adding%20and%20Subtracting%20Rational%20Expressions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bfbe804b-6d45-47d8-bfb5-fd823dc2a160
updated: 1486069557
title: Adding and Subtracting Rational Expressions 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/c_N9G3N9Ubc/default.jpg
    - https://i3.ytimg.com/vi/c_N9G3N9Ubc/1.jpg
    - https://i3.ytimg.com/vi/c_N9G3N9Ubc/2.jpg
    - https://i3.ytimg.com/vi/c_N9G3N9Ubc/3.jpg
---
