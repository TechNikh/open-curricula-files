---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isotropy
offline_file: ""
offline_thumbnail: ""
uuid: 63da8551-3c6c-438d-9c60-707bd8eba3c1
updated: 1484309295
title: Isotropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LvMS-Lvv.jpg
tags:
    - Mathematics
    - Physics
    - Materials science
    - Microfabrication
    - Antenna (radio)
    - Biology
    - Other sciences
categories:
    - Orientation (geometry)
---
Isotropy is uniformity in all orientations; it is derived from the Greek isos (ἴσος, "equal") and tropos (τρόπος, "way"). Precise definitions depend on the subject area. Exceptions, or inequalities, are frequently indicated by the prefix an, hence anisotropy. Anisotropy is also used to describe situations where properties vary systematically, dependent on direction. Isotropic radiation has the same intensity regardless of the direction of measurement, and an isotropic field exerts the same action regardless of how the test particle is oriented.
