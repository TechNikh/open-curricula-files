---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smartly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484455741
title: smartly
categories:
    - Dictionary
---
smartly
     adv 1: in a clever manner; "they were cleverly arranged"; "a
            smartly managed business" [syn: {cleverly}]
     2: with vigor; in a vigorous manner; "he defended his ideas
        vigorously" [syn: {vigorously}]
     3: in a stylish manner; "He was smartly dressed" [syn: {modishly},
         {sprucely}]
