---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sensitive_flame
offline_file: ""
offline_thumbnail: ""
uuid: 6824a651-5a2e-4dd7-a13a-ff019d99b01f
updated: 1484308896
title: Sensitive flame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-PSM_V36_D052_Sensitive_batwing_flames_testing_light_and_its_effect_on_sound.jpg
categories:
    - Waves
---
A sensitive flame is a gas flame which under suitable adjustment of pressure resonates readily with sounds or air vibrations in the vicinity.[1] Noticed by both the American scientist John LeConte and the English physicist William Fletcher Barrett, they recorded the effect that a shrill note had upon a gas flame issuing from a tapering jet. The phenomenon caught the attention of the Irish physicist John Tyndall who gave a lecture on the process to the Royal Institution in January 1867.[2]
