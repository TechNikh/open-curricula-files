---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ilo
offline_file: ""
offline_thumbnail: ""
uuid: 9a2e67aa-d831-4b87-bb49-4e443c554c56
updated: 1484310555
title: ilo
categories:
    - Dictionary
---
ILO
     n : the United Nations agency concerned with the interests of
         labor [syn: {International Labor Organization}, {International
         Labour Organization}]
