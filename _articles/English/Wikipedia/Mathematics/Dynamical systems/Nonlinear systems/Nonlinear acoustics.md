---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonlinear_acoustics
offline_file: ""
offline_thumbnail: ""
uuid: 5b2a16af-bf65-46a9-8af0-5a7115220a43
updated: 1484308811
title: Nonlinear acoustics
tags:
    - Introduction
    - Physical analysis
    - Mathematical model
    - Governing Equations to Derive Westervelt Equation
    - Westervelt equation
    - "Burgers' equation"
    - KZK equation
    - Common occurrences
    - Sonic boom
    - Acoustic levitation
    - Ultrasonic waves
    - Musical acoustics
categories:
    - Nonlinear systems
---
Nonlinear acoustics (NLA) is a branch of physics and acoustics dealing with sound waves of sufficiently large amplitudes. Large amplitudes require using full systems of governing equations of fluid dynamics (for sound waves in liquids and gases) and elasticity (for sound waves in solids). These equations are generally nonlinear, and their traditional linearization is no longer possible. The solutions of these equations show that, due to the effects of nonlinearity, sound waves are being distorted as they travel.
