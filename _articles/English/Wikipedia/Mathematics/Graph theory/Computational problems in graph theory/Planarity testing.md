---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planarity_testing
offline_file: ""
offline_thumbnail: ""
uuid: f00b23a4-6ad3-418e-b187-138bf4b89220
updated: 1484309396
title: Planarity testing
tags:
    - Planarity criteria
    - Algorithms
    - Path addition method
    - Vertex addition method
    - Edge addition method
    - Construction sequence method
categories:
    - Computational problems in graph theory
---
In graph theory, the planarity testing problem is the algorithmic problem of testing whether a given graph is a planar graph (that is, whether it can be drawn in the plane without edge intersections). This is a well-studied problem in computer science for which many practical algorithms have emerged, many taking advantage of novel data structures. Most of these methods operate in O(n) time (linear time), where n is the number of edges (or vertices) in the graph, which is asymptotically optimal. Rather than just being a single Boolean value, the output of a planarity testing algorithm may be a planar graph embedding, if the graph is planar, or an obstacle to planarity such as a Kuratowski ...
