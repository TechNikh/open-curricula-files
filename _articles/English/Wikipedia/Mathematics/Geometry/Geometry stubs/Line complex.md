---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Line_complex
offline_file: ""
offline_thumbnail: ""
uuid: ee756241-c783-49d7-94f8-9a553f22282f
updated: 1484309367
title: Line complex
categories:
    - Geometry stubs
---
In algebraic geometry, a line complex is a 3-fold given by the intersection of the Grassmannian G(2, 4) (embedded in projective space P5 by Plücker coordinates) with a hypersurface. It is called a line complex because points of G(2, 4) correspond to lines in P3, so a line complex can be thought of as a 3-dimensional family of lines in P3. The linear line complex and quadric line complex are the cases when the hypersurface has degree 1 or 2; they are both rational varieties.
