---
version: 1
type: article
id: https://en.wikipedia.org/wiki/AI_box
offline_file: ""
offline_thumbnail: ""
uuid: 0df708db-a5db-488b-8e26-e4ff18c92854
updated: 1484308662
title: AI box
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ArtificialFictionBrain.png
tags:
    - Motivation
    - Avenues of escape
    - Physical
    - Social engineering
    - AI-box experiment
    - Overall limitations
    - In fiction
categories:
    - Theoretical computer science
---
An AI box is a hypothetical isolated computer hardware system where a possibly dangerous artificial intelligence, or AI, is kept constrained in a "virtual prison" and not allowed to manipulate events in the external world. Such a box would be restricted to minimalist communication channels. Unfortunately, even if the box is well-designed, a sufficiently intelligent AI may nevertheless be able to persuade or trick its human keepers into releasing it, or otherwise be able to "hack" its way out of the box.[1]
