---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/politically
offline_file: ""
offline_thumbnail: ""
uuid: 1c4f2c3f-1fd4-4b9d-ae7c-388712052eaa
updated: 1484310549
title: politically
categories:
    - Dictionary
---
politically
     adv 1: with regard to social relationships involving authority;
            "politically correct clothing"
     2: with regard to government; "politically organized units"
