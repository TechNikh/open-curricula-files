---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harshly
offline_file: ""
offline_thumbnail: ""
uuid: 25478648-dc57-4312-977d-0aead9a24d4b
updated: 1484310609
title: harshly
categories:
    - Dictionary
---
harshly
     adv 1: in a harsh or unkind manner; "`That's enough!,' he cut in
            harshly"
     2: in a harsh and grating manner; "her voice fell gratingly on
        our ears" [syn: {gratingly}, {raspingly}]
