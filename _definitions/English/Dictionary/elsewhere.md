---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elsewhere
offline_file: ""
offline_thumbnail: ""
uuid: cbf89dcc-5bdb-4b52-89f4-e7fc927422c4
updated: 1484310447
title: elsewhere
categories:
    - Dictionary
---
elsewhere
     adv : in or to another place; "he went elsewhere"; "look elsewhere
           for the answer"
