---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance-transitive_graph
offline_file: ""
offline_thumbnail: ""
uuid: e9859f4b-2303-42b5-be40-4bcb9a018d4c
updated: 1484309371
title: Distance-transitive graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-BiggsSmith.svg.png
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a distance-transitive graph is a graph such that, given any two vertices v and w at any distance i, and any other two vertices x and y at the same distance, there is an automorphism of the graph that carries v to x and w to y.
