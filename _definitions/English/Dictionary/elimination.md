---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elimination
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484316302
title: elimination
categories:
    - Dictionary
---
elimination
     n 1: the act of removing or getting rid of something [syn: {riddance}]
     2: the bodily process of discharging waste matter [syn: {evacuation},
         {excretion}, {excreting}, {voiding}]
     3: analysis of a problem into alternative possibilities
        followed by the systematic elimination of unacceptable
        alternatives [syn: {reasoning by elimination}]
     4: the act of removing an unknown mathematical quantity by
        combining equations
     5: the murder of a competitor [syn: {liquidation}]
