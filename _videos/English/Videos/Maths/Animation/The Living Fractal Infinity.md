---
version: 1
type: video
provider: YouTube
id: ZIp-AuwGr6s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 681017de-fdb1-47c9-8ed8-621d4805acf3
updated: 1486069643
title: The Living Fractal Infinity
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZIp-AuwGr6s/default.jpg
    - https://i3.ytimg.com/vi/ZIp-AuwGr6s/1.jpg
    - https://i3.ytimg.com/vi/ZIp-AuwGr6s/2.jpg
    - https://i3.ytimg.com/vi/ZIp-AuwGr6s/3.jpg
---
