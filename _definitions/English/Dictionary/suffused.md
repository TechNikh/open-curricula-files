---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffused
offline_file: ""
offline_thumbnail: ""
uuid: 5d1cfa32-c832-4012-8b2d-097a8cf853f9
updated: 1484310541
title: suffused
categories:
    - Dictionary
---
suffused
     adj : being spread through with or as with color or light or
           liquid
