---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grey
offline_file: ""
offline_thumbnail: ""
uuid: f75ed423-509d-4815-a3c8-9df7f839b17c
updated: 1484310307
title: grey
categories:
    - Dictionary
---
grey
     adj 1: an achromatic color of any lightness between the extremes of
            black and white; "gray flannel suit"; "hair just
            turning gray" [syn: {gray}, {grayish}, {greyish}]
     2: showing characteristics of age, especially having gray or
        white hair; "whose beard with age is hoar"-Coleridge;
        "nodded his hoary head" [syn: {gray}, {gray-haired}, {grey-haired},
         {gray-headed}, {grey-headed}, {grizzly}, {hoar}, {hoary},
         {white-haired}]
     3: used to signify the Confederate forces in the Civil War (who
        wore gray uniforms); "a stalwart gray figure" [syn: {gray}]
     4: intermediate in character or position; "a gray area ...
