---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tried
offline_file: ""
offline_thumbnail: ""
uuid: c73b05b2-6f7e-4ab8-8fd0-62a370375ab5
updated: 1484310275
title: tried
categories:
    - Dictionary
---
tried
     adj 1: tested and proved useful or correct; "a tested method" [syn:
             {tested}, {well-tried}]
     2: tested and proved to be reliable [syn: {tested}, {time-tested},
         {tried and true}]
