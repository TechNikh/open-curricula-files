---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/listening
offline_file: ""
offline_thumbnail: ""
uuid: 0963cb37-aa84-4024-a56a-f78a96360468
updated: 1484310585
title: listening
categories:
    - Dictionary
---
listening
     adj : attending to or alert for sound; "be wary of listening
           ears"; "government-maintained listening posts"
     n : the act of hearing attentively; "you can learn a lot by just
         listening"; "they make good music--you should give them a
         hearing" [syn: {hearing}]
