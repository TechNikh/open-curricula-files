---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centre_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 8f0a32d4-af16-4a4b-8d2e-4fdc16b99e5d
updated: 1484308925
title: Centre (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Circle-withsegments.svg.png
tags:
    - Circles, spheres, and segments
    - Symmetric objects
    - Triangles
    - Tangential polygons and cyclic polygons
    - General polygons
categories:
    - Elementary geometry
---
In geometry, a centre (or center) (from Greek κέντρον) of an object is a point in some sense in the middle of the object. According to the specific definition of centre taken into consideration, an object might have no centre. If geometry is regarded as the study of isometry groups then a centre is a fixed point of all the isometries which move the object onto itself.
