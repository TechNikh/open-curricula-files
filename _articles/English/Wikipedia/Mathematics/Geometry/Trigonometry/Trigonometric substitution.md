---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_substitution
offline_file: ""
offline_thumbnail: ""
uuid: c75daed5-9931-456e-8c4b-53d1522017f2
updated: 1484309348
title: Trigonometric substitution
tags:
    - Examples
    - Integrals containing a2 − x2
    - Integrals containing a2 + x2
    - Integrals containing x2 − a2
    - Substitutions that eliminate trigonometric functions
    - Hyperbolic substitution
categories:
    - Trigonometry
---
In mathematics, Trigonometric substitution is the substitution of trigonometric functions for other expressions. One may use the trigonometric identities to simplify certain integrals containing radical expressions:[1][2]
