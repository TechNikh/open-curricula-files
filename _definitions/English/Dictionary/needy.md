---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needy
offline_file: ""
offline_thumbnail: ""
uuid: dfb5a7c1-6b66-487d-8fd0-07f35d4b911f
updated: 1484310533
title: needy
categories:
    - Dictionary
---
needy
     adj : poor enough to need help from others [syn: {destitute}, {impoverished},
            {indigent}, {necessitous}, {poverty-stricken}]
     [also: {neediest}, {needier}]
