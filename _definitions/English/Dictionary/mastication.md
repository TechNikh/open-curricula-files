---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mastication
offline_file: ""
offline_thumbnail: ""
uuid: 74f0fefd-bcb1-4ec4-a243-98394f66b099
updated: 1484310339
title: mastication
categories:
    - Dictionary
---
mastication
     n : biting and grinding food in your mouth so it becomes soft
         enough to swallow [syn: {chew}, {chewing}, {manduction}]
