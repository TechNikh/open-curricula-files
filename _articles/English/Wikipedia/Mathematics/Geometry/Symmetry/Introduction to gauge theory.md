---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Introduction_to_gauge_theory
offline_file: ""
offline_thumbnail: ""
uuid: 37428e0d-c857-41f4-bcf8-53fbc13e8ffe
updated: 1484309312
title: Introduction to gauge theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Diffeomorphism_of_a_square.svg.png
tags:
    - History and importance
    - In classical physics
    - Electromagnetism
    - General relativity
    - 'An example of a symmetry in a physical theory: translation invariance'
    - "Another example of a symmetry: the invariance of Einstein's field equation under arbitrary coordinate transformations"
    - In quantum mechanics
    - Quantum electrodynamics
    - Aharonov–Bohm experiment
    - Explanation with potentials
    - 'Gauge invariance: the results of the experiments are independent of the choice of the gauge for the potentials'
    - Summary
    - Types of gauge symmetries
    - Gauge bosons
categories:
    - Symmetry
---
A gauge theory is a type of theory in physics. Modern theories describe physical forces in terms of fields, e.g., the electromagnetic field, the gravitational field, and fields that describe forces between the elementary particles. A general feature of these field theories is that the fundamental fields cannot be directly measured; however, some associated quantities can be measured, such as charges, energies, and velocities. In field theories, different configurations of the unobservable fields can result in identical observable quantities. A transformation from one such field configuration to another is called a gauge transformation;[1][2] the lack of change in the measurable quantities, ...
