---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connective_constant
offline_file: ""
offline_thumbnail: ""
uuid: 1e9e30ca-4863-4888-8b6d-650a71fbb0e8
updated: 1484308601
title: Connective constant
tags:
    - Definition
    - 'Known values[5]'
    - Duminil-Copin–Smirnov proof
    - Conjectures
categories:
    - Discrete geometry
---
In mathematics, the connective constant is a numerical quantity associated with self-avoiding walks on a lattice. It is studied in connection with the notion of universality in two-dimensional statistical physics models.[1] While the connective constant depends on the choice of lattice so itself is not universal (similarly to other lattice-dependent quantities such as the critical probability threshold for percolation), it is nonetheless an important quantity that appears in conjectures for universal laws. Furthermore, the mathematical techniques used to understand the connective constant, for example in the recent rigorous proof by Duminil-Copin and Smirnov that the connective constant of ...
