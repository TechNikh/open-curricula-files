---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jonathan_Schaeffer
offline_file: ""
offline_thumbnail: ""
uuid: 6da76806-b0ba-48ee-aa86-6beef7c89db0
updated: 1484309026
title: Jonathan Schaeffer
tags:
    - Early life
    - 'Draughts: Chinook'
    - 'Poker: Polaris'
    - Currently
categories:
    - Game theorists
---
Jonathan Herbert Schaeffer (born 1957) is a Canadian researcher and professor at the University of Alberta and the Canada Research Chair in Artificial Intelligence.
