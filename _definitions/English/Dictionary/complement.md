---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484386921
title: complement
categories:
    - Dictionary
---
complement
     n 1: a word or phrase used to complete a grammatical construction
     2: a complete number or quantity; "a full complement"
     3: number needed to make up whole force; "a full complement of
        workers" [syn: {full complement}]
     4: something added to complete or make perfect; "a fine wine is
        a perfect complement to the dinner"
     5: one of a series of enzymes in the blood serum that are part
        of the immune response
     6: either of two parts that mutually complete each other
     v : make complete or perfect; supply what is wanting or form the
         complement to; "I need some pepper to complement the
         sweet touch in the soup"
