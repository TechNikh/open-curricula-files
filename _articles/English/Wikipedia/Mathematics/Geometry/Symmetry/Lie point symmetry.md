---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_point_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: bd7031e8-0da4-48f1-aed8-2eeec797b9f6
updated: 1484309312
title: Lie point symmetry
tags:
    - Overview
    - Types of symmetries
    - Applications
    - Geometrical framework
    - Infinitesimal approach
    - Lie groups and Lie algebras of infinitesimal generators
    - Continuous dynamical systems
    - Invariants
    - Definition of Lie point symmetries
    - Lie point symmetries of algebraic systems
    - Algebraic systems
    - Definition of Lie point symmetries
    - Example
    - Lie point symmetries of dynamical systems
    - Systems of ODEs and associated infinitesimal generators
    - Definition of Lie point symmetries
    - Example
    - Software
categories:
    - Symmetry
---
Towards the end of the nineteenth century, Sophus Lie introduced the notion of Lie group in order to study the solutions of ordinary differential equations[1][2][3] (ODEs). He showed the following main property: the order of an ordinary differential equation can be reduced by one if it is invariant under one-parameter Lie group of point transformations.[4] This observation unified and extended the available integration techniques. Lie devoted the remainder of his mathematical career to developing these continuous groups that have now an impact on many areas of mathematically based sciences. The applications of Lie groups to differential systems were mainly established by Lie and Emmy ...
