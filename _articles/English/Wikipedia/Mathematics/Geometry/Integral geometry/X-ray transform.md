---
version: 1
type: article
id: https://en.wikipedia.org/wiki/X-ray_transform
offline_file: ""
offline_thumbnail: ""
uuid: 042fb561-66c8-4630-ac4a-8193e893fc19
updated: 1484309255
title: X-ray transform
categories:
    - Integral geometry
---
In mathematics, the X-ray transform (also called John transform) is an integral transform introduced by Fritz John in 1938[1] that is one of the cornerstones of modern integral geometry. It is very closely related to the Radon transform, and coincides with it in two dimensions. In higher dimensions, the X-ray transform of a function is defined by integrating over lines rather than over hyperplanes as in the Radon transform. The X-ray transform derives its name from X-ray tomography because the X-ray transform of a function ƒ represents the scattering data of a tomographic scan through an inhomogeneous medium whose density is represented by the function ƒ. Inversion of the X-ray transform ...
