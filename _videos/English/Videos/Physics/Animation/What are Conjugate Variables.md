---
version: 1
type: video
provider: YouTube
id: DVGXGpYyUwU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 62f594b5-665b-47ff-942d-af587e02aa85
updated: 1486069653
title: What are Conjugate Variables
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DVGXGpYyUwU/default.jpg
    - https://i3.ytimg.com/vi/DVGXGpYyUwU/1.jpg
    - https://i3.ytimg.com/vi/DVGXGpYyUwU/2.jpg
    - https://i3.ytimg.com/vi/DVGXGpYyUwU/3.jpg
---
