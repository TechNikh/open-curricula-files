---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quintals
offline_file: ""
offline_thumbnail: ""
uuid: b7a410b4-2a99-484d-8d8b-170ae1925793
updated: 1484308458
title: Quintal
tags:
    - Name
    - Pound-based vs. kilogram-based
    - English use
categories:
    - Uncategorized
---
The quintal or centner is a historical unit of mass in many countries which is usually defined as 100 base units of either pounds or kilograms. In British English, it referred to the hundredweight; in American English, it formerly referred to an uncommon measure of 100 kilograms.
