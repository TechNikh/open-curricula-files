---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tiny_and_miny
offline_file: ""
offline_thumbnail: ""
uuid: 7a1fae75-6b74-4444-aee0-44208aa0446d
updated: 1484308533
title: Tiny and miny
categories:
    - Combinatorial game theory
---
In mathematics, tiny and miny are operators that yield infinitesimal values when applied to numbers in combinatorial game theory. Given a positive number G, tiny G (denoted by ⧾G in many texts) is equal to {0||0|-G} for any game G, whereas miny G (analogously denoted ⧿G) is tiny G’s negative, or {G|0||0}.
