---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/albumen
offline_file: ""
offline_thumbnail: ""
uuid: 6664b671-d265-4e75-9ddc-ca4d04e49459
updated: 1484310303
title: albumen
categories:
    - Dictionary
---
albumen
     n 1: a simple water-soluble protein found in many animal tissues
          and liquids [syn: {albumin}]
     2: the white of an egg; the nutritive and protective gelatinous
        substance surrounding the yolk consisting mainly of
        albumin dissolved in water [syn: {egg white}, {ovalbumin}]
