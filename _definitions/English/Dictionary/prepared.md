---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prepared
offline_file: ""
offline_thumbnail: ""
uuid: 2a46d60b-157b-4b3e-b044-f281b220b813
updated: 1484310373
title: prepared
categories:
    - Dictionary
---
prepared
     adj 1: made ready or fit or suitable beforehand; "a prepared
            statement"; "be prepared for emergencies" [ant: {unprepared}]
     2: having made preparations; "prepared to take risks" [syn: {disposed(p)},
         {fain}, {inclined(p)}]
     3: equipped or prepared with necessary intellectual resources;
        "graduates well equipped to handle such problems";
        "equipped to be a scholar"
