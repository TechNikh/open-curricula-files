---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalised_metric
offline_file: ""
offline_thumbnail: ""
uuid: d23bb3ff-e8c5-4483-9fb6-40f1130eb48b
updated: 1484309265
title: Generalised metric
categories:
    - Metric geometry
---
In mathematics, the concept of a generalised metric is a generalisation of that of a metric, in which the distance is not a real number but taken from an arbitrary ordered field.
