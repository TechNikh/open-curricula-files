---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/definite
offline_file: ""
offline_thumbnail: ""
uuid: 98e912cf-75e7-4ce1-9dd3-0c7880c95931
updated: 1484310224
title: definite
categories:
    - Dictionary
---
definite
     adj 1: precise; explicit and clearly defined; "I want a definite
            answer"; "a definite statement of the terms of the
            will"; "a definite amount"; "definite restrictions on
            the sale of alcohol"; "the wedding date is now
            definite"; "a definite drop in attendance" [ant: {indefinite}]
     2: known for certain; "it is definite that they have won"
