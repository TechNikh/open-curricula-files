---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemical_computer
offline_file: ""
offline_thumbnail: ""
uuid: 5823f67e-7092-4644-88d3-68b102ff7432
updated: 1484308667
title: Chemical computer
tags:
    - Background
    - Current research
categories:
    - Theoretical computer science
---
A chemical computer, also called reaction-diffusion computer, BZ computer (stands for Belousov–Zhabotinsky computer) or gooware computer is an unconventional computer based on a semi-solid chemical "soup" where data are represented by varying concentrations of chemicals.[1] The computations are performed by naturally occurring chemical reactions. Chemical computing in today's world mainly refers to the BZ reaction-diffusion model.
