---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spine
offline_file: ""
offline_thumbnail: ""
uuid: 2aec7545-eaae-4979-92d5-7dd5953ca8ef
updated: 1484310168
title: spine
categories:
    - Dictionary
---
spine
     n 1: the series of vertebrae forming the axis of the skeleton and
          protecting the spinal cord; "the fall broke his back"
          [syn: {spinal column}, {vertebral column}, {backbone}, {back},
           {rachis}]
     2: any pointed projection [syn: {spur}]
     3: a sharp-pointed tip on a stem or leaf [syn: {thorn}, {prickle},
         {pricker}, {sticker}]
     4: a sharp rigid animal process or appendage; as a porcupine
        quill or a ridge on a bone or a ray of a fish fin
