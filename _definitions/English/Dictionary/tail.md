---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tail
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383021
title: tail
categories:
    - Dictionary
---
tail
     n 1: the posterior part of the body of a vertebrate especially
          when elongated and extending beyond the trunk or main
          part of the body
     2: the time of the last part of something; "the fag end of this
        crisis-ridden century"; "the tail of the storm" [syn: {fag
        end}, {tail end}]
     3: any projection that resembles the tail of an animal [syn: {tail
        end}]
     4: the fleshy part of the human body that you sit on; "he
        deserves a good kick in the butt"; "are you going to sit
        on your fanny and do nothing?" [syn: {buttocks}, {nates},
        {arse}, {butt}, {backside}, {bum}, {buns}, {can}, {fundament},
         ...
