---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allow
offline_file: ""
offline_thumbnail: ""
uuid: 7f88dd50-dfdc-47d8-977e-b7af91e23f24
updated: 1484310208
title: allow
categories:
    - Dictionary
---
allow
     v 1: make it possible through a specific action or lack of action
          for something to happen; "This permits the water to rush
          in"; "This sealed door won't allow the water come into
          the basement"; "This will permit the rain to run off"
          [syn: {let}, {permit}] [ant: {prevent}]
     2: consent to, give permission; "She permitted her son to visit
        her estranged husband"; "I won't let the police search her
        basement"; "I cannot allow you to see your exam" [syn: {permit},
         {let}, {countenance}] [ant: {forbid}, {forbid}]
     3: let have; "grant permission"; "Mandela was allowed few
        visitors in prison" [syn: {grant}] ...
