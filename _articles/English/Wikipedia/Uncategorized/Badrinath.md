---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Badrinath
offline_file: ""
offline_thumbnail: ""
uuid: f66e04ec-3ec9-434b-90b6-71701d2d67bb
updated: 1484308435
title: Badrinath
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Badrinath_Temple_-_OCT_2014.jpg
tags:
    - Name
    - History
    - Temple
    - Legend
    - geography
    - Demographics
    - Citations
    - Bibliography
categories:
    - Uncategorized
---
Badrinath is a holy town and a nagar panchayat in Chamoli district in the state of Uttarakhand, India. It is the most important of the four sites in India's Char Dham pilgrimage and gets its name from the temple of Badrinath.
