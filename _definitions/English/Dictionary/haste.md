---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haste
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456941
title: haste
categories:
    - Dictionary
---
haste
     n 1: overly eager speed (and possible carelessness); "he soon
          regretted his haste" [syn: {hastiness}, {hurry}, {hurriedness},
           {precipitation}]
     2: the act of moving hurriedly and in a careless manner; "in
        his haste to leave he forgot his book" [syn: {hurry}, {rush},
         {rushing}]
     3: a condition of urgency making it necessary to hurry; "in a
        hurry to lock the door" [syn: {hurry}]
