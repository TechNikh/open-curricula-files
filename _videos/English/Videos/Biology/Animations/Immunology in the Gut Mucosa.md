---
version: 1
type: video
provider: YouTube
id: gnZEge78_78
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c497cd10-9776-4d69-a6e8-05e962cf860e
updated: 1486069669
title: Immunology in the Gut Mucosa
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/gnZEge78_78/default.jpg
    - https://i3.ytimg.com/vi/gnZEge78_78/1.jpg
    - https://i3.ytimg.com/vi/gnZEge78_78/2.jpg
    - https://i3.ytimg.com/vi/gnZEge78_78/3.jpg
---
