---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mislead
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421481
title: mislead
categories:
    - Dictionary
---
mislead
     v 1: lead someone in the wrong direction or give someone wrong
          directions; "The pedestrian misdirected the out-of-town
          driver" [syn: {misdirect}, {misguide}, {lead astray}]
     2: give false or misleading information to [syn: {misinform}]
     [also: {misled}]
