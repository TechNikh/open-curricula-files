---
version: 1
type: video
provider: YouTube
id: 4u8_AMacu-Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebraic%20Long%20Division.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2af5242c-7640-4c78-abbe-0e3aa1d57fc3
updated: 1486069571
title: Algebraic Long Division
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/4u8_AMacu-Y/default.jpg
    - https://i3.ytimg.com/vi/4u8_AMacu-Y/1.jpg
    - https://i3.ytimg.com/vi/4u8_AMacu-Y/2.jpg
    - https://i3.ytimg.com/vi/4u8_AMacu-Y/3.jpg
---
Dividing one polynomial into another
