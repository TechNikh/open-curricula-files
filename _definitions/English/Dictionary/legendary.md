---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legendary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557501
title: legendary
categories:
    - Dictionary
---
legendary
     adj 1: so celebrated as to having taken on the nature of a legend;
            "the legendary exploits of the arctic trailblazers"
     2: celebrated in fable or legend; "the fabled Paul Bunyan and
        his blue ox"; "legendary exploits of Jesse James" [syn: {fabled}]
