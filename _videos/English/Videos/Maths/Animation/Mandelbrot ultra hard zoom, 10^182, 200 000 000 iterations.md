---
version: 1
type: video
provider: YouTube
id: p1erDQbXWwQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 64e87d1c-bc21-4089-91c1-a64e9a50bf27
updated: 1486069645
title: Mandelbrot ultra hard zoom, 10^182, 200 000 000 iterations
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/p1erDQbXWwQ/default.jpg
    - https://i3.ytimg.com/vi/p1erDQbXWwQ/1.jpg
    - https://i3.ytimg.com/vi/p1erDQbXWwQ/2.jpg
    - https://i3.ytimg.com/vi/p1erDQbXWwQ/3.jpg
---
