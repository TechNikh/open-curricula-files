---
version: 1
type: video
provider: YouTube
id: z4M1Rr3SWIg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4f9e51aa-7114-4ef3-9418-9807ae1bac99
updated: 1486069658
title: Make A Cloud Disappear!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/z4M1Rr3SWIg/default.jpg
    - https://i3.ytimg.com/vi/z4M1Rr3SWIg/1.jpg
    - https://i3.ytimg.com/vi/z4M1Rr3SWIg/2.jpg
    - https://i3.ytimg.com/vi/z4M1Rr3SWIg/3.jpg
---
