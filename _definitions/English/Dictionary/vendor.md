---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vendor
offline_file: ""
offline_thumbnail: ""
uuid: c0a3bc13-785c-4cc1-b1dc-f481474767cf
updated: 1484310451
title: vendor
categories:
    - Dictionary
---
vendor
     n : someone who promotes or exchanges goods or services for
         money [syn: {seller}, {marketer}, {vender}, {trafficker}]
