---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wailing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580181
title: wailing
categories:
    - Dictionary
---
wailing
     adj : vocally expressing grief or sorrow or resembling such
           expression; "lamenting sinners"; "wailing mourners";
           "the wailing wind"; "wailful bagpipes"; "tangle her
           desires with wailful sonnets"- Shakespeare [syn: {lamenting},
            {wailful}]
     n : loud cries made while weeping [syn: {bawling}]
