---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coordination
offline_file: ""
offline_thumbnail: ""
uuid: 1bf1c488-02e5-48d6-ab2c-ce13ffc6eed8
updated: 1484310363
title: coordination
categories:
    - Dictionary
---
coordination
     n 1: the skillful and effective interaction of movements [ant: {incoordination}]
     2: the regulation of diverse elements into an integrated and
        harmonious operation
     3: the grammatical relation of two constituents having the same
        grammatical form
