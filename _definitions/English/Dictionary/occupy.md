---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occupy
offline_file: ""
offline_thumbnail: ""
uuid: 8690e5ad-dc3c-4c89-9348-5890c05cefe9
updated: 1484310389
title: occupy
categories:
    - Dictionary
---
occupy
     v 1: be present in; be inside of [syn: {inhabit}]
     2: keep busy with; "She busies herself with her butterfly
        collection" [syn: {busy}]
     3: live (in a certain place) [syn: {reside}, {lodge in}]
     4: occupy the whole of; "The liquid fills the container" [syn:
        {fill}]
     5: be on the mind of; "I worry about the second Germanic
        consonant" [syn: {concern}, {interest}, {worry}]
     6: as of time or space; "It took three hours to get to work
        this morning"; "This event occupied a very short time"
        [syn: {take}, {use up}]
     7: march aggressively into another's territory by military
        force for the purposes of conquest and ...
