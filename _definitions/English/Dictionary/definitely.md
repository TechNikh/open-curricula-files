---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/definitely
offline_file: ""
offline_thumbnail: ""
uuid: 5bfc412e-0898-4240-ba40-9952b30d14cb
updated: 1484310395
title: definitely
categories:
    - Dictionary
---
definitely
     adv : without question and beyond doubt; "it was decidedly too
           expensive"; "she told him off in spades"; "by all odds
           they should win" [syn: {decidedly}, {unquestionably}, {emphatically},
            {in spades}, {by all odds}]
