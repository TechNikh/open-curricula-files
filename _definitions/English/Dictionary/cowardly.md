---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cowardly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412841
title: cowardly
categories:
    - Dictionary
---
cowardly
     adj : lacking courage; ignobly timid and faint-hearted; "cowardly
           dogs, ye will not aid me then"- P.B.Shelley [syn: {fearful}]
           [ant: {brave}]
