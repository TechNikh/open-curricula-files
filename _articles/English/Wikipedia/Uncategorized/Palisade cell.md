---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Palisade_parenchyma
offline_file: ""
offline_thumbnail: ""
uuid: 25b1e107-1c04-463e-a63d-56fde35230af
updated: 1484308375
title: Palisade cell
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Leaf_Tissue_Structure.svg.png
categories:
    - Uncategorized
---
Palisade cells are plant cells located within the mesophyll in leaves, right below the upper epidermis and cuticle. They are vertically elongated, a different shape from the spongy mesophyll cells beneath them in the leaf. Their chloroplasts absorb a major portion of the light energy used by the leaf. Palisade cells occur in dicotyledonous plants, and also in the net-veined monocots, the Araceae and Dioscoreaceae.[1]
