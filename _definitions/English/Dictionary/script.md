---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/script
offline_file: ""
offline_thumbnail: ""
uuid: 797972da-7562-44bd-8802-c0e7e3f34aaf
updated: 1484310569
title: script
categories:
    - Dictionary
---
script
     n 1: a written version of a play or other dramatic composition;
          used in preparing for a performance [syn: {book}, {playscript}]
     2: something written by hand; "she recognized his handwriting";
        "his hand was illegible" [syn: {handwriting}, {hand}]
     3: a particular orthography or writing system
     v : write a script for; "The playwright scripted the movie"
