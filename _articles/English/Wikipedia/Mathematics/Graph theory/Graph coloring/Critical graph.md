---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_graph
offline_file: ""
offline_thumbnail: ""
uuid: 9de164c3-91f2-40cd-834e-c90143abca32
updated: 1484309376
title: Critical graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Critical_graph_sample.svg.png
categories:
    - Graph coloring
---
In general the notion of criticality can refer to any measure. But in graph theory, when the term is used without any qualification, it almost always refers to the chromatic number of a graph. Critical graphs are interesting because they are the minimal members in terms of chromatic number, which is a very important measure in graph theory. More precise definitions follow.
