---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Reversible_reference_system_propagation_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 65768ed1-04c1-41e3-9c29-88a9184b6d8a
updated: 1484308794
title: Reversible reference system propagation algorithm
categories:
    - Hamiltonian mechanics
---
It evolves the system state over time,
