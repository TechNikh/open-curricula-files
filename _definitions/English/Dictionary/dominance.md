---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominance
offline_file: ""
offline_thumbnail: ""
uuid: 633f47a6-0c2b-4808-99be-5fbc3bbf7627
updated: 1484310301
title: dominance
categories:
    - Dictionary
---
dominance
     n 1: superior development of one side of the body [syn: {laterality}]
     2: the state that exists when one person or group has power
        over another; "her apparent dominance of her husband was
        really her attempt to make him pay attention to her" [syn:
         {ascendance}, {ascendence}, {ascendancy}, {ascendency}, {control}]
     3: the power or right to give orders or make decisions; "he has
        the authority to issue warrants"; "deputies are given
        authorization to make arrests" [syn: {authority}, {authorization},
         {authorisation}, {say-so}]
