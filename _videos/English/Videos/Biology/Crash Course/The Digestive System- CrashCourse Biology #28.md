---
version: 1
type: video
provider: YouTube
id: s06XzaKqELk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7306349a-a3f0-464f-b368-6bc0aca6da0e
updated: 1486069678
title: 'The Digestive System- CrashCourse Biology #28'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/s06XzaKqELk/default.jpg
    - https://i3.ytimg.com/vi/s06XzaKqELk/1.jpg
    - https://i3.ytimg.com/vi/s06XzaKqELk/2.jpg
    - https://i3.ytimg.com/vi/s06XzaKqELk/3.jpg
---
