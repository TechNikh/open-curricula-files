---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dihybrid
offline_file: ""
offline_thumbnail: ""
uuid: a377f4d8-2667-431f-aeda-51039d8fabcc
updated: 1484310275
title: dihybrid
categories:
    - Dictionary
---
dihybrid
     n : a hybrid produced by parents that differ only at two gene
         loci that have two alleles each
