---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wandering_set
offline_file: ""
offline_thumbnail: ""
uuid: e0d26282-2c2b-4da6-b098-72fc31603818
updated: 1484308777
title: Wandering set
tags:
    - Wandering points
    - Non-wandering points
    - Wandering sets and dissipative systems
categories:
    - Ergodic theory
---
In those branches of mathematics called dynamical systems and ergodic theory, the concept of a wandering set formalizes a certain idea of movement and mixing in such systems. When a dynamical system has a wandering set of non-zero measure, then the system is a dissipative system. This is very much the opposite of a conservative system, for which the ideas of the Poincaré recurrence theorem apply. Intuitively, the connection between wandering sets and dissipation is easily understood: if a portion of the phase space "wanders away" during normal time-evolution of the system, and is never visited again, then the system is dissipative. The language of wandering sets can be used to give a ...
