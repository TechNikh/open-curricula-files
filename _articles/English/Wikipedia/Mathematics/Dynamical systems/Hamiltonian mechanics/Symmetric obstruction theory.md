---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_obstruction_theory
offline_file: ""
offline_thumbnail: ""
uuid: f9ee3c44-e672-4fcc-8b0a-f0b0b2d1c8f8
updated: 1484308791
title: Symmetric obstruction theory
categories:
    - Hamiltonian mechanics
---
In mathematics, a symmetric obstruction theory, introduced by Kai Behrend, is a perfect obstruction theory together with nondegenerate symmetric bilinear form.
