---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ornstein_isomorphism_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 20fc4e7f-08ba-4300-8d40-38c332cb6d38
updated: 1484308782
title: Ornstein isomorphism theorem
tags:
    - Discussion
    - History
categories:
    - Ergodic theory
---
In mathematics, the Ornstein isomorphism theorem is a deep result for ergodic theory. It states that if two different Bernoulli schemes have the same Kolmogorov entropy, then they are isomorphic.[1][2] The result, given by Donald Ornstein in 1970, is important because it states that many systems previously believed to be unrelated are in fact isomorphic; these include all finite stationary stochastic processes, subshifts of finite type and Markov shifts, Anosov flows and Sinai's billiards, ergodic automorphisms of the n-torus, and the continued fraction transform.
