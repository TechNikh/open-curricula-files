---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metalloid
offline_file: ""
offline_thumbnail: ""
uuid: 964a189c-0db1-476b-a687-433c6cb16dc6
updated: 1484310403
title: metalloid
categories:
    - Dictionary
---
metalloid
     adj : of or being a nonmetallic element that has some of the
           properties of metal; "arsenic is a metalloid element"
