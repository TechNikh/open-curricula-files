---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convey
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464621
title: convey
categories:
    - Dictionary
---
convey
     v 1: make known; pass on, of information [syn: {impart}]
     2: serve as a means for expressing something; "The painting of
        Mary carries motherly love"; "His voice carried a lot af
        anger" [syn: {carry}, {express}]
     3: transfer to another; "communicate a disease" [syn: {transmit},
         {communicate}]
     4: transmit a title or property
     5: transmit or serve as the medium for transmission; "Sound
        carries well over water"; "The airwaves carry the sound";
        "Many metals conduct heat" [syn: {conduct}, {transmit}, {carry},
         {channel}]
     6: take something or somebody with oneself somewhere; "Bring me
        the box from the other ...
