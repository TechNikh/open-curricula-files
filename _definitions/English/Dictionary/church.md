---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/church
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423101
title: church
categories:
    - Dictionary
---
church
     n 1: one of the groups of Christians who have their own beliefs
          and forms of worship [syn: {Christian church}]
     2: a place for public (especially Christian) worship; "the
        church was empty" [syn: {church building}]
     3: a service conducted in a church; "don't be late for church"
        [syn: {church service}]
     4: the body of people who attend or belong to a particular
        local church; "our church is hosting a picnic next week"
     v : perform a special church rite or service for; "church a
         woman after childbirth"
