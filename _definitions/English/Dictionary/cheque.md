---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheque
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499661
title: cheque
categories:
    - Dictionary
---
cheque
     n : a written order directing a bank to pay money; "he paid all
         his bills by check" [syn: {check}, {bank check}]
     v : withdraw money by writing a check [syn: {check out}]
