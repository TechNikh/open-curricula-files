---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/untimely
offline_file: ""
offline_thumbnail: ""
uuid: b617c583-ab73-43a2-8a0c-fb2ecee70975
updated: 1484310605
title: untimely
categories:
    - Dictionary
---
untimely
     adj 1: badly timed; "an ill-timed intervention"; "you think my
            intrusion unseasonable"; "an untimely remark"; "it was
            the wrong moment for a joke" [syn: {ill-timed(a)}, {ill
            timed(p)}, {unseasonable}, {wrong}]
     2: uncommonly early or before the expected time; "illness led
        to his premature death"; "alcohol brought him to an
        untimely end" [syn: {premature}]
     adv : too soon; in a premature manner; "I spoke prematurely" [syn:
            {prematurely}]
