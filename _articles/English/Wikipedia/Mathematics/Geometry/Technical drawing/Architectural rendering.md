---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Architectural_rendering
offline_file: ""
offline_thumbnail: ""
uuid: 4b19bb97-5368-4354-8603-397145bb6308
updated: 1484309322
title: Architectural rendering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Canada_Permanent_Building.jpg
tags:
    - Computer-generated renderings
    - Hand-drawn renderings or architectural illustration
    - Awards
    - Education
categories:
    - Technical drawing
---
Architectural rendering, or architectural illustration, is the art of creating two-dimensional images or animations showing the attributes of a proposed architectural design.
