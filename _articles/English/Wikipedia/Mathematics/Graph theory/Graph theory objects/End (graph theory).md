---
version: 1
type: article
id: https://en.wikipedia.org/wiki/End_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: d47fae7d-2923-4ab0-bbbb-6a22c1247276
updated: 1484309445
title: End (graph theory)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Typy_kultury_organizacyjnej%252C_Ateny_II_%2528ubt%2529.svg.png'
tags:
    - Definition and characterization
    - Examples
    - Relation to topological ends
    - Special kinds of ends
    - Free ends
    - Thick ends
    - Special kinds of graphs
    - Symmetric and almost-symmetric graphs
    - Cayley graphs
    - Notes
categories:
    - Graph theory objects
---
In the mathematics of infinite graphs, an end of a graph represents, intuitively, a direction in which the graph extends to infinity. Ends may be formalized mathematically as equivalence classes of infinite paths, as havens describing strategies for pursuit-evasion games on the graph, or (in the case of locally finite graphs) as topological ends of topological spaces associated with the graph.
