---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depression
offline_file: ""
offline_thumbnail: ""
uuid: cd31bd3b-47bf-458c-a49c-a1bbedce213f
updated: 1484310551
title: depression
categories:
    - Dictionary
---
depression
     n 1: a mental state characterized by a pessimistic sense of
          inadequacy and a despondent lack of activity [ant: {elation}]
     2: a long-term economic state characterized by unemployment and
        low prices and low levels of trade and investment [syn: {slump},
         {economic crisis}]
     3: a sunken or depressed geological formation [syn: {natural
        depression}] [ant: {natural elevation}]
     4: sad feelings of gloom and inadequacy
     5: a time period during the 1930s when there was a worldwide
        economic depression and mass unemployment [syn: {the
        Depression}, {the Great Depression}]
     6: an air mass of lower pressure; often ...
