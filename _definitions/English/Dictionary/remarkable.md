---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remarkable
offline_file: ""
offline_thumbnail: ""
uuid: b542e6f4-3072-4dc5-9c5e-9af4c694c69b
updated: 1484310284
title: remarkable
categories:
    - Dictionary
---
remarkable
     adj 1: unusual or striking; "a remarkable sight"; "such poise is
            singular in one so young" [syn: {singular}]
     2: worthy of notice; "a noteworthy fact is that her students
        rarely complain"; "a remarkable achievement" [syn: {noteworthy}]
