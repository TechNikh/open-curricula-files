---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Injective_sheaf
offline_file: ""
offline_thumbnail: ""
uuid: 7f9a4385-0959-40a0-a776-dde0be291aba
updated: 1484309156
title: Injective sheaf
tags:
    - Injective sheaves
    - Acyclic sheaves
    - Fine sheaves
    - Soft sheaves
    - Flasque or flabby sheaves
categories:
    - Algebraic geometry
---
In mathematics, injective sheaves of abelian groups are used to construct the resolutions needed to define sheaf cohomology (and other derived functors, such as sheaf Ext).
