---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/node
offline_file: ""
offline_thumbnail: ""
uuid: 39e94755-19dc-4d9c-8947-e72c608f344c
updated: 1484310162
title: node
categories:
    - Dictionary
---
node
     n 1: a connecting point at which several lines come together
     2: any thickened enlargement [syn: {knob}, {thickening}]
     3: (physics) the point of minimum displacement in a periodic
        system [ant: {antinode}]
     4: (astronomy) a point where an orbit crosses a plane
     5: the source of lymph and lymphocytes [syn: {lymph node}, {lymph
        gland}]
     6: any bulge or swelling of an anatomical structure or part
     7: (computer science) any computer that is hooked up to a
        computer network [syn: {client}, {guest}]
