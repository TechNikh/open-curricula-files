---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/requested
offline_file: ""
offline_thumbnail: ""
uuid: 2d9b2727-347c-4a92-a5bf-fafae4d15730
updated: 1484310607
title: requested
categories:
    - Dictionary
---
requested
     adj : asked for; "the requested aid is forthcoming" [ant: {unrequested}]
