---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lanka
offline_file: ""
offline_thumbnail: ""
uuid: b27c689a-de57-44ad-8581-0aff17a8b8e0
updated: 1484308426
title: Lanka
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-The_golden_abode_of_King_Ravana_India.jpg
tags:
    - Rulers of Lanka
    - Lanka, Sri Lanka, and possible locations
    - Description
    - References to Lanka in the Mahabharata
    - "Sahadeva's expedition to South"
    - "Presence of the King of Lanka in Yudhishthira's Rajasuya"
    - Other fragmentary references
categories:
    - Uncategorized
---
Lanka /ˈləŋkɑː/ is the name given in Hindu mythology to the island fortress capital of the legendary demon king Ravana in the epics of the Ramayana and the Mahabharata. The fortress was situated on a plateau between three mountain peaks known as the Trikuta Mountains. The ancient city of Lankapura is thought to have been burnt down by Hanuman. After its king, Ravana, was killed by Rama with the help of his brother Vibhishana, the latter was crowned king of Lankapura. The mythological site of Lankā is identified with Sri Lanka. His descendants were said to still rule the kingdom during the period of the Pandavas. According to the Mahabharata, the Pandava Sahadeva visited this kingdom ...
