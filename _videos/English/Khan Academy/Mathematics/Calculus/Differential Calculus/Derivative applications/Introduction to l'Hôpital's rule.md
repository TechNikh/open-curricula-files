---
version: 1
type: video
provider: YouTube
id: PdSzruR5OeE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/Introduction%20to%20l%27Ho%CC%82pital%27s%20rule.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 83698240-20ab-4a69-9706-885e1a7d0f1b
updated: 1486069693
title: "Introduction to l'Hôpital's rule"
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/PdSzruR5OeE/default.jpg
    - https://i3.ytimg.com/vi/PdSzruR5OeE/1.jpg
    - https://i3.ytimg.com/vi/PdSzruR5OeE/2.jpg
    - https://i3.ytimg.com/vi/PdSzruR5OeE/3.jpg
---
