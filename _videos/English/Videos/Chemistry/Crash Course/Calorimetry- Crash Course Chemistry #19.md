---
version: 1
type: video
provider: YouTube
id: JuWtBR-rDQk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Calorimetry-%20Crash%20Course%20Chemistry%20%2319.webm"
offline_file: ""
offline_thumbnail: ""
uuid: afc7c996-b52f-47c9-b2fa-25d13d87c353
updated: 1486069680
title: 'Calorimetry- Crash Course Chemistry #19'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/JuWtBR-rDQk/default.jpg
    - https://i3.ytimg.com/vi/JuWtBR-rDQk/1.jpg
    - https://i3.ytimg.com/vi/JuWtBR-rDQk/2.jpg
    - https://i3.ytimg.com/vi/JuWtBR-rDQk/3.jpg
---
