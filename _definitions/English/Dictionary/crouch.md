---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crouch
offline_file: ""
offline_thumbnail: ""
uuid: b9c4c3fc-965b-4d3a-8c92-9bf51ab96a23
updated: 1484310148
title: crouch
categories:
    - Dictionary
---
crouch
     n : the act of bending low with the limbs close to the body
     v 1: bend one's back forward from the waist on down; "he crouched
          down"; "She bowed before the Queen"; "The young man
          stooped to pick up the girl's purse" [syn: {stoop}, {bend},
           {bow}]
     2: sit on one's heels; "In some cultures, the women give birth
        while squatting"; "The children hunkered down to protect
        themselves from the sandstorm" [syn: {squat}, {scrunch}, {scrunch
        up}, {hunker}, {hunker down}]
