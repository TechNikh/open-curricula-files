---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/award
offline_file: ""
offline_thumbnail: ""
uuid: 90764c2a-cbe0-4931-b794-2f730a70ecc2
updated: 1484310429
title: award
categories:
    - Dictionary
---
award
     n 1: a grant made by a law court; "he criticized the awarding of
          compensation by the court" [syn: {awarding}]
     2: a tangible symbol signifying approval or distinction; "an
        award for bravery" [syn: {accolade}, {honor}, {honour}, {laurels}]
     3: something given for victory or superiority in a contest or
        competition or for winning a lottery; "the prize was a
        free trip to Europe" [syn: {prize}]
     v 1: give, especially as a reward; "bestow honors and prizes at
          graduation" [syn: {present}]
     2: give on the basis of merit; "Funds are granted to qualified
        researchers" [syn: {grant}]
     3: bestow an honor upon
