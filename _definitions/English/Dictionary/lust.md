---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lust
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604481
title: lust
categories:
    - Dictionary
---
lust
     n 1: a strong sexual desire [syn: {lecherousness}, {lustfulness}]
     2: self-indulgent sexual desire (personified as one of the
        deadly sins) [syn: {luxuria}]
     v : have a craving, appetite, or great desire for [syn: {crave},
          {hunger}, {thirst}, {starve}]
