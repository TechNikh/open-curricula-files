---
version: 1
type: video
provider: YouTube
id: yfR36PMWegg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Photosynthesis-%20%20Light%20Reactions%20and%20Photophosphorylation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 84c5d85c-bc78-4861-ad2e-60b41d2e5056
updated: 1486069585
title: 'Photosynthesis-  Light Reactions and Photophosphorylation'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/yfR36PMWegg/default.jpg
    - https://i3.ytimg.com/vi/yfR36PMWegg/1.jpg
    - https://i3.ytimg.com/vi/yfR36PMWegg/2.jpg
    - https://i3.ytimg.com/vi/yfR36PMWegg/3.jpg
---
More detail on the light reactions and photophorylation
More free lessons at: http://www.khanacademy.org/video?v=yfR36PMWegg
