---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cancer
offline_file: ""
offline_thumbnail: ""
uuid: 0e66a516-48f3-4c8f-a11d-99d07ce857df
updated: 1484310414
title: cancer
categories:
    - Dictionary
---
cancer
     n 1: any malignant growth or tumor caused by abnormal and
          uncontrolled cell division; it may spread to other parts
          of the body through the lymphatic system or the blood
          stream [syn: {malignant neoplastic disease}]
     2: (astrology) a person who is born while the sun is in Cancer
        [syn: {Crab}]
     3: a small zodiacal constellation in the northern hemisphere;
        between Leo and Gemini
     4: the fourth sign of the zodiac; the sun is in this sign from
        about June 21 to July 22 [syn: {Cancer the Crab}, {Crab}]
     5: type genus of the family Cancridae [syn: {genus Cancer}]
