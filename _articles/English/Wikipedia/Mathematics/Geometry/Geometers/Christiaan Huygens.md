---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Christiaan_Huygens
offline_file: ""
offline_thumbnail: ""
uuid: c66b0faa-354b-4915-a4a6-83eae1e0af80
updated: 1484309085
title: Christiaan Huygens
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Adriaen_Hanneman_-_Constantijn_Huygens_and_his-five-children.png
tags:
    - Early life
    - Student years
    - Early correspondence
    - Scientific debut
    - In France
    - Later life
    - Work in natural philosophy
    - Laws of motion, impact and gravitation
    - Optics
    - Horology
    - Pendulums
    - Balance spring watch
    - Astronomy
    - "Saturn's rings and Titan"
    - Mars and Syrtis Major
    - Cosmotheoros
    - Works
    - Portraits
    - During his lifetime
    - Named after Huygens
    - Science
    - Other
    - Notes
    - Primary sources, translations
    - Museums
    - Other
categories:
    - Geometers
---
Christiaan Huygens, FRS (/ˈhaɪɡənz/ or /ˈhɔɪɡənz/; Dutch: [ˈɦœyɣə(n)s] ( listen)) (Latin: Hugenius) (14 April 1629 – 8 July 1695) was a prominent Dutch mathematician and scientist. He is known particularly as an astronomer, physicist, probabilist and horologist.
