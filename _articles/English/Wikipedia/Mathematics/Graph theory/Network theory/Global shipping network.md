---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Global_shipping_network
offline_file: ""
offline_thumbnail: ""
uuid: df8140db-c25d-4a23-9b86-6980873450fa
updated: 1484309435
title: Global shipping network
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Container_ship_loading-700px.jpg
tags:
    - History
    - The network science perspective
    - Asymmetry
    - Clusters
    - Betweenness centrality
    - Different subnetworks
categories:
    - Network theory
---
The Global shipping network is the worldwide network of maritime traffic. From a network science perspective ports represent nodes and routes represent lines. Transportation networks have a crucial role in today’s economy, more precisely, maritime traffic is one of the most important drivers of global trade.
