---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purification
offline_file: ""
offline_thumbnail: ""
uuid: 7f25b83c-65d0-48b2-97c4-9770abd2e401
updated: 1484310407
title: purification
categories:
    - Dictionary
---
purification
     n 1: the act of cleaning by getting rid of impurities
     2: the process of removing impurities (as from oil or metals or
        sugar etc.) [syn: {refining}, {refinement}]
     3: a ceremonial cleansing from defilement or uncleanness by the
        performance of appropriate rites [syn: {purgation}]
     4: the act of purging of sin or guilt; moral or spiritual
        cleansing; "purification through repentance"
