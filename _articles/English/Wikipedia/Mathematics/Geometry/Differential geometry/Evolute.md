---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolute
offline_file: ""
offline_thumbnail: ""
uuid: 2caef891-62d7-4a2a-9b47-fef9a3e0cf07
updated: 1484309216
title: Evolute
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Evolute_of_an_ellipse.gif
tags:
    - History
    - Definition
    - General parameterizations
    - Properties
    - Relationship between a curve and its evolute
    - Radial curve
    - Examples
categories:
    - Differential geometry
---
In the differential geometry of curves, the evolute of a curve is the locus of all its centers of curvature. That is to say that when the center of curvature of each point on a curve is drawn, the resultant shape will be the evolute of that curve. The evolute of a circle is therefore a single point at its center.[1]
