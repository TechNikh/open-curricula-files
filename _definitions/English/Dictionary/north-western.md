---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/north-western
offline_file: ""
offline_thumbnail: ""
uuid: a72bdf09-6e4e-47b8-99ba-739c47b08d99
updated: 1484310589
title: north-western
categories:
    - Dictionary
---
northwestern
     adj 1: situated in or oriented toward the northwest [syn: {northwesterly},
             {northwest}]
     2: of a region of the United States generally including
        Washington; Oregon; Idaho; and sometimes Montana; Wyoming
