---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Signal-flow_graph
offline_file: ""
offline_thumbnail: ""
uuid: 154e2b4c-55ff-4fef-a129-ff2ff0ed94a5
updated: 1484309414
title: Signal-flow graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Flow_graphs.svg.png
tags:
    - History
    - Domain of application
    - Basic flow graph concepts
    - Choosing the variables
    - Non-uniqueness
    - Linear signal-flow graphs
    - Basic components
    - Systematic reduction to sources and sinks
    - Implementations
    - Solving linear equations
    - Putting the equations in "standard form"
    - "Applying Mason's gain formula"
    - Relation to block diagrams
    - "Interpreting 'causality'"
    - Signal-flow graphs for analysis and design
    - Signal-flow graphs for dynamic systems analysis
    - Signal-flow graphs for design synthesis
    - Shannon and Shannon-Happ formulas
    - Linear signal-flow graph examples
    - Simple voltage amplifier
    - Ideal negative feedback amplifier
    - Electrical circuit containing a two-port network
    - 'Mechatronics : Position servo with multi-loop feedback'
    - Terminology and classification of signal-flow graphs
    - Standards covering signal-flow graphs
    - State transition signal-flow graph
    - Closed flowgraph
    - Nonlinear flow graphs
    - Examples of nonlinear branch functions
    - Examples of nonlinear signal-flow graph models
    - Applications of SFG techniques in various fields of science
    - Notes
categories:
    - Graphs
---
A signal-flow graph or signal-flowgraph (SFG), invented by Claude Shannon,[1] but often called a Mason graph after Samuel Jefferson Mason who coined the term,[2] is a specialized flow graph, a directed graph in which nodes represent system variables, and branches (edges, arcs, or arrows) represent functional connections between pairs of nodes. Thus, signal-flow graph theory builds on that of directed graphs (also called digraphs), which includes as well that of oriented graphs. This mathematical theory of digraphs exists, of course, quite apart from its applications.[3][4]
