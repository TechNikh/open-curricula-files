---
version: 1
type: video
provider: YouTube
id: ZFN63oTeYzc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Math%20for%20fun%20and%20glory/AIME/2003%20AIME%20II%20problem%208.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8de82a69-7bdd-4aec-9ddb-47cd9c8d5f66
updated: 1486069694
title: 2003 AIME II problem 8
categories:
    - AIME
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZFN63oTeYzc/default.jpg
    - https://i3.ytimg.com/vi/ZFN63oTeYzc/1.jpg
    - https://i3.ytimg.com/vi/ZFN63oTeYzc/2.jpg
    - https://i3.ytimg.com/vi/ZFN63oTeYzc/3.jpg
---
