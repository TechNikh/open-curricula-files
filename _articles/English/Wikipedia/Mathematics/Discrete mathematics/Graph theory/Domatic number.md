---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Domatic_number
offline_file: ""
offline_thumbnail: ""
uuid: 1109e44b-d735-4b6f-9733-b1508623d5e2
updated: 1484308612
title: Domatic number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/90px-Domatic-partition.svg.png
tags:
    - Upper bounds
    - Lower bounds
    - Computational complexity
    - Comparison with similar concepts
    - Notes
categories:
    - Graph theory
---
In graph theory, a domatic partition of a graph 
  
    
      
        G
        =
        (
        V
        ,
        E
        )
      
    
    {\displaystyle G=(V,E)}
  
 is a partition of 
  
    
      
        V
      
    
    {\displaystyle V}
  
 into disjoint sets 
  
    
      
        
          V
          
            1
          
        
      
    
    {\displaystyle V_{1}}
  
, 
  
    
      
        
          V
          
            2
          
        
      
    
    {\displaystyle V_{2}}
  
,...,
  
    
      
        
          V
          
            K
          
        
      
    
    {\displaystyle V_{K}}
  
 such that each Vi is a dominating set for ...
