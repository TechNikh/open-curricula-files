---
version: 1
type: video
provider: YouTube
id: i1xGnax3Y-Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 286d0ae7-17d1-4d7c-a23d-e76c3a3e8c3c
updated: 1486069636
title: 'Magnetron Experiment 1- UV Bulb'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/i1xGnax3Y-Q/default.jpg
    - https://i3.ytimg.com/vi/i1xGnax3Y-Q/1.jpg
    - https://i3.ytimg.com/vi/i1xGnax3Y-Q/2.jpg
    - https://i3.ytimg.com/vi/i1xGnax3Y-Q/3.jpg
---
