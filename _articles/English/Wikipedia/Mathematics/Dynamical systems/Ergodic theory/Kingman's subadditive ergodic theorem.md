---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kingman%27s_subadditive_ergodic_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f26ecaf5-34de-46a5-91ed-0f9a5cd5d604
updated: 1484308780
title: "Kingman's subadditive ergodic theorem"
tags:
    - Statement of theorem
    - Applications
categories:
    - Ergodic theory
---
In mathematics, Kingman's subadditive ergodic theorem is one of several ergodic theorems. It can be seen as a generalization of Birkhoff's ergodic theorem.[1] Intuitively, the subadditive ergodic theorem is a kind of random variable version of Fekete's lemma (hence the name ergodic).[2] As a result, it can be rephrased in the language of probability, e.g. using a sequence of random variables and expected values. The theorem is named after John Kingman.
