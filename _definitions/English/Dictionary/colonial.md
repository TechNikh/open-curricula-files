---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colonial
offline_file: ""
offline_thumbnail: ""
uuid: c77655c4-591e-4f36-adf4-64ba536824c8
updated: 1484310477
title: colonial
categories:
    - Dictionary
---
colonial
     adj 1: of or relating to or characteristic of or inhabiting a
            colony
     2: of animals who live in colonies, such as ants
     3: composed of many distinct individuals united to form a whole
        or colony; "coral is a colonial organism" [syn: {compound}]
     n : a resident of a colony
