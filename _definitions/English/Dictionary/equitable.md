---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equitable
offline_file: ""
offline_thumbnail: ""
uuid: f44baf7d-cd14-4fb4-83e5-3259c2979ec2
updated: 1484310445
title: equitable
categories:
    - Dictionary
---
equitable
     adj : implying justice dictated by reason, conscience, and a
           natural sense of what is fair to all; "equitable
           treatment of all citizens"; "an equitable distribution
           of gifts among the children" [syn: {just}] [ant: {inequitable}]
