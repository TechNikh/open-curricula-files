---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Disjunction_and_existence_properties
offline_file: ""
offline_thumbnail: ""
uuid: 678fadf3-3673-49ff-b087-c923dcafbbac
updated: 1484309492
title: Disjunction and existence properties
tags:
    - Disjunction property
    - Existence property
    - Related properties
    - Background and history
    - In topoi
    - Relationships
categories:
    - Proof theory
---
In mathematical logic, the disjunction and existence properties are the "hallmarks" of constructive theories such as Heyting arithmetic and constructive set theories (Rathjen 2005).
