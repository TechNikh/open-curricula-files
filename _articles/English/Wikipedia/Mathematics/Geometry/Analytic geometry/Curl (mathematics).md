---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curl_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 1fcfce02-88eb-4ca3-9806-49816170b17c
updated: 1484309159
title: Curl (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Curl.svg.png
tags:
    - Definition
    - Intuitive interpretation
    - Usage
    - Examples
    - A simple vector field
    - A more involved example
    - Identities
    - Descriptive examples
    - Generalizations
    - Differential forms
    - Curl geometrically
    - Notes
categories:
    - Analytic geometry
---
In vector calculus, the curl is a vector operator that describes the infinitesimal rotation of a 3-dimensional vector field. At every point in the field, the curl of that point is represented by a vector. The attributes of this vector (length and direction) characterize the rotation at that point.
