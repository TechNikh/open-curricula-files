---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Q-ratio
offline_file: ""
offline_thumbnail: ""
uuid: a1827c4f-98f8-499f-bdbb-d55136552e7c
updated: 1484309059
title: Q-ratio
categories:
    - Poker strategy
---
The Q-ratio (also known as Q number or just Q) is used in poker tournament strategy. It is also known as the "weak force." The Q-ratio describes the relation of the player's stack to the tournament players' average stack. A low Q-ratio — less than 1 — indicates a below-average chip stack, implying disadvantage against opponents. It is an addition to the M-ratio ("strong force") and usually doesn't play a large role in tournament decision-making. However, its importance grows as the table average M-ratio drops.
