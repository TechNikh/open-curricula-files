---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tested
offline_file: ""
offline_thumbnail: ""
uuid: fefe4c9c-874b-45e3-b2be-99b4f77ab0cb
updated: 1484310311
title: tested
categories:
    - Dictionary
---
tested
     adj 1: tested and proved useful or correct; "a tested method" [syn:
             {tried}, {well-tried}]
     2: tested and proved to be reliable [syn: {time-tested}, {tried},
         {tried and true}]
