---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orbital_overlap
offline_file: ""
offline_thumbnail: ""
uuid: 8ebf4b79-eda9-49e2-aeea-070d65199a26
updated: 1484309279
title: Orbital overlap
categories:
    - Molecular geometry
---
In chemical bonds, an orbital overlap is the concentration of orbitals on adjacent atoms in the same regions of space. Orbital overlap can lead to bond formation. The importance of orbital overlap was emphasized by Linus Pauling to explain the molecular bond angles observed through experimentation and is the basis for the concept of orbital hybridization. Since s orbitals are spherical (and have no directionality) and p orbitals are oriented 90° to each other, a theory was needed to explain why molecules such as methane (CH4) had observed bond angles of 109.5°.[1] Pauling proposed that s and p orbitals on the carbon atom can combine to form hybrids (sp3 in the case of methane) which are ...
