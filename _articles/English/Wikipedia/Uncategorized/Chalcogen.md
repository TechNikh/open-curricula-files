---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chalcogen
offline_file: ""
offline_thumbnail: ""
uuid: 88b6e1de-1f6c-49f5-92b1-fab700ff2814
updated: 1484308383
title: Chalcogen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chalkogene.jpg
tags:
    - Properties
    - Atomic and physical
    - Isotopes
    - Allotropes
    - Chemical
    - Compounds
    - With halogens
    - Organic
    - With metals
    - With pnictogens
    - Other
    - History
    - Early discoveries
    - Periodic table placing
    - Modern discoveries
    - Etymology
    - Occurrence
    - Chalcophile elements
    - Production
    - Applications
    - Biological role
    - Toxicity
categories:
    - Uncategorized
---
The chalcogens (/ˈkælkədʒᵻnz/) are the chemical elements in group 16 of the periodic table. This group is also known as the oxygen family. It consists of the elements oxygen (O), sulfur (S), selenium (Se), tellurium (Te), and the radioactive element polonium (Po). The chemically uncharacterized synthetic element livermorium (Lv) is predicted to be a chalcogen as well.[1] Often, oxygen is treated separately from the other chalcogens, sometimes even excluded from the scope of the term "chalcogen" altogether, due to its very different chemical behavior from sulfur, selenium, tellurium, and polonium. The word "chalcogen" is derived from a combination of the Greek word khalkόs ...
