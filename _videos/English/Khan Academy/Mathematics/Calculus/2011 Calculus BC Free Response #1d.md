---
version: 1
type: video
provider: YouTube
id: v-4QScXlN0o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%231d.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 54128932-6fb2-4098-b62d-dba50c28c838
updated: 1486069690
title: '2011 Calculus BC Free Response #1d'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/v-4QScXlN0o/default.jpg
    - https://i3.ytimg.com/vi/v-4QScXlN0o/1.jpg
    - https://i3.ytimg.com/vi/v-4QScXlN0o/2.jpg
    - https://i3.ytimg.com/vi/v-4QScXlN0o/3.jpg
---
