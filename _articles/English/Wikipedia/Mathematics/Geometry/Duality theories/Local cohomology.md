---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 66e3d637-45df-4f1b-ae35-352267199c8b
updated: 1484309234
title: Local cohomology
categories:
    - Duality theories
---
In algebraic geometry, local cohomology is an analog of relative cohomology. Alexander Grothendieck introduced it in seminars in Harvard in 1961 written up by Hartshorne (1967), and in 1961-2 at IHES written up as SGA2 - Grothendieck (1968), republished as Grothendieck (2005).
