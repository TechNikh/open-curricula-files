---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stall
offline_file: ""
offline_thumbnail: ""
uuid: aeda8c31-b824-4b84-9b19-527ffb800608
updated: 1484310224
title: stall
categories:
    - Dictionary
---
stall
     n 1: a compartment in a stable where a single animal is confined
          and fed
     2: small area set off by walls for special use [syn: {booth}, {cubicle},
         {kiosk}]
     3: a booth where articles are displayed for sale [syn: {stand},
         {sales booth}]
     4: a malfunction in the flight of an aircraft in which there is
        a sudden loss of lift that results in a downward plunge;
        "the plane went into a stall and I couldn't control it"
     5: small individual study area in a library [syn: {carrel}, {carrell},
         {cubicle}]
     6: a tactic used to mislead or delay [syn: {stalling}]
     v 1: postpone doing what one should be doing; "He did not ...
