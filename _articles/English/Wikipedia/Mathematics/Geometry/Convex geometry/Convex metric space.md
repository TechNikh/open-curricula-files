---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_metric_space
offline_file: ""
offline_thumbnail: ""
uuid: f8e9335f-c080-487f-a36c-2344585ed191
updated: 1484309189
title: Convex metric space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convex_metric_illustration2.png
tags:
    - Examples
    - Metric segments
    - Convex metric spaces and convex sets
categories:
    - Convex geometry
---
In mathematics, convex metric spaces are, intuitively, metric spaces with the property any "segment" joining two points in that space has other points in it besides the endpoints.
