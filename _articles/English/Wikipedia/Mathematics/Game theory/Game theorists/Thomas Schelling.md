---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thomas_Schelling
offline_file: ""
offline_thumbnail: ""
uuid: c5f196be-5ab0-41bb-96e4-27c09ba3c06c
updated: 1484309026
title: Thomas Schelling
tags:
    - Biography
    - Early years
    - Career
    - The Strategy of Conflict (1960)
    - Arms and Influence (1966)
    - Micromotives and Macrobehavior
    - Global warming
    - Contributions to popular culture
    - Personal life
    - Bibliography
categories:
    - Game theorists
---
Thomas Crombie Schelling (born April 14, 1921) is an American economist and professor of foreign policy, national security, nuclear strategy, and arms control at the School of Public Policy at University of Maryland, College Park. He is also co-faculty at the New England Complex Systems Institute. He was awarded the 2005 Nobel Memorial Prize in Economic Sciences (shared with Robert Aumann) for "having enhanced our understanding of conflict and cooperation through game-theory analysis".
