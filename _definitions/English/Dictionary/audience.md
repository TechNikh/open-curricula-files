---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/audience
offline_file: ""
offline_thumbnail: ""
uuid: baacca31-24ab-4e03-9366-4813eef022a5
updated: 1484310148
title: audience
categories:
    - Dictionary
---
audience
     n 1: a gathering of spectators or listeners at a (usually public)
          performance; "the audience applauded"; "someone in the
          audience began to cough"
     2: the part of the general public interested in a source of
        information or entertainment; "every artist needs an
        audience"; "the broadcast reached an audience of millions"
     3: an opportunity to state your case and be heard; "they
        condemned him without a hearing"; "he saw that he had lost
        his audience" [syn: {hearing}]
     4: a conference (usually with someone important); "he had a
        consultation with the judge"; "he requested an audience
        with the king" [syn: ...
