---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Golden_angle
offline_file: ""
offline_thumbnail: ""
uuid: 97cca195-75a1-4620-a67a-fe67781544af
updated: 1484308938
title: Golden angle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Golden_Angle.svg.png
tags:
    - Derivation
    - Golden angle in nature
categories:
    - Elementary geometry
---
In geometry, the golden angle is the smaller of the two angles created by sectioning the circumference of a circle according to the golden ratio; that is, into two arcs such that the ratio of the length of the larger arc to the length of the smaller arc is the same as the ratio of the full circumference to the length of the larger arc.
