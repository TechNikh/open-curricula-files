---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transversal_(combinatorics)
offline_file: ""
offline_thumbnail: ""
uuid: e47dcb81-476f-4b22-8785-73fa85d9c3f5
updated: 1484308603
title: Transversal (combinatorics)
tags:
    - Examples
    - Systems of distinct representatives
    - Generalizations
    - Category theory
    - Notes
categories:
    - Combinatorics
---
In mathematics, given a collection C of sets, a transversal (also called a cross-section[1][2][3]) is a set containing exactly one element from each member of the collection. When the sets of the collection are mutually disjoint, each element of the transversal corresponds to exactly one member of C (the set it is a member of). If the original sets are not disjoint, there are two possibilities for the definition of a transversal. One variation, the one that mimics the situation when the sets are mutually disjoint, is that there is a bijection f from the transversal to C such that x is an element of f(x) for each x in the transversal. In this case, the transversal is also called a system of ...
