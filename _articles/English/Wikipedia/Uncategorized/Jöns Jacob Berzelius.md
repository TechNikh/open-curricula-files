---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Berzelius
offline_file: ""
offline_thumbnail: ""
uuid: 5a43dd37-da56-4d12-a049-0dc6efca897e
updated: 1484308394
title: Jöns Jacob Berzelius
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-J%25C3%25B6ns_Jacob_Berzelius_daguerreotype.jpg'
tags:
    - Biography
    - Achievements
    - Law of definite proportions
    - Chemical notation
    - Discovery of elements
    - New chemical terms
    - Biology
    - Vitalism
    - Relations with other scientists
    - family
categories:
    - Uncategorized
---
Baron Jöns Jacob Berzelius (Swedish: [jœns ˌjɑːkɔb bæɹˈseːliɵs]; 20 August 1779 – 7 August 1848), named by himself and contemporary society as Jacob Berzelius, was a Swedish chemist. Berzelius is considered, along with Robert Boyle, John Dalton, and Antoine Lavoisier, to be one of the founders of modern chemistry.[1]
