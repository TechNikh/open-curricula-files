---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plot
offline_file: ""
offline_thumbnail: ""
uuid: d1c0d709-9cee-4be0-aeb6-44193897ada2
updated: 1484310212
title: plot
categories:
    - Dictionary
---
plot
     n 1: a secret scheme to do something (especially something
          underhand or illegal); "they concocted a plot to
          discredit the governor"; "I saw through his little game
          from the start" [syn: {secret plan}, {game}]
     2: a small area of ground covered by specific vegetation; "a
        bean plot"; "a cabbage patch"; "a briar patch" [syn: {plot
        of ground}, {patch}]
     3: the story that is told in a novel or play or movie etc.;
        "the characters were well drawn but the plot was banal"
     4: a chart or map showing the movements or progress of an
        object
     v 1: plan secretly, usually something illegal; "They plotted the
          ...
