---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dark_room
offline_file: ""
offline_thumbnail: ""
uuid: c548ea30-b51a-418e-8c7f-cf34d577a4c6
updated: 1484308368
title: Darkroom
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-UCHSPhotoDarkRoom9.10.09ByLuigiNovi1.jpg
tags:
    - Darkroom equipment
    - Print processing
categories:
    - Uncategorized
---
A darkroom is a workshop used by photographers working with photographic film to make prints and carry out other associated tasks. It is a room that can be made completely dark to allow the processing of the light sensitive photographic materials, including film and photographic paper. Various equipment is used in the darkroom, including an enlarger, baths containing chemicals, and running water.
