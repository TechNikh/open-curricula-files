---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turmoil
offline_file: ""
offline_thumbnail: ""
uuid: eba5ce6f-35f8-4a17-9b5b-bbf57043cded
updated: 1484310569
title: turmoil
categories:
    - Dictionary
---
turmoil
     n 1: a violent disturbance; "the convulsions of the stock market"
          [syn: {convulsion}, {upheaval}]
     2: violent agitation [syn: {tumult}]
     3: disturbance usually in protest [syn: {agitation}, {excitement},
         {upheaval}, {hullabaloo}]
