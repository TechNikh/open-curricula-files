---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/like
offline_file: ""
offline_thumbnail: ""
uuid: af242e89-d8d6-4a8a-bfef-051def050cb8
updated: 1484310361
title: like
categories:
    - Dictionary
---
like
     adj 1: resembling or similar; having the same or some of the same
            characteristics; often used in combination; "suits of
            like design"; "a limited circle of like minds";
            "members of the cat family have like dispositions";
            "as like as two peas in a pod"; "doglike devotion"; "a
            dreamlike quality" [syn: {similar}] [ant: {unlike}]
     2: equal in amount or value; "like amounts"; "equivalent
        amounts"; "the same amount"; "gave one six blows and the
        other a like number"; "an equal number"; "the same number"
        [syn: {equal}, {equivalent}, {same}] [ant: {unlike}]
     3: having the same or similar ...
