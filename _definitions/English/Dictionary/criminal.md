---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/criminal
offline_file: ""
offline_thumbnail: ""
uuid: cb10d6b9-ad5e-4f78-bf1d-30ee473a9e19
updated: 1484310563
title: criminal
categories:
    - Dictionary
---
criminal
     adj 1: relating to crime or its punishment; "criminal court"
     2: bringing or deserving severe rebuke or censure; "a criminal
        waste of talent"; "a deplorable act of violence";
        "adultery is as reprehensible for a husband as for a wife"
        [syn: {condemnable}, {deplorable}, {reprehensible}]
     3: guilty of crime or serious offense; "criminal in the sight
        of God and man"
     4: involving or being or having the nature of a crime; "a
        criminal offense"; "criminal abuse"; "felonious intent"
        [syn: {felonious}]
     n : someone who has committed (or been legally convicted of) a
         crime [syn: {felon}, {crook}, {outlaw}, ...
