---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/changed
offline_file: ""
offline_thumbnail: ""
uuid: e78a3a3a-02de-45a2-88a5-d4708a7b4479
updated: 1484310210
title: changed
categories:
    - Dictionary
---
changed
     adj 1: made or become different in nature or form; "changed
            attitudes"; "changed styles of dress"; "a greatly
            changed country after the war" [ant: {unchanged}]
     2: made or become different in some respect; "he's an altered
        (or changed) man since his election to Congress"
     3: changed in constitution or structure or composition by
        metamorphism; "metamorphic rocks"
