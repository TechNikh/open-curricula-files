---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heap
offline_file: ""
offline_thumbnail: ""
uuid: 4ba427cc-7c06-4117-ac4b-447188af7097
updated: 1484310303
title: heap
categories:
    - Dictionary
---
heap
     n 1: a collection of objects laid on top of each other [syn: {pile},
           {mound}, {cumulus}]
     2: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it must
        have cost plenty" [syn: {batch}, {deal}, {flock}, {good
        deal}, {great deal}, {hatful}, {lot}, {mass}, {mess}, {mickle},
         {mint}, {muckle}, {peck}, {pile}, {plenty}, {pot}, {quite
        a little}, {raft}, {sight}, {slew}, {spate}, {stack}, {tidy
        sum}, {wad}, {whole lot}, {whole slew}]
     3: a car that is old and unreliable; "the fenders had fallen
        off ...
