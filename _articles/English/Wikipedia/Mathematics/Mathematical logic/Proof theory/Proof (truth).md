---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_(truth)
offline_file: ""
offline_thumbnail: ""
uuid: 12e19e09-273e-42d6-8547-75d4591d4e35
updated: 1484309497
title: Proof (truth)
categories:
    - Proof theory
---
The concept applies in a variety of disciplines,[5] with both the nature of the evidence or justification and the criteria for sufficiency being area-dependent. In the area of oral and written communication such as conversation, dialog, rhetoric, etc., a proof is a persuasive perlocutionary speech act, which demonstrates the truth of a proposition.[6] In any area of mathematics defined by its assumptions or axioms, a proof is an argument establishing a theorem of that area via accepted rules of inference starting from those axioms and from other previously established theorems.[7] The subject of logic, in particular proof theory, formalizes and studies the notion of formal proof.[8] In some ...
