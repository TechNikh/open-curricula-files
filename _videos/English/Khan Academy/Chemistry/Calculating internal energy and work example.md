---
version: 1
type: video
provider: YouTube
id: FTO0lS1Q9QI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Calculating%20internal%20energy%20and%20work%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d36bae30-4629-49e3-9013-fa536d4e365c
updated: 1486069589
title: Calculating internal energy and work example
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/FTO0lS1Q9QI/default.jpg
    - https://i3.ytimg.com/vi/FTO0lS1Q9QI/1.jpg
    - https://i3.ytimg.com/vi/FTO0lS1Q9QI/2.jpg
    - https://i3.ytimg.com/vi/FTO0lS1Q9QI/3.jpg
---
Worked example calculating the change in internal energy for a gas using the first law of thermodynamics.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/internal-energy-sal/v/specific-heat-and-latent-leat-of-fusion-and-vaporization-2?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/internal-energy-sal/v/more-on-internal-energy?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
