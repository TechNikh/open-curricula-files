---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shannon_capacity_of_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8c2bf550-6ac3-4f81-8a91-35c676de3085
updated: 1484309426
title: Shannon capacity of a graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Cycle_graph_C5.png
tags:
    - Graph models of communication channels
    - Relation to independent sets
    - Computational complexity
    - Upper bounds
    - Lovász number
    - "Haemers' bound"
categories:
    - Graph invariants
---
In graph theory, the Shannon capacity of a graph is a graph invariant defined from the number of independent sets of strong graph products. It measures the Shannon capacity of a communications channel defined from the graph, and is upper bounded by the Lovász number, which can be computed in polynomial time. However, the computational complexity of the Shannon capacity itself remains unknown.
