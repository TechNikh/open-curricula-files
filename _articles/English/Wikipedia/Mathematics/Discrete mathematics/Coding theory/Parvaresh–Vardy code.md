---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Parvaresh%E2%80%93Vardy_code'
offline_file: ""
offline_thumbnail: ""
uuid: e50a9c93-52fd-4ced-ad93-26d92625cff6
updated: 1484308553
title: Parvaresh–Vardy code
categories:
    - Coding theory
---
Parvaresh–Vardy codes are a family of error-correcting codes first described in 2005 by Farzad Parvaresh and Alexander Vardy.[1] They can be used for efficient list-decoding.
