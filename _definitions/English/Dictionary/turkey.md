---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turkey
offline_file: ""
offline_thumbnail: ""
uuid: 50b1900f-a890-4251-bb8a-9b6c5b5d7866
updated: 1484310551
title: turkey
categories:
    - Dictionary
---
turkey
     n 1: large gallinaceous bird with fan-shaped tail; widely
          domesticated for food [syn: {Meleagris gallopavo}]
     2: a Eurasian republic in Asia Minor and the Balkans; achieved
        independence from the Ottoman Empire in 1923 [syn: {Republic
        of Turkey}]
     3: a person who does something thoughtless or annoying; "some
        joker is blocking the driveway" [syn: {joker}]
     4: flesh of large domesticated fowl usually roasted
     5: an event that fails badly or is totally ineffectual; "the
        first experiment was a real turkey"; "the meeting was a
        dud as far as new business was concerned" [syn: {bomb}, {dud}]
     6: wild turkey of Central ...
