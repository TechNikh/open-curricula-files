---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cure
offline_file: ""
offline_thumbnail: ""
uuid: 1f5e70ad-0ae4-412e-9505-231eebe3daef
updated: 1484310547
title: cure
categories:
    - Dictionary
---
cure
     n : a medicine or therapy that cures disease or relieve pain
         [syn: {remedy}, {curative}]
     v 1: provide a cure for, make healthy again; "The treatment cured
          the boy's acne"; "The quack pretended to heal patients
          but never managed to" [syn: {bring around}, {heal}]
     2: prepare by drying, salting, or chemical processing in order
        to preserve; "cure meats"; "cure pickles"
     3: make (substances) hard and improve their usability; "cure
        resin"
     4: be or become preserved; "the apricots cure in the sun"
