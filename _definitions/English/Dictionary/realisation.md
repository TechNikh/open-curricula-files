---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realisation
offline_file: ""
offline_thumbnail: ""
uuid: 555ae3f7-d573-4cff-8621-7c099574c6d4
updated: 1484310152
title: realisation
categories:
    - Dictionary
---
realisation
     n 1: a musical composition that has been completed or enriched by
          someone other than the composer [syn: {realization}]
     2: coming to understand something clearly and distinctly; "a
        growing realization of the risk involved"; "a sudden
        recognition of the problem he faced"; "increasing
        recognition that diabetes frequently coexists with other
        chronic diseases" [syn: {realization}, {recognition}]
     3: a sale in order to obtain money (as a sale of stock or a
        sale of the estate of a bankrupt person) or the money so
        obtained [syn: {realization}]
     4: the completion or enrichment of a piece of music left
        ...
