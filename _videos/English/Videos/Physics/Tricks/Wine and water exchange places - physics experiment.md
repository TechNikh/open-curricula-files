---
version: 1
type: video
provider: YouTube
id: UBOIzW7O_5k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f58f0049-84ce-40c4-878e-4e825a6d9ba3
updated: 1486069657
title: 'Wine and water exchange places - physics experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/UBOIzW7O_5k/default.jpg
    - https://i3.ytimg.com/vi/UBOIzW7O_5k/1.jpg
    - https://i3.ytimg.com/vi/UBOIzW7O_5k/2.jpg
    - https://i3.ytimg.com/vi/UBOIzW7O_5k/3.jpg
---
