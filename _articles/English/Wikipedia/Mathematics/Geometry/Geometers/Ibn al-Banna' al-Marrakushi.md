---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ibn_al-Banna%27_al-Marrakushi'
offline_file: ""
offline_thumbnail: ""
uuid: abd10d76-7d20-49b3-a1f2-45624fac7afc
updated: 1484309085
title: "Ibn al-Banna' al-Marrakushi"
tags:
    - Biography
    - Works
    - Legacy
categories:
    - Geometers
---
Ibn al‐Bannāʾ al‐Marrākushī al-Azdi, also known as Abu'l-Abbas Ahmad ibn Muhammad ibn Uthman al-Azdi (Arabic: ابن البنّاء‎‎) (29 December 1256 – c. 1321), was a Moroccan mathematician, astronomer, Islamic scholar, Sufi, and a one-time astrologer.
