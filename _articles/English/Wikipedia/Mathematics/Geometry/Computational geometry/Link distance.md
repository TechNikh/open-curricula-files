---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Link_distance
offline_file: ""
offline_thumbnail: ""
uuid: e5b84aa8-11d0-4ae0-91d4-2187b45ae78f
updated: 1484309173
title: Link distance
categories:
    - Computational geometry
---
In computational geometry, the link distance between two points in a polygon is the minimum number of line segments of any polygonal chain within the polygon that has the two points as its endpoints. The link diameter of the polygon is the maximum link distance of any two of its points.
