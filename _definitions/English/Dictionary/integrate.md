---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integrate
offline_file: ""
offline_thumbnail: ""
uuid: 2a12ecce-862b-42a1-a579-c0d2ca548139
updated: 1484310547
title: integrate
categories:
    - Dictionary
---
integrate
     v 1: make into a whole or make part of a whole; "She incorporated
          his suggestions into her proposal" [syn: {incorporate}]
          [ant: {disintegrate}]
     2: open (a place) to members of all races and ethnic groups;
        "This school is completely desegregated" [syn: {desegregate},
         {mix}] [ant: {segregate}]
     3: become one; become integrated; "The students at this school
        integrate immediately, despite their different
        backgrounds"
     4: calculate the integral of; calculate by integration [ant: {differentiate}]
