---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flying
offline_file: ""
offline_thumbnail: ""
uuid: 2ed2ada9-e338-46ae-bf6b-a48e73df65ff
updated: 1484310283
title: flying
categories:
    - Dictionary
---
flying
     adj 1: capable of or engaged in flight; "the bat is a flying
            animal"
     2: moving swiftly; "fast-flying planes"; "played the difficult
        passage with flying fingers" [syn: {fast-flying}]
     3: streaming or flapping or spreading wide as if in a current
        of air; "ran quickly, her flaring coat behind her";
        "flying banners"; "flags waving in the breeze" [syn: {aflare},
         {flaring}, {waving}]
     4: designed for swift movement or action; "a flying police
        squad is trained for quick action anywhere in the city"
     5: of or relating to passage through the air especially
        aviation; "a flying time of three hours between ...
