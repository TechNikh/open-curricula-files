---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sverdrup_wave
offline_file: ""
offline_thumbnail: ""
uuid: e3e5ba2e-b619-4fa8-b9c2-dc3c65aee1c8
updated: 1484308898
title: Sverdrup wave
categories:
    - Waves
---
In shallow water gravity wave [or long wave (λ > 20 h)], (h stands for depth)only gravity effect the wave, the phase velocity of shallow water gravity wave (c) can be noted as
