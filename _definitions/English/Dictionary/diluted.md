---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diluted
offline_file: ""
offline_thumbnail: ""
uuid: 57abed47-c8e9-43cb-98ce-8c3a3346ecc1
updated: 1484310339
title: diluted
categories:
    - Dictionary
---
diluted
     adj 1: reduced in strength or concentration or quality or purity;
            "diluted alcohol"; "a dilute solution"; "dilute acetic
            acid" [syn: {dilute}] [ant: {undiluted}]
     2: made less strong or severe; "a pale gleam of diluted
        sunlight" [syn: {toned down(p)}]
