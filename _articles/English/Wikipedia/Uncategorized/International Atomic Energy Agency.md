---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iaea
offline_file: ""
offline_thumbnail: ""
uuid: 01499467-ca1d-4f70-8c18-7a4057654fac
updated: 1484308412
title: International Atomic Energy Agency
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Iaea-vienna.JPG
tags:
    - History
    - Structure and function
    - General
    - Board of Governors
    - General Conference
    - Secretariat
    - Missions
    - Peaceful uses
    - Safeguards
    - Nuclear safety
    - Criticism
    - Membership
    - List of Directors General
    - Notes
    - Works cited
categories:
    - Uncategorized
---
The International Atomic Energy Agency (IAEA) is an international organization that seeks to promote the peaceful use of nuclear energy, and to inhibit its use for any military purpose, including nuclear weapons. The IAEA was established as an autonomous organization on 29 July 1957. Though established independently of the United Nations through its own international treaty, the IAEA Statute,[1] the IAEA reports to both the United Nations General Assembly and Security Council.
