---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/puzzling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484650981
title: puzzling
categories:
    - Dictionary
---
puzzling
     adj 1: not clear to the understanding; "I didn't grasp the meaning
            of that enigmatic comment until much later";
            "prophetic texts so enigmatic that their meaning has
            been disputed for centuries" [syn: {enigmatic}, {enigmatical}]
     2: lacking clarity of meaning; causing confusion or perplexity;
        "sent confusing signals to Iraq"; "perplexing to someone
        who knew nothing about it"; "a puzzling statement" [syn: {confusing},
         {perplexing}]
