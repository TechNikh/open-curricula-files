---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ramanujan_graph
offline_file: ""
offline_thumbnail: ""
uuid: fb2aa9bb-8e8d-41a8-9c4a-04a6ce223954
updated: 1484309376
title: Ramanujan graph
tags:
    - Definition
    - Extremality of Ramanujan graphs
    - Constructions
categories:
    - Algebraic graph theory
---
In spectral graph theory, a Ramanujan graph, named after Srinivasa Ramanujan, is a regular graph whose spectral gap is almost as large as possible (see extremal graph theory). Such graphs are excellent spectral expanders.
