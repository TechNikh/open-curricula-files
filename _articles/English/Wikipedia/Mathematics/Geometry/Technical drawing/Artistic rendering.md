---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artistic_rendering
offline_file: ""
offline_thumbnail: ""
uuid: 77e1a06c-1b20-4e1e-a9b0-e7ea2b8bf218
updated: 1484309331
title: Artistic rendering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_13.jpg
categories:
    - Technical drawing
---
Rendering in visual art and technical drawing means the process of formulating, adding color, shading, and texturing of an image. It can also be used to describe the quality of execution of that process. When used as a means of expression, it is synonymous with illustrating. However, it may be used for mere visualization of existing data regardless of any preconceived message or idea to express. Rendering is also a technique that can be used while designing packaging and branding.
