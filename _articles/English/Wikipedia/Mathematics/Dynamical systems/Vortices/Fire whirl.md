---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fire_whirl
offline_file: ""
offline_thumbnail: ""
uuid: b331f66a-346a-4993-a73e-c2908f2d038c
updated: 1484308879
title: Fire whirl
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Large_bonfire_1.jpg
tags:
    - Formation
    - Examples
    - Classification
    - Firecane
    - Notes
    - Bibliography
categories:
    - Vortices
---
A fire whirl – also commonly known as a fire devil, fire tornado, firenado, or fire twister, or in Japan dragon twist – is a whirlwind induced by a fire and often made up of flame or ash. Fire whirls may occur when intense rising heat and turbulent wind conditions combine to form whirling eddies of air. These eddies can contract into a tornado-like structure that sucks in burning debris and combustible gases.
