---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_foresight
offline_file: ""
offline_thumbnail: ""
uuid: 78741851-6060-4126-8745-706b3dccdc4f
updated: 1484309065
title: Strategic foresight
tags:
    - Quotes
    - Conferences
    - Case studies
categories:
    - Strategy
---
Strategic foresight is a planning-oriented disicipline related to futures studies, the study of the future. Strategy is a high level plan to achieve one or more goals under conditions of uncertainty. Strategic foresight happens when any planner uses scanned inputs, forecasts, alternative futures exploration, analysis and feedback to produce or alter plans and actions of the organization.
