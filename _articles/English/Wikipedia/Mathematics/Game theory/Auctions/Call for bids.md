---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Call_for_bids
offline_file: ""
offline_thumbnail: ""
uuid: cec95728-f8e2-4391-a620-e3c97fdcc0cf
updated: 1484309013
title: Call for bids
tags:
    - Types of calls for tenders
    - Origin of the term
    - Double envelope system
    - Tender box
    - Security deposit
    - Locating tenders
    - Typical template contents (in project management)
categories:
    - Auctions
---
A call for bids,[1] call for tenders,[2] or invitation to tender[3] (ITT, often called tender for short) is a special procedure for generating competing offers from different bidders looking to obtain an award of business activity in works, supply, or service contracts. They are usually preceded by a pre-qualification questionnaire (PQQ).
