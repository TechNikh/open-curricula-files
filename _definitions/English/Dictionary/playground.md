---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/playground
offline_file: ""
offline_thumbnail: ""
uuid: bb72a965-ce4a-49fc-bbc4-139c54dca725
updated: 1484310188
title: playground
categories:
    - Dictionary
---
playground
     n 1: an area where many people go for recreation [syn: {resort
          area}, {vacation spot}]
     2: yard consisting of an outdoor area for children's play
