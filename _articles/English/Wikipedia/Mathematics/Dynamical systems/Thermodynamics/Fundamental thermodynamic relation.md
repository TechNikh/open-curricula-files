---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fundamental_thermodynamic_relation
offline_file: ""
offline_thumbnail: ""
uuid: 1a9117b8-f5f2-419e-8d0e-7f1b7d73fc4b
updated: 1484308842
title: Fundamental thermodynamic relation
tags:
    - Derivation from the first and second laws of thermodynamics
    - Derivation from statistical mechanical principles
categories:
    - Thermodynamics
---
In thermodynamics, the fundamental thermodynamic relation is generally expressed as an infinitesimal change in internal energy in terms of infinitesimal changes in entropy, and volume for a closed system in thermal equilibrium in the following way.
