---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnifying_lens
offline_file: ""
offline_thumbnail: ""
uuid: 01045bc9-186a-45b7-bd01-66d3f64166c4
updated: 1484308366
title: Magnifying glass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Magnifying-glass-green-brass.png
tags:
    - History
    - Magnification
    - Alternatives
    - Uses as a symbol
categories:
    - Uncategorized
---
A magnifying glass (called a hand lens in laboratory contexts) is a convex lens that is used to produce a magnified image of an object. The lens is usually mounted in a frame with a handle (see image).
