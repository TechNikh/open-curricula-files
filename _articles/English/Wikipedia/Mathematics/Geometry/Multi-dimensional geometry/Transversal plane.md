---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transversal_plane
offline_file: ""
offline_thumbnail: ""
uuid: 2c0ea575-8a5b-4fab-ab21-7121c01c1ae0
updated: 1484309287
title: Transversal plane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Transversal_planes.JPG
categories:
    - Multi-dimensional geometry
---
In geometry, a transversal plane is a plane that intersects (not contains) two or more lines or planes. A transversal plane may also form dihedral angles.
