---
version: 1
type: video
provider: YouTube
id: 7T5yHI-po_c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4d95fb8f-9c7c-4372-8a04-cbda33d4b1a6
updated: 1486069609
title: Geometric sequences
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/7T5yHI-po_c/default.jpg
    - https://i3.ytimg.com/vi/7T5yHI-po_c/1.jpg
    - https://i3.ytimg.com/vi/7T5yHI-po_c/2.jpg
    - https://i3.ytimg.com/vi/7T5yHI-po_c/3.jpg
---
