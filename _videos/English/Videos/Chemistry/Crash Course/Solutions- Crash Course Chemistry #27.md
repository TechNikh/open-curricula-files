---
version: 1
type: video
provider: YouTube
id: 9h2f1Bjr0p4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Solutions-%20Crash%20Course%20Chemistry%20%2327.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2272a366-e53a-444e-be91-4b07766263c6
updated: 1486069680
title: 'Solutions- Crash Course Chemistry #27'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/9h2f1Bjr0p4/default.jpg
    - https://i3.ytimg.com/vi/9h2f1Bjr0p4/1.jpg
    - https://i3.ytimg.com/vi/9h2f1Bjr0p4/2.jpg
    - https://i3.ytimg.com/vi/9h2f1Bjr0p4/3.jpg
---
