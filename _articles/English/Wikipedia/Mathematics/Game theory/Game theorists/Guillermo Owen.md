---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Guillermo_Owen
offline_file: ""
offline_thumbnail: ""
uuid: f7cb5369-d27b-4441-b761-254228b15061
updated: 1484309025
title: Guillermo Owen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Guillermo_Owen.jpg
tags:
    - Biography
    - Publications
categories:
    - Game theorists
---
Guillermo Owen (born 1938) is a Colombian mathematician, and Professor of Applied Mathematics at the Naval Postgraduate School in Monterey, California, known for his work in Game Theory.[1][2]
