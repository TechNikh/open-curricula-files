---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vapours
offline_file: ""
offline_thumbnail: ""
uuid: 0f103d56-096c-47b2-b6ae-ad52d9698138
updated: 1484310391
title: vapours
categories:
    - Dictionary
---
vapours
     n : a state of depression; "he had a bad case of the blues"
         [syn: {blues}, {blue devils}, {megrims}, {vapors}]
