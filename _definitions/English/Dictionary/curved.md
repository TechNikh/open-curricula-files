---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curved
offline_file: ""
offline_thumbnail: ""
uuid: 787ce773-22bd-4994-8b15-7240b82ac135
updated: 1484310224
title: curved
categories:
    - Dictionary
---
curved
     adj : not straight; having or marked by a curve or smoothly
           rounded bend; "the curved tusks of a walrus"; "his
           curved lips suggested a smile but his eyes were hard"
           [syn: {curving}] [ant: {straight}]
