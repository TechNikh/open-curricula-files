---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/improve
offline_file: ""
offline_thumbnail: ""
uuid: 666cece0-3227-46f9-9ce9-f2b15f3a09f4
updated: 1484310361
title: improve
categories:
    - Dictionary
---
improve
     v 1: to make better; "The editor improved the manuscript with his
          changes" [syn: {better}, {amend}, {ameliorate}, {meliorate}]
          [ant: {worsen}]
     2: get better; "The weather improved toward evening" [syn: {better},
         {ameliorate}, {meliorate}] [ant: {worsen}]
