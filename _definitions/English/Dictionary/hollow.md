---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hollow
offline_file: ""
offline_thumbnail: ""
uuid: 50e95ae8-5c28-438c-81ac-11ff8b2f3171
updated: 1484310414
title: hollow
categories:
    - Dictionary
---
hollow
     adj 1: not solid; having a space or gap or cavity; "a hollow wall";
            "a hollow tree"; "hollow cheeks"; "his face became
            gaunter and more hollow with each year" [ant: {solid}]
     2: deliberately deceptive; "hollow (or false) promises"; "false
        pretenses" [syn: {false}]
     3: as if echoing in a hollow space; "the hollow sound of
        footsteps in the empty ballroom"
     4: devoid of significance or point; "empty promises"; "a hollow
        victory"; "vacuous comments" [syn: {empty}, {vacuous}]
     n 1: a cavity or space in something; "hunger had caused the
          hollows in their cheeks"
     2: a small valley between mountains; "he built ...
