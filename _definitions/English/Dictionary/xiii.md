---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/xiii
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484329861
title: xiii
categories:
    - Dictionary
---
xiii
     adj : being one more than twelve [syn: {thirteen}, {13}]
     n : the cardinal number that is the sum of twelve and one [syn:
         {thirteen}, {13}, {baker's dozen}, {long dozen}]
