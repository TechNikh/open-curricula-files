---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pacifying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443441
title: pacifying
categories:
    - Dictionary
---
pacifying
     adj : freeing from fear and anxiety [syn: {assuasive}, {calming},
           {soothing}]
