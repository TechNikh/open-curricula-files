---
version: 1
type: article
id: https://en.wikipedia.org/wiki/C-symmetry
offline_file: ""
offline_thumbnail: ""
uuid: fbbf4db4-82f6-4a14-b5e1-9d1f04ba65a9
updated: 1484309305
title: C-symmetry
tags:
    - Charge reversal in electromagnetism
    - Combination of charge and parity reversal
    - Charge definition
categories:
    - Symmetry
---
In physics, C-symmetry means the symmetry of physical laws under a charge-conjugation transformation. Electromagnetism, gravity and the strong interaction all obey C-symmetry, but weak interactions violate C-symmetry.
