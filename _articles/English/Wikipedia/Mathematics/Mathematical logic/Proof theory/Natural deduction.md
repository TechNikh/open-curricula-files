---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_deduction
offline_file: ""
offline_thumbnail: ""
uuid: bb0da39e-55e6-44e8-b265-2bf7eff93743
updated: 1484309495
title: Natural deduction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-First_order_natural_deduction.png
tags:
    - Motivation
    - Judgments and propositions
    - Introduction and elimination
    - Hypothetical derivations
    - Consistency, completeness, and normal forms
    - First and higher-order extensions
    - Different presentations of natural deduction
    - Tree-like presentations
    - Sequential presentations
    - Proofs and type theory
    - Classical and modal logics
    - Comparison with other foundational approaches
    - Sequent calculus
    - Notes
categories:
    - Proof theory
---
In logic and proof theory, natural deduction is a kind of proof calculus in which logical reasoning is expressed by inference rules closely related to the "natural" way of reasoning. This contrasts with the axiomatic systems which instead use axioms as much as possible to express the logical laws of deductive reasoning.
