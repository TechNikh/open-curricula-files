---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extreme_point
offline_file: ""
offline_thumbnail: ""
uuid: 92895d28-9943-49bd-89a5-9509de27920e
updated: 1484309193
title: Extreme point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Extreme_points.svg_1.png
tags:
    - k-extreme points
    - Notes
categories:
    - Convex geometry
---
In mathematics, an extreme point of a convex set S in a real vector space is a point in S which does not lie in any open line segment joining two points of S. Intuitively, an extreme point is a "vertex" of S.
