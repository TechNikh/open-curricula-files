---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Hamiltonian_(quantum_mechanics)
offline_file: ""
offline_thumbnail: ""
uuid: dca70023-2f74-4b20-be54-f24537e20b08
updated: 1484308797
title: Hamiltonian (quantum mechanics)
tags:
    - Introduction
    - The Schrödinger Hamiltonian
    - One particle
    - Many particles
    - Schrödinger equation
    - Dirac formalism
    - Expressions for the Hamiltonian
    - General forms for one particle
    - Free particle
    - Constant-potential well
    - Simple harmonic oscillator
    - Rigid rotor
    - Electrostatic or coulomb potential
    - Electric dipole in an electric field
    - Magnetic dipole in a magnetic field
    - Charged particle in an electromagnetic field
    - Energy eigenket degeneracy, symmetry, and conservation laws
    - "Hamilton's equations"
categories:
    - Hamiltonian mechanics
---
In quantum mechanics, the Hamiltonian is the operator corresponding to the total energy of the system in most of the cases. It is usually denoted by H, also Ȟ or Ĥ. Its spectrum is the set of possible outcomes when one measures the total energy of a system. Because of its close relation to the time-evolution of a system, it is of fundamental importance in most formulations of quantum theory.
