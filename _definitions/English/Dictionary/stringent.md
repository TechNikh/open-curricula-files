---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stringent
offline_file: ""
offline_thumbnail: ""
uuid: 1d5595c5-c5b6-49a6-bba1-12d9092ad72b
updated: 1484310183
title: stringent
categories:
    - Dictionary
---
stringent
     adj : demanding strict attention to rules and procedures;
           "rigorous discipline"; "tight security"; "stringent
           safety measures" [syn: {rigorous}, {tight}]
