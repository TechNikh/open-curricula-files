---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/structure
offline_file: ""
offline_thumbnail: ""
uuid: c96a4314-0a6f-4513-b35d-3dfb66b0df70
updated: 1484310363
title: structure
categories:
    - Dictionary
---
structure
     n 1: a thing constructed; a complex construction or entity; "the
          structure consisted of a series of arches"; "she wore
          her hair in an amazing construction of whirls and
          ribbons" [syn: {construction}]
     2: the manner of construction of something and the arrangement
        of its parts; "artists must study the structure of the
        human body"; "the structure of the benzene molecule"
     3: the complex composition of knowledge as elements and their
        combinations; "his lectures have no structure"
     4: a particular complex anatomical structure; "he has good bone
        structure" [syn: {anatomical structure}, {complex body
        ...
