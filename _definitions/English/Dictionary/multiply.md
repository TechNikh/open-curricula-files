---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multiply
offline_file: ""
offline_thumbnail: ""
uuid: 550374cc-a748-4863-8cdc-348c60a57287
updated: 1484310373
title: multiply
categories:
    - Dictionary
---
multiply
     adv : in several ways; in a multiple manner; "they were multiply
           checked for errors" [ant: {singly}]
     v 1: combine by multiplication; "multiply 10 by 15" [ant: {divide}]
     2: combine or increase by multiplication; "He managed to
        multiply his profits" [syn: {manifold}]
     3: have young (animals); "pandas rarely breed in captivity"
        [syn: {breed}]
     4: have offspring or young; "The deer in our neighborhood
        reproduce madly"; "The Catholic Church tells people to
        procreate, no matter what their economic situation may be"
        [syn: {reproduce}, {procreate}]
     [also: {multiplied}]
