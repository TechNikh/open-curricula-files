---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metropolitan
offline_file: ""
offline_thumbnail: ""
uuid: e5c9652d-3af2-4419-8fa4-e223453d472c
updated: 1484310479
title: metropolitan
categories:
    - Dictionary
---
metropolitan
     adj : relating to or characteristic of a metropolis; "metropolitan
           area"
     n 1: in the Eastern Orthodox Church this title is given to a
          position between bishop and patriarch; equivalent to
          archbishop in western Christianity
     2: a person who lives in a metropolis
