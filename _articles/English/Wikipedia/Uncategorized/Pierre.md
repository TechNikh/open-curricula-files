---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pierre
offline_file: ""
offline_thumbnail: ""
uuid: e3691bdf-0b0e-46f9-ad90-88ef4af83473
updated: 1484308327
title: Pierre
tags:
    - Persons with the given name
    - Surname
    - Other
categories:
    - Uncategorized
---
Pierre is a masculine given name. It is a French form of the name Peter[1] (it can also be a surname and a place name). Pierre originally means "rock" or "stone" in French (derived from the Greek word "petros" meaning "stone, rock", via Latin "petra"). See also Peter.
