---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dynamic
offline_file: ""
offline_thumbnail: ""
uuid: a804fde7-4a95-4133-9e34-7613efb770ad
updated: 1484310471
title: dynamic
categories:
    - Dictionary
---
dynamic
     adj 1: characterized by action or forcefulness or force of
            personality; "a dynamic market"; "a dynamic speaker";
            "the dynamic president of the firm" [syn: {dynamical}]
            [ant: {undynamic}]
     2: of or relating to dynamics
     3: expressing action rather than a state of being; used of
        verbs (e.g. `to run') and participial adjectives (e.g.
        `running' in `running water') [syn: {active}] [ant: {stative}]
     n : an efficient incentive; "they hoped it would act as a
         spiritual dynamic on all churches" [syn: {moral force}]
