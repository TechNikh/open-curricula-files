---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stale
offline_file: ""
offline_thumbnail: ""
uuid: 2ea06c07-3d54-4e23-849c-56686a524755
updated: 1484310349
title: stale
categories:
    - Dictionary
---
stale
     adj 1: showing deterioration from age; "stale bread" [ant: {fresh}]
     2: lacking originality or spontaneity; no longer new;
        "moth-eaten theories about race" [syn: {old}, {moth-eaten}]
     3: no longer new; uninteresting; "cold (or stale) news" [syn: {cold}]
     v : urinate, of cattle and horses
