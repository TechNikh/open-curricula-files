---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphatically
offline_file: ""
offline_thumbnail: ""
uuid: 13d075cb-4828-474b-abda-a57993e6296a
updated: 1484310189
title: emphatically
categories:
    - Dictionary
---
emphatically
     adv : without question and beyond doubt; "it was decidedly too
           expensive"; "she told him off in spades"; "by all odds
           they should win" [syn: {decidedly}, {unquestionably}, {definitely},
            {in spades}, {by all odds}]
