---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neatly
offline_file: ""
offline_thumbnail: ""
uuid: 1ea65779-650b-469e-84bf-a61108519b79
updated: 1484310319
title: neatly
categories:
    - Dictionary
---
neatly
     adv : with neatness; "she put the slippers under the bed neatly"
           [syn: {showing neatness}]
