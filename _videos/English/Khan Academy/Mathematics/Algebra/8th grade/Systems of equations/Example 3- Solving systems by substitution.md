---
version: 1
type: video
provider: YouTube
id: 0BgUKHTW37E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Example%203-%20Solving%20systems%20by%20substitution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d15d8cb-14f8-49b6-a8ef-57951521346d
updated: 1486069561
title: 'Example 3- Solving systems by substitution'
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/0BgUKHTW37E/default.jpg
    - https://i3.ytimg.com/vi/0BgUKHTW37E/1.jpg
    - https://i3.ytimg.com/vi/0BgUKHTW37E/2.jpg
    - https://i3.ytimg.com/vi/0BgUKHTW37E/3.jpg
---
