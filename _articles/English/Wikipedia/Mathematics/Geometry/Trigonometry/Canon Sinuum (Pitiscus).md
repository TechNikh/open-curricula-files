---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canon_Sinuum_(Pitiscus)
offline_file: ""
offline_thumbnail: ""
uuid: 413f7471-47b3-47e7-8634-65744ae5b409
updated: 1484309340
title: Canon Sinuum (Pitiscus)
categories:
    - Trigonometry
---
The Canon Sinuum is the main part of Bartholomaeus Pitiscus' Thesaurus Mathematicus sive Canon Sinuum ad radium 1.00000.00000.00000 published as a folio in 1613 (misprinted on two of the titles 1513, by omission of a C) in Frankfurt.[1][2] It is a table of sines, originally computed by Rheticus,[3] with the sines given every 10 seconds to 15 places, with first, second, and third differences.[4] This table spans 270 pages.
