---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Expander_mixing_lemma
offline_file: ""
offline_thumbnail: ""
uuid: b010dcd6-85d3-4716-9a08-854193405651
updated: 1484308627
title: Expander mixing lemma
tags:
    - Statement
    - Proof
    - Converse
    - Notes
categories:
    - Graph theory
---
The expander mixing lemma states that, for any two subsets 
  
    
      
        S
        ,
        T
      
    
    {\displaystyle S,T}
  
 of a d-regular expander graph 
  
    
      
        G
      
    
    {\displaystyle G}
  
 with 
  
    
      
        n
      
    
    {\displaystyle n}
  
 vertices, the number of edges between 
  
    
      
        S
      
    
    {\displaystyle S}
  
 and 
  
    
      
        T
      
    
    {\displaystyle T}
  
 is approximately what you would expect in a random d-regular graph, i.e. 
  
    
      
        d
        
          |
        
        S
        
          |
        
        
          |
        
        T
        
    ...
