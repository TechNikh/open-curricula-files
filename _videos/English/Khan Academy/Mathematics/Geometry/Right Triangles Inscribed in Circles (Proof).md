---
version: 1
type: video
provider: YouTube
id: b0U1NxbRU4w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Right%20Triangles%20Inscribed%20in%20Circles%20%28Proof%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b277c234-7f90-4975-a50f-f8db1701608e
updated: 1486069575
title: Right Triangles Inscribed in Circles (Proof)
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/b0U1NxbRU4w/default.jpg
    - https://i3.ytimg.com/vi/b0U1NxbRU4w/1.jpg
    - https://i3.ytimg.com/vi/b0U1NxbRU4w/2.jpg
    - https://i3.ytimg.com/vi/b0U1NxbRU4w/3.jpg
---
Proof showing that a triangle inscribed in a circle having a diameter as one side is a right triangle
