---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Haruo_Hosoya
offline_file: ""
offline_thumbnail: ""
uuid: 700019af-9fb4-4117-b65a-6dee436c9f17
updated: 1484309426
title: Haruo Hosoya
categories:
    - Mathematical chemistry
---
Haruo Hosoya (細矢 治夫, Hosoya Haruo?, born 1936) is a Japanese chemist, emeritus professor of the Ochanomizu University, Tokyo, Japan, the namesake of the Hosoya index used in computational chemistry.[1]
