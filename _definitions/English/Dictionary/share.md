---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/share
offline_file: ""
offline_thumbnail: ""
uuid: 86c784c4-5b1e-4414-87e5-6e363e8b889c
updated: 1484310309
title: share
categories:
    - Dictionary
---
share
     n 1: any of the equal portions into which the capital stock of a
          corporation is divided and ownership of which is
          evidenced by a stock certificate; "he bought 100 shares
          of IBM at the market price"
     2: assets belonging to or due to or contributed by an
        individual person or group; "he wanted his share in cash"
        [syn: {portion}, {part}, {percentage}]
     3: the result of parcelling out or sharing; "death gets more
        than its share of attention from theologicans" [syn: {parcel},
         {portion}]
     4: any one of a number of individual efforts in a common
        endeavor; "I am proud of my contribution to the team's
       ...
