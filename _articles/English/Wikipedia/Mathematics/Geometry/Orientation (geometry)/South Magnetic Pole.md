---
version: 1
type: article
id: https://en.wikipedia.org/wiki/South_Magnetic_Pole
offline_file: ""
offline_thumbnail: ""
uuid: e0f7682b-7639-4561-ba9f-9ca1c6901445
updated: 1484309302
title: South Magnetic Pole
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Magnetic_South_Pole_locations.png
tags:
    - Expeditions
    - Fits to global data sets
    - South Geomagnetic Pole
categories:
    - Orientation (geometry)
---
The South Magnetic Pole is the wandering point on the Earth's Southern Hemisphere where the geomagnetic field lines are directed vertically upwards. It should not be confused with the lesser known South Geomagnetic Pole described later.
