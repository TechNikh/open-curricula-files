---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aggregative_game
offline_file: ""
offline_thumbnail: ""
uuid: 6ddc5dff-4f3d-4d66-b192-fd575f832740
updated: 1484309032
title: Aggregative game
tags:
    - Definition
    - Generalizations
    - Properties
    - Notes
categories:
    - Game theory game classes
---
In game theory, an aggregative game is a game in which every player’s payoff is a function of the player’s own strategy and the aggregate of all players’ strategies. The concept was first proposed by Nobel laureate Reinhard Selten in 1970 who considered the case where the aggregate is the sum of the players' strategies.
