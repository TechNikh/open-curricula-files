---
version: 1
type: video
provider: YouTube
id: 7aur5h44pV4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/More%20rigorous%20Gibbs%20free%20energy%20-%20spontaneity%20relationship.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a169698f-2faa-41bb-82a0-2b49142905e2
updated: 1486069583
title: More rigorous Gibbs free energy / spontaneity relationship
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/7aur5h44pV4/default.jpg
    - https://i3.ytimg.com/vi/7aur5h44pV4/1.jpg
    - https://i3.ytimg.com/vi/7aur5h44pV4/2.jpg
    - https://i3.ytimg.com/vi/7aur5h44pV4/3.jpg
---
More formal understanding of why a negative change in Gibbs Free Energy implies a spontaneous, irreversible reaction. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/gibbs-free-energy/v/a-look-at-a-seductive-but-wrong-gibbs-spontaneity-proof?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/gibbs-free-energy/v/gibbs-free-energy-example?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
