---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/failure
offline_file: ""
offline_thumbnail: ""
uuid: 9948f10d-8cb2-424d-ae0d-4a815955d314
updated: 1484310411
title: failure
categories:
    - Dictionary
---
failure
     n 1: an act that fails; "his failure to pass the test"
     2: an event that does not accomplish its intended purpose; "the
        surprise party was a complete failure" [ant: {success}]
     3: lack of success; "he felt that his entire life had been a
        failure"; "that year there was a crop failure" [ant: {success}]
     4: a person with a record of failing; someone who loses
        consistently [syn: {loser}, {nonstarter}, {unsuccessful
        person}] [ant: {achiever}]
     5: an unexpected omission; "he resented my failure to return
        his call"; "the mechanic's failure to check the brakes"
     6: inability to discharge all your debts as they come due; "the
  ...
