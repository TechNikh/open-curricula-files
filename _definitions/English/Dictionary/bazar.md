---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bazar
offline_file: ""
offline_thumbnail: ""
uuid: d8f0bf1a-a3f4-4620-98cb-6a82d7fe5d9b
updated: 1484310462
title: bazar
categories:
    - Dictionary
---
bazar
     n 1: a street of small shops (especially in Orient) [syn: {bazaar}]
     2: a shop where a variety of goods are sold [syn: {bazaar}]
