---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bhagirathi
offline_file: ""
offline_thumbnail: ""
uuid: f87b7960-ab91-45f8-bce5-c050e37689dc
updated: 1484308432
title: Bhagirathi River
tags:
    - Etymology
    - Course
    - Dams
    - Notes
categories:
    - Uncategorized
---
The Bhāgīrathī (Pron:/ˌbʌgɪˈɹɑːθɪ/) is a turbulent Himalayan river in the Indian states of Uttarakhand, and one of the two headstreams of the Ganges, the major river of Northern India and the holy river of Hinduism. In Hindu mythology and culture, the Bhagirathi is considered the source stream of the Ganges. However, in hydrology, the other headstream, Alaknanda, is considered the source stream on account of its great length and discharge.
