---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circuit_satisfiability_problem
offline_file: ""
offline_thumbnail: ""
uuid: f272adc2-2cfd-4c61-b1ae-d9a009d81da7
updated: 1484309485
title: Circuit satisfiability problem
tags:
    - Properties
    - The Tseitin transformation
categories:
    - Computability theory
---
In theoretical computer science, the circuit satisfiability problem (also known as CIRCUIT-SAT, CircuitSAT, CSAT, etc.) is the decision problem of determining whether a given Boolean circuit has an assignment of its inputs that makes the output true.[1]
