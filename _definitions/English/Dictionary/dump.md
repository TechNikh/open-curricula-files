---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dump
offline_file: ""
offline_thumbnail: ""
uuid: 2b1946fb-89ca-4381-a105-4a86a8baaa5d
updated: 1484310541
title: dump
categories:
    - Dictionary
---
dump
     n 1: a coarse term for defecation; "he took a shit" [syn: {shit}]
     2: a piece of land where waste materials are dumped [syn: {garbage
        dump}, {trash dump}, {rubbish dump}, {wasteyard}, {waste-yard},
         {dumpsite}]
     3: (computer science) a copy of the contents of a computer
        storage device; sometimes used in debugging programs
     v 1: throw away as refuse; "No dumping in these woods!"
     2: sever all ties with, usually unceremoniously or
        irresponsibly; "The company dumped him after many years of
        service"; "She dumped her boyfriend when she fell in love
        with a rich man" [syn: {ditch}]
     3: sell at artificially low prices ...
