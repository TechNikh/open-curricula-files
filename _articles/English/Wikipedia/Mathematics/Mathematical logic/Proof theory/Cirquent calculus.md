---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cirquent_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 170b0ea5-4f5c-4a3b-b328-6e2c5d3b17cb
updated: 1484309492
title: Cirquent calculus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cirquents_vs_sequents.png
categories:
    - Proof theory
---
Cirquent calculus is a proof calculus which manipulates graph-style constructs termed cirquents, as opposed to the traditional tree-style objects such as formulas or sequents. Cirquents come in a variety of forms, but they all share one main characteristic feature, making them different from the more traditional objects of syntactic manipulation. This feature is the ability to explicitly account for possible sharing of subcomponents between different components. For instance, it is possible to write an expression where two subexpressions F and E, while neither one is a subexpression of the other, still have a common occurrence of a subexpression G (as opposed to having two different ...
