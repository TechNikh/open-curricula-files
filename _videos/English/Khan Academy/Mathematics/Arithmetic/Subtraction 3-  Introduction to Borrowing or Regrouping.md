---
version: 1
type: video
provider: YouTube
id: ZaqOUE3H1mE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Subtraction%203-%20%20Introduction%20to%20Borrowing%20or%20Regrouping.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 809a50ad-57e9-454d-a07f-734f9449e20b
updated: 1486069567
title: 'Subtraction 3-  Introduction to Borrowing or Regrouping'
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZaqOUE3H1mE/default.jpg
    - https://i3.ytimg.com/vi/ZaqOUE3H1mE/1.jpg
    - https://i3.ytimg.com/vi/ZaqOUE3H1mE/2.jpg
    - https://i3.ytimg.com/vi/ZaqOUE3H1mE/3.jpg
---
Introduction to borrowing and regrouping
