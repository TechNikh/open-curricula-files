---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Snellius%E2%80%93Pothenot_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 6c2e3827-e874-4258-b4bd-220eb0333b6d
updated: 1484309348
title: Snellius–Pothenot problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Plaquete_huis_Willebrord_Snellius.jpg
tags:
    - Formulating the equations
    - Solution algorithm
    - Geometric (graphical) solution
    - Rational trigonometry approach
    - The indeterminate case
    - Solved examples
    - Naming controversy
    - Notes
categories:
    - Trigonometry
---
The Snellius–Pothenot problem is a problem in planar surveying. Given three known points A, B and C, an observer at an unknown point P observes that the segment AC subtends an angle 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
 and the segment CB subtends an angle 
  
    
      
        β
      
    
    {\displaystyle \beta }
  
; the problem is to determine the position of the point P. (See figure; the point denoted C is between A and B as seen from P).
