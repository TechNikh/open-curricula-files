---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_cube
offline_file: ""
offline_thumbnail: ""
uuid: 5d8f1565-ac86-4773-bf4e-bc6283b029a2
updated: 1484309432
title: Partial cube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-2SAT_median_graph.svg.png
tags:
    - History
    - Examples
    - The Djoković–Winkler relation
    - Recognition
    - Dimension
    - Application to chemical graph theory
    - Notes
categories:
    - Mathematical chemistry
---
In graph theory, a partial cube is a graph that is an isometric subgraph of a hypercube.[1] In other words, a partial cube is a subgraph of a hypercube that preserves distances—the distance between any two vertices in the subgraph is the same as the distance between those vertices in the hypercube. Equivalently, a partial cube is a graph whose vertices can be labeled with bit strings of equal length in such a way that the distance between two vertices in the graph is equal to the Hamming distance between their labels. Such a labeling is called a Hamming labeling; it represents an isometric embedding of the partial cube into a hypercube.
