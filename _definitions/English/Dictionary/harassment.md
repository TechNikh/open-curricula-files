---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harassment
offline_file: ""
offline_thumbnail: ""
uuid: adf6f74b-3957-4d92-8de1-4e4e8e7cd6a5
updated: 1484310166
title: harassment
categories:
    - Dictionary
---
harassment
     n 1: a feeling of intense annoyance caused by being tormented;
          "so great was his harassment that he wanted to destroy
          his tormentors" [syn: {torment}]
     2: the act of tormenting by continued persistent attacks and
        criticism [syn: {molestation}]
