---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_irreducibility
offline_file: ""
offline_thumbnail: ""
uuid: 14a6dc72-3728-4fbb-9c7f-e2b599bcc4b1
updated: 1484308671
title: Computational irreducibility
tags:
    - The idea
    - Implications
    - Analysis
    - External links and references
categories:
    - Theoretical computer science
---
