---
version: 1
type: video
provider: YouTube
id: BZxZ_eEuJBM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Imaginary%20and%20complex%20numbers/Complex%20conjugates.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4f3d3b80-9a65-49e0-8942-7093419d96e9
updated: 1486069696
title: Complex conjugates
categories:
    - Imaginary and complex numbers
thumbnail_urls:
    - https://i3.ytimg.com/vi/BZxZ_eEuJBM/default.jpg
    - https://i3.ytimg.com/vi/BZxZ_eEuJBM/1.jpg
    - https://i3.ytimg.com/vi/BZxZ_eEuJBM/2.jpg
    - https://i3.ytimg.com/vi/BZxZ_eEuJBM/3.jpg
---
