---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regina_Tyshkevich
offline_file: ""
offline_thumbnail: ""
uuid: f730dddf-56cd-4296-9be8-cc0ecd526573
updated: 1484309481
title: Regina Tyshkevich
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/590px-Flag_of_Regina.svg.png
categories:
    - Graph theorists
---
Regina Iosifovna Tyshkevich (Russian: Регина Иосифовна Тышкевич) (born October 30, 1929) is a Belarusian mathematician, a professor of the Belarusian State University, and an expert in graph theory.[1][2]
