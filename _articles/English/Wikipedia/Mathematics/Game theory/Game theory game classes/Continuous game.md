---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Continuous_game
offline_file: ""
offline_thumbnail: ""
uuid: 280b2229-4f21-49d0-ad90-e40e7c42edf8
updated: 1484309038
title: Continuous game
tags:
    - Formal definition
    - Separable games
    - Examples
    - Separable games
    - A polynomial game
    - Non-Separable Games
    - A rational pay-off function
    - Requiring a Cantor distribution
categories:
    - Game theory game classes
---
A continuous game is a mathematical generalization, used in game theory. It extends the notion of a discrete game, where the players choose from a finite set of pure strategies. The continuous game concepts allows games to include more general sets of pure strategies, which may be uncountably infinite.
