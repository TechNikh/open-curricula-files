---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loam
offline_file: ""
offline_thumbnail: ""
uuid: e36ec56c-6f42-48d8-bf56-b101dc992608
updated: 1484310262
title: loam
categories:
    - Dictionary
---
loam
     n : a rich soil consisting of a mixture of sand and clay and
         decaying organic materials
