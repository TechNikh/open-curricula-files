---
version: 1
type: video
provider: YouTube
id: wKhX-Rh1NhQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 33f10d73-7f47-4741-bc27-3a55914410fc
updated: 1486069658
title: "St. Patrick's Day Science - Cool Science Experiments"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/wKhX-Rh1NhQ/default.jpg
    - https://i3.ytimg.com/vi/wKhX-Rh1NhQ/1.jpg
    - https://i3.ytimg.com/vi/wKhX-Rh1NhQ/2.jpg
    - https://i3.ytimg.com/vi/wKhX-Rh1NhQ/3.jpg
---
