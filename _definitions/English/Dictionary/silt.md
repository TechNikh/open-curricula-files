---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silt
offline_file: ""
offline_thumbnail: ""
uuid: 72b3b2be-ce18-46dc-9d55-251a4a3471dc
updated: 1484310262
title: silt
categories:
    - Dictionary
---
silt
     n : mud or clay or small rocks deposited by a river or lake
     v : become chocked with silt; "The river silted up" [syn: {silt
         up}]
