---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/request
offline_file: ""
offline_thumbnail: ""
uuid: 1df16807-ffcd-4bbd-b741-405ddea1c004
updated: 1484310583
title: request
categories:
    - Dictionary
---
request
     n 1: a formal message requesting something that is submitted to
          an authority [syn: {petition}, {postulation}]
     2: the verbal act of requesting [syn: {asking}]
     v 1: express the need or desire for; ask for; "She requested an
          extra bed in her room"; "She called for room service"
          [syn: {bespeak}, {call for}, {quest}]
     2: ask (a person) to do something; "She asked him to be here at
        noon"; "I requested that she type the entire manuscript"
     3: inquire for (information); "I requested information from the
        secretary"
