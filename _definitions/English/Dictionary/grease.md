---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grease
offline_file: ""
offline_thumbnail: ""
uuid: 540bab25-cd7c-422f-951c-33798f9c097e
updated: 1484310327
title: grease
categories:
    - Dictionary
---
grease
     n 1: a thick fatty oil (especially one used to lubricate
          machinery) [syn: {lubricating oil}]
     2: the state of being covered with unclean things [syn: {dirt},
         {filth}, {grime}, {soil}, {stain}, {grunge}]
     v : lubricate with grease; "grease the wheels"
