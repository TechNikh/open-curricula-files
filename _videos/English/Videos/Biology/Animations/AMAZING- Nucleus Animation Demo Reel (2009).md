---
version: 1
type: video
provider: YouTube
id: yiuvOr4JRGI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/AMAZING-%20Nucleus%20Animation%20Demo%20Reel%20%282009%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ab43178f-d255-47f2-b12e-3a67c21974a6
updated: 1486069669
title: 'AMAZING- Nucleus Animation Demo Reel (2009)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/yiuvOr4JRGI/default.jpg
    - https://i3.ytimg.com/vi/yiuvOr4JRGI/1.jpg
    - https://i3.ytimg.com/vi/yiuvOr4JRGI/2.jpg
    - https://i3.ytimg.com/vi/yiuvOr4JRGI/3.jpg
---
