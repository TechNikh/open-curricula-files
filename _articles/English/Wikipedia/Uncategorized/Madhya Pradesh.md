---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Madhya
offline_file: ""
offline_thumbnail: ""
uuid: 405455d4-b7ee-47a9-95ac-d752771c6d52
updated: 1484308426
title: Madhya Pradesh
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bateswar_%25286%2529.JPG'
tags:
    - History
    - geography
    - Location in India
    - Climate
    - Ecology
    - Flora and Fauna
    - Rivers
    - Regions
    - Cities
    - Demographics
    - population
    - Languages
    - culture
    - religion
    - Economy
    - Infrastructure
    - Energy
    - Transport
    - Aviation
    - Other
    - Media
    - Government and politics
    - Administration
    - Education
    - Sports
categories:
    - Uncategorized
---
Madhya Pradesh (MP) (/ˈmɑːdjə prəˈdɛʃ/,   (help·info), meaning Central Province) is a state in central India. Its capital is Bhopal and the largest city is Indore. Nicknamed the "heart of India" due to its geographical location in India, Madhya Pradesh is the second-largest state in the country by area. With over 75 million inhabitants, it is the fifth-largest state in India by population. It borders the states of Uttar Pradesh to the northeast, Chhattisgarh to the southeast, Maharashtra to the south, Gujarat to the west, and Rajasthan to the northwest. Its total area is 308,245 km². Before 2000, When Chattisgarh was a part of Madhya Pradesh, Madhya Pradesh was the largest ...
