---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/two-thirds
offline_file: ""
offline_thumbnail: ""
uuid: e83b5f5c-5401-45bf-a4ae-835f41ef4d91
updated: 1484310573
title: two-thirds
categories:
    - Dictionary
---
two-thirds
     n : two of three equal parts of a divisible whole
