---
version: 1
type: video
provider: YouTube
id: IdyWKZz-sbo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 626961b2-e2c3-4ff2-a882-b93ab4f264b2
updated: 1486069619
title: Determining mistakes in steps example
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/IdyWKZz-sbo/default.jpg
    - https://i3.ytimg.com/vi/IdyWKZz-sbo/1.jpg
    - https://i3.ytimg.com/vi/IdyWKZz-sbo/2.jpg
    - https://i3.ytimg.com/vi/IdyWKZz-sbo/3.jpg
---
