---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/life-time
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484402941
title: life-time
categories:
    - Dictionary
---
lifetime
     n : the period during which something is functional (as between
         birth and death); "the battery had a short life"; "he
         lived a long and happy life" [syn: {life}, {lifespan}]
