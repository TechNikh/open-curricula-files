---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aquarium
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484394781
title: aquarium
categories:
    - Dictionary
---
aquarium
     n : a tank or pool or bowl filled with water for keeping live
         fish and underwater animals [syn: {fish tank}, {marine
         museum}]
     [also: {aquaria} (pl)]
