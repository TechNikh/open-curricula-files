---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypertoric_variety
offline_file: ""
offline_thumbnail: ""
uuid: d77d110e-6114-4bad-9980-6b97239fd87e
updated: 1484309156
title: Hypertoric variety
categories:
    - Algebraic geometry
---
In mathematics, a hypertoric variety or toric hyperkähler variety is a quaternionic analog of a toric variety constructed by applying the hyper-Kähler quotient construction of N. J. Hitchin, A. Karlhede, and U. Lindström et al. (1987) to a torus acting on a quaternionic vector space. Roger Bielawski and Andrew S. Dancer (2000) gave a systematic description of hypertoric varieties.
