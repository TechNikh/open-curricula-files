---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ping-pong
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494861
title: ping-pong
categories:
    - Dictionary
---
Ping-Pong
     n : a game (trade name Ping-Pong) resembling tennis but played
         on a table with paddles and a light hollow ball [syn: {table
         tennis}]
