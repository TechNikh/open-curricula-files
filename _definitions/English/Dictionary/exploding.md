---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exploding
offline_file: ""
offline_thumbnail: ""
uuid: 521cbcfb-4709-4635-b236-8cfe4307ded4
updated: 1484310188
title: exploding
categories:
    - Dictionary
---
exploding
     adj 1: increasing suddenly and with uncontrolled rapidity; "the
            world's exploding population"
     2: (of munitions) going off; "bursting bombs"; "an exploding
        nuclear device"; "a spectacular display of detonating
        anti-tank mines" [syn: {bursting}, {detonating}]
