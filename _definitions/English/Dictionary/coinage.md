---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coinage
offline_file: ""
offline_thumbnail: ""
uuid: 25124d15-0c8b-4469-9a2b-4039bfd730e3
updated: 1484310399
title: coinage
categories:
    - Dictionary
---
coinage
     n 1: coins collectively [syn: {mintage}, {specie}, {metal money}]
     2: a newly invented word or phrase [syn: {neologism}, {neology}]
     3: the act of inventing a word or phrase [syn: {neologism}, {neology}]
