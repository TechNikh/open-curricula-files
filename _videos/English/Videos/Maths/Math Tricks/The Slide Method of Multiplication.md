---
version: 1
type: video
provider: YouTube
id: iZG25uBwU7M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ced9aadb-55f8-4f44-b9a8-c7c218149e96
updated: 1486069622
title: The Slide Method of Multiplication
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/iZG25uBwU7M/default.jpg
    - https://i3.ytimg.com/vi/iZG25uBwU7M/1.jpg
    - https://i3.ytimg.com/vi/iZG25uBwU7M/2.jpg
    - https://i3.ytimg.com/vi/iZG25uBwU7M/3.jpg
---
