---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expect
offline_file: ""
offline_thumbnail: ""
uuid: 3f7e4f06-ee50-4578-8ae3-f01a555887b5
updated: 1484310226
title: expect
categories:
    - Dictionary
---
expect
     v 1: regard something as probable or likely; "The meteorologists
          are expecting rain for tomorrow" [syn: {anticipate}]
     2: consider obligatory; request and expect; "We require our
        secretary to be on time"; "Aren't we asking too much of
        these children?"; "I expect my students to arrive in time
        for their lessons" [syn: {ask}, {require}]
     3: look forward to the probable occurrence of; "We were
        expecting a visit from our relatives"; "She is looking to
        a promotion"; "he is waiting to be drafted" [syn: {look},
        {await}, {wait}]
     4: consider reasonable or due; "I'm expecting a full
        explanation as to why these ...
