---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hindmarsh%E2%80%93Rose_model'
offline_file: ""
offline_thumbnail: ""
uuid: 413fc0df-e2a9-439c-827c-496b210cf075
updated: 1484308805
title: Hindmarsh–Rose model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Simulation_of_hrose_neuron.png
categories:
    - Nonlinear systems
---
The Hindmarsh–Rose model of neuronal activity is aimed to study the spiking-bursting behavior of the membrane potential observed in experiments made with a single neuron. The relevant variable is the membrane potential, x(t), which is written in dimensionless units. There are two more variables, y(t) and z(t), which take into account the transport of ions across the membrane through the ion channels. The transport of sodium and potassium ions is made through fast ion channels and its rate is measured by y(t), which is called the spiking variable. The transport of other ions is made through slow channels, and is taken into account through z(t), which is called the bursting variable. Then, ...
