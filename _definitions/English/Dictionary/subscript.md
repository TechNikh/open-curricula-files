---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subscript
offline_file: ""
offline_thumbnail: ""
uuid: f9fbe01c-4680-4a1a-aeb9-4366a4443299
updated: 1484310371
title: subscript
categories:
    - Dictionary
---
subscript
     adj : written or printed below and to one side of another
           character [syn: {inferior}] [ant: {adscript}, {superscript}]
     n : a character or symbol set or printed or written beneath or
         slightly below and to the side of another character [syn:
          {inferior}] [ant: {superscript}]
