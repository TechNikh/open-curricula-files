---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fuzzy_game
offline_file: ""
offline_thumbnail: ""
uuid: 6a538e01-7269-421c-94e3-f323dba44a77
updated: 1484308539
title: Fuzzy game
categories:
    - Combinatorial game theory
---
In combinatorial game theory, a fuzzy game is a game which is incomparable with the zero game: it is not greater than 0, which would be a win for Left; nor less than 0 which would be a win for Right; nor equal to 0 which would be a win for the second player to move. It is therefore a first-player win.
