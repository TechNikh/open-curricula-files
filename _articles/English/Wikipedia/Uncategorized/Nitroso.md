---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nitroso
offline_file: ""
offline_thumbnail: ""
uuid: e3c4ae0f-4f85-4a20-93e1-b95bc4cec59e
updated: 1484308403
title: Nitroso
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nitroso-compound-2D.png
tags:
    - Nitrosyl as a ligand
    - Organonitroso compounds
    - Nitrosation vs. nitrosylation
    - In food
categories:
    - Uncategorized
---
Nitroso refers to a functional group in organic chemistry which has the NO group attached to an organic moiety. As such, various nitroso groups can be categorized as C-nitroso compounds (e.g., nitrosoalkanes; R−N=O), S-nitroso compounds (nitrosothiols; RS−N=O), N-nitroso compounds (e.g., nitrosamines, R1N(−R2)−N=O), and O-nitroso compounds (alkyl nitrites; RO−N=O).
