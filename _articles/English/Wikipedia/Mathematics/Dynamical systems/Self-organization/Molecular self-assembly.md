---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_self-assembly
offline_file: ""
offline_thumbnail: ""
uuid: 3e9764db-8122-427e-89a2-24f0e2589f9b
updated: 1484308825
title: Molecular self-assembly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NTCDI_AFM2a.jpg
tags:
    - Supramolecular systems
    - Biological systems
    - Nanotechnology
    - DNA nanotechnology
    - Two-dimensional monolayers
    - External and further reading
categories:
    - Self-organization
---
Molecular self-assembly is the process by which molecules adopt a defined arrangement without guidance or management from an outside source. There are two types of self-assembly. These are intramolecular self-assembly and intermolecular self-assembly. Commonly, the term molecular self-assembly refers to intermolecular self-assembly, while the intramolecular analog is more commonly called folding.
