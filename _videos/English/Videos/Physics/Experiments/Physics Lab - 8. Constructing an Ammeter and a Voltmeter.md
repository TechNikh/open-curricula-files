---
version: 1
type: video
provider: YouTube
id: oqI_pQz8BZY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 87798105-d379-4bea-8e1b-176e82362dcb
updated: 1486069640
title: 'Physics Lab - 8. Constructing an Ammeter and a Voltmeter'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/oqI_pQz8BZY/default.jpg
    - https://i3.ytimg.com/vi/oqI_pQz8BZY/1.jpg
    - https://i3.ytimg.com/vi/oqI_pQz8BZY/2.jpg
    - https://i3.ytimg.com/vi/oqI_pQz8BZY/3.jpg
---
