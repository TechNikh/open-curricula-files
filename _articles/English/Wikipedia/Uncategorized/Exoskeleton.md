---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exoskeletons
offline_file: ""
offline_thumbnail: ""
uuid: 488e2ae4-d6d1-4910-8626-457d3326218c
updated: 1484308342
title: Exoskeleton
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dragonfly-nymph-exoskeleton.jpg
tags:
    - Role
    - Diversity
    - growth
    - Paleontological significance
    - Evolution
categories:
    - Uncategorized
---
An exoskeleton (from Greek έξω, éxō "outer" and σκελετός, skeletos "skeleton"[1]) is the external skeleton that supports and protects an animal's body, in contrast to the internal skeleton (endoskeleton) of, for example, a human. In usage, some of the larger kinds of exoskeletons are known as "shells". Examples of animals with exoskeletons include insects such as grasshoppers and cockroaches, and crustaceans such as crabs and lobsters. The shells of certain sponges and the various groups of shelled molluscs, including those of snails, clams, tusk shells, chitons and nautilus, are also exoskeletons. Some animals, such as the tortoise, have both an endoskeleton and an exoskeleton.
