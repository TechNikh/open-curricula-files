---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/past
offline_file: ""
offline_thumbnail: ""
uuid: cb279292-6bf6-41ad-bc1e-39cd5b584f62
updated: 1484310284
title: past
categories:
    - Dictionary
---
past
     adj 1: earlier than the present time; no longer current; "time
            past"; "his youth is past"; "this past Thursday"; "the
            past year" [ant: {present(a)}, {future}]
     2: of a person who has held and relinquished a position or
        office; "a retiring member of the board" [syn: {past(a)},
        {preceding(a)}, {retiring(a)}]
     3: a verb tense or other construction referring to events or
        states that existed at some previous time; "past
        participle"
     n 1: the time that has elapsed; "forget the past" [syn: {past
          times}, {yesteryear}, {yore}] [ant: {future}]
     2: a earlier period in someone's life (especially one that they
   ...
