---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bronze
offline_file: ""
offline_thumbnail: ""
uuid: 1e99428b-6cd3-45a6-9c4c-4e17272d27de
updated: 1484310377
title: bronze
categories:
    - Dictionary
---
bronze
     adj 1: of the color of bronze [syn: {bronzy}]
     2: made from or consisting of bronze
     n 1: an alloy of copper and tin and sometimes other elements;
          also any copper-base alloy containing other elements in
          place of tin
     2: a sculpture made of bronze
     v 1: give the color and appearance of bronze to something;
          "bronze baby shoes"
     2: get a tan, from wind or sun [syn: {tan}]
