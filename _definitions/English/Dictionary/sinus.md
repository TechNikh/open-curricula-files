---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sinus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484373961
title: sinus
categories:
    - Dictionary
---
sinus
     n 1: an abnormal passage leading from a suppurating cavity to the
          body surface [syn: {fistula}]
     2: any of various air-filled cavities especially in the bones
        of the skull
     3: a wide channel containing blood; does not have the coating
        of an ordinary blood vessel [syn: {venous sinus}]
