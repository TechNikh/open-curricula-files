---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mauritius
offline_file: ""
offline_thumbnail: ""
uuid: 7c72385f-fd97-4906-94e8-62dd1b75b5a0
updated: 1484308460
title: Mauritius
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bonne_-_Isle_de_France_%2528Detail%2529.jpg'
tags:
    - Etymology
    - History
    - Dutch Mauritius (1638–1710)
    - French Mauritius (1715–1810)
    - British Mauritius (1810–1968)
    - Independence (since 1968)
    - Republic (since 1992)
    - Truth and Justice Commission
    - Politics
    - Parliament
categories:
    - Uncategorized
---
Mauritius (i/məˈrɪʃəs/; French: Maurice), officially the Republic of Mauritius (French: République de Maurice), is an island nation in the Indian Ocean about 2,000 kilometres (1,200 mi) off the southeast coast of the African continent. The country includes the island of Mauritius, Rodrigues [560 kilometres (350 mi) east], and the outer islands (Agaléga, St. Brandon and two disputed territories). The islands of Mauritius and Rodrigues (172 km (107 mi) southwest) form part of the Mascarene Islands, along with nearby Réunion, a French overseas department. The area of the country is 2,040 km2. The capital and largest city is Port Louis. Mauritius was a British colonial possession ...
