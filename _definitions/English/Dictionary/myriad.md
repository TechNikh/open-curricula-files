---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/myriad
offline_file: ""
offline_thumbnail: ""
uuid: 9efb28b2-a5f9-459c-8c88-2b28558cb86f
updated: 1484310311
title: myriad
categories:
    - Dictionary
---
myriad
     adj : too numerous to be counted; "incalculable riches";
           "countless hours"; "an infinite number of reasons";
           "innumerable difficulties"; "the multitudinous seas";
           "myriad stars"; "untold thousands" [syn: {countless}, {infinite},
            {innumerable}, {innumerous}, {myriad(a)}, {multitudinous},
            {numberless}, {uncounted}, {unnumberable}, {unnumbered},
            {unnumerable}]
     n 1: a large indefinite number; "he faced a myriad of details"
     2: the cardinal number that is the product of ten and one
        thousand [syn: {ten thousand}, {10000}]
