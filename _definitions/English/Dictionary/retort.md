---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retort
offline_file: ""
offline_thumbnail: ""
uuid: eccb8bd5-b51e-4cab-b650-d3b46f3ace4a
updated: 1484310230
title: retort
categories:
    - Dictionary
---
retort
     n 1: a quick reply to a question or remark (especially a witty or
          critical one); "it brought a sharp rejoinder from the
          teacher" [syn: {rejoinder}, {return}, {riposte}, {replication},
           {comeback}, {counter}]
     2: a vessel where substances are distilled or decomposed by
        heat
     v : answer back [syn: {come back}, {repay}, {return}, {riposte},
          {rejoin}]
