---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strong
offline_file: ""
offline_thumbnail: ""
uuid: 5d5d9e5e-dd94-45f8-9ea7-9e76f113dd52
updated: 1484310330
title: strong
categories:
    - Dictionary
---
strong
     adj 1: having strength or power greater than average or expected;
            "a strong radio signal"; "strong medicine"; "a strong
            man" [ant: {weak}]
     2: used of syllables or musical beats [syn: {accented}, {heavy}]
     3: not faint or feeble; "a strong odor of burning rubber"
     4: having or wielding force or authority; "providing the ground
        soldier with increasingly potent weapons" [syn: {potent}]
     5: having a strong physiological or chemical effect; "a potent
        toxin"; "potent liquor"; "a potent cup of tea" [syn: {potent}]
        [ant: {impotent}]
     6: able to withstand attack; "an impregnable fortress";
        "fortifications that ...
