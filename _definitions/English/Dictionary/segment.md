---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/segment
offline_file: ""
offline_thumbnail: ""
uuid: 34d759a8-378c-4522-8604-61fa2c2c01f7
updated: 1484310301
title: segment
categories:
    - Dictionary
---
segment
     n 1: one of several parts or pieces that fit with others to
          constitute a whole object; "a section of a fishing rod";
          "metal sections were used below ground"; "finished the
          final segment of the road" [syn: {section}]
     2: one of the parts into which something naturally divides; "a
        segment of an orange"
     v 1: divide into segments; "segment an orange"; "segment a
          compound word" [syn: {section}]
     2: divide or split up; "The cells segmented"
