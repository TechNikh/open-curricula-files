---
version: 1
type: video
provider: YouTube
id: X3dMsEfuqQ0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5aed7990-24d7-48c4-b1b1-61b204c37b9b
updated: 1486069634
title: >
    HOW TO MAKE A HOMEMADE LAVA LAMP | Lava Lamp DIY Science
    Experiment
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/X3dMsEfuqQ0/default.jpg
    - https://i3.ytimg.com/vi/X3dMsEfuqQ0/1.jpg
    - https://i3.ytimg.com/vi/X3dMsEfuqQ0/2.jpg
    - https://i3.ytimg.com/vi/X3dMsEfuqQ0/3.jpg
---
