---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impetus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569801
title: impetus
categories:
    - Dictionary
---
impetus
     n 1: a force that moves something along [syn: {drift}, {impulsion}]
     2: the act of applying force suddenly; "the impulse knocked him
        over" [syn: {impulse}, {impulsion}]
