---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sphere
offline_file: ""
offline_thumbnail: ""
uuid: 9da38006-ea45-4258-a431-95aedbbb4bb7
updated: 1484310218
title: sphere
categories:
    - Dictionary
---
sphere
     n 1: a particular environment or walk of life; "his social sphere
          is limited"; "it was a closed area of employment"; "he's
          out of my orbit" [syn: {domain}, {area}, {orbit}, {field},
           {arena}]
     2: any spherically shaped artifact
     3: the geographical area in which one nation is very
        influential [syn: {sphere of influence}]
     4: a particular aspect of life or activity; "he was helpless in
        an important sector of his life" [syn: {sector}]
     5: a solid figure bounded by a spherical surface (including the
        space it encloses)
     6: a three-dimensional closed surface such that every point on
        the surface is ...
