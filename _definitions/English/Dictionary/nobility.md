---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nobility
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628601
title: nobility
categories:
    - Dictionary
---
nobility
     n 1: a privileged class holding hereditary titles [syn: {aristocracy}]
     2: the quality of being exalted in character or ideals or
        conduct [syn: {magnanimousness}, {grandeur}]
     3: the state of being of noble birth [syn: {noblesse}]
