---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ordinary
offline_file: ""
offline_thumbnail: ""
uuid: 6cf0db9a-4da2-4da3-a43f-8ebd2bbfd5a8
updated: 1484310313
title: ordinary
categories:
    - Dictionary
---
ordinary
     adj 1: not exceptional in any way especially in quality or ability
            or size or degree; "ordinary everyday objects";
            "ordinary decency"; "an ordinary day"; "an ordinary
            wine" [ant: {extraordinary}]
     2: lacking special distinction, rank, or status; commonly
        encountered; "average people"; "the ordinary (or common)
        man in the street" [syn: {average}]
     n 1: a judge of a probate court
     2: the expected or commonplace condition or situation; "not out
        of the ordinary"
     3: a clergyman appointed to prepare condemned prisoners for
        death
     4: an early bicycle with a very large front wheel and small
       ...
