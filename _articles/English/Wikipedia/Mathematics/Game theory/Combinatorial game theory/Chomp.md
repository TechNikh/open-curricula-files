---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chomp
offline_file: ""
offline_thumbnail: ""
uuid: ea9013b6-417d-4ba8-bfd9-ef4626fa6b8c
updated: 1484309026
title: Chomp
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bir_bardak_s%25C3%25BCt_ve_Cadbury_f%25C4%25B1nd%25C4%25B1kl%25C4%25B1_%25C3%25A7ikolata.jpg'
tags:
    - Example game
    - Who wins?
    - Generalisations of Chomp
categories:
    - Combinatorial game theory
---
Chomp is a two-player strategy game played on a rectangular chocolate bar made up of smaller square blocks (cells). The players take it in turns to choose one block and "eat it" (remove from the board), together with those that are below it and to its right. The top left block is "poisoned" and the player who eats this loses.
