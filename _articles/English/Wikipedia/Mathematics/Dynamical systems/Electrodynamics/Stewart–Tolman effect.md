---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stewart%E2%80%93Tolman_effect'
offline_file: ""
offline_thumbnail: ""
uuid: b3dfec69-b631-43a2-90f4-0c1e35b2b731
updated: 1484308767
title: Stewart–Tolman effect
categories:
    - Electrodynamics
---
The Stewart–Tolman effect is a phenomenon in electrodynamics caused by the finite mass of electrons in conducting metal, or, more generally, the finite mass of charge carriers in an electrical conductor.
