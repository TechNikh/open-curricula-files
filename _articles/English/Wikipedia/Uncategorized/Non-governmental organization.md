---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-governmental
offline_file: ""
offline_thumbnail: ""
uuid: c2ca41d4-eacc-4ec2-90b1-ff00665ff80a
updated: 1484308464
title: Non-governmental organization
tags:
    - Types
    - By orientation
    - By level of operation
    - Track II diplomacy
    - activities
    - Operational
    - Campaigning
    - Both operational and campaigning
    - Public relations
    - Project management
categories:
    - Uncategorized
---
A non-governmental organization (NGO) is a not-for-profit organization that is independent from states and international governmental organizations. They are usually funded by donations but some avoid formal funding altogether and are run primarily by volunteers. NGOs are highly diverse groups of organizations engaged in a wide range of activities, and take different forms in different parts of the world. Some may have charitable status, while others may be registered for tax exemption based on recognition of social purposes. Others may be fronts for political, religious, or other interests.
