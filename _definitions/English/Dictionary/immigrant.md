---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immigrant
offline_file: ""
offline_thumbnail: ""
uuid: a3d6b3b7-1254-4432-8688-40fc40f1f10d
updated: 1484310551
title: immigrant
categories:
    - Dictionary
---
immigrant
     n : a person who comes to a country where they were not born in
         order to settle there
