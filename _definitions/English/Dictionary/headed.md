---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headed
offline_file: ""
offline_thumbnail: ""
uuid: 96af7d5f-a76e-4e69-a109-b8c2e5c6a425
updated: 1484310429
title: headed
categories:
    - Dictionary
---
headed
     adj 1: having a heading or course in a certain direction; "westward
            headed wagons"
     2: having a heading or caption; "a headed column"; "headed
        notepaper" [ant: {unheaded}]
     3: having a head or anything that serves as a head; often used
        in combination; "headed bolts"; "three-headed Cerberus";
        "a cool-headed fighter pilot" [ant: {headless}]
     4: of leafy vegetables; having formed into a head; "headed
        cabbages"
