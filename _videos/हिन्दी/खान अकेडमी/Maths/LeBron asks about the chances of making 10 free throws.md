---
version: 1
type: video
provider: YouTube
id: 0o78n6PY0P4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d00fa0f-ef30-4c47-ac74-bd0815fd7be7
updated: 1486069618
title: LeBron asks about the chances of making 10 free throws
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/0o78n6PY0P4/default.jpg
    - https://i3.ytimg.com/vi/0o78n6PY0P4/1.jpg
    - https://i3.ytimg.com/vi/0o78n6PY0P4/2.jpg
    - https://i3.ytimg.com/vi/0o78n6PY0P4/3.jpg
---
