---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preparation
offline_file: ""
offline_thumbnail: ""
uuid: 47df546f-3202-4640-901b-8bb4dc72e4e1
updated: 1484310381
title: preparation
categories:
    - Dictionary
---
preparation
     n 1: the activity of putting or setting in order in advance of
          some act or purpose; "preparations for the ceremony had
          begun" [syn: {readying}]
     2: a substance prepared according to a formula [syn: {formulation}]
     3: the cognitive process of thinking about what you will do in
        the event of something happening; "his planning for
        retirement was hindered by several uncertainties" [syn: {planning},
         {provision}]
     4: the state of having been made ready or prepared for use or
        action (especially military action); "putting them in
        readiness"; "their preparation was more than adequate"
        [syn: {readiness}, ...
