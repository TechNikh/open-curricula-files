---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/billions
offline_file: ""
offline_thumbnail: ""
uuid: db9cd6dc-8a15-4f29-b82c-e9dd9ab9290e
updated: 1484310249
title: billions
categories:
    - Dictionary
---
billions
     n : a very large indefinite number (usually hyperbole) [syn: {millions},
          {trillions}, {zillions}, {jillions}]
