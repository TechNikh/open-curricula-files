---
version: 1
type: video
provider: YouTube
id: uUE5chiEIxU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/English%20Compound%20Sentences.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 13887a3c-ce29-48e9-ac68-22ed8f7f50ef
updated: 1486069632
title: English Compound Sentences
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/uUE5chiEIxU/default.jpg
    - https://i3.ytimg.com/vi/uUE5chiEIxU/1.jpg
    - https://i3.ytimg.com/vi/uUE5chiEIxU/2.jpg
    - https://i3.ytimg.com/vi/uUE5chiEIxU/3.jpg
---
