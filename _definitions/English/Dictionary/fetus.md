---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fetus
offline_file: ""
offline_thumbnail: ""
uuid: 1b26deba-0388-40f8-bcec-07e3b76f1e3e
updated: 1484310471
title: fetus
categories:
    - Dictionary
---
fetus
     n : an unborn or unhatched vertebrate in the later stages of
         development showing the main recognizable features of the
         mature animal [syn: {foetus}]
