---
version: 1
type: video
provider: YouTube
id: GwjiR2_7A7Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20value%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 739f376d-4d90-43f9-8e10-1459d12c721f
updated: 1486069565
title: Absolute value equations 1
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/GwjiR2_7A7Y/default.jpg
    - https://i3.ytimg.com/vi/GwjiR2_7A7Y/1.jpg
    - https://i3.ytimg.com/vi/GwjiR2_7A7Y/2.jpg
    - https://i3.ytimg.com/vi/GwjiR2_7A7Y/3.jpg
---
