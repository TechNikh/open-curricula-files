---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/temporary
offline_file: ""
offline_thumbnail: ""
uuid: 7286437c-3d09-493a-aac5-d3738e07b728
updated: 1484310369
title: temporary
categories:
    - Dictionary
---
temporary
     adj 1: not permanent; not lasting; "politics is an impermanent
            factor of life"- James Thurber; "impermanent palm
            cottages"; "a temperary arrangement"; "temporary
            housing" [syn: {impermanent}] [ant: {permanent}]
     2: lacking continuity or regularity; "an irregular worker";
        "employed on a temporary basis" [syn: {irregular}]
