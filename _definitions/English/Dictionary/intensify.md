---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intensify
offline_file: ""
offline_thumbnail: ""
uuid: 916b3d6a-352a-4ef1-bdcf-b33cc36c89b8
updated: 1484310236
title: intensify
categories:
    - Dictionary
---
intensify
     v 1: increase in extent or intensity; "The Allies escalated the
          bombing" [syn: {escalate}, {step up}] [ant: {de-escalate}]
     2: make more intense, stronger, or more marked; "The efforts
        were intensified", "Her rudeness intensified his dislike
        for her"; "Potsmokers claim it heightens their awareness";
        "This event only deepened my convictions" [syn: {compound},
         {heighten}, {deepen}]
     3: become more intense; "The debate intensified"; "His dislike
        for raw fish only deepened in Japan" [syn: {deepen}]
     4: make the chemically affected part of (a negative) denser or
        more opaque in order produce a stronger contrast ...
