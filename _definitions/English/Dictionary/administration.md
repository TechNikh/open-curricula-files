---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/administration
offline_file: ""
offline_thumbnail: ""
uuid: 295fbcea-f088-4437-baa7-f436cc9959a9
updated: 1484310451
title: administration
categories:
    - Dictionary
---
administration
     n 1: a method of tending to (especially business) matters [syn: {disposal}]
     2: the persons (or committees or departments etc.) who make up
        a body for the purpose of administering something; "he
        claims that the present administration is corrupt"; "the
        governance of an association is responsible to its
        members"; "he quickly became recognized as a member of the
        establishment" [syn: {governance}, {governing body}, {establishment},
         {brass}, {organization}, {organisation}]
     3: the act of administering medication [syn: {giving medication}]
     4: the tenure of a president; "things were quiet during the
        ...
