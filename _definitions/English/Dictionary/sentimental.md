---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sentimental
offline_file: ""
offline_thumbnail: ""
uuid: 2ac98a49-48d6-4a53-b814-d89a694105cf
updated: 1484310238
title: sentimental
categories:
    - Dictionary
---
sentimental
     adj 1: given to or marked by sentiment or sentimentality
     2: effusively or insincerely emotional; "a bathetic novel";
        "maudlin expressons of sympathy"; "mushy effusiveness"; "a
        schmaltzy song"; "sentimental soap operas"; "slushy
        poetry" [syn: {bathetic}, {drippy}, {hokey}, {maudlin}, {mawkish},
         {mushy}, {schmaltzy}, {schmalzy}, {slushy}]
