---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_topology
offline_file: ""
offline_thumbnail: ""
uuid: 85be6abf-cf87-4873-af5e-e40471346c52
updated: 1484308573
title: Combinatorial topology
categories:
    - Combinatorics
---
In mathematics, combinatorial topology was an older name for algebraic topology, dating from the time when topological invariants of spaces (for example the Betti numbers) were regarded as derived from combinatorial decompositions of spaces, such as decomposition into simplicial complexes . After the proof of the simplicial approximation theorem this approach provided rigour.
