---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bondareva%E2%80%93Shapley_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1a3d89dc-3356-44db-a59f-3307c0e82c03
updated: 1484308528
title: Bondareva–Shapley theorem
categories:
    - Cooperative games
---
The Bondareva–Shapley theorem, in game theory, describes a necessary and sufficient condition for the non-emptiness of the core of a cooperative game in characteristic function form. Specifically, the game's core is non-empty if and only if the game is balanced. The Bondareva–Shapley theorem implies that market games and convex games have non-empty cores. The theorem was formulated independently by Olga Bondareva and Lloyd Shapley in the 1960s.
