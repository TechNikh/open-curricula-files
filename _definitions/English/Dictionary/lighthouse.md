---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lighthouse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484381581
title: lighthouse
categories:
    - Dictionary
---
lighthouse
     n : a tower with a light that gives warning of shoals to passing
         ships [syn: {beacon}, {beacon light}, {pharos}]
