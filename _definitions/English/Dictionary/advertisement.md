---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advertisement
offline_file: ""
offline_thumbnail: ""
uuid: fd569232-abd3-4691-b239-8d59eb115dfa
updated: 1484310522
title: advertisement
categories:
    - Dictionary
---
advertisement
     n : a public promotion of some product or service [syn: {ad}, {advertizement},
          {advertising}, {advertizing}, {advert}]
