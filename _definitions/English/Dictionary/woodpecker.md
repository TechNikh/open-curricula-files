---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woodpecker
offline_file: ""
offline_thumbnail: ""
uuid: f637b76c-21b0-43ed-9a76-4bcc7561e553
updated: 1484310266
title: woodpecker
categories:
    - Dictionary
---
woodpecker
     n : bird with strong claws and a stiff tail adapted for climbing
         and a hard chisel-like bill for boring into wood for
         insects [syn: {peckerwood}, {pecker}]
