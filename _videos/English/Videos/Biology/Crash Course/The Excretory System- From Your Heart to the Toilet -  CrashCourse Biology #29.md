---
version: 1
type: video
provider: YouTube
id: WtrYotjYvtU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Excretory%20System-%20From%20Your%20Heart%20to%20the%20Toilet%20-%20%20CrashCourse%20Biology%20%2329.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d256385-8cb1-402e-9cad-0941c5aaa901
updated: 1486069677
title: 'The Excretory System- From Your Heart to the Toilet -  CrashCourse Biology #29'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/WtrYotjYvtU/default.jpg
    - https://i3.ytimg.com/vi/WtrYotjYvtU/1.jpg
    - https://i3.ytimg.com/vi/WtrYotjYvtU/2.jpg
    - https://i3.ytimg.com/vi/WtrYotjYvtU/3.jpg
---
