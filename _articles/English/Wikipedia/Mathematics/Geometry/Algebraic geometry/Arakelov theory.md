---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arakelov_theory
offline_file: ""
offline_thumbnail: ""
uuid: 25ba359b-c5ca-4aaf-8976-9f428891dc6e
updated: 1484309134
title: Arakelov theory
tags:
    - Background
    - Results
    - Arithmetic Chow groups
    - The arithmetic Riemann–Roch theorem
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, Arakelov theory (or Arakelov geometry) is an approach to Diophantine geometry, named for Suren Arakelov. It is used to study Diophantine equations in higher dimensions.
