---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orange
offline_file: ""
offline_thumbnail: ""
uuid: 2376d4da-a113-40bf-8ff3-a870fc6c699f
updated: 1484310305
title: orange
categories:
    - Dictionary
---
orange
     adj : similar to the color of a ripe orange [syn: {orangish}]
     n 1: round yellow to orange fruit of any of several citrus trees
     2: any of a range of colors between red and yellow [syn: {orangeness}]
     3: any citrus tree bearing oranges [syn: {orange tree}]
     4: any pigment producing the orange color
     5: a river in South Africa that flows generally westward to the
        Atlantic Ocean [syn: {Orange River}]
