---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamental_vector_field
offline_file: ""
offline_thumbnail: ""
uuid: 15f56f55-db17-4652-aa29-4d0fb384512a
updated: 1484308787
title: Fundamental vector field
tags:
    - Motivation
    - Definition
    - Applications
    - Lie groups
    - Hamiltonian group actions
categories:
    - Hamiltonian mechanics
---
In the study of mathematics and especially differential geometry, fundamental vector fields are an instrument that describes the infinitesimal behaviour of a smooth Lie group action on a smooth manifold. Such vector fields find important applications in the study of Lie theory, symplectic geometry, and the study of Hamiltonian group actions.
