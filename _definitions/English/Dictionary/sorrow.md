---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sorrow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436061
title: sorrow
categories:
    - Dictionary
---
sorrow
     n 1: an emotion of great sadness associated with loss or
          bereavement; "he tried to express his sorrow at her
          loss" [ant: {joy}]
     2: sadness associated with some wrong done or some
        disappointment; "he drank to drown his sorrows"; "he wrote
        a note expressing his regret"; "to his rue, the error cost
        him the game" [syn: {regret}, {rue}, {ruefulness}]
     3: something that causes great unhappiness; "her death was a
        great grief to John" [syn: {grief}]
     4: the state of being sad; "she tired of his perpetual sadness"
        [syn: {sadness}, {sorrowfulness}]
     v : feel grief; eat one's heart out [syn: {grieve}]
