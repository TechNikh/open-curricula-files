---
version: 1
type: video
provider: YouTube
id: xggkPaB6Z5o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20GeoPhysics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c50144c9-e7fa-4daa-8669-c2de1104bf78
updated: 1486069654
title: What is GeoPhysics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xggkPaB6Z5o/default.jpg
    - https://i3.ytimg.com/vi/xggkPaB6Z5o/1.jpg
    - https://i3.ytimg.com/vi/xggkPaB6Z5o/2.jpg
    - https://i3.ytimg.com/vi/xggkPaB6Z5o/3.jpg
---
