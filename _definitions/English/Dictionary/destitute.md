---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destitute
offline_file: ""
offline_thumbnail: ""
uuid: 744206ed-9c9b-4424-ad81-5c154587e55f
updated: 1484310535
title: destitute
categories:
    - Dictionary
---
destitute
     adj : poor enough to need help from others [syn: {impoverished}, {indigent},
            {necessitous}, {needy}, {poverty-stricken}]
