---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delivery
offline_file: ""
offline_thumbnail: ""
uuid: ee166f07-7df1-440b-8d8a-57bf33c205e6
updated: 1484310373
title: delivery
categories:
    - Dictionary
---
delivery
     n 1: the act of delivering or distributing something (as goods or
          mail); "his reluctant delivery of bad news" [syn: {bringing}]
     2: the event of giving birth; "she had a difficult delivery"
     3: your characteristic style or manner of expressing yourself
        orally; "his manner of speaking was quite abrupt"; "her
        speech was barren of southernisms"; "I detected a slight
        accent in his speech" [syn: {manner of speaking}, {speech}]
     4: the voluntary transfer of something (title or possession)
        from one party to another [syn: {livery}, {legal transfer}]
     5: (baseball) the throwing of a baseball by a pitcher to a
        batter ...
