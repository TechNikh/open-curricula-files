---
version: 1
type: article
id: https://en.wikipedia.org/wiki/40_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 074357df-9978-46a8-9fd4-9021d57c42a6
updated: 1484308955
title: 40 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Crush_40_Logo.jpg
tags:
    - In mathematics
    - In science
    - Astronomy
    - In religion
    - Judaism
    - Christianity
    - Islam
    - Yazidism
    - Funerary customs
    - Hinduism
    - Sumerian
    - In sports
    - In other fields
categories:
    - Integers
---
Despite being related to the word "four" (4), the modern spelling of 40 is "forty". The archaic form "fourty" is now considered a misspelling. The modern spelling possibly reflects a pronunciation change due to the horse–hoarse merger.[citation needed]
