---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edwin_E._Moise
offline_file: ""
offline_thumbnail: ""
uuid: c1667a0b-350d-47f9-8ebe-b86bf2d038f7
updated: 1484309091
title: Edwin E. Moise
tags:
    - Early life and education
    - Career
    - Selected publications
    - Notes
categories:
    - Geometers
---
Edwin Evariste Moise (/moʊˈiːz/;[1] December 22, 1918 – December 18, 1998)[1][2] was an American mathematician and mathematics education reformer. After his retirement from mathematics he became a literary critic of 19th century English poetry and had several notes published in that field.[1][3]
