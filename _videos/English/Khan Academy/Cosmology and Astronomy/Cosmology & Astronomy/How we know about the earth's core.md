---
version: 1
type: video
provider: YouTube
id: KL0i1RSnpfI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/How%20we%20know%20about%20the%20earth%27s%20core.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ea74912b-48b9-499a-b6bc-66fd4a8da8c4
updated: 1486069685
title: "How we know about the earth's core"
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/KL0i1RSnpfI/default.jpg
    - https://i3.ytimg.com/vi/KL0i1RSnpfI/1.jpg
    - https://i3.ytimg.com/vi/KL0i1RSnpfI/2.jpg
    - https://i3.ytimg.com/vi/KL0i1RSnpfI/3.jpg
---
