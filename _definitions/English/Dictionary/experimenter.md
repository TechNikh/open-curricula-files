---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experimenter
offline_file: ""
offline_thumbnail: ""
uuid: d52fc11c-08b9-4140-84e1-3cc77b98d5f1
updated: 1484310279
title: experimenter
categories:
    - Dictionary
---
experimenter
     n 1: a research worker who conducts experiments
     2: a person who enjoys testing innovative ideas; "she was an
        experimenter in new forms of poetry"
