---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retaining
offline_file: ""
offline_thumbnail: ""
uuid: 45ba3d76-48c3-4967-8bd5-a25be65ce120
updated: 1484310254
title: retaining
categories:
    - Dictionary
---
retaining
     adj : designed for (usually temporary) retention; "a holding pen";
           "a retaining wall" [syn: {holding}]
