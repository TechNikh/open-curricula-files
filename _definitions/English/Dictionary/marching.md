---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marching
offline_file: ""
offline_thumbnail: ""
uuid: c26bc9e3-a77a-45d4-aa11-5660c074f0c6
updated: 1484310559
title: marching
categories:
    - Dictionary
---
marching
     n : the act of marching; walking with regular steps (especially
         in a procession of some kind); "it was a long march"; "we
         heard the sound of marching" [syn: {march}]
