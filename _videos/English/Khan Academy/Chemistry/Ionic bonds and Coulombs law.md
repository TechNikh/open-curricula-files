---
version: 1
type: video
provider: YouTube
id: TwqMtO2FLYk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Ionic%20bonds%20and%20Coulombs%20law.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0219aa3f-e176-4fee-a252-e2d626424284
updated: 1486069583
title: Ionic bonds and Coulombs law
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/TwqMtO2FLYk/default.jpg
    - https://i3.ytimg.com/vi/TwqMtO2FLYk/1.jpg
    - https://i3.ytimg.com/vi/TwqMtO2FLYk/2.jpg
    - https://i3.ytimg.com/vi/TwqMtO2FLYk/3.jpg
---
Introduction to how the strength of ionic bonds is related to Coulomb's law. Example of using Coulomb's law to explain differences in melting points of ionic compounds.
