---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bamboo
offline_file: ""
offline_thumbnail: ""
uuid: 30eeb3cd-bcff-4c5e-b356-1d4c03a78889
updated: 1484310244
title: bamboo
categories:
    - Dictionary
---
bamboo
     n 1: the hard woody stems of bamboo plants; used in construction
          and crafts and fishing poles
     2: woody tropical grass having hollow woody stems; mature canes
        used for construction and furniture
