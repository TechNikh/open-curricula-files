---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/portrayed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554261
title: portrayed
categories:
    - Dictionary
---
portrayed
     adj : represented graphically by sketch or design or lines [syn: {depicted},
            {pictured}]
