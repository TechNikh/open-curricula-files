---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greatness
offline_file: ""
offline_thumbnail: ""
uuid: f7b21c7c-4fce-4f93-be86-00cb6e9049b0
updated: 1484310563
title: greatness
categories:
    - Dictionary
---
greatness
     n 1: the property possessed by something or someone of
          outstanding importance
     2: unusual largeness in size or extent [syn: {enormousness}, {grandness},
         {immenseness}, {immensity}, {sizeableness}, {vastness}]
