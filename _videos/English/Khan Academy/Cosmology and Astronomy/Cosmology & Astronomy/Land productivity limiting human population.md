---
version: 1
type: video
provider: YouTube
id: xM8szz4VB28
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Land%20productivity%20limiting%20human%20population.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e3274bbb-5805-4dd1-95d9-2d88c77ca26a
updated: 1486069685
title: Land productivity limiting human population
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/xM8szz4VB28/default.jpg
    - https://i3.ytimg.com/vi/xM8szz4VB28/1.jpg
    - https://i3.ytimg.com/vi/xM8szz4VB28/2.jpg
    - https://i3.ytimg.com/vi/xM8szz4VB28/3.jpg
---
