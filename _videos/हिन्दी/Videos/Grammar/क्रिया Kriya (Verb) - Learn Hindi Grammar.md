---
version: 1
type: video
provider: YouTube
id: QhKY40_O3wk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d2f91b96-1865-4ae5-a5b9-a2d45291e0fb
updated: 1486069629
title: 'क्रिया Kriya (Verb) - Learn Hindi Grammar'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/QhKY40_O3wk/default.jpg
    - https://i3.ytimg.com/vi/QhKY40_O3wk/1.jpg
    - https://i3.ytimg.com/vi/QhKY40_O3wk/2.jpg
    - https://i3.ytimg.com/vi/QhKY40_O3wk/3.jpg
---
