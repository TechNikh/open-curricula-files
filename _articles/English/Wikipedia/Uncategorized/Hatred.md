---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hate
offline_file: ""
offline_thumbnail: ""
uuid: 6444dc5c-7f8a-45ab-9a0f-1fe79fe3a614
updated: 1484308309
title: Hatred
tags:
    - Ethnolinguistics
    - Psychoanalytic views
    - Neurological research
    - Legal issues
    - Religious perspectives
    - Christianity
categories:
    - Uncategorized
---
Hatred or hate is a deep and extreme emotional dislike. It can be directed against individuals, groups, entities, objects, behaviors, or ideas. Hatred is often associated with feelings of anger, disgust and a disposition towards hostility.
