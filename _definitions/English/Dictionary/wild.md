---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wild
offline_file: ""
offline_thumbnail: ""
uuid: 4a304a20-c89e-4679-995d-c7a70fe01fb0
updated: 1484310246
title: wild
categories:
    - Dictionary
---
wild
     adj 1: marked by extreme lack of restraint or control; "wild
            ideas"; "wild talk"; "wild originality"; "wild
            parties" [ant: {tame}]
     2: in a natural state; not tamed or domesticated or cultivated;
        "wild geese"; "edible wild plants" [syn: {untamed}] [ant:
        {tame}]
     3: in a state of extreme emotion; "wild with anger"; "wild with
        grief"
     4: deviating widely from an intended course; "a wild bullet";
        "a wild pitch"
     5: (of colors or sounds) intensely vivid or loud; "a violent
        clash of colors"; "her dress was a violent red"; "a
        violent noise"; "wild colors"; "wild shouts" [syn: {violent}]
     6: not ...
