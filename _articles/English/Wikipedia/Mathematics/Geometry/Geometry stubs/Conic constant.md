---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conic_constant
offline_file: ""
offline_thumbnail: ""
uuid: 8197cf11-b3f2-4396-819f-defcf95b668f
updated: 1484309359
title: Conic constant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Conic_constant.svg.png
categories:
    - Geometry stubs
---
In geometry, the conic constant (or Schwarzschild constant,[1] after Karl Schwarzschild) is a quantity describing conic sections, and is represented by the letter K. For negative K it is given by
