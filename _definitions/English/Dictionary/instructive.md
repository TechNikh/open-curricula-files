---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instructive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476561
title: instructive
categories:
    - Dictionary
---
instructive
     adj 1: serving to instruct of enlighten or inform [syn: {informative}]
            [ant: {uninstructive}]
     2: tending to increase knowledge or dissipate ignorance; "an
        enlightening glimpse of government in action" [syn: {enlightening},
         {informative}] [ant: {unenlightening}]
