---
version: 1
type: video
provider: YouTube
id: 37DSsCmKo08
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c543b09b-c7c3-4a9e-9893-043e7adc183e
updated: 1486069617
title: Testing similarity through transformations
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/37DSsCmKo08/default.jpg
    - https://i3.ytimg.com/vi/37DSsCmKo08/1.jpg
    - https://i3.ytimg.com/vi/37DSsCmKo08/2.jpg
    - https://i3.ytimg.com/vi/37DSsCmKo08/3.jpg
---
