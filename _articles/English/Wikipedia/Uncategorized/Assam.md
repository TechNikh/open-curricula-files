---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Assom
offline_file: ""
offline_thumbnail: ""
uuid: 658fc599-bc41-46de-ac09-d49f4d5be391
updated: 1484308415
title: Assam
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Sukapha.jpg
tags:
    - Etymology
    - History
    - Pre-history
    - Mythology
    - Ancient
    - Medieval
    - Colonial era
    - Modern history
    - geography
    - Climate
    - flooding
    - Fauna
    - Flora
    - Geology
    - Demographics
    - population
    - Religions
    - Languages
    - Government and politics
    - Local government
    - Education
    - Universities
    - Medical colleges
    - Engineering and technological colleges
    - Agriculture colleges
    - Law colleges
    - Economy
    - Tea plantations
    - Macro-economy
    - Employment
    - Agriculture
    - Industry
    - Tourism
    - culture
    - Symbols
    - Festivals and traditions
    - Music, dance, and drama
    - Cuisine
    - Literature
    - Fine arts
    - Traditional crafts
    - Media
    - Notes
categories:
    - Uncategorized
---
Assam (English pronunciation: /əˈsæm/  listen (help·info); Assamese: অসম Ôxôm [ɔˈxɔm]  listen (help·info); Bodo: आसाम Asam; Bengali: আসাম Âshám [ɑːˈʃɑːm]) is a state in northeastern India. Located south of the eastern Himalayas, Assam comprises the Brahmaputra Valley and the Barak Valley along with the Karbi Anglong and Dima Hasao districts with an area of 30,285 sq mi (78,440 km2). Assam, along with Arunachal Pradesh, Nagaland, Manipur, Mizoram, Tripura, and Meghalaya, is one of the Seven Sister States. Geographically, Assam and these states are connected to the rest of India via a 22 kilometres (14 mi) strip of land in West Bengal called the ...
