---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Svante_Janson
offline_file: ""
offline_thumbnail: ""
uuid: a5e4b531-ad12-4845-ab92-2c911a72a063
updated: 1484309468
title: Svante Janson
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Carleson_cropped.jpg
tags:
    - Biography
    - From prodigy to docent
    - Professorships
    - Awards
    - Works by Janson
    - Books
    - Selected articles
categories:
    - Graph theorists
---
Svante Janson (born 21 May 1955) is a Swedish mathematician. A member of the Royal Swedish Academy of Sciences since 1994, Janson has been the chaired professor of mathematics at Uppsala University since 1987.
