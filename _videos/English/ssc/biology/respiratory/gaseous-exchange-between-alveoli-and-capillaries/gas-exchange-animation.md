---
title: Gas Exchange animation
youtube_id: Z1h29R82mVc
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
