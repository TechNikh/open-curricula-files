---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wheel
offline_file: ""
offline_thumbnail: ""
uuid: aca6837d-7808-4c88-84ed-584b540696a6
updated: 1484310409
title: wheel
categories:
    - Dictionary
---
wheel
     n 1: a simple machine consisting of a circular frame with spokes
          (or a solid disc) that can rotate on a shaft or axle (as
          in vehicles or other machines)
     2: a handwheel that is used for steering [syn: {steering wheel}]
     3: a circular helm to control the rudder of a vessel
     4: game equipment consisting of a rotating wheel with slots
        that is used for gambling; players bet on which slot the
        roulette ball will stop in [syn: {roulette wheel}]
     5: an instrument of torture that stretches or disjoints or
        mutilates victims [syn: {rack}]
     6: a wheeled vehicle that has two wheels and is moved by foot
        pedals [syn: ...
