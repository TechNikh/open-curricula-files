---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/painting
offline_file: ""
offline_thumbnail: ""
uuid: 04048517-d1f5-49a0-8175-d00c279c9ebc
updated: 1484310379
title: painting
categories:
    - Dictionary
---
painting
     n 1: graphic art consisting of an artistic composition made by
          applying paints to a surface; "a small painting by
          Picasso"; "he bought the painting as an investment";
          "his pictures hang in the Louvre" [syn: {picture}]
     2: creating a picture with paints; "he studied painting and
        sculpture for many years"
     3: the act of applying paint to a surface; "you can finish the
        job of painting faster with a roller than with a brush"
     4: the occupation of a house painter; "house painting was the
        only craft he knew" [syn: {house painting}]
