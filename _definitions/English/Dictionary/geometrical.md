---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geometrical
offline_file: ""
offline_thumbnail: ""
uuid: 86bb949f-4a88-4a1e-b663-4895763fd1a5
updated: 1484310290
title: geometrical
categories:
    - Dictionary
---
geometrical
     adj 1: of or relating to or determined by geometry [syn: {geometric}]
     2: characterized by simple geometric forms in design and
        decoration; "a buffalo hide painted with red and black
        geometric designs" [syn: {geometric}]
