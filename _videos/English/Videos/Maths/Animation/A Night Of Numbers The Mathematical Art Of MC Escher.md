---
version: 1
type: video
provider: YouTube
id: UQq-kBSgxI8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d50b53ed-7d57-48c8-adbb-b503e47ad2a4
updated: 1486069644
title: A Night Of Numbers The Mathematical Art Of MC Escher
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/UQq-kBSgxI8/default.jpg
    - https://i3.ytimg.com/vi/UQq-kBSgxI8/1.jpg
    - https://i3.ytimg.com/vi/UQq-kBSgxI8/2.jpg
    - https://i3.ytimg.com/vi/UQq-kBSgxI8/3.jpg
---
