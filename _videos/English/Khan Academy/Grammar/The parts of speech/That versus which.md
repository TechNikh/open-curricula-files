---
version: 1
type: video
provider: YouTube
id: 6Js8tBCfbWk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/That%20versus%20which.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3fe8eaac-2946-4332-8185-c948dbc9fe9a
updated: 1486069599
title: That versus which
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/6Js8tBCfbWk/default.jpg
    - https://i3.ytimg.com/vi/6Js8tBCfbWk/1.jpg
    - https://i3.ytimg.com/vi/6Js8tBCfbWk/2.jpg
    - https://i3.ytimg.com/vi/6Js8tBCfbWk/3.jpg
---
Much has been made of the distinction between "that" and "which" in English, but it can mostly be summed up in two points: 1. "That" doesn't work so well with commas. 2. "Which" doesn't work so well with people. David the Grammarian explains.

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/who-versus-whom-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar 

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/relative-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
