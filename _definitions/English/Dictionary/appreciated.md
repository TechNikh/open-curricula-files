---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appreciated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484398381
title: appreciated
categories:
    - Dictionary
---
appreciated
     adj 1: giving pleasure or satisfaction [syn: {gratifying}, {pleasing},
             {satisfying}]
     2: fully understood or grasped; "dangers not yet appreciated";
        "these apprehended truths"; "a thing comprehended is a
        thing known as fully as it can be known" [syn: {apprehended},
         {comprehended}]
