---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flag
offline_file: ""
offline_thumbnail: ""
uuid: f485ffa1-718f-4a04-8585-f424ea40d932
updated: 1484310563
title: flag
categories:
    - Dictionary
---
flag
     n 1: emblem usually consisting of a rectangular piece of cloth of
          distinctive design
     2: plants with sword-shaped leaves and erect stalks bearing
        bright-colored flowers composed of three petals and three
        drooping sepals [syn: {iris}, {fleur-de-lis}, {sword lily}]
     3: a rectangular piece of fabric used as a signalling device
        [syn: {signal flag}]
     4: a listing printed in all issues of a newspaper or magazine
        (usually on the editorial page) that gives the name of the
        publication and the names of the editorial staff, etc.
        [syn: {masthead}]
     5: flagpole used to mark the position of the hole on a golf
        ...
