---
version: 1
type: video
provider: YouTube
id: r4wlwNP1YtA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 188dc7a6-571f-4f82-89eb-69115401aeb7
updated: 1486069661
title: Amazing Candle Trick!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/r4wlwNP1YtA/default.jpg
    - https://i3.ytimg.com/vi/r4wlwNP1YtA/1.jpg
    - https://i3.ytimg.com/vi/r4wlwNP1YtA/2.jpg
    - https://i3.ytimg.com/vi/r4wlwNP1YtA/3.jpg
---
