---
version: 1
type: video
provider: YouTube
id: 1sQX1st5bbw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Neutron%20Generators%20using%20Particle%20Accelerators.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 26759590-8a26-47f9-94b0-240060dc5fa0
updated: 1486069656
title: Neutron Generators using Particle Accelerators
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1sQX1st5bbw/default.jpg
    - https://i3.ytimg.com/vi/1sQX1st5bbw/1.jpg
    - https://i3.ytimg.com/vi/1sQX1st5bbw/2.jpg
    - https://i3.ytimg.com/vi/1sQX1st5bbw/3.jpg
---
