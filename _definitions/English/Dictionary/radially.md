---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radially
offline_file: ""
offline_thumbnail: ""
uuid: 0efef17b-5f6d-4ff7-a6c1-eb79e5012a4d
updated: 1484310426
title: radially
categories:
    - Dictionary
---
radially
     adv : in a radial manner; "an imaginative dispersal of the pews
           radially from the central focus of the pulpit"
