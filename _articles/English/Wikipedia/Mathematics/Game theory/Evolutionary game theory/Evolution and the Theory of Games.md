---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Evolution_and_the_Theory_of_Games
offline_file: ""
offline_thumbnail: ""
uuid: d9f53c31-1ae0-4601-8691-ad4b4e8b73d4
updated: 1484308524
title: Evolution and the Theory of Games
tags:
    - Oveview
categories:
    - Evolutionary game theory
---
Evolution and the Theory of Games is a book by the British evolutionary biologist John Maynard Smith on evolutionary game theory.[1][2][3] The book was initially published in December 1982 by Cambridge University Press.
