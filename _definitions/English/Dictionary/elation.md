---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484314021
title: elation
categories:
    - Dictionary
---
elation
     n 1: an exhilarating psychological state of pride and optimism;
          an absence of depression [ant: {depression}]
     2: a feeling of joy and pride [syn: {high spirits}]
