---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 59e0a203-4220-496d-86d0-df4a75ae5555
updated: 1484308773
title: Arithmetic combinatorics
tags:
    - Scope
    - Important results
    - "Szemerédi's theorem"
    - Green-Tao theorem and extensions
    - Example
    - Extensions
    - Notes
categories:
    - Ergodic theory
---
