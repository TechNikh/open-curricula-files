---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thriving
offline_file: ""
offline_thumbnail: ""
uuid: d84aca17-057e-4fc3-8a71-b67e11810db8
updated: 1484310181
title: thriving
categories:
    - Dictionary
---
thriving
     adj 1: very lively and profitable; "flourishing businesses"; "a
            palmy time for stockbrokers"; "a prosperous new
            business"; "doing a roaring trade"; "a thriving
            tourist center"; "did a thriving business in orchids"
            [syn: {booming}, {flourishing}, {palmy}, {prospering},
             {prosperous}, {roaring}]
     2: having or showing vigorous vegetal or animal life;
        "flourishing crops"; "flourishing chicks"; "a growing
        boy"; "fast-growing weeds"; "a thriving deer population"
        [syn: {flourishing}, {growing}]
