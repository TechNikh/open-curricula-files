---
version: 1
type: article
id: https://en.wikipedia.org/wiki/9_(number)
offline_file: ""
offline_thumbnail: ""
uuid: c3504d94-4d19-4810-9a38-e7ca506de4a5
updated: 1484308948
title: 9 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-9ball_rack_2.jpg
tags:
    - Alphabets and codes
    - Commerce
    - Culture and mythology
    - Indian culture
    - Chinese culture
    - Ancient Egypt
    - European culture
    - Greek mythology
    - Evolution of the glyph
    - Idioms and popular phrases
    - Internet
    - Literature
    - Mathematics
    - List of basic calculations
    - Numeral systems
    - Probability
    - Organizations
    - Places and thoroughfares
    - Religion and philosophy
    - Science
    - Astronomy
    - chemistry
    - Physiology
    - Sports
    - Auto racing
    - Baseball
    - Billiards
    - Rugby
    - Soccer
    - All sports
    - Technology
    - Other fields
categories:
    - Integers
---
