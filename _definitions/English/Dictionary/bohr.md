---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bohr
offline_file: ""
offline_thumbnail: ""
uuid: b2b6464f-781d-47b0-83ad-fdbcb1277a34
updated: 1484310387
title: bohr
categories:
    - Dictionary
---
Bohr
     n : Danish physicist who studied atomic structure and
         radiations; the Bohr theory of the atom accounted for the
         spectrum of hydrogen (1885-1962) [syn: {Niels Bohr}, {Niels
         Henrik David Bohr}]
