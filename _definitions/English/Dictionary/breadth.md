---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breadth
offline_file: ""
offline_thumbnail: ""
uuid: f47696ef-90fb-413d-997c-dde55357bb6a
updated: 1484310297
title: breadth
categories:
    - Dictionary
---
breadth
     n 1: an ability to understand a broad range of topics; "a teacher
          must have a breadth of knowledge of the subject" [syn: {comprehensiveness}]
     2: the extent of something from side to side [syn: {width}]
