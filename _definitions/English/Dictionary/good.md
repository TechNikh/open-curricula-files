---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/good
offline_file: ""
offline_thumbnail: ""
uuid: 6ef1e676-72e1-4483-a3ce-22779d7f1181
updated: 1484310359
title: good
categories:
    - Dictionary
---
good
     adj 1: having desirable or positive qualities especially those
            suitable for a thing specified; "good news from the
            hospital"; "a good report card"; "when she was good
            she was very very good"; "a good knife is one good for
            cutting"; "this stump will make a good picnic table";
            "a good check"; "a good joke"; "a good exterior
            paint"; "a good secretary"; "a good dress for the
            office" [ant: {bad}]
     2: having the normally expected amount; "gives full measure";
        "gives good measure"; "a good mile from here" [syn: {full}]
     3: morally admirable [ant: {evil}]
     4: deserving of esteem and ...
