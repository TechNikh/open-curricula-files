---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mosquito
offline_file: ""
offline_thumbnail: ""
uuid: 15ebe426-2d4c-4ea2-a84b-0782a8a8d8f5
updated: 1484310539
title: mosquito
categories:
    - Dictionary
---
mosquito
     n : two-winged insect whose female has a long proboscis to
         pierce the skin and suck the blood of humans and animals
     [also: {mosquitoes} (pl)]
