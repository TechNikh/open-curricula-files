---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Relations_between_heat_capacities
offline_file: ""
offline_thumbnail: ""
uuid: 2b81adea-a231-486d-a90d-a315eba64cbe
updated: 1484308855
title: Relations between heat capacities
tags:
    - Relations
    - Derivation
    - Ideal gas
categories:
    - Thermodynamics
---
In thermodynamics, the heat capacity at constant volume, 
  
    
      
        
          C
          
            V
          
        
      
    
    {\displaystyle C_{V}}
  
, and the heat capacity at constant pressure, 
  
    
      
        
          C
          
            P
          
        
      
    
    {\displaystyle C_{P}}
  
, are extensive properties that have the magnitude of energy divided by temperature.
