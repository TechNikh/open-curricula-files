---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grothendieck_local_duality
offline_file: ""
offline_thumbnail: ""
uuid: adeec9ff-bf3d-42c8-83d6-376a23b9fcb3
updated: 1484309236
title: Grothendieck local duality
categories:
    - Duality theories
---
In commutative algebra, Grothendieck local duality is a duality theorem for cohomology of modules over local rings, analogous to Serre duality of coherent sheaves.
