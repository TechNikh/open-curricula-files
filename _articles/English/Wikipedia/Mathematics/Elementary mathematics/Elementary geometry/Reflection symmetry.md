---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reflection_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 0f2d669c-833f-45f6-83d8-b6fe35e9ba93
updated: 1484308935
title: Reflection symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Symmetry.png
tags:
    - Symmetric function
    - Symmetric geometrical shapes
    - Mathematical equivalents
    - Advanced types of reflection symmetry
    - In nature
    - In architecture
    - Bibliography
    - General
    - Advanced
categories:
    - Elementary geometry
---
Reflection symmetry, line symmetry, mirror symmetry, mirror-image symmetry, is symmetry with respect to reflection. That is, a figure which does not change upon undergoing a reflection has reflectional symmetry.
