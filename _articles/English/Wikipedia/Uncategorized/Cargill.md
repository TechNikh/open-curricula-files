---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cargill
offline_file: ""
offline_thumbnail: ""
uuid: ffb4433e-02b3-48ae-8d22-191773845624
updated: 1484308460
title: Cargill
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2009-0612-07-CargillLakeOffice.jpg
tags:
    - History
    - 20th century
    - 2000s
    - Board of directors
    - Countries of operation
    - Africa
    - Asia Pacific
    - Cargill in India
    - Europe
    - Latin America
categories:
    - Uncategorized
---
Cargill, Inc. is an American privately held[2][3] global corporation based in Minnetonka, Minnesota, a Minneapolis suburb. Founded in 1865, it is now the largest privately held corporation in the United States in terms of revenue.[4] If it were a public company, it would rank, as of 2015, number 12 on the Fortune 500, behind McKesson and ahead of AT&T.[5]
