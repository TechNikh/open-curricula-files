---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/association
offline_file: ""
offline_thumbnail: ""
uuid: 6d8871cb-53d1-40f1-958b-40c2b15a1ad7
updated: 1484310531
title: association
categories:
    - Dictionary
---
association
     n 1: a formal organization of people or groups of people; "he
          joined the Modern Language Association"
     2: the act of consorting with or joining with others; "you
        cannot be convicted of criminal guilt by association"
     3: the state of being connected together as in memory or
        imagination; "his association of his father with being
        beaten was too strong to break" [ant: {disassociation}]
     4: a social or business relationship; "a valuable financial
        affiliation"; "he was sorry he had to sever his ties with
        other members of the team"; "many close associations with
        England" [syn: {affiliation}, {tie}, {tie-up}]
    ...
