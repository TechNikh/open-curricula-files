---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-adic_number
offline_file: ""
offline_thumbnail: ""
uuid: d72f2403-0272-48bd-bbb2-4d7a4d33cf32
updated: 1484308652
title: P-adic number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3-adic_integers_with_dual_colorings.svg.png
tags:
    - Introduction
    - p-adic expansions
    - Notation
    - Constructions
    - Analytic approach
    - Algebraic approach
    - Properties
    - Cardinality
    - Topology
    - Metric completions and algebraic closures
    - Multiplicative group of Qp
    - Analysis on Qp
    - Rational arithmetic
    - Generalizations and related concepts
    - Local–global principle
    - Footnotes
    - Notes
    - Citations
categories:
    - Number theory
---
In mathematics the p-adic number system for any prime number p extends the ordinary arithmetic of the rational numbers in a way different from the extension of the rational number system to the real and complex number systems. The extension is achieved by an alternative interpretation of the concept of "closeness" or absolute value. In particular, p-adic numbers have the interesting property that they are said to be close when their difference is divisible by a high power of p: the higher the power, the closer they are. This property enables p-adic numbers to encode congruence information in a way that turns out to have powerful applications in number theory—including, for example, in ...
