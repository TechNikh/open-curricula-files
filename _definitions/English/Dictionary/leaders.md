---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leaders
offline_file: ""
offline_thumbnail: ""
uuid: d590f2da-a0fe-4739-a947-363508130af0
updated: 1484310441
title: leaders
categories:
    - Dictionary
---
leaders
     n : the body of people who lead a group; "the national
         leadership adopted his plan" [syn: {leadership}]
