---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outclassed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425741
title: outclassed
categories:
    - Dictionary
---
outclassed
     adj : decisively surpassed by something else so as to appear to be
           of a lower class
