---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Potatoes
offline_file: ""
offline_thumbnail: ""
uuid: c3bf7bad-a721-421b-b1bd-8c1c7a2a1c11
updated: 1484308378
title: Potato
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Potato_flowers.jpg
tags:
    - Etymology
    - characteristics
    - Genetics
    - History
    - Role in world food supply
    - International Year of the Potato
    - Nutrition
    - Comparison to other major staple foods
    - Toxicity
    - Growth and cultivation
    - Storage
    - Yield
    - Varieties
    - Blue varieties
    - Genetically engineered potatoes
    - Pests
    - Pesticides
    - Uses
    - Culinary uses
    - Grading
    - Latin America
    - European cuisine
    - North America
    - South Asia
    - East Asia
    - art
    - Notes
    - Sources
categories:
    - Uncategorized
---
The potato is a starchy, tuberous crop from the perennial nightshade Solanum tuberosum. The word "potato" may refer either to the plant itself or to the edible tuber.[2] In the Andes, where the species is indigenous, there are some other closely related cultivated potato species. Potatoes were introduced outside the Andes region approximately four centuries ago,[3] and have since become an integral part of much of the world's food supply. It is the world's fourth-largest food crop, following maize, wheat, and rice.[4] The green leaves and green skins of tubers exposed to the light are toxic.
