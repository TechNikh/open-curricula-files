---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/temporarily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651761
title: temporarily
categories:
    - Dictionary
---
temporarily
     adv : for a limited time only; not permanently; "he will work here
           temporarily"; "he was brought out of retirement
           temporarily"; "a power failure temporarily darkened the
           town" [ant: {permanently}]
