---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anxiety
offline_file: ""
offline_thumbnail: ""
uuid: a1c61f27-d097-42a9-83c2-32780954814a
updated: 1484310563
title: anxiety
categories:
    - Dictionary
---
anxiety
     n 1: a relatively permanent state of anxiety occurring in a
          variety of mental disorders [syn: {anxiousness}]
     2: a vague unpleasant emotion that is experienced in
        anticipation of some (usually ill-defined) misfortune
