---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pander
offline_file: ""
offline_thumbnail: ""
uuid: 2f06a2ad-540b-427a-abf9-1a8257f5ef95
updated: 1484310186
title: pander
categories:
    - Dictionary
---
pander
     n : someone who procures customers for whores (in England they
         call a pimp a ponce) [syn: {pimp}, {procurer}, {panderer},
          {pandar}, {fancy man}, {ponce}]
     v 1: yield (to); give satisfaction to [syn: {gratify}, {indulge}]
     2: arrange for sexual partners for others [syn: {pimp}, {procure}]
