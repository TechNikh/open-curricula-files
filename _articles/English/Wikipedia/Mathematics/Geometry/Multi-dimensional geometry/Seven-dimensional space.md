---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seven-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: e2a2b301-36b5-4fd9-a569-d797ca2efc64
updated: 1484309287
title: Seven-dimensional space
tags:
    - Geometry
    - 7-polytope
    - 6-sphere
    - Applications
    - Cross product
    - Exotic spheres
categories:
    - Multi-dimensional geometry
---
In mathematics, a sequence of n real numbers can be understood as a location in n-dimensional space. When n = 7, the set of all such locations is called 7-dimensional space. Often such a space is studied as a vector space, without any notion of distance. Seven-dimensional Euclidean space is seven-dimensional space equipped with a Euclidean metric, which is defined by the dot product.[disputed – discuss]
