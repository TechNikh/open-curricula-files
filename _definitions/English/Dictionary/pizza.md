---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pizza
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468881
title: pizza
categories:
    - Dictionary
---
pizza
     n : Italian open pie made of thin bread dough spread with a
         spiced mixture of e.g. tomato sauce and cheese [syn: {pizza
         pie}]
