---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clothing
offline_file: ""
offline_thumbnail: ""
uuid: be990ee2-f949-4cf7-ad91-4901cf4923c8
updated: 1484310469
title: clothing
categories:
    - Dictionary
---
clothing
     n : a covering designed to be worn on a person's body [syn: {article
         of clothing}, {vesture}, {wear}]
