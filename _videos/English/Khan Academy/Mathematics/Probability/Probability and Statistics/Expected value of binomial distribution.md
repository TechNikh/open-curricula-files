---
version: 1
type: video
provider: YouTube
id: SqcxYnNlI3Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Expected%20value%20of%20binomial%20distribution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e467a36f-53ee-4cee-8035-c155736975ee
updated: 1486069690
title: Expected value of binomial distribution
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/SqcxYnNlI3Y/default.jpg
    - https://i3.ytimg.com/vi/SqcxYnNlI3Y/1.jpg
    - https://i3.ytimg.com/vi/SqcxYnNlI3Y/2.jpg
    - https://i3.ytimg.com/vi/SqcxYnNlI3Y/3.jpg
---
