---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Conchoid_of_D%C3%BCrer'
offline_file: ""
offline_thumbnail: ""
uuid: 0156ceb3-723e-4b93-8188-4bf9fa324b4e
updated: 1484309357
title: Conchoid of Dürer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-DuererMuschellinie.png
tags:
    - Construction
    - Equation
    - Properties
    - History
categories:
    - Geometry stubs
---
The conchoid of Dürer, also called Dürer's shell curve, is a variant of a conchoid or plane algebraic curve, named after Albrecht Dürer. It is not a true conchoid.
