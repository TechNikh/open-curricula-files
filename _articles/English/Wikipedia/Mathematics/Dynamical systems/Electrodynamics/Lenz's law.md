---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lenz%27s_law'
offline_file: ""
offline_thumbnail: ""
uuid: aa4bae1a-743f-4e27-b6d4-d01e30fd7497
updated: 1484308754
title: "Lenz's law"
tags:
    - Opposing currents
    - Example
    - Detailed interaction of charges in these currents
    - Field energy
    - Conservation of momentum
categories:
    - Electrodynamics
---
Lenz's law is shown by the negative sign in Faraday's law of induction:
