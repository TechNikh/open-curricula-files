---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moon
offline_file: ""
offline_thumbnail: ""
uuid: 4d76ea87-df0d-4086-8576-7ead6332aab7
updated: 1484310178
title: moon
categories:
    - Dictionary
---
moon
     n 1: the natural satellite of the Earth; "the average distance to
          the moon is 384,400 kilometers"; "men first stepped on
          the moon in 1969"
     2: any object resembling a moon; "he made a moon lamp that he
        used as a night light"; "the clock had a moon that showed
        various phases"
     3: the period between successive new moons (29.531 days) [syn:
        {lunar month}, {lunation}, {synodic month}]
     4: the light of the moon; "moonlight is the smuggler's enemy";
        "the moon was bright enough to read by" [syn: {moonlight},
         {moonshine}]
     5: United States religious leader (born in Korea) who founded
        the Unification ...
