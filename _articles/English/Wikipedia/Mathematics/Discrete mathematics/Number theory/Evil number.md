---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evil_number
offline_file: ""
offline_thumbnail: ""
uuid: d2dc6f5e-9bef-415c-b5bb-33a35c7e875a
updated: 1484308636
title: Evil number
categories:
    - Number theory
---
The first evil numbers are: 0, 3, 5, 6, 9, 10, 12, 15, 17, 18, 20, 23, 24, 27, 29, 30, 33, 34, 36, 39 ...[1]
