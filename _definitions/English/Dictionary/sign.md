---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sign
offline_file: ""
offline_thumbnail: ""
uuid: 760afba2-1bb9-4743-9c1e-63b2d8a859b4
updated: 1484310214
title: sign
categories:
    - Dictionary
---
sign
     adj : used of the language of the deaf [syn: {gestural}, {sign(a)},
            {signed}, {sign-language(a)}]
     n 1: a perceptible indication of something not immediately
          apparent (as a visible clue that something has
          happened); "he showed signs of strain"; "they welcomed
          the signs of spring" [syn: {mark}]
     2: a public display of a (usually written) message; "he posted
        signs in all the shop windows"
     3: any communication that encodes a message; "signals from the
        boat suddenly stopped" [syn: {signal}, {signaling}]
     4: structure displaying a board on which advertisements can be
        posted; "the highway was lined with ...
