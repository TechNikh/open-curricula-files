---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s_conjecture_on_arithmetic_progressions'
offline_file: ""
offline_thumbnail: ""
uuid: b7d0d5cf-068a-4673-b4d9-5d2497759c57
updated: 1484308572
title: Erdős conjecture on arithmetic progressions
tags:
    - History
    - Progress and related results
categories:
    - Combinatorics
---
Erdős' conjecture on arithmetic progressions, often referred to as the Erdős–Turán conjecture, is a conjecture in arithmetic combinatorics (not to be confused with the Erdős–Turán conjecture on additive bases). It states that if the sum of the reciprocals of the members of a set A of positive integers diverges, then A contains arbitrarily long arithmetic progressions.
