---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Randi%C4%87%27s_molecular_connectivity_index'
offline_file: ""
offline_thumbnail: ""
uuid: cd86fcc0-a018-43b8-aeee-eb2cd8f343b9
updated: 1484309422
title: "Randić's molecular connectivity index"
categories:
    - Graph invariants
---
The Randić index, also known as the connectivity index, of a graph is the sum of bond contributions 
  
    
      
        1
        
          /
        
        (
        
          d
          
            i
          
        
        
          d
          
            j
          
        
        
          )
          
            1
            
              /
            
            2
          
        
      
    
    {\displaystyle 1/(d_{i}d_{j})^{1/2}}
  
 where 
  
    
      
        
          d
          
            i
          
        
      
    
    {\displaystyle d_{i}}
  
 and 
  
    
      
        
          d
          
            j
          
        
      ...
