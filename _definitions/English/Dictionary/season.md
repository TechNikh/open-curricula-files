---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/season
offline_file: ""
offline_thumbnail: ""
uuid: caf69503-2fe2-4309-ae0a-90340a884581
updated: 1484310303
title: season
categories:
    - Dictionary
---
season
     n 1: a period of the year marked by special events or activities
          in some field; "he celebrated his 10th season with the
          ballet company"; "she always looked forward to the
          avocado season"
     2: one of the natural periods into which the year is divided by
        the equinoxes and solstices or atmospheric conditions;
        "the regular sequence of the seasons" [syn: {time of year}]
     3: a recurrent time marked by major holidays; "it was the
        Christmas season"
     v 1: lend flavor to; "Season the chicken breast after roasting
          it" [syn: {flavor}, {flavour}]
     2: make fit; "This trip will season even the hardiest
        ...
