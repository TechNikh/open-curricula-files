---
version: 1
type: article
id: https://en.wikipedia.org/wiki/H_topology
offline_file: ""
offline_thumbnail: ""
uuid: 369ae605-7bcc-43c0-b49b-24bcca776068
updated: 1484309153
title: H topology
categories:
    - Algebraic geometry
---
In algebraic geometry, the h topology is a Grothendieck topology introduced by Vladimir Voevodsky to study the homology of scheme. It has several variants, such as the qfh and cdh topologies.
