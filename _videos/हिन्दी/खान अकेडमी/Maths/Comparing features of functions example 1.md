---
version: 1
type: video
provider: YouTube
id: gZIsyUgBnt4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: efc6f0af-310c-4bf4-9e91-f862f52cb226
updated: 1486069607
title: Comparing features of functions example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/gZIsyUgBnt4/default.jpg
    - https://i3.ytimg.com/vi/gZIsyUgBnt4/1.jpg
    - https://i3.ytimg.com/vi/gZIsyUgBnt4/2.jpg
    - https://i3.ytimg.com/vi/gZIsyUgBnt4/3.jpg
---
