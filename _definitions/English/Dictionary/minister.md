---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minister
offline_file: ""
offline_thumbnail: ""
uuid: 7bded95c-aa9f-4a73-8fd1-3b7198ad33ca
updated: 1484310429
title: minister
categories:
    - Dictionary
---
minister
     n 1: a person authorized to conduct religious worship [syn: {curate},
           {parson}, {pastor}, {rector}]
     2: a person appointed to a high office in the government;
        "Minister of Finance" [syn: {government minister}]
     3: a diplomat representing one government to another; ranks
        below ambassador [syn: {diplomatic minister}]
     4: the job of a head of a government department
     v 1: attend to the wants and needs of others; "I have to minister
          to my mother all the time"
     2: work as a minister; "She is ministering in an old parish"
