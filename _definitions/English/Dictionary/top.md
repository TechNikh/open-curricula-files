---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/top
offline_file: ""
offline_thumbnail: ""
uuid: ada34006-858f-4fb4-887c-f6c48ee98bc6
updated: 1484310341
title: top
categories:
    - Dictionary
---
top
     adj 1: situated at the top or highest position; "the top shelf"
            [syn: {top(a)}] [ant: {bottom(a)}, {side(a)}]
     2: not to be surpassed; "his top effort" [syn: {greatest}]
     n 1: the upper part of anything; "the mower cuts off the tops of
          the grass"; "the title should be written at the top of
          the first page"
     2: the highest or uppermost side of anything; "put your books
        on top of the desk"; "only the top side of the box was
        painted" [syn: {top side}, {upper side}, {upside}]
     3: the top point of a mountain or hill; "the view from the peak
        was magnificent"; "they clambered to the summit of
        Monadnock" [syn: ...
