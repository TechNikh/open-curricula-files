---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rule_of_sum
offline_file: ""
offline_thumbnail: ""
uuid: af5fb9d4-0c24-47b8-88f4-acadd7652300
updated: 1484308599
title: Rule of sum
categories:
    - Combinatorics
---
In combinatorics, the rule of sum or addition principle is a basic counting principle. Stated simply, it is the idea that if we have a ways of doing something and b ways of doing another thing and we can not do both at the same time, then there are a + b ways to choose one of the actions.
