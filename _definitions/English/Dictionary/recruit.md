---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recruit
offline_file: ""
offline_thumbnail: ""
uuid: 9486f6f9-20f5-42bd-8166-ddffead7eb45
updated: 1484310515
title: recruit
categories:
    - Dictionary
---
recruit
     n 1: a recently enlisted soldier
     2: any new member or supporter (as in the armed forces) [syn: {enlistee}]
     v 1: register formally as a participant or member; "The party
          recruited many new members" [syn: {enroll}, {inscribe},
          {enter}, {enrol}]
     2: seek to employ; "The lab director recruited an able crew of
        assistants"
     3: cause to assemble or enlist in the military; "raise an
        army"; "recruit new soldiers" [syn: {levy}, {raise}]
