---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Adams_hemisphere-in-a-square_projection
offline_file: ""
offline_thumbnail: ""
uuid: dffe5732-85ee-4e07-ad4a-cf1edc52c63c
updated: 1484309352
title: Adams hemisphere-in-a-square projection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Adams_hemisphere_in_a_square.JPG
categories:
    - Geometry stubs
---
The Adams hemisphere-in-a-square is a conformal map projection for a hemisphere. It is a transverse version of the Peirce quincuncial projection, and is named after American cartographer Oscar Sherman Adams, who published it in 1925.[1] When it is used to represent the entire sphere it is known as the Adams doubly periodic projection. Like many conformal projections, conformality fails at certain points, in this case at the four corners.
