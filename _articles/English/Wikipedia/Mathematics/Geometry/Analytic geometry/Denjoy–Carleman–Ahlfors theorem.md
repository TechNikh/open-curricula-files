---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Denjoy%E2%80%93Carleman%E2%80%93Ahlfors_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 6d1204a4-cad0-4431-8938-0228ece4683a
updated: 1484309160
title: Denjoy–Carleman–Ahlfors theorem
categories:
    - Analytic geometry
---
The Denjoy–Carleman–Ahlfors theorem states that the number of asymptotic values attained by a non-constant entire function of order ρ on curves going outwards toward infinite absolute value is less than or equal to 2ρ. It was first conjectured by Arnaud Denjoy in 1907.[1] Torsten Carleman showed that the number of asymptotic values was less than or equal to (5/2)ρ in 1921.[2] In 1929 Lars Ahlfors confirmed Denjoy's conjecture of 2ρ.[3] Finally, in 1933, Carleman published a very short proof.[4]
