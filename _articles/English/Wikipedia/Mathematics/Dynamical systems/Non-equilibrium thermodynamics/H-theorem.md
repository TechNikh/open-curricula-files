---
version: 1
type: article
id: https://en.wikipedia.org/wiki/H-theorem
offline_file: ""
offline_thumbnail: ""
uuid: 18230030-67e9-410b-80d5-c83dcdb4bba0
updated: 1484308797
title: H-theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Translational_motion.gif
tags:
    - "Definition and meaning of Boltzmann's H"
    - "Boltzmann's H theorem"
    - Impact
    - Criticism of the H-theorem and exceptions
    - "Loschmidt's paradox"
    - Spin echo
    - Poincaré recurrence
    - Fluctuations of H in small systems
    - Connection to information theory
    - "Tolman's H-theorem"
    - Classical mechanical
    - Quantum mechanical
    - "Gibbs' H-theorem"
    - Notes
categories:
    - Non-equilibrium thermodynamics
---
In classical statistical mechanics, the H-theorem, introduced by Ludwig Boltzmann in 1872, describes the tendency to increase in the quantity H (defined below) in a nearly-ideal gas of molecules.[1] As this quantity H was meant to represent the entropy of thermodynamics, the H-theorem was an early demonstration of the power of statistical mechanics as it claimed to derive the second law of thermodynamics—a statement about fundamentally irreversible processes—from reversible microscopic mechanics.
