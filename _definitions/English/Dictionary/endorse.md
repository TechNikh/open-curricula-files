---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endorse
offline_file: ""
offline_thumbnail: ""
uuid: 78027a9d-e6ff-41b0-b28b-1f861854f380
updated: 1484310589
title: endorse
categories:
    - Dictionary
---
endorse
     v 1: be behind; approve of; "He plumped for the Labor Party"; "I
          backed Kennedy in 1960" [syn: {back}, {indorse}, {plump
          for}, {plunk for}, {support}]
     2: give support or one's approval to; "I'll second that
        motion"; "I can't back this plan"; "endorse a new project"
        [syn: {second}, {back}, {indorse}]
     3: guarantee as meeting a certain standard; "certified grade
        AAA meat" [syn: {certify}, {indorse}]
     4: of documents or cheques [syn: {indorse}]
