---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tabulate
offline_file: ""
offline_thumbnail: ""
uuid: ebbd3009-980c-418e-b5f2-8c0cf9c3291d
updated: 1484310210
title: tabulate
categories:
    - Dictionary
---
tabulate
     v 1: arrange in tabular form [syn: {tabularize}, {tabularise}]
     2: shape or cut with a flat surface
