---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Candle_auction
offline_file: ""
offline_thumbnail: ""
uuid: 4509a40f-5e80-4d2f-8e65-099d0e5d16ae
updated: 1484309011
title: Candle auction
categories:
    - Auctions
---
A candle auction, or auction by the candle, is a variation on the typical English auction that became popular in the 17th and 18th centuries.[1] In a candle auction, the end of the auction is signaled by the expiration of a candle flame, which was intended to ensure that no one could know exactly when the auction would end and make a last-second bid. Sometimes, other unpredictable processes, such as a footrace, were used in place of the expiration of a candle.
