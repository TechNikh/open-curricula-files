---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equity_(economics)
offline_file: ""
offline_thumbnail: ""
uuid: c340cda3-bfa0-413f-ac3a-b3ef1495cc3d
updated: 1484308520
title: Equity (economics)
tags:
    - Overview
    - Taxation
    - Health care
    - Fair division
    - Notes
categories:
    - Fair division
---
Equity or economic equality is the concept or idea of fairness in economics, particularly in regard to taxation or welfare economics. More specifically, it may refer to equal life chances regardless of identity, to provide all citizens with a basic and equal minimum of income, goods, and services or to increase funds and commitment for redistribution.[1]
