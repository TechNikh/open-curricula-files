---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dulmage%E2%80%93Mendelsohn_decomposition'
offline_file: ""
offline_thumbnail: ""
uuid: 937bda53-9242-44ca-9399-202a6d8a5ed0
updated: 1484309426
title: Dulmage–Mendelsohn decomposition
tags:
    - The coarse decomposition
    - The fine decomposition
    - Core
    - Applications
categories:
    - Matching
---
In graph theory, the Dulmage–Mendelsohn decomposition is a partition of the vertices of a bipartite graph into subsets, with the property that two adjacent vertices belong to the same subset if and only if they are paired with each other in a perfect matching of the graph. It is named after A. L. Dulmage and Nathan Mendelsohn, who published it in 1958.
