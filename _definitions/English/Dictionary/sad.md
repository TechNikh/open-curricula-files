---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sad
offline_file: ""
offline_thumbnail: ""
uuid: 16881017-445b-4f82-9bf4-4c88b70247dd
updated: 1484310599
title: sad
categories:
    - Dictionary
---
sad
     adj 1: experiencing or showing sorrow or unhappiness; "feeling sad
            because his dog had died"; "Better by far that you
            should forget and smile / Than that you should
            remember and be sad"- Christina Rossetti [ant: {glad}]
     2: of things that make you feel sad; "sad news"; "she doesn't
        like sad movies"; "it was a very sad story"; "When I am
        dead, my dearest, / Sing no sad songs for me"- Christina
        Rossetti
     3: bad; unfortunate; "my finances were in a deplorable state";
        "a lamentable decision"; "her clothes were in sad shape";
        "a sorry state of affairs" [syn: {deplorable}, {distressing},
         ...
