---
version: 1
type: video
provider: YouTube
id: hHteUIS0OFY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Compositional%20and%20mechanical%20layers%20of%20the%20earth.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3d6b1805-dcc7-4aa0-8ea3-b831e17dc2f4
updated: 1486069688
title: Compositional and mechanical layers of the earth
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/hHteUIS0OFY/default.jpg
    - https://i3.ytimg.com/vi/hHteUIS0OFY/1.jpg
    - https://i3.ytimg.com/vi/hHteUIS0OFY/2.jpg
    - https://i3.ytimg.com/vi/hHteUIS0OFY/3.jpg
---
