---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urge
offline_file: ""
offline_thumbnail: ""
uuid: 8a2fe0bf-d8da-47df-a02b-86fbcb08492b
updated: 1484310249
title: urge
categories:
    - Dictionary
---
urge
     n 1: an instinctive motive; "profound religious impulses" [syn: {impulse}]
     2: a strong restless desire; "why this urge to travel?" [syn: {itch}]
     v 1: force or impel in an indicated direction; "I urged him to
          finish his studies" [syn: {urge on}, {press}, {exhort}]
     2: push for something; "The travel agent recommended strongly
        that we not travel on Thanksgiving Day" [syn: {recommend},
         {advocate}]
     3: urge on or encourage especially by shouts; "The crowd
        cheered the demonstrating strikers" [syn: {cheer}, {inspire},
         {barrack}, {urge on}, {exhort}, {pep up}]
