---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gentleman
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484502061
title: gentleman
categories:
    - Dictionary
---
gentleman
     n 1: a man of refinement
     2: a manservant who acts as a personal attendant to his
        employer; "Jeeves was Bertie Wooster's man" [syn: {valet},
         {valet de chambre}, {gentleman's gentleman}, {man}]
