---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Morrie%27s_law'
offline_file: ""
offline_thumbnail: ""
uuid: cb754154-9c85-4612-a017-ffdba4e64aaa
updated: 1484309347
title: "Morrie's law"
categories:
    - Trigonometry
---
It is a special case of the more general identity
