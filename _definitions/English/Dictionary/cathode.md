---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cathode
offline_file: ""
offline_thumbnail: ""
uuid: 2d66a289-50ad-4c79-9494-9ff63b6c2036
updated: 1484310204
title: cathode
categories:
    - Dictionary
---
cathode
     n 1: a negatively charged electrode that is the source of
          electrons in an electrical device [ant: {anode}]
     2: the positively charged terminal of a voltaic cell or storage
        battery that supplies current [ant: {anode}]
