---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/J%C3%A1nos_Pach'
offline_file: ""
offline_thumbnail: ""
uuid: 27ac652b-fa10-4eb2-b7f6-f16b41a6525c
updated: 1484309473
title: János Pach
tags:
    - Biography
    - Research
    - Awards and honors
    - Books
categories:
    - Graph theorists
---
János Pach (born May 3, 1954)[2] is a mathematician and computer scientist working in the fields of combinatorics and discrete and computational geometry.
