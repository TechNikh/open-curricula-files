---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Church%E2%80%93Turing_thesis'
offline_file: ""
offline_thumbnail: ""
uuid: 798f8d4d-e8bb-4ea5-ad8b-22018aba546c
updated: 1484309485
title: Church–Turing thesis
tags:
    - "Statement in Church's and Turing's words"
    - History
    - Circa 1930–1952
    - Later developments
    - The thesis as a definition
    - Success of the thesis
    - Informal usage in proofs
    - Variations
    - Philosophical Implications
    - Non-computable functions
    - Footnotes
categories:
    - Computability theory
---
In computability theory, the Church–Turing thesis (also known as Computability Thesis,[1] the Turing–Church thesis,[2] the Church–Turing conjecture, Church's thesis, Church's conjecture, and Turing's thesis) is a hypothesis about the nature of computable functions. It states that a function on the natural numbers is computable by a human being following an algorithm, ignoring resource limitations, if and only if it is computable by a Turing machine. The thesis is named after American mathematician Alonzo Church and the British mathematician Alan Turing. Before the precise definition of computable function, mathematicians often used the informal term effectively calculable to describe ...
