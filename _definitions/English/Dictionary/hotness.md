---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hotness
offline_file: ""
offline_thumbnail: ""
uuid: 5c7f8efb-c074-4d9e-b159-9e28c5e1a654
updated: 1484310234
title: hotness
categories:
    - Dictionary
---
hotness
     n 1: the presence of heat [syn: {heat}, {high temperature}] [ant:
           {coldness}]
     2: a state of sexual arousal [syn: {horniness}, {hot pants}]
     3: a hot spiciness [syn: {pepperiness}]
