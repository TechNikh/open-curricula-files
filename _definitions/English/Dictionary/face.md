---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/face
offline_file: ""
offline_thumbnail: ""
uuid: 9f37ca2b-c089-4c6f-b6ca-180f5b297f15
updated: 1484310309
title: face
categories:
    - Dictionary
---
face
     n 1: the front of the human head from the forehead to the chin
          and ear to ear; "he washed his face"; "I wish I had seen
          the look on his face when he got the news" [syn: {human
          face}]
     2: the expression on a person's face; "a sad expression"; "a
        look of triumph"; "an angry face" [syn: {expression}, {look},
         {aspect}, {facial expression}]
     3: the general outward appearance of something; "the face of
        the city is changing"
     4: the act of confronting bravely; "he hated facing the facts";
        "he excelled in the face of danger" [syn: {facing}]
     5: the striking or working surface of an implement
     6: a part of a ...
