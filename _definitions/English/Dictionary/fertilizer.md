---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fertilizer
offline_file: ""
offline_thumbnail: ""
uuid: 66670ae7-557f-46e4-b6fe-c896dd783358
updated: 1484310256
title: fertilizer
categories:
    - Dictionary
---
fertilizer
     n : any substance such as manure or a mixture of nitrates used
         to make soil more fertile [syn: {fertiliser}, {plant food}]
