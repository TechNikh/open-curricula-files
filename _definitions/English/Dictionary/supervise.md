---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supervise
offline_file: ""
offline_thumbnail: ""
uuid: 7ea47169-a747-472e-8799-9db4c29b285c
updated: 1484310471
title: supervise
categories:
    - Dictionary
---
supervise
     v 1: watch and direct; "Who is overseeing this project?" [syn: {oversee},
           {superintend}, {manage}]
     2: keep tabs on; keep an eye on; keep under surveillance [syn:
        {monitor}, {ride herd on}]
