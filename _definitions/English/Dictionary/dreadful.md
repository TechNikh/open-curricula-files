---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dreadful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579881
title: dreadful
categories:
    - Dictionary
---
dreadful
     adj 1: causing fear or dread or terror; "the awful war"; "an awful
            risk"; "dire news"; "a career or vengeance so direful
            that London was shocked"; "the dread presence of the
            headmaster"; "polio is no longer the dreaded disease
            it once was"; "a dreadful storm"; "a fearful howling";
            "horrendous explosions shook the city"; "a terrible
            curse" [syn: {awful}, {dire}, {direful}, {dread(a)}, {dreaded},
             {fearful}, {fearsome}, {frightening}, {horrendous}, {horrific},
             {terrible}]
     2: exceptionally bad or displeasing; "atrocious taste";
        "abominable workmanship"; "an awful voice"; ...
