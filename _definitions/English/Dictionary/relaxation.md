---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relaxation
offline_file: ""
offline_thumbnail: ""
uuid: ea4bdd23-55d7-4fa0-931b-ef703b68a8af
updated: 1484310335
title: relaxation
categories:
    - Dictionary
---
relaxation
     n 1: (physiology) the gradual lengthening of inactive muscle or
          muscle fibers
     2: (physics) the exponential return of a system to equilibrium
        after a disturbance [syn: {relaxation behavior}]
     3: a state of refreshing tranquility [syn: {easiness}]
     4: an occurrence of control or strength weakening; "the
        relaxation of requirements"; "the loosening of his grip";
        "the slackening of the wind" [syn: {loosening}, {slackening}]
     5: freedom from activity (work or strain or responsibility);
        "took his repose by the swimming pool" [syn: {rest}, {ease},
         {repose}]
     6: a method of solving simultaneous equations by ...
