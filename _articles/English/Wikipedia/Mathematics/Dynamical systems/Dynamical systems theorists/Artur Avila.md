---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artur_Avila
offline_file: ""
offline_thumbnail: ""
uuid: 4c69093f-9dcb-4b04-9fb4-543813101e26
updated: 1484308688
title: Artur Avila
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_1.jpg
tags:
    - Biography
    - Prizes
    - Mathematical work
    - Notes and references
categories:
    - Dynamical systems theorists
---
Artur Avila Cordeiro de Melo (born 29 June 1979) is a Brazilian and French mathematician working primarily on dynamical systems and spectral theory. He is one of the winners of the 2014 Fields Medal,[2] being the first Latin American to win such award. He is a researcher at both the IMPA and the CNRS (working a half-year in each one).
