---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bifurcation_theory
offline_file: ""
offline_thumbnail: ""
uuid: 1b8ec65a-34de-4cd6-b085-83beae297080
updated: 1484308712
title: Bifurcation theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Saddlenode_0.gif
tags:
    - Bifurcation types
    - Local bifurcations
    - Global bifurcations
    - Codimension of a bifurcation
    - Applications in semiclassical and quantum physics
    - Notes
categories:
    - Bifurcation theory
---
Bifurcation theory is the mathematical study of changes in the qualitative or topological structure of a given family, such as the integral curves of a family of vector fields, and the solutions of a family of differential equations. Most commonly applied to the mathematical study of dynamical systems, a bifurcation occurs when a small smooth change made to the parameter values (the bifurcation parameters) of a system causes a sudden 'qualitative' or topological change in its behaviour.[1] Bifurcations occur in both continuous systems (described by ODEs, DDEs or PDEs) and discrete systems (described by maps). The name "bifurcation" was first introduced by Henri Poincaré in 1885 in the ...
