---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poster
offline_file: ""
offline_thumbnail: ""
uuid: 396632b5-019a-4491-bfbb-c73b14466e8d
updated: 1484310555
title: poster
categories:
    - Dictionary
---
poster
     n 1: a sign posted in a public place as an advertisement; "a
          poster advertised the coming attractions" [syn: {posting},
           {placard}, {notice}, {bill}, {card}]
     2: someone who pastes up bills or placards on walls or
        billboards [syn: {bill poster}, {bill sticker}]
     3: a horse kept at an inn or post house for use by mail
        carriers or for rent to travelers [syn: {post horse}]
