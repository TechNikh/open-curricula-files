---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exist
offline_file: ""
offline_thumbnail: ""
uuid: a27c61ac-2486-406e-b89d-9b95975b47bb
updated: 1484310271
title: exist
categories:
    - Dictionary
---
exist
     v 1: have an existence, be extant; "Is there a God?" [syn: {be}]
     2: support oneself; "he could barely exist on such a low wage";
        "Can you live on $2000 a month in New York City?"; "Many
        people in the world have to subsist on $1 a day" [syn: {survive},
         {live}, {subsist}]
