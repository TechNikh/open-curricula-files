---
version: 1
type: video
provider: YouTube
id: SMtWvDbfHLo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c8853ffa-45be-4bc8-b718-2398f2333912
updated: 1486069670
title: DNA Transcription (Advanced)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/SMtWvDbfHLo/default.jpg
    - https://i3.ytimg.com/vi/SMtWvDbfHLo/1.jpg
    - https://i3.ytimg.com/vi/SMtWvDbfHLo/2.jpg
    - https://i3.ytimg.com/vi/SMtWvDbfHLo/3.jpg
---
