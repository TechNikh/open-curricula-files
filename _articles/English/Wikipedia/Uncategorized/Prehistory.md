---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pre-historic
offline_file: ""
offline_thumbnail: ""
uuid: 8ee001e2-1465-4e23-936d-b8ba1acd5052
updated: 1484308344
title: Prehistory
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-G%25C3%25B6bekli_Tepe%252C_Urfa.jpg'
tags:
    - Definition
    - History of the term
    - Means of research
    - Stone Age
    - Palaeolithic
    - Mesolithic
    - Neolithic
    - Chalcolithic
    - Bronze Age
    - Iron Age
    - Timeline
    - By region
categories:
    - Uncategorized
---
Prehistory means literally "before history", from the Latin word for "before," præ, and Greek ιστορία. Human prehistory is the period from the time that behaviorally and anatomically modern humans first appear until the appearance of recorded history following the invention of writing systems. Since both the time of settlement of modern humans and the evolution of human civilisations differ from region to region, prehistory starts and ends at different moments in time[clarification needed], depending on the region concerned.
