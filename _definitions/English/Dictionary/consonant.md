---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consonant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494381
title: consonant
categories:
    - Dictionary
---
consonant
     adj 1: involving or characterized by harmony [syn: {harmonic}, {harmonical},
             {harmonized}, {harmonised}, {in harmony}]
     2: in keeping; "salaries agreeable with current trends"; "plans
        conformable with your wishes"; "expressed views concordant
        with his background" [syn: {accordant}, {agreeable}, {conformable},
         {concordant}]
     n 1: a speech sound that is not a vowel [ant: {vowel}]
     2: a letter of the alphabet standing for a spoken consonant
