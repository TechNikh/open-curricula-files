---
version: 1
type: video
provider: YouTube
id: j-KJ7DqzZ8w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: db216fed-1316-401f-b502-3df160592a58
updated: 1486069654
title: Permittivity lesson 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/j-KJ7DqzZ8w/default.jpg
    - https://i3.ytimg.com/vi/j-KJ7DqzZ8w/1.jpg
    - https://i3.ytimg.com/vi/j-KJ7DqzZ8w/2.jpg
    - https://i3.ytimg.com/vi/j-KJ7DqzZ8w/3.jpg
---
