---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thereof
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593861
title: thereof
categories:
    - Dictionary
---
thereof
     adv 1: of or concerning this or that; "a problem and the solution
            thereof"
     2: from that circumstance or source; "atomic formulas and all
        compounds thence constructible"- W.V.Quine; "a natural
        conclusion follows thence"; "public interest and a policy
        deriving therefrom"; "typhus fever results therefrom"
        [syn: {thence}, {therefrom}]
