---
version: 1
type: video
provider: YouTube
id: JVlfQEhzLMM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Mixture%20problems%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bf42bd1d-5be7-407c-94ee-e280fcea5ddf
updated: 1486069559
title: Mixture problems 3
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/JVlfQEhzLMM/default.jpg
    - https://i3.ytimg.com/vi/JVlfQEhzLMM/1.jpg
    - https://i3.ytimg.com/vi/JVlfQEhzLMM/2.jpg
    - https://i3.ytimg.com/vi/JVlfQEhzLMM/3.jpg
---
