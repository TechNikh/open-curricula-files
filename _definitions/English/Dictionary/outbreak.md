---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outbreak
offline_file: ""
offline_thumbnail: ""
uuid: 9199db04-bb19-4aa8-8b5e-1b746e9e7c2e
updated: 1484310561
title: outbreak
categories:
    - Dictionary
---
outbreak
     n : a sudden violent spontaneous occurrence (usually of some
         undesirable condition); "the outbreak of hostilities"
         [syn: {eruption}, {irruption}]
