---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blame
offline_file: ""
offline_thumbnail: ""
uuid: cb60f313-e788-4802-a4a2-bef35a13b685
updated: 1484310469
title: blame
categories:
    - Dictionary
---
blame
     adj : expletives used informally as intensifiers; "he's a blasted
           idiot"; "it's a blamed shame"; "a blame cold winter";
           "not a blessed dime"; "I'll be damned (or blessed or
           darned or goddamned) if I'll do any such thing"; "he's
           a damn (or goddam or goddamned) fool"; "a deuced
           idiot"; "tired or his everlasting whimpering"; "an
           infernal nuisance" [syn: {blasted}, {blamed}, {blessed},
            {damn}, {damned}, {darned}, {deuced}, {everlasting}, {goddam},
            {goddamn}, {goddamned}, {infernal}]
     n 1: an accusation that you are responsible for some lapse or
          misdeed; "his incrimination was based ...
