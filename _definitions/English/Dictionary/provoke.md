---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provoke
offline_file: ""
offline_thumbnail: ""
uuid: 69fa2de2-8589-4e6d-80e3-9ab65293b783
updated: 1484310174
title: provoke
categories:
    - Dictionary
---
provoke
     v 1: call forth (emotions, feelings, and responses); "arouse
          pity"; "raise a smile"; "evoke sympathy" [syn: {arouse},
           {elicit}, {enkindle}, {kindle}, {evoke}, {fire}, {raise}]
     2: call forth; "Her behavior provoked a quarrel between the
        couple" [syn: {evoke}, {call forth}, {kick up}]
     3: provide the needed stimulus for [syn: {stimulate}]
     4: annoy continually or chronically; "He is known to harry his
        staff when he is overworked"; "This man harasses his
        female co-workers" [syn: {harass}, {hassle}, {harry}, {chivy},
         {chivvy}, {chevy}, {chevvy}, {beset}, {plague}, {molest}]
