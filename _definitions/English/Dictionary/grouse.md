---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grouse
offline_file: ""
offline_thumbnail: ""
uuid: 78bc73dc-3431-4605-9743-9007bf6ec51e
updated: 1484310609
title: grouse
categories:
    - Dictionary
---
grouse
     n 1: flesh of any of various grouse of the family Tetraonidae;
          usually roasted; flesh too dry to broil
     2: popular game bird having a plump body and feathered legs and
        feet
     v 1: hunt grouse
     2: complain; "What was he hollering about?" [syn: {gripe}, {crab},
         {beef}, {squawk}, {bellyache}, {holler}]
