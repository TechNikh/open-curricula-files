---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bid-to-cover_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 4008ff66-d480-485c-914e-3bda9f524152
updated: 1484309011
title: Bid-to-cover ratio
tags:
    - Example
categories:
    - Auctions
---
Bid-To-Cover Ratio is a ratio used to express the demand for a particular security during offerings and auctions. In general, it is used for shares, bonds, and other securities. It may be computed in two ways: either the number of bids received divided by the number of bids accepted, or the value of bids received divided by the value of bids accepted.[1]
