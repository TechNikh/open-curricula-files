---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Giovanni_Forni_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 4cc31062-b74b-43e0-aed8-4be4ed2f7fe9
updated: 1484308684
title: Giovanni Forni (mathematician)
categories:
    - Dynamical systems theorists
---
After graduating from the University of Bologna in 1989,[1] he obtained his PhD in 1993 from Princeton University, under the supervision of John Mather.[2]
