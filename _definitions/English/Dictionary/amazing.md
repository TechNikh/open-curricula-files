---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amazing
offline_file: ""
offline_thumbnail: ""
uuid: ac997d97-5561-4818-96d8-eab93ecf33da
updated: 1484310527
title: amazing
categories:
    - Dictionary
---
amazing
     adj 1: surprising greatly; "she does an amazing amount of work";
            "the dog was capable of astonishing tricks" [syn: {astonishing}]
     2: inspiring awe or admiration or wonder; "New York is an
        amazing city"; "the Grand Canyon is an awe-inspiring
        sight"; "the awesome complexity of the universe"; "this
        sea, whose gently awful stirrings seem to speak of some
        hidden soul beneath"- Melville; "Westminster Hall's awing
        majesty, so vast, so high, so silent" [syn: {awe-inspiring},
         {awesome}, {awful}, {awing}]
