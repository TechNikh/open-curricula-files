---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bow-wow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496361
title: bow-wow
categories:
    - Dictionary
---
bow-wow
     n 1: the bark of a dog
     2: informal terms for dogs [syn: {pooch}, {doggie}, {doggy}, {barker}]
