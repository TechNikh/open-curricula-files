---
version: 1
type: video
provider: YouTube
id: lEXkkGQ1crM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dffe77e6-3dd8-4230-abb9-0172a69ee8f9
updated: 1486069621
title: Time and Work Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/lEXkkGQ1crM/default.jpg
    - https://i3.ytimg.com/vi/lEXkkGQ1crM/1.jpg
    - https://i3.ytimg.com/vi/lEXkkGQ1crM/2.jpg
    - https://i3.ytimg.com/vi/lEXkkGQ1crM/3.jpg
---
