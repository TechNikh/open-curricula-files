---
version: 1
type: video
provider: YouTube
id: WVXvhcS5k14
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cefd1cf4-faa5-4853-a5dc-4eac28026e7e
updated: 1486069614
title: Multiplying 2 digit numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/WVXvhcS5k14/default.jpg
    - https://i3.ytimg.com/vi/WVXvhcS5k14/1.jpg
    - https://i3.ytimg.com/vi/WVXvhcS5k14/2.jpg
    - https://i3.ytimg.com/vi/WVXvhcS5k14/3.jpg
---
