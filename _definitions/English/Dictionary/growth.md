---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/growth
offline_file: ""
offline_thumbnail: ""
uuid: d9ee6032-0a47-47ca-8573-836a8f1cb971
updated: 1484310264
title: growth
categories:
    - Dictionary
---
growth
     n 1: (biology) the process of an individual organism growing
          organically; a purely biological unfolding of events
          involved in an organism changing gradually from a simple
          to a more complex level; "he proposed an indicator of
          osseous development in children" [syn: {growing}, {maturation},
           {development}, {ontogeny}, {ontogenesis}] [ant: {nondevelopment}]
     2: a progression from simpler to more complex forms; "the
        growth of culture"
     3: a process of becoming larger or longer or more numerous or
        more important; "the increase in unemployment"; "the
        growth of population" [syn: {increase}, {increment}] ...
