---
version: 1
type: video
provider: YouTube
id: -WA7-k_UcWY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Cochlear%20Implant%20Introduction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9154d9a6-c0ac-406a-8295-0242b57baeca
updated: 1486069668
title: Cochlear Implant Introduction
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/-WA7-k_UcWY/default.jpg
    - https://i3.ytimg.com/vi/-WA7-k_UcWY/1.jpg
    - https://i3.ytimg.com/vi/-WA7-k_UcWY/2.jpg
    - https://i3.ytimg.com/vi/-WA7-k_UcWY/3.jpg
---
