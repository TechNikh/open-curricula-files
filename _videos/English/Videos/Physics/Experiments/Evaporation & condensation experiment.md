---
version: 1
type: video
provider: YouTube
id: ubOG0Dgp4E0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f4a0f8e1-f211-42df-9ef2-fd4ac58e0788
updated: 1486069635
title: 'Evaporation & condensation experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ubOG0Dgp4E0/default.jpg
    - https://i3.ytimg.com/vi/ubOG0Dgp4E0/1.jpg
    - https://i3.ytimg.com/vi/ubOG0Dgp4E0/2.jpg
    - https://i3.ytimg.com/vi/ubOG0Dgp4E0/3.jpg
---
