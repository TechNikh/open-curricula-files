---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Haematite
offline_file: ""
offline_thumbnail: ""
uuid: 87e2f859-b311-4799-b2a3-1941816ba7a3
updated: 1484308389
title: Hematite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-WLA_hmns_Hematite.jpg
tags:
    - Etymology and history
    - magnetism
    - Mine tailings
    - Mars
    - Jewelry
    - Gallery
categories:
    - Uncategorized
---
Hematite, also spelled as haematite, is the mineral form of iron(III) oxide (Fe2O3), one of several iron oxides. Hematite crystallizes in the rhombohedral lattice system, and it has the same crystal structure as ilmenite and corundum. Hematite and ilmenite form a complete solid solution at temperatures above 950 °C (1,740 °F).
