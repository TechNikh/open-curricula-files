---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diplomacy
offline_file: ""
offline_thumbnail: ""
uuid: 6a4843c5-54f1-47d5-ab58-1b3791da0467
updated: 1484310549
title: diplomacy
categories:
    - Dictionary
---
diplomacy
     n 1: negotiation between nations [syn: {diplomatic negotiations}]
     2: subtly skillful handling of a situation [syn: {delicacy}, {discreetness},
         {finesse}]
     3: wisdom in the management of public affairs [syn: {statesmanship},
         {statecraft}]
