---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/empowerment
offline_file: ""
offline_thumbnail: ""
uuid: 1b2ed12c-ea74-4d96-984f-13226ce15ea7
updated: 1484310181
title: empowerment
categories:
    - Dictionary
---
empowerment
     n : the act of conferring legality or sanction or formal warrant
         [syn: {authorization}, {authorisation}]
