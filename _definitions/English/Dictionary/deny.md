---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deny
offline_file: ""
offline_thumbnail: ""
uuid: 8f306269-32f9-49eb-80b4-2f7037d75b34
updated: 1484310515
title: deny
categories:
    - Dictionary
---
deny
     v 1: declare untrue; contradict; "He denied the allegations";
          "She denied that she had taken money" [ant: {admit}]
     2: refuse to accept or believe; "He denied his fatal illness"
     3: refuse to grant, as of a petition or request; "The dean
        denied the students' request for more physics courses";
        "the prisoners were denied the right to exercise for more
        than 2 hours a day"
     4: refuse to let have; "She denies me every pleasure"; "he
        denies her her weekly allowance" [syn: {refuse}] [ant: {allow}]
     5: deny oneself (something); restrain, especially from
        indulging in some pleasure; "She denied herself wine and
        ...
