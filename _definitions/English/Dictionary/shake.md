---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shake
offline_file: ""
offline_thumbnail: ""
uuid: d0d8a6d1-95c7-4abe-b920-d44d440164a9
updated: 1484310337
title: shake
categories:
    - Dictionary
---
shake
     n 1: building material used as siding or roofing [syn: {shingle}]
     2: frothy drink of milk and flavoring and sometimes fruit or
        ice cream [syn: {milkshake}, {milk shake}]
     3: a note that alternates rapidly with another note a semitone
        above it [syn: {trill}]
     4: grasping and shaking a person's hand (as to acknowledge an
        introduction or to agree on a contract) [syn: {handshake},
         {handshaking}, {handclasp}]
     5: reflex shaking caused by cold or fear or excitement [syn: {tremble},
         {shiver}]
     6: causing to move repeatedly from side to side [syn: {wag}, {waggle}]
     v 1: move or cause to move back and forth; "The chemist ...
