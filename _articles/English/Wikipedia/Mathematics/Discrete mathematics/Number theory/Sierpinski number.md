---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sierpinski_number
offline_file: ""
offline_thumbnail: ""
uuid: c89d67dd-ab2b-48cc-9137-5668c4bdfc23
updated: 1484308659
title: Sierpinski number
tags:
    - Known Sierpiński numbers
    - Sierpiński problem
    - Smallest n for which k×2n+1 is prime
    - Simultaneously Sierpiński and Riesel
    - Dual Sierpinski problem
    - Sierpinski number base b
categories:
    - Number theory
---
In number theory, a Sierpinski or Sierpiński number is an odd natural number k such that 
  
    
      
        k
        ×
        
          2
          
            n
          
        
        +
        1
      
    
    {\displaystyle k\times 2^{n}+1}
  
 is composite, for all natural numbers n. In 1960, Wacław Sierpiński proved that there are infinitely many odd integers k which have this property.
