---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_state
offline_file: ""
offline_thumbnail: ""
uuid: 5b510538-24f4-493e-bc0d-68f360f5f854
updated: 1484308870
title: Thermodynamic state
tags:
    - State functions
    - Equilibrium state
    - Bibliography
categories:
    - Thermodynamics
---
For thermodynamics, a thermodynamic state of a system is its condition at a specific time, that is fully identified by values of a suitable set of parameters known as state variables, state parameters or thermodynamic variables. Once such a set of values of thermodynamic variables has been specified for a system, the values of all thermodynamic properties of the system are uniquely determined. Usually, by default, a thermodynamic state is taken to be one of thermodynamic equilibrium. This means that the state is not merely the condition of the system at a specific time, but that the condition is the same, unchanging, over an indefinitely long duration of time.
