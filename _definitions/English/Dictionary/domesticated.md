---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/domesticated
offline_file: ""
offline_thumbnail: ""
uuid: 5f7d9f92-5f2a-4f93-9bfc-beaea55ee961
updated: 1484310541
title: domesticated
categories:
    - Dictionary
---
domesticated
     adj 1: converted or adapted to domestic use; "domestic animals";
            "domesticated plants like maize" [syn: {domestic}]
     2: accustomed to home life; "some men think it unmanly to be
        domesticated; others find gratification in it"
