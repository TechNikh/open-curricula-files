---
version: 1
type: video
provider: YouTube
id: U7HQ_G_N6vo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%2C%20gradient%2C%20divergence%2C%20curl/Gradient%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 54c0e79f-0629-40d0-8720-12acadcccea6
updated: 1486069690
title: Gradient 1
categories:
    - Partial derivatives, gradient, divergence, curl
thumbnail_urls:
    - https://i3.ytimg.com/vi/U7HQ_G_N6vo/default.jpg
    - https://i3.ytimg.com/vi/U7HQ_G_N6vo/1.jpg
    - https://i3.ytimg.com/vi/U7HQ_G_N6vo/2.jpg
    - https://i3.ytimg.com/vi/U7HQ_G_N6vo/3.jpg
---
