---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bill
offline_file: ""
offline_thumbnail: ""
uuid: 256fd38e-c823-43b1-8fa7-20a143f024fa
updated: 1484310194
title: bill
categories:
    - Dictionary
---
bill
     n 1: a statute in draft before it becomes law; "they held a
          public hearing on the bill" [syn: {measure}]
     2: an itemized statement of money owed for goods shipped or
        services rendered; "he paid his bill and left"; "send me
        an account of what I owe" [syn: {account}, {invoice}]
     3: a piece of paper money (especially one issued by a central
        bank); "he peeled off five one-thousand-zloty notes" [syn:
         {note}, {government note}, {bank bill}, {banker's bill},
        {bank note}, {banknote}, {Federal Reserve note}, {greenback}]
     4: the entertainment offered at a public presentation
     5: a list of particulars (as a playbill or bill ...
