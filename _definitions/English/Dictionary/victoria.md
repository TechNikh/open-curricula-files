---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/victoria
offline_file: ""
offline_thumbnail: ""
uuid: ebf01d84-ab24-4224-89ec-d568951cc6c6
updated: 1484310150
title: victoria
categories:
    - Dictionary
---
Victoria
     n 1: queen of Great Britain and Ireland and empress of India from
          1837 to 1901 (1819-1901) [syn: {Queen Victoria}]
     2: (Roman mythology) goddess of victory; counterpart of Greek
        Nike
     3: a waterfall in the Zambezi River on the border between
        Zimbabwe and Zambia; diminishes seasonally [syn: {Victoria
        Falls}]
     4: a town in southeast Texas southeast of San Antonio
     5: port city and the capital of Seychelles [syn: {capital of
        Seychelles}]
     6: a state in southeastern Australia
     7: capital of the Canadian province of British Columbia on
        Vancouver Island
