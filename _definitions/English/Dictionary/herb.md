---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herb
offline_file: ""
offline_thumbnail: ""
uuid: c1c47e92-334a-4550-b0ed-657546cf3c44
updated: 1484310543
title: herb
categories:
    - Dictionary
---
herb
     n 1: a plant lacking a permanent woody stem; many are flowering
          garden plants or potherbs; some having medicinal
          properties; some are pests [syn: {herbaceous plant}]
     2: aromatic potherb used in cookery for its savory qualities
