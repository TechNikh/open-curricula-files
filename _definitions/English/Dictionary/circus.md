---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circus
offline_file: ""
offline_thumbnail: ""
uuid: 804be2e3-017d-4e32-b1ee-0c31102f8506
updated: 1484310154
title: circus
categories:
    - Dictionary
---
circus
     n 1: a travelling company of entertainers; including trained
          animals; "he ran away from home to join the circus"
     2: performance given by a traveling company of acrobats clowns
        and trained animals; "the children always love to go to
        the circus"
     3: a frenetic disorganized (and often comic) disturbance
        suggestive of a circus or carnival; "it was so funny it
        was a circus"; "the whole occasion had a carnival
        atmosphere" [syn: {carnival}]
     4: (antiquity) an open-air stadium for chariot races and
        gladiatorial games
     5: an arena consisting of an oval or circular area enclosed by
        tiers of seats and ...
