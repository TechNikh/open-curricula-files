---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484314621
title: liner
categories:
    - Dictionary
---
liner
     n 1: (baseball) a hit that flies straight out from the batter;
          "the batter hit a liner to the shortstop" [syn: {line
          drive}]
     2: a piece of cloth that is used as the inside surface of a
        garment [syn: {lining}]
     3: a large commercial ship (especially one that carries
        passengers on a regular schedule) [syn: {ocean liner}]
