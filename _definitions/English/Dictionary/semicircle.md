---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/semicircle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484355781
title: semicircle
categories:
    - Dictionary
---
semicircle
     n : a plane figure with the shape of half a circle [syn: {hemicycle}]
