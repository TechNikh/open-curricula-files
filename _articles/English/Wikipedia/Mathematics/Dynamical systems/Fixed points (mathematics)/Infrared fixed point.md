---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infrared_fixed_point
offline_file: ""
offline_thumbnail: ""
uuid: a24d143a-028b-41ea-af8f-01f3ac3aad8e
updated: 1484308790
title: Infrared fixed point
tags:
    - Statistical physics
    - Top Quark
    - Standard Model
    - Minimal Supersymmetric Standard Model
    - Banks-Zaks fixed point
categories:
    - Fixed points (mathematics)
---
In physics, an infrared fixed point is a set of coupling constants, or other parameters that evolve from initial values at very high energies (short distance), to fixed stable values, usually predictable, at low energies (large distance). This usually involves the use of the renormalization group, which specifically details the way parameters in a physical system (a quantum field theory) depend on the energy scale being probed.
