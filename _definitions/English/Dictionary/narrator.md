---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrator
offline_file: ""
offline_thumbnail: ""
uuid: 143e16be-4bdc-480a-b180-32913b23fb6f
updated: 1484310152
title: narrator
categories:
    - Dictionary
---
narrator
     n : someone who tells a story [syn: {storyteller}, {teller}]
