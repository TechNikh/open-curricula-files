---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unbeatable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441881
title: unbeatable
categories:
    - Dictionary
---
unbeatable
     adj 1: hard to defeat; "an unbeatable ball team"
     2: incapable of being overcome or subdued; "an invincible
        army"; "her invincible spirit" [syn: {invincible}, {unvanquishable}]
