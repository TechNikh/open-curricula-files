---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gaze
offline_file: ""
offline_thumbnail: ""
uuid: afc85879-91a6-4085-9cf7-a9ff50680618
updated: 1484310601
title: gaze
categories:
    - Dictionary
---
gaze
     n : a long fixed look; "he fixed his paternal gaze on me" [syn:
         {regard}]
     v : look at with fixed eyes; "The students stared at the teacher
         with amazement" [syn: {stare}]
