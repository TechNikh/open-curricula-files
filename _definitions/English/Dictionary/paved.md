---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paved
offline_file: ""
offline_thumbnail: ""
uuid: 0e81ea9d-8ce9-4255-b184-631673582bb3
updated: 1484310252
title: paved
categories:
    - Dictionary
---
paved
     adj : covered with a firm surface [ant: {unpaved}]
