---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Truthful_job_scheduling
offline_file: ""
offline_thumbnail: ""
uuid: bd754ea1-07b1-454b-9bee-6d5856dcfe3a
updated: 1484309046
title: Truthful job scheduling
tags:
    - Definitions
    - 'Positive bound - m - VCG mechanism'
    - 'Negative bound - 2'
categories:
    - Mechanism design
---
We have a project composed of several "jobs" (tasks). There are several workers. Each worker can do any job, but for each worker it takes a different amount of time to complete each job. Our goal is to allocate jobs to workers such that the total makespan of the project is minimized. In the standard Job shop scheduling problem, the timings of all workers are known, so we have a standard optimization problem. In contrast, in the truthful job scheduling problem, the timings of the workers are not known. We ask each worker how much time he needs to do each job, but, the workers might lie to us. Therefore, we have to give the workers an incentive to tell us their true timings by paying them a ...
