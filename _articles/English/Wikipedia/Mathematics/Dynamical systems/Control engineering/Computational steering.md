---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_steering
offline_file: ""
offline_thumbnail: ""
uuid: 025648c0-8232-4127-b056-61e996bdf30f
updated: 1484308728
title: Computational steering
tags:
    - Examples
    - System design
    - Disambiguation
    - Computational steering software
categories:
    - Control engineering
---
Computational steering is the practice of manually intervening with an otherwise autonomous computational process, to change its outcome. The term is commonly used within the numerical simulation community, where it more specifically refers to the practice of interactively guiding a computational experiment into some region of interest.[citation needed]
