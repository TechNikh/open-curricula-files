---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undergone
offline_file: ""
offline_thumbnail: ""
uuid: 562e69d7-1f29-46ac-98b0-f7703e909605
updated: 1484310290
title: undergone
categories:
    - Dictionary
---
undergo
     v 1: of mental or physical states or experiences; "get an idea";
          "experience vertigo"; "get nauseous"; "undergo a strange
          sensation"; "The chemical undergoes a sudden change";
          "The fluid undergoes shear"; "receive injuries"; "have a
          feeling" [syn: {experience}, {receive}, {have}, {get}]
     2: go or live through; "We had many trials to go through"; "he
        saw action in Viet Nam" [syn: {experience}, {see}, {go
        through}]
     3: accept or undergo, often unwillingly; "We took a pay cut"
        [syn: {take}, {submit}]
     [also: {underwent}, {undergone}]
