---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodic_Ramsey_theory
offline_file: ""
offline_thumbnail: ""
uuid: 39878c5e-bf27-42c2-a0db-f8404ba3e550
updated: 1484308775
title: Ergodic Ramsey theory
tags:
    - History
    - "Szemerédi's theorem"
categories:
    - Ergodic theory
---
