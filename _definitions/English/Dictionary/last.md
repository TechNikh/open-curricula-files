---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/last
offline_file: ""
offline_thumbnail: ""
uuid: add2dc3a-bedf-4272-9c0c-426c9dfa10e8
updated: 1484310319
title: last
categories:
    - Dictionary
---
last
     adj 1: immediately past; "last Thursday"; "the last chapter we
            read" [syn: {last(a)}]
     2: coming after all others in time or space or degree or being
        the only one remaining; "the last time I saw Paris"; "the
        last day of the month"; "had the last word"; "waited until
        the last minute"; "he raised his voice in a last supreme
        call"; "the last game of the season"; "down to his last
        nickel" [ant: {intermediate}, {first}]
     3: occurring at or forming an end or termination; "his
        concluding words came as a surprise"; "the final chapter";
        "the last days of the dinosaurs"; "terminal leave" [syn: {concluding},
         ...
