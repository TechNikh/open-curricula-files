---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quiet
offline_file: ""
offline_thumbnail: ""
uuid: c0c9c606-dc3e-4a93-acea-73ba9a977a58
updated: 1484310441
title: quiet
categories:
    - Dictionary
---
quiet
     adj 1: characterized by an absence or near absence of agitation or
            activity; "a quiet life"; "a quiet throng of
            onlookers"; "quiet peace-loving people"; "the factions
            remained quiet for almost 10 years" [ant: {unquiet}]
     2: free of noise or uproar; or making little if any sound; "a
        quiet audience at the concert"; "the room was dark and
        quiet" [ant: {noisy}]
     3: not showy or obtrusive; "clothes in quiet good taste" [syn:
        {restrained}]
     4: in a softened tone; "hushed voices"; "muted trumpets"; "a
        subdued whisper"; "a quiet reprimand" [syn: {hushed}, {muted},
         {subdued}]
     5: without untoward ...
