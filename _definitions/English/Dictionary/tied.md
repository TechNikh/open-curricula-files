---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tied
offline_file: ""
offline_thumbnail: ""
uuid: 389a12ab-366b-47b8-83d4-0c1da1c3bb9a
updated: 1484310486
title: tied
categories:
    - Dictionary
---
tied
     adj 1: bound or secured closely; "the guard was found trussed up
            with his arms and legs securely tied"; "a trussed
            chicken" [syn: {trussed}]
     2: bound together by or as if by a strong rope; especially as
        by a bond of affection; "people tied by blood or marriage"
     3: fastened with strings or cords; "a neatly tied bundle" [syn:
         {fastened}] [ant: {untied}]
     4: closed with a lace; "snugly laced shoes" [syn: {laced}]
        [ant: {unlaced}]
     5: of the score in a contest; "the score is tied" [syn: {tied(p)},
         {even}, {level(p)}]
