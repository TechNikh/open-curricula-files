---
version: 1
type: video
provider: YouTube
id: KWAsz59F8gA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Nuclear%20Chemistry-%20Crash%20Course%20Chemistry%20%2338.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ad1fa0bb-5b0d-4a0d-9e17-6634a3a01b76
updated: 1486069678
title: 'Nuclear Chemistry- Crash Course Chemistry #38'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/KWAsz59F8gA/default.jpg
    - https://i3.ytimg.com/vi/KWAsz59F8gA/1.jpg
    - https://i3.ytimg.com/vi/KWAsz59F8gA/2.jpg
    - https://i3.ytimg.com/vi/KWAsz59F8gA/3.jpg
---
