---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heterozygous
offline_file: ""
offline_thumbnail: ""
uuid: 7b1479b8-670e-411f-807e-d5404ffa925b
updated: 1484310301
title: heterozygous
categories:
    - Dictionary
---
heterozygous
     adj : having dissimilar alleles at corresponding chromosomal loci;
           "heterozygous for eye color" [ant: {homozygous}]
