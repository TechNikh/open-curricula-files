---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equator
offline_file: ""
offline_thumbnail: ""
uuid: 51ce1e0d-a2f3-4241-8751-9ed2f9e0ccfc
updated: 1484310271
title: equator
categories:
    - Dictionary
---
equator
     n 1: an imaginary line around the Earth forming the great circle
          that is equidistant from the north and south poles; "the
          equator is the boundary between the northern and
          southern hemispheres"
     2: a circle dividing a sphere or other surface into two usually
        equal and symmetrical parts
