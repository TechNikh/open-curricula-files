---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longitudinal
offline_file: ""
offline_thumbnail: ""
uuid: 94d2263e-410b-451f-a7ae-afc05cfeb15c
updated: 1484310333
title: longitudinal
categories:
    - Dictionary
---
longitudinal
     adj 1: of or relating to lines of longitude; "longitudinal
            reckoning by the navigator"
     2: running lengthwise; "a thin longitudinal strip";
        "longitudinal measurements of the hull"
     3: over an extended time; "a longitudinal study of twins"
