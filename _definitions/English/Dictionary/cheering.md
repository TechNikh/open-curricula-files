---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheering
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483341
title: cheering
categories:
    - Dictionary
---
cheering
     adj 1: providing freedom from worry [syn: {comforting}, {satisfying}]
     2: bringing cheer or gladness; "cheering news"
     n : encouragement in the form of cheers from spectators; "it's
         all over but the shouting" [syn: {shouting}]
