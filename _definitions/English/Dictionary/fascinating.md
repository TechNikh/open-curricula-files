---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fascinating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516101
title: fascinating
categories:
    - Dictionary
---
fascinating
     adj 1: capable of arousing and holding the attention; "a
            fascinating story" [syn: {absorbing}, {engrossing}, {gripping},
             {riveting}]
     2: capturing interest as if by a spell; "bewitching smile";
        "Roosevelt was a captivating speaker"; "enchanting music";
        "an enthralling book"; "antique papers of entrancing
        design"; "a fascinating woman" [syn: {bewitching}, {captivating},
         {enchanting}, {enthralling}, {entrancing}]
