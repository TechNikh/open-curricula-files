---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/see-saw
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495041
title: see-saw
categories:
    - Dictionary
---
seesaw
     n : a plaything consisting of a board balanced on a fulcrum; the
         board is ridden up and down by children at either end
         [syn: {teeter-totter}, {teeterboard}, {tilting board}, {dandle
         board}]
     v 1: ride on a plank
     2: move up and down as if on a seesaw
     3: move unsteadily, with a rocking motion [syn: {teeter}, {totter}]
