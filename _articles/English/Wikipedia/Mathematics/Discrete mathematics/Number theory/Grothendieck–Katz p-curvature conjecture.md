---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grothendieck%E2%80%93Katz_p-curvature_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: f32bea09-9eac-4891-83b5-a84ed8a323e7
updated: 1484308644
title: Grothendieck–Katz p-curvature conjecture
tags:
    - Formulation
    - "Katz's formulation for the Galois group"
    - Progress
    - History
    - Notes
categories:
    - Number theory
---
In mathematics, the Grothendieck–Katz p-curvature conjecture is a local-global principle for linear ordinary differential equations, related to differential Galois theory and in a loose sense analogous to the result in the Chebotarev density theorem considered as the polynomial case. It is a conjecture of Alexander Grothendieck from the late 1960s, and apparently not published by him in any form.
