---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graciela_Chichilnisky
offline_file: ""
offline_thumbnail: ""
uuid: b2d3699e-9c05-4847-ab78-9ce1c7ffe830
updated: 1484308513
title: Graciela Chichilnisky
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Debreu%252C_G%25C3%25A9rard_%25281921-2004%2529.jpeg'
tags:
    - Background and education
    - Career
    - Research
    - Litigation
categories:
    - Game theorists
---
Graciela Chichilnisky (born 1944) is an Argentine American mathematical economist and an authority on climate change. She is a professor of economics at Columbia University.[1][2]
