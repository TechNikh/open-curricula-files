---
version: 1
type: video
provider: YouTube
id: wtLTb_VaI-k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%231a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cb74353f-01c6-43a1-8aa4-b086ea76513d
updated: 1486069692
title: '2011 Calculus BC Free Response #1a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/wtLTb_VaI-k/default.jpg
    - https://i3.ytimg.com/vi/wtLTb_VaI-k/1.jpg
    - https://i3.ytimg.com/vi/wtLTb_VaI-k/2.jpg
    - https://i3.ytimg.com/vi/wtLTb_VaI-k/3.jpg
---
