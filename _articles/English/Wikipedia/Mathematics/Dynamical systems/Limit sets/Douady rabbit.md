---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Douady_rabbit
offline_file: ""
offline_thumbnail: ""
uuid: c5efbd2f-70f1-4a09-a9d6-78115e4513fd
updated: 1484308808
title: Douady rabbit
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cpmj.png
tags:
    - Name
    - Forms of the complex quadratic map
    - Mandelbrot and filled Julia sets
    - The Douady rabbit
categories:
    - Limit sets
---
The Douady rabbit is any of various particular filled Julia sets associated with the parameter near the center period 3 buds of Mandelbrot set for complex quadratic map. The rabbit is a parabolic Julia set for internal angle 1/3 (parameter c is a root point between period 1 and period 3 components of Mandelbrot set).
