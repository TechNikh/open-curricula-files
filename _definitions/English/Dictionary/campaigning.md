---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/campaigning
offline_file: ""
offline_thumbnail: ""
uuid: 6a599905-6626-4b1a-a63c-1df1d146c276
updated: 1484310192
title: campaigning
categories:
    - Dictionary
---
campaigning
     n : the campaign of a candidate to be elected [syn: {candidacy},
          {candidature}, {electioneering}, {political campaign}]
