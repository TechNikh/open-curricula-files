---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extensively
offline_file: ""
offline_thumbnail: ""
uuid: 842ba53d-7f72-4738-bbf2-be7e3ead0bc4
updated: 1484310214
title: extensively
categories:
    - Dictionary
---
extensively
     adv 1: in a widespread way; "oxidation ponds are extensively used
            for sewage treatment in the Midwest"
     2: to a great extent
