---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/duality
offline_file: ""
offline_thumbnail: ""
uuid: d4bc0dba-f111-46e0-b403-cfcfe86a2424
updated: 1484310597
title: duality
categories:
    - Dictionary
---
duality
     n 1: being twofold; a classification into two opposed parts or
          subclasses; "the dichotomy between eastern and western
          culture" [syn: {dichotomy}]
     2: (physics) the property of matter and electromagnetic
        radiation that is characterized by the fact that some
        properties can be explained best by wave theory and others
        by particle theory [syn: {wave-particle duality}]
     3: (geometry) the interchangeability of the roles of points and
        planes in the theorems of projective geometry
