---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414041
title: partner
categories:
    - Dictionary
---
partner
     n 1: a person's partner in marriage [syn: {spouse}, {married
          person}, {mate}, {better half}]
     2: an associate who works with others toward a common goal;
        "partners in crime" [syn: {collaborator}, {cooperator}, {pardner}]
     3: a person who is a member of a partnership
     v 1: provide with a partner
     2: act as a partner; "Astaire partnered Rogers"
