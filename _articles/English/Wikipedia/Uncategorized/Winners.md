---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Winners
offline_file: ""
offline_thumbnail: ""
uuid: f14d4bf0-dda9-4cfa-ac42-20ad1f393547
updated: 1484308476
title: Winners
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MarkvilleWinners.jpg
tags:
    - History
categories:
    - Uncategorized
---
Winners Merchants International L.P is a chain of off-price Canadian department stores owned by TJX Companies which also owns HomeSense. It offers brand name clothing, footwear, bedding, furniture, fine jewellery, beauty products, and housewares. According to an example in the Winners FAQ, an item selling there for $29.99 was made to sell for 20-60% more at a specialty or department store.[1] The company operates 234 stores across Canada.[2] Winners' market niche is similar to that of its American sister store T.J. Maxx.
