---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Locus_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 047e8c83-77e3-4574-91a3-a2bd34399c14
updated: 1484308935
title: Locus (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Locus_Curve.svg.png
tags:
    - History and philosophy
    - Examples in plane geometry
    - Proof of a locus
    - Examples
    - First example
    - Second example
    - Third example
    - Fourth example
categories:
    - Elementary geometry
---
In geometry, a locus (plural: loci) (Latin word for "place", "location") is a set of points (commonly, a line, a line segment, a curve or a surface), whose location satisfies or is determined by one or more specified conditions.[1][2]
