---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continent
offline_file: ""
offline_thumbnail: ""
uuid: 05f516a7-3eef-4120-8368-242f38c0d692
updated: 1484310252
title: continent
categories:
    - Dictionary
---
continent
     adj 1: having control over urination and defecation [ant: {incontinent}]
     2: abstaining from sexual intercourse; "celibate priests" [syn:
         {celibate}]
     n 1: one of the large landmasses of the earth; "there are seven
          continents"; "pioneers had to cross the continent on
          foot"
     2: the European mainland; "Englishmen like to visit the
        Continent but they wouldn't like to live there"
