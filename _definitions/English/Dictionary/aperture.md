---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aperture
offline_file: ""
offline_thumbnail: ""
uuid: 9cabdb82-f6f8-45c1-aee9-9863e6c3cd4e
updated: 1484310319
title: aperture
categories:
    - Dictionary
---
aperture
     n 1: a device that controls amount of light admitted
     2: a natural opening in something
     3: an man-made opening; usually small
