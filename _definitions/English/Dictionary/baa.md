---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484364901
title: baa
categories:
    - Dictionary
---
baa
     n : the cry made by sheep
     v : cry plaintively; "The lambs were bleating" [syn: {bleat}, {blate},
          {blat}]
