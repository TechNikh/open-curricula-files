---
title: Epiglottis Swiss-Rolling and Infolding during Swallowing
youtube_id: aPMw7acrVro
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - pathway
---
