---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refresh
offline_file: ""
offline_thumbnail: ""
uuid: 88f658f7-da23-4ac4-a48e-62b1f83320b2
updated: 1484310220
title: refresh
categories:
    - Dictionary
---
refresh
     v 1: refresh one's memory; "I reviewed the material before the
          test" [syn: {review}, {brush up}]
     2: make (to feel) fresh; "The cool water refreshed us" [syn: {freshen}]
     3: become or make oneself fresh again; "She freshened up after
        the tennis game" [syn: {freshen}, {refreshen}, {freshen up}]
     4: make fresh again [syn: {freshen}, {refreshen}] [ant: {tire}]
