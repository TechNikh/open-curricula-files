---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ascendency
offline_file: ""
offline_thumbnail: ""
uuid: 97d3837d-1eeb-4466-8357-df9a09a4f4bd
updated: 1484308767
title: Ascendency
categories:
    - Entropy and information
---
Ascendency is a quantitative attribute of an ecosystem, defined as a function of the ecosystem's trophic network. Ascendency is derived using mathematical tools from information theory. It is intended to capture in a single index the ability of an ecosystem to prevail against disturbance by virtue of its combined organization and size.
