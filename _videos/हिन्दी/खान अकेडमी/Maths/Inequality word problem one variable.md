---
version: 1
type: video
provider: YouTube
id: r7B1W_d36Ak
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d655aee1-e1e8-435a-8fd1-f489f90e0816
updated: 1486069614
title: Inequality word problem one variable
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/r7B1W_d36Ak/default.jpg
    - https://i3.ytimg.com/vi/r7B1W_d36Ak/1.jpg
    - https://i3.ytimg.com/vi/r7B1W_d36Ak/2.jpg
    - https://i3.ytimg.com/vi/r7B1W_d36Ak/3.jpg
---
