---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gr%C3%B6bner_basis'
offline_file: ""
offline_thumbnail: ""
uuid: 84add433-4488-4c26-bd28-41b00463173c
updated: 1484309149
title: Gröbner basis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parabola_and_three_vertical_lines.svg.png
tags:
    - Background
    - Polynomial ring
    - Monomial ordering
    - Reduction
    - Formal definition
    - Example and counterexample
    - Properties and applications of Gröbner bases
    - Equality of ideals
    - Membership and inclusion of ideals
    - Solutions of a system of algebraic equations
    - Dimension, degree and Hilbert series
    - Elimination
    - Intersecting ideals
    - Implicitization of a rational curve
    - Saturation
    - Definition of the saturation
    - Computation of the saturation
    - Effective Nullstellensatz
    - Implicitization in higher dimension
    - Implementations
categories:
    - Algebraic geometry
---
In mathematics, and more specifically in computer algebra, computational algebraic geometry, and computational commutative algebra, a Gröbner basis is a particular kind of generating set of an ideal in a polynomial ring over a field K[x1, ..,xn]. A Gröbner basis allows many important properties of the ideal and the associated algebraic variety to be deduced easily, such as the dimension and the number of zeros when it is finite. Gröbner basis computation is one of the main practical tools for solving systems of polynomial equations and computing the images of algebraic varieties under projections or rational maps.
