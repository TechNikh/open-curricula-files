---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Detailed_balance
offline_file: ""
offline_thumbnail: ""
uuid: 06acd98d-6b8c-4fad-b1ea-f0c3fc81e87f
updated: 1484308797
title: Detailed balance
tags:
    - History
    - Microscopical background
    - Reversible Markov chains
    - Detailed balance and entropy increase
    - "Wegscheider's conditions for the generalized mass action law"
    - Dissipation in systems with detailed balance
    - Onsager reciprocal relations and detailed balance
    - Semi-detailed balance
    - Dissipation in systems with semi-detailed balance
    - Detailed balance for systems with irreversible reactions
categories:
    - Non-equilibrium thermodynamics
---
The principle of detailed balance is formulated for kinetic systems which are decomposed into elementary processes (collisions, or steps, or elementary reactions): At equilibrium, each elementary process should be equilibrated by its reverse process.
