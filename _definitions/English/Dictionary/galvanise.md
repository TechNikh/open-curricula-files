---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galvanise
offline_file: ""
offline_thumbnail: ""
uuid: 62f88a52-2fea-4653-ab1c-bf48caffc320
updated: 1484310605
title: galvanise
categories:
    - Dictionary
---
galvanise
     v 1: to stimulate to action ; "..startled him awake"; "galvanized
          into action" [syn: {startle}, {galvanize}]
     2: cover with zinc; "galvanize steel" [syn: {galvanize}]
     3: stimulate (muscles) by administering a shock [syn: {galvanize}]
