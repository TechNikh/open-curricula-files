---
version: 1
type: video
provider: YouTube
id: 01tJDdcKyHk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 10d9541f-134f-49ae-97cd-9ea018d49b30
updated: 1486069611
title: Pieces of fruit
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/01tJDdcKyHk/default.jpg
    - https://i3.ytimg.com/vi/01tJDdcKyHk/1.jpg
    - https://i3.ytimg.com/vi/01tJDdcKyHk/2.jpg
    - https://i3.ytimg.com/vi/01tJDdcKyHk/3.jpg
---
