---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accelerated
offline_file: ""
offline_thumbnail: ""
uuid: 09c14221-b073-41c7-8609-b588789eaed5
updated: 1484310202
title: accelerated
categories:
    - Dictionary
---
accelerated
     adj 1: caused to go more rapidly [syn: {speeded up}]
     2: speeded up, as of an academic course; "in an accelerated
        program in school"
