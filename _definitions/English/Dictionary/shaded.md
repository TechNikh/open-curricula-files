---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaded
offline_file: ""
offline_thumbnail: ""
uuid: a5bc2df5-f0c5-4b6d-936e-9f8d5ebc1b5b
updated: 1484310138
title: shaded
categories:
    - Dictionary
---
shaded
     adj 1: protected from heat and light with shade or shadow; "shaded
            avenues"; "o'er the shaded billows rushed the night"-
            Alexander Pope [ant: {unshaded}]
     2: (of pictures or drawings) drawn or painted with degrees or
        gradations of shadow; "the shaded areas of the face seemed
        to recede" [ant: {unshaded}]
