---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artin_billiard
offline_file: ""
offline_thumbnail: ""
uuid: f518141b-aba5-4509-acb7-082a2fc8804f
updated: 1484308724
title: Artin billiard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_4.jpg
categories:
    - Chaotic maps
---
In mathematics and physics, the Artin billiard is a type of a dynamical billiard first studied by Emil Artin in 1924. It describes the geodesic motion of a free particle on the non-compact Riemann surface 
  
    
      
        
          H
        
        
          /
        
        Γ
        ,
      
    
    {\displaystyle \mathbb {H} /\Gamma ,}
  
 where 
  
    
      
        
          H
        
      
    
    {\displaystyle \mathbb {H} }
  
 is the upper half-plane endowed with the Poincaré metric and 
  
    
      
        Γ
        =
        P
        S
        L
        (
        2
        ,
        
          Z
        
        )
      
    
    {\displaystyle \Gamma ...
