---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loser
offline_file: ""
offline_thumbnail: ""
uuid: dde145c2-67ec-4697-b2db-de5348ba4277
updated: 1484310557
title: loser
categories:
    - Dictionary
---
loser
     n 1: a contestant who loses the contest [syn: {also-ran}] [ant: {winner}]
     2: a person with a record of failing; someone who loses
        consistently [syn: {failure}, {nonstarter}, {unsuccessful
        person}] [ant: {achiever}]
     3: a gambler who loses a bet [ant: {winner}]
