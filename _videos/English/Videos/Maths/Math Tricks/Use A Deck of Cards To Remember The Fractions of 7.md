---
version: 1
type: video
provider: YouTube
id: wooVm21j9sc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a74db6aa-277d-4d5c-99ae-460788dfdfb0
updated: 1486069623
title: Use A Deck of Cards To Remember The Fractions of 7
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/wooVm21j9sc/default.jpg
    - https://i3.ytimg.com/vi/wooVm21j9sc/1.jpg
    - https://i3.ytimg.com/vi/wooVm21j9sc/2.jpg
    - https://i3.ytimg.com/vi/wooVm21j9sc/3.jpg
---
