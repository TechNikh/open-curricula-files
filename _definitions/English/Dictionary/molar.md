---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/molar
offline_file: ""
offline_thumbnail: ""
uuid: 61782658-36bc-4593-bfbd-da8c7d5bf864
updated: 1484310373
title: molar
categories:
    - Dictionary
---
molar
     adj 1: designating a solution containing 1 mole of solute per 1000
            grams of solvent [syn: {molal}]
     2: of or pertaining to molar teeth; "molar weight"
     3: containing one mole of a substance; "molar weight"
     4: pertaining to large units of behavior; "such molar problems
        of personality as the ego functions"--R.R. Hunt [syn: {molar(a)}]
        [ant: {molecular(a)}]
     n : grinding tooth with a broad crown; located behind the
         premolars [syn: {grinder}]
