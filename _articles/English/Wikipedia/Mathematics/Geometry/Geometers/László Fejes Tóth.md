---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A1szl%C3%B3_Fejes_T%C3%B3th'
offline_file: ""
offline_thumbnail: ""
uuid: 38c76c87-7cd3-400a-b986-7923074bc481
updated: 1484309100
title: László Fejes Tóth
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/150px-L%25C3%25A1szl%25C3%25B3_Fejes_T%25C3%25B3th-1958-TableTennis.png'
tags:
    - Early life and career
    - Work on regular figures
    - Honors and recognition
    - Partial bibliography
categories:
    - Geometers
---
László Fejes Tóth (Hungarian: Fejes Tóth László, pronounced [ˈfɛjɛʃ ˈtoːt ˈlaːsloː] Szeged, 12 March 1915 – Budapest, 17 March 2005) was a Hungarian mathematician who specialized in geometry. He proved that a lattice pattern is the most efficient way to pack centrally symmetric convex sets on the Euclidean plane (a generalization of Thue's theorem, a 2-dimensional analog of the Kepler conjecture).[1] He also investigated the sphere packing problem. He was the first to show, in 1953, that proof of the Kepler conjecture can be reduced to a finite case analysis and, later, that the problem might be solved using a computer.
