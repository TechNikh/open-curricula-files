---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/directive
offline_file: ""
offline_thumbnail: ""
uuid: 5e7d0538-44d6-4d85-8d98-9f38c97a1fb0
updated: 1484310599
title: directive
categories:
    - Dictionary
---
directive
     adj : showing the way by conducting or leading; imposing direction
           on; "felt his mother's directing arm around him"; "the
           directional role of science on industrial progress"
           [syn: {directing}, {directional}, {guiding}]
     n : a pronouncement encouraging or banning some activity; "the
         boss loves to send us directives"
