---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recycling
offline_file: ""
offline_thumbnail: ""
uuid: aa3661e8-d257-4752-b1a9-1427e0aefa30
updated: 1484310238
title: recycling
categories:
    - Dictionary
---
recycling
     n : the act of processing used or abandoned materials for use in
         creating new products
