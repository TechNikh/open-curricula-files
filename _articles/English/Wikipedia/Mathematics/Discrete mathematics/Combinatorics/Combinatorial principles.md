---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_principles
offline_file: ""
offline_thumbnail: ""
uuid: 50208c89-d114-4db5-8af3-ee8c37e3f785
updated: 1484308549
title: Combinatorial principles
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Inclusion-exclusion.svg.png
tags:
    - Rule of sum
    - Rule of product
    - Inclusion-exclusion principle
    - Bijective proof
    - Double counting
    - Pigeonhole principle
    - Method of distinguished element
    - Generating function
    - Recurrence relation
categories:
    - Combinatorics
---
The rule of sum, rule of product, and inclusion-exclusion principle are often used for enumerative purposes. Bijective proofs are utilized to demonstrate that two sets have the same number of elements. The pigeonhole principle often ascertains the existence of something or is used to determine the minimum or maximum number of something in a discrete context. Many combinatorial identities arise from double counting methods or the method of distinguished element. Generating functions and recurrence relations are powerful tools that can be used to manipulate sequences, and can describe if not resolve many combinatorial situations.
