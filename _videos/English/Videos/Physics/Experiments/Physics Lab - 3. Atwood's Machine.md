---
version: 1
type: video
provider: YouTube
id: GY9LWUCkrU4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1183b442-57e8-4691-8d21-35e617ad8c8c
updated: 1486069640
title: "Physics Lab - 3. Atwood's Machine"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GY9LWUCkrU4/default.jpg
    - https://i3.ytimg.com/vi/GY9LWUCkrU4/1.jpg
    - https://i3.ytimg.com/vi/GY9LWUCkrU4/2.jpg
    - https://i3.ytimg.com/vi/GY9LWUCkrU4/3.jpg
---
