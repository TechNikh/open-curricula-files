---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malnourished
offline_file: ""
offline_thumbnail: ""
uuid: 7ab050c3-4acf-4d1c-ac32-f43c0dbb0109
updated: 1484310535
title: malnourished
categories:
    - Dictionary
---
malnourished
     adj : not being provided with adequate nourishment [ant: {nourished}]
