---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cosine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484371501
title: cosine
categories:
    - Dictionary
---
cosine
     n : ratio of the adjacent side to the hypotenuse of a
         right-angled triangle [syn: {cos}]
