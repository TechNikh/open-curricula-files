---
version: 1
type: video
provider: YouTube
id: gfBcM3uvWfs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Molecular%20and%20Empirical%20Formulas.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 682aa99e-60e9-4a76-a8cc-20fecae853b5
updated: 1486069583
title: Molecular and Empirical Formulas
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/gfBcM3uvWfs/default.jpg
    - https://i3.ytimg.com/vi/gfBcM3uvWfs/1.jpg
    - https://i3.ytimg.com/vi/gfBcM3uvWfs/2.jpg
    - https://i3.ytimg.com/vi/gfBcM3uvWfs/3.jpg
---
Introduction to molecular and empirical formulas.  Calculating molecular mass.
More free lessons at: http://www.khanacademy.org/video?v=gfBcM3uvWfs
