---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jail
offline_file: ""
offline_thumbnail: ""
uuid: 0405c841-6171-46b2-b349-c1b2c6bd3611
updated: 1484310543
title: jail
categories:
    - Dictionary
---
jail
     n : a correctional institution used to detain persons who are in
         the lawful custody of the government (either accused
         persons awaiting trial or convicted persons serving a
         sentence) [syn: {jailhouse}, {gaol}, {clink}, {slammer}]
     v : lock up or confine, in or as in a jail; "The suspects were
         imprisoned without trial"; "the murderer was incarcerated
         for the rest of his life" [syn: {imprison}, {incarcerate},
          {lag}, {immure}, {put behind bars}, {jug}, {gaol}, {put
         away}, {remand}]
