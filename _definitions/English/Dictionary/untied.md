---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/untied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483042
title: untied
categories:
    - Dictionary
---
untied
     adj 1: not tied [syn: {unfastened}] [ant: {tied}]
     2: with laces not tied; "teenagers slopping around in unlaced
        sneakers" [syn: {unlaced}] [ant: {laced}]
     3: not bound by shackles and chains [syn: {unchained}, {unfettered},
         {unshackled}]
