---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discovered
offline_file: ""
offline_thumbnail: ""
uuid: 4aa7c012-0353-47ad-abd3-4276c4f4b3d7
updated: 1484308471
title: Discovery (observation)
tags:
    - Description
    - Within science
    - Exploration
categories:
    - Uncategorized
---
Discovery is the act of detecting something new, or something "old" that had been unrecognized as meaningful. With reference to sciences and academic disciplines, discovery is the observation of new phenomena, new actions, or new events and providing new reasoning to explain the knowledge gathered through such observations with previously acquired knowledge from abstract thought and everyday experiences. A discovery may sometimes be based on earlier discoveries, collaborations, or ideas. Some discoveries represent a radical breakthrough in knowledge or technology.
