---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fixed_points_of_isometry_groups_in_Euclidean_space
offline_file: ""
offline_thumbnail: ""
uuid: f0c09ff6-d7ae-4ff4-8e0a-4573c1fd022e
updated: 1484308780
title: Fixed points of isometry groups in Euclidean space
tags:
    - 1D
    - 2D
    - 3D
    - Arbitrary dimension
categories:
    - Fixed points (mathematics)
---
A fixed point of an isometry group is a point that is a fixed point for every isometry in the group. For any isometry group in Euclidean space the set of fixed points is either empty or an affine space.
