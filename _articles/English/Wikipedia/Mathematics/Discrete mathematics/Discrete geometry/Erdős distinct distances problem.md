---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s_distinct_distances_problem'
offline_file: ""
offline_thumbnail: ""
uuid: edf4402d-223b-47bc-a9b1-581a182f65a1
updated: 1484308583
title: Erdős distinct distances problem
tags:
    - The conjecture
    - Partial results
    - Higher dimensions
categories:
    - Discrete geometry
---
In discrete geometry, the Erdős distinct distances problem states that between n distinct points on a plane there are at least n1 − o(1) distinct distances. It was posed by Paul Erdős in 1946 and proven by Guth & Katz (2015).
