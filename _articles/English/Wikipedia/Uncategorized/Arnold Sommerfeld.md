---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sommerfeld
offline_file: ""
offline_thumbnail: ""
uuid: aeee5ebc-26f4-4327-b2b0-a4e2e803dec3
updated: 1484308307
title: Arnold Sommerfeld
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Sommerfeld%252CArnold_1935_Stuttgart.jpg'
tags:
    - Early life and education
    - Career
    - Göttingen
    - Aachen
    - Munich
    - Bibliography
    - Articles
    - Books
categories:
    - Uncategorized
---
Arnold Johannes Wilhelm Sommerfeld, ForMemRS[2] (5 December 1868 – 26 April 1951) was a German theoretical physicist who pioneered developments in atomic and quantum physics, and also educated and mentored a large number of students for the new era of theoretical physics. He served as PhD supervisor for many Nobel Prize winners in physics and chemistry (only J. J. Thomson's record of mentorship is comparable to his).
