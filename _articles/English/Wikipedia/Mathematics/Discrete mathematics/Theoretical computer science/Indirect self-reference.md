---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indirect_self-reference
offline_file: ""
offline_thumbnail: ""
uuid: 10fd9078-5908-4144-9630-5b21156e9481
updated: 1484308675
title: Indirect self-reference
categories:
    - Theoretical computer science
---
For example, define the function f such that f(x) = x(x). Any function passed as an argument to f is invoked with itself as an argument, and thus in any use of that argument is indirectly referring to itself.
