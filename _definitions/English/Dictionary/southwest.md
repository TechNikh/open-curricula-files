---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/southwest
offline_file: ""
offline_thumbnail: ""
uuid: 1d4cbc0d-ceea-41cc-abaf-51e97818be95
updated: 1484310461
title: southwest
categories:
    - Dictionary
---
south-west
     adv : to, toward, or in the southwest [syn: {southwest}, {sou'west}]
