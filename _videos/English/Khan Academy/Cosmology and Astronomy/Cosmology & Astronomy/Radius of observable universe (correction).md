---
version: 1
type: video
provider: YouTube
id: b6VQv76BQDs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Radius%20of%20observable%20universe%20%28correction%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8bd0e172-ae8e-4a32-8970-ce435f60ead2
updated: 1486069686
title: Radius of observable universe (correction)
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/b6VQv76BQDs/default.jpg
    - https://i3.ytimg.com/vi/b6VQv76BQDs/1.jpg
    - https://i3.ytimg.com/vi/b6VQv76BQDs/2.jpg
    - https://i3.ytimg.com/vi/b6VQv76BQDs/3.jpg
---
