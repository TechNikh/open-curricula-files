---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Guillotine_problem
offline_file: ""
offline_thumbnail: ""
uuid: 88e054e1-0988-4074-9ee3-e05380164399
updated: 1484308614
title: Guillotine problem
categories:
    - Discrete geometry
---
Closely related to packing problems and specifically to cutting stock and bin packing problems,[1] it is the question of how to get the maximum number of sheets of one rectangular size out of a larger sheet, only orthogonal cuts that bisect one component of the sheet are allowed, as on a paper cutting guillotine.
