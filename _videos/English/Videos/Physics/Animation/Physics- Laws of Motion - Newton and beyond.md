---
version: 1
type: video
provider: YouTube
id: Ixf9ZyZaE9Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9f996137-153c-4d28-9078-45c908ea0190
updated: 1486069654
title: 'Physics- Laws of Motion - Newton and beyond'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ixf9ZyZaE9Q/default.jpg
    - https://i3.ytimg.com/vi/Ixf9ZyZaE9Q/1.jpg
    - https://i3.ytimg.com/vi/Ixf9ZyZaE9Q/2.jpg
    - https://i3.ytimg.com/vi/Ixf9ZyZaE9Q/3.jpg
---
