---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smart
offline_file: ""
offline_thumbnail: ""
uuid: 55374f17-55e7-4214-92b4-02455d987c9d
updated: 1484310221
title: smart
categories:
    - Dictionary
---
smart
     adj 1: showing mental alertness and calculation and resourcefulness
            [ant: {stupid}]
     2: elegant and stylish; "chic elegance"; "a smart new dress";
        "a suit of voguish cut" [syn: {chic}, {voguish}]
     3: characterized by quickness and ease in learning; "some
        children are brighter in one subject than another"; "smart
        children talk earlier than the average" [syn: {bright}]
     4: improperly forward or bold; "don't be fresh with me";
        "impertinent of a child to lecture a grownup"; "an
        impudent boy given to insulting strangers" [syn: {fresh},
        {impertinent}, {impudent}, {overbold}, {saucy}, {sassy}]
     5: marked by ...
