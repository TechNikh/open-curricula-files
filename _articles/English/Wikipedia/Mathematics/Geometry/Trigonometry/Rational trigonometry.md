---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rational_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: 0cd8ca98-ce66-48fe-b83f-a73ba5ebbfab
updated: 1484309348
title: Rational trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Spread_as_ratio.svg.png
tags:
    - Approach
    - Quadrance
    - Quadrea
    - Spread
    - Calculating spread
    - Trigonometric
    - Vector/slope (two-variable)
    - Cartesian (three-variable)
    - Spread compared to angle
    - Turn and coturn
    - Twist
    - Spread polynomials
    - Identities
    - Explicit formulas
    - Recursion formula
    - Relation to Chebyshev polynomials
    - Composition
    - Coefficients in finite fields
    - Orthogonality
    - Generating functions
    - Differential equation
    - Spread periodicity theorem
    - Table of spread polynomials, with factorizations
    - Laws of rational trigonometry
    - Triple quad formula
    - "Pythagoras's theorem"
    - Spread law
    - Cross law
    - Triple spread formula
    - Trigonometry over arbitrary fields
    - 'Example: (verify the spread law in F13)'
    - Computation – complexity and efficiency
    - Notability and criticism
    - Notes
categories:
    - Trigonometry
---
Rational trigonometry is a proposed reformulation of metrical planar and solid geometries (which includes trigonometry) by Canadian mathematician Norman J. Wildberger, currently an associate professor of mathematics at the University of New South Wales. His ideas are set out in his 2005 book Divine Proportions: Rational Trigonometry to Universal Geometry.[1] According to New Scientist, part of his motivation for an alternative to traditional trigonometry was to avoid some problems that occur when infinite series are used in mathematics. Rational trigonometry avoids direct use of transcendental functions like sine and cosine by substituting their squared equivalents.[2] Wildberger draws ...
