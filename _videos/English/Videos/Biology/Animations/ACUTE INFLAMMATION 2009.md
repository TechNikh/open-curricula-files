---
version: 1
type: video
provider: YouTube
id: suCKm97yvyk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/ACUTE%20INFLAMMATION%202009.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 266dfcef-de5e-4fd0-9123-3c137d1734ae
updated: 1486069669
title: ACUTE INFLAMMATION 2009
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/suCKm97yvyk/default.jpg
    - https://i3.ytimg.com/vi/suCKm97yvyk/1.jpg
    - https://i3.ytimg.com/vi/suCKm97yvyk/2.jpg
    - https://i3.ytimg.com/vi/suCKm97yvyk/3.jpg
---
