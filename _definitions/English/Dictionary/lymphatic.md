---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lymphatic
offline_file: ""
offline_thumbnail: ""
uuid: d0201556-3275-4179-ba3d-573ddbee1411
updated: 1484310315
title: lymphatic
categories:
    - Dictionary
---
lymphatic
     adj : of or relating to or produced by lymph
