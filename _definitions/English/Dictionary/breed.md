---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breed
offline_file: ""
offline_thumbnail: ""
uuid: 4a729172-2fb6-4d32-a058-9a8c70f8a023
updated: 1484310301
title: breed
categories:
    - Dictionary
---
breed
     n 1: a special lineage; "a breed of Americans"
     2: a special variety of domesticated animals within a species;
        "he experimented on a particular breed of white rats"; "he
        created a new strain of sheep" [syn: {strain}, {stock}]
     3: half-caste offspring of parents of different races
        (especially of white and Indian parents) [syn: {half-breed}]
     4: a lineage or race of people [syn: {strain}]
     v 1: call forth [syn: {engender}, {spawn}]
     2: copulate with a female, used especially of horses; "The
        horse covers the mare" [syn: {cover}]
     3: of plants or animals; "She breeds dogs"
     4: have young (animals); "pandas rarely breed in ...
