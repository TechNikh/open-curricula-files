---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promise
offline_file: ""
offline_thumbnail: ""
uuid: bcb6c4ba-5ac4-4544-a9c2-7bacf7e67a4c
updated: 1484310522
title: promise
categories:
    - Dictionary
---
promise
     n 1: a verbal commitment by one person to another agreeing to do
          (or not to do) something in the future
     2: grounds for feeling hopeful about the future; "there is
        little or no promise that he will recover" [syn: {hope}]
     v 1: make a promise or commitment [syn: {assure}]
     2: promise to undertake or give; "I promise you my best effort"
     3: make a prediction about; tell in advance; "Call the outcome
        of an election" [syn: {predict}, {foretell}, {prognosticate},
         {call}, {forebode}, {anticipate}]
     4: give grounds for expectations; "The new results were
        promising"; "The results promised fame and glory"
