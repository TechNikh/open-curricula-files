---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tense
offline_file: ""
offline_thumbnail: ""
uuid: 63387eaa-e22c-44e7-acf6-f146cb4c385b
updated: 1484310178
title: tense
categories:
    - Dictionary
---
tense
     adj 1: in or of a state of physical or nervous tension [ant: {relaxed}]
     2: pronounced with relatively tense tongue muscles (e.g., the
        vowel sound in `beat') [ant: {lax}]
     3: taut or rigid; stretched tight; "tense piano strings" [ant:
        {lax}]
     n : a grammatical category of verbs used to express distinctions
         of time
     v 1: stretch or force to the limit; "strain the rope" [syn: {strain}]
     2: increase the tension on; "tense a rope"
     3: become tense or tenser; "He tensed up when he saw his
        opponent enter the room" [syn: {tense up}] [ant: {relax}]
     4: make tense and uneasy or nervous or anxious; [syn: {strain},
         {tense ...
