---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wall
offline_file: ""
offline_thumbnail: ""
uuid: cc2a9014-93b9-4203-b8e0-f45f97b90155
updated: 1484310355
title: wall
categories:
    - Dictionary
---
wall
     n 1: an architectural partition with a height and length greater
          than its thickness; used to divide or enclose an area or
          to support another structure; "the south wall had a
          small window"; "the walls were covered with pictures"
     2: an embankment built around a space for defensive purposes;
        "they stormed the ramparts of the city"; "they blew the
        trumpet and the walls came tumbling down" [syn: {rampart},
         {bulwark}]
     3: anything that suggests a wall in structure or function or
        effect; "a wall of water"; "a wall of smoke"; "a wall of
        prejudice"; "negotiations ran into a brick wall"
     4: a masonry fence ...
