---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simplicity
offline_file: ""
offline_thumbnail: ""
uuid: 967f314e-93f6-486d-ac0a-8e819277bbaf
updated: 1484310447
title: simplicity
categories:
    - Dictionary
---
simplicity
     n 1: the quality of being simple or uncompounded; "the simplicity
          of a crystal" [syn: {simpleness}] [ant: {complexity}]
     2: a lack of penetration or subtlety; "they took advantage of
        her simplicity" [syn: {simple mindedness}]
     3: absence of affectation or pretense
     4: freedom from difficulty or hardship or effort; "he rose
        through the ranks with apparent ease"; "they put it into
        containers for ease of transportation" [syn: {ease}, {easiness}]
        [ant: {difficulty}]
     5: lack of ornamentation; "the room was simply decorated with
        great restraint" [syn: {chasteness}, {restraint}]
