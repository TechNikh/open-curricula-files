---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indonesia
offline_file: ""
offline_thumbnail: ""
uuid: f02e6b31-9979-4e0d-8e64-f3962f79e348
updated: 1484310283
title: indonesia
categories:
    - Dictionary
---
Indonesia
     n : a republic in southeastern Asia on an archipelago including
         more than 13,000 islands; achieved independence from the
         Netherlands in 1945; the principal oil producer in the
         Far East and Pacific regions [syn: {Republic of Indonesia},
          {Dutch East Indies}]
