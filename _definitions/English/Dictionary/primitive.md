---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/primitive
offline_file: ""
offline_thumbnail: ""
uuid: 478d0500-d76b-4287-a0e0-fd58348380f1
updated: 1484310569
title: primitive
categories:
    - Dictionary
---
primitive
     adj 1: belonging to an early stage of technical development;
            characterized by simplicity and (often) crudeness;
            "the crude weapons and rude agricultural implements of
            early man"; "primitive movies of the 1890s";
            "primitive living conditions in the Appalachian
            mountains" [syn: {crude}, {rude}]
     2: little evolved from or characteristic of an earlier
        ancestral type; "archaic forms of life"; "primitive
        mammals"; "the okapi is a short-necked primitive cousin of
        the giraffe" [syn: {archaic}]
     3: used of preliterate or tribal or nonindustrial societies;
        "primitive societies"
     4: ...
