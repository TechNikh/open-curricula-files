---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anticlockwise
offline_file: ""
offline_thumbnail: ""
uuid: 256d24cf-5fcb-4984-9855-62031814826e
updated: 1484310365
title: anticlockwise
categories:
    - Dictionary
---
anticlockwise
     adj : in the direction opposite to the rotation of the hands of a
           clock [syn: {counterclockwise}, {contraclockwise}]
           [ant: {clockwise}]
     adv : in a direction opposite to the direction in which the hands
           of a clock move; "please move counterclockwise in a
           circle!" [syn: {counterclockwise}] [ant: {clockwise}]
