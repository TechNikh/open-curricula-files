---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_entropy_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: ff834b68-3d79-45d9-ba1d-123a8ab5ccac
updated: 1484308801
title: Maximum entropy thermodynamics
tags:
    - Maximum Shannon entropy
    - Philosophical Implications
    - The nature of the probabilities in statistical mechanics
    - Is entropy "real"?
    - Is ergodic theory relevant?
    - The Second Law
    - Caveats with the argument
    - Criticisms
    - Bibliography of cited references
categories:
    - Non-equilibrium thermodynamics
---
In physics, maximum entropy thermodynamics (colloquially, MaxEnt thermodynamics) views equilibrium thermodynamics and statistical mechanics as inference processes. More specifically, MaxEnt applies inference techniques rooted in Shannon information theory, Bayesian probability, and the principle of maximum entropy. These techniques are relevant to any situation requiring prediction from incomplete or insufficient data (e.g., image reconstruction, signal processing, spectral analysis, and inverse problems). MaxEnt thermodynamics began with two papers by Edwin T. Jaynes published in the 1957 Physical Review.[1][2]
