---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Holonomy
offline_file: ""
offline_thumbnail: ""
uuid: 10bc4d97-807e-4066-93c2-5cef46abdc5f
updated: 1484309223
title: Holonomy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parallel_Transport.svg.png
tags:
    - Definitions
    - Holonomy of a connection in a vector bundle
    - Holonomy of a connection in a principal bundle
    - Holonomy bundles
    - Monodromy
    - Local and infinitesimal holonomy
    - Ambrose–Singer theorem
    - Riemannian holonomy
    - Reducible holonomy and the de Rham decomposition
    - The Berger classification
    - Special holonomy and spinors
    - Applications to string theory
    - Affine holonomy
    - Etymology
    - Notes
categories:
    - Differential geometry
---
In differential geometry, the holonomy of a connection on a smooth manifold is a general geometrical consequence of the curvature of the connection measuring the extent to which parallel transport around closed loops fails to preserve the geometrical data being transported. For flat connections, the associated holonomy is a type of monodromy, and is an inherently global notion. For curved connections, holonomy has nontrivial local and global features.
