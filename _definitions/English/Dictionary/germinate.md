---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/germinate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484601181
title: germinate
categories:
    - Dictionary
---
germinate
     v 1: produce buds, branches, or germinate; "the potatoes
          sprouted" [syn: {shoot}, {spud}, {pullulate}, {bourgeon},
           {burgeon forth}, {sprout}]
     2: work out; "We have developed a new theory of evolution"
        [syn: {evolve}, {develop}]
     3: cause to grow or sprout; "the plentiful rain germinated my
        plants"
