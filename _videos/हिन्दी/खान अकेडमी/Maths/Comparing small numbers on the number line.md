---
version: 1
type: video
provider: YouTube
id: s2SQikiRfQQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 71f42dad-cf9a-4e5e-9fcd-a2e47aee5140
updated: 1486069616
title: Comparing small numbers on the number line
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/s2SQikiRfQQ/default.jpg
    - https://i3.ytimg.com/vi/s2SQikiRfQQ/1.jpg
    - https://i3.ytimg.com/vi/s2SQikiRfQQ/2.jpg
    - https://i3.ytimg.com/vi/s2SQikiRfQQ/3.jpg
---
