---
version: 1
type: video
provider: YouTube
id: 4VDOrVEDjxk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 18cc1937-e303-494a-a133-f22bf439d195
updated: 1486069640
title: 'JC2001 - Wave Dispersion and Soliton Waves Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/4VDOrVEDjxk/default.jpg
    - https://i3.ytimg.com/vi/4VDOrVEDjxk/1.jpg
    - https://i3.ytimg.com/vi/4VDOrVEDjxk/2.jpg
    - https://i3.ytimg.com/vi/4VDOrVEDjxk/3.jpg
---
