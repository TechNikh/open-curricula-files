---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sty
offline_file: ""
offline_thumbnail: ""
uuid: 1164ab57-1478-4873-8412-9e1cef6025da
updated: 1484308329
title: Sty
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fourneau_St-Michel_-_Porcherie_%2528Forri%25C3%25A8res%2529.JPG'
tags:
    - Family farm hog pen
    - The hog pen
    - Slopping the hogs
categories:
    - Uncategorized
---
A sty or pigsty is a small-scale outdoor enclosure for raising domestic pigs. It is sometimes referred to as a hog pen, hog parlor, pigpen, pig parlor, or pig-cote. Pigsties are generally fenced areas of bare dirt and/or mud. "Sty" and "pigsty" and "pigpen" are used as derogatory descriptions of dirty messy areas. There are three contributing reasons that pigs, generally clean animals, create such a living environment:
