---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lease
offline_file: ""
offline_thumbnail: ""
uuid: 34563947-9758-4954-b725-caa8b28f318e
updated: 1484310484
title: lease
categories:
    - Dictionary
---
lease
     n 1: property that is leased or rented out or let [syn: {rental},
           {letting}]
     2: a contract granting use or occupation of property during a
        specified time for a specified payment
     3: the period of time during which a contract conveying
        property to a person is in effect [syn: {term of a
        contract}]
     v 1: let for money; "We rented our apartment to friends while we
          were abroad" [syn: {rent}]
     2: hold under a lease or rental agreement; of goods and
        services [syn: {rent}, {hire}, {charter}]
     3: grant use or occupation of under a term of contract; "I am
        leasing my country estate to some foreigners" [syn: ...
