---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/safely
offline_file: ""
offline_thumbnail: ""
uuid: 2e77b6ed-4490-4484-90dc-b6a56df31b6c
updated: 1484310180
title: safely
categories:
    - Dictionary
---
safely
     adv : with safety; in a safe manner; "we are safely out of there"
