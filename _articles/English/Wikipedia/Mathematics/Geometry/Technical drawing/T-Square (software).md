---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-Square_(software)
offline_file: ""
offline_thumbnail: ""
uuid: e244cd76-4959-4622-931c-1d431fd23e5f
updated: 1484309334
title: T-Square (software)
tags:
    - Authors
    - CAD
    - Input device
    - Influence
    - Notes
categories:
    - Technical drawing
---
T-Square is an early drafting program written by Peter Samson assisted by Alan Kotok and possibly Robert A. Saunders while they were students at the Massachusetts Institute of Technology and members of the Tech Model Railroad Club.
