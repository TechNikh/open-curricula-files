---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doubt
offline_file: ""
offline_thumbnail: ""
uuid: 12f5ea6c-f3b9-40f3-92f4-67e45d850bd2
updated: 1484096560
title: doubt
categories:
    - Dictionary
---
doubt
     n 1: the state of being unsure of something [syn: {uncertainty},
          {incertitude}, {dubiety}, {doubtfulness}, {dubiousness}]
          [ant: {certainty}]
     2: uncertainty about the truth or factuality of existence of
        something; "the dubiousness of his claim"; "there is no
        question about the validity of the enterprise" [syn: {dubiousness},
         {doubtfulness}, {question}]
     v 1: consider unlikely or have doubts about; "I doubt that she
          will accept his proposal of marriage"
     2: lack confidence in or have doubts about; "I doubt these
        reports"; "I suspect her true motives"; "she distrusts her
        stepmother"
