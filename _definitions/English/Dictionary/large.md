---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/large
offline_file: ""
offline_thumbnail: ""
uuid: b811ae6e-d7d7-4d55-8a56-fcf20d035fce
updated: 1484310339
title: large
categories:
    - Dictionary
---
large
     adj 1: above average in size or number or quantity or magnitude or
            extent; "a large city"; "set out for the big city"; "a
            large sum"; "a big (or large) barn"; "a large family";
            "big businesses"; "a big expenditure"; "a large number
            of newspapers"; "a big group of scientists"; "large
            areas of the world" [syn: {big}] [ant: {small}, {small}]
     2: fairly large or important in effect; influential; "played a
        large role in the negotiations"
     3: large enough to be visible to the naked eye [syn: {macroscopic},
         {macroscopical}] [ant: {microscopic}]
     4: ostentatiously lofty in style; "a man given to ...
