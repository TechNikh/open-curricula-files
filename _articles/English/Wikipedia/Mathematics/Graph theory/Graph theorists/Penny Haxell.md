---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Penny_Haxell
offline_file: ""
offline_thumbnail: ""
uuid: 79d0115e-e13a-44a0-91ca-cbb8e54913c5
updated: 1484309466
title: Penny Haxell
categories:
    - Graph theorists
---
Penelope Evelyn (Penny) Haxell is a Canadian mathematician who works as a professor in the department of combinatorics and optimization at the University of Waterloo. Her research interests include extremal combinatorics and graph theory.[1]
