---
version: 1
type: video
provider: YouTube
id: VnwCiiapUQI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2d8f4396-7de7-4ab5-b5c6-a26ec8a824a6
updated: 1486069618
title: Adding 5 + 3 + 6
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/VnwCiiapUQI/default.jpg
    - https://i3.ytimg.com/vi/VnwCiiapUQI/1.jpg
    - https://i3.ytimg.com/vi/VnwCiiapUQI/2.jpg
    - https://i3.ytimg.com/vi/VnwCiiapUQI/3.jpg
---
