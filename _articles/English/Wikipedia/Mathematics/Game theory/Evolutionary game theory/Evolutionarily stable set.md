---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolutionarily_stable_set
offline_file: ""
offline_thumbnail: ""
uuid: 15c35c58-4848-4781-a5d0-1fb0fcbc6c7d
updated: 1484308524
title: Evolutionarily stable set
categories:
    - Evolutionary game theory
---
In game theory an evolutionarily stable set (ES set), sometimes referred to as evolutionary stable sets, is a set of strategies, which score equally against each other, each of which would be an evolutionarily stable strategy (ESS) were it not for the other members of the set. ES sets were first defined by Thomas (1985ab).[1]
