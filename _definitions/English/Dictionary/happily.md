---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/happily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484589061
title: happily
categories:
    - Dictionary
---
happily
     adv 1: in a joyous manner; "they shouted happily" [syn: {merrily},
            {mirthfully}, {gayly}, {blithely}, {jubilantly}, {with
            happiness}] [ant: {unhappily}]
     2: in an unexpectedly lucky way; "happily he was not injured"
        [ant: {sadly}]
