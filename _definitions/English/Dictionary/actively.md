---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/actively
offline_file: ""
offline_thumbnail: ""
uuid: b99b1470-76c9-432a-aba5-74416b67c5c2
updated: 1484310583
title: actively
categories:
    - Dictionary
---
actively
     adv : in an active manner; "he participated actively in the war"
           [ant: {passively}]
