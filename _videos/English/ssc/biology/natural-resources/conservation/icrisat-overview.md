---
title: ICRISAT (Overview)
youtube_id: DCl5i9s7HZ4
tags:
    - ICRISAT
    - International Crops Research Institute for the Semi-Arid Tropics
    - Agriculture
    - Food
    - Nutrition
categories:
    - Conservation
---
