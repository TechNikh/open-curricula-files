---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anthony_Hilton
offline_file: ""
offline_thumbnail: ""
uuid: 64da54f8-219b-4784-96fb-022a03011da9
updated: 1484309463
title: Anthony Hilton
categories:
    - Graph theorists
---
Anthony J. W. Hilton (born 4 April 1941) is a British mathematician specializing in combinatorics and graph theory. His current positions are as emeritus professor of Combinatorial Mathematics at the University of Reading and Professorial Research Fellow at Queen Mary College, University of London.
