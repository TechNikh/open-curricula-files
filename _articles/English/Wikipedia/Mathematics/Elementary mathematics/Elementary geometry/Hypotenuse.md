---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypotenuse
offline_file: ""
offline_thumbnail: ""
uuid: 65d706c3-dec8-4d8d-bb18-6f6ac4e52f4a
updated: 1484308938
title: Hypotenuse
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Hypotenuse.svg.png
tags:
    - Etymology
    - Calculating the hypotenuse
    - Properties
    - Trigonometric ratios
    - Notes
categories:
    - Elementary geometry
---
In geometry, a hypotenuse (rarely: hypothenuse[1]) is the longest side of a right-angled triangle, the side opposite of the right angle. The length of the hypotenuse of a right triangle can be found using the Pythagorean theorem, which states that the square of the length of the hypotenuse equals the sum of the squares of the lengths of the other two sides. For example, if one of the other sides has a length of 3 (when squared, 9) and the other has a length of 4 (when squared, 16), then their squares add up to 25. The length of the hypotenuse is the square root of 25, that is, 5.
