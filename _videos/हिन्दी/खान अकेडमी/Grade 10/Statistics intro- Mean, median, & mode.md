---
version: 1
type: video
provider: YouTube
id: xVvnHusUjlM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 76f80d7c-73c3-4c0f-86a5-6465bccbb763
updated: 1486069618
title: 'Statistics intro- Mean, median, & mode'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/xVvnHusUjlM/default.jpg
    - https://i3.ytimg.com/vi/xVvnHusUjlM/1.jpg
    - https://i3.ytimg.com/vi/xVvnHusUjlM/2.jpg
    - https://i3.ytimg.com/vi/xVvnHusUjlM/3.jpg
---
