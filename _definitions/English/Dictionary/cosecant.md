---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cosecant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484372101
title: cosecant
categories:
    - Dictionary
---
cosecant
     n : ratio of the hypotenuse to the opposite side of a
         right-angled triangle [syn: {cosec}]
