---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bruce_Bueno_de_Mesquita
offline_file: ""
offline_thumbnail: ""
uuid: 743a9160-dd4a-4520-ab4f-89ae1b12d9af
updated: 1484308517
title: Bruce Bueno de Mesquita
tags:
    - Biography
    - Publications
categories:
    - Game theorists
---
Bruce Bueno de Mesquita (born November 24, 1946) is a political scientist, professor at New York University, and senior fellow at Stanford University's Hoover Institution.
