---
version: 1
type: video
provider: YouTube
id: 4Flj9plmKGQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%208%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fd0e20df-7ae5-4f6d-9e08-1fd46dcc082f
updated: 1486069689
title: Solid of Revolution (part 8)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/4Flj9plmKGQ/default.jpg
    - https://i3.ytimg.com/vi/4Flj9plmKGQ/1.jpg
    - https://i3.ytimg.com/vi/4Flj9plmKGQ/2.jpg
    - https://i3.ytimg.com/vi/4Flj9plmKGQ/3.jpg
---
