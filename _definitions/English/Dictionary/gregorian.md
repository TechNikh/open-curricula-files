---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gregorian
offline_file: ""
offline_thumbnail: ""
uuid: 03d9a21d-6396-4ab5-b567-9ae989cf6511
updated: 1484310559
title: gregorian
categories:
    - Dictionary
---
Gregorian
     adj 1: of or relating to Pope Gregory I or to the plainsong chants
            of the Roman Catholic Church
     2: of or relating to Pope Gregory XIII or the calendar he
        introduced in 1582
