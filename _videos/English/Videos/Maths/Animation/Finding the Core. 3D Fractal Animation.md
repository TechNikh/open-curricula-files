---
version: 1
type: video
provider: YouTube
id: N4M2q478qYk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fe3eb326-94af-4cd8-b905-367fbdc01bec
updated: 1486069649
title: Finding the Core. 3D Fractal Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/N4M2q478qYk/default.jpg
    - https://i3.ytimg.com/vi/N4M2q478qYk/1.jpg
    - https://i3.ytimg.com/vi/N4M2q478qYk/2.jpg
    - https://i3.ytimg.com/vi/N4M2q478qYk/3.jpg
---
