---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homologous
offline_file: ""
offline_thumbnail: ""
uuid: 83e66f16-5568-4342-a00c-9cfc8db209e4
updated: 1484310289
title: homologous
categories:
    - Dictionary
---
homologous
     adj 1: having the same evolutionary origin but serving different
            functions; "the wing of a bat and the arm of a man are
            homologous" [ant: {analogous}, {heterologous}]
     2: corresponding or similar in position or structure or
        function or characteristics; especially derived from an
        organism of the same species; "a homologous tissue graft"
        [ant: {heterologous}, {autologous}]
