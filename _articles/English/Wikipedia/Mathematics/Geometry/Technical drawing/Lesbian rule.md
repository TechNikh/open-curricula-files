---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lesbian_rule
offline_file: ""
offline_thumbnail: ""
uuid: 726220ac-0891-454c-9fa2-65853a39b3d6
updated: 1484309327
title: Lesbian rule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Krzywik.jpg
categories:
    - Technical drawing
---
A lesbian rule was historically a flexible mason's rule made of lead that could be bent to the curves of a molding, and used to measure or reproduce irregular curves.[1][2][3] Lesbian rules were originally constructed of a pliable kind of lead found on the island of Lesbos.[4]
