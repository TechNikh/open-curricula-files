---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imitate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496181
title: imitate
categories:
    - Dictionary
---
imitate
     v 1: reproduce someone's behavior or looks; "The mime imitated
          the passers-by"; "Children often copy their parents or
          older siblings" [syn: {copy}, {simulate}]
     2: appear like, as in behavior or appearance; "Life imitate
        art"
     3: make a reproduction or copy of
