---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-autonomous_mechanics
offline_file: ""
offline_thumbnail: ""
uuid: 7ec3c663-bf7b-4426-a246-8179cdbe2790
updated: 1484308797
title: Non-autonomous mechanics
categories:
    - Hamiltonian mechanics
---
Non-autonomous mechanics describe non-relativistic mechanical systems subject to time-dependent transformations. In particular, this is the case of mechanical systems whose Lagrangians and Hamiltonians depend on the time. The configuration space of non-autonomous mechanics is a fiber bundle 
  
    
      
        Q
        →
        
          R
        
      
    
    {\displaystyle Q\to \mathbb {R} }
  
 over the time axis 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
  
 coordinated by 
  
    
      
        (
        t
        ,
        
          q
          
            i
          
        
        )
      
    
    {\displaystyle ...
