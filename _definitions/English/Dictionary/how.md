---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/how
offline_file: ""
offline_thumbnail: ""
uuid: 4191352f-76f2-4082-9334-89eb88c78e1c
updated: 1484310359
title: how
categories:
    - Dictionary
---
how
     adv 1: to what extent or amount or degree; "how tall is she?"
     2: in what way or manner or by what means (`however' is
        sometimes used as an intensive form of `how'); "how did
        you catch the snake?"; "he told us how he did it";
        "however did you get here so soon?" [syn: {however}]
