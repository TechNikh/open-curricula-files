---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polarisation
offline_file: ""
offline_thumbnail: ""
uuid: 44fba00b-c194-4e2f-b79a-ceff8528101c
updated: 1484310189
title: polarisation
categories:
    - Dictionary
---
polarisation
     n 1: the condition of having or giving polarity [syn: {polarization}]
     2: the phenomenon in which waves of light or other radiation
        are restricted in direction of vibration [syn: {polarization}]
