---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parity_game
offline_file: ""
offline_thumbnail: ""
uuid: 5b12b032-6f7c-4739-8f1b-1b344adabd7f
updated: 1484309040
title: Parity game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Example_Parity_Game_Solved.png
tags:
    - Solving a game
    - Recursive algorithm for solving parity games
    - Related games and their decision problems
    - Relation with logic and automata theory
categories:
    - Game theory game classes
---
A parity game is played on a colored directed graph, where each node has been colored by a priority – one of (usually) finitely many natural numbers. Two players, 0 and 1, move a (single, shared) token along the edges of the graph. The owner of the node that the token falls on selects the successor node, resulting in a (possibly infinite) path, called the play.
