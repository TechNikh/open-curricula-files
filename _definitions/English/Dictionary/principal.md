---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/principal
offline_file: ""
offline_thumbnail: ""
uuid: 08900dcd-a58a-4833-bd75-af75d30fb28f
updated: 1484310216
title: principal
categories:
    - Dictionary
---
principal
     adj : most important element; "the chief aim of living"; "the main
           doors were of solid glass"; "the principal rivers of
           America"; "the principal example"; "policemen were
           primary targets" [syn: {chief(a)}, {main(a)}, {primary(a)},
            {principal(a)}]
     n 1: the original amount of a debt on which interest is
          calculated
     2: the educator who has executive authority for a school; "she
        sent unruly pupils to see the principal" [syn: {school
        principal}, {head teacher}, {head}]
     3: an actor who plays a principal role [syn: {star}, {lead}]
     4: capital as contrasted with the income derived from it [syn:
  ...
