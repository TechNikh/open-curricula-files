---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Odious_number
offline_file: ""
offline_thumbnail: ""
uuid: d3d4e42c-650d-44b9-9bd9-cf8af16596d3
updated: 1484308649
title: Odious number
categories:
    - Number theory
---
The first odious numbers are: 1, 2, 4, 7, 8, 11, 13, 14, 16, 19, 21, 22, 25, 26, 28, 31, 32, 35, 37, 38 ... [1]
