---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/master
offline_file: ""
offline_thumbnail: ""
uuid: 3324d7ab-deb6-46aa-9477-8c034f404cae
updated: 1484310431
title: master
categories:
    - Dictionary
---
master
     n 1: an artist of consummate skill; "a master of the violin";
          "one of the old masters" [syn: {maestro}]
     2: a person who has general authority over others [syn: {overlord},
         {lord}]
     3: a combatant who is able to defeat rivals [syn: {victor}, {superior}]
     4: directs the work of other
     5: presiding officer of a school [syn: {headmaster}, {schoolmaster}]
     6: an original creation (i.e., an audio recording) from which
        copies can be made [syn: {master copy}, {original}]
     7: an officer who is licensed to command a merchant ship [syn:
        {captain}, {sea captain}, {skipper}]
     8: someone who holds a master's degree from academic
 ...
