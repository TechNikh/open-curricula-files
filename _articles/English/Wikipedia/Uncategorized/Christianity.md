---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Christianity
offline_file: ""
offline_thumbnail: ""
uuid: cfae900d-8a5b-43e6-9039-0318691aff3b
updated: 1484308482
title: Christianity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nicaea_icon.jpg
tags:
    - Beliefs
    - Creeds
    - Jesus
    - Death and resurrection
    - Salvation
    - Trinity
    - Trinitarians
    - Nontrinitarianism
    - Scriptures
    - Catholic interpretation
categories:
    - Uncategorized
---
Christianity[note 1] is a monotheistic[1] religion based on the life and teachings of Jesus Christ, who serves as the focal point for the religion. It is the world's largest religion,[2][3] with over 2.4 billion followers,[4][5][6] or 33% of the global population, known as Christians.[note 2] Christians believe that Jesus is the Son of God and the savior of humanity whose coming as the Messiah (the Christ) was prophesied in the Old Testament.[7]
