---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theta_graph
offline_file: ""
offline_thumbnail: ""
uuid: 90c66619-589f-427a-b70c-31f8f3fba818
updated: 1484309179
title: Theta graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Theta-cone.svg.png
tags:
    - Construction
    - Properties
categories:
    - Computational geometry
---
In computational geometry, the Theta graph, or 
  
    
      
        Θ
      
    
    {\displaystyle \Theta }
  
-graph, is a type of geometric spanner similar to a Yao graph. The basic method of construction involves partitioning the space around each vertex into a set of cones, which themselves partition the remaining vertices of the graph. Like Yao Graphs, a 
  
    
      
        Θ
      
    
    {\displaystyle \Theta }
  
-graph contains at most one edge per cone; where they differ is how that edge is selected. Whereas Yao Graphs will select the nearest vertex according to the metric space of the graph, the 
  
    
      
        Θ
      
    
    {\displaystyle \Theta }
  ...
