---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substance
offline_file: ""
offline_thumbnail: ""
uuid: 1e5542f0-67e5-4c5c-a2c5-7f3cf777261e
updated: 1484310346
title: substance
categories:
    - Dictionary
---
substance
     n 1: that which has mass and occupies space; "an atom is the
          smallest indivisible unit of matter" [syn: {matter}]
     2: the stuff of which an object consists
     3: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {core}, {center}, {essence},
         {gist}, {heart}, {heart and soul}, {inwardness}, {marrow},
         {meat}, {nub}, {pith}, {sum}, {nitty-gritty}]
     4: the idea that is intended; "What is the meaning of this
        proverb?" [syn: {meaning}]
     5: considerable ...
