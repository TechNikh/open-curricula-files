---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispersion
offline_file: ""
offline_thumbnail: ""
uuid: 0ca0d597-4ed9-409d-94ab-1595ce28952f
updated: 1484310414
title: dispersion
categories:
    - Dictionary
---
dispersion
     n 1: spreading widely or driving off [syn: {scattering}]
     2: the spatial property of being scattered about over an area
        or volume [syn: {distribution}] [ant: {concentration}]
     3: the act of dispersing or diffusing something; "the
        dispersion of the troops"; "the diffusion of knowledge"
        [syn: {dispersal}, {dissemination}, {diffusion}]
