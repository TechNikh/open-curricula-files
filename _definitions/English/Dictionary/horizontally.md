---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horizontally
offline_file: ""
offline_thumbnail: ""
uuid: 51fe7adc-ef81-4fb0-ae44-90c45e29664b
updated: 1484310204
title: horizontally
categories:
    - Dictionary
---
horizontally
     adv : in a horizontal direction; "a gallery quite often is added
           to make use of space vertically as well as
           horizontally"
