---
version: 1
type: video
provider: YouTube
id: SbpoyXTpC84
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Example-%209%20card%20hands.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dbc17a97-0656-483d-abb6-ec58bcbc9b1e
updated: 1486069557
title: 'Example- 9 card hands'
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/SbpoyXTpC84/default.jpg
    - https://i3.ytimg.com/vi/SbpoyXTpC84/1.jpg
    - https://i3.ytimg.com/vi/SbpoyXTpC84/2.jpg
    - https://i3.ytimg.com/vi/SbpoyXTpC84/3.jpg
---
