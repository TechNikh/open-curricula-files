---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sit
offline_file: ""
offline_thumbnail: ""
uuid: 26813377-b87d-4538-8a12-bcad8e9576c9
updated: 1484310210
title: sit
categories:
    - Dictionary
---
sit
     v 1: be seated [syn: {sit down}] [ant: {stand}, {lie}]
     2: sit around, often unused; "The object sat in the corner"
     3: take a seat [syn: {sit down}] [ant: {arise}]
     4: be in session; "When does the court of law sit?"
     5: assume a posture as for artistic purposes; "We don't know
        the woman who posed for Leonardo so often" [syn: {model},
        {pose}, {posture}]
     6: sit and travel on the back of animal, usually while
        controlling its motions; "She never sat a horse!"; "Did
        you ever ride a camel?"; "The girl liked to drive the
        young mare" [syn: {ride}]
     7: work or act as a baby-sitter; "I cannot baby-sit tonight; I
        have ...
