---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plethystic_substitution
offline_file: ""
offline_thumbnail: ""
uuid: c4996d68-cd53-4a63-8b12-ef79fc4679aa
updated: 1484308577
title: Plethystic substitution
tags:
    - Definition
    - Examples
categories:
    - Combinatorics
---
Plethystic substitution is a shorthand notation for a common kind of substitution in the algebra of symmetric functions and that of symmetric polynomials. It is essentially basic substitution of variables, but allows for a change in the number of variables used.
