---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obstinate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561701
title: obstinate
categories:
    - Dictionary
---
obstinate
     adj 1: stubbornly persistent in wrongdoing [syn: {cussed}, {obdurate},
             {unrepentant}]
     2: resistant to guidance or discipline; "Mary Mary quite
        contrary"; "an obstinate child with a violent temper"; "a
        perverse mood"; "wayward behavior" [syn: {contrary}, {perverse},
         {wayward}]
     3: persisting in a reactionary stand [syn: {stubborn}, {unregenerate}]
     v : persist stubbornly; "he obstinates himself against all
         rational arguments"
