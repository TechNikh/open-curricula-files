---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/piercing
offline_file: ""
offline_thumbnail: ""
uuid: 728427ae-a579-4fb3-8869-a1936a0fff1e
updated: 1484310401
title: piercing
categories:
    - Dictionary
---
piercing
     adj 1: having or demonstrating ability to recognize or draw fine
            distinctions; "an acute observer of politics and
            politicians"; "incisive comments"; "icy knifelike
            reasoning"; "as sharp and incisive as the stroke of a
            fang"; "penetrating insight"; "frequent penetrative
            observations" [syn: {acute}, {discriminating}, {incisive},
             {keen}, {knifelike}, {penetrating}, {penetrative}, {sharp}]
     2: high-pitched and sharp; "piercing screams"; "a shrill
        whistle" [syn: {shrill}, {sharp}]
     3: as physically painful as if caused by a sharp instrument; "a
        cutting wind"; "keen winds"; "knifelike ...
