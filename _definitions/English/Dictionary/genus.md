---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genus
offline_file: ""
offline_thumbnail: ""
uuid: 0529efa7-fe54-42b7-a7fc-a9b83bc0b102
updated: 1484310397
title: genus
categories:
    - Dictionary
---
genus
     n 1: a general kind of something; "ignore the genus communism"
     2: (biology) taxonomic group containing one or more species
     [also: {genera} (pl)]
