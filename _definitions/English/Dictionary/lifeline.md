---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lifeline
offline_file: ""
offline_thumbnail: ""
uuid: 09cb83e1-2ae7-456b-abf4-a8ef8decf2b7
updated: 1484310254
title: lifeline
categories:
    - Dictionary
---
life line
     n : a crease on the palm; its length is said by palmists to
         indicate how long you will live [syn: {line of life}, {lifeline}]
