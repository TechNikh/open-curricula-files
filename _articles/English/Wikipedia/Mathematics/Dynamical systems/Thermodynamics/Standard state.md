---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Standard_state
offline_file: ""
offline_thumbnail: ""
uuid: ef2c01c5-b765-4ce7-8d6a-a36edbab5569
updated: 1484308864
title: Standard state
tags:
    - Conventional standard states
    - Gases
    - Liquids and solids
    - Solutes
    - Typesetting
categories:
    - Thermodynamics
---
In chemistry, the standard state of a material (pure substance, mixture or solution) is a reference point used to calculate its properties under different conditions. In principle, the choice of standard state is arbitrary, although the International Union of Pure and Applied Chemistry (IUPAC) recommends a conventional set of standard states for general use.[1] IUPAC recommends using a standard pressure po = 105 Pa.[2] Strictly speaking, temperature is not part of the definition of a standard state. For example, as discussed below, the standard state of a gas is conventionally chosen to be unit pressure (usually in bar) ideal gas, regardless of the temperature. However, most tables of ...
