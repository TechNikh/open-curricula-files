---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extensive-form_game
offline_file: ""
offline_thumbnail: ""
uuid: 9c4d2c23-6b04-4fb0-8567-a941a67ce847
updated: 1484309035
title: Extensive-form game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Extensive-form_tree.svg.png
tags:
    - Finite extensive-form games
    - Perfect and complete information
    - Imperfect information
    - Incomplete information
    - Formal definition
    - Infinite action space
categories:
    - Game theory game classes
---
An extensive-form game is a specification of a game in game theory, allowing (as the name suggests) explicit representation of a number of important aspects, like the sequencing of players' possible moves, their choices at every decision point, the (possibly imperfect) information each player has about the other player's moves when he makes a decision, and his payoffs for all possible game outcomes. Extensive-form games also allow representation of incomplete information in the form of chance events encoded as "moves by nature".
