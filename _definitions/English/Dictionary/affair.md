---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affair
offline_file: ""
offline_thumbnail: ""
uuid: 90899884-b901-42b6-906e-d6a8c43add43
updated: 1484310464
title: affair
categories:
    - Dictionary
---
affair
     n 1: a vaguely specified concern; "several matters to attend to";
          "it is none of your affair"; "things are going well"
          [syn: {matter}, {thing}]
     2: a usually secretive or illicit sexual relationship [syn: {affaire},
         {intimacy}, {liaison}, {involvement}, {amour}]
     3: a vaguely specified social event; "the party was quite an
        affair"; "an occasion arranged to honor the president"; "a
        seemingly endless round of social functions" [syn: {occasion},
         {social occasion}, {function}, {social function}]
