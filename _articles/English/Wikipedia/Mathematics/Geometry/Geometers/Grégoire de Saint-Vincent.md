---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gr%C3%A9goire_de_Saint-Vincent'
offline_file: ""
offline_thumbnail: ""
uuid: 80fee912-026f-4e79-aa92-e73367b3e3a9
updated: 1484309100
title: Grégoire de Saint-Vincent
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Gr%25C3%25A9goire_de_Saint-Vincent_%25281584-1667%2529.jpg'
tags:
    - Ductus plani in planum
    - Quadrature of the hyperbola
categories:
    - Geometers
---
Grégoire de Saint-Vincent (22 March 1584 Bruges – 5 June 1667 Ghent) was a Flemish Jesuit and mathematician. He is remembered for his work on quadrature of the hyperbola.
