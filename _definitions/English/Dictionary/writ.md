---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/writ
offline_file: ""
offline_thumbnail: ""
uuid: c581faa8-6428-47ab-be84-f12a43a1422c
updated: 1484310395
title: writ
categories:
    - Dictionary
---
writ
     n : (law) a legal document issued by a court or judicial officer
         [syn: {judicial writ}]
