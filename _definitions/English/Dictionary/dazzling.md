---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dazzling
offline_file: ""
offline_thumbnail: ""
uuid: 575b1648-b660-4409-89d4-b2c42dfe446b
updated: 1484310373
title: dazzling
categories:
    - Dictionary
---
dazzling
     adj 1: amazingly impressive; suggestive of the flashing of
            lightning; "the skater's dazzling virtuosic leaps";
            "these great best canvases still look as astonishing
            and as invitingly new as they did...when...his
            fulgurant popularity was in full growth"- Janet
            Flanner; "adventures related...in a style both vivid
            and fulgurous"- Idwal Jones [syn: {eye-popping}, {fulgurant},
             {fulgurous}]
     2: shining intensely; "the blazing sun"; "blinding headlights";
        "dazzling snow"; "fulgent patterns of sunlight"; "the
        glaring sun" [syn: {blazing}, {blinding}, {fulgent}, {glaring},
         ...
