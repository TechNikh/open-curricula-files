---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 6a35765f-72be-4f71-bb12-04387fa8e5ed
updated: 1484308868
title: Thermal expansion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dehnungsfuge.jpg
tags:
    - Overview
    - Predicting expansion
    - Contraction effects (negative thermal expansion)
    - Factors affecting thermal expansion
    - Coefficient of thermal expansion
    - General volumetric thermal expansion coefficient
    - Expansion in solids
    - Linear expansion
    - Effects on strain
    - Area expansion
    - Volume expansion
    - Isotropic materials
    - Anisotropic materials
    - Isobaric expansion in gases
    - Expansion in liquids
    - Expansion in mixtures and alloys
    - Apparent and absolute expansion
    - Examples and applications
    - Thermal expansion coefficients for various materials
categories:
    - Thermodynamics
---
Temperature is a monotonic function of the average molecular kinetic energy of a substance. When a substance is heated, the kinetic energy of its molecules increases. Thus, the molecules begin moving more and usually maintain a greater average separation. Materials which contract with increasing temperature are unusual; this effect is limited in size, and only occurs within limited temperature ranges (see examples below). The degree of expansion divided by the change in temperature is called the material's coefficient of thermal expansion and generally varies with temperature.
