---
version: 1
type: video
provider: YouTube
id: TeiuG81mbII
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/What%20is%20a%20sentence.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 89c2d125-8a55-45d2-b1e0-8575e28e898e
updated: 1486069601
title: What is a sentence?
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/TeiuG81mbII/default.jpg
    - https://i3.ytimg.com/vi/TeiuG81mbII/1.jpg
    - https://i3.ytimg.com/vi/TeiuG81mbII/2.jpg
    - https://i3.ytimg.com/vi/TeiuG81mbII/3.jpg
---
A sentence is a grammatically complete idea.  All sentences have a noun or pronoun component called the subject, and a verb part called the predicate.  David and Paige explore this division across several different example sentences.

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/three-types-of-sentence-syntax-khan-academy

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
