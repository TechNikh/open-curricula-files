---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marcel_Berger
offline_file: ""
offline_thumbnail: ""
uuid: 30733762-f0c3-465b-a8aa-61ab8d21a9e1
updated: 1484309077
title: Marcel Berger
tags:
    - Awards and honors
    - Selected publications
categories:
    - Geometers
---
Marcel Berger (14 April 1927 – 15 October 2016) was a French mathematician, doyen of French differential geometry, and a former director of the Institut des Hautes Études Scientifiques (IHÉS), France. Formerly residing in Le Castera in Lasseube, Berger was instrumental in Mikhail Gromov's accepting positions both at the University of Paris and at the IHÉS.[1]
