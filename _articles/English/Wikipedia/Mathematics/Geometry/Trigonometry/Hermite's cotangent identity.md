---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hermite%27s_cotangent_identity'
offline_file: ""
offline_thumbnail: ""
uuid: 6d585602-b962-431b-8ec4-fc8d40851d14
updated: 1484309343
title: "Hermite's cotangent identity"
categories:
    - Trigonometry
---
In mathematics, Hermite's cotangent identity is a trigonometric identity discovered by Charles Hermite.[1] Suppose a1, ..., an are complex numbers, no two of which differ by an integer multiple of π. Let
