---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circumference
offline_file: ""
offline_thumbnail: ""
uuid: d46d042a-3f1d-45f6-bc7c-515890973a68
updated: 1484310216
title: circumference
categories:
    - Dictionary
---
circumference
     n 1: the size of something as given by the distance around it
          [syn: {perimeter}]
     2: the length of the closed curve of a circle
