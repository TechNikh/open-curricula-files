---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_cycle
offline_file: ""
offline_thumbnail: ""
uuid: dde4d3ac-373f-45f9-a3a5-b8f786d3c506
updated: 1484309130
title: Algebraic cycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_5.png
tags:
    - Definition
    - Flat pullback and proper pushforward
categories:
    - Algebraic geometry
---
In mathematics, an algebraic cycle on an algebraic variety V is, roughly speaking, a homology class on V that is represented by a linear combination of subvarieties of V. Therefore, the algebraic cycles on V are the part of the algebraic topology of V that is directly accessible in algebraic geometry. With the formulation of some fundamental conjectures in the 1950s and 1960s, the study of algebraic cycles became one of the main objectives of the algebraic geometry of general varieties.
