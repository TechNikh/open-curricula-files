---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tons
offline_file: ""
offline_thumbnail: ""
uuid: 037ebba0-ca80-4ec0-9b86-583a04692675
updated: 1484310479
title: tons
categories:
    - Dictionary
---
tons
     n : a large number or amount; "made lots of new friends"; "she
         amassed a mountain of newspapers" [syn: {dozens}, {heaps},
          {lots}, {mountain}, {piles}, {scores}, {stacks}, {loads},
          {rafts}, {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
