---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judging
offline_file: ""
offline_thumbnail: ""
uuid: 462eb11b-946e-4f6c-8dca-768b9ab603a1
updated: 1484310221
title: judging
categories:
    - Dictionary
---
judging
     n : the cognitive process of reaching a decision or drawing
         conclusions [syn: {judgment}, {judgement}]
