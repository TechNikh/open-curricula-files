---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kraft%E2%80%93McMillan_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 156ab7be-c813-40db-b318-46ae286204f4
updated: 1484308553
title: Kraft–McMillan inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AVLtreef.svg.png
tags:
    - Applications and intuitions
    - Formal statement
    - 'Example: binary trees'
    - Proof
    - Proof for prefix codes
    - Proof of the general case
    - Alternative construction for the converse
    - Notes
categories:
    - Coding theory
---
In coding theory, the Kraft–McMillan inequality gives a necessary and sufficient condition for the existence of a prefix code[1] (in Kraft's version) or a uniquely decodable code (in McMillan's version) for a given set of codeword lengths. Its applications to prefix codes and trees often find use in computer science and information theory.
