---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unexpected
offline_file: ""
offline_thumbnail: ""
uuid: c968f513-cedc-4b7a-8991-42926066ecd5
updated: 1484310545
title: unexpected
categories:
    - Dictionary
---
unexpected
     adj 1: not expected or anticipated; "unexpected guests";
            "unexpected news" [ant: {expected}]
     2: not foreseen; "unsuspected difficulties arose"; "unsuspected
        turnings in the road"
     3: made necessary by an unexpected situation or emergency; "a
        forced landing" [syn: {forced}]
     4: causing surprise or amazement by not being expected; "the
        curtains opened to reveal a completely unexpected scene"
     5: not planned; "an unexpected pregnancy"
     6: happening or coming quickly and without warning; "a sudden
        unexpected development" [syn: {unforeseen}]
