---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mad
offline_file: ""
offline_thumbnail: ""
uuid: 6ab6ed48-6c1d-4e91-8a33-4b19d4f61a97
updated: 1484310591
title: mad
categories:
    - Dictionary
---
mad
     adj 1: roused to anger; "stayed huffy a good while"- Mark Twain;
            "she gets mad when you wake her up so early"; "mad at
            his friend"; "sore over a remark" [syn: {huffy}, {sore}]
     2: affected with madness or insanity; "a man who had gone mad"
        [syn: {brainsick}, {crazy}, {demented}, {distracted}, {disturbed},
         {sick}, {unbalanced}, {unhinged}]
     3: marked by uncontrolled excitement or emotion; "a crowd of
        delirious baseball fans"; "something frantic in their
        gaiety"; "a mad whirl of pleasure" [syn: {delirious}, {excited},
         {frantic}, {unrestrained}]
     4: very foolish; "harebrained ideas"; "took insane risks ...
