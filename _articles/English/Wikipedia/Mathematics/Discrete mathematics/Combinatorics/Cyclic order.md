---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_order
offline_file: ""
offline_thumbnail: ""
uuid: 2d6672a0-c121-4ec4-a13c-5fe1dc67da93
updated: 1484308567
title: Cyclic order
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Orientovan%25C3%25A1_kru%25C5%25BEnice.svg.png'
tags:
    - Finite cycles
    - Definitions
    - The ternary relation
    - Rolling and cuts
    - Intervals
    - Automorphisms
    - Monotone functions
    - Functions on finite sets
    - Completion
    - Further constructions
    - Unrolling and covers
    - Products and retracts
    - Topology
    - Related structures
    - Groups
    - Modified axioms
    - Symmetries and model theory
    - Cognition
    - Notes on usage
categories:
    - Combinatorics
---
In mathematics, a cyclic order is a way to arrange a set of objects in a circle.[nb] Unlike most structures in order theory, a cyclic order is not modeled as a binary relation, such as "a < b". One does not say that east is "more clockwise" than west. Instead, a cyclic order is defined as a ternary relation [a, b, c], meaning "after a, one reaches b before c". For example, [June, October, February]. A ternary relation is called a cyclic order if it is cyclic, asymmetric, transitive, and total. Dropping the "total" requirement results in a partial cyclic order.
