---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Comparison_triangle
offline_file: ""
offline_thumbnail: ""
uuid: f2384661-59f7-494a-8eb8-33ed7a4ac13a
updated: 1484309262
title: Comparison triangle
categories:
    - Metric geometry
---
Define 
  
    
      
        
          M
          
            k
          
          
            2
          
        
      
    
    {\displaystyle M_{k}^{2}}
  
 as the 2-dimensional metric space of constant curvature 
  
    
      
        k
      
    
    {\displaystyle k}
  
. So, for example, 
  
    
      
        
          M
          
            0
          
          
            2
          
        
      
    
    {\displaystyle M_{0}^{2}}
  
 is the Euclidean plane, 
  
    
      
        
          M
          
            1
          
          
            2
          
        
      
    
    {\displaystyle M_{1}^{2}}
  
 is the surface of the unit sphere, and ...
