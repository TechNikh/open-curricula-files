---
version: 1
type: video
provider: YouTube
id: Dx4GuHH4lTI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: edb95daa-32e5-40d2-9358-01928972e97d
updated: 1486069618
title: Using the product rule and the chain rule
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Dx4GuHH4lTI/default.jpg
    - https://i3.ytimg.com/vi/Dx4GuHH4lTI/1.jpg
    - https://i3.ytimg.com/vi/Dx4GuHH4lTI/2.jpg
    - https://i3.ytimg.com/vi/Dx4GuHH4lTI/3.jpg
---
