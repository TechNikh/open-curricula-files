---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paris
offline_file: ""
offline_thumbnail: ""
uuid: 3200b87a-3989-4220-b0d0-b4ccfd42c867
updated: 1484310387
title: paris
categories:
    - Dictionary
---
Paris
     n 1: the capital and largest city of France; and international
          center of culture and commerce [syn: {City of Light}, {French
          capital}, {capital of France}]
     2: sometimes placed in subfamily Trilliaceae [syn: {genus Paris}]
     3: a town in northeast Texas
