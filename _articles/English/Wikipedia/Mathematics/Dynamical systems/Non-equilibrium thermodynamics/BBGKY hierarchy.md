---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BBGKY_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 0fdaca52-cf3d-4fc6-b605-dc5d58e0e242
updated: 1484308797
title: BBGKY hierarchy
tags:
    - Formulation
    - Physical interpretation and applications
    - Bibliography
categories:
    - Non-equilibrium thermodynamics
---
In statistical physics, the BBGKY hierarchy (Bogoliubov–Born–Green–Kirkwood–Yvon hierarchy, sometimes called Bogoliubov hierarchy) is a set of equations describing the dynamics of a system of a large number of interacting particles. The equation for an s-particle distribution function (probability density function) in the BBGKY hierarchy includes the (s + 1)-particle distribution function thus forming a coupled chain of equations. This formal theoretic result is named after Bogoliubov, Born, Green, Kirkwood, and Yvon.
