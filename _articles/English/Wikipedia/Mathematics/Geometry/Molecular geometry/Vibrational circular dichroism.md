---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vibrational_circular_dichroism
offline_file: ""
offline_thumbnail: ""
uuid: 4c9eba9b-1b03-42a0-9d0f-23d43be0dd16
updated: 1484309282
title: Vibrational circular dichroism
tags:
    - Vibrational modes
    - Theory of VCD
    - VCD of peptides and proteins
    - VCD of nucleic acids
    - VCD Instrumentation
    - Magnetic VCD
    - Raman optical activity (ROA)
categories:
    - Molecular geometry
---
Vibrational circular dichroism (VCD) is a spectroscopic technique which detects differences in attenuation of left and right circularly polarized light passing through a sample. It is the extension of circular dichroism spectroscopy into the infrared and near infrared ranges.[1]
