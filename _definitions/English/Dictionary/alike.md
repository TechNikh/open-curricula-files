---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alike
offline_file: ""
offline_thumbnail: ""
uuid: 55eafcd0-1fa2-4917-aa5f-b5625e8af40f
updated: 1484310545
title: alike
categories:
    - Dictionary
---
alike
     adj : having the same or similar characteristics; "all politicians
           are alike"; "they looked utterly alike"; "friends are
           generaly alike in background and taste" [syn: {alike(p)},
            {similar}, {like}] [ant: {unalike}]
     adv 1: equally; "parents and teachers alike demanded reforms" [syn:
             {likewise}]
     2: in a like manner; "they walk alike"
