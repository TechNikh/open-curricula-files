---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compilation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516401
title: compilation
categories:
    - Dictionary
---
compilation
     n 1: something that is compiled (as into a single book or file)
          [syn: {digest}]
     2: the act of compiling (as into a single book or file or
        list); "the job of compiling the inventory took several
        hours" [syn: {compiling}]
