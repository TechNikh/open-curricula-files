---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sooner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425981
title: sooner
categories:
    - Dictionary
---
sooner
     adv 1: comparatives of `soon' or `early'; "Come a little sooner, if
            you can"; "came earlier than I expected" [syn: {earlier}]
     2: more readily or willingly; "clean it well, preferably with
        warm water"; "I'd rather be in Philadelphia"; "I'd sooner
        die than give up" [syn: {preferably}, {rather}]
