---
version: 1
type: video
provider: YouTube
id: tWy2QfC9APM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2c30c248-5d14-4917-9f1f-c62181ffcf47
updated: 1486069636
title: 'How does light bend? - Live Experiments (Ep 30) - Head Squeeze'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tWy2QfC9APM/default.jpg
    - https://i3.ytimg.com/vi/tWy2QfC9APM/1.jpg
    - https://i3.ytimg.com/vi/tWy2QfC9APM/2.jpg
    - https://i3.ytimg.com/vi/tWy2QfC9APM/3.jpg
---
