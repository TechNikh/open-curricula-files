---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_selection
offline_file: ""
offline_thumbnail: ""
uuid: 38c39494-3219-4c8e-a72e-883d948bffe6
updated: 1484308377
title: Natural selection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Aristotle_Altemps_Inv8575.jpg
tags:
    - Historical development
    - Pre-Darwinian theories
    - "Darwin's theory"
    - The "modern evolutionary synthesis" of the mid-20th century
    - Terminology
    - Mechanism
    - Heritable variation, differential reproduction
    - Fitness
    - Competition
    - Types of selection
    - Sexual selection
    - Natural selection in action
    - Evolution by means of natural selection
    - Speciation
    - Genetic basis
    - Genotype and phenotype
    - Directionality of selection
    - Selection, genetic variation, and drift
    - Impact
    - Origin of life
    - Cell and molecular biology
    - Social and psychological theory
    - Information and systems theory
    - Notes and references
    - Bibliography
categories:
    - Uncategorized
---
Natural selection is the differential survival and reproduction of individuals due to differences in phenotype. It is a key mechanism of evolution, the change in heritable traits of a population over time. Charles Darwin popularised the term "natural selection", and compared it with artificial selection.
