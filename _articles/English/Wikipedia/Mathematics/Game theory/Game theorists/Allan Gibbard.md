---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Allan_Gibbard
offline_file: ""
offline_thumbnail: ""
uuid: 3d2bceb0-c52c-4dc5-a594-49aa1293fda3
updated: 1484308512
title: Allan Gibbard
tags:
    - Education and career
    - Philosophical work
    - Interviews with Gibbard
    - Notes
categories:
    - Game theorists
---
Allan Gibbard (born 1942) is the Richard B. Brandt Distinguished University Professor of Philosophy Emeritus at the University of Michigan, Ann Arbor.[1] Gibbard has made major contributions to contemporary ethical theory, in particular metaethics, where he has developed a contemporary version of non-cognitivism. He has also published articles in the philosophy of language, metaphysics, and social choice theory.[2]
