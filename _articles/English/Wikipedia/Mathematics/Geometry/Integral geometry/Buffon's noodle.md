---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Buffon%27s_noodle'
offline_file: ""
offline_thumbnail: ""
uuid: 7dd824e7-f107-4b54-9ccc-6eb3bef0b353
updated: 1484309255
title: "Buffon's noodle"
tags:
    - "Buffon's needle"
    - Bending the needle
categories:
    - Integral geometry
---
In geometric probability, the problem of Buffon's noodle is a variation on the well-known problem of Buffon's needle, named after Georges-Louis Leclerc, Comte de Buffon who lived in the 18th century. That problem solved by Buffon was the earliest geometric probability problem to be solved.
