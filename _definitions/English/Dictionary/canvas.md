---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/canvas
offline_file: ""
offline_thumbnail: ""
uuid: f3beb1ae-e91a-4520-b365-122c5bd168c9
updated: 1484310154
title: canvas
categories:
    - Dictionary
---
canvas
     n 1: heavy closely woven fabric (used for clothing or chairs or
          sails or tents) [syn: {canvass}]
     2: an oil painting on canvas [syn: {canvass}]
     3: the setting for a narrative or fictional or dramatic
        account; "the crowded canvas of history"; "the movie
        demanded a dramatic canvas of sound" [syn: {canvass}]
     4: a tent made of canvas [syn: {canvas tent}, {canvass}]
     5: a large piece of fabric (as canvas) by means of which wind
        is used to propel a sailing vessel [syn: {sail}, {canvass},
         {sheet}]
     6: the mat that forms the floor of the ring in which boxers or
        professional wrestlers compete; "the boxer picked ...
