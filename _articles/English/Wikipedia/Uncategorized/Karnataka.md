---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Karnataka
offline_file: ""
offline_thumbnail: ""
uuid: 9635e8e7-65aa-4167-a09a-f1d19d745981
updated: 1484308425
title: Karnataka
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Hoysala_emblem.JPG
tags:
    - History
    - geography
    - Sub-divisions
    - Demographics
    - Government and administration
    - Economy
    - Transport
    - culture
    - religion
    - language
    - Education
    - High literacy districts
    - High literacy taluks
    - Media
    - Sports
    - Flora and Fauna
    - Tourism
    - Notes
categories:
    - Uncategorized
---
Karnataka (Kannada: ಕರ್ನಾಟಕ, IPA:/kərˈnɑːtəkə, kɑːr-/)[10] is a state in south western region of India. It was formed on 1 November 1956, with the passage of the States Reorganisation Act. Originally known as the State of Mysore, it was renamed Karnataka in 1973.[11] The capital and largest city is Bangalore (Bengaluru). Karnataka is bordered by the Arabian Sea and the Laccadive Sea to the west, Goa to the northwest, Maharashtra to the north, Telangana to the northeast, Andhra Pradesh to the east, Tamil Nadu to the southeast, and Kerala to the southwest. The state covers an area of 191,976 square kilometres (74,122 sq mi), or 5.83 percent of the total geographical ...
