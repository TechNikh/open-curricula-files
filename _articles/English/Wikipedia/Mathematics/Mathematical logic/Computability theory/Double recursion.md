---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_recursion
offline_file: ""
offline_thumbnail: ""
uuid: 19a89138-812d-471c-a1c5-4f58bbf87994
updated: 1484309483
title: Double recursion
categories:
    - Computability theory
---
In recursive function theory, double recursion is an extension of primitive recursion which allows the definition of non-primitive recursive functions like the Ackermann function.
