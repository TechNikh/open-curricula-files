---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chirality_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: f5ea0be5-de12-43c8-8024-ea2f01409fcc
updated: 1484309304
title: Chirality (physics)
tags:
    - Chirality and helicity
    - Chiral theories
    - Chiral symmetry
    - 'Example: u and d quarks in QCD'
    - More Flavors
    - An application in Particle Physics
categories:
    - Symmetry
---
A chiral phenomenon is one that is not identical to its mirror image (see the article on mathematical chirality). The spin of a particle may be used to define a handedness, or helicity, for that particle, which, in the case of a massless particle, is the same as chirality. A symmetry transformation between the two is called parity. Invariance under parity by a Dirac fermion is called chiral symmetry.
