---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maintained
offline_file: ""
offline_thumbnail: ""
uuid: 7bdd5138-5b13-4529-83c5-588a5a513599
updated: 1484310357
title: maintained
categories:
    - Dictionary
---
maintained
     adj 1: kept in good condition [syn: {kept up(p)}, {well-kept}]
     2: made ready for service [syn: {repaired}, {serviced}]
     3: continued in your keeping or use or memory; "in...the
        retained pattern of dancers and guests remembered" [syn: {retained}]
