---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lakshadweep
offline_file: ""
offline_thumbnail: ""
uuid: e351530d-f140-4cc1-84e8-f06ad1446bc9
updated: 1484308425
title: Lakshadweep
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Map_of_Lakshadweep-en.svg.png
tags:
    - History
    - Independent India
    - geography
    - "India's Coral Islands"
    - Flora and Fauna
    - Government and administration
    - Demographics
    - religion
    - Languages
    - Economy
    - Fisheries
    - Tourism
    - Desalination
    - Transport and tourism
categories:
    - Uncategorized
---
Lakshadweep (/ləkˈʃɑːdwiːp/,  Lakṣadvīp (help·info), Lakshadīb), formerly known as the Laccadive, Minicoy, and Aminidivi Islands (/ˌlækədaɪv ˌmɪnᵻkɔɪ/ & /ˌæmᵻnˈdiːvi/),[2] is a group of islands in the Laccadive Sea, 200 to 440 kilometres (120 to 270 mi) off the south western coast of India. The archipelago is a Union Territory and is governed by the Union Government of India. They were also known as Laccadive Islands, although geographically this is only the name of the central subgroup of the group. Lakshadweep comes from "Lakshadweepa", which means "one hundred thousand islands" in Sanskrit.[3][4] The islands form the smallest Union Territory of India: their ...
