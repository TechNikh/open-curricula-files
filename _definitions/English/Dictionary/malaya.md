---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malaya
offline_file: ""
offline_thumbnail: ""
uuid: a1ade3f0-c525-4bc2-810f-7070ce952c97
updated: 1484310525
title: malaya
categories:
    - Dictionary
---
Malaya
     n : a constitutional monarchy in southeastern Asia on Borneo and
         the Malay Peninsula; achieved independence from the
         United Kingdom in 1957 [syn: {Malaysia}]
