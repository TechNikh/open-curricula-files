---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eutrophication
offline_file: ""
offline_thumbnail: ""
uuid: 5be2dcde-8cb9-4f9d-b057-2c20e5df2532
updated: 1484308340
title: Eutrophication
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Potomac_green_water.JPG
tags:
    - Mechanism of eutrophication
    - Lakes and rivers
    - Natural eutrophication
    - Ocean waters
    - Terrestrial ecosystems
    - Ecological effects
    - Decreased biodiversity
    - New species invasion
    - Toxicity
    - Sources of high nutrient runoff
    - Point sources
    - Nonpoint sources
    - Soil retention
    - Runoff to surface water and leaching to groundwater
    - Atmospheric deposition
    - Other causes
    - Prevention and reversal
    - 'Shellfish in estuaries: unique solutions'
    - 'Minimizing nonpoint pollution: future work'
    - Riparian buffer zones
    - Prevention policy
    - Nitrogen testing and modeling
    - Organic farming
    - Cultural eutrophication
categories:
    - Uncategorized
---
Eutrophication (Greek: eutrophia (from eu "well" + trephein "nourish".); German: Eutrophie), or more precisely hypertrophication, is the depletion of oxygen in a water body, which kills aquatic animals. It is a response to the addition of excess nutrients, mainly phosphates, which induces explosive growth of plants and algae, the decaying of which consumes oxygen from the water.[1] One example is the "bloom" or great increase of phytoplankton in a water body as a response to increased levels of nutrients. Eutrophication is almost always induced by the discharge of phosphate-containing detergents, fertilizers, or sewage, into an aquatic system.
