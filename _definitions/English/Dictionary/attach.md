---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attach
offline_file: ""
offline_thumbnail: ""
uuid: 0b3f0087-8234-46c9-9229-e80b5e41cc04
updated: 1484310423
title: attach
categories:
    - Dictionary
---
attach
     v 1: cause to be attached [ant: {detach}]
     2: be attached; be in contact with
     3: become attached; "The spider's thread attached to the window
        sill" [ant: {detach}]
     4: create social or emotional ties; "The grandparents want to
        bond with the child" [syn: {bind}, {tie}, {bond}]
     5: take temporary possession of as a security, by legal
        authority; "The FBI seized the drugs"; "The customs agents
        impounded the illegal shipment"; "The police confiscated
        the stolen artwork" [syn: {impound}, {sequester}, {confiscate},
         {seize}]
