---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/robbery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484458441
title: robbery
categories:
    - Dictionary
---
robbery
     n 1: larceny by threat of violence
     2: plundering during riots or in wartime [syn: {looting}]
