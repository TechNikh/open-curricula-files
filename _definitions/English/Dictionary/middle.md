---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/middle
offline_file: ""
offline_thumbnail: ""
uuid: ad2aa4cf-fc82-43b8-96f1-de5ed0af38cb
updated: 1484310220
title: middle
categories:
    - Dictionary
---
middle
     adj 1: being neither at the beginning nor at the end in a series;
            "adolescence is an awkward in-between age"; "in a
            mediate position"; "the middle point on a line" [syn:
            {in-between}, {mediate}]
     2: equally distant from the extremes [syn: {center(a)}, {halfway},
         {middle(a)}, {midway}]
     3: of a stage in the development of a language or literature
        between earlier and later stages; "Middle English is the
        English language from about 1100 to 1500"; "Middle Gaelic"
        [ant: {late}, {early}]
     4: between an earlier and a later period of time; "in the
        middle years"; "in his middle thirties" [ant: ...
