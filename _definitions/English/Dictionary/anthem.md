---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anthem
offline_file: ""
offline_thumbnail: ""
uuid: 7cf715d6-1286-4dd4-ae15-3841e92e9b35
updated: 1484310160
title: anthem
categories:
    - Dictionary
---
anthem
     n 1: a song of devotion or loyalty (as to a nation or school)
     2: a song of praise (to God or to a saint or to a nation) [syn:
         {hymn}]
