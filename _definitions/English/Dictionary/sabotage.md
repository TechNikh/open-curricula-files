---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sabotage
offline_file: ""
offline_thumbnail: ""
uuid: a7f59b78-fdec-4d13-8016-07d0bdeb843c
updated: 1484310585
title: sabotage
categories:
    - Dictionary
---
sabotage
     n : a deliberate act of destruction or disruption in which
         equipment is damaged
     v : destroy property or hinder normal operations; "The
         Resistance sabotaged railroad operations during the war"
         [syn: {undermine}, {countermine}, {counteract}, {subvert},
          {weaken}]
