---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/catastrophic
offline_file: ""
offline_thumbnail: ""
uuid: ecf2e601-ee08-436e-b3d6-022bc68a78dc
updated: 1484310545
title: catastrophic
categories:
    - Dictionary
---
catastrophic
     adj : extremely harmful; bringing physical or financial ruin; "a
           catastrophic depression"; "catastrophic illness"; "a
           ruinous course of action" [syn: {ruinous}]
