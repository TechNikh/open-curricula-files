---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/editorial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421781
title: editorial
categories:
    - Dictionary
---
editorial
     adj 1: of or relating to an article stating opinions or giving
            perspectives; "editorial column"
     2: relating to or characteristic of an editor; "editorial
        duties"
     n : an article giving opinions or perspectives [syn: {column}, {newspaper
         column}]
