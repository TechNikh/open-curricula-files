---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robustness_of_complex_networks
offline_file: ""
offline_thumbnail: ""
uuid: 6dd765b9-b17e-4a51-9706-52547ba3f6e2
updated: 1484309442
title: Robustness of complex networks
tags:
    - Percolation Theory
    - Critical threshold for random failures
    - Random network
    - Scale-free network
    - Targeted attacks on scale-free networks
    - Cascading failures
categories:
    - Network theory
---
The study of robustness in complex networks is important for many fields. In ecology, robustness is an important attribute of ecosystems, and can give insight into the reaction to disturbances such as the extinction of species.[1] For biologists, network robustness can help the study of diseases and mutations, and how to recover from some mutations.[2] In economics, network robustness principles can help our[who?] understanding of the stability and risks of banking systems.[3] And in engineering, network robustness can help to evaluate the resilience of infrastructure networks such as the Internet or power grids.[4]
