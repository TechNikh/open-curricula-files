---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484354041
title: secant
categories:
    - Dictionary
---
secant
     n 1: a straight line that intersects a curve at two or more
          points
     2: ratio of the hypotenuse to the adjacent side of a
        right-angled triangle [syn: {sec}]
