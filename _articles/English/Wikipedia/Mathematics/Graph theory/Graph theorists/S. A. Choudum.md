---
version: 1
type: article
id: https://en.wikipedia.org/wiki/S._A._Choudum
offline_file: ""
offline_thumbnail: ""
uuid: ad15bd1f-816f-4c68-b0fa-7433db6ae06c
updated: 1484309459
title: S. A. Choudum
categories:
    - Graph theorists
---
Sheshayya A. Choudum (born 1947) is a professor and a former chair of the department of mathematics at IIT Madras specializing in graph theory. He has often worked in chromatic number, degree sequence, graph enumeration, bivariegated graphs, and networks.
