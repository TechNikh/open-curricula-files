---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formulated
offline_file: ""
offline_thumbnail: ""
uuid: ca3bde2d-4046-4a63-a5e6-326a0c6099dd
updated: 1484310605
title: formulated
categories:
    - Dictionary
---
formulated
     adj : devised; developed according to an orderly plan; "he had
           well formulated opinions on schooling"
