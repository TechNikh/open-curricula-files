---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Demsetz_auction
offline_file: ""
offline_thumbnail: ""
uuid: 474a40a9-7164-4464-a8e3-0c3aa2752f73
updated: 1484309011
title: Demsetz auction
categories:
    - Auctions
---
A Demsetz auction is a system which awards an exclusive contract to the agent bidding the lowest price. This is sometimes referred to "competition for the field." It is in contrast to "competition in the field," which calls for two or more agents to be granted the contract and provide the good or service competitively. Martin Ricketts writes that "under competitive conditions, the bid prices should fall until they just enable firms to achieve a normal return on capital."[1] Disadvantages of a Demsetz auction include the fact that the entire risk associated with falling demand is borne by one agent and that the winner of the bid, once locked into the contract, may accumulate non-transferable ...
