---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isoperimetric_inequality
offline_file: ""
offline_thumbnail: ""
uuid: e66806af-33b0-4d40-9066-4e7bca4a37a1
updated: 1484309162
title: Isoperimetric inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Isoperimetric_inequality_illustr1.svg.png
tags:
    - The isoperimetric problem in the plane
    - Isoperimetric inequality on a plane
    - Isoperimetric inequality on a sphere
    - |
        Isoperimetric inequality in
        
        
        
        
        
        R
        
        
        n
        
        
        
        
        {\displaystyle \mathbb {R} ^{n}}
    - Isoperimetric inequalities in a metric measure space
    - Isoperimetric inequalities for graphs
    - 'Example: Isoperimetric inequalities for hypercubes'
    - Edge isoperimetric inequality
    - Vertex isoperimetric inequality
    - Isoperimetric inequality for triangles
    - Notes
categories:
    - Analytic geometry
---
In mathematics, the isoperimetric inequality is a geometric inequality involving the surface area of a set and its volume. In 
  
    
      
        n
      
    
    {\displaystyle n}
  
-dimensional space 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 the inequality lower bounds the surface area 
  
    
      
        
          s
          u
          r
          f
        
        (
        S
        )
      
    
    {\displaystyle \mathrm {surf} (S)}
  
 of a set 
  
    
      
        S
        ⊂
        
          
            R
          
          
            n
    ...
