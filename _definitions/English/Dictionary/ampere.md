---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ampere
offline_file: ""
offline_thumbnail: ""
uuid: 71dc7279-88ba-4a4d-b2e6-ae5a0a946d0e
updated: 1484310204
title: ampere
categories:
    - Dictionary
---
ampere
     n 1: a former unit of electric current (slightly smaller than the
          SI ampere) [syn: {international ampere}]
     2: the basic unit of electric current adopted under the Systeme
        International d'Unites; "a typical household circuit
        carries 15 to 50 amps" [syn: {amp}, {A}]
