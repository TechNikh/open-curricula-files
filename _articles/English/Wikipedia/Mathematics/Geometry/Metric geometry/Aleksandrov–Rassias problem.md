---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Aleksandrov%E2%80%93Rassias_problem'
offline_file: ""
offline_thumbnail: ""
uuid: fc5e8dc1-37fa-4f7e-81bc-a8e04197a129
updated: 1484309262
title: Aleksandrov–Rassias problem
categories:
    - Metric geometry
---
The theory of isometries in the framework of Banach spaces has its beginning in a paper by Stanisław Mazur and Stanisław M. Ulam in 1932.[1] They proved that each isometry of a normed real linear space onto a normed real linear space is a linear mapping up to translation. In 1970, Aleksandr Danilovich Aleksandrov asked whether the existence of a single conservative distance for some mapping implies that it is an isometry. Themistocles M. Rassias posed the following problem:
