---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stratification
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484594341
title: stratification
categories:
    - Dictionary
---
stratification
     n 1: the act or process or arranging persons into classes or
          social strata
     2: the condition of being arranged in social strata or classes
        within a group [syn: {social stratification}]
     3: forming or depositing in layers
     4: a layered configuration
     5: the placing of seeds in damp sand or sawdust or peat moss in
        ordere to preserve them or promote germination
