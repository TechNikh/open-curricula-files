---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Donald_B._Gillies
offline_file: ""
offline_thumbnail: ""
uuid: 886dbc27-99f4-4b88-9a9c-e75ef930557b
updated: 1484308517
title: Donald B. Gillies
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/MersennePrimeStamp.gif
tags:
    - Education
    - Early career
    - Later career
    - In memoriam
    - Students
categories:
    - Game theorists
---
Donald Bruce Gillies (October 15, 1928 – July 17, 1975) was a Canadian mathematician and computer scientist, known for his work in game theory, computer design, and minicomputer programming environments.
