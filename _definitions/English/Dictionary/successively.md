---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/successively
offline_file: ""
offline_thumbnail: ""
uuid: 5c0c5b13-e0c2-46cc-82a2-7d24b84b617f
updated: 1484310383
title: successively
categories:
    - Dictionary
---
successively
     adv : in proper order or sequence; "talked to each child in turn";
           "the stable became in turn a chapel and then a movie
           theater" [syn: {in turn}]
