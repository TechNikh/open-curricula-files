---
version: 1
type: video
provider: YouTube
id: MUxU5AWCBDU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 02c6b475-6a16-41cd-b9fa-fd17af6e2237
updated: 1486069611
title: "King's cupcakes  Solving systems by elimination"
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/MUxU5AWCBDU/default.jpg
    - https://i3.ytimg.com/vi/MUxU5AWCBDU/1.jpg
    - https://i3.ytimg.com/vi/MUxU5AWCBDU/2.jpg
    - https://i3.ytimg.com/vi/MUxU5AWCBDU/3.jpg
---
