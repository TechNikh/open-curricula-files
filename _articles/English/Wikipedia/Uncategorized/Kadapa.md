---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kadapa
offline_file: ""
offline_thumbnail: ""
uuid: f42471e1-6d2b-4719-acb9-766b5c6cd252
updated: 1484308447
title: Kadapa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kadapa_Municipal_Corporation_Logo.jpg
tags:
    - Etymology
    - History
    - Post classical era (200-800 AD)
    - Medieval Era (8th to 18th centuries AD)
    - geography
    - Topography
    - Climate
    - Demographics
    - Languages
    - Administration
    - Local government
    - culture
    - Arts and crafts
    - Cuisine
    - Economy
    - Education
    - Institutions
    - Transport
    - Roadways
    - Railways
    - Airways
categories:
    - Uncategorized
---
Kadapa (formerly known as Cuddapah) is a city in the Rayalseema region of the south-central part of Andhra Pradesh, India. It is the district headquarters of YSR Kadapa district. As of 2011 Census of India, the city had a population of 344,078. It is located 8 kilometres (5.0 mi) south of the Penna River. The city is surrounded on three sides by the Nallamala and Palakonda hills lying on the tectonic landscape between the Eastern and Western ghats. Black and Red ferrous soils occupy the region. The city is nicknamed "Gadapa" ('threshold') since it is the gateway from the west to the sacred hills of Tirumala.
