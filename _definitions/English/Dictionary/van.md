---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/van
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484429461
title: van
categories:
    - Dictionary
---
van
     n 1: any creative group active in the innovation and application
          of new concepts and techniques in a given field
          (especially in the arts) [syn: {avant-garde}, {vanguard},
           {new wave}]
     2: the leading units moving at the head of an army [syn: {vanguard}]
     3: a camper equipped with living quarters [syn: {caravan}]
     4: a truck with an enclosed cargo space
