---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cantic_octagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 9b2329dc-9b63-49f7-b1a9-6612abe3e279
updated: 1484309357
title: Cantic octagonal tiling
tags:
    - Dual tiling
    - Related polyhedra and tiling
categories:
    - Geometry stubs
---
In geometry, the tritetratrigonal tiling or shieldotritetragonal tiling is a uniform tiling of the hyperbolic plane. It has Schläfli symbol of t1,2(4,3,3). It can also be named as a cantic octagonal tiling, h2{8,3}.
