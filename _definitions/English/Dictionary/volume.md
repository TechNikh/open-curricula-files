---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/volume
offline_file: ""
offline_thumbnail: ""
uuid: 578c9e3c-4b4b-4127-bd66-f89fc78f9116
updated: 1484310330
title: volume
categories:
    - Dictionary
---
volume
     n 1: the amount of 3-dimensional space occupied by an object;
          "the gas expanded to twice its original volume"
     2: the property of something that is great in magnitude; "it is
        cheaper to buy it in bulk"; "he received a mass of
        correspondence"; "the volume of exports" [syn: {bulk}, {mass}]
     3: physical objects consisting of a number of pages bound
        together; "he used a large book as a doorstop" [syn: {book}]
     4: a publication that is one of a set of several similar
        publications; "the third volume was missing"; "he asked
        for the 1989 volume of the Annual Review"
     5: a relative amount; "mix one volume of the solution ...
