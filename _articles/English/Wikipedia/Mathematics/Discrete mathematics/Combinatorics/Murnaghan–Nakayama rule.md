---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Murnaghan%E2%80%93Nakayama_rule'
offline_file: ""
offline_thumbnail: ""
uuid: c30b6115-126b-417f-95af-785e5b72b182
updated: 1484308582
title: Murnaghan–Nakayama rule
categories:
    - Combinatorics
---
In mathematics, the Murnaghan–Nakayama rule is a combinatorial method to compute irreducible character values of the symmetric group.[1] There are several generalizations of this rule.
