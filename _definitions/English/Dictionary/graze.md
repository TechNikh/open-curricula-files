---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graze
offline_file: ""
offline_thumbnail: ""
uuid: 03c33480-7587-48f3-bcee-c4e7321a8784
updated: 1484310543
title: graze
categories:
    - Dictionary
---
graze
     n 1: a superficial abrasion
     2: the act of grazing [syn: {grazing}]
     v 1: feed as in a meadow or pasture; "the herd was grazing" [syn:
           {crop}, {browse}, {range}, {pasture}]
     2: break the skin (of a body part) by scraping; "She was grazed
        by the stray bullet"
     3: let feed in a field or pasture or meadow [syn: {crop}, {pasture}]
     4: scrape gently; "graze the skin" [syn: {crease}, {rake}]
     5: eat lightly, try different dishes; "There was so much food
        at the party that we quickly got sated just by browsing"
        [syn: {browse}]
