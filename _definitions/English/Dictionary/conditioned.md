---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conditioned
offline_file: ""
offline_thumbnail: ""
uuid: cabe536b-2ddc-4ef9-bd3c-388326f06511
updated: 1484310349
title: conditioned
categories:
    - Dictionary
---
conditioned
     adj 1: established by conditioning or learning; "a conditioned
            response" [syn: {learned}] [ant: {unconditioned}]
     2: physically fit; "exercised daily to keep herself in
        condition" [syn: {in condition(p)}]
