---
version: 1
type: article
id: https://en.wikipedia.org/wiki/A_New_Kind_of_Science
offline_file: ""
offline_thumbnail: ""
uuid: fc4aa76a-5926-42fd-827f-25a5c57f989a
updated: 1484308823
title: A New Kind of Science
tags:
    - Contents
    - Computation and its implications
    - Simple programs
    - Mapping and mining the computational universe
    - Systematic abstract science
    - Philosophical underpinnings
    - Computational irreducibility
    - Principle of computational equivalence
    - Applications and results
    - NKS Summer School
    - Reception
    - Scientific philosophy
    - Methodology
    - Utility
    - PCE
    - The fundamental theory (NKS Chapter 9)
    - Natural selection
    - Originality
categories:
    - Self-organization
---
A New Kind of Science is a best-selling,[1] controversial book by Stephen Wolfram, published by his own company in 2002. It contains an empirical and systematic study of computational systems such as cellular automata. Wolfram calls these systems simple programs and argues that the scientific philosophy and methods appropriate for the study of simple programs are relevant to other fields of science.
