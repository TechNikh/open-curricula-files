---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tanner_graph
offline_file: ""
offline_thumbnail: ""
uuid: cbdc3c48-8477-4d0b-b90c-4e052d8592d1
updated: 1484308573
title: Tanner graph
tags:
    - Origins
    - Tanner graphs for linear block codes
    - Bounds proved by Tanner
    - Computational complexity of Tanner graph based methods
    - Applications of Tanner graph
    - Notes
categories:
    - Coding theory
---
In coding theory, a Tanner graph, named after Michael Tanner, is a bipartite graph used to state constraints or equations which specify error correcting codes. In coding theory, Tanner graphs are used to construct longer codes from smaller ones. Both encoders and decoders employ these graphs extensively.
