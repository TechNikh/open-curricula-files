---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reader
offline_file: ""
offline_thumbnail: ""
uuid: efb65c91-f984-4e6e-b15f-0bf2594a25a1
updated: 1484310429
title: reader
categories:
    - Dictionary
---
reader
     n 1: a person who enjoys reading
     2: someone who contracts to receive and pay for a certain
        number of issues of a publication [syn: {subscriber}]
     3: a person who can read; a literate person
     4: someone who reads manuscripts and judges their suitability
        for publication [syn: {reviewer}, {referee}]
     5: someone who reads proof in order to find errors and mark
        corrections [syn: {proofreader}]
     6: someone who reads the lessons in a church service; someone
        ordained in a minor order of the Roman Catholic Church
        [syn: {lector}]
     7: a public lecturer at certain universities [syn: {lector}, {lecturer}]
     8: one of a ...
