---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adversary
offline_file: ""
offline_thumbnail: ""
uuid: 9700558b-67db-4586-853e-05d4245218d2
updated: 1484310176
title: adversary
categories:
    - Dictionary
---
adversary
     n : someone who offers opposition [syn: {antagonist}, {opponent},
          {opposer}, {resister}] [ant: {agonist}]
