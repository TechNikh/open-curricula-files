---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/third-class
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500981
title: third-class
categories:
    - Dictionary
---
third class
     n 1: mail consisting of printed matter qualifying for reduced
          postal rates [syn: {third-class mail}]
     2: inexpensive accommodations on a ship or train [syn: {tourist
        class}]
