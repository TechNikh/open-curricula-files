---
version: 1
type: video
provider: YouTube
id: PhHWmehThg8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Bay%20Area%20CBS%20Station%20with%20Salman%20Khan.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 99837853-e637-4ab1-98ee-a70fc6e17ce7
updated: 1486069557
title: Bay Area CBS Station with Salman Khan
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/PhHWmehThg8/default.jpg
    - https://i3.ytimg.com/vi/PhHWmehThg8/1.jpg
    - https://i3.ytimg.com/vi/PhHWmehThg8/2.jpg
    - https://i3.ytimg.com/vi/PhHWmehThg8/3.jpg
---
