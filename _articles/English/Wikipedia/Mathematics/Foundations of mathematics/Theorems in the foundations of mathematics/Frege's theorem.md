---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Frege%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c005881d-6bd6-4ddd-a406-b6285c6d45a9
updated: 1484308994
title: "Frege's theorem"
categories:
    - Theorems in the foundations of mathematics
---
In metalogic and metamathematics, Frege's theorem is a metatheorem that states that the Peano axioms of arithmetic can be derived in second-order logic from Hume's principle. It was first proven, informally, by Gottlob Frege in his Die Grundlagen der Arithmetik (The Foundations of Arithmetic),[page needed] published in 1884, and proven more formally in his Grundgesetze der Arithmetik (The Basic Laws of Arithmetic),[page needed] published in two volumes, in 1893 and 1903. The theorem was re-discovered by Crispin Wright in the early 1980s and has since been the focus of significant work. It is at the core of the philosophy of mathematics known as neo-logicism.
