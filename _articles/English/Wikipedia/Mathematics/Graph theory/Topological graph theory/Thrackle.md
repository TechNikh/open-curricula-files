---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thrackle
offline_file: ""
offline_thumbnail: ""
uuid: 6fc8c1f0-1769-414c-a13f-4701a4a9df46
updated: 1484309481
title: Thrackle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6-cycle_thrackle.png
tags:
    - Linear thrackles
    - Thrackle conjecture
    - Known bounds
categories:
    - Topological graph theory
---
A thrackle is an embedding of a graph in the plane, such that each edge is a Jordan arc and every pair of edges meet once. Edges may either meet at a common endpoint, or, if they have no endpoints in common, at a point in their interiors. In the latter case, the crossing must be transverse.[1]
