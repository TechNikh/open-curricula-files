---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polygon_partition
offline_file: ""
offline_thumbnail: ""
uuid: afb96b35-4ad0-4e71-86aa-a7328326e34c
updated: 1484309179
title: Polygon partition
tags:
    - Applications
    - Partitioning a polygon to triangles
    - Partitioning a polygon to pseudo-triangles
    - Partitioning a rectilinear polygon to rectangles
    - Partition a polygon to trapezoids
    - Partition a polygon to convex quadrilaterals
    - Partition a polygon to m-gons
    - More general component shapes
categories:
    - Computational geometry
---
A partition of a polygon is a set of primitive units (e.g. squares), which do not overlap and whose union equals the polygon. A polygon partition problem is a problem of finding a partition which is minimal in some sense, for example a partition with a smallest number of units or with units of smallest total side-length.
