---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oracle_Spatial_and_Graph
offline_file: ""
offline_thumbnail: ""
uuid: ddbb2290-f140-4f7a-941e-93eacf91514f
updated: 1484309405
title: Oracle Spatial and Graph
tags:
    - Components
    - Geospatial data features
    - Network Data Model
    - RDF semantic
    - Availability
    - History
    - Additional reading
categories:
    - Graph databases
---
Oracle Spatial and Graph, formerly Oracle Spatial, forms a separately-licensed option component of the Oracle Database. The spatial features in Oracle Spatial and Graph aid users in managing geographic and location-data in a native type within an Oracle database, potentially supporting a wide range of applications — from automated mapping, facilities management, and geographic information systems (AM/FM/GIS), to wireless location services and location-enabled e-business. The graph features in Oracle Spatial and Graph include Oracle Network Data Model (NDM) graphs used in traditional network applications in major transportation, telcos, utilities and energy organizations and RDF semantic ...
