---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/links
offline_file: ""
offline_thumbnail: ""
uuid: 2787ca0c-b23a-473f-99df-83b554135310
updated: 1484310279
title: links
categories:
    - Dictionary
---
links
     n : course consisting of a large landscaped area for playing
         golf [syn: {golf course}, {golf links}]
