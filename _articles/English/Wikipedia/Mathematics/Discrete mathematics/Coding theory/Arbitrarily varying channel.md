---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arbitrarily_varying_channel
offline_file: ""
offline_thumbnail: ""
uuid: 4ce9c48a-e0fa-4a45-a487-bb688721d565
updated: 1484308545
title: Arbitrarily varying channel
tags:
    - Capacities and associated proofs
    - Capacity of deterministic AVCs
    - Capacity of AVCs with input and state constraints
    - Capacity of randomized AVCs
categories:
    - Coding theory
---
An arbitrarily varying channel (AVC) is a communication channel model used in coding theory, and was first introduced by Blackwell, Breiman, and Thomasian. This particular channel has unknown parameters that can change over time and these changes may not have a uniform pattern during the transmission of a codeword. n{\displaystyle \textstyle n} uses of this channel can be described using a stochastic matrix Wn:Xn×{\displaystyle \textstyle W^{n}:X^{n}\times } Sn→Yn{\displaystyle \textstyle S^{n}\rightarrow Y^{n}}, where X{\displaystyle \textstyle X} is the input alphabet, Y{\displaystyle \textstyle Y} is the output alphabet, and Wn(y|x,s){\displaystyle \textstyle W^{n}(y|x,s)} is the ...
