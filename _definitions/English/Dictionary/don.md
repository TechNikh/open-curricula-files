---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/don
offline_file: ""
offline_thumbnail: ""
uuid: e226e4ad-9020-4667-ba45-9200ac4383ad
updated: 1484310244
title: don
categories:
    - Dictionary
---
Don
     n 1: a Spanish title of respect for a gentleman or nobleman
     2: teacher at a university of college (especially at Cambridge
        or Oxford) [syn: {preceptor}]
     3: the head of an organized crime family [syn: {father}]
     4: Celtic goddess; mother of Gwydion and Arianrhod; corresponds
        to Irish Danu
     5: a European river in southwestern Russia; flows into the Sea
        of Azov [syn: {Don River}]
     v : put clothing on one's body; "What should I wear today?"; "He
         put on his best suit for the wedding"; "The princess
         donned a long blue dress"; "The queen assumed the stately
         robes"; "He got into his jeans" [syn: {wear}, {put on}, ...
