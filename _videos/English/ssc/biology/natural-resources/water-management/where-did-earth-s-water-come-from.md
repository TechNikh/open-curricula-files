---
title: Where Did Earth's Water Come From?
youtube_id: _LpgBvEPozk
tags:
    - MinuteEarth
    - Minute Earth
    - MinutePhysics
    - Minute Physics
    - Earth
    - History
    - Science
    - Environment
    - environmental science
    - earth science
    - blue planet
    - Water
    - meteor
    - meteorite
    - carbonaceous chondrite
    - comets
    - space invaders
categories:
    - Water management
---
