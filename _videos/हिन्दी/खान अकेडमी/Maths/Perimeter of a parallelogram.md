---
version: 1
type: video
provider: YouTube
id: YkQhvqxNR5s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5e6c4597-171d-4a01-9e41-e91fb648f6c8
updated: 1486069611
title: Perimeter of a parallelogram
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/YkQhvqxNR5s/default.jpg
    - https://i3.ytimg.com/vi/YkQhvqxNR5s/1.jpg
    - https://i3.ytimg.com/vi/YkQhvqxNR5s/2.jpg
    - https://i3.ytimg.com/vi/YkQhvqxNR5s/3.jpg
---
