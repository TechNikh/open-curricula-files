---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nerves
offline_file: ""
offline_thumbnail: ""
uuid: cfdb7644-4453-4753-a4a3-48d744cbba22
updated: 1484310335
title: nerves
categories:
    - Dictionary
---
nerves
     n 1: an uneasy psychological state; "he suffered an attack of
          nerves" [syn: {nervousness}]
     2: control of your emotions; "this kind of tension is not good
        for my nerves"
