---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A9n%C3%A1rt_sphere'
offline_file: ""
offline_thumbnail: ""
uuid: e03b834d-1298-4c21-b8d3-88b9ef20783d
updated: 1484309345
title: Lénárt sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/236px-Istvan_Lenart_and_spheres.jpg
tags:
    - Basic set
    - Related products
    - History
    - Spherical tessellation
categories:
    - Trigonometry
---
A Lénárt sphere is a teaching and educational research model for spherical geometry. The Lénárt sphere is a modern replacement of a "spherical blackboard".[1] It can be used for use in visualizing spherical polygons (especially triangles) showing the relationships between the sides and the angles.
