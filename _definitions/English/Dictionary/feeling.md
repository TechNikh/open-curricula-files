---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feeling
offline_file: ""
offline_thumbnail: ""
uuid: 3596913b-bda0-4526-8242-381196caa002
updated: 1484310359
title: feeling
categories:
    - Dictionary
---
feeling
     n 1: the experiencing of affective and emotional states; "she had
          a feeling of euphoria"; "he had terrible feelings of
          guilt"; "I disliked him and the feeling was mutual"
     2: a vague idea in which some confidence is placed; "his
        impression of her was favorable"; "what are your feelings
        about the crisis?"; "it strengthened my belief in his
        sincerity"; "I had a feeling that she was lying" [syn: {impression},
         {belief}, {notion}, {opinion}]
     3: the general atmosphere of a place or situation and the
        effect that it has on people; "the feel of the city
        excited him"; "a clergyman improved the tone of the
      ...
