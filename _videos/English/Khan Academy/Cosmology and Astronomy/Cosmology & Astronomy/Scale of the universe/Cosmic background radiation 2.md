---
version: 1
type: video
provider: YouTube
id: 06z7Q8TWPyU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Cosmic%20background%20radiation%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c0d2e560-80b7-4060-b0ad-6c75bde71926
updated: 1486069685
title: Cosmic background radiation 2
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/06z7Q8TWPyU/default.jpg
    - https://i3.ytimg.com/vi/06z7Q8TWPyU/1.jpg
    - https://i3.ytimg.com/vi/06z7Q8TWPyU/2.jpg
    - https://i3.ytimg.com/vi/06z7Q8TWPyU/3.jpg
---
