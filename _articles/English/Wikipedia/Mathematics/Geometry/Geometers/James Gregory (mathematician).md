---
version: 1
type: article
id: https://en.wikipedia.org/wiki/James_Gregory_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: a54e3103-7103-487e-88b8-92a41e381bf4
updated: 1484309080
title: James Gregory (mathematician)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gregory_-_Vera_circuli_et_hyperbolae_quadratura%252C_in_propria_sua_proportionis_specie%252C_inventa_et_demonstrata%252C_1667_-_878952.jpg'
tags:
    - Biography
    - Published works
    - Optica Promota
    - Vera Circuli et Hyperbolae Quadratura
    - Gregorian telescope
    - Mathematics
    - Other work
    - Works
categories:
    - Geometers
---
James Gregory (also spelled James Gregorie) FRS (November 1638 – October 1675) was a Scottish mathematician and astronomer. He described an early practical design for the reflecting telescope – the Gregorian telescope – and made advances in trigonometry, discovering infinite series representations for several trigonometric functions.
