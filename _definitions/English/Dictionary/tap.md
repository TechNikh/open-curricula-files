---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tap
offline_file: ""
offline_thumbnail: ""
uuid: 7fa84cf9-0726-4166-a62b-081d8c1937c7
updated: 1484310256
title: tap
categories:
    - Dictionary
---
tap
     n 1: the sound made by a gentle blow [syn: {pat}, {rap}]
     2: a faucet for drawing water from a pipe or cask [syn: {water
        faucet}, {water tap}, {spigot}, {hydrant}]
     3: a gentle blow [syn: {rap}, {strike}]
     4: a small metal plate that attaches to the toe or heel of a
        shoe (as in tap dancing)
     5: a tool for cutting female (internal) screw threads
     6: a plug for a bunghole in a cask [syn: {spigot}]
     7: the act of tapping a telephone or telegraph line to get
        information [syn: {wiretap}]
     8: a light touch or stroke [syn: {pat}, {dab}]
     v 1: cut a female screw thread with a tap
     2: draw from or dip into to get something; "tap ...
