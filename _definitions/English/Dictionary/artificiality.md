---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/artificiality
offline_file: ""
offline_thumbnail: ""
uuid: 094811b9-8707-4907-b1d4-ab4b0b368bc3
updated: 1484310144
title: artificiality
categories:
    - Dictionary
---
artificiality
     n : the quality of being produced by people and not occurring
         naturally
