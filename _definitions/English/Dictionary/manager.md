---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manager
offline_file: ""
offline_thumbnail: ""
uuid: 7bbbc375-9456-45cd-bdfe-a8eb1ca6e306
updated: 1484310521
title: manager
categories:
    - Dictionary
---
manager
     n 1: someone who controls resources and expenditures [syn: {director},
           {managing director}]
     2: (sports) someone in charge of training an athlete or a team
        [syn: {coach}, {handler}]
