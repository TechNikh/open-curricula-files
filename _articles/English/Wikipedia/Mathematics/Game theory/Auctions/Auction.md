---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auction
offline_file: ""
offline_thumbnail: ""
uuid: 441672c7-162b-400a-8181-aac9988360fd
updated: 1484309010
title: Auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Auction.jpg
tags:
    - History
    - Modern revival
    - Types
    - Primary
    - secondary
    - Genres
    - Time requirements
    - characteristics
    - Common uses
    - Bidding strategy
    - Bid shading
    - Chandelier or rafter bidding
    - Collusion
    - Suggested opening bid (SOB)
    - Terminology
    - JEL classification
    - Notes
categories:
    - Auctions
---
An auction is a process of buying and selling goods or services by offering them up for bid, taking bids, and then selling the item to the highest bidder. The open ascending price auction is arguably the most common form of auction in use today.[1] Participants bid openly against one another, with each subsequent bid required to be higher than the previous bid.[2] An auctioneer may announce prices, bidders may call out their bids themselves (or have a proxy call out a bid on their behalf), or bids may be submitted electronically with the highest current bid publicly displayed.[2] In a Dutch auction, the auctioneer begins with a high asking price for some quantity of like items; the price is ...
