---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tsar
offline_file: ""
offline_thumbnail: ""
uuid: a56aaadc-3733-4e57-9f11-ccf907c87669
updated: 1484310557
title: tsar
categories:
    - Dictionary
---
tsar
     n : a male monarch or emperor (especially of Russia prior to
         1917) [syn: {czar}, {tzar}]
