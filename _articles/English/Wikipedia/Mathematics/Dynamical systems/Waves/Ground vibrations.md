---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ground_vibrations
offline_file: ""
offline_thumbnail: ""
uuid: ca803eaf-d05a-4e3c-9561-2dea89132e16
updated: 1484308885
title: Ground vibrations
tags:
    - General information
    - Ground vibrations from railways
    - Ground vibrations from road traffic
    - Ground vibrations at construction
categories:
    - Waves
---
Ground vibrations is a technical term that is being used to describe mostly man-made vibrations of the ground, in contrast to natural vibrations of the Earth studied by seismology. For example, vibrations caused by explosions, construction works, railway and road transport, etc. - all belong to ground vibrations.
