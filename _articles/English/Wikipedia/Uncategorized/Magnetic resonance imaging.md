---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mri
offline_file: ""
offline_thumbnail: ""
uuid: 9e8b492d-8118-45c2-baa7-ccb149c9b16c
updated: 1484308450
title: Magnetic resonance imaging
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-White_Matter_Connections_Obtained_with_MRI_Tractography.png
tags:
    - Medical uses
    - Neuroimaging
    - Cardiovascular
    - Musculoskeletal
    - Liver and gastrointestinal imaging MRI
    - Functional MRI
    - Oncology
    - Phase Contrast MRI
    - Safety
    - Structure and Certification
    - Implants
    - Projectile risk
    - MRI-EEG
    - Genotoxic effects
    - Peripheral nerve stimulation (PNS)
    - Heating caused by absorption of radio waves
    - Acoustic noise
    - Cryogens
    - Pregnancy
    - Claustrophobia and discomfort
    - MRI versus CT
    - Guidance
    - The European Directive on Electromagnetic Fields
    - Procedure
    - Contrast
    - Contrast agents
    - Interpretation
    - History
    - 2003 Nobel Prize
    - Economics
    - Overuse
    - Specialized applications
    - Diffusion MRI
    - Magnetic resonance angiography
    - Magnetic resonance spectroscopy
    - Functional MRI
    - Real-time MRI
    - Interventional MRI
    - Magnetic resonance guided focused ultrasound
    - Multinuclear imaging
    - Molecular imaging by MRI
    - Other specialized sequences
    - Magnetization transfer MRI
    - T1rho MRI
    - Proton density weighted
    - Fluid attenuated inversion recovery (FLAIR)
    - Susceptibility weighted imaging (SWI)
    - Neuromelanin imaging
categories:
    - Uncategorized
---
Magnetic resonance imaging (MRI) is a medical imaging technique used in radiology to form pictures of the anatomy and the physiological processes of the body in both health and disease. MRI scanners use strong magnetic fields, radio waves, and field gradients to generate images of the inside of the body.
