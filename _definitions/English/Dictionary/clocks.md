---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clocks
offline_file: ""
offline_thumbnail: ""
uuid: 0e68a695-3278-4aba-968e-eabe73f24f0b
updated: 1484310439
title: clocks
categories:
    - Dictionary
---
clocks
     n : European weed naturalized in southwestern United States and
         Mexico having reddish decumbent stems with small fernlike
         leaves and small deep reddish-lavender flowers followed
         by slender fruits that stick straight up; often grown for
         forage [syn: {redstem storksbill}, {alfilaria}, {alfileria},
          {filaree}, {filaria}, {pin grass}, {pin clover}, {Erodium
         cicutarium}]
