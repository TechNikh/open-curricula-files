---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infer
offline_file: ""
offline_thumbnail: ""
uuid: ee3a0c53-fa18-4d12-a145-e1cbf13c234c
updated: 1484310218
title: infer
categories:
    - Dictionary
---
infer
     v 1: reason by deduction; establish by deduction [syn: {deduce},
          {deduct}, {derive}]
     2: draw from specific cases for more general cases [syn: {generalize},
         {generalise}, {extrapolate}]
     3: conclude by reasoning; in logic [syn: {deduce}]
     4: guess correctly; solve by guessing; "He guessed the right
        number of beans in the jar and won the prize" [syn: {guess}]
     5: believe to be the case; "I understand you have no previous
        experience?" [syn: {understand}]
     [also: {inferring}, {inferred}]
