---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/packing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484400781
title: packing
categories:
    - Dictionary
---
packing
     n 1: any material used especially to protect something [syn: {packing
          material}, {wadding}]
     2: the enclosure of something in a package or box [syn: {boxing}]
     3: carrying something in a pack on the back; "the backpacking
        of oxygen is essential for astronauts" [syn: {backpacking}]
