---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidase
offline_file: ""
offline_thumbnail: ""
uuid: 16dbb370-c8cd-42cf-8c52-e51e5680ff0d
updated: 1484310375
title: oxidase
categories:
    - Dictionary
---
oxidase
     n : any of the enzymes that catalyze biological oxidation
