---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malleability
offline_file: ""
offline_thumbnail: ""
uuid: 4621d807-c4dc-472c-a5e1-40bf9a9e38e0
updated: 1484310407
title: malleability
categories:
    - Dictionary
---
malleability
     n : the property of being physically malleable; the property of
         something that can be worked or hammered or shaped under
         pressure without breaking [syn: {plasticity}] [ant: {unmalleability}]
