---
version: 1
type: video
provider: YouTube
id: 1LxhXqD3_CE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Generalized%20Taylor%20Series%20Approximation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0b7d73d3-393b-4c2b-afda-c6c62c7a14b9
updated: 1486069694
title: Generalized Taylor Series Approximation
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/1LxhXqD3_CE/default.jpg
    - https://i3.ytimg.com/vi/1LxhXqD3_CE/1.jpg
    - https://i3.ytimg.com/vi/1LxhXqD3_CE/2.jpg
    - https://i3.ytimg.com/vi/1LxhXqD3_CE/3.jpg
---
