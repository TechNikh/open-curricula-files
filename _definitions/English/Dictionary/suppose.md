---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suppose
offline_file: ""
offline_thumbnail: ""
uuid: fed3461f-e694-4ef2-a638-e1dabbe8a1a8
updated: 1484310346
title: suppose
categories:
    - Dictionary
---
suppose
     v 1: express a supposition; "Let us say that he did not tell the
          truth"; "Let's say you had a lot of money--what would
          you do?" [syn: {say}]
     2: expect, believe, or suppose; "I imagine she earned a lot of
        money with her new novel"; "I thought to find her in a bad
        state"; "he didn't think to find her in the kitchen"; "I
        guess she is angry at me for standing her up" [syn: {think},
         {opine}, {imagine}, {reckon}, {guess}]
     3: to believe especially on uncertain or tentative grounds;
        "Scientists supposed that large dinosaurs lived in swamps"
        [syn: {speculate}, {theorize}, {theorise}, {conjecture}, ...
