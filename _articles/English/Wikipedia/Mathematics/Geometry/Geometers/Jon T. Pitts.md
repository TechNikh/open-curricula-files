---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jon_T._Pitts
offline_file: ""
offline_thumbnail: ""
uuid: 47fd0e3d-6dc9-472b-bd28-d9f811202d55
updated: 1484309096
title: Jon T. Pitts
categories:
    - Geometers
---
Jon T. Pitts (born 1948) is an American mathematician working on geometric analysis and variational calculus. He is a professor at Texas A&M University.
