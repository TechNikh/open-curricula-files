---
version: 1
type: video
provider: YouTube
id: TxKI68nTAnQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 75c46fa2-d3cf-4480-be7f-f7d6df7ed46f
updated: 1486069611
title: Using expressions to understand relationships
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/TxKI68nTAnQ/default.jpg
    - https://i3.ytimg.com/vi/TxKI68nTAnQ/1.jpg
    - https://i3.ytimg.com/vi/TxKI68nTAnQ/2.jpg
    - https://i3.ytimg.com/vi/TxKI68nTAnQ/3.jpg
---
