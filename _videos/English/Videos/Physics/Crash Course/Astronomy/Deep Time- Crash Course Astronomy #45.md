---
version: 1
type: video
provider: YouTube
id: jDF-N3A60DE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Deep%20Time-%20Crash%20Course%20Astronomy%20%2345.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5f0315e0-fc63-4eea-9fc3-43f86c321534
updated: 1486069684
title: 'Deep Time- Crash Course Astronomy #45'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/jDF-N3A60DE/default.jpg
    - https://i3.ytimg.com/vi/jDF-N3A60DE/1.jpg
    - https://i3.ytimg.com/vi/jDF-N3A60DE/2.jpg
    - https://i3.ytimg.com/vi/jDF-N3A60DE/3.jpg
---
