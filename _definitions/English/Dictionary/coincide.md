---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coincide
offline_file: ""
offline_thumbnail: ""
uuid: ab411469-4f4d-474b-b6a6-12d2693ab5dc
updated: 1484310216
title: coincide
categories:
    - Dictionary
---
coincide
     v 1: go with, fall together [syn: {co-occur}, {cooccur}]
     2: happen simultaneously; "The two events coincided" [syn: {concur}]
     3: be the same; "our views on this matter coincided"
