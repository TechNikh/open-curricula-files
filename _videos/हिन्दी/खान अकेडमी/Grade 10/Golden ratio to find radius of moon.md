---
version: 1
type: video
provider: YouTube
id: A_VJN3pfvJ0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0b272218-f820-4ec7-8ecd-05d55379b65f
updated: 1486069619
title: Golden ratio to find radius of moon
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/A_VJN3pfvJ0/default.jpg
    - https://i3.ytimg.com/vi/A_VJN3pfvJ0/1.jpg
    - https://i3.ytimg.com/vi/A_VJN3pfvJ0/2.jpg
    - https://i3.ytimg.com/vi/A_VJN3pfvJ0/3.jpg
---
