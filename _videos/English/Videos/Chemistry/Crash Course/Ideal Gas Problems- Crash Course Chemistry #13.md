---
version: 1
type: video
provider: YouTube
id: 8SRAkXMu3d0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6c436bdd-66de-4784-b2a5-e0899a17bace
updated: 1486069680
title: 'Ideal Gas Problems- Crash Course Chemistry #13'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/8SRAkXMu3d0/default.jpg
    - https://i3.ytimg.com/vi/8SRAkXMu3d0/1.jpg
    - https://i3.ytimg.com/vi/8SRAkXMu3d0/2.jpg
    - https://i3.ytimg.com/vi/8SRAkXMu3d0/3.jpg
---
