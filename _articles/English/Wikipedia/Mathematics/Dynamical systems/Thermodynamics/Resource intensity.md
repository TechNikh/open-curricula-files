---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resource_intensity
offline_file: ""
offline_thumbnail: ""
uuid: 7ab376b9-4d0b-4f4a-93f8-2dfbb17565d6
updated: 1484308857
title: Resource intensity
categories:
    - Thermodynamics
---
Resource intensity is a measure of the resources (e.g. water, energy, materials) needed for the production, processing and disposal of a unit of good or service, or for the completion of a process or activity; it is therefore a measure of the efficiency of resource use. It is often expressed as the quantity of resource embodied in unit cost e.g. litres of water per $1 spent on product. In national economic and sustainability accounting it can be calculated as units of resource expended per unit of GDP. When applied to a single person it is expressed as the resource use of that person per unit of consumption. Relatively high resource intensities indicate a high price or environmental cost of ...
