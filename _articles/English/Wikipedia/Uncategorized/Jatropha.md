---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jatropha
offline_file: ""
offline_thumbnail: ""
uuid: 439a60da-838b-4c03-b650-b8a6874fc0ff
updated: 1484308331
title: Jatropha
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jatropha_pandurifolia_02.jpg
tags:
    - Uses
    - Toxicity
    - Selected species
    - Gallery
    - Formerly placed here
    - Synonyms
categories:
    - Uncategorized
---
Jatropha is a genus of flowering plants in the spurge family, Euphorbiaceae. The name is derived from the Greek words ἰατρός (iatros), meaning "physician", and τροφή (trophe), meaning "nutrition", hence the common name physic nut. Another common name is nettlespurge.[2] It contains approximately 170 species of succulent plants, shrubs and trees (some are deciduous, like Jatropha curcas). Most of these are native to the Americas, with 66 species found in the Old World.[3] Plants produce separate male and female flowers. As with many members of the family Euphorbiaceae, Jatropha contains compounds that are highly toxic. Jatropha species have traditionally been used in ...
