---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mexican
offline_file: ""
offline_thumbnail: ""
uuid: b99e11ce-38b3-4e5d-a559-42bc6ecdcb51
updated: 1484310515
title: mexican
categories:
    - Dictionary
---
Mexican
     adj : of or relating to Mexico or its inhabitants; "Mexican food
           is hot"
     n : a native of inhabitant of Mexico
