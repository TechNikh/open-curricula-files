---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bochner_identity
offline_file: ""
offline_thumbnail: ""
uuid: 7839c342-7e75-4eaf-a0d5-570d5f5f8276
updated: 1484309206
title: Bochner identity
tags:
    - Statement of the result
categories:
    - Differential geometry
---
In mathematics — specifically, differential geometry — the Bochner identity is an identity concerning harmonic maps between Riemannian manifolds. The identity is named after the American mathematician Salomon Bochner.
