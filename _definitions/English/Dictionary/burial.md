---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580841
title: burial
categories:
    - Dictionary
---
burial
     n 1: the ritual placing of a corpse in a grave [syn: {entombment},
           {inhumation}, {interment}, {sepulture}]
     2: concealing something under the ground [syn: {burying}]
