---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414881
title: affable
categories:
    - Dictionary
---
affable
     adj : diffusing warmth and friendliness; "an affable smile"; "an
           amiable gathering"; "cordial relations"; "a cordial
           greeting"; "a genial host" [syn: {amiable}, {cordial},
           {genial}]
