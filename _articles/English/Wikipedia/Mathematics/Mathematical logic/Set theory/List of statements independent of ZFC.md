---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_statements_independent_of_ZFC
offline_file: ""
offline_thumbnail: ""
uuid: d64c39f4-5ef1-4cc2-8bef-0f3d53b28918
updated: 1484309507
title: List of statements independent of ZFC
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Undecidable_ZFC_statements_implication_chains.png
tags:
    - Axiomatic set theory
    - Set theory of the real line
    - Order theory
    - Abstract algebra
    - Number theory
    - Measure theory
    - Topology
    - Functional analysis
    - Model Theory
categories:
    - Set theory
---
The mathematical statements discussed below are provably independent of ZFC (the Zermelo–Fraenkel axioms plus the axiom of choice, the canonical axiomatic set theory of contemporary mathematics), assuming that ZFC is consistent. A statement is independent of ZFC (sometimes phrased "undecidable in ZFC") if it can neither be proven nor disproven from the axioms of ZFC.
