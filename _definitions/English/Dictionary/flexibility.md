---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flexibility
offline_file: ""
offline_thumbnail: ""
uuid: 938c3c03-fe74-4a23-af52-bbd823af249f
updated: 1484310531
title: flexibility
categories:
    - Dictionary
---
flexibility
     n 1: the property of being flexible [syn: {flexibleness}] [ant: {inflexibility}]
     2: the quality of being adaptable or variable; "he enjoyed the
        flexibility of his working arrangement" [ant: {inflexibility}]
     3: the trait of being easily persuaded [syn: {tractability}, {tractableness}]
        [ant: {intractability}]
