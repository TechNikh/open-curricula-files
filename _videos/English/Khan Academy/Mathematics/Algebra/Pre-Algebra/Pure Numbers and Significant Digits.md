---
version: 1
type: video
provider: YouTube
id: Ym5u5IlYWcg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Pure%20Numbers%20and%20Significant%20Digits.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8a4685d5-19f9-4ce2-88b3-1af1eb0b0990
updated: 1486069567
title: Pure Numbers and Significant Digits
tags:
    - Pure
    - Numbers
    - and
    - Significant
    - Digits
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ym5u5IlYWcg/default.jpg
    - https://i3.ytimg.com/vi/Ym5u5IlYWcg/1.jpg
    - https://i3.ytimg.com/vi/Ym5u5IlYWcg/2.jpg
    - https://i3.ytimg.com/vi/Ym5u5IlYWcg/3.jpg
---
Pure Numbers and Significant Digits
