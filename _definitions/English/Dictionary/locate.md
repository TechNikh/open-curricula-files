---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/locate
offline_file: ""
offline_thumbnail: ""
uuid: 4955245a-f9b8-43b2-8a2a-d7227ca2618c
updated: 1484310208
title: locate
categories:
    - Dictionary
---
locate
     v 1: discover the location of; determine the place of; find by
          searching or examining; "Can you locate your cousins in
          the Midwest?"; "My search turned up nothing" [syn: {turn
          up}]
     2: determine or indicate the place, site, or limits of, as if
        by an instrument or by a survey; "Our sense of sight
        enables us to locate objects in space"; "Locate the
        boundaries of the property" [syn: {situate}]
     3: assign a location to; "The company located some of their
        agents in Los Angeles" [syn: {place}, {site}]
     4: take up residence and become established; "The immigrants
        settled in the Midwest" [syn: {settle}]
