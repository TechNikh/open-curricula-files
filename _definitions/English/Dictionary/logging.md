---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logging
offline_file: ""
offline_thumbnail: ""
uuid: 962ccda2-f318-41a8-8f70-d06178d4456e
updated: 1484310246
title: logging
categories:
    - Dictionary
---
log
     n 1: a segment of the trunk of a tree when stripped of branches
     2: large log at the back of a hearth fire [syn: {backlog}]
     3: the exponent required to produce a given number [syn: {logarithm}]
     4: a written record of messages sent or received; "they kept a
        log of all transmission by the radio station"; "an email
        log"
     5: a written record of events on a voyage (of a ship or plane)
     6: measuring instrument that consists of a float that trails
        from a ship by a knotted line in order to measure the
        ship's speed through the water
     v 1: enter into a log, as on ships and planes
     2: cut lumber, as in woods and forests [syn: ...
