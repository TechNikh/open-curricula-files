---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_functions
offline_file: ""
offline_thumbnail: ""
uuid: adea6b24-4bd3-4edf-b679-aa1497f2fb99
updated: 1484309348
title: Trigonometric functions
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Academ_Base_of_trigonometry.svg.png
tags:
    - Right-angled triangle definitions
    - Sine, cosine and tangent
    - Reciprocal functions
    - Slope definitions
    - Unit-circle definitions
    - Algebraic values
    - Explicit values
    - Series definitions
    - Tangent
    - Cosecant
    - Secant
    - Cotangent
    - Relationship to exponential function and complex numbers
    - Complex graphs
    - Definitions via differential equations
    - The significance of radians
    - Identities
    - calculus
    - Definitions using functional equations
    - Computation
    - Special values in trigonometric functions
    - Inverse functions
    - Connection to the inner product
    - Properties and applications
    - Law of sines
    - Law of cosines
    - Law of tangents
    - Law of cotangents
    - Periodic functions
    - History
    - Etymology
    - Notes
categories:
    - Trigonometry
---
In mathematics, the trigonometric functions (also called the circular functions) are functions of an angle. They relate the angles of a triangle to the lengths of its sides. Trigonometric functions are important in the study of triangles and modeling periodic phenomena, among many other applications.
