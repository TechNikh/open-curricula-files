---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolutionarily_stable_strategy
offline_file: ""
offline_thumbnail: ""
uuid: 5a4e8c63-30d2-4ba3-a2aa-e2aa526b1363
updated: 1484308527
title: Evolutionarily stable strategy
tags:
    - History
    - Motivation
    - Nash equilibria and ESS
    - Examples of differences between Nash Equilibria and ESSes
    - ESS vs. Evolutionarily Stable State
    - Stochastic ESS
    - "Prisoner's dilemma and ESS"
    - ESS and human behavior
categories:
    - Evolutionary game theory
---
An evolutionarily stable strategy (ESS) is a strategy which, if adopted by a population in a given environment, cannot be invaded by any alternative strategy that is initially rare. It is relevant in game theory, behavioural ecology, and evolutionary psychology. An ESS is an equilibrium refinement of the Nash equilibrium. It is a Nash equilibrium that is "evolutionarily" stable: once it is fixed in a population, natural selection alone is sufficient to prevent alternative (mutant) strategies from invading successfully. The theory is not intended to deal with the possibility of gross external changes to the environment that bring new selective forces to bear.
