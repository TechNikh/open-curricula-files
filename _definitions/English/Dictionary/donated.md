---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/donated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559361
title: donated
categories:
    - Dictionary
---
donated
     adj : given freely especially to a cause or fund; "the donated van
           made their meal-on-wheels venture possible"
