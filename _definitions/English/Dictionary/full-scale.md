---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/full-scale
offline_file: ""
offline_thumbnail: ""
uuid: 15a8345d-61a7-4df6-84b2-6224892f70cb
updated: 1484310174
title: full-scale
categories:
    - Dictionary
---
full-scale
     adj : using all available resources; "all-out war"; "a full-scale
           campaign against nuclear power plants" [syn: {all-out}]
