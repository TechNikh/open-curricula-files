---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solid
offline_file: ""
offline_thumbnail: ""
uuid: 575049b1-9c44-46c0-8a36-6e0487794dd7
updated: 1484310230
title: solid
categories:
    - Dictionary
---
solid
     adj 1: of definite shape and volume; firm; neither liquid nor
            gaseous; "ice is water in the solid state" [ant: {liquid},
             {gaseous}]
     2: of good substantial quality; "solid comfort"; "a solid base
        hit"
     3: entirely of one substance with no holes inside; "solid
        silver"; "a solid block of wood" [ant: {hollow}]
     4: of one substance or character throughout; "solid gold"; "a
        solid color"; "carved out of solid rock"
     5: uninterrupted in space; having no gaps or breaks; "a solid
        line across the page"; "solid sheets of water"
     6: providing abundant nourishment; "a hearty meal"; "good solid
        food"; "ate a ...
