---
version: 1
type: video
provider: YouTube
id: aqsIWLqlDhE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Fractions/Number%20sets%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 321e9c04-248d-48cf-832e-ffbb5fea7cc8
updated: 1486069565
title: Number sets 1
categories:
    - Fractions
thumbnail_urls:
    - https://i3.ytimg.com/vi/aqsIWLqlDhE/default.jpg
    - https://i3.ytimg.com/vi/aqsIWLqlDhE/1.jpg
    - https://i3.ytimg.com/vi/aqsIWLqlDhE/2.jpg
    - https://i3.ytimg.com/vi/aqsIWLqlDhE/3.jpg
---
