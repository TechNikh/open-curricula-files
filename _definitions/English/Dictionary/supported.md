---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supported
offline_file: ""
offline_thumbnail: ""
uuid: 6d4c722f-53d6-4996-9aa7-fbb63b26e526
updated: 1484310266
title: supported
categories:
    - Dictionary
---
supported
     adj 1: sustained or maintained by aid (as distinct from physical
            support); "a club entirely supported by membership
            dues"; "well-supported allegations" [ant: {unsupported}]
     2: held up or having the weight borne especially from below;
        "supported joints in a railroad track have ties directly
        under the rail ends" [ant: {unsupported}, {unsupported}]
