---
version: 1
type: article
id: https://en.wikipedia.org/wiki/InfiniteGraph
offline_file: ""
offline_thumbnail: ""
uuid: e7a54fab-6e8b-4f9e-a4ad-460378fad4e4
updated: 1484309404
title: InfiniteGraph
tags:
    - features
    - History
categories:
    - Graph databases
---
InfiniteGraph is an enterprise distributed graph database implemented in Java, and is from a class of NOSQL (or Not Only SQL) database technologies that focus on graph data structures. Developers use Infinitegraph to find useful and often hidden relationships in highly connected big data sets.[1] [2] InfiniteGraph is cross-platform, scalable, cloud enabled, and is designed to handle very high throughput.[3] [4] [5]
