---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destroy
offline_file: ""
offline_thumbnail: ""
uuid: e358f0d6-86f8-4322-97e3-5bdeeb80338a
updated: 1484310246
title: destroy
categories:
    - Dictionary
---
destroy
     v 1: do away with, cause the destruction or undoing of; "The fire
          destroyed the house" [syn: {destruct}]
     2: destroy completely; damage irreparably; "You have ruined my
        car by pouring sugar in the tank!"; "The tears ruined her
        make-up" [syn: {ruin}]
     3: defeat soundly; "The home team demolished the visitors"
        [syn: {demolish}]
     4: as of animals; "The customs agents destroyed the dog that
        was found to be rabid"
