---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurrying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477341
title: hurrying
categories:
    - Dictionary
---
hurrying
     adj : moving with great haste; "affection for this hurrying
           driving...little man"; "lashed the scurrying horses"
           [syn: {scurrying}]
     n : changing location rapidly [syn: {speed}, {speeding}]
