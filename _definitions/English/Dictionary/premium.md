---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/premium
offline_file: ""
offline_thumbnail: ""
uuid: 85bfdbd0-dd24-4289-a1ff-df7c802d5a80
updated: 1484310517
title: premium
categories:
    - Dictionary
---
premium
     adj : having or reflecting superior quality or value; "premium
           gasoline at a premium price"
     n 1: payment for insurance [syn: {insurance premium}]
     2: a fee charged for exchanging currencies [syn: {agio}, {agiotage},
         {exchange premium}]
     3: payment or reward (especially from a government) for acts
        such as catching criminals or killing predatory animals or
        enlisting in the military [syn: {bounty}]
