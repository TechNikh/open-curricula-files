---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/goodness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619721
title: goodness
categories:
    - Dictionary
---
goodness
     n 1: that which is good or valuable or useful; "weigh the good
          against the bad"; "among the highest goods of all are
          happiness and self-realization" [syn: {good}] [ant: {bad},
           {bad}]
     2: moral excellence or admirableness; "there is much good to be
        found in people" [syn: {good}] [ant: {evil}, {evil}]
