---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exact_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 00256985-6773-4569-924e-fc43d8a1dbb2
updated: 1484309379
title: Exact coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Exact_coloring.svg.png
tags:
    - Complete graphs, detachments, and Euler tours
    - Related types of coloring
    - Computational complexity
categories:
    - Graph coloring
---
In graph theory, an exact coloring is a (proper) vertex coloring in which every pair of colors appears on exactly one pair of adjacent vertices. That is, it is a partition of the vertices of the graph into disjoint independent sets such that, for each pair of distinct independent sets in the partition, there is exactly one edge with endpoints in each set.[1][2]
