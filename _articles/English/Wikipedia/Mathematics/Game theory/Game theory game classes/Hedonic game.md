---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hedonic_game
offline_file: ""
offline_thumbnail: ""
uuid: f8a8dce1-2c20-4ac1-9e7f-111801eb4569
updated: 1484309035
title: Hedonic game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_hedonic_game_with_5_players_that_has_empty_core.png
tags:
    - Definition
    - Solution concepts
    - Examples
    - Concise representations and restricted preferences
    - Existence guarantees
    - Computational complexity
categories:
    - Game theory game classes
---
In cooperative game theory, a hedonic game[1][2] (also known as a (hedonic) coalition formation game) is a game that models the formation of coalitions (groups) of players when players have preferences over which group they belong to. A hedonic game is specified by giving a finite set of players, and, for each player, a preference ranking over all coalitions (subsets) of players that the player belongs to. The outcome of a hedonic game consists of a partition of the players into disjoint coalitions, that is, each player is assigned a unique group. Such partitions are often referred to as coalition structures.
