---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_cycle
offline_file: ""
offline_thumbnail: ""
uuid: 8559e17a-c189-497e-90b0-a0e0ac8d3312
updated: 1484308840
title: Thermodynamic cycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/PdV_work_cycle.gif
tags:
    - Heat and work
    - Relationship to work
    - Each Point in the Cycle
    - Power cycles
    - Heat pump cycles
    - Modelling real systems
    - Well-known thermodynamic cycles
    - Ideal cycle
    - Carnot cycle
    - Stirling cycle
    - State functions and entropy
categories:
    - Thermodynamics
---
A thermodynamic cycle consists of a linked sequence of thermodynamic processes that involve transfer of heat and work into and out of the system, while varying pressure, temperature, and other state variables within the system, and that eventually returns the system to its initial state.[1] In the process of passing through a cycle, the working fluid (system) may convert heat from a warm source into useful work, and dispose of the remaining heat to a cold sink, thereby acting as a heat engine. Conversely, the cycle may be reversed and use work to move heat from a cold source and transfer it to a warm sink thereby acting as a heat pump.
