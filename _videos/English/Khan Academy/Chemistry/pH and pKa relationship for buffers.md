---
version: 1
type: video
provider: YouTube
id: 7ZLjzp7thNI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/pH%20and%20pKa%20relationship%20for%20buffers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c3882acb-7427-4052-8af8-e70e82713c3f
updated: 1486069587
title: pH and pKa relationship for buffers
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/7ZLjzp7thNI/default.jpg
    - https://i3.ytimg.com/vi/7ZLjzp7thNI/1.jpg
    - https://i3.ytimg.com/vi/7ZLjzp7thNI/2.jpg
    - https://i3.ytimg.com/vi/7ZLjzp7thNI/3.jpg
---
How the Henderson-Hasselbalch equation can be used to look at the ratio of conjugate acid and base using relationship between buffer pH and pKa.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/buffer-solutions/v/buffer-capacity?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/buffer-solutions/v/buffer-solution-calculations?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
