---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belief
offline_file: ""
offline_thumbnail: ""
uuid: c094f968-4226-4b3d-9c6d-6df00cae2dbe
updated: 1484310553
title: belief
categories:
    - Dictionary
---
belief
     n 1: any cognitive content held as true [ant: {unbelief}]
     2: a vague idea in which some confidence is placed; "his
        impression of her was favorable"; "what are your feelings
        about the crisis?"; "it strengthened my belief in his
        sincerity"; "I had a feeling that she was lying" [syn: {impression},
         {feeling}, {notion}, {opinion}]
