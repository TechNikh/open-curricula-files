---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463301
title: honest
categories:
    - Dictionary
---
honest
     adj 1: not disposed to cheat or defraud; not deceptive or
            fraudulent; "honest lawyers"; "honest reporting"; "an
            honest wage"; "honest weight" [syn: {honorable}] [ant:
             {dishonest}]
     2: without dissimulation; frank; "my honest opinion"
     3: worthy of being depended on; "a dependable worker"; "an
        honest working stiff"; "a reliable source of information";
        "he was true to his word"; "I would be true for there are
        those who trust me" [syn: {dependable}, {reliable}, {true(p)}]
     4: free from guile; "his answer was simple and honest" [syn: {guileless}]
     5: without pretensions; "worked at an honest trade"; "good
  ...
