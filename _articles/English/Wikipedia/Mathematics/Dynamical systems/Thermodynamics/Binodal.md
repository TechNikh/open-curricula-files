---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binodal
offline_file: ""
offline_thumbnail: ""
uuid: 040773c8-97d7-4a8f-9336-08657337171a
updated: 1484308837
title: Binodal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-LCST-UCST_plot.svg_1.png
categories:
    - Thermodynamics
---
In thermodynamics, the binodal, also known as the coexistence curve or binodal curve, denotes the condition at which two distinct phases may coexist. Equivalently, it is the boundary between the set of conditions in which it is thermodynamically favorable for the system to be fully mixed and the set of conditions in which it is thermodynamically favorable for it to phase separate.[1] In general, the binodal is defined by the condition at which the chemical potential of all solution components is equal in each phase. The extremum of a binodal curve in temperature coincides with the one of the spinodal curve and is known as a critical point.
