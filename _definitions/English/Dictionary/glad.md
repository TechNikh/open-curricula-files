---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glad
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484457001
title: glad
categories:
    - Dictionary
---
glad
     adj 1: showing or causing joy and pleasure; especially made happy;
            "glad you are here"; "glad that they succeeded"; "gave
            a glad shout"; "a glad smile"; "heard the glad news";
            "a glad occasion" [ant: {sad}]
     2: (`lief' is archaic) very willing; "was lief to go"; "glad to
        help" [syn: {lief(p)}]
     3: feeling happy appreciation; "glad of the fire's warmth"
     4: cheerful and bright; "a beaming smile"; "a glad May morning"
        [syn: {beaming}]
     [also: {gladdest}, {gladder}]
