---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Williams_(geometer)
offline_file: ""
offline_thumbnail: ""
uuid: 9674cdbe-ac80-4dfd-81a1-d93c76b0eeea
updated: 1484309110
title: Robert Williams (geometer)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tnurseries.jpg
tags:
    - Biography—life, theories, and work
    - Environmental design work
    - Catenatic Geometry and Sacred Geometry
    - Catenatic Geometry
    - Sacred Geometry
    - Publications
    - U. S. Patent Office publications
categories:
    - Geometers
---
Robert Edward Williams (born 1942) is an American designer, mathematician, and architect. He is noted for books on the geometry of natural structure, the discovery of a new space-filling polyhedron, the development of theoretical principles of Catenatic Geometry, and the invention of the Ars-Vivant Wild-life Protector System for repopulating the Western Mojave Desert in California, USA with desert tortoises.
