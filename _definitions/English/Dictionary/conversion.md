---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conversion
offline_file: ""
offline_thumbnail: ""
uuid: 74dc07c2-b22d-4803-b24a-9af55f6b9d18
updated: 1484310264
title: conversion
categories:
    - Dictionary
---
conversion
     n 1: an event that results in a transformation [syn: {transition},
           {changeover}]
     2: a change in the units or form of an expression: "conversion
        from Fahrenheit to Centigrade"
     3: a successful free throw or try for point after a touchdown
     4: a spiritual enlightenment causing a person to lead a new
        life [syn: {rebirth}, {spiritual rebirth}]
     5: (psychiatry) a defense mechanism represses emotional
        conflicts which are then converted into physical symptoms
        that have no organic basis
     6: a change of religion; "his conversion to the Catholic faith"
     7: interchange of subject and predicate of a proposition
     8: ...
