---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ductility
offline_file: ""
offline_thumbnail: ""
uuid: 882194fa-a61e-4699-a445-44995a34084f
updated: 1484308389
title: Ductility
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Al_tensile_test.jpg
tags:
    - Materials science
    - Ductile–brittle transition temperature
categories:
    - Uncategorized
---
In materials science, ductility is a solid material's ability to deform under tensile stress; this is often characterized by the material's ability to be stretched into a wire. Malleability, a similar property, is a material's ability to deform under compressive stress; this is often characterized by the material's ability to form a thin sheet by hammering or rolling. Both of these mechanical properties are aspects of plasticity, the extent to which a solid material can be plastically deformed without fracture. Also, these material properties are dependent on temperature and pressure (investigated by Percy Williams Bridgman as part of his Nobel Prize-winning work on high pressures).
