---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modernization
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613061
title: modernization
categories:
    - Dictionary
---
modernization
     n 1: making modern in appearance or behavior; "the modernization
          of Nigeria will be a long process" [syn: {modernisation}]
     2: a modernized version (as of a play)
