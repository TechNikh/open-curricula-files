---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indication
offline_file: ""
offline_thumbnail: ""
uuid: 0f37dc60-31fd-453f-ae79-3a41e543e48b
updated: 1484310384
title: indication
categories:
    - Dictionary
---
indication
     n 1: something that serves to indicate or suggest; "an indication
          of foul play"; "indications of strain"; "symptoms are
          the prime indicants of disease" [syn: {indicant}]
     2: the act of indicating or pointing out by name [syn: {denotation}]
     3: (medicine) a reason to prescribe a drug or perform a
        procedure; "the presence of bacterial infection was an
        indication for the use of antibiotics" [ant: {contraindication}]
     4: something (as a course of action) that is indicated as
        expedient or necessary; "there were indications that it
        was time to leave"
     5: a datum about some physical state that is presented to a
    ...
