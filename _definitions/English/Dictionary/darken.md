---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/darken
offline_file: ""
offline_thumbnail: ""
uuid: 39575a16-d315-42c4-9e12-6efceabc2156
updated: 1484310148
title: darken
categories:
    - Dictionary
---
darken
     v 1: become dark or darker; "The sky darkened" [ant: {brighten}]
     2: become or make darker; "The screen darkend"; "He darkened
        the colors by adding brown" [syn: {dim}] [ant: {brighten}]
     3: tarnish or stain; "a scandal that darkened the family's good
        name"
     4: make dark or darker; "darken a room" [ant: {brighten}]
