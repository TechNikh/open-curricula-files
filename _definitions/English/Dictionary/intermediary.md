---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intermediary
offline_file: ""
offline_thumbnail: ""
uuid: 598933ef-ead9-4c03-93df-87370ffea111
updated: 1484310453
title: intermediary
categories:
    - Dictionary
---
intermediary
     n : a negotiator who acts as a link between parties [syn: {mediator},
          {go-between}, {intermediator}, {intercessor}]
