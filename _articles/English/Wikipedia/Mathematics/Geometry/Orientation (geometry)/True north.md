---
version: 1
type: article
id: https://en.wikipedia.org/wiki/True_north
offline_file: ""
offline_thumbnail: ""
uuid: 0409bc37-e0b0-47e3-aaaf-2044436cc95a
updated: 1484309302
title: True north
categories:
    - Orientation (geometry)
---
True geodetic north differs from magnetic north (the direction a compass points toward the magnetic north pole), and from grid north (the direction northwards along the grid lines of a map projection). Geodetic true north also differs very slightly from astronomical true north (typically by a few arcseconds) because the local gravity may not point at the exact rotational axis of the earth.
