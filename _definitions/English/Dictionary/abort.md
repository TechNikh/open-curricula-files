---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abort
offline_file: ""
offline_thumbnail: ""
uuid: 1a5d6a6d-23cc-4a77-b1e2-70700b4ed96f
updated: 1484310471
title: abort
categories:
    - Dictionary
---
abort
     v 1: terminate before completion; "abort the mission"; "abort the
          process running on my computer"
     2: terminate a pregnancy by undergoing an abortion
