---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reliance
offline_file: ""
offline_thumbnail: ""
uuid: 2a20156d-a612-4286-ad67-a0becdf400ea
updated: 1484310240
title: reliance
categories:
    - Dictionary
---
reliance
     n 1: certainty based on past experience; "he wrote the paper with
          considerable reliance on the work of other scientists";
          "he put more trust in his own two legs than in the gun"
          [syn: {trust}]
     2: the state of relying on something
