---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/household
offline_file: ""
offline_thumbnail: ""
uuid: 08cbb158-bbb5-44d1-8b3d-cca8894b3e49
updated: 1484310264
title: household
categories:
    - Dictionary
---
household
     n : a social unit living together; "he moved his family to
         Virginia"; "It was a good Christian household"; "I waited
         until the whole house was asleep"; "the teacher asked how
         many people made up his home" [syn: {family}, {house}, {home},
          {menage}]
