---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atlantic
offline_file: ""
offline_thumbnail: ""
uuid: cc106eae-5589-473e-830c-3f8d47fec9f2
updated: 1484310180
title: atlantic
categories:
    - Dictionary
---
Atlantic
     adj : relating to or bordering the Atlantic Ocean; "Atlantic
           currents"
     n : the 2nd largest ocean; separates North and South America on
         the west from Europe and Africa on the east [syn: {Atlantic
         Ocean}]
