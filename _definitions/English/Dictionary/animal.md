---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/animal
offline_file: ""
offline_thumbnail: ""
uuid: 1f1b71fa-6580-48d0-95c6-1ea087763e2b
updated: 1484310344
title: animal
categories:
    - Dictionary
---
animal
     adj 1: of the appetites and passions of the body; "animal
            instincts"; "carnal knowledge"; "fleshly desire"; "a
            sensual delight in eating"; "music is the only sensual
            pleasure without vice" [syn: {animal(a)}, {carnal}, {fleshly},
             {sensual}]
     2: of the nature of or characteristic of or derived from an
        animal or animals; "the animal kingdom"; "animal
        instincts"; "animal fats" [ant: {vegetable}, {mineral}]
     n : a living organism characterized by voluntary movement [syn:
         {animate being}, {beast}, {brute}, {creature}, {fauna}]
