---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/memorial
offline_file: ""
offline_thumbnail: ""
uuid: 070446d9-b52e-4034-9a41-b2d450b6938d
updated: 1484310543
title: memorial
categories:
    - Dictionary
---
memorial
     n 1: a recognition of meritorious service [syn: {commemoration},
          {remembrance}]
     2: a written statement of facts submitted in conjunction with a
        petition to an authority
     3: a structure erected to commemorate persons or events [syn: {monument}]
