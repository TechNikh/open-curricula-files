---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emerging
offline_file: ""
offline_thumbnail: ""
uuid: 6156c2e1-7d16-4515-bbba-ae70c1b2ff2f
updated: 1484310459
title: emerging
categories:
    - Dictionary
---
emerging
     adj 1: coming into view; "as newly emerging emotions and ideas well
            up in him"
     2: coming into existence; "a nascent republic" [syn: {emergent},
         {nascent}]
     3: coming to maturity; "the rising generation" [syn: {rising}]
