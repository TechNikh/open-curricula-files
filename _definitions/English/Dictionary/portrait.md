---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/portrait
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608321
title: portrait
categories:
    - Dictionary
---
portrait
     n 1: a painting of a person's face
     2: a word picture of a person's appearance and character [syn:
        {portrayal}, {portraiture}]
     3: any likeness of a person; "the photographer made excellent
        portraits" [syn: {portrayal}]
