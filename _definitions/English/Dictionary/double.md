---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/double
offline_file: ""
offline_thumbnail: ""
uuid: d230c9e1-3803-4df6-8046-ae137d725035
updated: 1484310295
title: double
categories:
    - Dictionary
---
double
     adj 1: having more than one decidedly dissimilar aspects or
            qualities; "a double (or dual) role for an actor";
            "the office of a clergyman is twofold; public
            preaching and private influence"- R.W.Emerson; "every
            episode has its double and treble meaning"-Frederick
            Harrison [syn: {dual}, {twofold}, {treble}, {threefold}]
     2: consisting of or involving two parts or components usually
        in pairs; "an egg with a double yolk"; "a double (binary)
        star"; "double doors"; "dual controls for pilot and
        copilot"; "duple (or double) time consists of two (or a
        multiple of two) beats to a measure" ...
