---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pipes
offline_file: ""
offline_thumbnail: ""
uuid: 90c0ee31-ede7-4b2b-9aa4-c1b6ec696c02
updated: 1484310249
title: pipes
categories:
    - Dictionary
---
pipes
     n : a wind instrument; the player blows air into a bag and
         squeezes it out through pipes [syn: {bagpipe}]
