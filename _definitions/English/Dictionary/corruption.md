---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corruption
offline_file: ""
offline_thumbnail: ""
uuid: d1f31f76-9113-40e6-94d7-3f3326bb7d6f
updated: 1484310477
title: corruption
categories:
    - Dictionary
---
corruption
     n 1: lack of integrity or honesty (especially susceptibility to
          bribery); use of a position of trust for dishonest gain
          [syn: {corruptness}] [ant: {incorruptness}]
     2: in a state of progressive putrefaction [syn: {putrescence},
        {putridness}, {rottenness}]
     3: decay of matter (as by rot or oxidation)
     4: moral perversion; impairment of virtue and moral principles;
        "the luxury and corruption among the upper classes";
        "moral degeneracy followed intellectual degeneration";
        "its brothels; its opium parlors; its depravity" [syn: {degeneracy},
         {depravity}]
     5: destroying someone's (or some group's) honesty ...
