---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sauce
offline_file: ""
offline_thumbnail: ""
uuid: 83578526-576d-49ab-89e1-a69773a3486f
updated: 1484310221
title: sauce
categories:
    - Dictionary
---
sauce
     n : flavorful relish or dressing or topping served as an
         accompaniment to food
     v 1: behave saucy or impudently towards
     2: dress (food) with a relish
     3: add zest or flavor to, make more interesting; "sauce the
        roast"
