---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_group
offline_file: ""
offline_thumbnail: ""
uuid: c7f56255-f8ec-489f-af23-7e78224d38d2
updated: 1484309311
title: Lie group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Circle_as_Lie_group.svg.png
tags:
    - Overview
    - Definitions and examples
    - First examples
    - Related concepts
    - More examples of Lie groups
    - Examples with a specific number of dimensions
    - Examples with n dimensions
    - Constructions
    - Related notions
    - Basic concepts
    - The Lie algebra associated with a Lie group
    - Homomorphisms and isomorphisms
    - The exponential map
    - Lie subgroup
    - Early history
    - >
        The concept of a Lie group, and possibilities of
        classification
    - Infinite-dimensional Lie groups
    - Notes
    - Explanatory notes
    - Citations
categories:
    - Symmetry
---
In mathematics, a Lie group /ˈliː/ is a group that is also a differentiable manifold, with the property that the group operations are compatible with the smooth structure. Lie groups are named after Sophus Lie, who laid the foundations of the theory of continuous transformation groups. The term groupes de Lie first appeared in French in 1893 in the thesis of Lie’s student Arthur Tresse.[1]
