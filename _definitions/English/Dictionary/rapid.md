---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rapid
offline_file: ""
offline_thumbnail: ""
uuid: 2e341b9a-c3e3-473f-93d6-9ee07de29729
updated: 1484310366
title: rapid
categories:
    - Dictionary
---
rapid
     adj 1: done or occurring in a brief period of time; "a rapid rise
            through the ranks"
     2: characterized by speed; moving with or capable of moving
        with high speed; "a rapid movment"; "a speedy car"; "a
        speedy errand boy" [syn: {speedy}]
     n : a part of a river where the current is very fast
