---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/projector
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484536261
title: projector
categories:
    - Dictionary
---
projector
     n 1: an optical device for projecting a beam of light
     2: an optical instrument that projects an enlarged image onto a
        screen
