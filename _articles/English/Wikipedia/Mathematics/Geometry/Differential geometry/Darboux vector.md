---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Darboux_vector
offline_file: ""
offline_thumbnail: ""
uuid: a54f3bcd-3ca1-4aa5-9977-e2bea077468e
updated: 1484309215
title: Darboux vector
categories:
    - Differential geometry
---
In differential geometry, especially the theory of space curves, the Darboux vector is the angular velocity vector of the Frenet frame of a space curve.[1] It is named after Gaston Darboux who discovered it.[2] It is also called angular momentum vector, because it is directly proportional to angular momentum.
