---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interchange
offline_file: ""
offline_thumbnail: ""
uuid: d39d2cb6-f9ca-4f5c-990f-591181711786
updated: 1484310204
title: interchange
categories:
    - Dictionary
---
interchange
     n 1: a junction of highways on different levels that permits
          traffic to move from one to another without crossing
          traffic streams
     2: mutual interaction; the activity of interchanging or
        reciprocating [syn: {reciprocation}, {give-and-take}]
     3: reciprocal transfer of equivalent sums of money especially
        the currencies of different countries; "he earns his
        living from the interchange of currency" [syn: {exchange}]
     v 1: give to, and receive from, one another; "Would you change
          places with me?"; "We have been exchanging letters for a
          year" [syn: {exchange}, {change}]
     2: cause to change places; ...
