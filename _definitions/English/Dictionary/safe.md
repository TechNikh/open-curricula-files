---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/safe
offline_file: ""
offline_thumbnail: ""
uuid: 9633366f-1153-4ce7-bb14-50415bd6bdef
updated: 1484310443
title: safe
categories:
    - Dictionary
---
safe
     adj 1: free from danger or the risk of harm; "a safe trip"; "you
            will be safe here"; "a safe place"; "a safe bet" [ant:
             {dangerous}]
     2: of an undertaking
     3: having reached a base without being put out; "the runner was
        called safe when the baseman dropped the ball" [syn: {safe(p)}]
        [ant: {out(p)}]
     4: financially sound; "a good investment"; "a secure
        investment" [syn: {dependable}, {good}, {secure}]
     5: in safekeeping; "your secret is safe with me"
     n 1: strongbox where valuables can be kept safe
     2: a ventilated or refrigerated cupboard for securing
        provisions from pests
     3: contraceptive device ...
