---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monaco
offline_file: ""
offline_thumbnail: ""
uuid: abfa8500-1622-42e3-b7e2-afad2c6c97ad
updated: 1484310180
title: monaco
categories:
    - Dictionary
---
Monaco
     n : a constitutional monarchy in a tiny enclave on the French
         Riviera [syn: {Principality of Monaco}]
