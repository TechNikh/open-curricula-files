---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiple_description_coding
offline_file: ""
offline_thumbnail: ""
uuid: 3c31adbe-8934-4f89-bdc5-1674965cfc01
updated: 1484308562
title: Multiple description coding
categories:
    - Coding theory
---
Multiple description coding (MDC) is a coding technique that fragments a single media stream into n substreams (n ≥ 2) referred to as descriptions. The packets of each description are routed over multiple, (partially) disjoint paths. In order to decode the media stream, any description can be used, however, the quality improves with the number of descriptions received in parallel. The idea of MDC is to provide error resilience to media streams. Since an arbitrary subset of descriptions can be used to decode the original stream, network congestion or packet loss — which are common in best-effort networks such as the Internet — will not interrupt the stream but only cause a ...
