---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gustatory
offline_file: ""
offline_thumbnail: ""
uuid: e8b471f5-edd8-446a-853d-4f99b2f2feda
updated: 1484308377
title: Taste
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Taste_bud.svg.png
tags:
    - Basic tastes
    - Sweetness
    - Sourness
    - Saltiness
    - Bitterness
    - Umami
    - Measuring relative tastes
    - Functional structure
    - Further sensations and transmission
    - Pungency (also spiciness or hotness)
    - Coolness
    - Numbness
    - Astringency
    - Metallicness
    - Calcium
    - Fattiness (oleogustus)
    - Heartiness (kokumi)
    - Temperature
    - Starchiness
    - Nerve supply and neural connections
    - Other concepts
    - Supertasters
    - Aftertaste
    - Acquired taste
    - Clinical significance
    - Disorders of taste
    - History
    - Research
    - Notes
    - Footnotes
    - Citations
categories:
    - Uncategorized
---
Taste is the sensation produced when a substance in the mouth reacts chemically with taste receptor cells located on taste buds in the oral cavity, mostly on the tongue. Taste, along with smell (olfaction) and trigeminal nerve stimulation (registering texture, pain, and temperature), determines flavors of food or other substances. Humans have taste receptors on taste buds (gustatory calyculi) and other areas including the upper surface of the tongue and the epiglottis.[2][3]
