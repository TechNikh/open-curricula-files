---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/river
offline_file: ""
offline_thumbnail: ""
uuid: f21053ca-2e49-4ed8-81b5-ad12d9cd11bf
updated: 1484310254
title: river
categories:
    - Dictionary
---
river
     n : a large natural stream of water (larger than a creek); "the
         river was navigable for 50 miles"
