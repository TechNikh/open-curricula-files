---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spread
offline_file: ""
offline_thumbnail: ""
uuid: a130193f-6a5b-44a7-9266-12764a882204
updated: 1484310343
title: spread
categories:
    - Dictionary
---
spread
     adj 1: distributed or spread over a considerable extent; "has ties
            with many widely dispersed friends"; "eleven million
            Jews are spread throughout Europe" [syn: {dispersed}]
     2: prepared or arranged for a meal; especially having food set
        out; "a table spread with food"
     3: fully extended in width; "outspread wings"; "with arms
        spread wide" [syn: {outspread}]
     n 1: process or result of distributing or extending over a wide
          expanse of space [syn: {spreading}]
     2: a conspicuous disparity or difference as between two
        figures; "gap between income and outgo"; "the spread
        between lending and borrowing ...
