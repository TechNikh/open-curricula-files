---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/globalisation
offline_file: ""
offline_thumbnail: ""
uuid: 3d6549e7-bc72-4bcd-88e0-63422c414cfa
updated: 1484310522
title: globalisation
categories:
    - Dictionary
---
globalisation
     n : growth to a global or worldwide scale; "the globalization of
         the communication industry" [syn: {globalization}]
