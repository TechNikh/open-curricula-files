---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Markov_partition
offline_file: ""
offline_thumbnail: ""
uuid: 837c30bb-081c-4d58-9110-8c3701ab3a8e
updated: 1484308832
title: Markov partition
tags:
    - Motivation
    - Formal definition
    - Examples
categories:
    - Symbolic dynamics
---
A Markov partition is a tool used in dynamical systems theory, allowing the methods of symbolic dynamics to be applied to the study of hyperbolic systems. By using a Markov partition, the system can be made to resemble a discrete-time Markov process, with the long-term dynamical characteristics of the system represented as a Markov shift. The appellation 'Markov' is appropriate because the resulting dynamics of the system obeys the Markov property. The Markov partition thus allows standard techniques from symbolic dynamics to be applied, including the computation of expectation values, correlations, topological entropy, topological zeta functions, Fredholm determinants and the like.
