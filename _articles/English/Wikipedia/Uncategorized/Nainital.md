---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nainital
offline_file: ""
offline_thumbnail: ""
uuid: 843eeccf-41e7-4a59-911b-2541c3fe1e57
updated: 1484308415
title: Nainital
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nainital2005.jpg
tags:
    - Geography and Climate
    - Demographics
    - Mythology
    - History
    - Early construction
    - The landslip of 1880
    - Establishment of schools
    - Libraries
    - Famous people
    - Gallery
    - Notes and references
categories:
    - Uncategorized
---
Nainital  pronunciation (help·info) is a popular hill station in the Indian state of Uttarakhand and headquarters of Nainital district in the Kumaon foothills of the outer Himalayas. Situated at an altitude of 2,084 metres (6,837 ft) above sea level, Nainital is set in a valley containing a pear-shaped lake, approximately two miles in circumference, and surrounded by mountains, of which the highest are Naina (2,615 m (8,579 ft)) on the north, Deopatha (2,438 m (7,999 ft)) on the west, and Ayarpatha (2,278 m (7,474 ft)) on the south. From the tops of the higher peaks, "magnificent views can be obtained of the vast plain to the south, or of the mass of tangled ridges lying north, ...
