---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gypsum
offline_file: ""
offline_thumbnail: ""
uuid: b791d909-1505-4213-928a-3943fa4b6371
updated: 1484310387
title: gypsum
categories:
    - Dictionary
---
gypsum
     n : a common white or colorless mineral (hydrated calcium
         sulphate) used to make cements and plasters (especially
         plaster of Paris)
