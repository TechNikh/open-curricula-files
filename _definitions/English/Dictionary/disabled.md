---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disabled
offline_file: ""
offline_thumbnail: ""
uuid: 6624c3c6-71e8-4648-a4f4-cf0737dec685
updated: 1484310565
title: disabled
categories:
    - Dictionary
---
disabled
     adj 1: incapacitated by injury or illness [syn: {handicapped}, {incapacitated}]
     2: so badly injured as to be unable to continue; "disabled
        veterans" [syn: {hors de combat}, {out of action}]
     n : people who are crippled or otherwise physically handicapped;
         "technology to help the elderly and the disabled"
