---
version: 1
type: video
provider: YouTube
id: o-ZbdYVGehI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Example%205-%20Factoring%20a%20fourth%20degree%20polynomial%20using%20the%20-perfect%20square%E2%80%9D%20form.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8b82066a-449e-443f-8d74-c0d4ce84b1e9
updated: 1486069559
title: 'Example 5- Factoring a fourth degree polynomial using the "perfect square” form'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/o-ZbdYVGehI/default.jpg
    - https://i3.ytimg.com/vi/o-ZbdYVGehI/1.jpg
    - https://i3.ytimg.com/vi/o-ZbdYVGehI/2.jpg
    - https://i3.ytimg.com/vi/o-ZbdYVGehI/3.jpg
---
