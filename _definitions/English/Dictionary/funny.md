---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funny
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412601
title: funny
categories:
    - Dictionary
---
funny
     adj 1: arousing or provoking laughter; "an amusing film with a
            steady stream of pranks and pratfalls"; "an amusing
            fellow"; "a comic hat"; "a comical look of surprise";
            "funny stories that made everybody laugh"; "a very
            funny writer"; "it would have been laughable if it
            hadn't hurt so much"; "a mirthful experience";
            "risible courtroom antics" [syn: {amusing}, {comic}, {comical},
             {laughable}, {mirthful}, {risible}]
     2: beyond or deviating from the usual or expected; "a curious
        hybrid accent"; "her speech has a funny twang"; "they have
        some funny ideas about war"; "had an odd ...
