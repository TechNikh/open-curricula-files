---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atom
offline_file: ""
offline_thumbnail: ""
uuid: b830fac3-0b2d-4d73-9e3d-8a15bae1c93d
updated: 1484310371
title: atom
categories:
    - Dictionary
---
atom
     n 1: (physics and chemistry) the smallest component of an element
          having the chemical properties of the element
     2: (nontechnical usage) a tiny piece of anything [syn: {molecule},
         {particle}, {corpuscle}, {mote}, {speck}]
