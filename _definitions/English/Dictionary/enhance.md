---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enhance
offline_file: ""
offline_thumbnail: ""
uuid: d9a474bd-1af3-48a3-8b66-45accdd0b1e1
updated: 1484310353
title: enhance
categories:
    - Dictionary
---
enhance
     v 1: increase; "This will enhance your enjoyment"; "heighten the
          tension" [syn: {heighten}, {raise}]
     2: make better or more attractive; "This sauce will enhance the
        flavor of the meat"
