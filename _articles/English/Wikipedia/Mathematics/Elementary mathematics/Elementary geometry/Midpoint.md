---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Midpoint
offline_file: ""
offline_thumbnail: ""
uuid: 844420e4-ed4e-4809-b491-909305133195
updated: 1484308938
title: Midpoint
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/282px-Midpoint.svg.png
tags:
    - Formulas
    - Construction
    - Geometric properties involving midpoints
    - Circle
    - Ellipse
    - Hyperbola
    - Triangle
    - Quadrilateral
    - General polygons
    - Generalizations
categories:
    - Elementary geometry
---
In geometry, the midpoint is the middle point of a line segment. It is equidistant from both endpoints, and it is the centroid both of the segment and of the endpoints. It bisects the segment.
