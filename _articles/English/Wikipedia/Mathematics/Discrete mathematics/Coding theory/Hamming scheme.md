---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_scheme
offline_file: ""
offline_thumbnail: ""
uuid: 841a111a-689f-43e8-a7cb-94709c1604c9
updated: 1484308544
title: Hamming scheme
categories:
    - Coding theory
---
The Hamming scheme, named after Richard Hamming, is also known as the hyper-cubic association scheme, and it is the most important example for coding theory.[1][2][3] In this scheme 
  
    
      
        X
        =
        
          
            
              F
            
          
          
            n
          
        
      
    
    {\displaystyle X={\mathcal {F}}^{n}}
  
, the set of binary vectors of length 
  
    
      
        n
      
    
    {\displaystyle n}
  
, and two vectors 
  
    
      
        x
      
    
    {\displaystyle x}
  
, 
  
    
      
        y
        ∈
        
          
            
              F
            
          
          
  ...
