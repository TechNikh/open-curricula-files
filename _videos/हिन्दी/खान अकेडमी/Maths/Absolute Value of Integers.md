---
version: 1
type: video
provider: YouTube
id: Bwfbugxpw78
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7629a7f1-a1c7-44d4-85ff-27a6bf445ba2
updated: 1486069617
title: Absolute Value of Integers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Bwfbugxpw78/default.jpg
    - https://i3.ytimg.com/vi/Bwfbugxpw78/1.jpg
    - https://i3.ytimg.com/vi/Bwfbugxpw78/2.jpg
    - https://i3.ytimg.com/vi/Bwfbugxpw78/3.jpg
---
