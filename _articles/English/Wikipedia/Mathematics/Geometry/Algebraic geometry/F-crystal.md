---
version: 1
type: article
id: https://en.wikipedia.org/wiki/F-crystal
offline_file: ""
offline_thumbnail: ""
uuid: d4c04381-6cc3-4c56-a05b-1d30fdc14cac
updated: 1484309145
title: F-crystal
tags:
    - F-crystals and F-isocrystals over perfect fields
    - Dieudonné–Manin classification theorem
    - The Newton polygon of an F-isocrystal
    - The Hodge polygon of an F-crystal
    - Isocrystals over more general schemes
categories:
    - Algebraic geometry
---
In algebraic geometry, F-crystals are objects introduced by Mazur (1972) that capture some of the structure of crystalline cohomology groups. The letter F stands for Frobenius, indicating that F-crystals have an action of Frobenius on them. F-isocrystals are crystals "up to isogeny".
