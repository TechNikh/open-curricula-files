---
version: 1
type: article
id: https://en.wikipedia.org/wiki/42_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 45f03811-edbe-423c-8594-7a308b3fd3db
updated: 1484308946
title: 42 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Simple_Magic_Cube.svg.png
tags:
    - Mathematics
    - Science
    - Technology
    - Astronomy
    - religion
    - Popular culture
    - "The Hitchhiker's Guide to the Galaxy"
    - Works of Lewis Carroll
    - Music
    - Television and film
    - Video games
    - Sports
    - Gaming
    - Other fields
    - Other languages
categories:
    - Integers
---
