---
version: 1
type: video
provider: YouTube
id: iYOos9KTWkY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 10503490-3f1b-4ed4-91c0-e0ad5742ed75
updated: 1486069640
title: 'Physics 625 Lab - Schlieren Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/iYOos9KTWkY/default.jpg
    - https://i3.ytimg.com/vi/iYOos9KTWkY/1.jpg
    - https://i3.ytimg.com/vi/iYOos9KTWkY/2.jpg
    - https://i3.ytimg.com/vi/iYOos9KTWkY/3.jpg
---
