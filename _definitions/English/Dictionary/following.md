---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/following
offline_file: ""
offline_thumbnail: ""
uuid: f6eb52eb-f737-49fd-9284-7342977bc006
updated: 1484310357
title: following
categories:
    - Dictionary
---
following
     adj 1: going or proceeding or coming after in the same direction;
            "the crowd of following cars made the occasion seem
            like a parade"; "tried to outrun the following
            footsteps" [ant: {leading}]
     2: in the desired direction; "a following wind" [syn: {following(a)}]
     3: immediately following in time or order; "the following day";
        "next in line"; "the next president"; "the next item on
        the list" [syn: {next}]
     4: about to be mentioned or specified; "the following items"
        [syn: {following(a)}, {undermentioned}]
     n 1: a group of followers or enthusiasts [syn: {followers}]
     2: the act of pursuing in an ...
