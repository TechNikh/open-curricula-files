---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herr
offline_file: ""
offline_thumbnail: ""
uuid: f79d1258-292a-44c2-b636-5c393e6afa52
updated: 1484310583
title: herr
categories:
    - Dictionary
---
Herr
     n : a German man; used before the name as a title equivalent to
         Mr in English
