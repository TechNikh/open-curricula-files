---
version: 1
type: video
provider: YouTube
id: 1GhsJbWuPrU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d197920-1133-4f5c-9f06-2524cac7026f
updated: 1486069607
title: Constructing an equation for a proportional relationship
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/1GhsJbWuPrU/default.jpg
    - https://i3.ytimg.com/vi/1GhsJbWuPrU/1.jpg
    - https://i3.ytimg.com/vi/1GhsJbWuPrU/2.jpg
    - https://i3.ytimg.com/vi/1GhsJbWuPrU/3.jpg
---
