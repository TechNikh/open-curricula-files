---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/signing
offline_file: ""
offline_thumbnail: ""
uuid: 4fd39b50-eda9-4994-8db2-d316f2702366
updated: 1484310172
title: signing
categories:
    - Dictionary
---
signing
     n : language expressed by visible hand gestures [syn: {sign
         language}]
