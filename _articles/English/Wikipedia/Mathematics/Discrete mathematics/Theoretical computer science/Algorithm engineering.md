---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algorithm_engineering
offline_file: ""
offline_thumbnail: ""
uuid: 5d045618-44bd-46cb-9f8c-02b90cf75104
updated: 1484308669
title: Algorithm engineering
tags:
    - Origins
    - Difference to Algorithm Theory
    - Methodology
    - Realistic Models and Real Inputs
    - Design
    - Analysis
    - Implementation
    - Experiments
    - Application Engineering
    - Algorithm Libraries
    - Conferences
categories:
    - Theoretical computer science
---
Algorithm Engineering focuses on the design, analysis, implementation, optimization, profiling and experimental evaluation of computer algorithms, bridging the gap between algorithm theory and practical applications of algorithms in software engineering.[1] It is a general methodology for algorithmic research.[2]
