---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_network_zeta_function
offline_file: ""
offline_thumbnail: ""
uuid: a03e02bf-15e3-4fa7-81ff-da4727a3db5a
updated: 1484308599
title: Complex network zeta function
tags:
    - Definition
    - Properties
    - Values for discrete regular lattices
    - Random graph zeta function
categories:
    - Graph theory
---
Different definitions have been given for the dimension of a complex network or graph. For example, metric dimension is defined in terms of the resolving set for a graph. Dimension has also been defined based on the box covering method applied to graphs.[1] Here we describe the definition based on the complex network zeta function.[2] This generalises the definition based on the scaling property of the volume with distance.[3] The best definition depends on the application.
