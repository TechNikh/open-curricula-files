---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/learn
offline_file: ""
offline_thumbnail: ""
uuid: b40afc4b-d63a-46d6-95d8-3bbe00c249c2
updated: 1484310299
title: learn
categories:
    - Dictionary
---
learn
     v 1: acquire or gain knowledge or skills; "She learned dancing
          from her sister"; "I learned Sanskrit"; "Children
          acquire language at an amazing rate" [syn: {larn}, {acquire}]
     2: get to know or become aware of, usually accidentally; "I
        learned that she has two grown-up children"; "I see that
        you have been promoted" [syn: {hear}, {get word}, {get
        wind}, {pick up}, {find out}, {get a line}, {discover}, {see}]
     3: commit to memory; learn by heart; "Have you memorized your
        lines for the play yet?" [syn: {memorize}, {memorise}, {con}]
     4: be a student of a certain subject; "She is reading for the
        bar exam" [syn: ...
