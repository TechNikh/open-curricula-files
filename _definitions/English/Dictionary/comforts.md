---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comforts
offline_file: ""
offline_thumbnail: ""
uuid: 4c0ef6cb-d785-401b-99b7-e289288f8e58
updated: 1484096617
title: comforts
categories:
    - Dictionary
---
comforts
     n : things that make you comfortable and at ease; "all the
         comforts of home" [syn: {creature comforts}, {amenities},
          {conveniences}]
