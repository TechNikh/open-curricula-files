---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractal_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 10295fe0-efc2-44b8-9269-2ab08813161e
updated: 1484308709
title: Fractal analysis
tags:
    - Types of fractal analysis
    - Applications
categories:
    - Chaos theory
---
Fractal analysis is assessing fractal characteristics of data. It consists of several methods to assign a fractal dimension and other fractal characteristics to a dataset which may be a theoretical dataset or a pattern or signal extracted from phenomena including natural geometric objects, sound, market fluctuations,[1][2][3] heart rates,[4] digital images,[5] molecular motion, networks, etc. Fractal analysis is now widely used in all areas of science.[6] An important limitation of fractal analysis is that arriving at an empirically determined fractal dimension does not necessarily prove that a pattern is fractal; rather, other essential characteristics have to be considered.[7]
