---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/origin
offline_file: ""
offline_thumbnail: ""
uuid: 97b563da-3d39-4f9b-b987-ff17c6c38a19
updated: 1484310284
title: origin
categories:
    - Dictionary
---
origin
     n 1: the place where something begins, where it springs into
          being; "the Italian beginning of the Renaissance";
          "Jupiter was the origin of the radiation"; "Pittsburgh
          is the source of the Ohio River"; "communism's Russian
          root" [syn: {beginning}, {root}, {rootage}, {source}]
     2: properties attributable to your ancestry; "he comes from
        good origins" [syn: {descent}, {extraction}]
     3: an event that is a beginning; a first part or stage of
        subsequent events [syn: {origination}, {inception}]
     4: the point of intersection of coordinate axes; where the
        values of the coordinates are all zero
     5: the ...
