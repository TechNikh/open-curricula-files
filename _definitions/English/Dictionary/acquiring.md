---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acquiring
offline_file: ""
offline_thumbnail: ""
uuid: 3332ba98-681e-4e64-8958-2725820853d2
updated: 1484310279
title: acquiring
categories:
    - Dictionary
---
acquiring
     n : the act of acquiring something; "I envied his talent for
         acquiring"; "he's much more interested in the getting
         than in the giving" [syn: {getting}]
