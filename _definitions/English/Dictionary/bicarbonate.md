---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bicarbonate
offline_file: ""
offline_thumbnail: ""
uuid: 73f80260-613d-4d82-9b72-63582d7354a9
updated: 1484310429
title: bicarbonate
categories:
    - Dictionary
---
bicarbonate
     n : a salt of carbonic acid (containing the anion HCO3) in which
         one hydrogen atom has been replaced; an acid carbonate
         [syn: {hydrogen carbonate}]
