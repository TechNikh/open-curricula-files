---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amount
offline_file: ""
offline_thumbnail: ""
uuid: 7d78b2b0-1d6f-4f02-8062-9d81a10aabf6
updated: 1484310337
title: amount
categories:
    - Dictionary
---
amount
     n 1: how much of something is available; "an adequate amount of
          food for four people"
     2: a quantity of money; "he borrowed a large sum"; "the amount
        he had in cash was insufficient" [syn: {sum}, {sum of
        money}, {amount of money}]
     3: how much there is of something that you can quantify [syn: {measure},
         {quantity}]
     4: a quantity obtained by addition [syn: {sum}, {total}]
     v 1: be tantamount or equivalent to; "Her action amounted to a
          rebellion"
     2: add up in number or quantity; "The bills amounted to
        $2,000"; "The bill came to $2,000" [syn: {total}, {number},
         {add up}, {come}]
     3: develop ...
