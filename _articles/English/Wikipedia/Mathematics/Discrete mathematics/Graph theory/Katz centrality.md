---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Katz_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 454c084c-5aee-470f-9939-c34f5b466adb
updated: 1484308630
title: Katz centrality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Katz_example_net.png
tags:
    - Measuring Katz centrality
    - Mathematical formulation
    - Applications
categories:
    - Graph theory
---
In graph theory, the Katz centrality of a node is a measure of centrality in a network. It was introduced by Leo Katz in 1953 and is used to measure the relative degree of influence of an actor (or node) within a social network.[1] Unlike typical centrality measures which consider only the shortest path (the geodesic) between a pair of actors, Katz centrality measures influence by taking into account the total number of walks between a pair of actors.[2]
