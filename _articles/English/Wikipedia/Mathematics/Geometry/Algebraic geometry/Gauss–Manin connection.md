---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gauss%E2%80%93Manin_connection'
offline_file: ""
offline_thumbnail: ""
uuid: 53d017b4-95c5-4637-8fb4-38401aa917da
updated: 1484309147
title: Gauss–Manin connection
tags:
    - Example
    - D-module explanation
    - Equations "arising from geometry"
categories:
    - Algebraic geometry
---
In mathematics, the Gauss–Manin connection is a connection on a certain vector bundle over a base space S of a family of algebraic varieties Vs. The fibers of the vector bundle are the de Rham cohomology groups 
  
    
      
        
          H
          
            D
            R
          
          
            k
          
        
        (
        
          V
          
            s
          
        
        )
      
    
    {\displaystyle H_{DR}^{k}(V_{s})}
  
 of the fibers Vs of the family. It was introduced by Manin (1958) for curves S and by Grothendieck (1966) in higher dimensions.
