---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Motion_planning
offline_file: ""
offline_thumbnail: ""
uuid: 7b0bbf91-57fc-4590-be73-74c72ac494f5
updated: 1484308674
title: Motion planning
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Motion_planning_workspace_1.svg.png
tags:
    - concepts
    - Configuration Space
    - Free Space
    - Target Space
    - Algorithms
    - Grid-Based Search
    - Interval-Based Search
    - Geometric Algorithms
    - Reward-Based Algorithms
    - Artificial Potential Fields
    - Sampling-Based Algorithms
    - List of notable algorithms
    - Completeness and Performance
    - Problem Variants
    - Differential Constraints
    - Optimality Constraints
    - Hybrid Systems
    - Uncertainty
    - Applications
categories:
    - Theoretical computer science
---
Motion planning (also known as the navigation problem or the piano mover's problem) is a term used in robotics for the process of breaking down a desired movement task into discrete motions that satisfy movement constraints and possibly optimize some aspect of the movement.
