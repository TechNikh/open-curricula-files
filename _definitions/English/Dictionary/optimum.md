---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optimum
offline_file: ""
offline_thumbnail: ""
uuid: 4d16446e-40d2-453c-ac25-d0949e868cdb
updated: 1484310256
title: optimum
categories:
    - Dictionary
---
optimum
     adj : most desirable possible under a restriction expressed or
           implied; "an optimum return on capital"; "optimal
           concentration of a drug" [syn: {optimal}]
     n : most favorable condition or greatest degree or amount
         possible under given circumstances
     [also: {optima} (pl)]
