---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judicious
offline_file: ""
offline_thumbnail: ""
uuid: 15b9f58b-5da8-4aaa-94dc-8fcaebe1ac17
updated: 1484310464
title: judicious
categories:
    - Dictionary
---
judicious
     adj 1: characterized by good judgment or sound thinking; "judicious
            journalism"
     2: marked by the exercise of good judgment or common sense in
        practical matters; "judicious use of one's money"; "a
        sensible manager"; "a wise decision" [syn: {sensible}, {wise}]
     3: proceeding from good sense or judgment; "a sensible choice"
        [syn: {sensible}]
