---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decolonisation
offline_file: ""
offline_thumbnail: ""
uuid: 97bca270-1fc5-4fb8-a777-cb7964402d25
updated: 1484310557
title: decolonisation
categories:
    - Dictionary
---
decolonisation
     n : the action of changing from colonial to independent status
         [syn: {decolonization}]
