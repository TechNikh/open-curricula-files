---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/former
offline_file: ""
offline_thumbnail: ""
uuid: fd30f9b8-2685-4d42-9af7-ae81aef53f4a
updated: 1484310559
title: former
categories:
    - Dictionary
---
former
     adj 1: referring to the first of two things or persons mentioned
            (or the earlier one or ones of several); "the novel
            was made into a film in 1943 and again in 1967; I
            prefer the former version to the latter one" [syn: {former(a)}]
            [ant: {latter(a)}]
     2: belonging to some prior time; "erstwhile friend"; "our
        former glory"; "the once capital of the state"; "her
        quondam lover" [syn: {erstwhile(a)}, {former(a)}, {once(a)},
         {onetime(a)}, {quondam(a)}, {sometime(a)}]
     3: (used especially of persons) of the immediate past; "the
        former president"; "our late President is still very
        active"; ...
