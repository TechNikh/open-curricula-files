---
version: 1
type: video
provider: YouTube
id: TLRZAFU_9Kg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Passing%20Gases-%20Effusion%2C%20Diffusion%20and%20the%20Velocity%20of%20a%20Gas%20-%20Crash%20Course%20Chemistry%20%2316.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 98a117c7-5929-439f-8bbb-37fe7e951e83
updated: 1486069680
title: 'Passing Gases- Effusion, Diffusion and the Velocity of a Gas - Crash Course Chemistry #16'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/TLRZAFU_9Kg/default.jpg
    - https://i3.ytimg.com/vi/TLRZAFU_9Kg/1.jpg
    - https://i3.ytimg.com/vi/TLRZAFU_9Kg/2.jpg
    - https://i3.ytimg.com/vi/TLRZAFU_9Kg/3.jpg
---
