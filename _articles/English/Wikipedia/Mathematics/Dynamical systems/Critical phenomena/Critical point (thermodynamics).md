---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Critical_point_(thermodynamics)
offline_file: ""
offline_thumbnail: ""
uuid: 1ce53942-48ad-466e-a2c5-4daa0dc73077
updated: 1484308746
title: Critical point (thermodynamics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-CriticalPointMeasurementEthane.jpg
tags:
    - Liquid-vapor critical point
    - Overview
    - History
    - Theory
    - >
        Table of liquid–vapor critical temperature and pressure
        for selected substances
    - 'Mixtures: liquid–liquid critical point'
    - Mathematical definition
    - In renormalization group theory
    - Footnotes
categories:
    - Critical phenomena
---
In thermodynamics, a critical point (or critical state) is the end point of a phase equilibrium curve. The most prominent example is the liquid-vapor critical point, the end point of the pressure-temperature curve that designates conditions under which a liquid and its vapor can coexist. At the critical point, defined by a critical temperature Tc and a critical pressure pc, phase boundaries vanish. Other examples include the liquid–liquid critical points in mixtures.
