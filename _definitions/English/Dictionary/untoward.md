---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/untoward
offline_file: ""
offline_thumbnail: ""
uuid: 91d98e50-fa21-495a-bb86-db697dc85b1e
updated: 1484310189
title: untoward
categories:
    - Dictionary
---
untoward
     adj 1: not in keeping with accepted standards of what is right or
            proper in polite society; "was buried with indecent
            haste"; "indecorous behavior"; "language unbecoming to
            a lady"; "unseemly to use profanity"; "moved to curb
            their untoward ribaldry" [syn: {indecent}, {indecorous},
             {unbecoming}, {uncomely}, {unseemly}]
     2: contrary to your interests or welfare; "adverse
        circumstances"; "made a place for themselves under the
        most untoward conditions" [syn: {adverse}, {harmful}, {inauspicious}]
