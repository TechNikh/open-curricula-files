---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perennial
offline_file: ""
offline_thumbnail: ""
uuid: fb9bf776-c575-45eb-967b-93e9a9638b69
updated: 1484310433
title: perennial
categories:
    - Dictionary
---
perennial
     adj 1: lasting three seasons or more; "the common buttercup is a
            popular perennial plant" [ant: {annual}, {biennial}]
     2: lasting an indefinitely long time; suggesting self-renewal;
        "perennial happiness"
     3: recurring again and again; "perennial efforts to stipulate
        the requirements" [syn: {recurrent}, {repeated}]
     n : a plant lasting for three seasons or more
