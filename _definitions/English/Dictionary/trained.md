---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trained
offline_file: ""
offline_thumbnail: ""
uuid: 0330dbe9-dc66-4ef5-8a41-87a09f35d411
updated: 1484310473
title: trained
categories:
    - Dictionary
---
trained
     adj 1: shaped or conditioned or disciplined by training; often used
            as a combining form; "a trained mind"; "trained
            pigeons"; "well-trained servants" [ant: {untrained}]
     2: having acquired necessary skills by e.g. undergoing a course
        of study; "a trained nurse"; "a trained voice"; "trained
        manpower"; "psychologically trained workers"
