---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fan_Chung
offline_file: ""
offline_thumbnail: ""
uuid: 8f780508-fbb9-40ef-90d4-eab265399a6a
updated: 1484309459
title: Fan Chung
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ronald_graham_couple_with_erdos_1986.jpg
tags:
    - life
    - Biography
    - Fan Chung and Bell Laboratories
    - Fan Chung and Ron Graham
    - Fan Chung and spectral graph theory
    - Awards and honors
    - Sources
categories:
    - Graph theorists
---
Fan-Rong King Chung Graham (Chinese: 金芳蓉; pinyin: Jīn Fāngróng; born October 9, 1949), known professionally as Fan Chung, is a mathematician who works mainly in the areas of spectral graph theory, extremal graph theory and random graphs, in particular in generalizing the Erdős–Rényi model for graphs with general degree distribution (including power-law graphs in the study of large information networks).
