---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Lower_critical_solution_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 952062d8-4451-49e3-8c1f-80744683f6e7
updated: 1484308751
title: Lower critical solution temperature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-LCST-UCST_plot.svg_0.png
tags:
    - Polymer-solvent mixtures
    - Physical basis
    - Theory
    - Prediction of θ (lower critical solution temperature) LCST
categories:
    - Critical phenomena
---
The lower critical solution temperature (LCST) or lower consolute temperature is the critical temperature below which the components of a mixture are miscible for all compositions.[1] The word lower indicates that the LCST is a lower bound to a temperature interval of partial miscibility, or miscibility for certain compositions only.
