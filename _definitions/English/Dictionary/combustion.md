---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/combustion
offline_file: ""
offline_thumbnail: ""
uuid: c1a10809-2cb7-4ecf-b801-137bbca52d76
updated: 1484310315
title: combustion
categories:
    - Dictionary
---
combustion
     n 1: a process in which a substance reacts with oxygen to give
          heat and light [syn: {burning}]
     2: a state of violent disturbance and excitement; "combustion
        grew until revolt was unavoidable"
     3: the act of burning something; "the burning of leaves was
        prohibited by a town ordinance" [syn: {burning}]
