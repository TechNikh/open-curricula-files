---
version: 1
type: video
provider: YouTube
id: dfoXtodyiIA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Parabola%20vertex%20and%20axis%20of%20symmetry.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8faee78a-dc15-4fcf-8ad9-eb0aa146f8ba
updated: 1486069557
title: Parabola vertex and axis of symmetry
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dfoXtodyiIA/default.jpg
    - https://i3.ytimg.com/vi/dfoXtodyiIA/1.jpg
    - https://i3.ytimg.com/vi/dfoXtodyiIA/2.jpg
    - https://i3.ytimg.com/vi/dfoXtodyiIA/3.jpg
---
