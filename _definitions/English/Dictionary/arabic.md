---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arabic
offline_file: ""
offline_thumbnail: ""
uuid: 61e6644e-87ec-440b-bc48-998f671f8673
updated: 1484310395
title: arabic
categories:
    - Dictionary
---
Arabic
     adj : relating to or characteristic of Arabs; "Arabic languages"
     n : the Semitic language of the Arabs; spoken in a variety of
         dialects [syn: {Arabic language}]
