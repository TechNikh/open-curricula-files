---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rejoinder
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484551081
title: rejoinder
categories:
    - Dictionary
---
rejoinder
     n 1: a quick reply to a question or remark (especially a witty or
          critical one); "it brought a sharp rejoinder from the
          teacher" [syn: {retort}, {return}, {riposte}, {replication},
           {comeback}, {counter}]
     2: (law) a pleading made by a defendant in response to the
        plaintiff's replication
