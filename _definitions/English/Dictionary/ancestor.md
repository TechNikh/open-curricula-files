---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ancestor
offline_file: ""
offline_thumbnail: ""
uuid: 7f6d56f6-8431-4c7e-80e4-71c45c5eafa3
updated: 1484310284
title: ancestor
categories:
    - Dictionary
---
ancestor
     n : someone from whom you are descended (but usually more remote
         than a grandparent) [syn: {ascendant}, {ascendent}, {antecedent},
          {root}] [ant: {descendant}]
