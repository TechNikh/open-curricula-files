---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lattice_plane
offline_file: ""
offline_thumbnail: ""
uuid: 5cd8b87b-faf2-4207-bdbf-45ec6c942542
updated: 1484309365
title: Lattice plane
categories:
    - Geometry stubs
---
In crystallography, a lattice plane of a given Bravais lattice is a plane (or family of parallel planes) whose intersections with the lattice (or any crystalline structure of that lattice) are periodic (i.e. are described by 2d Bravais lattices) and intersect the Bravais lattice; equivalently, a lattice plane is any plane containing at least three noncollinear Bravais lattice points.[1] All lattice planes can be described by a set of integer Miller indices, and vice versa (all integer Miller indices define lattice planes).
