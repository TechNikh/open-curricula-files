---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accelerate
offline_file: ""
offline_thumbnail: ""
uuid: df1fa99d-6418-454f-b27a-53560fa6c917
updated: 1484310202
title: accelerate
categories:
    - Dictionary
---
accelerate
     v 1: move faster; "The car accelerated" [syn: {speed up}, {speed},
           {quicken}] [ant: {decelerate}]
     2: cause to move faster; "He accelerated the car" [syn: {speed},
         {speed up}] [ant: {decelerate}]
