---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uniform
offline_file: ""
offline_thumbnail: ""
uuid: 94556894-ec82-41e0-91dc-8c80f6aab6dd
updated: 1484310286
title: uniform
categories:
    - Dictionary
---
uniform
     adj 1: always the same; showing a single form or character in all
            occurrences; "a street of uniform tall white
            buildings" [syn: {unvarying}] [ant: {multiform}]
     2: the same throughout in structure or composition; "bituminous
        coal is often treated as a consistent and homogeneous
        product" [syn: {consistent}]
     3: not differentiated [syn: {undifferentiated}] [ant: {differentiated}]
     4: evenly spaced; "at regular (or uniform) intervals"
     n : clothing of distinctive design worn by members of a
         particular group as a means of identification
     v : provide with uniforms; "The guards were uniformed"
