---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continuous
offline_file: ""
offline_thumbnail: ""
uuid: 37153cf7-a3a5-4756-bb4e-105086cf9f02
updated: 1484310290
title: continuous
categories:
    - Dictionary
---
continuous
     adj 1: continuing in time or space without interruption; "a
            continuous rearrangement of electrons in the solar
            atoms results in the emission of light"- James Jeans;
            "a continuous bout of illness lasting six months";
            "lived in continuous fear"; "a continuous row of
            warehouses"; "a continuous line has no gaps or breaks
            in it"; "moving midweek holidays to the nearest Monday
            or Friday allows uninterrupted work weeks" [syn: {uninterrupted}]
            [ant: {discontinuous}]
     2: of a function or curve; extending without break or
        irregularity [ant: {discontinuous}]
