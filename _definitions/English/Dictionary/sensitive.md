---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sensitive
offline_file: ""
offline_thumbnail: ""
uuid: 2495fb8e-0884-4401-bd81-b4745dd536d8
updated: 1484310349
title: sensitive
categories:
    - Dictionary
---
sensitive
     adj 1: responsive to physical stimuli; "a mimosa's leaves are
            sensitive to touch"; "a sensitive voltmeter";
            "sensitive skin"; "sensitive to light" [ant: {insensitive}]
     2: having acute mental or emotional sensibility; "sensitive to
        the local community and its needs" [ant: {insensitive}]
     3: able to feel or perceive; "even amoeba are sensible
        creatures"; "the more sensible p{ enveloping(a),
        shrouding(a), concealing,& (concealing by enclosing or
        wrapping as if in something that is not solid; "the
        enveloping darkness"; "hills concealed by shrouding
        mists") }arts of the skin" [syn: {sensible}] [ant: ...
