---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relief
offline_file: ""
offline_thumbnail: ""
uuid: 1df0f32b-4b90-46af-a702-2aba9517ce6f
updated: 1484310383
title: relief
categories:
    - Dictionary
---
relief
     n 1: the feeling that comes when something burdensome is removed
          or reduced; "as he heard the news he was suddenly
          flooded with relief" [syn: {alleviation}, {assuagement}]
     2: the condition of being comfortable or relieved (especially
        after being relieved of distress); "he enjoyed his relief
        from responsibility"; "getting it off his conscience gave
        him some ease" [syn: {ease}]
     3: (law) redress awarded by a court; "was the relief supposed
        to be protection from future harm or compensation for past
        injury?"
     4: someone who takes the place of another (as when things get
        dangerous or difficult); "the ...
