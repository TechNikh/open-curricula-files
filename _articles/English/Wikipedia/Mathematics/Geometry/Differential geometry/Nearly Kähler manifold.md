---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Nearly_K%C3%A4hler_manifold'
offline_file: ""
offline_thumbnail: ""
uuid: 4acb3012-fe14-48ae-8529-f9aa2562dee7
updated: 1484309230
title: Nearly Kähler manifold
categories:
    - Differential geometry
---
In mathematics, a nearly Kähler manifold is an almost Hermitian manifold 
  
    
      
        M
      
    
    {\displaystyle M}
  
, with almost complex structure 
  
    
      
        J
      
    
    {\displaystyle J}
  
, such that the (2,1)-tensor 
  
    
      
        ∇
        J
      
    
    {\displaystyle \nabla J}
  
 is skew-symmetric. So,
