---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tissue
offline_file: ""
offline_thumbnail: ""
uuid: 4c475a8b-97ec-4b48-b3b2-daf9936e1437
updated: 1484310343
title: tissue
categories:
    - Dictionary
---
tissue
     n 1: a part of an organism consisting of an aggregate of cells
          having a similar structure and function
     2: a soft thin (usually translucent) paper [syn: {tissue paper}]
     v : create a piece of cloth by interlacing strands of fabric,
         such as wool or cotton; "tissue textiles" [syn: {weave}]
