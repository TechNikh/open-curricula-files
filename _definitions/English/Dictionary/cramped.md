---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cramped
offline_file: ""
offline_thumbnail: ""
uuid: 02ab0990-cb05-4687-aaa8-8f7ca7e2a21c
updated: 1484310486
title: cramped
categories:
    - Dictionary
---
cramped
     adj : constricted in size; "cramped quarters"; "trying to bring
           children up in cramped high-rise apartments"
