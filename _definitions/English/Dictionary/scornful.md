---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scornful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499121
title: scornful
categories:
    - Dictionary
---
scornful
     adj 1: expressing extreme contempt [syn: {contemptuous}, {disdainful},
             {insulting}]
     2: expressing offensive reproach [syn: {abusive}, {insulting},
        {opprobrious}, {scurrilous}]
