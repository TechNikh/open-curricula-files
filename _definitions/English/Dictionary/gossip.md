---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gossip
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484577361
title: gossip
categories:
    - Dictionary
---
gossip
     n 1: light informal conversation for social occasions [syn: {chitchat},
           {small talk}, {gab}, {gabfest}, {tittle-tattle}, {chin-wag},
           {chin-wagging}, {causerie}]
     2: a report (often malicious) about the behavior of other
        people; "the divorce caused much gossip" [syn: {comment},
        {scuttlebutt}]
     3: a person given to gossiping and divulging personal
        information about others [syn: {gossiper}, {gossipmonger},
         {rumormonger}, {rumourmonger}, {newsmonger}]
     v 1: wag one's tongue; speak about others and reveal secrets or
          intimacies; "She won't dish the dirt" [syn: {dish the
          dirt}]
     2: talk socially ...
