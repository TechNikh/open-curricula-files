---
version: 1
type: video
provider: YouTube
id: Z3X6QqezqUg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%232%20%28a%20%26%20b%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bf2f8e2c-92eb-4746-9e77-7600a6895af1
updated: 1486069692
title: '2011 Calculus AB Free Response #2 (a & b)'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z3X6QqezqUg/default.jpg
    - https://i3.ytimg.com/vi/Z3X6QqezqUg/1.jpg
    - https://i3.ytimg.com/vi/Z3X6QqezqUg/2.jpg
    - https://i3.ytimg.com/vi/Z3X6QqezqUg/3.jpg
---
