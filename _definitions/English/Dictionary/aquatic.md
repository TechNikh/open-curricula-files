---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aquatic
offline_file: ""
offline_thumbnail: ""
uuid: c85daabe-70f2-4870-b33c-848a85784e88
updated: 1484310266
title: aquatic
categories:
    - Dictionary
---
aquatic
     adj 1: relating to or consisting of or being in water; "an aquatic
            environment"
     2: operating or living or growing in water; "boats are aquatic
        vehicles"; "water lilies are aquatic plants"; "fish are
        aquatic animals" [ant: {terrestrial}, {amphibious}]
     n : a plant that lives in or on water
