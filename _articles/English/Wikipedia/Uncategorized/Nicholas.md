---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nicholas
offline_file: ""
offline_thumbnail: ""
uuid: 2f19c40e-9a01-42ae-a119-1cda7b4db008
updated: 1484308479
title: Nicholas
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gheorghe_Tattarescu_-_Sfantul_Nicolae.jpg
tags:
    - Male variations
    - Female forms
    - People known as Nicholas
    - Single name (rulers, popes, patriarch and antipopes)
    - Given (first) name
    - Nobility
    - Saints
    - Surname
categories:
    - Uncategorized
---
Nicholas or Nikolas or Nicolas or Nickolas is a male given name, derived from the Greek name Νικόλαος (Nikolaos), a compound of νίκη nikē 'victory' and λαός; laos 'people', that is, victor of the people. The name became popular through Saint Nicholas, Bishop of Myra in Lycia, the inspiration for Santa Claus. The name Nikolaos (Νικόλαος) pre-existed the Bishop of Myra who became Saint Nicholas, by several centuries. The Athenian historian Thucydides mentions that in the second year of the Peloponnesian war (431 to 404 BC) between Sparta and Athens, the Spartans sent a delegation to the Persian king to ask for his help to fight the Athenians. He mentions the names of ...
