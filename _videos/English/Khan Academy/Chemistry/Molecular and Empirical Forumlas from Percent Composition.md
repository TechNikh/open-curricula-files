---
version: 1
type: video
provider: YouTube
id: _H009sTvYE0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Molecular%20and%20Empirical%20Forumlas%20from%20Percent%20Composition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2b21a8b2-a166-41dd-bbeb-5f1cf872be9a
updated: 1486069589
title: Molecular and Empirical Forumlas from Percent Composition
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/_H009sTvYE0/default.jpg
    - https://i3.ytimg.com/vi/_H009sTvYE0/1.jpg
    - https://i3.ytimg.com/vi/_H009sTvYE0/2.jpg
    - https://i3.ytimg.com/vi/_H009sTvYE0/3.jpg
---
Molecular and Empirical Forumlas from Percent Composition.  Example 2.9 from Kotz Chemistry book
More free lessons at: http://www.khanacademy.org/video?v=_H009sTvYE0
