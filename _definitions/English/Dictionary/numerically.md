---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/numerically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484316121
title: numerically
categories:
    - Dictionary
---
numerically
     adv : in number; with regard to numbers; "in ten years' time the
           Oxbridge mathematicians, scientists, and engineers will
           not be much more significant numerically than the
           Oxbridge medical schools are now"
