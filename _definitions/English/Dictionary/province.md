---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/province
offline_file: ""
offline_thumbnail: ""
uuid: a8a20d01-24a0-46ae-85fe-af1ea7e91be5
updated: 1484310593
title: province
categories:
    - Dictionary
---
province
     n 1: the territory occupied by one of the constituent
          administrative districts of a nation; "his state is in
          the deep south" [syn: {state}]
     2: the proper sphere or extent of your activities; "it was his
        province to take care of himself" [syn: {responsibility}]
