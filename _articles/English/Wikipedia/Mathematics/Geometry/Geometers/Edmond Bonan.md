---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edmond_Bonan
offline_file: ""
offline_thumbnail: ""
uuid: f452a170-0dab-4297-b2e7-f865a732d443
updated: 1484309071
title: Edmond Bonan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Edmond_Bonan.jpg
categories:
    - Geometers
---
Alumnus of the École polytechnique, Bonan completed in 1967 his doctoral dissertation [3] in Differential geometry at the University of Paris under the supervision of André Lichnerowicz. From 1968 to 1997, he held the post of lecturer and then professor at the University of Picardie Jules Verne in Amiens, currently professor emeritus. At the same time, from 1969 to 1981, he lectured at École Polytechnique.
