---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imported
offline_file: ""
offline_thumbnail: ""
uuid: 5d1d54f5-4e46-4b45-b935-122276a29236
updated: 1484310525
title: imported
categories:
    - Dictionary
---
imported
     adj : used of especially merchandise brought from a foreign
           source; "imported wines"
