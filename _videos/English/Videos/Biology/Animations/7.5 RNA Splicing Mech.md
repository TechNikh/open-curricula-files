---
version: 1
type: video
provider: YouTube
id: TRQEWSXPubU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/7.5%20RNA%20Splicing%20Mech.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ba88524b-104f-4bf3-b2ad-62d50321bcd8
updated: 1486069676
title: 7.5 RNA Splicing Mech
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TRQEWSXPubU/default.jpg
    - https://i3.ytimg.com/vi/TRQEWSXPubU/1.jpg
    - https://i3.ytimg.com/vi/TRQEWSXPubU/2.jpg
    - https://i3.ytimg.com/vi/TRQEWSXPubU/3.jpg
---
