---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manifestation
offline_file: ""
offline_thumbnail: ""
uuid: 5d60ed98-97fe-4e1f-a3d6-ef776e44960d
updated: 1484310549
title: manifestation
categories:
    - Dictionary
---
manifestation
     n 1: a clear appearance; "a manifestation of great emotion"
     2: a manifest indication of the existence or presence or nature
        of some person or thing; "a manifestation of disease"
     3: an appearance in bodily form (as of a disembodied spirit)
        [syn: {materialization}, {materialisation}]
     4: expression without words; "tears are an expression of
        grief"; "the pulse is a reflection of the heart's
        condition" [syn: {expression}, {reflection}, {reflexion}]
     5: a public display of group feelings (usually of a political
        nature); "there were violent demonstrations against the
        war" [syn: {demonstration}]
