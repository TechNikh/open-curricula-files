---
version: 1
type: video
provider: YouTube
id: Wl4re38deh0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Hubble%20image%20of%20galaxies.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c2e5e391-146b-4440-9a10-d9dd46f316f7
updated: 1486069688
title: Hubble image of galaxies
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wl4re38deh0/default.jpg
    - https://i3.ytimg.com/vi/Wl4re38deh0/1.jpg
    - https://i3.ytimg.com/vi/Wl4re38deh0/2.jpg
    - https://i3.ytimg.com/vi/Wl4re38deh0/3.jpg
---
