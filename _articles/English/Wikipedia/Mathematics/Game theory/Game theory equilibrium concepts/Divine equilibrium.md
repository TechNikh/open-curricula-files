---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divine_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 5df0f59b-191c-47b1-9671-ae0d58e0d3f7
updated: 1484309029
title: Divine equilibrium
categories:
    - Game theory equilibrium concepts
---
The Divinity Criterion or Divine Equilibrium or Universal Divinity or D1-Criterion is a refinement of Perfect Bayesian equilibrium in a signaling game.
