---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occasion
offline_file: ""
offline_thumbnail: ""
uuid: 1ecd268d-ddb5-4697-891e-7f0b3409d0d8
updated: 1484310186
title: occasion
categories:
    - Dictionary
---
occasion
     n 1: an event that occurs at a critical time; "at such junctures
          he always had an impulse to leave"; "it was needed only
          on special occasions" [syn: {juncture}]
     2: a vaguely specified social event; "the party was quite an
        affair"; "an occasion arranged to honor the president"; "a
        seemingly endless round of social functions" [syn: {affair},
         {social occasion}, {function}, {social function}]
     3: reason; "there was no occasion for complaint"
     4: the time of a particular event; "on the occasion of his 60th
        birthday"
     5: an opportunity to do something; "there was never an occasion
        for her to demonstrate ...
