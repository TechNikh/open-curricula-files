---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypothalamus
offline_file: ""
offline_thumbnail: ""
uuid: 2c15c286-e238-4bc0-8fa5-90f265c1f2e7
updated: 1484310162
title: hypothalamus
categories:
    - Dictionary
---
hypothalamus
     n : a basal part of the diencephalon governing autonomic nervous
         system
     [also: {hypothalami} (pl)]
