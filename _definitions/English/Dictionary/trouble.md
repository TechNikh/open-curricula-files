---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trouble
offline_file: ""
offline_thumbnail: ""
uuid: 5a663137-a5c6-422c-bfd4-127ae155f439
updated: 1484310517
title: trouble
categories:
    - Dictionary
---
trouble
     n 1: a source of difficulty; "one trouble after another delayed
          the job"; "what's the problem?" [syn: {problem}]
     2: an angry disturbance; "he didn't want to make a fuss"; "they
        had labor trouble"; "a spot of bother" [syn: {fuss}, {bother},
         {hassle}]
     3: an event causing distress or pain; "what is the trouble?";
        "heart trouble"
     4: an effort that is inconvenient; "I went to a lot of
        trouble"; "he won without any trouble"; "had difficulty
        walking"; "finished the test only with great difficulty"
        [syn: {difficulty}]
     5: a strong feeling of anxiety; "his worry over the prospect of
        being fired"; "it ...
