---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/duodenum
offline_file: ""
offline_thumbnail: ""
uuid: 0f676935-e022-42cc-b3f2-cbed1cae7168
updated: 1484310328
title: duodenum
categories:
    - Dictionary
---
duodenum
     n : the part of the small intestine between the stomach and the
         jejunum
     [also: {duonas} (pl), {duona} (pl)]
