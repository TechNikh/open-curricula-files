---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grateful
offline_file: ""
offline_thumbnail: ""
uuid: 662c756a-c8e0-41f4-a950-a76669ff35be
updated: 1484310583
title: grateful
categories:
    - Dictionary
---
grateful
     adj 1: feeling or showing gratitude; "a grateful heart"; "grateful
            for the tree's shade"; "a thankful smile" [syn: {thankful}]
            [ant: {ungrateful}]
     2: affording comfort or pleasure; "the grateful warmth of the
        fire"
