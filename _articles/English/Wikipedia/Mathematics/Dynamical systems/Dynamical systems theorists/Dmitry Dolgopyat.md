---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dmitry_Dolgopyat
offline_file: ""
offline_thumbnail: ""
uuid: 08bda341-2723-4188-9722-ff23331de57c
updated: 1484308683
title: Dmitry Dolgopyat
categories:
    - Dynamical systems theorists
---
From 1989 to 1994, he was an undergraduate student at Moscow State University.[1] From 1994 to 1997, he was enrolled in Princeton University, where he earned a PhD under the guidance of Yakov Sinai.[2]
