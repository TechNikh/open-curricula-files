---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computable_analysis
offline_file: ""
offline_thumbnail: ""
uuid: f3c187d1-bbb7-4bd6-b16e-b1d0093678b8
updated: 1484309483
title: Computable analysis
tags:
    - Basic constructions
    - Computable real numbers
    - Computable real functions
    - Basic Results
categories:
    - Computability theory
---
In mathematics and computer science, computable analysis is the study of mathematical analysis from the perspective of computability theory. It is concerned with the parts of real analysis and functional analysis that can be carried out in a computable manner. The field is closely related to constructive analysis and numerical analysis.
