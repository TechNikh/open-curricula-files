---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/small
offline_file: ""
offline_thumbnail: ""
uuid: 573045e7-ae90-4f62-8082-ea7991251987
updated: 1484310344
title: small
categories:
    - Dictionary
---
small
     adj 1: limited or below average in number or quantity or magnitude
            or extent; "a little dining room"; "a little house";
            "a small car"; "a little (or small) group"; "a small
            voice" [syn: {little}] [ant: {large}, {large}]
     2: limited in size or scope; "a small business"; "a newspaper
        with a modest circulation"; "small-scale plans"; "a
        pocket-size country" [syn: {minor}, {modest}, {small-scale},
         {pocket-size}, {pocket-sized}]
     3: low or inferior in station or quality; "a humble cottage";
        "a lowly parish priest"; "a modest man of the people";
        "small beginnings" [syn: {humble}, {low}, {lowly}, ...
