---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outermost
offline_file: ""
offline_thumbnail: ""
uuid: 4f8d30f8-07ba-4a70-bf6d-9cc177be08ca
updated: 1484310393
title: outermost
categories:
    - Dictionary
---
outermost
     adj : situated at the farthest possible point from a center [syn:
           {outmost}]
