---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_explosion
offline_file: ""
offline_thumbnail: ""
uuid: d71f8c92-38ee-45ac-b9e0-b9301b9bdf2b
updated: 1484308565
title: Combinatorial explosion
tags:
    - Examples
    - Computing
    - Arithmetics
    - Communication
categories:
    - Combinatorics
---
Examples of such functions include the factorial function and related functions. Pathological examples of combinatorial explosion include functions such as the Ackermann function.
