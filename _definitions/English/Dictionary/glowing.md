---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glowing
offline_file: ""
offline_thumbnail: ""
uuid: 6e8d7b1d-f51c-4f6b-9017-769dd700f7a2
updated: 1484310206
title: glowing
categories:
    - Dictionary
---
glowing
     adj 1: softly bright or radiant; "a house aglow with lights";
            "glowing embers"; "lambent tongues of flame"; "the
            lucent moon"; "a sky luminous with stars" [syn: {aglow(p)},
             {lambent}, {lucent}, {luminous}]
     2: highly enthusiastic; "glowing praise"
     n : the amount of electromagnetic radiation leaving or arriving
         at a point on a surface [syn: {radiance}, {glow}]
