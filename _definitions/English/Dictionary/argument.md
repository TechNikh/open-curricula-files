---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/argument
offline_file: ""
offline_thumbnail: ""
uuid: a58c8770-d53e-4dd0-afe9-547f068e596b
updated: 1484310277
title: argument
categories:
    - Dictionary
---
argument
     n 1: a fact or assertion offered as evidence that something is
          true; "it was a strong argument that his hypothesis was
          true" [syn: {statement}]
     2: a contentious speech act; a dispute where there is strong
        disagreement; "they were involved in a violent argument"
        [syn: {controversy}, {contention}, {contestation}, {disputation},
         {disceptation}, {tilt}, {arguing}]
     3: a discussion in which reasons are advanced for and against
        some proposition or proposal; "the argument over foreign
        aid goes on and on" [syn: {argumentation}, {debate}]
     4: a summary of the subject or plot of a literary work or play
        or ...
