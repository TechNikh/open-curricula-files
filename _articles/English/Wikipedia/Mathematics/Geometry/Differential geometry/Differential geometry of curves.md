---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Differential_geometry_of_curves
offline_file: ""
offline_thumbnail: ""
uuid: 5e71a0b9-38b9-4909-a1f9-0e4602c16214
updated: 1484309215
title: Differential geometry of curves
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Frenet_frame.png
tags:
    - Definitions
    - Reparametrization and equivalence relation
    - Length and natural parametrization
    - Frenet frame
    - Bertrand curve
    - Special Frenet vectors and generalized curvatures
    - Tangent vector
    - Normal or curvature vector
    - Curvature
    - Binormal vector
    - Torsion
    - Main theorem of curve theory
    - Frenet–Serret formulas
    - 2 dimensions
    - 3 dimensions
    - n dimensions (general formula)
    - Additional reading
categories:
    - Differential geometry
---
Differential geometry of curves is the branch of geometry that deals with smooth curves in the plane and in the Euclidean space by methods of differential and integral calculus.
