---
version: 1
type: video
provider: YouTube
id: jvWTncUlu6k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 461fec04-4e0a-41a8-a9f8-276adadfae82
updated: 1486069647
title: 'Super Constellations - Mandelbulb 3D fractal animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jvWTncUlu6k/default.jpg
    - https://i3.ytimg.com/vi/jvWTncUlu6k/1.jpg
    - https://i3.ytimg.com/vi/jvWTncUlu6k/2.jpg
    - https://i3.ytimg.com/vi/jvWTncUlu6k/3.jpg
---
