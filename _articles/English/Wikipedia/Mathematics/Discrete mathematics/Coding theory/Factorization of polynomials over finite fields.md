---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Factorization_of_polynomials_over_finite_fields
offline_file: ""
offline_thumbnail: ""
uuid: 2b054da2-ddf6-4430-b361-c15afa3814ce
updated: 1484308549
title: Factorization of polynomials over finite fields
tags:
    - Background
    - Finite field
    - Irreducible polynomials
    - Example
    - Complexity
    - Factoring algorithms
    - "Berlekamp's algorithm"
    - Square-free factorization
    - Example of a square-free factorization
    - Distinct-degree factorization
    - Equal-degree factorization
    - Cantor–Zassenhaus algorithm
    - "Victor Shoup's algorithm"
    - Time complexity
    - "Rabin's test of irreducibility"
    - Notes
categories:
    - Coding theory
---
In mathematics and computer algebra the factorization of a polynomial consists of decomposing it into a product of irreducible factors. This decomposition is theoretically possible and is unique for polynomials with coefficients in any field, but rather strong restrictions on the field of the coefficients are needed to allow the computation of the factorization by means of an algorithm. In practice, algorithms have been designed only for polynomials with coefficients in a finite field, in the field of rationals or in a finitely generated field extension of one of them.
