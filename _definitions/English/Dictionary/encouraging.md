---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encouraging
offline_file: ""
offline_thumbnail: ""
uuid: de9616df-77b4-42c3-91ce-3f96a55f2948
updated: 1484310539
title: encouraging
categories:
    - Dictionary
---
encouraging
     adj 1: giving courage or confidence or hope; "encouraging advances
            in medical research" [ant: {discouraging}]
     2: furnishing support and encouragement; "the anxious child
        needs supporting and accepting treatment from the teacher"
        [syn: {supporting}]
     3: tending to favor or bring good luck; "miracles are
        auspicious accidents"; "encouraging omens"; "a favorable
        time to ask for a raise"; "lucky stars"; "a prosperous
        moment to make a decision" [syn: {auspicious}, {favorable},
         {favourable}, {lucky}, {prosperous}]
