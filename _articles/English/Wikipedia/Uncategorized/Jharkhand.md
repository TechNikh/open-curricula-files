---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jharkhand
offline_file: ""
offline_thumbnail: ""
uuid: 3447f4df-fcfc-4e7f-87e3-7eeb08c0fb2f
updated: 1484308420
title: Jharkhand
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jaganath_Temple%252C_Ranchi.jpg'
tags:
    - History
    - Post-independence
    - geography
    - Flora and Fauna
    - Demography
    - religion
    - language
    - Hindi
    - Santhali
    - Angika
    - Administration
    - Divisions and Districts
    - Major Cities
    - Government and politics
    - Naxal insurgency
    - Economy
    - Education
    - schools
    - Universities and colleges
    - Engineering and Management Institutes
    - Medical colleges
    - Health
    - Sports
    - Media
categories:
    - Uncategorized
---
Jharkhand (lit. "Bushland") is a state in eastern India carved out of the southern part of Bihar on 15 November 2000.[3] The state shares its border with the states of Bihar to the north, Uttar Pradesh to the north-west, Chhattisgarh to the west, Odisha to the south, and West Bengal to the east. It has an area of 79,710 km2 (30,778 sq mi). The city of Ranchi is its capital while the industrial city of Jamshedpur is the most populous city of the state.
