---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Glaisher%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4b02b0ed-dfbd-4bf2-a6ec-83781bd96b49
updated: 1484308583
title: "Glaisher's theorem"
categories:
    - Combinatorics
---
It states that the number of partitions of an integer 
  
    
      
        N
      
    
    {\displaystyle N}
  
 into parts not divisible by 
  
    
      
        d
      
    
    {\displaystyle d}
  
 is equal to the number of partitions of the form
