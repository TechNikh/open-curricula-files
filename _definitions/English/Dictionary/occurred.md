---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occurred
offline_file: ""
offline_thumbnail: ""
uuid: 45f3e459-64a7-4bd7-98ae-7860b2ebbb9b
updated: 1484310429
title: occurred
categories:
    - Dictionary
---
occur
     v 1: come to pass; "What is happening?"; "The meeting took place
          off without an incidence"; "Nothing occurred that seemed
          important" [syn: {happen}, {hap}, {go on}, {pass off}, {pass},
           {fall out}, {come about}, {take place}]
     2: come to one's mind; suggest itself; "It occurred to me that
        we should hire another secretary"; "A great idea then came
        to her" [syn: {come}]
     3: to be found to exist; "sexism occurs in many workplaces";
        "precious stones occur in a large area in Brazil"
     [also: {occurring}, {occurred}]
