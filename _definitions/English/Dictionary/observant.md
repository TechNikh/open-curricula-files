---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414341
title: observant
categories:
    - Dictionary
---
observant
     adj 1: paying close attention especially to details
     2: quick to notice; showing quick and keen perception [syn: {observing}]
     3: (of individuals) adhering strictly to laws and rules and
        customs; "law-abiding citizens"; "observant of the speed
        limit" [syn: {law-abiding}]
