---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/massive
offline_file: ""
offline_thumbnail: ""
uuid: 10264987-ec1c-47cd-8b2d-8b2205fde01c
updated: 1484310461
title: massive
categories:
    - Dictionary
---
massive
     adj 1: imposing in size or bulk or solidity; "massive oak doors";
            "Moore's massive sculptures"; "the monolithic
            proportions of Stalinist architecture"; "a monumental
            scale" [syn: {monolithic}, {monumental}]
     2: being the same substance throughout; "massive silver"
     3: imposing in scale or scope or degree or power; "massive
        retaliatory power"; "a massive increase in oil prices";
        "massive changes"
     4: consisting of great mass; containing a great quantity of
        matter; "Earth is the most massive of the terrestrial
        planets"
