---
version: 1
type: article
id: https://en.wikipedia.org/wiki/James_B._Carrell
offline_file: ""
offline_thumbnail: ""
uuid: 2d46c507-4950-4bee-b764-3d83d77dab1e
updated: 1484309075
title: James B. Carrell
categories:
    - Geometers
---
James B. Carrell (1940, Seattle) is an American and Canadian mathematician, who is currently an emeritus professor of mathematics at the University of British Columbia, Vancouver, Canada.[1] His areas of research are algebraic geometry, Lie theory, transformation groups and differential geometry.
