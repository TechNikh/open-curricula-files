---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fraudulent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421421
title: fraudulent
categories:
    - Dictionary
---
fraudulent
     adj : intended to deceive; "deceitful advertising"; "fallacious
           testimony"; "smooth, shining, and deceitful as thin
           ice" - S.T.Coleridge; "a fraudulent scheme to escape
           paying taxes" [syn: {deceitful}, {fallacious}]
