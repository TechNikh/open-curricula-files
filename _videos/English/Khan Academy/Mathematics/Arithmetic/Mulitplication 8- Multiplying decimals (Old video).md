---
version: 1
type: video
provider: YouTube
id: m5z6pOsxF_8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Mulitplication%208-%20Multiplying%20decimals%20%28Old%20video%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1cc6a8ed-370a-4cf9-9fe4-3fcd21ffc21b
updated: 1486069567
title: 'Mulitplication 8- Multiplying decimals (Old video)'
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/m5z6pOsxF_8/default.jpg
    - https://i3.ytimg.com/vi/m5z6pOsxF_8/1.jpg
    - https://i3.ytimg.com/vi/m5z6pOsxF_8/2.jpg
    - https://i3.ytimg.com/vi/m5z6pOsxF_8/3.jpg
---
Multiplying decimals (www.khanacademy.org)
