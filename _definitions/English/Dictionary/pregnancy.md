---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pregnancy
offline_file: ""
offline_thumbnail: ""
uuid: 48b75613-8446-48ae-93fa-ac4432ea81dd
updated: 1484310162
title: pregnancy
categories:
    - Dictionary
---
pregnancy
     n : the state of being pregnant; the period from conception to
         birth when a woman carries a developing fetus in her
         uterus [syn: {gestation}, {maternity}]
