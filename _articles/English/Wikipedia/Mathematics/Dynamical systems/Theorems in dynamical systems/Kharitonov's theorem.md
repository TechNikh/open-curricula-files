---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kharitonov%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 582b1a55-c892-4e03-976e-b0a07588787b
updated: 1484308690
title: "Kharitonov's theorem"
categories:
    - Theorems in dynamical systems
---
Kharitonov's theorem is a result used in control theory to assess the stability of a dynamical system when the physical parameters of the system are not known precisely. When the coefficients of the characteristic polynomial are known, the Routh-Hurwitz stability criterion can be used to check if the system is stable (i.e. if all roots have negative real parts). Kharitonov's theorem can be used in the case where the coefficients are only known to be within specified ranges. It provides a test of stability for a so-called interval polynomial, while Routh-Hurwitz is concerned with an ordinary polynomial.
