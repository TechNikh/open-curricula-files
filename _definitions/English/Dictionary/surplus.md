---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surplus
offline_file: ""
offline_thumbnail: ""
uuid: 3db8d560-7135-4334-b0e5-56b1222f5229
updated: 1484310518
title: surplus
categories:
    - Dictionary
---
surplus
     adj : more than is needed, desired, or required; "trying to lose
           excess weight"; "found some extra change lying on the
           dresser"; "yet another book on heraldry might be
           thought redundant"; "skills made redundant by
           technological advance"; "sleeping in the spare room";
           "supernumerary ornamentation"; "it was supererogatory
           of her to gloat"; "delete superfluous (or unnecessary)
           words"; "extra ribs as well as other supernumerary
           internal parts"; "surplus cheese distributed to the
           needy" [syn: {excess}, {extra}, {redundant}, {spare}, {supererogatory},
            {superfluous}, ...
