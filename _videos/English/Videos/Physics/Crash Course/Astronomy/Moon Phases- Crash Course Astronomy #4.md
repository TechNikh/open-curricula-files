---
version: 1
type: video
provider: YouTube
id: AQ5vty8f9Xc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd91453a-0bfe-4eef-966f-7b7259b02b8b
updated: 1486069684
title: 'Moon Phases- Crash Course Astronomy #4'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/AQ5vty8f9Xc/default.jpg
    - https://i3.ytimg.com/vi/AQ5vty8f9Xc/1.jpg
    - https://i3.ytimg.com/vi/AQ5vty8f9Xc/2.jpg
    - https://i3.ytimg.com/vi/AQ5vty8f9Xc/3.jpg
---
