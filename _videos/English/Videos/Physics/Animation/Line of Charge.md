---
version: 1
type: video
provider: YouTube
id: Xjb4e1MbwGA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Line%20of%20Charge.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c06ed330-9b75-4a0b-985a-34d4ed65e0fd
updated: 1486069652
title: Line of Charge
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xjb4e1MbwGA/default.jpg
    - https://i3.ytimg.com/vi/Xjb4e1MbwGA/1.jpg
    - https://i3.ytimg.com/vi/Xjb4e1MbwGA/2.jpg
    - https://i3.ytimg.com/vi/Xjb4e1MbwGA/3.jpg
---
