---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Implicit_graph
offline_file: ""
offline_thumbnail: ""
uuid: bc559f6b-3b87-4414-b2d4-97162f3dca50
updated: 1484308625
title: Implicit graph
tags:
    - Neighborhood representations
    - Adjacency labeling schemes
    - The implicit representation conjecture
    - Labeling schemes and induced universal graphs
    - Evasiveness
categories:
    - Graph theory
---
In the study of graph algorithms, an implicit graph representation (or more simply implicit graph) is a graph whose vertices or edges are not represented as explicit objects in a computer's memory, but rather are determined algorithmically from some more concise input.
