---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Foot-binding
offline_file: ""
offline_thumbnail: ""
uuid: 28ccf1e9-76c1-41df-b9d2-32f32036f790
updated: 1484308485
title: Foot binding
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-A_Chinese_Golden_Lily_Foot%252C_Lai_Afong%252C_c1870s.jpg'
tags:
    - History
    - Origin
    - Spread of practice
    - Variations and prevalence
    - Demise
    - Process
    - Health issues
    - Views and interpretations
    - Role of Confucianism
    - Erotic appeal
categories:
    - Uncategorized
---
Foot binding was the custom of applying painfully tight binding to the feet of young girls to prevent further growth. The practice possibly originated among upper-class court dancers during the Five Dynasties and Ten Kingdoms period in Imperial China (10th or 11th century), then became popular during the Song dynasty and eventually spread to all social classes. Foot binding became popular as a means of displaying status (women from wealthy families, who did not need their feet to work, could afford to have them bound) and was correspondingly adopted as a symbol of beauty in Chinese culture. Its prevalence and practice however varied in different parts of the country. Feet altered by binding ...
