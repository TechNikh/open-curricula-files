---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refractive
offline_file: ""
offline_thumbnail: ""
uuid: 0a4de070-97d5-48ff-ae85-5a0bde14fff9
updated: 1484310212
title: refractive
categories:
    - Dictionary
---
refractive
     adj 1: of or relating to or capable of refraction; "the refractive
            characteristics of the eye" [syn: {refractile}]
     2: capable of changing the direction (of a light or sound wave)
        [syn: {deflective}]
