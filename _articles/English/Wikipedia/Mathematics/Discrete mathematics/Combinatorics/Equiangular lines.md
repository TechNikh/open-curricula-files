---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equiangular_lines
offline_file: ""
offline_thumbnail: ""
uuid: 11abbfab-52a8-4958-b10a-de420d9fc669
updated: 1484308573
title: Equiangular lines
categories:
    - Combinatorics
---
Computing the maximum number of equiangular lines in n-dimensional Euclidean space is a difficult problem, and unsolved in general, though bounds are known. The maximal number of equiangular lines in 2-dimensional Euclidean space is 3: we can take the lines through opposite vertices of a regular hexagon, each at an angle 120 degrees from the other two. The maximum in 3 dimensions is 6: we can take lines through opposite vertices of an icosahedron. The maximum in dimensions 1 through 13 is listed in The On-Line Encyclopedia of Integer Sequences as follows:
