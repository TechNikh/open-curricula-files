---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buddhist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565361
title: buddhist
categories:
    - Dictionary
---
Buddhist
     adj : of or relating to or supporting Buddhism; "Buddhist
           sculpture" [syn: {Buddhistic}]
     n : one who follows the teachings of Buddha
