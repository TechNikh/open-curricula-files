---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/analgesic
offline_file: ""
offline_thumbnail: ""
uuid: a3039fad-7c36-437d-b074-3741f16eeb38
updated: 1484310389
title: analgesic
categories:
    - Dictionary
---
analgesic
     adj : capable of relieving pain; "the anodyne properties of
           certain drugs"; "an analgesic effect" [syn: {analgetic},
            {anodyne}]
     n : a medicine used to relieve pain [syn: {anodyne}, {painkiller},
          {pain pill}]
