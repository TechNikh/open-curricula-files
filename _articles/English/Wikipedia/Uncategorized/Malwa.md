---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Malwa
offline_file: ""
offline_thumbnail: ""
uuid: 62b76d1e-d20f-4fdf-8081-9d3caa14d588
updated: 1484308420
title: Malwa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-NavdatoliGoblet1300BCE.jpg
tags:
    - History
    - geography
    - Demographics
    - Economy
    - culture
    - Tourism
    - Sports
    - Venues
    - Notes
categories:
    - Uncategorized
---
Malwa is a natural region in west-central India occupying a plateau of volcanic origin. Geologically, the Malwa Plateau generally refers to the volcanic upland north of the Vindhya Range. Politically and administratively, the historical Malwa region includes districts of western Madhya Pradesh and parts of south-eastern Rajasthan. The definition of Malwa is sometimes extended to include the Nimar region north of the Vindhyas.
