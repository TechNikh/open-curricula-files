---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_covariance
offline_file: ""
offline_thumbnail: ""
uuid: e19ef346-68d1-4a39-b6a4-56933804bf7a
updated: 1484309221
title: General covariance
tags:
    - Remarks
    - Notes
categories:
    - Differential geometry
---
In theoretical physics, general covariance (also known as diffeomorphism covariance or general invariance) is the invariance of the form of physical laws under arbitrary differentiable coordinate transformations. The essential idea is that coordinates do not exist a priori in nature, but are only artifices used in describing nature, and hence should play no role in the formulation of fundamental physical laws.
