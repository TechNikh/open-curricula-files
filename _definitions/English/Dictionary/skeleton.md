---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skeleton
offline_file: ""
offline_thumbnail: ""
uuid: c5d4242b-729e-4170-a1ac-bac076b5b9af
updated: 1484310371
title: skeleton
categories:
    - Dictionary
---
skeleton
     n 1: something reduced to its minimal form; "the battalion was a
          mere skeleton of its former self"; "the bare skeleton of
          a novel"
     2: a scandal that is kept secret; "there must be a skeleton
        somewhere in that family's closet" [syn: {skeleton in the
        closet}, {skeleton in the cupboard}]
     3: the hard structure (bones and cartilages) that provides a
        frame for the body of an animal [syn: {skeletal system}, {frame},
         {systema skeletale}]
     4: the internal supporting structure that gives an artifact its
        shape; "the building has a steel skeleton" [syn: {skeletal
        frame}, {frame}, {underframe}]
