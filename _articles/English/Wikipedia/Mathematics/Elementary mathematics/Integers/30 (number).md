---
version: 1
type: article
id: https://en.wikipedia.org/wiki/30_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 750e9c61-56cc-4955-9283-a6aadac33e44
updated: 1484308955
title: 30 (number)
tags:
    - In mathematics
    - In science
    - Astronomy
    - In other fields
    - History and literature
    - Sports
    - Music
categories:
    - Integers
---
