---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Japaridze%27s_polymodal_logic'
offline_file: ""
offline_thumbnail: ""
uuid: c5d33aa2-3764-47fc-9663-9c29546ae072
updated: 1484309495
title: "Japaridze's polymodal logic"
tags:
    - Language and axiomatization
    - Provability semantics
    - Other semantics
    - Computational complexity
    - History
    - Literature
categories:
    - Proof theory
---
Japaridze's polymodal logic (GLP), is a system of provability logic with infinitely many modal (provability) operators. This system has played an important role in some applications of provability algebras in proof theory, and has been extensively studied since the late 1980s. It is named after Giorgi Japaridze.
