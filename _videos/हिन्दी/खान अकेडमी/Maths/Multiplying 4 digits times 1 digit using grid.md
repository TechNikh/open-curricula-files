---
version: 1
type: video
provider: YouTube
id: E8XLvptlB1s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6b78775b-41f5-416b-a439-8ef4af5a2a30
updated: 1486069618
title: Multiplying 4 digits times 1 digit using grid
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/E8XLvptlB1s/default.jpg
    - https://i3.ytimg.com/vi/E8XLvptlB1s/1.jpg
    - https://i3.ytimg.com/vi/E8XLvptlB1s/2.jpg
    - https://i3.ytimg.com/vi/E8XLvptlB1s/3.jpg
---
