---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/added
offline_file: ""
offline_thumbnail: ""
uuid: 462b95a6-024b-4281-921c-1e2de49a8f56
updated: 1484310369
title: added
categories:
    - Dictionary
---
added
     adj : combined or joined to increase in size or quantity or scope;
           "takes on added significance"; "an added attraction"
           [ant: {subtracted}]
