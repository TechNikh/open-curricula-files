---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humility
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424721
title: humility
categories:
    - Dictionary
---
humility
     n 1: a disposition to be humble; a lack of false pride; "not
          everyone regards humility as a virtue" [syn: {humbleness}]
          [ant: {pride}]
     2: a humble feeling; "he was filled with humility at the sight
        of the Pope" [syn: {humbleness}] [ant: {pride}]
