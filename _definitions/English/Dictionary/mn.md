---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mn
offline_file: ""
offline_thumbnail: ""
uuid: 7e10bedc-cac0-41e7-aecf-e10a67eb950e
updated: 1484310303
title: mn
categories:
    - Dictionary
---
Mn
     n 1: a hard brittle gray polyvalent metallic element that
          resembles iron but is not magnetic; used in making
          steel; occurs in many minerals [syn: {manganese}, {atomic
          number 25}]
     2: a midwestern state [syn: {Minnesota}, {Gopher State}, {North
        Star State}]
