---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/varied
offline_file: ""
offline_thumbnail: ""
uuid: 60c4b067-e62e-40db-a0ee-21f6bca1c7b6
updated: 1484310303
title: varied
categories:
    - Dictionary
---
varied
     adj 1: characterized by variety; "immigrants' varied ethnic and
            religious traditions"; "his work is interesting and
            varied" [ant: {unvaried}]
     2: widely different; "varied motives prompt people to join a
        political party"; "varied ethnic traditions of the
        immigrants" [syn: {wide-ranging}]
     3: broken away from sameness or identity or duplication; "her
        quickly varied answers indicated uncertainty"
