---
version: 1
type: video
provider: YouTube
id: ieX9UlD087c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca5df6b1-41ff-41f3-acc5-00e69d184db8
updated: 1486069625
title: Multiplication with fingers (Math tricks)
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/ieX9UlD087c/default.jpg
    - https://i3.ytimg.com/vi/ieX9UlD087c/1.jpg
    - https://i3.ytimg.com/vi/ieX9UlD087c/2.jpg
    - https://i3.ytimg.com/vi/ieX9UlD087c/3.jpg
---
