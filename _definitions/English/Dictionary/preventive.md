---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preventive
offline_file: ""
offline_thumbnail: ""
uuid: 4a1ae361-b31a-438a-ae9e-776e95cfffbc
updated: 1484310447
title: preventive
categories:
    - Dictionary
---
preventive
     adj 1: preventing or contributing to the prevention of disease;
            "preventive medicine"; "vaccines are prophylactic"; "a
            prophylactic drug" [syn: {preventative}, {prophylactic}]
     2: tending to prevent or hinder [syn: {preventative}] [ant: {permissive}]
     3: tending to ward off; "the swastika...a very ancient
        prophylactic symbol occurring among all peoples"- Victor
        Schultze [syn: {cautionary}, {prophylactic}]
     n 1: remedy that prevents or slows the course of an illness or
          disease; "the doctor recommended several preventatives"
          [syn: {preventative}, {prophylactic}]
     2: any obstruction that impedes or is ...
