---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interpretability
offline_file: ""
offline_thumbnail: ""
uuid: 0e44612a-ab62-4ca2-a7c3-653c676cbcbb
updated: 1484309495
title: Interpretability
categories:
    - Proof theory
---
In mathematical logic, interpretability is a relation between formal theories that expresses the possibility of interpreting or translating one into the other.
