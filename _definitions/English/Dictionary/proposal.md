---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proposal
offline_file: ""
offline_thumbnail: ""
uuid: 7fa6ba5f-891b-4d14-897e-bc6840ff8156
updated: 1484310389
title: proposal
categories:
    - Dictionary
---
proposal
     n 1: something proposed (such as a plan or assumption)
     2: an offer of marriage [syn: {marriage proposal}, {proposal of
        marriage}, {marriage offer}]
     3: the act of making a proposal; "they listened to her
        proposal" [syn: {proposition}]
