---
version: 1
type: video
provider: YouTube
id: ldjDzriyi28
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a2fafc45-a054-4397-aff6-452c2b417b4f
updated: 1486069609
title: Complex Numbers part 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ldjDzriyi28/default.jpg
    - https://i3.ytimg.com/vi/ldjDzriyi28/1.jpg
    - https://i3.ytimg.com/vi/ldjDzriyi28/2.jpg
    - https://i3.ytimg.com/vi/ldjDzriyi28/3.jpg
---
