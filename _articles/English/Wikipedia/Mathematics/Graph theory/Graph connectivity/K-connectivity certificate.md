---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-connectivity_certificate
offline_file: ""
offline_thumbnail: ""
uuid: a3584a0b-31a2-4755-8c63-4a275da1d628
updated: 1484309400
title: K-connectivity certificate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Kv3.PNG
tags:
    - Sparse certificates
    - Scan-first search
    - The Main Certificate Theorem
    - Computational complexity
categories:
    - Graph connectivity
---
In graph theory, for a graph G = (V, E), there exists some subset of edges E' ∈ E, such that the sub-graph G' = (V, E') is k-connected if and only if G is k-connected. This subset is considered a certificate for the k-connectivity of the graph G.[1]
