---
version: 1
type: video
provider: YouTube
id: uA_nhDP8b1g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1f5ea646-6887-4f71-80d6-8b56ceb4ab47
updated: 1486069631
title: 'Nouns- Gender for Class 3rd Kids'
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/uA_nhDP8b1g/default.jpg
    - https://i3.ytimg.com/vi/uA_nhDP8b1g/1.jpg
    - https://i3.ytimg.com/vi/uA_nhDP8b1g/2.jpg
    - https://i3.ytimg.com/vi/uA_nhDP8b1g/3.jpg
---
