---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doctorial
offline_file: ""
offline_thumbnail: ""
uuid: b0572560-ba3a-45f8-a354-2be04c3c4fea
updated: 1484310163
title: doctorial
categories:
    - Dictionary
---
doctorial
     adj : of or relating to a doctor or doctorate; "doctoral
           dissertation"; "doctorial candidates" [syn: {doctoral}]
