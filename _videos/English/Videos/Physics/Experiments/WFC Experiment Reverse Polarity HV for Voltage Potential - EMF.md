---
version: 1
type: video
provider: YouTube
id: fki6kz3J3po
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 77282df4-7dbc-42ab-8461-cfab84c03adb
updated: 1486069637
title: >
    WFC Experiment Reverse Polarity HV for Voltage Potential \
    EMF
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/fki6kz3J3po/default.jpg
    - https://i3.ytimg.com/vi/fki6kz3J3po/1.jpg
    - https://i3.ytimg.com/vi/fki6kz3J3po/2.jpg
    - https://i3.ytimg.com/vi/fki6kz3J3po/3.jpg
---
