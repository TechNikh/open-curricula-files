---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stable_marriage_problem
offline_file: ""
offline_thumbnail: ""
uuid: 53d600f5-5a87-4e7e-978d-c91d23d4db22
updated: 1484308605
title: Stable marriage problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gale-Shapley.gif
tags:
    - Applications
    - Solution
    - Algorithm
    - Optimality of the solution
    - Stable Marriage with indifference
    - Similar problems
    - Implementation in software package
    - >
        Textbooks and other important references not cited in the
        text
categories:
    - Combinatorics
---
In mathematics, economics, and computer science, the stable marriage problem (also stable matching problem or SMP) is the problem of finding a stable matching between two equally sized sets of elements given an ordering of preferences for each element. A matching is a mapping from the elements of one set to the elements of the other set. A matching is not stable if:
