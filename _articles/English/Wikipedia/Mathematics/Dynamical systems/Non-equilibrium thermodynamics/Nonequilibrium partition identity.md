---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Nonequilibrium_partition_identity
offline_file: ""
offline_thumbnail: ""
uuid: d1575a9d-2ec4-4039-92fd-201b11808dd8
updated: 1484308805
title: Nonequilibrium partition identity
categories:
    - Non-equilibrium thermodynamics
---
The nonequilibrium partition identity (NPI) is a remarkably simple and elegant[citation needed] consequence of the Fluctuation Theorem previously known as the Kawasaki Identity:
