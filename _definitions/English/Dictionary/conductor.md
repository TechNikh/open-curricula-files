---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conductor
offline_file: ""
offline_thumbnail: ""
uuid: bc81f2e5-0412-487e-9e50-ab7cd6725097
updated: 1484310204
title: conductor
categories:
    - Dictionary
---
conductor
     n 1: the person who leads a musical group [syn: {music director},
           {director}]
     2: a device designed to transmit electricity, heat, etc.
     3: a substance that readily conducts e.g. electricity and heat
        [ant: {insulator}]
     4: the person who collects fares on a public conveyance
