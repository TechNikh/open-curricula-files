---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unnecessarily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572561
title: unnecessarily
categories:
    - Dictionary
---
unnecessarily
     adv 1: in an unnecessary manner; "they were unnecessarily rude"
            [ant: {necessarily}]
     2: without any necessity; "this marathon would exhaust him
        unnecessarily"
