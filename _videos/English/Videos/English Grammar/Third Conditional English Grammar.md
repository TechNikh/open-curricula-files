---
version: 1
type: video
provider: YouTube
id: Kgvqyt_8Zus
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Third%20Conditional%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7d89d02a-e66b-472f-a570-57ff26590fbd
updated: 1486069631
title: Third Conditional English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/Kgvqyt_8Zus/default.jpg
    - https://i3.ytimg.com/vi/Kgvqyt_8Zus/1.jpg
    - https://i3.ytimg.com/vi/Kgvqyt_8Zus/2.jpg
    - https://i3.ytimg.com/vi/Kgvqyt_8Zus/3.jpg
---
