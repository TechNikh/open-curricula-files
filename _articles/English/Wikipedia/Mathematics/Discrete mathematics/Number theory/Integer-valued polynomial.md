---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integer-valued_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: b0bdf2b0-3b5f-4199-b77d-cbc6df272988
updated: 1484308649
title: Integer-valued polynomial
tags:
    - Classification
    - Fixed prime divisors
    - Other rings
    - Applications
    - Algebra
    - Algebraic topology
categories:
    - Number theory
---
In mathematics, an integer-valued polynomial (also known as a numerical polynomial) P(t) is a polynomial whose value P(n) is an integer for every integer n. Every polynomial with integer coefficients is integer-valued, but the converse is not true. For example, the polynomial
