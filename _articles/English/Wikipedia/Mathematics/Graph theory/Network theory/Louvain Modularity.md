---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Louvain_Modularity
offline_file: ""
offline_thumbnail: ""
uuid: 5daae345-ae81-46e8-a7a5-920f94a0a0fa
updated: 1484309435
title: Louvain Modularity
tags:
    - Modularity Optimization
    - Algorithm
    - Previous Uses
    - Comparison to Other Methods
categories:
    - Network theory
---
The Louvain Method for community detection is a method to extract communities from large networks created by Vincent Blondel, Jean-Loup Guillaume, Renaud Lambiotte and Etienne Lefebvre.[1] The method is a greedy optimization method that appears to run in time O(n log n).
