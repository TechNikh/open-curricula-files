---
version: 1
type: video
provider: YouTube
id: z2ucTyTx5zg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a4fa8ac2-357a-43cb-a8d7-fa71db4a8477
updated: 1486069609
title: Addition word problems within 10
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/z2ucTyTx5zg/default.jpg
    - https://i3.ytimg.com/vi/z2ucTyTx5zg/1.jpg
    - https://i3.ytimg.com/vi/z2ucTyTx5zg/2.jpg
    - https://i3.ytimg.com/vi/z2ucTyTx5zg/3.jpg
---
