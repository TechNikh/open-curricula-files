---
version: 1
type: video
provider: YouTube
id: TEl4jeETVmg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a715f02-222a-4cc9-bb2c-38bcac1887f4
updated: 1486069662
title: 'How big is a mole? (Not the animal, the other one.) - Daniel Dulek'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TEl4jeETVmg/default.jpg
    - https://i3.ytimg.com/vi/TEl4jeETVmg/1.jpg
    - https://i3.ytimg.com/vi/TEl4jeETVmg/2.jpg
    - https://i3.ytimg.com/vi/TEl4jeETVmg/3.jpg
---
