---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/victim
offline_file: ""
offline_thumbnail: ""
uuid: 93ee8699-29a6-4152-a816-005707c43e75
updated: 1484310599
title: victim
categories:
    - Dictionary
---
victim
     n 1: an unfortunate person who suffers from some adverse
          circumstance
     2: a person who is tricked or swindled [syn: {dupe}]
