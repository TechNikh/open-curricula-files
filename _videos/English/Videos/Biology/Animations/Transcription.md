---
version: 1
type: video
provider: YouTube
id: WsofH466lqk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transcription.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e3c62c22-eb72-4142-8704-4d0211aee7e1
updated: 1486069668
title: Transcription
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/WsofH466lqk/default.jpg
    - https://i3.ytimg.com/vi/WsofH466lqk/1.jpg
    - https://i3.ytimg.com/vi/WsofH466lqk/2.jpg
    - https://i3.ytimg.com/vi/WsofH466lqk/3.jpg
---
