---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engineering
offline_file: ""
offline_thumbnail: ""
uuid: b408a475-4552-47e9-b376-e4ae8a5ed8da
updated: 1484310479
title: engineering
categories:
    - Dictionary
---
engineering
     n 1: the practical application of science to commerce or industry
          [syn: {technology}]
     2: the discipline dealing with the art or science of applying
        scientific knowledge to practical problems; "he had
        trouble deciding which branch of engineering to study"
        [syn: {engineering science}, {applied science}, {technology}]
     3: a room (as on a ship) in which the engine is located [syn: {engine
        room}]
