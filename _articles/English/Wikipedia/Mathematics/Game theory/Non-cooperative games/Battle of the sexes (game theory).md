---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Battle_of_the_sexes_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 90bbe48b-6e98-4069-a3a6-b2914368c4ef
updated: 1484309052
title: Battle of the sexes (game theory)
tags:
    - Equilibrium analysis
    - Working out the above
    - Burning money
    - Battle of the Sexes Game with Ambiguity
categories:
    - Non-cooperative games
---
In game theory, battle of the sexes (BoS) is a two-player coordination game. Imagine a couple that agreed to meet this evening, but cannot recall if they will be attending the opera or a football match (and the fact that they forgot is common knowledge). The husband would prefer to go to the football game. The wife would rather go to the opera. Both would prefer to go to the same place rather than different ones. If they cannot communicate, where should they go?
