---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indistinctly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570941
title: indistinctly
categories:
    - Dictionary
---
indistinctly
     adv : in a dim indistinct manner; "we perceived the change only
           dimly" [syn: {dimly}]
