---
version: 1
type: video
provider: YouTube
id: FE8SpzfqVAw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9e2f0fae-4bb6-4feb-97b9-008823734c68
updated: 1486069669
title: 'How to Slip a Disc in Your Back? Mechanism of Herniated Lumbar Disc Animation - Slipped Spinal Disk'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/FE8SpzfqVAw/default.jpg
    - https://i3.ytimg.com/vi/FE8SpzfqVAw/1.jpg
    - https://i3.ytimg.com/vi/FE8SpzfqVAw/2.jpg
    - https://i3.ytimg.com/vi/FE8SpzfqVAw/3.jpg
---
