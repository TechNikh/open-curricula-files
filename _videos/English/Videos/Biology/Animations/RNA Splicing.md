---
version: 1
type: video
provider: YouTube
id: aVgwr0QpYNE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/RNA%20Splicing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c43af476-69fc-49ec-987c-13bf189121ed
updated: 1486069672
title: RNA Splicing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/aVgwr0QpYNE/default.jpg
    - https://i3.ytimg.com/vi/aVgwr0QpYNE/1.jpg
    - https://i3.ytimg.com/vi/aVgwr0QpYNE/2.jpg
    - https://i3.ytimg.com/vi/aVgwr0QpYNE/3.jpg
---
