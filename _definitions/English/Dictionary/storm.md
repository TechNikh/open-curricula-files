---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/storm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484379541
title: storm
categories:
    - Dictionary
---
storm
     n 1: a violent weather condition with winds 64-72 knots (11 on
          the Beaufort scale) and precipitation and thunder and
          lightening [syn: {violent storm}]
     2: a violent commotion or disturbance; "the storms that had
        characterized their relationship had died away"; "it was
        only a tempest in a teapot" [syn: {tempest}]
     3: a direct and violent assault on a stronghold
     v 1: behave violently, as if in state of a great anger [syn: {ramp},
           {rage}]
     2: take by force; "Storm the fort" [syn: {force}]
     3: rain, hail, or snow hard and be very windy, often with
        thunder or lightning; "If it storms, we'll need shelter"
     ...
