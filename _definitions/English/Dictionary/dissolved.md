---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissolved
offline_file: ""
offline_thumbnail: ""
uuid: d17b590d-340b-4152-b497-e244af77f728
updated: 1484310344
title: dissolved
categories:
    - Dictionary
---
dissolved
     adj 1: (of solid matter) reduced to a liquid form; "add the
            dissolved gelatin"
     2: sundered by divorce or separation or desertion; "a dissolved
        marriage"
