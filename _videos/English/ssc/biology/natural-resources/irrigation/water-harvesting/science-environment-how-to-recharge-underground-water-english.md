---
title: Science - Environment - How to recharge underground water - English
youtube_id: sH8XmG1Bvz8
tags:
    - Science
    - Water
    - Recharge pit
    - rain water harvesting pit
    - Rainwater Harvesting
    - bore-well
    - underground water recharge
    - solution of water logging
    - sand filter
    - Upper Primary class
    - Educational video
categories:
    - Water harvesting
---
