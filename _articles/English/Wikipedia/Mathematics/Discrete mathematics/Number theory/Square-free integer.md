---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Square-free_integer
offline_file: ""
offline_thumbnail: ""
uuid: ec7c1ed4-6b87-43ac-867c-03dbe6be3027
updated: 1484308654
title: Square-free integer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Composite_number_Cuisenaire_rods_10.png
tags:
    - Equivalent characterizations
    - Dirichlet generating function
    - Distribution
    - Encoding as binary numbers
    - Erdős squarefree conjecture
    - Squarefree core
    - Notes
categories:
    - Number theory
---
In mathematics, a square-free, or quadratfrei (from German language) integer, is an integer which is divisible by no other perfect square than 1. For example, 10 is square-free but 18 is not, as 18 is divisible by 9 = 32. The smallest positive square-free numbers are
