---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thermal
offline_file: ""
offline_thumbnail: ""
uuid: 5877db42-313b-4fed-b8e6-6b812698737b
updated: 1484310232
title: thermal
categories:
    - Dictionary
---
thermal
     adj 1: relating to or associated with heat; "thermal movements of
            molecules"; "thermal capacity"; "thermic energy"; "the
            caloric effect of sunlight" [syn: {thermic}, {caloric}]
            [ant: {nonthermal}]
     2: of or relating to hot a hot spring; "thermal water"
     3: caused by or designed to retain heat; "a thermal burn";
        "thermal underwear"
     n : rising current of warm air
