---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Peripheral_cycle
offline_file: ""
offline_thumbnail: ""
uuid: 2872942c-ce90-4f2b-8686-f68e5dbd1526
updated: 1484309443
title: Peripheral cycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-6n-graf-clique.svg.png
tags:
    - Definitions
    - Properties
    - Related concepts
categories:
    - Graph theory objects
---
In graph theory, a peripheral cycle (or peripheral circuit) in an undirected graph is, intuitively, a cycle that does not separate any part of the graph from any other part. Peripheral cycles (or, as they were initially called, peripheral polygons, because Tutte called cycles "polygons") were first studied by Tutte (1963), and play important roles in the characterization of planar graphs and in generating the cycle spaces of nonplanar graphs.[1]
