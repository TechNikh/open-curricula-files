---
version: 1
type: video
provider: YouTube
id: SPGRkexI_cs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/The%20Lungs%20and%20Pulmonary%20System.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6633e64b-c37f-498f-8d2e-bd400435dc9d
updated: 1486069585
title: The Lungs and Pulmonary System
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/SPGRkexI_cs/default.jpg
    - https://i3.ytimg.com/vi/SPGRkexI_cs/1.jpg
    - https://i3.ytimg.com/vi/SPGRkexI_cs/2.jpg
    - https://i3.ytimg.com/vi/SPGRkexI_cs/3.jpg
---
The pulmonary system including the lungs, larynx, trachea, bronchi, bronchioles, alveoli and thoracic diaphragm
More free lessons at: http://www.khanacademy.org/video?v=SPGRkexI_cs
