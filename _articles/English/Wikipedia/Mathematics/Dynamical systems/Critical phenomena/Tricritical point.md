---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tricritical_point
offline_file: ""
offline_thumbnail: ""
uuid: f757ed87-cc3a-4fa9-99a0-d0fd83004cad
updated: 1484308746
title: Tricritical point
categories:
    - Critical phenomena
---
In condensed matter physics, dealing with the macroscopic physical properties of matter, a tricritical point is a point in the phase diagram of a system at which three-phase coexistence terminates.[1] This definition is clearly parallel to the definition of an ordinary critical point as the point at which two-phase coexistence terminates.
