---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digest
offline_file: ""
offline_thumbnail: ""
uuid: 3c7cfaf5-5e6c-4018-ae13-ade88294d319
updated: 1484310327
title: digest
categories:
    - Dictionary
---
digest
     n 1: a periodical that summarizes the news
     2: something that is compiled (as into a single book or file)
        [syn: {compilation}]
     v 1: convert food into absorbable substances; "I cannot digest
          milk products"
     2: arrange and integrate in the mind; "I cannot digest all this
        information"
     3: put up with something or somebody unpleasant; "I cannot bear
        his constant criticism"; "The new secretary had to endure
        a lot of unprofessional remarks"; "he learned to tolerate
        the heat"; "She stuck out two years in a miserable
        marriage" [syn: {endure}, {stick out}, {stomach}, {bear},
        {stand}, {tolerate}, {support}, ...
