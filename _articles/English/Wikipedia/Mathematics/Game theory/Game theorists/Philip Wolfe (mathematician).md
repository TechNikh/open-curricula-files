---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Philip_Wolfe_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 753b4f84-1c51-48db-b85c-6c3758d18ce3
updated: 1484309029
title: Philip Wolfe (mathematician)
tags:
    - life
    - Career
    - Honors and awards
    - Selected publications
    - External Information
categories:
    - Game theorists
---
