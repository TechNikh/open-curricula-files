---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viewing
offline_file: ""
offline_thumbnail: ""
uuid: a63ff486-12dd-45a0-8992-68e364c62e03
updated: 1484310210
title: viewing
categories:
    - Dictionary
---
viewing
     n 1: the display of a motion picture [syn: {screening}, {showing}]
     2: a vigil held over a corpse the night before burial; "there's
        no weeping at an Irish wake" [syn: {wake}]
