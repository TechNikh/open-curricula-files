---
version: 1
type: video
provider: YouTube
id: eTyhI-l0BMI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c9fc5604-eaaa-4784-ae5f-b51148cde743
updated: 1486069618
title: Pythagorean theorem proof using similarity
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/eTyhI-l0BMI/default.jpg
    - https://i3.ytimg.com/vi/eTyhI-l0BMI/1.jpg
    - https://i3.ytimg.com/vi/eTyhI-l0BMI/2.jpg
    - https://i3.ytimg.com/vi/eTyhI-l0BMI/3.jpg
---
