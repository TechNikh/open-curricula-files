---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neighbourhood_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: b865bae3-1f77-4935-8aa6-db626cd21028
updated: 1484309445
title: Neighbourhood (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graf.svg_2.png
tags:
    - Local properties in graphs
    - Neighbourhood of a Set
categories:
    - Graph theory objects
---
In graph theory, an adjacent vertex of a vertex v in a graph is a vertex that is connected to v by an edge. The neighbourhood of a vertex v in a graph G is the induced subgraph of G consisting of all vertices adjacent to v. For example, the image shows a graph of 6 vertices and 7 edges. Vertex 5 is adjacent to vertices 1, 2, and 4 but it is not adjacent to 3 and 6. The neighbourhood of vertex 5 is the graph with three vertices, 1, 2, and 4, and one edge connecting vertices 1 and 2.
