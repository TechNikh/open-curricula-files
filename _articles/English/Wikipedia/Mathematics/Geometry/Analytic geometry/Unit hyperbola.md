---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unit_hyperbola
offline_file: ""
offline_thumbnail: ""
uuid: e4b88a33-03bf-40b2-b99b-be57381efb0c
updated: 1484309167
title: Unit hyperbola
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_15.png
tags:
    - Asymptotes
    - Minkowski diagram
    - Parametrization
    - Complex plane algebra
categories:
    - Analytic geometry
---
In geometry, the unit hyperbola is the set of points (x,y) in the Cartesian plane that satisfies 
  
    
      
        
          x
          
            2
          
        
        −
        
          y
          
            2
          
        
        =
        1.
      
    
    {\displaystyle x^{2}-y^{2}=1.}
  
 In the study of indefinite orthogonal groups, the unit hyperbola forms the basis for an alternative radial length
