---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metal_foil
offline_file: ""
offline_thumbnail: ""
uuid: 90f27f52-592c-4132-a933-b52dba75d1f9
updated: 1484308366
title: Foil (metal)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Aluminumfoil.jpg
categories:
    - Uncategorized
---
A foil is a very thin sheet of metal, usually made by hammering or rolling. Foils are most easily made with malleable metals, such as aluminium, copper, tin, and gold. Foils usually bend under their own weight and can be torn easily. The more malleable a metal, the thinner foil can be made with it. For example, aluminium foil is usually about 1/1000 inch (0.03 mm), whereas gold (more malleable than aluminium) can be made into foil only a few atoms thick, called gold leaf. Extremely thin foil is called metal leaf. Leaf tears very easily and must be picked up with special brushes.
