---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insecurity
offline_file: ""
offline_thumbnail: ""
uuid: d8628648-b84e-490d-b7ca-eb6bff2419b8
updated: 1484310549
title: insecurity
categories:
    - Dictionary
---
insecurity
     n 1: the state of being subject to danger or injury [ant: {security}]
     2: the anxiety you experience when you feel vulnerable and
        insecure
