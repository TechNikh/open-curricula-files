---
version: 1
type: video
provider: YouTube
id: 7ynX_F-SwNY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/C-4%20Photosynthesis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 994028ae-feee-42d0-ba8a-2fd1daa55199
updated: 1486069583
title: C-4 Photosynthesis
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/7ynX_F-SwNY/default.jpg
    - https://i3.ytimg.com/vi/7ynX_F-SwNY/1.jpg
    - https://i3.ytimg.com/vi/7ynX_F-SwNY/2.jpg
    - https://i3.ytimg.com/vi/7ynX_F-SwNY/3.jpg
---
C-4 Photosynthesis: How some plants avoid photorespiration
More free lessons at: http://www.khanacademy.org/video?v=7ynX_F-SwNY
