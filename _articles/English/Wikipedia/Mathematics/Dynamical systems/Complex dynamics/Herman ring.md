---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Herman_ring
offline_file: ""
offline_thumbnail: ""
uuid: 9a469d8d-6dc4-4764-b74a-01c7d3bf9244
updated: 1484308727
title: Herman ring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Herman_Standard.png
tags:
    - Formal definition
    - Name
    - Function
    - Examples
categories:
    - Complex dynamics
---
In the mathematical discipline known as complex dynamics, the Herman ring is a Fatou component.[1] where the rational function is conformally conjugate to an irrational rotation of the standard annulus.
