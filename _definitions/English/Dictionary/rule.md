---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rule
offline_file: ""
offline_thumbnail: ""
uuid: 5c4f60e3-af48-42e9-b2ae-e7394284986e
updated: 1484310216
title: rule
categories:
    - Dictionary
---
rule
     n 1: a principle or condition that customarily governs behavior;
          "it was his rule to take a walk before breakfast";
          "short haircuts were the regulation" [syn: {regulation}]
     2: something regarded as a normative example; "the convention
        of not naming the main character"; "violence is the rule
        not the exception"; "his formula for impressing visitors"
        [syn: {convention}, {normal}, {pattern}, {formula}]
     3: prescribed guide for conduct or action [syn: {prescript}]
     4: (linguistics) a rule describing (or prescribing) a
        linguistic practice [syn: {linguistic rule}]
     5: a basic generalization that is accepted as true and ...
