---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_function
offline_file: ""
offline_thumbnail: ""
uuid: a59e5a92-2f1d-44f0-af2e-977c2ac5fe9a
updated: 1484308588
title: Symmetric function
tags:
    - Symmetrization
    - Examples
    - Applications
    - U-statistics
categories:
    - Combinatorics
---
In mathematics, a symmetric function of n variables is one whose value at any n-tuple of arguments is the same as its value at any permutation of that n-tuple. So, if e.g. 
  
    
      
        f
        (
        
          
            x
          
        
        )
        =
        f
        (
        
          x
          
            1
          
        
        ,
        
          x
          
            2
          
        
        ,
        
          x
          
            3
          
        
        )
      
    
    {\displaystyle f({\mathbf {x}})=f(x_{1},x_{2},x_{3})}
  
, the function can be symmetric on all its variables, or just on 
  
    
      
        (
      ...
