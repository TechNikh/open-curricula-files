---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attendance
offline_file: ""
offline_thumbnail: ""
uuid: 88801b95-7516-495a-9f71-27fd57302472
updated: 1484310447
title: attendance
categories:
    - Dictionary
---
attendance
     n : the act of being present (at a meeting or event etc.) [syn:
         {attending}] [ant: {nonattendance}]
