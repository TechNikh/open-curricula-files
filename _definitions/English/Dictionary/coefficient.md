---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coefficient
offline_file: ""
offline_thumbnail: ""
uuid: 881e5af3-3899-4f40-bada-f39344a6d070
updated: 1484310371
title: coefficient
categories:
    - Dictionary
---
coefficient
     n : a constant number that serves as a measure of some property
         or characteristic
