---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitching
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484545441
title: pitching
categories:
    - Dictionary
---
pitching
     n 1: (baseball) playing the position of pitcher on a baseball
          team
     2: abrupt up-and-down motion (as caused by a ship or other
        conveyance); "the pitching and tossing was quite exciting"
        [syn: {lurch}, {pitch}]
