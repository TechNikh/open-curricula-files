---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caloric
offline_file: ""
offline_thumbnail: ""
uuid: cb2fccc5-f0e1-4cde-aa10-72f4e938bb6a
updated: 1484308464
title: Caloric
categories:
    - Uncategorized
---
Caloric Corporation began as the Klein Stove Company in Philadelphia in 1890.[1] The Caloric brand was introduced in 1903.[2] It was reorganized in 1946 as the Caloric Stove Company in Topton, Pennsylvania.[3] The company was renamed Caloric Appliance Corp. in 1954 and became famous for offering a complete package of kitchen appliances in the 1950s and 1960s. Its most popular product was their built-in wall oven. In 1967, it was acquired by Raytheon Corporation which also owned Amana refrigerators and Speed Queen laundry products. By the early 1990s, Caloric was absorbed into Amana and the Caloric brand was phased out. The Topton plant was shut down in 1991.[4] In 1997 the company was ...
