---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complete_coloring
offline_file: ""
offline_thumbnail: ""
uuid: a7eaa6e7-a388-4127-874f-fcd0d3539296
updated: 1484309376
title: Complete coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Complete_coloring_clebsch_graph.svg.png
tags:
    - Complexity theory
    - Algorithms
    - Special classes of graphs
categories:
    - Graph coloring
---
In graph theory, complete coloring is the opposite of harmonious coloring in the sense that it is a vertex coloring in which every pair of colors appears on at least one pair of adjacent vertices. Equivalently, a complete coloring is minimal in the sense that it cannot be transformed into a proper coloring with fewer colors by merging pairs of color classes. The achromatic number ψ(G) of a graph G is the maximum number of colors possible in any complete coloring of G.
