---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ingredient
offline_file: ""
offline_thumbnail: ""
uuid: 67c042c7-3ceb-4bf4-ab40-8196e80c864b
updated: 1484310384
title: ingredient
categories:
    - Dictionary
---
ingredient
     n 1: a component of a mixture or compound
     2: an abstract part of something; "jealousy was a component of
        his character"; "two constituents of a musical composition
        are melody and harmony"; "the grammatical elements of a
        sentence"; "a key factor in her success"; "humor: an
        effective ingredient of a speech" [syn: {component}, {constituent},
         {element}, {factor}]
     3: food that is a component of a mixture in cooking; "the
        recipe lists all the fixings for a salad" [syn: {fixings}]
