---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emission
offline_file: ""
offline_thumbnail: ""
uuid: 6d5a6099-9bc9-4678-a5e6-600b24299b57
updated: 1484310375
title: emission
categories:
    - Dictionary
---
emission
     n 1: the act of emitting; causing to flow forth [syn: {emanation}]
     2: a substance that is emitted or released [syn: {discharge}]
     3: the release of electrons from parent atoms
     4: any of several bodily processes by which substances go out
        of the body; "the discharge of pus" [syn: {discharge}, {expelling}]
     5: the occurrence of a flow of water (as from a pipe)
