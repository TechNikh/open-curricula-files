---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earn
offline_file: ""
offline_thumbnail: ""
uuid: d0bbe208-b19b-4d90-89e8-aed1a92979d9
updated: 1484310445
title: earn
categories:
    - Dictionary
---
earn
     v 1: earn on some commercial or business transaction; earn as
          salary or wages; "How much do you make a month in your
          new job?"; "She earns a lot in her new job"; "this
          merger brought in lots of money"; "He clears $5,000 each
          month" [syn: {gain}, {take in}, {clear}, {make}, {realize},
           {realise}, {pull in}, {bring in}]
     2: acquire or deserve by one's efforts or actions [syn: {garner}]
