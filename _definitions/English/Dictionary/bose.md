---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bose
offline_file: ""
offline_thumbnail: ""
uuid: 66914777-8473-4432-90b0-ede6c51770cf
updated: 1484310587
title: bose
categories:
    - Dictionary
---
Bose
     n : Indian physicist who with Albert Einstein proposed
         statistical laws based on the indistinguishability of
         particles; led to the description of fundamental
         particles that later came to be known as bosons [syn: {Satyendra
         N. Bose}, {Satyendra Nath Bose}]
