---
version: 1
type: video
provider: YouTube
id: E_1gEtiGPNI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Quotient%20rule%20and%20common%20derivatives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a38a2561-e5be-4196-9e03-61e06f6b4fe1
updated: 1486069692
title: Quotient rule and common derivatives
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/E_1gEtiGPNI/default.jpg
    - https://i3.ytimg.com/vi/E_1gEtiGPNI/1.jpg
    - https://i3.ytimg.com/vi/E_1gEtiGPNI/2.jpg
    - https://i3.ytimg.com/vi/E_1gEtiGPNI/3.jpg
---
