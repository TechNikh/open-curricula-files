---
version: 1
type: video
provider: YouTube
id: Za7G_eWKiF4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Independent%20or%20dependent%20probability%20event.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 56e5f6dd-6f93-45d6-868c-07043fd0e8d5
updated: 1486069559
title: Independent or dependent probability event?
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Za7G_eWKiF4/default.jpg
    - https://i3.ytimg.com/vi/Za7G_eWKiF4/1.jpg
    - https://i3.ytimg.com/vi/Za7G_eWKiF4/2.jpg
    - https://i3.ytimg.com/vi/Za7G_eWKiF4/3.jpg
---
