---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stirrer
offline_file: ""
offline_thumbnail: ""
uuid: 54bd66b9-9ad2-464f-851a-ec172935003e
updated: 1484310226
title: stirrer
categories:
    - Dictionary
---
stirrer
     n 1: a person who spreads frightening rumors and stirs up trouble
          [syn: {scaremonger}]
     2: an implement used for stirring
