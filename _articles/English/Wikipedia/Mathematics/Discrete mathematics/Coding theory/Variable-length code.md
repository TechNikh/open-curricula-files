---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Variable-length_code
offline_file: ""
offline_thumbnail: ""
uuid: 55e8d733-df5c-4d85-8d2e-1e2984c2e69c
updated: 1484308572
title: Variable-length code
tags:
    - Codes and their extensions
    - Classes of variable-length codes
    - Non-singular codes
    - Uniquely decodable codes
    - Prefix codes
    - Advantages
    - Notes
categories:
    - Coding theory
---
Variable-length codes can allow sources to be compressed and decompressed with zero error (lossless data compression) and still be read back symbol by symbol. With the right coding strategy an independent and identically-distributed source may be compressed almost arbitrarily close to its entropy. This is in contrast to fixed length coding methods, for which data compression is only possible for large blocks of data, and any compression beyond the logarithm of the total number of possibilities comes with a finite (though perhaps arbitrarily small) probability of failure.
