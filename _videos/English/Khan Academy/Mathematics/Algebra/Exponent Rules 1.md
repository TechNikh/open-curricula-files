---
version: 1
type: video
provider: YouTube
id: kSYJxGqOcjA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Exponent%20Rules%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: efa9cbf8-be14-4ab3-a376-3b98b02409a5
updated: 1486069567
title: Exponent Rules 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/kSYJxGqOcjA/default.jpg
    - https://i3.ytimg.com/vi/kSYJxGqOcjA/1.jpg
    - https://i3.ytimg.com/vi/kSYJxGqOcjA/2.jpg
    - https://i3.ytimg.com/vi/kSYJxGqOcjA/3.jpg
---
