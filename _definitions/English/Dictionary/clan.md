---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clan
offline_file: ""
offline_thumbnail: ""
uuid: 71dd76f0-514b-49b8-9647-57af925e501a
updated: 1484310545
title: clan
categories:
    - Dictionary
---
clan
     n : group of people related by blood or marriage [syn: {kin}, {kin
         group}, {kinship group}, {kindred}, {tribe}]
