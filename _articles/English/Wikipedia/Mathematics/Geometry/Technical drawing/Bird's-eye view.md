---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bird%27s-eye_view'
offline_file: ""
offline_thumbnail: ""
uuid: eeb0869d-860a-4149-9b3f-f0872657732f
updated: 1484309325
title: "Bird's-eye view"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Friedhof_Melaten_in_K%25C3%25B6ln_%252823785443871%2529.jpg'
tags:
    - Terminology
    - Gallery
    - "Bird's-flight view"
categories:
    - Technical drawing
---
A bird's-eye view is an elevated view of an object from above, with a perspective as though the observer were a bird, often used in the making of blueprints, floor plans and maps.
