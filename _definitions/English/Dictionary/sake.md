---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sake
offline_file: ""
offline_thumbnail: ""
uuid: 5b983d07-8f50-4778-87f3-00d7ac9cc765
updated: 1484310279
title: sake
categories:
    - Dictionary
---
sake
     n 1: a reason for wanting something done; "for your sake"; "died
          for the sake of his country"; "in the interest of
          safety"; "in the common interest" [syn: {interest}]
     2: Japanese alcoholic beverage made from fermented rice;
        usually served hot [syn: {saki}, {rice beer}]
     3: the purpose of achieving or obtaining; "for the sake of
        argument"
