---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sec
offline_file: ""
offline_thumbnail: ""
uuid: ee8b9e90-a24a-4ad1-97b1-e5604f2be467
updated: 1484310154
title: sec
categories:
    - Dictionary
---
sec
     adj : (of champagne) moderately dry [syn: {unsweet}]
     n 1: 1/60 of a minute; the basic unit of time adopted under the
          Systeme International d'Unites [syn: {second}, {s}]
     2: ratio of the hypotenuse to the adjacent side of a
        right-angled triangle [syn: {secant}]
     3: an independent federal agency that oversees the exchange of
        securities to protect investors [syn: {Securities and
        Exchange Commission}]
