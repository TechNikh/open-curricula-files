---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moef
offline_file: ""
offline_thumbnail: ""
uuid: 4ed8fefb-c2bd-4c7b-ab74-f331f82bcca7
updated: 1484308337
title: Ministry of Environment, Forest and Climate Change
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/70px-Emblem_of_India.svg.png
tags:
    - History
    - Organisation
    - List of environment ministers of India
categories:
    - Uncategorized
---
The Ministry of Environment, Forest and Climate Change (MoEFCC) is an Indian government ministry. The Minister of Environment, Forest and Climate Change holds cabinet rank as a member of the Council of Ministers. The ministry portfolio is currently held by Anil Madhav Dave, Union Minister of Environment, Forest and Climate Change.
