---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Position_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: a7ef4f99-ca7b-4d02-ae71-ef1437621cc6
updated: 1484309059
title: Position (poker)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Example_of_position-sm.jpg
tags:
    - "Position in Texas hold 'em"
    - "Texas hold 'em example"
categories:
    - Poker strategy
---
Position in poker refers to the order in which players are seated around the table and the related poker strategy implications. Players who act first are in "early position"; players who act later are in "late position"; players who act in between are in "middle position".[1] A player "has position" on opponents acting before him and is "out of position" to opponents acting after him.[2] Because players act in clockwise order, a player "has position" on opponents seated to his right, except when the opponent has the button and certain cases in the first betting round of games with blinds.
