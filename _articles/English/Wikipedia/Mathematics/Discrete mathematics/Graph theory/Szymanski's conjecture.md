---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Szymanski%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 1a7fd16e-d3e2-428a-a662-5b881d1c1513
updated: 1484308620
title: "Szymanski's conjecture"
categories:
    - Graph theory
---
In mathematics, Szymanski's conjecture, named after Ted H. Szymanski (1989), states that every permutation on the n-dimensional doubly directed hypercube graph can be routed with edge-disjoint paths. That is, if the permutation σ matches each vertex v to another vertex σ(v), then for each v there exists a path in the hypercube graph from v to σ(v) such that no two paths for two different vertices u and v use the same edge in the same direction.
