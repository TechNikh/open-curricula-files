---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Poussin_proof
offline_file: ""
offline_thumbnail: ""
uuid: 03f2a0e2-1ed1-4cc0-84b2-8a75e94747eb
updated: 1484308661
title: Poussin proof
categories:
    - Number theory
---
In 1838, Peter Gustav Lejeune Dirichlet proved an approximate formula for the average number of divisors of all the numbers from 1 to η:
