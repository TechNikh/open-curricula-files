---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mehdi_Behzad
offline_file: ""
offline_thumbnail: ""
uuid: 9b0a7505-47d5-4b71-8ac7-826d9a072dd5
updated: 1484309456
title: Mehdi Behzad
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Behzad%2527s_Stamps.jpg'
tags:
    - Graph theory
    - Professorship
    - Institutionalizing of basic sciences
    - Popularization of mathematics
    - Awards and recognitions
    - Books
categories:
    - Graph theorists
---
Mehdi Behzad (Persian:مهدی بهزاد; born April 22, 1936) is a mathematician of Iranian origin specializing in graph theory. He introduced his total coloring theory (also known as "Behzad's conjecture" or "the total chromatic number conjecture"), which is regarded as one of the most beautiful conjectures in graph theory,[1] during his Ph.D. studies in 1965.[2] Despite active work on this problem during the last 50 years, including publication of several books and dissertations as well as hundreds of papers,[3] this conjecture remains as challenging as it is open. In fact, Behzad's conjecture now belongs to mathematics’ classic open problems.[4]
