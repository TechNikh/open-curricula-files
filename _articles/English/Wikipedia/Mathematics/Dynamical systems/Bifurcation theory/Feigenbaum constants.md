---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Feigenbaum_constants
offline_file: ""
offline_thumbnail: ""
uuid: 4ae6d60f-33fb-4f68-ad16-df00a402bda0
updated: 1484308699
title: Feigenbaum constants
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Feigenbaum.png
tags:
    - History
    - The first constant
    - Illustration
    - Non-linear maps
    - Fractals
    - The second constant
    - Properties
    - Approximations
    - Notes
categories:
    - Bifurcation theory
---
In mathematics, specifically bifurcation theory, the Feigenbaum constants are two mathematical constants which both express ratios in a bifurcation diagram for a non-linear map. They are named after the mathematician Mitchell Feigenbaum.
