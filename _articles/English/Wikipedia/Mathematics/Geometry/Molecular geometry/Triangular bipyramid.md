---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangular_bipyramid
offline_file: ""
offline_thumbnail: ""
uuid: 08bd099f-033e-40d7-850d-8b8eab336a6f
updated: 1484309282
title: Triangular bipyramid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangular_bipyramid.png
tags:
    - Dual polyhedron
    - Related polyhedra and honeycombs
categories:
    - Molecular geometry
---
In geometry, the triangular bipyramid (or dipyramid) is a type of hexahedron, being the first in the infinite set of face-transitive bipyramids. It is the dual of the triangular prism with 6 isosceles triangle faces.
