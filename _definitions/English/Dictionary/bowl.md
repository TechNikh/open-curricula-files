---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bowl
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484368082
title: bowl
categories:
    - Dictionary
---
bowl
     n 1: a round vessel that is open at the top; used for holding
          fruit or liquids or for serving food
     2: a concave shape with an open top [syn: {trough}]
     3: a dish that is round and open at the top for serving foods
     4: the quantity contained in a bowl [syn: {bowlful}]
     5: a large structure for open-air sports or entertainments
        [syn: {stadium}, {arena}, {sports stadium}]
     6: a wooden ball (with flattened sides) used in the game of
        bowls
     7: a small round container that is open at the top for holding
        tobacco [syn: {pipe bowl}]
     v 1: roll (a ball)
     2: engage in the sport of bowling; "My parents like to bowl on
        ...
