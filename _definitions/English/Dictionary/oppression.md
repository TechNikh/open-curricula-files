---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oppression
offline_file: ""
offline_thumbnail: ""
uuid: af54b8b0-165c-45a2-8b25-31572ac7d72c
updated: 1484310593
title: oppression
categories:
    - Dictionary
---
oppression
     n 1: the act of subjugating by cruelty; "the tyrant's oppression
          of the people" [syn: {subjugation}]
     2: the state of being kept down by unjust use of force or
        authority: "after years of oppression they finally
        revolted"
     3: a feeling of being oppressed [syn: {oppressiveness}]
