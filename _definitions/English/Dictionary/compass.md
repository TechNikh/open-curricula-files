---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compass
offline_file: ""
offline_thumbnail: ""
uuid: 091dc106-0e78-472b-a985-80860cef2088
updated: 1484310194
title: compass
categories:
    - Dictionary
---
compass
     n 1: navigational instrument for finding directions
     2: an area in which something acts or operates or has power or
        control: "the range of a supersonic jet"; "the ambit of
        municipal legislation"; "within the compass of this
        article"; "within the scope of an investigation"; "outside
        the reach of the law"; "in the political orbit of a world
        power" [syn: {scope}, {range}, {reach}, {orbit}, {ambit}]
     3: the limit of capability; "within the compass of education"
        [syn: {range}, {reach}, {grasp}]
     4: drafting instrument used for drawing circles
     v 1: bring about; accomplish; "This writer attempts more than his
          ...
