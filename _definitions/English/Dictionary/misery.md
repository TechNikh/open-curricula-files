---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misery
offline_file: ""
offline_thumbnail: ""
uuid: 30d42b90-f997-4fa0-8cfd-b14cb1305678
updated: 1484310181
title: misery
categories:
    - Dictionary
---
misery
     n 1: a state of ill-being due to affliction or misfortune; "the
          misery and wretchedness of those slums is intolerable"
          [syn: {wretchedness}]
     2: a feeling of intense unhappiness; "she was exhausted by her
        misery and grief"
