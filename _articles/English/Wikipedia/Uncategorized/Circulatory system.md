---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardio_vascular
offline_file: ""
offline_thumbnail: ""
uuid: b5e8d5a6-cc90-4467-a3b2-e38b5785b31b
updated: 1484308372
title: Circulatory system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Circulatory_System_en.svg.png
tags:
    - Structure
    - Cardiovascular system
    - Arteries
    - Capillaries
    - Veins
    - Coronary vessels
    - Portal veins
    - Heart
    - lungs
    - Systemic circulation
    - Brain
    - Kidneys
    - Lymphatic system
    - Physiology
    - Development
    - Arterial development
    - Venous development
    - Clinical significance
    - Cardiovascular disease
    - Measurement techniques
    - Surgery
    - Society and culture
    - Other animals
    - Other vertebrates
    - Open circulatory system
    - Absence of circulatory system
    - History
categories:
    - Uncategorized
---
The circulatory system, also called the cardiovascular system or the vascular system, is an organ system that permits blood to circulate and transport nutrients (such as amino acids and electrolytes), oxygen, carbon dioxide, hormones, and blood cells to and from the cells in the body to provide nourishment and help in fighting diseases, stabilize temperature and pH, and maintain homeostasis. The study of the blood flow is called hemodynamics. The study of the properties of the blood flow is called hemorheology.
