---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coronary
offline_file: ""
offline_thumbnail: ""
uuid: a2c12e59-518e-4164-8c1c-64b1d3fd7629
updated: 1484310160
title: coronary
categories:
    - Dictionary
---
coronary
     adj : surrounding like a crown (especially of the blood vessels
           surrounding the heart); "coronary arteries"
     n : obstruction of blood flow in a coronary artery by a blood
         clot (thrombus) [syn: {coronary thrombosis}]
