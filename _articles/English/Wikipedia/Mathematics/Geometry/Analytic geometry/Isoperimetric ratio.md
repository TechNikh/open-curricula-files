---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isoperimetric_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 81f2ec68-0443-481f-9efa-07d7dc572fda
updated: 1484309165
title: Isoperimetric ratio
categories:
    - Analytic geometry
---
In analytic geometry, the isoperimetric ratio of a simple closed curve in the Euclidean plane is the ratio L2/A, where L is the length of the curve and A is its area. It is a dimensionless quantity that is invariant under similarity transformations of the curve.
