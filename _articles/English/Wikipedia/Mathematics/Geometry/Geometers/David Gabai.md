---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Gabai
offline_file: ""
offline_thumbnail: ""
uuid: 523d8b28-1d15-44b2-aca4-9773f21d326a
updated: 1484309083
title: David Gabai
tags:
    - Biography
    - Honours and awards
    - Work
    - Selected works
categories:
    - Geometers
---
David Gabai, a mathematician, is the Hughes-Rogers Professor of Mathematics at Princeton University.[2] Focused on low-dimensional topology and hyperbolic geometry, he is a leading researcher in those subjects.
