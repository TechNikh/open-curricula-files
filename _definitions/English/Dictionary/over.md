---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/over
offline_file: ""
offline_thumbnail: ""
uuid: fef45671-5906-4351-adce-3c9da74ca8ac
updated: 1484310341
title: over
categories:
    - Dictionary
---
over
     adj : having come or been brought to a conclusion; "the harvesting
           was complete"; "the affair is over, ended, finished";
           "the abruptly terminated interview" [syn: {complete}, {concluded},
            {ended}, {over(p)}, {all over}, {terminated}]
     n : (cricket) the period during which a given number of balls (6
         in England but 8 in Australia) are bowled at the batsman
         by one player from the other team from the same end of
         the pitch
     adv 1: at or to a point across intervening space etc.; "come over
            and see us some time"; "over there"
     2: throughout an area; "he is known the world over"
     3: in such a manner ...
