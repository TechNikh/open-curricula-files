---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barring
offline_file: ""
offline_thumbnail: ""
uuid: 0f456b81-1c98-440e-83be-d5bc1831887d
updated: 1484310517
title: barring
categories:
    - Dictionary
---
bar
     n 1: a room or establishment where alcoholic drinks are served
          over a counter; "he drowned his sorrows in whiskey at
          the bar" [syn: {barroom}, {saloon}, {ginmill}, {taproom}]
     2: a counter where you can obtain food or drink; "he bought a
        hot dog and a coke at the bar"
     3: a rigid piece of metal or wood; usually used as a fastening
        or obstruction or weapon; "there were bars in the windows
        to prevent escape"
     4: musical notation for a repeating pattern of musical beats;
        "the orchestra omitted the last twelve bars of the song"
        [syn: {measure}]
     5: an obstruction (usually metal) placed at the top of a goal;
    ...
