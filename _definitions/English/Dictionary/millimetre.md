---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/millimetre
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484402821
title: millimetre
categories:
    - Dictionary
---
millimetre
     n : a metric unit of length equal to one thousandth of a meter
         [syn: {millimeter}, {mm}]
