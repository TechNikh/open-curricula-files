---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curie
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653441
title: curie
categories:
    - Dictionary
---
Curie
     n 1: French chemist (born in Poland) who won two Nobel Prizes;
          one (with her husband and Henri Becquerel) for research
          on radioactivity and another for her discovery of radium
          and polonium (1867-1934) [syn: {Marie Curie}, {Madame
          Curie}, {Marya Sklodowska}]
     2: French physicist; husband of Marie Curie (1859-1906) [syn: {Pierre
        Curie}]
     3: a unit of radioactivity equal to the amount of a radioactive
        isotope that decays at the rate of 37,000,000,000
        disintegrations per second [syn: {Ci}]
