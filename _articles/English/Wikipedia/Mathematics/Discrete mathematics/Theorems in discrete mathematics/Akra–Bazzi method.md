---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Akra%E2%80%93Bazzi_method'
offline_file: ""
offline_thumbnail: ""
uuid: 8949272d-5858-4c0d-a62d-bf6b94d693f7
updated: 1484308659
title: Akra–Bazzi method
tags:
    - The formula
    - An example
    - Significance
categories:
    - Theorems in discrete mathematics
---
In computer science, the Akra–Bazzi method, or Akra–Bazzi theorem, is used to analyze the asymptotic behavior of the mathematical recurrences that appear in the analysis of divide and conquer algorithms where the sub-problems have substantially different sizes. It is a generalization of the well-known master theorem, which assumes that the sub-problems have equal size. It is named after mathematicians Mohamad Akra and Louay Bazzi.
