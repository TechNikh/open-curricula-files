---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sugarscape
offline_file: ""
offline_thumbnail: ""
uuid: 4fe480b7-e148-4ecb-a51b-d7293f1df57a
updated: 1484308822
title: Sugarscape
tags:
    - Origin
    - Principles
    - Model implementations
    - Ascape
    - NetLogo
    - SugarScape on steroids
    - Mathematica
    - MASON
categories:
    - Self-organization
---
Sugarscape is a model artificially intelligent agent-based social simulation following some or all rules presented by Joshua M. Epstein & Robert Axtell in their book Growing Artificial Societies.[1]
