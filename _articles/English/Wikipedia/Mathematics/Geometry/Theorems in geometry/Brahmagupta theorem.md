---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brahmagupta_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 9134b43c-302a-46c9-b0fc-e81df48f0126
updated: 1484309111
title: Brahmagupta theorem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Brahmaguptra%2527s_theorem.svg.png'
tags:
    - Proof
categories:
    - Theorems in geometry
---
In geometry, Brahmagupta's theorem states that if a cyclic quadrilateral is orthodiagonal (that is, has perpendicular diagonals), then the perpendicular to a side from the point of intersection of the diagonals always bisects the opposite side.[1] It is named after the Indian mathematician Brahmagupta.[2]
