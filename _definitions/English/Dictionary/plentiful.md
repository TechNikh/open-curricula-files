---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plentiful
offline_file: ""
offline_thumbnail: ""
uuid: 76e2a1ad-114f-46a3-96b6-d48c93a12c07
updated: 1484310244
title: plentiful
categories:
    - Dictionary
---
plentiful
     adj 1: existing in great number or quantity; "rhinoceroses were
            once plentiful here"
     2: affording an abundant supply; "had ample food for the
        party"; "copious provisions"; "food is plentiful"; "a
        plenteous grape harvest"; "a rich supply" [syn: {ample}, {copious},
         {plenteous}, {rich}]
     3: producing in abundance; "the bountiful earth"; "a plentiful
        year"; "fruitful soil" [syn: {bountiful}]
