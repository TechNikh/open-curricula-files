---
version: 1
type: video
provider: YouTube
id: YQfWiDlFEcA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/ATP-%20Adenosine%20Triphosphate.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 61dbd2ae-2b71-494f-ac8a-0987c2fdabec
updated: 1486069581
title: 'ATP- Adenosine Triphosphate'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/YQfWiDlFEcA/default.jpg
    - https://i3.ytimg.com/vi/YQfWiDlFEcA/1.jpg
    - https://i3.ytimg.com/vi/YQfWiDlFEcA/2.jpg
    - https://i3.ytimg.com/vi/YQfWiDlFEcA/3.jpg
---
Introduction to ATP or Adenosine Triphosphate
