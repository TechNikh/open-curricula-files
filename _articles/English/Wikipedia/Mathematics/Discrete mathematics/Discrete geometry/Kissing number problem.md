---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kissing_number_problem
offline_file: ""
offline_thumbnail: ""
uuid: ec33dda8-393f-41d7-a181-33e339c30594
updated: 1484308601
title: Kissing number problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Kissing-3d.png
tags:
    - Known greatest kissing numbers
    - Some known bounds
    - Generalization
    - Mathematical statement
    - Notes
categories:
    - Discrete geometry
---
In geometry, a kissing number is defined as the number of non-overlapping unit spheres that can be arranged such that they each touch another given unit sphere. For a lattice packing the kissing number is the same for every sphere, but for an arbitrary sphere packing the kissing number may vary from one sphere to another. Other names for kissing number that have been used are Newton number (after the originator of the problem), and contact number.
