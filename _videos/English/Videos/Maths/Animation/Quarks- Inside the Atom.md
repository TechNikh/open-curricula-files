---
version: 1
type: video
provider: YouTube
id: SMgi2j9Ks9k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: aaf9dedf-02ed-4d9d-a8c8-a18fea754fc7
updated: 1486069644
title: 'Quarks- Inside the Atom'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/SMgi2j9Ks9k/default.jpg
    - https://i3.ytimg.com/vi/SMgi2j9Ks9k/1.jpg
    - https://i3.ytimg.com/vi/SMgi2j9Ks9k/2.jpg
    - https://i3.ytimg.com/vi/SMgi2j9Ks9k/3.jpg
---
