---
version: 1
type: video
provider: YouTube
id: Ds8ijPsg26g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6b63c4cb-11ad-4d64-a481-0d459e25ab26
updated: 1486069623
title: How To Calculate Cube Roots In Your Head
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ds8ijPsg26g/default.jpg
    - https://i3.ytimg.com/vi/Ds8ijPsg26g/1.jpg
    - https://i3.ytimg.com/vi/Ds8ijPsg26g/2.jpg
    - https://i3.ytimg.com/vi/Ds8ijPsg26g/3.jpg
---
