---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_cone
offline_file: ""
offline_thumbnail: ""
uuid: 73a86ff0-93ec-440d-b700-fb622a30dbca
updated: 1484309189
title: Convex cone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convex_cone_illust.svg.png
tags:
    - Definition
    - Examples
    - Special Examples
    - Affine convex cones
    - Half-spaces
    - Polyhedral and finitely generated cones
    - Blunt, pointed, flat, salient, and proper cones
    - Dual cone
    - Partial order defined by a convex cone
    - Notes
categories:
    - Convex geometry
---
In linear algebra, a convex cone is a subset of a vector space over an ordered field that is closed under linear combinations with positive coefficients.
