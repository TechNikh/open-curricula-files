---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accomplished
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441821
title: accomplished
categories:
    - Dictionary
---
accomplished
     adj 1: highly skilled; "an accomplished pianist"; "a complete
            musician" [syn: {complete}]
     2: successfully completed or brought to an end; "his mission
        accomplished he took a vacation"; "the completed project";
        "the joy of a realized ambition overcame him" [syn: {completed},
         {realized}, {realised}]
     3: settled securely and unconditionally; "that smoking causes
        health problems is an accomplished fact" [syn: {effected},
         {established}]
