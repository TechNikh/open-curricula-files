---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assembled
offline_file: ""
offline_thumbnail: ""
uuid: cd741e8c-1262-431e-8f95-968d995831ae
updated: 1484310525
title: assembled
categories:
    - Dictionary
---
assembled
     adj 1: brought together into a group or crowd; "the accumulated
            letters in my office" [syn: {accumulated}, {amassed},
            {collected}, {congregate}, {massed}]
     2: formed by fitting or joining components together [syn: {built(a)},
         {made-up}]
