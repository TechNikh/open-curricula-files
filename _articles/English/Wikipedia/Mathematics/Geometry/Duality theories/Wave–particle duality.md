---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wave%E2%80%93particle_duality'
offline_file: ""
offline_thumbnail: ""
uuid: 7a1372c6-9820-4112-8f57-99ce11d33788
updated: 1484309241
title: Wave–particle duality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Young_Diffraction.png
tags:
    - Brief history of wave and particle viewpoints
    - Turn of the 20th century and the paradigm shift
    - Particles of electricity
    - Radiation quantization
    - Photoelectric effect illuminated
    - "Einstein's explanation of the photoelectric effect"
    - "De Broglie's wavelength"
    - "Heisenberg's uncertainty principle"
    - de Broglie–Bohm theory
    - Wave behavior of large objects
    - Treatment in modern quantum mechanics
    - Visualization
    - Alternative views
    - Both-particle-and-wave view
    - Wave-only view
    - Particle-only view
    - Neither-wave-nor-particle view
    - Relational approach to wave–particle duality
    - Applications
    - Notes and references
categories:
    - Duality theories
---
Wave–particle duality is the concept that every elementary particle or quantic entity may be partly described in terms not only of particles, but also of waves. It expresses the inability of the classical concepts "particle" or "wave" to fully describe the behavior of quantum-scale objects. As Albert Einstein wrote: "It seems as though we must use sometimes the one theory and sometimes the other, while at times we may use either. We are faced with a new kind of difficulty. We have two contradictory pictures of reality; separately neither of them fully explains the phenomena of light, but together they do".[1]
