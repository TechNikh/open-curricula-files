---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stacked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331061
title: stacked
categories:
    - Dictionary
---
stacked
     adj 1: arranged in a stack
     2: well or attractively formed with respect to physique [syn: {built(p)},
         {stacked(p)}, {well-stacked}]
