---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forty-five
offline_file: ""
offline_thumbnail: ""
uuid: 4075be84-720d-4ed6-a3b0-b35f1b2a5e47
updated: 1484310181
title: forty-five
categories:
    - Dictionary
---
forty-five
     adj : being five more than forty [syn: {45}, {xlv}]
     n : a .45-caliber pistol
