---
version: 1
type: article
id: https://en.wikipedia.org/wiki/R._Duncan_Luce
offline_file: ""
offline_thumbnail: ""
uuid: ade0edc9-5f0c-4075-ab27-423536ac542a
updated: 1484308513
title: R. Duncan Luce
tags:
    - Books by Luce
    - Archival collections
    - Other
categories:
    - Game theorists
---
Robert Duncan Luce (May 16, 1925 – August 11, 2012)[1] was a renowned mathematician and social scientist, and one of the most preeminent figures in the field of mathematical psychology. At the end of his life, he held the position of Distinguished Research Professor of Cognitive Science at the University of California, Irvine.[2]
