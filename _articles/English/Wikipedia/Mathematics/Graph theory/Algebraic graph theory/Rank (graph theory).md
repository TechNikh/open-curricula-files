---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rank_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: dcdf63f8-fd9a-45ee-af4d-cca621fd74e3
updated: 1484309376
title: Rank (graph theory)
categories:
    - Algebraic graph theory
---
In graph theory, a branch of mathematics, the rank of an undirected graph has two unrelated definitions. Let n equal the number of vertices of the graph.
