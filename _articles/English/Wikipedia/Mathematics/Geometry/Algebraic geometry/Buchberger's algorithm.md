---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Buchberger%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: aee94d56-5232-458d-aca3-b45f3bbd4215
updated: 1484309136
title: "Buchberger's algorithm"
tags:
    - Algorithm
    - Complexity
    - Notes
categories:
    - Algebraic geometry
---
In computational algebraic geometry and computational commutative algebra, Buchberger's algorithm is a method of transforming a given set of generators for a polynomial ideal into a Gröbner basis with respect to some monomial order. It was invented by Austrian mathematician Bruno Buchberger. One can view it as a generalization of the Euclidean algorithm for univariate GCD computation and of Gaussian elimination for linear systems.
