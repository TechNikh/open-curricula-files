---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unskilled
offline_file: ""
offline_thumbnail: ""
uuid: 47c525da-0571-46c6-8011-9634b31cdecb
updated: 1484310515
title: unskilled
categories:
    - Dictionary
---
unskilled
     adj 1: not having or showing or requiring special skill or
            proficiency; "unskilled in the art of rhetoric"; "an
            enthusiastic but unskillful mountain climber";
            "unskilled labor"; "workers in unskilled occupations
            are finding fewer and fewer job opportunities";
            "unskilled workmanship" [ant: {skilled}]
     2: lacking professional skill or expertise; "a very amateurish
        job"; "inexpert but conscientious efforts"; "an unskilled
        painting" [syn: {amateurish}, {amateur}, {inexpert}]
     3: not doing a good job; "incompetent at chess" [syn: {incompetent}]
