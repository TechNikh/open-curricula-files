---
version: 1
type: video
provider: YouTube
id: V1Ary3Qzygk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 92b9a11e-8c91-4688-bd8b-5a94587d69c7
updated: 1486069628
title: Hindi Grammar Tenses (Kaal Ke Prakaar) | Learn Hindi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/V1Ary3Qzygk/default.jpg
    - https://i3.ytimg.com/vi/V1Ary3Qzygk/1.jpg
    - https://i3.ytimg.com/vi/V1Ary3Qzygk/2.jpg
    - https://i3.ytimg.com/vi/V1Ary3Qzygk/3.jpg
---
