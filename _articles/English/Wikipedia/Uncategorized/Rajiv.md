---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rajiv
offline_file: ""
offline_thumbnail: ""
uuid: 49cbc720-e5f5-4224-9b27-207b745b3813
updated: 1484308450
title: Rajiv
categories:
    - Uncategorized
---
Rajeev (Hindi: राजीव) is a popular Indian male name, also spelt Rajeev or Rajib. in Hindu texts. In Ramayana, Lord Rama is also referred as "Rajiv Lochan", meaning "one whose eyes are like lotus flowers". It also referred as a quality of the flower lotus. It is said that lotus flower though grown in mud water doesn't accumulate the mud particles onto it such is the quality being described rajiv. Today, in several Indian languages, including Hindi, Telugu, Bengali, Madheshi, Assamese, Marathi, and Kannada the word refers to "lotus flower" (Nelumbo nucifera).
