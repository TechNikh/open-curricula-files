---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Climate_as_complex_networks
offline_file: ""
offline_thumbnail: ""
uuid: 851bc5a4-c2d4-45fa-a57e-6b790a1466ca
updated: 1484309431
title: Climate as complex networks
tags:
    - Construction of Climate Networks
    - Applications of Climate Networks
    - Computational Issues and Challenges
categories:
    - Network theory
---
The field of Complex Networks has emerged as an important area of science to generate novel insights into nature of complex systems.[1] [2] The application of the theory to Climate Science is a young and emerging field. ,[3] ,[4] ,[5] [6] To identify and analyze patterns in global climate, scientists model the climate data as Complex Networks.
