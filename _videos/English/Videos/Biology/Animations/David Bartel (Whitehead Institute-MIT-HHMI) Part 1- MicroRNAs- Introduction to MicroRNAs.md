---
version: 1
type: video
provider: YouTube
id: dupzE66J8u4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/David%20Bartel%20%28Whitehead%20Institute-MIT-HHMI%29%20Part%201-%20MicroRNAs-%20Introduction%20to%20MicroRNAs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: edc21973-82cb-459d-ad4f-a3a6f056dadf
updated: 1486069672
title: 'David Bartel (Whitehead Institute/MIT/HHMI) Part 1- MicroRNAs- Introduction to MicroRNAs'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dupzE66J8u4/default.jpg
    - https://i3.ytimg.com/vi/dupzE66J8u4/1.jpg
    - https://i3.ytimg.com/vi/dupzE66J8u4/2.jpg
    - https://i3.ytimg.com/vi/dupzE66J8u4/3.jpg
---
