---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sum
offline_file: ""
offline_thumbnail: ""
uuid: 14bd1a08-54a1-4862-9805-5d7f7e6b7d68
updated: 1484310275
title: sum
categories:
    - Dictionary
---
sum
     n 1: a quantity of money; "he borrowed a large sum"; "the amount
          he had in cash was insufficient" [syn: {sum of money}, {amount},
           {amount of money}]
     2: a quantity obtained by addition [syn: {amount}, {total}]
     3: the final aggregate; "the sum of all our troubles did not
        equal the misery they suffered" [syn: {summation}, {sum
        total}]
     4: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {substance}, {core},
         {center}, {essence}, {gist}, {heart}, ...
