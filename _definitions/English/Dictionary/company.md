---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/company
offline_file: ""
offline_thumbnail: ""
uuid: 1335b470-d96d-4f32-96c6-4317c8bfc212
updated: 1484310249
title: company
categories:
    - Dictionary
---
company
     n 1: an institution created to conduct business; "he only invests
          in large well-established companies"; "he started the
          company in his garage"
     2: organization of performers and associated personnel
        (especially theatrical); "the traveling company all stayed
        at the same hotel" [syn: {troupe}]
     3: the state of being with someone; "he missed their company";
        "he enjoyed the society of his friends" [syn: {companionship},
         {fellowship}, {society}]
     4: small military unit; usually two or three platoons
     5: a band of people associated temporarily in some activity;
        "they organized a party to search for food"; ...
