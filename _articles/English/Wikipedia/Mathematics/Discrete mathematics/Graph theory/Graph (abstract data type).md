---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_(abstract_data_type)
offline_file: ""
offline_thumbnail: ""
uuid: 0b007f52-8565-4f72-af17-81525bf438e7
updated: 1484308612
title: Graph (abstract data type)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/125px-Directed.svg.png
tags:
    - Operations
    - Representations
categories:
    - Graph theory
---
A graph data structure consists of a finite (and possibly mutable) set of vertices or nodes or points, together with a set of unordered pairs of these vertices for an undirected graph or a set of ordered pairs for a directed graph. These pairs are known as edges, arcs, or lines for an undirected graph and as arrows, directed edges, directed arcs, or directed lines for a directed graph. The vertices may be part of the graph structure, or may be external entities represented by integer indices or references.
