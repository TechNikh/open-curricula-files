---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woven
offline_file: ""
offline_thumbnail: ""
uuid: 28dd8bc7-e80d-4ce6-ab17-ec49fe64a00b
updated: 1484310148
title: woven
categories:
    - Dictionary
---
weave
     n : pattern of weaving or structure of a fabric
     v 1: interlace by or as it by weaving [syn: {interweave}] [ant: {unweave}]
     2: create a piece of cloth by interlacing strands of fabric,
        such as wool or cotton; "tissue textiles" [syn: {tissue}]
     3: sway to and fro [syn: {waver}]
     4: to move or cause to move in a sinuous, spiral, or circular
        course; "the river winds through the hills"; "the path
        meanders through the vineyards"; "sometimes, the gout
        wanders through the entire body" [syn: {wind}, {thread}, {meander},
         {wander}]
     [also: {woven}, {wove}]
