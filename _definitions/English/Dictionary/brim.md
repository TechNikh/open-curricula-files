---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brim
offline_file: ""
offline_thumbnail: ""
uuid: 97cb26a6-618f-441b-9a47-6909ecb650df
updated: 1484310210
title: brim
categories:
    - Dictionary
---
brim
     n 1: the top edge of a vessel [syn: {rim}, {lip}]
     2: a circular projection that sticks outward from the crown of
        a hat
     v 1: be completely full; "His eyes brimmed with tears"
     2: fill as much as possible; "brim a cup to good fellowship"
     [also: {brimming}, {brimmed}]
