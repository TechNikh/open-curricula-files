---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dizzy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572381
title: dizzy
categories:
    - Dictionary
---
dizzy
     adj 1: having or causing a whirling sensation; liable to falling;
            "had a dizzy spell"; "a dizzy pinnacle"; "had a
            headache and felt giddy"; "a giddy precipice";
            "feeling woozy from the blow on his head"; "a
            vertiginous climb up the face of the cliff" [syn: {giddy},
             {woozy}, {vertiginous}]
     2: lacking seriousness; given to frivolity; "a dizzy blonde";
        "light-headed teenagers"; "silly giggles" [syn: {airheaded},
         {empty-headed}, {featherbrained}, {giddy}, {light-headed},
         {lightheaded}, {silly}]
     v : make dizzy or giddy; "a dizzying pace"
     [also: {dizzied}, {dizziest}, {dizzier}]
