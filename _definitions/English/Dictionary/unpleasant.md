---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unpleasant
offline_file: ""
offline_thumbnail: ""
uuid: 20b7a8f4-bca9-4c17-832a-c6b6071575ec
updated: 1484310424
title: unpleasant
categories:
    - Dictionary
---
unpleasant
     adj 1: not pleasant; "an unpleasant personality"; "unpleasant
            repercussions"; "unpleasant odors" [ant: {pleasant}]
     2: causing disapproval or protest; "a vulgar and objectionable
        person" [syn: {objectionable}, {obnoxious}]
     3: very unpleasant or annoying
