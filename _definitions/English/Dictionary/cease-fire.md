---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cease-fire
offline_file: ""
offline_thumbnail: ""
uuid: 2aa0d8f8-df2f-4f6e-b1ed-b303aa7eb864
updated: 1484310178
title: cease-fire
categories:
    - Dictionary
---
cease-fire
     n : a state of peace agreed to between opponents so they can
         discuss peace terms [syn: {armistice}, {truce}]
