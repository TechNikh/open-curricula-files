---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heated
offline_file: ""
offline_thumbnail: ""
uuid: f81fc8d5-268a-4114-bc42-1dbda41c05d5
updated: 1484310228
title: heated
categories:
    - Dictionary
---
heated
     adj 1: made warm or hot (`het' is a dialectal variant of `heated');
            "a heated swimming pool"; "wiped his heated-up face
            with a large bandana"; "he was all het up and sweaty"
            [syn: {heated up}, {het}, {het up}]
     2: marked by emotional heat; vehement; "a heated argument"
