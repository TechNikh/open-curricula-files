---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crack
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484654101
title: crack
categories:
    - Dictionary
---
crack
     adj : of the highest quality; "an ace reporter"; "a crack shot";
           "a first-rate golfer"; "a super party"; "played
           top-notch tennis"; "an athlete in tiptop condition";
           "she is absolutely tops" [syn: {ace}, {A-one}, {first-rate},
            {super}, {tiptop}, {topnotch}, {tops(p)}]
     n 1: a long narrow opening [syn: {cleft}, {crevice}, {fissure}, {scissure}]
     2: a narrow opening; "he opened the window a crack" [syn: {gap}]
     3: a long narrow depression in a surface [syn: {crevice}, {cranny},
         {fissure}, {chap}]
     4: a sudden sharp noise; "the crack of a whip"; "he heard the
        cracking of the ice"; "he can hear the snap of ...
