---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impossible
offline_file: ""
offline_thumbnail: ""
uuid: d626deb3-8b02-480b-9923-852ee25ed458
updated: 1484310395
title: impossible
categories:
    - Dictionary
---
impossible
     adj 1: not capable of occurring or being accomplished or dealt
            with; "an impossible dream"; "an impossible situation"
            [ant: {possible}]
     2: totally unlikely [syn: {inconceivable}, {out of the question},
         {unimaginable}]
     3: used of persons or their behavior; "impossible behavior";
        "insufferable insolence" [syn: {insufferable}, {unacceptable},
         {unsufferable}]
     n : something that cannot be done; "his assignment verged on the
         impossible"
