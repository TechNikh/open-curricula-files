---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/martin
offline_file: ""
offline_thumbnail: ""
uuid: d7011a45-25ed-4458-8813-2529eef05820
updated: 1484310316
title: martin
categories:
    - Dictionary
---
Martin
     n 1: French bishop who is a patron saint of France (died in 397)
          [syn: {St. Martin}]
     2: United States actor and comedian (born in 1945) [syn: {Steve
        Martin}]
     3: United States actress (1913-1990) [syn: {Mary Martin}]
     4: United States singer (1917-1995) [syn: {Dean Martin}, {Dino
        Paul Crocetti}]
     5: any of various swallows with squarish or slightly forked
        tail and long pointed wings; migrate around Martinmas
