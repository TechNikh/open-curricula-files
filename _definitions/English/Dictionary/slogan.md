---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slogan
offline_file: ""
offline_thumbnail: ""
uuid: f0755dc4-6537-4237-acb3-540568c16dd9
updated: 1484310609
title: slogan
categories:
    - Dictionary
---
slogan
     n : a favorite saying of a sect or political group [syn: {motto},
          {catchword}, {shibboleth}]
