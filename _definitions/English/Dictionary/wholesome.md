---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wholesome
offline_file: ""
offline_thumbnail: ""
uuid: 4b8558c1-7625-4cb5-a2c8-f7ccc8806eae
updated: 1484310531
title: wholesome
categories:
    - Dictionary
---
wholesome
     adj 1: conducive to or characteristic of physical or moral
            well-being; "wholesome attitude"; "wholesome
            appearance"; "wholesome food" [ant: {unwholesome}]
     2: sound or exhibiting soundness in body or mind; "exercise
        develops wholesome appetites"; "a grin on his ugly
        wholesome face"
