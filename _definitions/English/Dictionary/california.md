---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/california
offline_file: ""
offline_thumbnail: ""
uuid: 6bf96f63-d2b1-485c-aaa0-79cdd640487e
updated: 1484310549
title: california
categories:
    - Dictionary
---
California
     n : a state in the western United States on the Pacific; the 3rd
         largest state; known for earthquakes [syn: {Golden State},
          {CA}, {Calif.}]
