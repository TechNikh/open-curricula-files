---
version: 1
type: video
provider: YouTube
id: JPQjlbxPgYQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2024%20-%20Conduction.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 87be5bf5-004c-48c2-b8d1-ccf7517ddf98
updated: 1486069665
title: 'Eureka 24 - Conduction.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/JPQjlbxPgYQ/default.jpg
    - https://i3.ytimg.com/vi/JPQjlbxPgYQ/1.jpg
    - https://i3.ytimg.com/vi/JPQjlbxPgYQ/2.jpg
    - https://i3.ytimg.com/vi/JPQjlbxPgYQ/3.jpg
---
