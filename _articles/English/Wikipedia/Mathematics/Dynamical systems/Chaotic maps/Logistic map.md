---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Logistic_map
offline_file: ""
offline_thumbnail: ""
uuid: f3c37c45-8cb2-4212-8565-c954da6dfffd
updated: 1484308727
title: Logistic map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Logistic_Bifurcation_map_High_Resolution.png
tags:
    - |
        Behavior dependent on
        
        
        
        r
        
        
        {\displaystyle r}
    - Chaos and the logistic map
    - Solution in some cases
    - |
        Finding cycles of any length when
        
        
        
        r
        =
        4
        
        
        {\displaystyle r=4}
    - Notes
categories:
    - Chaotic maps
---
The logistic map is a polynomial mapping (equivalently, recurrence relation) of degree 2, often cited as an archetypal example of how complex, chaotic behaviour can arise from very simple non-linear dynamical equations. The map was popularized in a seminal 1976 paper by the biologist Robert May,[1] in part as a discrete-time demographic model analogous to the logistic equation first created by Pierre François Verhulst.[2] Mathematically, the logistic map is written
