---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hereditarily_finite_set
offline_file: ""
offline_thumbnail: ""
uuid: b8565411-305d-4a38-b9f5-017e99f5669d
updated: 1484309506
title: Hereditarily finite set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Nested_set_V4.svg.png
tags:
    - Formal definition
    - Discussion
    - "Ackermann's bijection"
    - Rado graph
categories:
    - Set theory
---
