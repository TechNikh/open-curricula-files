---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irrigation
offline_file: ""
offline_thumbnail: ""
uuid: 139ac17b-d453-46b9-9788-6ab3b5faba6c
updated: 1484310264
title: irrigation
categories:
    - Dictionary
---
irrigation
     n 1: supplying dry land with water by means of ditches etc
     2: (medicine) cleaning a wound or body organ by flushing or
        washing out with water or a medicated solution
