---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pinch_point_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: d7814674-286b-4143-bd97-04fd3ce30c88
updated: 1484309369
title: Pinch point (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Whitney_unbrella.png
categories:
    - Geometry stubs
---
The equation for the surface near a pinch point may be put in the form
