---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manava
offline_file: ""
offline_thumbnail: ""
uuid: cd660616-fdfc-4907-8b0e-c3dc00dde090
updated: 1484309090
title: Manava
categories:
    - Geometers
---
The Manava Sulbasutra is not the oldest (the one by Baudhayana is older), nor is it one of the most important, there being at least three Sulbasutras which are considered more important. Historians place his lifetime at around 750 BC.
