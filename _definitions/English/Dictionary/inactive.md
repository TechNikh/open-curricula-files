---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inactive
offline_file: ""
offline_thumbnail: ""
uuid: ad2399a2-2de6-4867-adf1-8e53ed4ab7f5
updated: 1484310405
title: inactive
categories:
    - Dictionary
---
inactive
     adj 1: not participating in a chemical reaction; "desired amounts
            of inactive chlorine"
     2: not progressing or increasing; or progressing slowly [ant: {active}]
     3: not active or exerting influence [ant: {active}]
     4: of e.g. volcanos; permanently inactive; "an extinct volcano"
        [syn: {extinct}] [ant: {active}, {dormant}]
     5: of e.g. volcanos; temporarily inactive; "a dormant volcano"
        [syn: {dormant}] [ant: {extinct}, {active}]
     6: lacking in energy or will; "Much benevolence of the passive
        order may be traced to a disinclination to inflict pain
        upon oneself"- George Meredith [syn: {passive}] [ant: {active}]
     ...
