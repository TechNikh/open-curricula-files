---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rencontres_numbers
offline_file: ""
offline_thumbnail: ""
uuid: a976ae6f-db65-4c76-98af-66587f5135c8
updated: 1484308790
title: Rencontres numbers
tags:
    - Numerical values
    - Formulas
    - Probability distribution
    - Limiting probability distribution
categories:
    - Fixed points (mathematics)
---
In combinatorial mathematics, the rencontres numbers are a triangular array of integers that enumerate permutations of the set { 1, ..., n } with specified numbers of fixed points: in other words, partial derangements. (Rencontre is French for encounter. By some accounts, the problem is named after a solitaire game.) For n ≥ 0 and 0 ≤ k ≤ n, the rencontres number Dn, k is the number of permutations of { 1, ..., n } that have exactly k fixed points.
