---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Huisken%27s_monotonicity_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 819c1640-44ef-4ea6-8890-dc91b37cb2f3
updated: 1484309223
title: "Huisken's monotonicity formula"
categories:
    - Differential geometry
---
In differential geometry, Huisken's monotonicity formula states that, if an n-dimensional surface in (n + 1)-dimensional Euclidean space undergoes the mean curvature flow, then its convolution with an appropriately scaled and time-reversed heat kernel is non-increasing.[1][2] The result is named after Gerhard Huisken, who published it in 1990.[3]
