---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bitterness
offline_file: ""
offline_thumbnail: ""
uuid: 445b0ae3-89b3-41ff-a55f-2ec8fe8ce4c7
updated: 1484310178
title: bitterness
categories:
    - Dictionary
---
bitterness
     n 1: a feeling of deep and bitter anger and ill-will [syn: {resentment},
           {gall}, {rancor}, {rancour}]
     2: a sharp and bitter manner [syn: {acrimony}, {acerbity}, {jaundice}]
     3: the taste experience when quinine or coffee is taken into
        the mouth [syn: {bitter}]
     4: the property of having a harsh unpleasant taste [syn: {bitter}]
