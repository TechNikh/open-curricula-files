---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Latent_heat
offline_file: ""
offline_thumbnail: ""
uuid: 767e297f-d0dd-47ad-b869-30a9806d7d86
updated: 1484308846
title: Latent heat
tags:
    - Usage
    - Meteorology
    - History
    - Specific latent heat
    - Table of specific latent heats
    - Specific latent heat for condensation of water
categories:
    - Thermodynamics
---
Latent heat is energy released or absorbed, by a body or a thermodynamic system, during a constant-temperature process. An example is latent heat of fusion for a phase change, melting, at a specified temperature and pressure.[1][2] The term was introduced around 1762 by Scottish chemist Joseph Black. It is derived from the Latin latere (to lie hidden). Black used the term in the context of calorimetry where a heat transfer caused a volume change while the thermodynamic system's temperature was constant.
